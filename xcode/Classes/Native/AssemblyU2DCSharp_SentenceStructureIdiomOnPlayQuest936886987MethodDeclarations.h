﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SentenceStructureIdiomOnPlayQuestion
struct SentenceStructureIdiomOnPlayQuestion_t936886987;
// SentenceStructureIdiomQuestionData
struct SentenceStructureIdiomQuestionData_t3251732102;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomQuestionDa3251732102.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomOnPlayQues3958551864.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void SentenceStructureIdiomOnPlayQuestion::.ctor(SentenceStructureIdiomQuestionData)
extern "C"  void SentenceStructureIdiomOnPlayQuestion__ctor_m3740272498 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, SentenceStructureIdiomQuestionData_t3251732102 * ___questionData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SentenceStructureIdiomOnPlayQuestion::CheckCorrect()
extern "C"  bool SentenceStructureIdiomOnPlayQuestion_CheckCorrect_m4084496898 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SentenceStructureIdiomOnPlayQuestion/SelectedState SentenceStructureIdiomOnPlayQuestion::get_currentSelected()
extern "C"  int32_t SentenceStructureIdiomOnPlayQuestion_get_currentSelected_m3661517574 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::set_currentSelected(SentenceStructureIdiomOnPlayQuestion/SelectedState)
extern "C"  void SentenceStructureIdiomOnPlayQuestion_set_currentSelected_m3722462147 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SentenceStructureIdiomQuestionData SentenceStructureIdiomOnPlayQuestion::get_questionData()
extern "C"  SentenceStructureIdiomQuestionData_t3251732102 * SentenceStructureIdiomOnPlayQuestion_get_questionData_m2647072446 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::set_questionData(SentenceStructureIdiomQuestionData)
extern "C"  void SentenceStructureIdiomOnPlayQuestion_set_questionData_m1586730965 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, SentenceStructureIdiomQuestionData_t3251732102 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::SetView(UnityEngine.GameObject)
extern "C"  void SentenceStructureIdiomOnPlayQuestion_SetView_m779025821 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, GameObject_t1756533147 * ___viewGo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::RemoveView()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_RemoveView_m717006993 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::ShowResult()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_ShowResult_m3919794390 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SentenceStructureIdiomOnPlayQuestion::GetScore()
extern "C"  float SentenceStructureIdiomOnPlayQuestion_GetScore_m3988825802 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::InitView()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_InitView_m2583909545 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::SetEventHandler()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_SetEventHandler_m1044468216 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::HandleSummitAction()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_HandleSummitAction_m2761825089 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::PlaySound()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_PlaySound_m3067689565 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::<SetEventHandler>m__0()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_U3CSetEventHandlerU3Em__0_m2105658403 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::<SetEventHandler>m__1()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_U3CSetEventHandlerU3Em__1_m2105658498 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::<SetEventHandler>m__2()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_U3CSetEventHandlerU3Em__2_m2105658469 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomOnPlayQuestion::<SetEventHandler>m__3()
extern "C"  void SentenceStructureIdiomOnPlayQuestion_U3CSetEventHandlerU3Em__3_m2105658564 (SentenceStructureIdiomOnPlayQuestion_t936886987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
