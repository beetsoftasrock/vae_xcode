﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VersionChecker
struct  VersionChecker_t1395544065  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct VersionChecker_t1395544065_StaticFields
{
public:
	// System.Boolean VersionChecker::<IsCheckDone>k__BackingField
	bool ___U3CIsCheckDoneU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsCheckDoneU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VersionChecker_t1395544065_StaticFields, ___U3CIsCheckDoneU3Ek__BackingField_2)); }
	inline bool get_U3CIsCheckDoneU3Ek__BackingField_2() const { return ___U3CIsCheckDoneU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsCheckDoneU3Ek__BackingField_2() { return &___U3CIsCheckDoneU3Ek__BackingField_2; }
	inline void set_U3CIsCheckDoneU3Ek__BackingField_2(bool value)
	{
		___U3CIsCheckDoneU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
