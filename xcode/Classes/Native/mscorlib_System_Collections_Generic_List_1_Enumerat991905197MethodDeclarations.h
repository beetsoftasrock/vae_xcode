﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct List_1_t1457175523;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat991905197.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1267196596_gshared (Enumerator_t991905197 * __this, List_1_t1457175523 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1267196596(__this, ___l0, method) ((  void (*) (Enumerator_t991905197 *, List_1_t1457175523 *, const MethodInfo*))Enumerator__ctor_m1267196596_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m997086566_gshared (Enumerator_t991905197 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m997086566(__this, method) ((  void (*) (Enumerator_t991905197 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m997086566_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3397371856_gshared (Enumerator_t991905197 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3397371856(__this, method) ((  Il2CppObject * (*) (Enumerator_t991905197 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3397371856_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Beetsoft.VAE.Purchaser/PurchaseSession>::Dispose()
extern "C"  void Enumerator_Dispose_m11628619_gshared (Enumerator_t991905197 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11628619(__this, method) ((  void (*) (Enumerator_t991905197 *, const MethodInfo*))Enumerator_Dispose_m11628619_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Beetsoft.VAE.Purchaser/PurchaseSession>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3201789146_gshared (Enumerator_t991905197 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3201789146(__this, method) ((  void (*) (Enumerator_t991905197 *, const MethodInfo*))Enumerator_VerifyState_m3201789146_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Beetsoft.VAE.Purchaser/PurchaseSession>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3396079182_gshared (Enumerator_t991905197 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3396079182(__this, method) ((  bool (*) (Enumerator_t991905197 *, const MethodInfo*))Enumerator_MoveNext_m3396079182_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Current()
extern "C"  PurchaseSession_t2088054391  Enumerator_get_Current_m1452050999_gshared (Enumerator_t991905197 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1452050999(__this, method) ((  PurchaseSession_t2088054391  (*) (Enumerator_t991905197 *, const MethodInfo*))Enumerator_get_Current_m1452050999_gshared)(__this, method)
