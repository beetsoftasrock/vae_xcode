﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EyeRaycaster
struct EyeRaycaster_t3983099531;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void EyeRaycaster::.ctor()
extern "C"  void EyeRaycaster__ctor_m127252950 (EyeRaycaster_t3983099531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EyeRaycaster::DoCheckingLoop()
extern "C"  Il2CppObject * EyeRaycaster_DoCheckingLoop_m2248058965 (EyeRaycaster_t3983099531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EyeRaycaster::OnEnable()
extern "C"  void EyeRaycaster_OnEnable_m3558320202 (EyeRaycaster_t3983099531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EyeRaycaster::Check()
extern "C"  void EyeRaycaster_Check_m2520721718 (EyeRaycaster_t3983099531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
