﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChapterSelector
struct ChapterSelector_t191193704;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterSelector/<jumpToChapter>c__Iterator2
struct  U3CjumpToChapterU3Ec__Iterator2_t1172627506  : public Il2CppObject
{
public:
	// System.Int32 ChapterSelector/<jumpToChapter>c__Iterator2::chapterId
	int32_t ___chapterId_0;
	// UnityEngine.Vector3 ChapterSelector/<jumpToChapter>c__Iterator2::<target>__0
	Vector3_t2243707580  ___U3CtargetU3E__0_1;
	// ChapterSelector ChapterSelector/<jumpToChapter>c__Iterator2::$this
	ChapterSelector_t191193704 * ___U24this_2;
	// System.Object ChapterSelector/<jumpToChapter>c__Iterator2::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean ChapterSelector/<jumpToChapter>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 ChapterSelector/<jumpToChapter>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_chapterId_0() { return static_cast<int32_t>(offsetof(U3CjumpToChapterU3Ec__Iterator2_t1172627506, ___chapterId_0)); }
	inline int32_t get_chapterId_0() const { return ___chapterId_0; }
	inline int32_t* get_address_of_chapterId_0() { return &___chapterId_0; }
	inline void set_chapterId_0(int32_t value)
	{
		___chapterId_0 = value;
	}

	inline static int32_t get_offset_of_U3CtargetU3E__0_1() { return static_cast<int32_t>(offsetof(U3CjumpToChapterU3Ec__Iterator2_t1172627506, ___U3CtargetU3E__0_1)); }
	inline Vector3_t2243707580  get_U3CtargetU3E__0_1() const { return ___U3CtargetU3E__0_1; }
	inline Vector3_t2243707580 * get_address_of_U3CtargetU3E__0_1() { return &___U3CtargetU3E__0_1; }
	inline void set_U3CtargetU3E__0_1(Vector3_t2243707580  value)
	{
		___U3CtargetU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CjumpToChapterU3Ec__Iterator2_t1172627506, ___U24this_2)); }
	inline ChapterSelector_t191193704 * get_U24this_2() const { return ___U24this_2; }
	inline ChapterSelector_t191193704 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ChapterSelector_t191193704 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CjumpToChapterU3Ec__Iterator2_t1172627506, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CjumpToChapterU3Ec__Iterator2_t1172627506, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CjumpToChapterU3Ec__Iterator2_t1172627506, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
