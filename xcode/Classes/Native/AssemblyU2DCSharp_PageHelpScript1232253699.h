﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PageHelpScript
struct  PageHelpScript_t1232253699  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite PageHelpScript::activeHelp
	Sprite_t309593783 * ___activeHelp_2;
	// UnityEngine.Sprite PageHelpScript::inactiveHelp
	Sprite_t309593783 * ___inactiveHelp_3;

public:
	inline static int32_t get_offset_of_activeHelp_2() { return static_cast<int32_t>(offsetof(PageHelpScript_t1232253699, ___activeHelp_2)); }
	inline Sprite_t309593783 * get_activeHelp_2() const { return ___activeHelp_2; }
	inline Sprite_t309593783 ** get_address_of_activeHelp_2() { return &___activeHelp_2; }
	inline void set_activeHelp_2(Sprite_t309593783 * value)
	{
		___activeHelp_2 = value;
		Il2CppCodeGenWriteBarrier(&___activeHelp_2, value);
	}

	inline static int32_t get_offset_of_inactiveHelp_3() { return static_cast<int32_t>(offsetof(PageHelpScript_t1232253699, ___inactiveHelp_3)); }
	inline Sprite_t309593783 * get_inactiveHelp_3() const { return ___inactiveHelp_3; }
	inline Sprite_t309593783 ** get_address_of_inactiveHelp_3() { return &___inactiveHelp_3; }
	inline void set_inactiveHelp_3(Sprite_t309593783 * value)
	{
		___inactiveHelp_3 = value;
		Il2CppCodeGenWriteBarrier(&___inactiveHelp_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
