﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChapterSelector
struct ChapterSelector_t191193704;

#include "AssemblyU2DCSharp_OpenHelpScript3207620564.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenHelp02HomeScript
struct  OpenHelp02HomeScript_t2955721185  : public OpenHelpScript_t3207620564
{
public:
	// ChapterSelector OpenHelp02HomeScript::chapterSelector
	ChapterSelector_t191193704 * ___chapterSelector_10;

public:
	inline static int32_t get_offset_of_chapterSelector_10() { return static_cast<int32_t>(offsetof(OpenHelp02HomeScript_t2955721185, ___chapterSelector_10)); }
	inline ChapterSelector_t191193704 * get_chapterSelector_10() const { return ___chapterSelector_10; }
	inline ChapterSelector_t191193704 ** get_address_of_chapterSelector_10() { return &___chapterSelector_10; }
	inline void set_chapterSelector_10(ChapterSelector_t191193704 * value)
	{
		___chapterSelector_10 = value;
		Il2CppCodeGenWriteBarrier(&___chapterSelector_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
