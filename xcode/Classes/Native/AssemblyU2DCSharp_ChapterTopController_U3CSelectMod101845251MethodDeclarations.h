﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopController/<SelectMode>c__AnonStorey2
struct U3CSelectModeU3Ec__AnonStorey2_t101845251;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterTopController/<SelectMode>c__AnonStorey2::.ctor()
extern "C"  void U3CSelectModeU3Ec__AnonStorey2__ctor_m813042766 (U3CSelectModeU3Ec__AnonStorey2_t101845251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController/<SelectMode>c__AnonStorey2::<>m__0()
extern "C"  void U3CSelectModeU3Ec__AnonStorey2_U3CU3Em__0_m618280225 (U3CSelectModeU3Ec__AnonStorey2_t101845251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController/<SelectMode>c__AnonStorey2::<>m__1()
extern "C"  void U3CSelectModeU3Ec__AnonStorey2_U3CU3Em__1_m618280258 (U3CSelectModeU3Ec__AnonStorey2_t101845251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
