﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PracticeVRSwitcher
struct PracticeVRSwitcher_t654240754;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void PracticeVRSwitcher::.ctor()
extern "C"  void PracticeVRSwitcher__ctor_m2459420781 (PracticeVRSwitcher_t654240754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher::GoToNormalMode(UnityEngine.Transform)
extern "C"  void PracticeVRSwitcher_GoToNormalMode_m1549531945 (PracticeVRSwitcher_t654240754 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher::YesGoToVRMode()
extern "C"  void PracticeVRSwitcher_YesGoToVRMode_m3065281106 (PracticeVRSwitcher_t654240754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher::GoToVRMode(UnityEngine.Transform)
extern "C"  void PracticeVRSwitcher_GoToVRMode_m2153952590 (PracticeVRSwitcher_t654240754 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher::BackToTop()
extern "C"  void PracticeVRSwitcher_BackToTop_m3468611856 (PracticeVRSwitcher_t654240754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher::QuitVRMode(UnityEngine.Transform)
extern "C"  void PracticeVRSwitcher_QuitVRMode_m595678698 (PracticeVRSwitcher_t654240754 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher::YesQuitVRMode()
extern "C"  void PracticeVRSwitcher_YesQuitVRMode_m2832202470 (PracticeVRSwitcher_t654240754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher::NoQuitVRMode(UnityEngine.Transform)
extern "C"  void PracticeVRSwitcher_NoQuitVRMode_m3700189777 (PracticeVRSwitcher_t654240754 * __this, Transform_t3275118058 * ___popupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher::<GoToNormalMode>m__0()
extern "C"  void PracticeVRSwitcher_U3CGoToNormalModeU3Em__0_m3250278533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
