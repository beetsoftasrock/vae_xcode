﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct Comparer_1_t978063510;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor()
extern "C"  void Comparer_1__ctor_m2579919006_gshared (Comparer_1_t978063510 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2579919006(__this, method) ((  void (*) (Comparer_1_t978063510 *, const MethodInfo*))Comparer_1__ctor_m2579919006_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.cctor()
extern "C"  void Comparer_1__cctor_m2064322975_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m2064322975(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m2064322975_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2998589567_gshared (Comparer_1_t978063510 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m2998589567(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t978063510 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m2998589567_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Default()
extern "C"  Comparer_1_t978063510 * Comparer_1_get_Default_m2097553054_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m2097553054(__this /* static, unused */, method) ((  Comparer_1_t978063510 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m2097553054_gshared)(__this /* static, unused */, method)
