﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.CrossPlatformValidator
struct CrossPlatformValidator_t305796431;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.Security.CrossPlatformValidator::.ctor(System.Byte[],System.Byte[],System.String)
extern "C"  void CrossPlatformValidator__ctor_m2169949003 (CrossPlatformValidator_t305796431 * __this, ByteU5BU5D_t3397334013* ___googlePublicKey0, ByteU5BU5D_t3397334013* ___appleRootCert1, String_t* ___appBundleId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.CrossPlatformValidator::.ctor(System.Byte[],System.Byte[],System.String,System.String)
extern "C"  void CrossPlatformValidator__ctor_m2046766649 (CrossPlatformValidator_t305796431 * __this, ByteU5BU5D_t3397334013* ___googlePublicKey0, ByteU5BU5D_t3397334013* ___appleRootCert1, String_t* ___googleBundleId2, String_t* ___appleBundleId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
