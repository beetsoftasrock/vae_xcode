﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableWithTimeCountdown
struct  DisableWithTimeCountdown_t622718188  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DisableWithTimeCountdown::autoDestroy
	bool ___autoDestroy_2;
	// System.Single DisableWithTimeCountdown::timeCountdown
	float ___timeCountdown_3;
	// System.Single DisableWithTimeCountdown::timeDelayStart
	float ___timeDelayStart_4;
	// System.Single DisableWithTimeCountdown::timeDelayFinish
	float ___timeDelayFinish_5;
	// UnityEngine.Events.UnityEvent DisableWithTimeCountdown::OnFinish
	UnityEvent_t408735097 * ___OnFinish_6;

public:
	inline static int32_t get_offset_of_autoDestroy_2() { return static_cast<int32_t>(offsetof(DisableWithTimeCountdown_t622718188, ___autoDestroy_2)); }
	inline bool get_autoDestroy_2() const { return ___autoDestroy_2; }
	inline bool* get_address_of_autoDestroy_2() { return &___autoDestroy_2; }
	inline void set_autoDestroy_2(bool value)
	{
		___autoDestroy_2 = value;
	}

	inline static int32_t get_offset_of_timeCountdown_3() { return static_cast<int32_t>(offsetof(DisableWithTimeCountdown_t622718188, ___timeCountdown_3)); }
	inline float get_timeCountdown_3() const { return ___timeCountdown_3; }
	inline float* get_address_of_timeCountdown_3() { return &___timeCountdown_3; }
	inline void set_timeCountdown_3(float value)
	{
		___timeCountdown_3 = value;
	}

	inline static int32_t get_offset_of_timeDelayStart_4() { return static_cast<int32_t>(offsetof(DisableWithTimeCountdown_t622718188, ___timeDelayStart_4)); }
	inline float get_timeDelayStart_4() const { return ___timeDelayStart_4; }
	inline float* get_address_of_timeDelayStart_4() { return &___timeDelayStart_4; }
	inline void set_timeDelayStart_4(float value)
	{
		___timeDelayStart_4 = value;
	}

	inline static int32_t get_offset_of_timeDelayFinish_5() { return static_cast<int32_t>(offsetof(DisableWithTimeCountdown_t622718188, ___timeDelayFinish_5)); }
	inline float get_timeDelayFinish_5() const { return ___timeDelayFinish_5; }
	inline float* get_address_of_timeDelayFinish_5() { return &___timeDelayFinish_5; }
	inline void set_timeDelayFinish_5(float value)
	{
		___timeDelayFinish_5 = value;
	}

	inline static int32_t get_offset_of_OnFinish_6() { return static_cast<int32_t>(offsetof(DisableWithTimeCountdown_t622718188, ___OnFinish_6)); }
	inline UnityEvent_t408735097 * get_OnFinish_6() const { return ___OnFinish_6; }
	inline UnityEvent_t408735097 ** get_address_of_OnFinish_6() { return &___OnFinish_6; }
	inline void set_OnFinish_6(UnityEvent_t408735097 * value)
	{
		___OnFinish_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnFinish_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
