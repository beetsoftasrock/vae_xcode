﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InvokerModule/<DoReplayAllCommand>c__Iterator1
struct U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void InvokerModule/<DoReplayAllCommand>c__Iterator1::.ctor()
extern "C"  void U3CDoReplayAllCommandU3Ec__Iterator1__ctor_m2367288606 (U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InvokerModule/<DoReplayAllCommand>c__Iterator1::MoveNext()
extern "C"  bool U3CDoReplayAllCommandU3Ec__Iterator1_MoveNext_m132694926 (U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InvokerModule/<DoReplayAllCommand>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoReplayAllCommandU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3528415186 (U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InvokerModule/<DoReplayAllCommand>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoReplayAllCommandU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m930336154 (U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule/<DoReplayAllCommand>c__Iterator1::Dispose()
extern "C"  void U3CDoReplayAllCommandU3Ec__Iterator1_Dispose_m605515379 (U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule/<DoReplayAllCommand>c__Iterator1::Reset()
extern "C"  void U3CDoReplayAllCommandU3Ec__Iterator1_Reset_m402790941 (U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
