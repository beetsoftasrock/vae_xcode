﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// Beetsoft.VAE.SoundResult
struct SoundResult_t4177104648;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.ManagerInstance/<trackingPlay>c__Iterator0
struct  U3CtrackingPlayU3Ec__Iterator0_t3547164181  : public Il2CppObject
{
public:
	// UnityEngine.AudioSource Beetsoft.VAE.ManagerInstance/<trackingPlay>c__Iterator0::targetSource
	AudioSource_t1135106623 * ___targetSource_0;
	// Beetsoft.VAE.SoundResult Beetsoft.VAE.ManagerInstance/<trackingPlay>c__Iterator0::soundResult
	SoundResult_t4177104648 * ___soundResult_1;
	// System.Object Beetsoft.VAE.ManagerInstance/<trackingPlay>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean Beetsoft.VAE.ManagerInstance/<trackingPlay>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Beetsoft.VAE.ManagerInstance/<trackingPlay>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_targetSource_0() { return static_cast<int32_t>(offsetof(U3CtrackingPlayU3Ec__Iterator0_t3547164181, ___targetSource_0)); }
	inline AudioSource_t1135106623 * get_targetSource_0() const { return ___targetSource_0; }
	inline AudioSource_t1135106623 ** get_address_of_targetSource_0() { return &___targetSource_0; }
	inline void set_targetSource_0(AudioSource_t1135106623 * value)
	{
		___targetSource_0 = value;
		Il2CppCodeGenWriteBarrier(&___targetSource_0, value);
	}

	inline static int32_t get_offset_of_soundResult_1() { return static_cast<int32_t>(offsetof(U3CtrackingPlayU3Ec__Iterator0_t3547164181, ___soundResult_1)); }
	inline SoundResult_t4177104648 * get_soundResult_1() const { return ___soundResult_1; }
	inline SoundResult_t4177104648 ** get_address_of_soundResult_1() { return &___soundResult_1; }
	inline void set_soundResult_1(SoundResult_t4177104648 * value)
	{
		___soundResult_1 = value;
		Il2CppCodeGenWriteBarrier(&___soundResult_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CtrackingPlayU3Ec__Iterator0_t3547164181, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CtrackingPlayU3Ec__Iterator0_t3547164181, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CtrackingPlayU3Ec__Iterator0_t3547164181, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
