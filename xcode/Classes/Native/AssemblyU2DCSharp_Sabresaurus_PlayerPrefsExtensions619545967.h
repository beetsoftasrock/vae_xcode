﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Security.Cryptography.RijndaelManaged
struct RijndaelManaged_t1034060848;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sabresaurus.PlayerPrefsExtensions.SimpleEncryption
struct  SimpleEncryption_t619545967  : public Il2CppObject
{
public:

public:
};

struct SimpleEncryption_t619545967_StaticFields
{
public:
	// System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::key
	String_t* ___key_0;
	// System.Security.Cryptography.RijndaelManaged Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::provider
	RijndaelManaged_t1034060848 * ___provider_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(SimpleEncryption_t619545967_StaticFields, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_provider_1() { return static_cast<int32_t>(offsetof(SimpleEncryption_t619545967_StaticFields, ___provider_1)); }
	inline RijndaelManaged_t1034060848 * get_provider_1() const { return ___provider_1; }
	inline RijndaelManaged_t1034060848 ** get_address_of_provider_1() { return &___provider_1; }
	inline void set_provider_1(RijndaelManaged_t1034060848 * value)
	{
		___provider_1 = value;
		Il2CppCodeGenWriteBarrier(&___provider_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
