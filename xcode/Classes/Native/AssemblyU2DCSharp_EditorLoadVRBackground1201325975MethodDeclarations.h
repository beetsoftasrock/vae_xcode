﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditorLoadVRBackground
struct EditorLoadVRBackground_t1201325975;

#include "codegen/il2cpp-codegen.h"

// System.Void EditorLoadVRBackground::.ctor()
extern "C"  void EditorLoadVRBackground__ctor_m4192953712 (EditorLoadVRBackground_t1201325975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorLoadVRBackground::Start()
extern "C"  void EditorLoadVRBackground_Start_m969509540 (EditorLoadVRBackground_t1201325975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
