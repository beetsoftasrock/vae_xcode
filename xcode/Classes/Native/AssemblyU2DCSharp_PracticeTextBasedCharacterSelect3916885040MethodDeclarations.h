﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PracticeTextBasedCharacterSelect
struct PracticeTextBasedCharacterSelect_t3916885040;

#include "codegen/il2cpp-codegen.h"

// System.Void PracticeTextBasedCharacterSelect::.ctor()
extern "C"  void PracticeTextBasedCharacterSelect__ctor_m4085711673 (PracticeTextBasedCharacterSelect_t3916885040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeTextBasedCharacterSelect::Awake()
extern "C"  void PracticeTextBasedCharacterSelect_Awake_m3451663756 (PracticeTextBasedCharacterSelect_t3916885040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeTextBasedCharacterSelect::HandleCharacterChangeEvent(System.Int32)
extern "C"  void PracticeTextBasedCharacterSelect_HandleCharacterChangeEvent_m506239985 (PracticeTextBasedCharacterSelect_t3916885040 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
