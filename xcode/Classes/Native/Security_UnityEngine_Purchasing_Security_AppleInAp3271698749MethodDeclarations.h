﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct AppleInAppPurchaseReceipt_t3271698749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::.ctor()
extern "C"  void AppleInAppPurchaseReceipt__ctor_m1941626255 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_quantity()
extern "C"  int32_t AppleInAppPurchaseReceipt_get_quantity_m2899296427 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_quantity(System.Int32)
extern "C"  void AppleInAppPurchaseReceipt_set_quantity_m347140636 (AppleInAppPurchaseReceipt_t3271698749 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_productID()
extern "C"  String_t* AppleInAppPurchaseReceipt_get_productID_m152993489 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_productID(System.String)
extern "C"  void AppleInAppPurchaseReceipt_set_productID_m2720522190 (AppleInAppPurchaseReceipt_t3271698749 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_transactionID()
extern "C"  String_t* AppleInAppPurchaseReceipt_get_transactionID_m677217978 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_transactionID(System.String)
extern "C"  void AppleInAppPurchaseReceipt_set_transactionID_m3165163845 (AppleInAppPurchaseReceipt_t3271698749 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_originalTransactionIdentifier(System.String)
extern "C"  void AppleInAppPurchaseReceipt_set_originalTransactionIdentifier_m2823063774 (AppleInAppPurchaseReceipt_t3271698749 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_purchaseDate()
extern "C"  DateTime_t693205669  AppleInAppPurchaseReceipt_get_purchaseDate_m1052795456 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_purchaseDate(System.DateTime)
extern "C"  void AppleInAppPurchaseReceipt_set_purchaseDate_m1082187471 (AppleInAppPurchaseReceipt_t3271698749 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_originalPurchaseDate(System.DateTime)
extern "C"  void AppleInAppPurchaseReceipt_set_originalPurchaseDate_m1920060578 (AppleInAppPurchaseReceipt_t3271698749 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_subscriptionExpirationDate()
extern "C"  DateTime_t693205669  AppleInAppPurchaseReceipt_get_subscriptionExpirationDate_m3742016707 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_subscriptionExpirationDate(System.DateTime)
extern "C"  void AppleInAppPurchaseReceipt_set_subscriptionExpirationDate_m2975097394 (AppleInAppPurchaseReceipt_t3271698749 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_cancellationDate()
extern "C"  DateTime_t693205669  AppleInAppPurchaseReceipt_get_cancellationDate_m4106835304 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_cancellationDate(System.DateTime)
extern "C"  void AppleInAppPurchaseReceipt_set_cancellationDate_m2514312931 (AppleInAppPurchaseReceipt_t3271698749 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
