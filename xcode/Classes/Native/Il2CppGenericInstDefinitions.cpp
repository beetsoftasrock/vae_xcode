﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2071877448_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Char_t3454481338_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
extern const Il2CppType IConvertible_t908092482_0_0_0;
static const Il2CppType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { &IConvertible_t908092482_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
extern const Il2CppType IComparable_t1857082765_0_0_0;
static const Il2CppType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { &IComparable_t1857082765_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
extern const Il2CppType IComparable_1_t991353265_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t991353265_0_0_0_Types[] = { &IComparable_1_t991353265_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t991353265_0_0_0 = { 1, GenInst_IComparable_1_t991353265_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1363496211_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1363496211_0_0_0_Types[] = { &IEquatable_1_t1363496211_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1363496211_0_0_0 = { 1, GenInst_IEquatable_1_t1363496211_0_0_0_Types };
extern const Il2CppType ValueType_t3507792607_0_0_0;
static const Il2CppType* GenInst_ValueType_t3507792607_0_0_0_Types[] = { &ValueType_t3507792607_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t3507792607_0_0_0 = { 1, GenInst_ValueType_t3507792607_0_0_0_Types };
extern const Il2CppType Int64_t909078037_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
extern const Il2CppType UInt32_t2149682021_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
extern const Il2CppType UInt64_t2909196914_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
extern const Il2CppType Byte_t3683104436_0_0_0;
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
extern const Il2CppType SByte_t454417549_0_0_0;
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Types[] = { &SByte_t454417549_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
extern const Il2CppType Int16_t4041245914_0_0_0;
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
extern const Il2CppType UInt16_t986882611_0_0_0;
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Types[] = { &UInt16_t986882611_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { &IEnumerable_t2911409499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
extern const Il2CppType ICloneable_t3853279282_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { &ICloneable_t3853279282_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { &IComparable_1_t3861059456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { &IEquatable_1_t4233202402_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t3412036974_0_0_0;
static const Il2CppType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { &IReflect_t3412036974_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
extern const Il2CppType _Type_t102776839_0_0_0;
static const Il2CppType* GenInst__Type_t102776839_0_0_0_Types[] = { &_Type_t102776839_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { &_MemberInfo_t332722161_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
extern const Il2CppType IFormattable_t1523031934_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1523031934_0_0_0_Types[] = { &IFormattable_t1523031934_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1523031934_0_0_0 = { 1, GenInst_IFormattable_t1523031934_0_0_0_Types };
extern const Il2CppType IComparable_1_t3903716671_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3903716671_0_0_0_Types[] = { &IComparable_1_t3903716671_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3903716671_0_0_0 = { 1, GenInst_IComparable_1_t3903716671_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4275859617_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4275859617_0_0_0_Types[] = { &IEquatable_1_t4275859617_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4275859617_0_0_0 = { 1, GenInst_IEquatable_1_t4275859617_0_0_0_Types };
extern const Il2CppType Double_t4078015681_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
extern const Il2CppType IComparable_1_t1614887608_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1614887608_0_0_0_Types[] = { &IComparable_1_t1614887608_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1614887608_0_0_0 = { 1, GenInst_IComparable_1_t1614887608_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1987030554_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1987030554_0_0_0_Types[] = { &IEquatable_1_t1987030554_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1987030554_0_0_0 = { 1, GenInst_IEquatable_1_t1987030554_0_0_0_Types };
extern const Il2CppType IComparable_1_t3981521244_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3981521244_0_0_0_Types[] = { &IComparable_1_t3981521244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3981521244_0_0_0 = { 1, GenInst_IComparable_1_t3981521244_0_0_0_Types };
extern const Il2CppType IEquatable_1_t58696894_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t58696894_0_0_0_Types[] = { &IEquatable_1_t58696894_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t58696894_0_0_0 = { 1, GenInst_IEquatable_1_t58696894_0_0_0_Types };
extern const Il2CppType IComparable_1_t1219976363_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1219976363_0_0_0_Types[] = { &IComparable_1_t1219976363_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1219976363_0_0_0 = { 1, GenInst_IComparable_1_t1219976363_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1592119309_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1592119309_0_0_0_Types[] = { &IEquatable_1_t1592119309_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1592119309_0_0_0 = { 1, GenInst_IEquatable_1_t1592119309_0_0_0_Types };
extern const Il2CppType Single_t2076509932_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
extern const Il2CppType IComparable_1_t3908349155_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3908349155_0_0_0_Types[] = { &IComparable_1_t3908349155_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3908349155_0_0_0 = { 1, GenInst_IComparable_1_t3908349155_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4280492101_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4280492101_0_0_0_Types[] = { &IEquatable_1_t4280492101_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4280492101_0_0_0 = { 1, GenInst_IEquatable_1_t4280492101_0_0_0_Types };
extern const Il2CppType Decimal_t724701077_0_0_0;
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType Boolean_t3825574718_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Delegate_t3022476291_0_0_0;
static const Il2CppType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
extern const Il2CppType ISerializable_t1245643778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { &ISerializable_t1245643778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { &_ParameterInfo_t470209990_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { &ParameterModifier_t1820634920_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
extern const Il2CppType IComparable_1_t2818721834_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2818721834_0_0_0_Types[] = { &IComparable_1_t2818721834_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2818721834_0_0_0 = { 1, GenInst_IComparable_1_t2818721834_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3190864780_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3190864780_0_0_0_Types[] = { &IEquatable_1_t3190864780_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3190864780_0_0_0 = { 1, GenInst_IEquatable_1_t3190864780_0_0_0_Types };
extern const Il2CppType IComparable_1_t446068841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t446068841_0_0_0_Types[] = { &IComparable_1_t446068841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t446068841_0_0_0 = { 1, GenInst_IComparable_1_t446068841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t818211787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t818211787_0_0_0_Types[] = { &IEquatable_1_t818211787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t818211787_0_0_0 = { 1, GenInst_IEquatable_1_t818211787_0_0_0_Types };
extern const Il2CppType IComparable_1_t1578117841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1578117841_0_0_0_Types[] = { &IComparable_1_t1578117841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1578117841_0_0_0 = { 1, GenInst_IComparable_1_t1578117841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1950260787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1950260787_0_0_0_Types[] = { &IEquatable_1_t1950260787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1950260787_0_0_0 = { 1, GenInst_IEquatable_1_t1950260787_0_0_0_Types };
extern const Il2CppType IComparable_1_t2286256772_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2286256772_0_0_0_Types[] = { &IComparable_1_t2286256772_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2286256772_0_0_0 = { 1, GenInst_IComparable_1_t2286256772_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2658399718_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2658399718_0_0_0_Types[] = { &IEquatable_1_t2658399718_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2658399718_0_0_0 = { 1, GenInst_IEquatable_1_t2658399718_0_0_0_Types };
extern const Il2CppType IComparable_1_t2740917260_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2740917260_0_0_0_Types[] = { &IComparable_1_t2740917260_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2740917260_0_0_0 = { 1, GenInst_IComparable_1_t2740917260_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3113060206_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3113060206_0_0_0_Types[] = { &IEquatable_1_t3113060206_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3113060206_0_0_0 = { 1, GenInst_IEquatable_1_t3113060206_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { &_FieldInfo_t2511231167_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { &_MethodInfo_t3642518830_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
extern const Il2CppType MethodBase_t904190842_0_0_0;
static const Il2CppType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { &MethodBase_t904190842_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { &_MethodBase_t1935530873_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { &_PropertyInfo_t1567586598_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { &_ConstructorInfo_t3269099341_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t2011406615_0_0_0;
static const Il2CppType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { &TableRange_t2011406615_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { &TailoringInfo_t1449609243_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
extern const Il2CppType Link_t2723257478_0_0_0;
static const Il2CppType* GenInst_Link_t2723257478_0_0_0_Types[] = { &Link_t2723257478_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
extern const Il2CppType Contraction_t1673853792_0_0_0;
static const Il2CppType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { &Contraction_t1673853792_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
extern const Il2CppType Level2Map_t3322505726_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { &Level2Map_t3322505726_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
extern const Il2CppType BigInteger_t925946152_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { &BigInteger_t925946152_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
extern const Il2CppType KeySizes_t3144736271_0_0_0;
static const Il2CppType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { &KeySizes_t3144736271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType IComparable_1_t1362446645_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1362446645_0_0_0_Types[] = { &IComparable_1_t1362446645_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1362446645_0_0_0 = { 1, GenInst_IComparable_1_t1362446645_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1734589591_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1734589591_0_0_0_Types[] = { &IEquatable_1_t1734589591_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1734589591_0_0_0 = { 1, GenInst_IEquatable_1_t1734589591_0_0_0_Types };
extern const Il2CppType Slot_t2022531261_0_0_0;
static const Il2CppType* GenInst_Slot_t2022531261_0_0_0_Types[] = { &Slot_t2022531261_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
extern const Il2CppType Slot_t2267560602_0_0_0;
static const Il2CppType* GenInst_Slot_t2267560602_0_0_0_Types[] = { &Slot_t2267560602_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
extern const Il2CppType StackFrame_t2050294881_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { &StackFrame_t2050294881_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
extern const Il2CppType Calendar_t585061108_0_0_0;
static const Il2CppType* GenInst_Calendar_t585061108_0_0_0_Types[] = { &Calendar_t585061108_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { &ModuleBuilder_t4156028127_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { &_ModuleBuilder_t1075102050_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
extern const Il2CppType Module_t4282841206_0_0_0;
static const Il2CppType* GenInst_Module_t4282841206_0_0_0_Types[] = { &Module_t4282841206_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
extern const Il2CppType _Module_t2144668161_0_0_0;
static const Il2CppType* GenInst__Module_t2144668161_0_0_0_Types[] = { &_Module_t2144668161_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { &ParameterBuilder_t3344728474_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { &_ParameterBuilder_t2251638747_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { &TypeU5BU5D_t1664964607_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t91669223_0_0_0;
static const Il2CppType* GenInst_ICollection_t91669223_0_0_0_Types[] = { &ICollection_t91669223_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
extern const Il2CppType IList_t3321498491_0_0_0;
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Types[] = { &IList_t3321498491_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { &ILTokenInfo_t149559338_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
extern const Il2CppType LabelData_t3712112744_0_0_0;
static const Il2CppType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { &LabelData_t3712112744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { &LabelFixup_t4090909514_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1370236603_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { &_TypeBuilder_t2783404358_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { &MethodBuilder_t644187984_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { &_MethodBuilder_t3932949077_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { &ConstructorBuilder_t700974433_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { &_ConstructorBuilder_t1236878896_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { &PropertyBuilder_t3694255912_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { &_PropertyBuilder_t3341912621_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { &FieldBuilder_t2784804005_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { &_FieldBuilder_t1895266044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { &CustomAttributeData_t3093286891_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { &ResourceInfo_t3933049236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { &ResourceCacheItem_t333236149_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
extern const Il2CppType IContextProperty_t287246399_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { &IContextProperty_t287246399_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
extern const Il2CppType Header_t2756440555_0_0_0;
static const Il2CppType* GenInst_Header_t2756440555_0_0_0_Types[] = { &Header_t2756440555_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { &ITrackingHandler_t2759960940_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { &IContextAttribute_t2439121372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
extern const Il2CppType DateTime_t693205669_0_0_0;
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Types[] = { &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType IComparable_1_t2525044892_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2525044892_0_0_0_Types[] = { &IComparable_1_t2525044892_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2525044892_0_0_0 = { 1, GenInst_IComparable_1_t2525044892_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2897187838_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2897187838_0_0_0_Types[] = { &IEquatable_1_t2897187838_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2897187838_0_0_0 = { 1, GenInst_IEquatable_1_t2897187838_0_0_0_Types };
extern const Il2CppType IComparable_1_t2556540300_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2556540300_0_0_0_Types[] = { &IComparable_1_t2556540300_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2556540300_0_0_0 = { 1, GenInst_IComparable_1_t2556540300_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2928683246_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2928683246_0_0_0_Types[] = { &IEquatable_1_t2928683246_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2928683246_0_0_0 = { 1, GenInst_IEquatable_1_t2928683246_0_0_0_Types };
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { &TimeSpan_t3430258949_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
extern const Il2CppType IComparable_1_t967130876_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t967130876_0_0_0_Types[] = { &IComparable_1_t967130876_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t967130876_0_0_0 = { 1, GenInst_IComparable_1_t967130876_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1339273822_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1339273822_0_0_0_Types[] = { &IEquatable_1_t1339273822_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1339273822_0_0_0 = { 1, GenInst_IEquatable_1_t1339273822_0_0_0_Types };
extern const Il2CppType TypeTag_t141209596_0_0_0;
static const Il2CppType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { &TypeTag_t141209596_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
extern const Il2CppType Enum_t2459695545_0_0_0;
static const Il2CppType* GenInst_Enum_t2459695545_0_0_0_Types[] = { &Enum_t2459695545_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2459695545_0_0_0 = { 1, GenInst_Enum_t2459695545_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t2988747270_0_0_0;
static const Il2CppType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { &StrongName_t2988747270_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
extern const Il2CppType WaitHandle_t677569169_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t677569169_0_0_0_Types[] = { &WaitHandle_t677569169_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t677569169_0_0_0 = { 1, GenInst_WaitHandle_t677569169_0_0_0_Types };
extern const Il2CppType IDisposable_t2427283555_0_0_0;
static const Il2CppType* GenInst_IDisposable_t2427283555_0_0_0_Types[] = { &IDisposable_t2427283555_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t2427283555_0_0_0 = { 1, GenInst_IDisposable_t2427283555_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1285298191_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1285298191_0_0_0_Types[] = { &MarshalByRefObject_t1285298191_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1285298191_0_0_0 = { 1, GenInst_MarshalByRefObject_t1285298191_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { &DateTimeOffset_t1362988906_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
extern const Il2CppType Guid_t2533601593_0_0_0;
static const Il2CppType* GenInst_Guid_t2533601593_0_0_0_Types[] = { &Guid_t2533601593_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2533601593_0_0_0 = { 1, GenInst_Guid_t2533601593_0_0_0_Types };
extern const Il2CppType Version_t1755874712_0_0_0;
static const Il2CppType* GenInst_Version_t1755874712_0_0_0_Types[] = { &Version_t1755874712_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
extern const Il2CppType BigInteger_t925946153_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { &BigInteger_t925946153_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType X509Certificate_t283079845_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { &X509Certificate_t283079845_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { &IDeserializationCallback_t327125377_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { &ClientCertificateType_t4001384466_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { &X509ChainStatus_t4278378721_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
extern const Il2CppType IPAddress_t1399971723_0_0_0;
static const Il2CppType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { &IPAddress_t1399971723_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { &ArraySegment_1_t2594217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
extern const Il2CppType Cookie_t3154017544_0_0_0;
static const Il2CppType* GenInst_Cookie_t3154017544_0_0_0_Types[] = { &Cookie_t3154017544_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t3154017544_0_0_0 = { 1, GenInst_Cookie_t3154017544_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
extern const Il2CppType Capture_t4157900610_0_0_0;
static const Il2CppType* GenInst_Capture_t4157900610_0_0_0_Types[] = { &Capture_t4157900610_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
extern const Il2CppType Group_t3761430853_0_0_0;
static const Il2CppType* GenInst_Group_t3761430853_0_0_0_Types[] = { &Group_t3761430853_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
extern const Il2CppType Mark_t2724874473_0_0_0;
static const Il2CppType* GenInst_Mark_t2724874473_0_0_0_Types[] = { &Mark_t2724874473_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
extern const Il2CppType UriScheme_t1876590943_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { &UriScheme_t1876590943_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
extern const Il2CppType Link_t865133271_0_0_0;
static const Il2CppType* GenInst_Link_t865133271_0_0_0_Types[] = { &Link_t865133271_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
extern const Il2CppType jvalue_t3412352577_0_0_0;
static const Il2CppType* GenInst_jvalue_t3412352577_0_0_0_Types[] = { &jvalue_t3412352577_0_0_0 };
extern const Il2CppGenericInst GenInst_jvalue_t3412352577_0_0_0 = { 1, GenInst_jvalue_t3412352577_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_t4251328308_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_t4251328308_0_0_0_Types[] = { &AndroidJavaObject_t4251328308_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_t4251328308_0_0_0 = { 1, GenInst_AndroidJavaObject_t4251328308_0_0_0_Types };
extern const Il2CppType Object_t1021602117_0_0_0;
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Types[] = { &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
extern const Il2CppType Camera_t189460977_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
extern const Il2CppType Behaviour_t955675639_0_0_0;
static const Il2CppType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { &Behaviour_t955675639_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
extern const Il2CppType Component_t3819376471_0_0_0;
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Types[] = { &Component_t3819376471_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
extern const Il2CppType Display_t3666191348_0_0_0;
static const Il2CppType* GenInst_Display_t3666191348_0_0_0_Types[] = { &Display_t3666191348_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { &AchievementDescription_t3110978151_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { &IAchievementDescription_t3498529102_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
extern const Il2CppType UserProfile_t3365630962_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { &UserProfile_t3365630962_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { &IUserProfile_t4108565527_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { &GcLeaderboard_t453887929_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4083280315_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { &IAchievementU5BU5D_t2709554645_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
extern const Il2CppType IAchievement_t1752291260_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { &IAchievement_t1752291260_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { &GcAchievementData_t1754866149_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
extern const Il2CppType Achievement_t1333316625_0_0_0;
static const Il2CppType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { &Achievement_t1333316625_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { &IScoreU5BU5D_t3237304636_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
extern const Il2CppType IScore_t513966369_0_0_0;
static const Il2CppType* GenInst_IScore_t513966369_0_0_0_Types[] = { &IScore_t513966369_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { &GcScoreData_t3676783238_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
extern const Il2CppType Score_t2307748940_0_0_0;
static const Il2CppType* GenInst_Score_t2307748940_0_0_0_Types[] = { &Score_t2307748940_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { &IUserProfileU5BU5D_t3461248430_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
extern const Il2CppType Touch_t407273883_0_0_0;
static const Il2CppType* GenInst_Touch_t407273883_0_0_0_Types[] = { &Touch_t407273883_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t407273883_0_0_0 = { 1, GenInst_Touch_t407273883_0_0_0_Types };
extern const Il2CppType Keyframe_t1449471340_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { &Keyframe_t1449471340_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
extern const Il2CppType Vector3_t2243707580_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
extern const Il2CppType Vector4_t2243707581_0_0_0;
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
extern const Il2CppType Vector2_t2243707579_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType Color_t2020392075_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
extern const Il2CppType Color32_t874517518_0_0_0;
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
extern const Il2CppType Playable_t3667545548_0_0_0;
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0 = { 1, GenInst_Playable_t3667545548_0_0_0_Types };
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &LoadSceneMode_t2981886439_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { &ContactPoint_t1376425630_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
extern const Il2CppType RaycastHit_t87180320_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { &RaycastHit_t87180320_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { &Rigidbody2D_t502193897_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { &RaycastHit2D_t4063908774_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { &ContactPoint2D_t3659330976_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
extern const Il2CppType AudioMixerGroup_t959546644_0_0_0;
static const Il2CppType* GenInst_AudioMixerGroup_t959546644_0_0_0_Types[] = { &AudioMixerGroup_t959546644_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioMixerGroup_t959546644_0_0_0 = { 1, GenInst_AudioMixerGroup_t959546644_0_0_0_Types };
extern const Il2CppType UIVertex_t1204258818_0_0_0;
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
extern const Il2CppType Font_t4239498691_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Types[] = { &Font_t4239498691_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { &GUILayoutOption_t4183744904_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { &GUILayoutEntry_t3828586629_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t3120781045_0_0_0_Types[] = { &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t3120781045_0_0_0 = { 1, GenInst_LayoutCache_t3120781045_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
extern const Il2CppType Event_t3028476042_0_0_0;
extern const Il2CppType TextEditOp_t3138797698_0_0_0;
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t488203048_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0 = { 1, GenInst_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0 = { 1, GenInst_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_Types[] = { &Event_t3028476042_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0 = { 1, GenInst_Event_t3028476042_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3799506081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3799506081_0_0_0_Types[] = { &KeyValuePair_2_t3799506081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3799506081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3799506081_0_0_0_Types };
extern const Il2CppType ConstructorDelegate_t3084043859_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t3084043859_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0 = { 2, GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_Types };
static const Il2CppType* GenInst_ConstructorDelegate_t3084043859_0_0_0_Types[] = { &ConstructorDelegate_t3084043859_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t3084043859_0_0_0 = { 1, GenInst_ConstructorDelegate_t3084043859_0_0_0_Types };
extern const Il2CppType IDictionary_2_t266144316_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t266144316_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_Types };
extern const Il2CppType GetDelegate_t352281633_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t352281633_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0 = { 2, GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_Types };
static const Il2CppType* GenInst_GetDelegate_t352281633_0_0_0_Types[] = { &GetDelegate_t352281633_0_0_0 };
extern const Il2CppGenericInst GenInst_GetDelegate_t352281633_0_0_0 = { 1, GenInst_GetDelegate_t352281633_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t266144316_0_0_0_Types[] = { &IDictionary_2_t266144316_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t266144316_0_0_0 = { 1, GenInst_IDictionary_2_t266144316_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3814930911_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3814930911_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3901068228_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t3901068228_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0 = { 2, GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_Types };
extern const Il2CppType SetDelegate_t4206365109_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_SetDelegate_t4206365109_0_0_0_Types[] = { &Type_t_0_0_0, &SetDelegate_t4206365109_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t4206365109_0_0_0 = { 2, GenInst_Type_t_0_0_0_SetDelegate_t4206365109_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3901068228_0_0_0_Types[] = { &KeyValuePair_2_t3901068228_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3901068228_0_0_0 = { 1, GenInst_KeyValuePair_2_t3901068228_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3814930911_0_0_0_Types[] = { &IDictionary_2_t3814930911_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3814930911_0_0_0 = { 1, GenInst_IDictionary_2_t3814930911_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1683227291_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1683227291_0_0_0_Types[] = { &KeyValuePair_2_t1683227291_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1683227291_0_0_0 = { 1, GenInst_KeyValuePair_2_t1683227291_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t3084043859_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2778746978_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2778746978_0_0_0_Types[] = { &KeyValuePair_2_t2778746978_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2778746978_0_0_0 = { 1, GenInst_KeyValuePair_2_t2778746978_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t266144316_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4255814731_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4255814731_0_0_0_Types[] = { &KeyValuePair_2_t4255814731_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4255814731_0_0_0 = { 1, GenInst_KeyValuePair_2_t4255814731_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3814930911_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3509634030_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3509634030_0_0_0_Types[] = { &KeyValuePair_2_t3509634030_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3509634030_0_0_0 = { 1, GenInst_KeyValuePair_2_t3509634030_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t352281633_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t24406117_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t24406117_0_0_0_Types[] = { &KeyValuePair_2_t24406117_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t24406117_0_0_0 = { 1, GenInst_KeyValuePair_2_t24406117_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t1683227291_0_0_0_Types[] = { &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t1683227291_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t1683227291_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t1683227291_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t3901068228_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3573192712_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3573192712_0_0_0_Types[] = { &KeyValuePair_2_t3573192712_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3573192712_0_0_0 = { 1, GenInst_KeyValuePair_2_t3573192712_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { &DisallowMultipleComponent_t2656950_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
extern const Il2CppType Attribute_t542643598_0_0_0;
static const Il2CppType* GenInst_Attribute_t542643598_0_0_0_Types[] = { &Attribute_t542643598_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
extern const Il2CppType _Attribute_t1557664299_0_0_0;
static const Il2CppType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { &_Attribute_t1557664299_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { &ExecuteInEditMode_t3043633143_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
extern const Il2CppType RequireComponent_t864575032_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { &RequireComponent_t864575032_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
extern const Il2CppType HitInfo_t1761367055_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { &HitInfo_t1761367055_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { &PersistentCall_t3793436469_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { &BaseInvokableCall_t2229564840_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
extern const Il2CppType SignerInfo_t4122348804_0_0_0;
static const Il2CppType* GenInst_SignerInfo_t4122348804_0_0_0_Types[] = { &SignerInfo_t4122348804_0_0_0 };
extern const Il2CppGenericInst GenInst_SignerInfo_t4122348804_0_0_0 = { 1, GenInst_SignerInfo_t4122348804_0_0_0_Types };
extern const Il2CppType X509Cert_t481809278_0_0_0;
static const Il2CppType* GenInst_X509Cert_t481809278_0_0_0_Types[] = { &X509Cert_t481809278_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Cert_t481809278_0_0_0 = { 1, GenInst_X509Cert_t481809278_0_0_0_Types };
extern const Il2CppType AppleInAppPurchaseReceipt_t3271698749_0_0_0;
static const Il2CppType* GenInst_AppleInAppPurchaseReceipt_t3271698749_0_0_0_Types[] = { &AppleInAppPurchaseReceipt_t3271698749_0_0_0 };
extern const Il2CppGenericInst GenInst_AppleInAppPurchaseReceipt_t3271698749_0_0_0 = { 1, GenInst_AppleInAppPurchaseReceipt_t3271698749_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0, &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Int32_t2071877448_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 3, GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType HashSet_1_t275936122_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t275936122_0_0_0_Types[] = { &HashSet_1_t275936122_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t275936122_0_0_0 = { 1, GenInst_HashSet_1_t275936122_0_0_0_Types };
extern const Il2CppType ProductDefinition_t1942475268_0_0_0;
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0 = { 1, GenInst_ProductDefinition_t1942475268_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType IStoreConfiguration_t2978822016_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_Types[] = { &Type_t_0_0_0, &IStoreConfiguration_t2978822016_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0 = { 2, GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IStoreConfiguration_t2978822016_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IStoreConfiguration_t2978822016_0_0_0_Types[] = { &IStoreConfiguration_t2978822016_0_0_0 };
extern const Il2CppGenericInst GenInst_IStoreConfiguration_t2978822016_0_0_0 = { 1, GenInst_IStoreConfiguration_t2978822016_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2673525135_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2673525135_0_0_0_Types[] = { &KeyValuePair_2_t2673525135_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2673525135_0_0_0 = { 1, GenInst_KeyValuePair_2_t2673525135_0_0_0_Types };
extern const Il2CppType IPurchasingModule_t4085676839_0_0_0;
static const Il2CppType* GenInst_IPurchasingModule_t4085676839_0_0_0_Types[] = { &IPurchasingModule_t4085676839_0_0_0 };
extern const Il2CppGenericInst GenInst_IPurchasingModule_t4085676839_0_0_0 = { 1, GenInst_IPurchasingModule_t4085676839_0_0_0_Types };
extern const Il2CppType Product_t1203687971_0_0_0;
static const Il2CppType* GenInst_Product_t1203687971_0_0_0_Types[] = { &Product_t1203687971_0_0_0 };
extern const Il2CppGenericInst GenInst_Product_t1203687971_0_0_0 = { 1, GenInst_Product_t1203687971_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_Types[] = { &String_t_0_0_0, &Product_t1203687971_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Product_t1203687971_0_0_0 = { 2, GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Product_t1203687971_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t875812455_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t875812455_0_0_0_Types[] = { &KeyValuePair_2_t875812455_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t875812455_0_0_0 = { 1, GenInst_KeyValuePair_2_t875812455_0_0_0_Types };
static const Il2CppType* GenInst_Product_t1203687971_0_0_0_String_t_0_0_0_Types[] = { &Product_t1203687971_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Product_t1203687971_0_0_0_String_t_0_0_0 = { 2, GenInst_Product_t1203687971_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType IStoreExtension_t1396898229_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_Types[] = { &Type_t_0_0_0, &IStoreExtension_t1396898229_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0 = { 2, GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IStoreExtension_t1396898229_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IStoreExtension_t1396898229_0_0_0_Types[] = { &IStoreExtension_t1396898229_0_0_0 };
extern const Il2CppGenericInst GenInst_IStoreExtension_t1396898229_0_0_0 = { 1, GenInst_IStoreExtension_t1396898229_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1091601348_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1091601348_0_0_0_Types[] = { &KeyValuePair_2_t1091601348_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1091601348_0_0_0 = { 1, GenInst_KeyValuePair_2_t1091601348_0_0_0_Types };
extern const Il2CppType InitializationFailureReason_t2954032642_0_0_0;
static const Il2CppType* GenInst_InitializationFailureReason_t2954032642_0_0_0_Types[] = { &InitializationFailureReason_t2954032642_0_0_0 };
extern const Il2CppGenericInst GenInst_InitializationFailureReason_t2954032642_0_0_0 = { 1, GenInst_InitializationFailureReason_t2954032642_0_0_0_Types };
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_Product_t1203687971_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0, &Product_t1203687971_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0_Product_t1203687971_0_0_0 = { 2, GenInst_ProductDefinition_t1942475268_0_0_0_Product_t1203687971_0_0_0_Types };
extern const Il2CppType ProductDescription_t3318267523_0_0_0;
static const Il2CppType* GenInst_ProductDescription_t3318267523_0_0_0_Types[] = { &ProductDescription_t3318267523_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDescription_t3318267523_0_0_0 = { 1, GenInst_ProductDescription_t3318267523_0_0_0_Types };
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { &BaseInputModule_t1295781545_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
extern const Il2CppType RaycastResult_t21186376_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { &IDeselectHandler_t3182198310_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { &IEventSystemHandler_t2741188318_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
extern const Il2CppType List_1_t2110309450_0_0_0;
static const Il2CppType* GenInst_List_1_t2110309450_0_0_0_Types[] = { &List_1_t2110309450_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
extern const Il2CppType List_1_t2058570427_0_0_0;
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_Types[] = { &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
extern const Il2CppType List_1_t3188497603_0_0_0;
static const Il2CppType* GenInst_List_1_t3188497603_0_0_0_Types[] = { &List_1_t3188497603_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { &ISelectHandler_t2812555161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { &BaseRaycaster_t2336171397_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
extern const Il2CppType Entry_t3365010046_0_0_0;
static const Il2CppType* GenInst_Entry_t3365010046_0_0_0_Types[] = { &Entry_t3365010046_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { &BaseEventData_t2681005625_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { &IPointerEnterHandler_t193164956_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { &IPointerExitHandler_t461019860_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { &IPointerDownHandler_t3929046918_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { &IPointerUpHandler_t1847764461_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { &IPointerClickHandler_t96169666_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { &IInitializePotentialDragHandler_t3350809087_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { &IBeginDragHandler_t3135127860_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { &IDragHandler_t2583993319_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { &IEndDragHandler_t1349123600_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { &IDropHandler_t2390101210_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { &IScrollHandler_t3834677510_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { &IUpdateSelectedHandler_t3778909353_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { &IMoveHandler_t2611925506_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { &ISubmitHandler_t525803901_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { &ICancelHandler_t1980319651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
extern const Il2CppType Transform_t3275118058_0_0_0;
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Types[] = { &Transform_t3275118058_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
extern const Il2CppType GameObject_t1756533147_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType BaseInput_t621514313_0_0_0;
static const Il2CppType* GenInst_BaseInput_t621514313_0_0_0_Types[] = { &BaseInput_t621514313_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInput_t621514313_0_0_0 = { 1, GenInst_BaseInput_t621514313_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { &UIBehaviour_t3960014691_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { &MonoBehaviour_t1158329972_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
extern const Il2CppType ButtonState_t2688375492_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { &ButtonState_t2688375492_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { &ColorBlock_t2652774230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
extern const Il2CppType OptionData_t2420267500_0_0_0;
static const Il2CppType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { &OptionData_t2420267500_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { &DropdownItem_t4139978805_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
extern const Il2CppType FloatTween_t2986189219_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { &FloatTween_t2986189219_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
extern const Il2CppType Sprite_t309593783_0_0_0;
static const Il2CppType* GenInst_Sprite_t309593783_0_0_0_Types[] = { &Sprite_t309593783_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
extern const Il2CppType Canvas_t209405766_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_Types[] = { &Canvas_t209405766_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
extern const Il2CppType List_1_t3873494194_0_0_0;
static const Il2CppType* GenInst_List_1_t3873494194_0_0_0_Types[] = { &List_1_t3873494194_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
extern const Il2CppType HashSet_1_t2984649583_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType Text_t356221433_0_0_0;
static const Il2CppType* GenInst_Text_t356221433_0_0_0_Types[] = { &Text_t356221433_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_HashSet_1_t2984649583_0_0_0_Types[] = { &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2984649583_0_0_0 = { 1, GenInst_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t850112849_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t850112849_0_0_0_Types[] = { &KeyValuePair_2_t850112849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t850112849_0_0_0 = { 1, GenInst_KeyValuePair_2_t850112849_0_0_0_Types };
extern const Il2CppType ColorTween_t3438117476_0_0_0;
static const Il2CppType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { &ColorTween_t3438117476_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
extern const Il2CppType Graphic_t2426225576_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t286373651_0_0_0_Types[] = { &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t286373651_0_0_0 = { 1, GenInst_IndexedSet_1_t286373651_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
extern const Il2CppType Type_t3352948571_0_0_0;
static const Il2CppType* GenInst_Type_t3352948571_0_0_0_Types[] = { &Type_t3352948571_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
extern const Il2CppType FillMethod_t1640962579_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { &FillMethod_t1640962579_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
extern const Il2CppType ContentType_t1028629049_0_0_0;
static const Il2CppType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { &ContentType_t1028629049_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
extern const Il2CppType LineType_t2931319356_0_0_0;
static const Il2CppType* GenInst_LineType_t2931319356_0_0_0_Types[] = { &LineType_t2931319356_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
extern const Il2CppType InputType_t1274231802_0_0_0;
static const Il2CppType* GenInst_InputType_t1274231802_0_0_0_Types[] = { &InputType_t1274231802_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { &TouchScreenKeyboardType_t875112366_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { &CharacterValidation_t3437478890_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
extern const Il2CppType Mask_t2977958238_0_0_0;
static const Il2CppType* GenInst_Mask_t2977958238_0_0_0_Types[] = { &Mask_t2977958238_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
extern const Il2CppType List_1_t2347079370_0_0_0;
static const Il2CppType* GenInst_List_1_t2347079370_0_0_0_Types[] = { &List_1_t2347079370_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { &RectMask2D_t1156185964_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
extern const Il2CppType List_1_t525307096_0_0_0;
static const Il2CppType* GenInst_List_1_t525307096_0_0_0_Types[] = { &List_1_t525307096_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
extern const Il2CppType Navigation_t1571958496_0_0_0;
static const Il2CppType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { &Navigation_t1571958496_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
extern const Il2CppType IClippable_t1941276057_0_0_0;
static const Il2CppType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { &IClippable_t1941276057_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
extern const Il2CppType Direction_t3696775921_0_0_0;
static const Il2CppType* GenInst_Direction_t3696775921_0_0_0_Types[] = { &Direction_t3696775921_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
extern const Il2CppType Selectable_t1490392188_0_0_0;
static const Il2CppType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { &Selectable_t1490392188_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
extern const Il2CppType Transition_t605142169_0_0_0;
static const Il2CppType* GenInst_Transition_t605142169_0_0_0_Types[] = { &Transition_t605142169_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
extern const Il2CppType SpriteState_t1353336012_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { &SpriteState_t1353336012_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { &CanvasGroup_t3296560743_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
extern const Il2CppType Direction_t1525323322_0_0_0;
static const Il2CppType* GenInst_Direction_t1525323322_0_0_0_Types[] = { &Direction_t1525323322_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
extern const Il2CppType MatEntry_t3157325053_0_0_0;
static const Il2CppType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { &MatEntry_t3157325053_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
extern const Il2CppType Toggle_t3976754468_0_0_0;
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IClipper_t900477982_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Types[] = { &IClipper_t900477982_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
extern const Il2CppType AspectMode_t1166448724_0_0_0;
static const Il2CppType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { &AspectMode_t1166448724_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
extern const Il2CppType FitMode_t4030874534_0_0_0;
static const Il2CppType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { &FitMode_t4030874534_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
extern const Il2CppType RectTransform_t3349966182_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { &RectTransform_t3349966182_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { &LayoutRebuilder_t2155218138_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType List_1_t1612828712_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828712_0_0_0_Types[] = { &List_1_t1612828712_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
extern const Il2CppType List_1_t243638650_0_0_0;
static const Il2CppType* GenInst_List_1_t243638650_0_0_0_Types[] = { &List_1_t243638650_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
extern const Il2CppType List_1_t1612828711_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828711_0_0_0_Types[] = { &List_1_t1612828711_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
extern const Il2CppType List_1_t1612828713_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828713_0_0_0_Types[] = { &List_1_t1612828713_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
extern const Il2CppType List_1_t1440998580_0_0_0;
static const Il2CppType* GenInst_List_1_t1440998580_0_0_0_Types[] = { &List_1_t1440998580_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
extern const Il2CppType List_1_t573379950_0_0_0;
static const Il2CppType* GenInst_List_1_t573379950_0_0_0_Types[] = { &List_1_t573379950_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
extern const Il2CppType IAsyncResult_t1999651008_0_0_0;
static const Il2CppType* GenInst_IAsyncResult_t1999651008_0_0_0_Types[] = { &IAsyncResult_t1999651008_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0 = { 1, GenInst_IAsyncResult_t1999651008_0_0_0_Types };
extern const Il2CppType WinProductDescription_t1075111405_0_0_0;
static const Il2CppType* GenInst_WinProductDescription_t1075111405_0_0_0_Types[] = { &WinProductDescription_t1075111405_0_0_0 };
extern const Il2CppGenericInst GenInst_WinProductDescription_t1075111405_0_0_0 = { 1, GenInst_WinProductDescription_t1075111405_0_0_0_Types };
extern const Il2CppType IPurchaseReceipt_t2402701844_0_0_0;
static const Il2CppType* GenInst_IPurchaseReceipt_t2402701844_0_0_0_Types[] = { &IPurchaseReceipt_t2402701844_0_0_0 };
extern const Il2CppGenericInst GenInst_IPurchaseReceipt_t2402701844_0_0_0 = { 1, GenInst_IPurchaseReceipt_t2402701844_0_0_0_Types };
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_ProductDefinition_t1942475268_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_WinProductDescription_t1075111405_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0, &WinProductDescription_t1075111405_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0_WinProductDescription_t1075111405_0_0_0 = { 2, GenInst_ProductDefinition_t1942475268_0_0_0_WinProductDescription_t1075111405_0_0_0_Types };
extern const Il2CppType LoginResultState_t2459016979_0_0_0;
static const Il2CppType* GenInst_LoginResultState_t2459016979_0_0_0_String_t_0_0_0_Types[] = { &LoginResultState_t2459016979_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginResultState_t2459016979_0_0_0_String_t_0_0_0 = { 2, GenInst_LoginResultState_t2459016979_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_LoginResultState_t2459016979_0_0_0_Il2CppObject_0_0_0_Types[] = { &LoginResultState_t2459016979_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginResultState_t2459016979_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LoginResultState_t2459016979_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FastRegisterError_t341731807_0_0_0;
static const Il2CppType* GenInst_FastRegisterError_t341731807_0_0_0_String_t_0_0_0_Types[] = { &FastRegisterError_t341731807_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FastRegisterError_t341731807_0_0_0_String_t_0_0_0 = { 2, GenInst_FastRegisterError_t341731807_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_FastRegisterError_t341731807_0_0_0_Il2CppObject_0_0_0_Types[] = { &FastRegisterError_t341731807_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_FastRegisterError_t341731807_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_FastRegisterError_t341731807_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType RestoreTransactionIDState_t2487303652_0_0_0;
static const Il2CppType* GenInst_RestoreTransactionIDState_t2487303652_0_0_0_Types[] = { &RestoreTransactionIDState_t2487303652_0_0_0 };
extern const Il2CppGenericInst GenInst_RestoreTransactionIDState_t2487303652_0_0_0 = { 1, GenInst_RestoreTransactionIDState_t2487303652_0_0_0_Types };
extern const Il2CppType ValidateReceiptState_t4359597_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ValidateReceiptState_t4359597_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &ValidateReceiptState_t4359597_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ValidateReceiptState_t4359597_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_ValidateReceiptState_t4359597_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ValidateReceiptState_t4359597_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ValidateReceiptState_t4359597_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ValidateReceiptState_t4359597_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ValidateReceiptState_t4359597_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType RequestPayOutState_t3537434082_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_RequestPayOutState_t3537434082_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &RequestPayOutState_t3537434082_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RequestPayOutState_t3537434082_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_RequestPayOutState_t3537434082_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_RequestPayOutState_t3537434082_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &RequestPayOutState_t3537434082_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_RequestPayOutState_t3537434082_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_RequestPayOutState_t3537434082_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType PurchaseFailureReason_t1322959839_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &PurchaseFailureReason_t1322959839_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PurchaseFailureReason_t1322959839_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ProductCatalogItem_t977711995_0_0_0;
static const Il2CppType* GenInst_ProductCatalogItem_t977711995_0_0_0_Types[] = { &ProductCatalogItem_t977711995_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductCatalogItem_t977711995_0_0_0 = { 1, GenInst_ProductCatalogItem_t977711995_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_InitializationFailureReason_t2954032642_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &InitializationFailureReason_t2954032642_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_InitializationFailureReason_t2954032642_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_InitializationFailureReason_t2954032642_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &PurchaseFailureReason_t1322959839_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_PurchaseFailureReason_t1322959839_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Types };
extern const Il2CppType StoreID_t471452324_0_0_0;
static const Il2CppType* GenInst_StoreID_t471452324_0_0_0_Types[] = { &StoreID_t471452324_0_0_0 };
extern const Il2CppGenericInst GenInst_StoreID_t471452324_0_0_0 = { 1, GenInst_StoreID_t471452324_0_0_0_Types };
extern const Il2CppType LocalizedProductDescription_t1525635964_0_0_0;
static const Il2CppType* GenInst_LocalizedProductDescription_t1525635964_0_0_0_Types[] = { &LocalizedProductDescription_t1525635964_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizedProductDescription_t1525635964_0_0_0 = { 1, GenInst_LocalizedProductDescription_t1525635964_0_0_0_Types };
extern const Il2CppType AndroidStore_t3203055206_0_0_0;
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0 = { 2, GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t566713506_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t566713506_0_0_0_Types[] = { &KeyValuePair_2_t566713506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t566713506_0_0_0 = { 1, GenInst_KeyValuePair_2_t566713506_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0 = { 1, GenInst_AndroidStore_t3203055206_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_AndroidStore_t3203055206_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0, &AndroidStore_t3203055206_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_AndroidStore_t3203055206_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_AndroidStore_t3203055206_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t566713506_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t566713506_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t566713506_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t566713506_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4201451740_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4201451740_0_0_0_Types[] = { &KeyValuePair_2_t4201451740_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4201451740_0_0_0 = { 1, GenInst_KeyValuePair_2_t4201451740_0_0_0_Types };
static const Il2CppType* GenInst_ProductDefinition_t1942475268_0_0_0_String_t_0_0_0_Types[] = { &ProductDefinition_t1942475268_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1942475268_0_0_0_String_t_0_0_0 = { 2, GenInst_ProductDefinition_t1942475268_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Action_t3226471752_0_0_0;
static const Il2CppType* GenInst_Action_t3226471752_0_0_0_Types[] = { &Action_t3226471752_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0 = { 1, GenInst_Action_t3226471752_0_0_0_Types };
extern const Il2CppType RuntimePlatform_t1869584967_0_0_0;
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0 = { 1, GenInst_RuntimePlatform_t1869584967_0_0_0_Types };
extern const Il2CppType Action_1_t3627374100_0_0_0;
static const Il2CppType* GenInst_Action_1_t3627374100_0_0_0_Types[] = { &Action_1_t3627374100_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3627374100_0_0_0 = { 1, GenInst_Action_1_t3627374100_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3531071141_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Dictionary_2_t3531071141_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Dictionary_2_t3531071141_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Dictionary_2_t3531071141_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Dictionary_2_t3531071141_0_0_0_Types };
extern const Il2CppType IGeneratedExtensionLite_t1616291879_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_Types[] = { &String_t_0_0_0, &IGeneratedExtensionLite_t1616291879_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0 = { 2, GenInst_String_t_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &IGeneratedExtensionLite_t1616291879_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IGeneratedExtensionLite_t1616291879_0_0_0_Types[] = { &IGeneratedExtensionLite_t1616291879_0_0_0 };
extern const Il2CppGenericInst GenInst_IGeneratedExtensionLite_t1616291879_0_0_0 = { 1, GenInst_IGeneratedExtensionLite_t1616291879_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1288416363_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1288416363_0_0_0_Types[] = { &KeyValuePair_2_t1288416363_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1288416363_0_0_0 = { 1, GenInst_KeyValuePair_2_t1288416363_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Dictionary_2_t3531071141_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Dictionary_2_t3531071141_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Dictionary_2_t3531071141_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Dictionary_2_t3531071141_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3531071141_0_0_0_Types[] = { &Dictionary_2_t3531071141_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3531071141_0_0_0 = { 1, GenInst_Dictionary_2_t3531071141_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t880476491_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t880476491_0_0_0_Types[] = { &KeyValuePair_2_t880476491_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t880476491_0_0_0 = { 1, GenInst_KeyValuePair_2_t880476491_0_0_0_Types };
extern const Il2CppType ExtensionIntPair_t3093161221_0_0_0;
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0, &IGeneratedExtensionLite_t1616291879_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0 = { 2, GenInst_ExtensionIntPair_t3093161221_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_Types };
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2164262055_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2164262055_0_0_0_Types[] = { &KeyValuePair_2_t2164262055_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2164262055_0_0_0 = { 1, GenInst_KeyValuePair_2_t2164262055_0_0_0_Types };
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0 = { 1, GenInst_ExtensionIntPair_t3093161221_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1002176094_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1002176094_0_0_0_Types[] = { &IEquatable_1_t1002176094_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1002176094_0_0_0 = { 1, GenInst_IEquatable_1_t1002176094_0_0_0_Types };
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_ExtensionIntPair_t3093161221_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0, &Il2CppObject_0_0_0, &ExtensionIntPair_t3093161221_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_ExtensionIntPair_t3093161221_0_0_0 = { 3, GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_ExtensionIntPair_t3093161221_0_0_0_Types };
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2164262055_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2164262055_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2164262055_0_0_0 = { 3, GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2164262055_0_0_0_Types };
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0, &IGeneratedExtensionLite_t1616291879_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ExtensionIntPair_t3093161221_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1091104639_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1091104639_0_0_0_Types[] = { &KeyValuePair_2_t1091104639_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1091104639_0_0_0 = { 1, GenInst_KeyValuePair_2_t1091104639_0_0_0_Types };
extern const Il2CppType ByteString_t3153909979_0_0_0;
static const Il2CppType* GenInst_ByteString_t3153909979_0_0_0_Types[] = { &ByteString_t3153909979_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteString_t3153909979_0_0_0 = { 1, GenInst_ByteString_t3153909979_0_0_0_Types };
extern const Il2CppType FieldWithTarget_t2256174789_0_0_0;
static const Il2CppType* GenInst_FieldWithTarget_t2256174789_0_0_0_Types[] = { &FieldWithTarget_t2256174789_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldWithTarget_t2256174789_0_0_0 = { 1, GenInst_FieldWithTarget_t2256174789_0_0_0_Types };
extern const Il2CppType FieldWithRemoteSettingsKey_t2620356393_0_0_0;
static const Il2CppType* GenInst_FieldWithRemoteSettingsKey_t2620356393_0_0_0_Types[] = { &FieldWithRemoteSettingsKey_t2620356393_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldWithRemoteSettingsKey_t2620356393_0_0_0 = { 1, GenInst_FieldWithRemoteSettingsKey_t2620356393_0_0_0_Types };
static const Il2CppType* GenInst_Product_t1203687971_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Types[] = { &Product_t1203687971_0_0_0, &PurchaseFailureReason_t1322959839_0_0_0 };
extern const Il2CppGenericInst GenInst_Product_t1203687971_0_0_0_PurchaseFailureReason_t1322959839_0_0_0 = { 2, GenInst_Product_t1203687971_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PurchaseFailureReason_t1322959839_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PurchaseFailureReason_t1322959839_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Types };
extern const Il2CppType IAPButton_t3077837360_0_0_0;
static const Il2CppType* GenInst_IAPButton_t3077837360_0_0_0_Types[] = { &IAPButton_t3077837360_0_0_0 };
extern const Il2CppGenericInst GenInst_IAPButton_t3077837360_0_0_0 = { 1, GenInst_IAPButton_t3077837360_0_0_0_Types };
extern const Il2CppType Image_t2042527209_0_0_0;
static const Il2CppType* GenInst_Image_t2042527209_0_0_0_Types[] = { &Image_t2042527209_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
extern const Il2CppType ISerializationCallbackReceiver_t1665913161_0_0_0;
static const Il2CppType* GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0_Types[] = { &ISerializationCallbackReceiver_t1665913161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0 = { 1, GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0_Types };
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0 = { 1, GenInst_ILayoutElement_t1975293769_0_0_0_Types };
extern const Il2CppType ICanvasRaycastFilter_t1367822892_0_0_0;
static const Il2CppType* GenInst_ICanvasRaycastFilter_t1367822892_0_0_0_Types[] = { &ICanvasRaycastFilter_t1367822892_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t1367822892_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t1367822892_0_0_0_Types };
extern const Il2CppType MaskableGraphic_t540192618_0_0_0;
static const Il2CppType* GenInst_MaskableGraphic_t540192618_0_0_0_Types[] = { &MaskableGraphic_t540192618_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t540192618_0_0_0 = { 1, GenInst_MaskableGraphic_t540192618_0_0_0_Types };
extern const Il2CppType IMaskable_t1431842707_0_0_0;
static const Il2CppType* GenInst_IMaskable_t1431842707_0_0_0_Types[] = { &IMaskable_t1431842707_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaskable_t1431842707_0_0_0 = { 1, GenInst_IMaskable_t1431842707_0_0_0_Types };
extern const Il2CppType IMaterialModifier_t3028564983_0_0_0;
static const Il2CppType* GenInst_IMaterialModifier_t3028564983_0_0_0_Types[] = { &IMaterialModifier_t3028564983_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t3028564983_0_0_0 = { 1, GenInst_IMaterialModifier_t3028564983_0_0_0_Types };
extern const Il2CppType LearningData_t1664811342_0_0_0;
static const Il2CppType* GenInst_LearningData_t1664811342_0_0_0_Types[] = { &LearningData_t1664811342_0_0_0 };
extern const Il2CppGenericInst GenInst_LearningData_t1664811342_0_0_0 = { 1, GenInst_LearningData_t1664811342_0_0_0_Types };
extern const Il2CppType RegisterResponse_t410466074_0_0_0;
static const Il2CppType* GenInst_RegisterResponse_t410466074_0_0_0_Types[] = { &RegisterResponse_t410466074_0_0_0 };
extern const Il2CppGenericInst GenInst_RegisterResponse_t410466074_0_0_0 = { 1, GenInst_RegisterResponse_t410466074_0_0_0_Types };
extern const Il2CppType ChangeDeviceResponse_t284910379_0_0_0;
static const Il2CppType* GenInst_ChangeDeviceResponse_t284910379_0_0_0_Types[] = { &ChangeDeviceResponse_t284910379_0_0_0 };
extern const Il2CppGenericInst GenInst_ChangeDeviceResponse_t284910379_0_0_0 = { 1, GenInst_ChangeDeviceResponse_t284910379_0_0_0_Types };
extern const Il2CppType LearningLogResponse_t112900141_0_0_0;
static const Il2CppType* GenInst_LearningLogResponse_t112900141_0_0_0_Types[] = { &LearningLogResponse_t112900141_0_0_0 };
extern const Il2CppGenericInst GenInst_LearningLogResponse_t112900141_0_0_0 = { 1, GenInst_LearningLogResponse_t112900141_0_0_0_Types };
extern const Il2CppType LearningLogData_t2500481692_0_0_0;
static const Il2CppType* GenInst_LearningLogData_t2500481692_0_0_0_Types[] = { &LearningLogData_t2500481692_0_0_0 };
extern const Il2CppGenericInst GenInst_LearningLogData_t2500481692_0_0_0 = { 1, GenInst_LearningLogData_t2500481692_0_0_0_Types };
extern const Il2CppType LearningChapterLogData_t1224380751_0_0_0;
static const Il2CppType* GenInst_LearningChapterLogData_t1224380751_0_0_0_Types[] = { &LearningChapterLogData_t1224380751_0_0_0 };
extern const Il2CppGenericInst GenInst_LearningChapterLogData_t1224380751_0_0_0 = { 1, GenInst_LearningChapterLogData_t1224380751_0_0_0_Types };
extern const Il2CppType AssetBundle_t2054978754_0_0_0;
static const Il2CppType* GenInst_AssetBundle_t2054978754_0_0_0_Types[] = { &AssetBundle_t2054978754_0_0_0 };
extern const Il2CppGenericInst GenInst_AssetBundle_t2054978754_0_0_0 = { 1, GenInst_AssetBundle_t2054978754_0_0_0_Types };
extern const Il2CppType AssetBundleLoader_t639004779_0_0_0;
static const Il2CppType* GenInst_AssetBundleLoader_t639004779_0_0_0_Types[] = { &AssetBundleLoader_t639004779_0_0_0 };
extern const Il2CppGenericInst GenInst_AssetBundleLoader_t639004779_0_0_0 = { 1, GenInst_AssetBundleLoader_t639004779_0_0_0_Types };
extern const Il2CppType Band_t1120454049_0_0_0;
static const Il2CppType* GenInst_Band_t1120454049_0_0_0_Types[] = { &Band_t1120454049_0_0_0 };
extern const Il2CppGenericInst GenInst_Band_t1120454049_0_0_0 = { 1, GenInst_Band_t1120454049_0_0_0_Types };
extern const Il2CppType Button_t2872111280_0_0_0;
static const Il2CppType* GenInst_Button_t2872111280_0_0_0_Types[] = { &Button_t2872111280_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
extern const Il2CppType PostureCharacter_t580023905_0_0_0;
static const Il2CppType* GenInst_PostureCharacter_t580023905_0_0_0_Types[] = { &PostureCharacter_t580023905_0_0_0 };
extern const Il2CppGenericInst GenInst_PostureCharacter_t580023905_0_0_0 = { 1, GenInst_PostureCharacter_t580023905_0_0_0_Types };
extern const Il2CppType StateEmotionCharacter_t3036603695_0_0_0;
static const Il2CppType* GenInst_StateEmotionCharacter_t3036603695_0_0_0_Types[] = { &StateEmotionCharacter_t3036603695_0_0_0 };
extern const Il2CppGenericInst GenInst_StateEmotionCharacter_t3036603695_0_0_0 = { 1, GenInst_StateEmotionCharacter_t3036603695_0_0_0_Types };
extern const Il2CppType SelectButton_t132497280_0_0_0;
static const Il2CppType* GenInst_SelectButton_t132497280_0_0_0_Types[] = { &SelectButton_t132497280_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectButton_t132497280_0_0_0 = { 1, GenInst_SelectButton_t132497280_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t1021602117_0_0_0_Types[] = { &String_t_0_0_0, &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t1021602117_0_0_0 = { 2, GenInst_String_t_0_0_0_Object_t1021602117_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t1021602117_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Object_t1021602117_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t1021602117_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Object_t1021602117_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t693726601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t693726601_0_0_0_Types[] = { &KeyValuePair_2_t693726601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t693726601_0_0_0 = { 1, GenInst_KeyValuePair_2_t693726601_0_0_0_Types };
extern const Il2CppType ChapterTopSetting_t2509872352_0_0_0;
static const Il2CppType* GenInst_ChapterTopSetting_t2509872352_0_0_0_Types[] = { &ChapterTopSetting_t2509872352_0_0_0 };
extern const Il2CppGenericInst GenInst_ChapterTopSetting_t2509872352_0_0_0 = { 1, GenInst_ChapterTopSetting_t2509872352_0_0_0_Types };
extern const Il2CppType ConversationState_t3360496668_0_0_0;
static const Il2CppType* GenInst_ConversationState_t3360496668_0_0_0_Types[] = { &ConversationState_t3360496668_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationState_t3360496668_0_0_0 = { 1, GenInst_ConversationState_t3360496668_0_0_0_Types };
extern const Il2CppType Cell_t3051913968_0_0_0;
static const Il2CppType* GenInst_Cell_t3051913968_0_0_0_Types[] = { &Cell_t3051913968_0_0_0 };
extern const Il2CppGenericInst GenInst_Cell_t3051913968_0_0_0 = { 1, GenInst_Cell_t3051913968_0_0_0_Types };
extern const Il2CppType IComparable_1_t588785895_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t588785895_0_0_0_Types[] = { &IComparable_1_t588785895_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t588785895_0_0_0 = { 1, GenInst_IComparable_1_t588785895_0_0_0_Types };
extern const Il2CppType LessonValue_t1076530945_0_0_0;
static const Il2CppType* GenInst_LessonValue_t1076530945_0_0_0_Types[] = { &LessonValue_t1076530945_0_0_0 };
extern const Il2CppGenericInst GenInst_LessonValue_t1076530945_0_0_0 = { 1, GenInst_LessonValue_t1076530945_0_0_0_Types };
extern const Il2CppType GraphValue_t2739901601_0_0_0;
static const Il2CppType* GenInst_GraphValue_t2739901601_0_0_0_Types[] = { &GraphValue_t2739901601_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphValue_t2739901601_0_0_0 = { 1, GenInst_GraphValue_t2739901601_0_0_0_Types };
extern const Il2CppType ViewTextUI_t4118675828_0_0_0;
static const Il2CppType* GenInst_ViewTextUI_t4118675828_0_0_0_Types[] = { &ViewTextUI_t4118675828_0_0_0 };
extern const Il2CppGenericInst GenInst_ViewTextUI_t4118675828_0_0_0 = { 1, GenInst_ViewTextUI_t4118675828_0_0_0_Types };
extern const Il2CppType MyPageChapterController_t2166380602_0_0_0;
static const Il2CppType* GenInst_MyPageChapterController_t2166380602_0_0_0_Types[] = { &MyPageChapterController_t2166380602_0_0_0 };
extern const Il2CppGenericInst GenInst_MyPageChapterController_t2166380602_0_0_0 = { 1, GenInst_MyPageChapterController_t2166380602_0_0_0_Types };
extern const Il2CppType Column_t1930583302_0_0_0;
static const Il2CppType* GenInst_Column_t1930583302_0_0_0_Types[] = { &Column_t1930583302_0_0_0 };
extern const Il2CppGenericInst GenInst_Column_t1930583302_0_0_0 = { 1, GenInst_Column_t1930583302_0_0_0_Types };
extern const Il2CppType ChapterMyPageScript_t3593529611_0_0_0;
static const Il2CppType* GenInst_ChapterMyPageScript_t3593529611_0_0_0_Types[] = { &ChapterMyPageScript_t3593529611_0_0_0 };
extern const Il2CppGenericInst GenInst_ChapterMyPageScript_t3593529611_0_0_0 = { 1, GenInst_ChapterMyPageScript_t3593529611_0_0_0_Types };
extern const Il2CppType ConversationSelectionData_t4090008535_0_0_0;
static const Il2CppType* GenInst_ConversationSelectionData_t4090008535_0_0_0_Types[] = { &ConversationSelectionData_t4090008535_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationSelectionData_t4090008535_0_0_0 = { 1, GenInst_ConversationSelectionData_t4090008535_0_0_0_Types };
extern const Il2CppType Param_t4123818474_0_0_0;
static const Il2CppType* GenInst_Param_t4123818474_0_0_0_Types[] = { &Param_t4123818474_0_0_0 };
extern const Il2CppGenericInst GenInst_Param_t4123818474_0_0_0 = { 1, GenInst_Param_t4123818474_0_0_0_Types };
extern const Il2CppType AudioClip_t1932558630_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_AudioClip_t1932558630_0_0_0_Types[] = { &String_t_0_0_0, &AudioClip_t1932558630_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AudioClip_t1932558630_0_0_0 = { 2, GenInst_String_t_0_0_0_AudioClip_t1932558630_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &AudioClip_t1932558630_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AudioClip_t1932558630_0_0_0_Types[] = { &AudioClip_t1932558630_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioClip_t1932558630_0_0_0 = { 1, GenInst_AudioClip_t1932558630_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1604683114_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1604683114_0_0_0_Types[] = { &KeyValuePair_2_t1604683114_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1604683114_0_0_0 = { 1, GenInst_KeyValuePair_2_t1604683114_0_0_0_Types };
extern const Il2CppType ConversationTalkData_t1570298305_0_0_0;
static const Il2CppType* GenInst_ConversationTalkData_t1570298305_0_0_0_Types[] = { &ConversationTalkData_t1570298305_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationTalkData_t1570298305_0_0_0 = { 1, GenInst_ConversationTalkData_t1570298305_0_0_0_Types };
extern const Il2CppType IConversationModule_t2617440232_0_0_0;
static const Il2CppType* GenInst_IConversationModule_t2617440232_0_0_0_Types[] = { &IConversationModule_t2617440232_0_0_0 };
extern const Il2CppGenericInst GenInst_IConversationModule_t2617440232_0_0_0 = { 1, GenInst_IConversationModule_t2617440232_0_0_0_Types };
extern const Il2CppType ConversationCommand_t3660105836_0_0_0;
static const Il2CppType* GenInst_ConversationCommand_t3660105836_0_0_0_Types[] = { &ConversationCommand_t3660105836_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationCommand_t3660105836_0_0_0 = { 1, GenInst_ConversationCommand_t3660105836_0_0_0_Types };
extern const Il2CppType ISubtextItem_t4284961295_0_0_0;
static const Il2CppType* GenInst_ISubtextItem_t4284961295_0_0_0_Types[] = { &ISubtextItem_t4284961295_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubtextItem_t4284961295_0_0_0 = { 1, GenInst_ISubtextItem_t4284961295_0_0_0_Types };
extern const Il2CppType CharacterSelectionView_t2239851166_0_0_0;
static const Il2CppType* GenInst_CharacterSelectionView_t2239851166_0_0_0_Types[] = { &CharacterSelectionView_t2239851166_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterSelectionView_t2239851166_0_0_0 = { 1, GenInst_CharacterSelectionView_t2239851166_0_0_0_Types };
extern const Il2CppType ConversationSceneSelectionView_t407867144_0_0_0;
static const Il2CppType* GenInst_ConversationSceneSelectionView_t407867144_0_0_0_Types[] = { &ConversationSceneSelectionView_t407867144_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationSceneSelectionView_t407867144_0_0_0 = { 1, GenInst_ConversationSceneSelectionView_t407867144_0_0_0_Types };
extern const Il2CppType ExcerciseSceneSelectionView_t3932267486_0_0_0;
static const Il2CppType* GenInst_ExcerciseSceneSelectionView_t3932267486_0_0_0_Types[] = { &ExcerciseSceneSelectionView_t3932267486_0_0_0 };
extern const Il2CppGenericInst GenInst_ExcerciseSceneSelectionView_t3932267486_0_0_0 = { 1, GenInst_ExcerciseSceneSelectionView_t3932267486_0_0_0_Types };
extern const Il2CppType ConversationHistoryItemView_t3846761105_0_0_0;
static const Il2CppType* GenInst_ConversationHistoryItemView_t3846761105_0_0_0_Types[] = { &ConversationHistoryItemView_t3846761105_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationHistoryItemView_t3846761105_0_0_0 = { 1, GenInst_ConversationHistoryItemView_t3846761105_0_0_0_Types };
extern const Il2CppType ChapterItem_t2155291334_0_0_0;
static const Il2CppType* GenInst_ChapterItem_t2155291334_0_0_0_Types[] = { &ChapterItem_t2155291334_0_0_0 };
extern const Il2CppGenericInst GenInst_ChapterItem_t2155291334_0_0_0 = { 1, GenInst_ChapterItem_t2155291334_0_0_0_Types };
extern const Il2CppType CachedDragInfo_t1136705792_0_0_0;
static const Il2CppType* GenInst_CachedDragInfo_t1136705792_0_0_0_Types[] = { &CachedDragInfo_t1136705792_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedDragInfo_t1136705792_0_0_0 = { 1, GenInst_CachedDragInfo_t1136705792_0_0_0_Types };
extern const Il2CppType EditUsernameResponse_t1388559799_0_0_0;
static const Il2CppType* GenInst_EditUsernameResponse_t1388559799_0_0_0_Types[] = { &EditUsernameResponse_t1388559799_0_0_0 };
extern const Il2CppGenericInst GenInst_EditUsernameResponse_t1388559799_0_0_0 = { 1, GenInst_EditUsernameResponse_t1388559799_0_0_0_Types };
extern const Il2CppType UserInfoResponse_t693113528_0_0_0;
static const Il2CppType* GenInst_UserInfoResponse_t693113528_0_0_0_Types[] = { &UserInfoResponse_t693113528_0_0_0 };
extern const Il2CppGenericInst GenInst_UserInfoResponse_t693113528_0_0_0 = { 1, GenInst_UserInfoResponse_t693113528_0_0_0_Types };
extern const Il2CppType TransferCodeResponse_t1278852611_0_0_0;
static const Il2CppType* GenInst_TransferCodeResponse_t1278852611_0_0_0_Types[] = { &TransferCodeResponse_t1278852611_0_0_0 };
extern const Il2CppGenericInst GenInst_TransferCodeResponse_t1278852611_0_0_0 = { 1, GenInst_TransferCodeResponse_t1278852611_0_0_0_Types };
extern const Il2CppType ResetDataResponse_t2526516544_0_0_0;
static const Il2CppType* GenInst_ResetDataResponse_t2526516544_0_0_0_Types[] = { &ResetDataResponse_t2526516544_0_0_0 };
extern const Il2CppGenericInst GenInst_ResetDataResponse_t2526516544_0_0_0 = { 1, GenInst_ResetDataResponse_t2526516544_0_0_0_Types };
extern const Il2CppType PurchaseSession_t2088054391_0_0_0;
static const Il2CppType* GenInst_PurchaseSession_t2088054391_0_0_0_Types[] = { &PurchaseSession_t2088054391_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchaseSession_t2088054391_0_0_0 = { 1, GenInst_PurchaseSession_t2088054391_0_0_0_Types };
extern const Il2CppType Listening1QuestionData_t3381069024_0_0_0;
static const Il2CppType* GenInst_Listening1QuestionData_t3381069024_0_0_0_Types[] = { &Listening1QuestionData_t3381069024_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening1QuestionData_t3381069024_0_0_0 = { 1, GenInst_Listening1QuestionData_t3381069024_0_0_0_Types };
extern const Il2CppType TrueFalseAnswerSelection_t3406544577_0_0_0;
static const Il2CppType* GenInst_TrueFalseAnswerSelection_t3406544577_0_0_0_Types[] = { &TrueFalseAnswerSelection_t3406544577_0_0_0 };
extern const Il2CppGenericInst GenInst_TrueFalseAnswerSelection_t3406544577_0_0_0 = { 1, GenInst_TrueFalseAnswerSelection_t3406544577_0_0_0_Types };
static const Il2CppType* GenInst_TrueFalseAnswerSelection_t3406544577_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &TrueFalseAnswerSelection_t3406544577_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_TrueFalseAnswerSelection_t3406544577_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_TrueFalseAnswerSelection_t3406544577_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType SelectableItem_t2941161729_0_0_0;
static const Il2CppType* GenInst_SelectableItem_t2941161729_0_0_0_Types[] = { &SelectableItem_t2941161729_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectableItem_t2941161729_0_0_0 = { 1, GenInst_SelectableItem_t2941161729_0_0_0_Types };
extern const Il2CppType Listening1OnplayQuestion_t3534871625_0_0_0;
static const Il2CppType* GenInst_Listening1OnplayQuestion_t3534871625_0_0_0_Types[] = { &Listening1OnplayQuestion_t3534871625_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening1OnplayQuestion_t3534871625_0_0_0 = { 1, GenInst_Listening1OnplayQuestion_t3534871625_0_0_0_Types };
extern const Il2CppType Listening2QuestionData_t2443975119_0_0_0;
static const Il2CppType* GenInst_Listening2QuestionData_t2443975119_0_0_0_Types[] = { &Listening2QuestionData_t2443975119_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening2QuestionData_t2443975119_0_0_0 = { 1, GenInst_Listening2QuestionData_t2443975119_0_0_0_Types };
extern const Il2CppType Listening2SelectableItem_t492261174_0_0_0;
static const Il2CppType* GenInst_Listening2SelectableItem_t492261174_0_0_0_Types[] = { &Listening2SelectableItem_t492261174_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening2SelectableItem_t492261174_0_0_0 = { 1, GenInst_Listening2SelectableItem_t492261174_0_0_0_Types };
extern const Il2CppType Listening2OnplayQuestion_t3701806074_0_0_0;
static const Il2CppType* GenInst_Listening2OnplayQuestion_t3701806074_0_0_0_Types[] = { &Listening2OnplayQuestion_t3701806074_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening2OnplayQuestion_t3701806074_0_0_0 = { 1, GenInst_Listening2OnplayQuestion_t3701806074_0_0_0_Types };
extern const Il2CppType Dialog_t1378192732_0_0_0;
static const Il2CppType* GenInst_Dialog_t1378192732_0_0_0_Types[] = { &Dialog_t1378192732_0_0_0 };
extern const Il2CppGenericInst GenInst_Dialog_t1378192732_0_0_0 = { 1, GenInst_Dialog_t1378192732_0_0_0_Types };
extern const Il2CppType DisableWithTimeAudioName_t4015415354_0_0_0;
static const Il2CppType* GenInst_DisableWithTimeAudioName_t4015415354_0_0_0_Types[] = { &DisableWithTimeAudioName_t4015415354_0_0_0 };
extern const Il2CppGenericInst GenInst_DisableWithTimeAudioName_t4015415354_0_0_0 = { 1, GenInst_DisableWithTimeAudioName_t4015415354_0_0_0_Types };
extern const Il2CppType DialogPanel_t1568014038_0_0_0;
static const Il2CppType* GenInst_DialogPanel_t1568014038_0_0_0_Types[] = { &DialogPanel_t1568014038_0_0_0 };
extern const Il2CppGenericInst GenInst_DialogPanel_t1568014038_0_0_0 = { 1, GenInst_DialogPanel_t1568014038_0_0_0_Types };
extern const Il2CppType ResultSaveResponse_t3438979681_0_0_0;
static const Il2CppType* GenInst_ResultSaveResponse_t3438979681_0_0_0_Types[] = { &ResultSaveResponse_t3438979681_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultSaveResponse_t3438979681_0_0_0 = { 1, GenInst_ResultSaveResponse_t3438979681_0_0_0_Types };
extern const Il2CppType ZipSoundItem_t558808959_0_0_0;
static const Il2CppType* GenInst_ZipSoundItem_t558808959_0_0_0_Types[] = { &ZipSoundItem_t558808959_0_0_0 };
extern const Il2CppGenericInst GenInst_ZipSoundItem_t558808959_0_0_0 = { 1, GenInst_ZipSoundItem_t558808959_0_0_0_Types };
extern const Il2CppType PushZipResponse_t1905049400_0_0_0;
static const Il2CppType* GenInst_PushZipResponse_t1905049400_0_0_0_Types[] = { &PushZipResponse_t1905049400_0_0_0 };
extern const Il2CppGenericInst GenInst_PushZipResponse_t1905049400_0_0_0 = { 1, GenInst_PushZipResponse_t1905049400_0_0_0_Types };
extern const Il2CppType SentenceStructureIdiomQuestionData_t3251732102_0_0_0;
static const Il2CppType* GenInst_SentenceStructureIdiomQuestionData_t3251732102_0_0_0_Types[] = { &SentenceStructureIdiomQuestionData_t3251732102_0_0_0 };
extern const Il2CppGenericInst GenInst_SentenceStructureIdiomQuestionData_t3251732102_0_0_0 = { 1, GenInst_SentenceStructureIdiomQuestionData_t3251732102_0_0_0_Types };
extern const Il2CppType SentenceStructureIdiomOnPlayQuestion_t936886987_0_0_0;
static const Il2CppType* GenInst_SentenceStructureIdiomOnPlayQuestion_t936886987_0_0_0_Types[] = { &SentenceStructureIdiomOnPlayQuestion_t936886987_0_0_0 };
extern const Il2CppGenericInst GenInst_SentenceStructureIdiomOnPlayQuestion_t936886987_0_0_0 = { 1, GenInst_SentenceStructureIdiomOnPlayQuestion_t936886987_0_0_0_Types };
extern const Il2CppType SentenceListeningSpellingQuestionData_t3011907248_0_0_0;
static const Il2CppType* GenInst_SentenceListeningSpellingQuestionData_t3011907248_0_0_0_Types[] = { &SentenceListeningSpellingQuestionData_t3011907248_0_0_0 };
extern const Il2CppGenericInst GenInst_SentenceListeningSpellingQuestionData_t3011907248_0_0_0 = { 1, GenInst_SentenceListeningSpellingQuestionData_t3011907248_0_0_0_Types };
extern const Il2CppType SentenceListeningSpellingOnplayQuestion_t2514545327_0_0_0;
static const Il2CppType* GenInst_SentenceListeningSpellingOnplayQuestion_t2514545327_0_0_0_Types[] = { &SentenceListeningSpellingOnplayQuestion_t2514545327_0_0_0 };
extern const Il2CppGenericInst GenInst_SentenceListeningSpellingOnplayQuestion_t2514545327_0_0_0 = { 1, GenInst_SentenceListeningSpellingOnplayQuestion_t2514545327_0_0_0_Types };
extern const Il2CppType AudioSource_t1135106623_0_0_0;
static const Il2CppType* GenInst_AudioSource_t1135106623_0_0_0_Types[] = { &AudioSource_t1135106623_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t1135106623_0_0_0 = { 1, GenInst_AudioSource_t1135106623_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1428657631_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1428657631_0_0_0_Types[] = { &KeyValuePair_2_t1428657631_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1428657631_0_0_0 = { 1, GenInst_KeyValuePair_2_t1428657631_0_0_0_Types };
static const Il2CppType* GenInst_Sprite_t309593783_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Sprite_t309593783_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Sprite_t309593783_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType OpenHelpScript_t3207620564_0_0_0;
static const Il2CppType* GenInst_OpenHelpScript_t3207620564_0_0_0_Types[] = { &OpenHelpScript_t3207620564_0_0_0 };
extern const Il2CppGenericInst GenInst_OpenHelpScript_t3207620564_0_0_0 = { 1, GenInst_OpenHelpScript_t3207620564_0_0_0_Types };
extern const Il2CppType ManageATutorial_t1512139496_0_0_0;
static const Il2CppType* GenInst_ManageATutorial_t1512139496_0_0_0_Types[] = { &ManageATutorial_t1512139496_0_0_0 };
extern const Il2CppGenericInst GenInst_ManageATutorial_t1512139496_0_0_0 = { 1, GenInst_ManageATutorial_t1512139496_0_0_0_Types };
extern const Il2CppType TrackIndexObject_t1096184091_0_0_0;
static const Il2CppType* GenInst_TrackIndexObject_t1096184091_0_0_0_Types[] = { &TrackIndexObject_t1096184091_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackIndexObject_t1096184091_0_0_0 = { 1, GenInst_TrackIndexObject_t1096184091_0_0_0_Types };
extern const Il2CppType SelectionItem_t610427083_0_0_0;
static const Il2CppType* GenInst_SelectionItem_t610427083_0_0_0_Types[] = { &SelectionItem_t610427083_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectionItem_t610427083_0_0_0 = { 1, GenInst_SelectionItem_t610427083_0_0_0_Types };
extern const Il2CppType SelectItem_t2844432199_0_0_0;
static const Il2CppType* GenInst_SelectItem_t2844432199_0_0_0_Types[] = { &SelectItem_t2844432199_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectItem_t2844432199_0_0_0 = { 1, GenInst_SelectItem_t2844432199_0_0_0_Types };
extern const Il2CppType VocabularyQuestionData_t2021074976_0_0_0;
static const Il2CppType* GenInst_VocabularyQuestionData_t2021074976_0_0_0_Types[] = { &VocabularyQuestionData_t2021074976_0_0_0 };
extern const Il2CppGenericInst GenInst_VocabularyQuestionData_t2021074976_0_0_0 = { 1, GenInst_VocabularyQuestionData_t2021074976_0_0_0_Types };
extern const Il2CppType VocabularyOnplayQuestion_t3519225043_0_0_0;
static const Il2CppType* GenInst_VocabularyOnplayQuestion_t3519225043_0_0_0_Types[] = { &VocabularyOnplayQuestion_t3519225043_0_0_0 };
extern const Il2CppGenericInst GenInst_VocabularyOnplayQuestion_t3519225043_0_0_0 = { 1, GenInst_VocabularyOnplayQuestion_t3519225043_0_0_0_Types };
extern const Il2CppType RawImage_t2749640213_0_0_0;
static const Il2CppType* GenInst_RawImage_t2749640213_0_0_0_Types[] = { &RawImage_t2749640213_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t2749640213_0_0_0 = { 1, GenInst_RawImage_t2749640213_0_0_0_Types };
extern const Il2CppType Slider_t297367283_0_0_0;
static const Il2CppType* GenInst_Slider_t297367283_0_0_0_Types[] = { &Slider_t297367283_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t297367283_0_0_0 = { 1, GenInst_Slider_t297367283_0_0_0_Types };
extern const Il2CppType ScrubberEvents_t2429506345_0_0_0;
static const Il2CppType* GenInst_ScrubberEvents_t2429506345_0_0_0_Types[] = { &ScrubberEvents_t2429506345_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrubberEvents_t2429506345_0_0_0 = { 1, GenInst_ScrubberEvents_t2429506345_0_0_0_Types };
extern const Il2CppType GvrEye_t3930157106_0_0_0;
static const Il2CppType* GenInst_GvrEye_t3930157106_0_0_0_Types[] = { &GvrEye_t3930157106_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrEye_t3930157106_0_0_0 = { 1, GenInst_GvrEye_t3930157106_0_0_0_Types };
static const Il2CppType* GenInst_GvrEye_t3930157106_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &GvrEye_t3930157106_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrEye_t3930157106_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_GvrEye_t3930157106_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType GvrHead_t3923315805_0_0_0;
static const Il2CppType* GenInst_GvrEye_t3930157106_0_0_0_GvrHead_t3923315805_0_0_0_Types[] = { &GvrEye_t3930157106_0_0_0, &GvrHead_t3923315805_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrEye_t3930157106_0_0_0_GvrHead_t3923315805_0_0_0 = { 2, GenInst_GvrEye_t3930157106_0_0_0_GvrHead_t3923315805_0_0_0_Types };
static const Il2CppType* GenInst_GvrHead_t3923315805_0_0_0_Types[] = { &GvrHead_t3923315805_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrHead_t3923315805_0_0_0 = { 1, GenInst_GvrHead_t3923315805_0_0_0_Types };
extern const Il2CppType GvrAudioRoom_t1253442178_0_0_0;
static const Il2CppType* GenInst_GvrAudioRoom_t1253442178_0_0_0_Types[] = { &GvrAudioRoom_t1253442178_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrAudioRoom_t1253442178_0_0_0 = { 1, GenInst_GvrAudioRoom_t1253442178_0_0_0_Types };
extern const Il2CppType PhoneEvent_t2572128318_0_0_0;
extern const Il2CppType Builder_t2537253112_0_0_0;
static const Il2CppType* GenInst_PhoneEvent_t2572128318_0_0_0_Builder_t2537253112_0_0_0_Types[] = { &PhoneEvent_t2572128318_0_0_0, &Builder_t2537253112_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneEvent_t2572128318_0_0_0_Builder_t2537253112_0_0_0 = { 2, GenInst_PhoneEvent_t2572128318_0_0_0_Builder_t2537253112_0_0_0_Types };
extern const Il2CppType EmulatorConfig_t616150261_0_0_0;
static const Il2CppType* GenInst_EmulatorConfig_t616150261_0_0_0_Types[] = { &EmulatorConfig_t616150261_0_0_0 };
extern const Il2CppGenericInst GenInst_EmulatorConfig_t616150261_0_0_0 = { 1, GenInst_EmulatorConfig_t616150261_0_0_0_Types };
extern const Il2CppType Pointer_t3000685002_0_0_0;
static const Il2CppType* GenInst_Pointer_t3000685002_0_0_0_Types[] = { &Pointer_t3000685002_0_0_0 };
extern const Il2CppGenericInst GenInst_Pointer_t3000685002_0_0_0 = { 1, GenInst_Pointer_t3000685002_0_0_0_Types };
extern const Il2CppType Pointer_t1211758263_0_0_0;
static const Il2CppType* GenInst_Pointer_t1211758263_0_0_0_Types[] = { &Pointer_t1211758263_0_0_0 };
extern const Il2CppGenericInst GenInst_Pointer_t1211758263_0_0_0 = { 1, GenInst_Pointer_t1211758263_0_0_0_Types };
extern const Il2CppType MotionEvent_t4072706903_0_0_0;
extern const Il2CppType Builder_t3452538341_0_0_0;
static const Il2CppType* GenInst_MotionEvent_t4072706903_0_0_0_Builder_t3452538341_0_0_0_Types[] = { &MotionEvent_t4072706903_0_0_0, &Builder_t3452538341_0_0_0 };
extern const Il2CppGenericInst GenInst_MotionEvent_t4072706903_0_0_0_Builder_t3452538341_0_0_0 = { 2, GenInst_MotionEvent_t4072706903_0_0_0_Builder_t3452538341_0_0_0_Types };
extern const Il2CppType Builder_t2701542133_0_0_0;
static const Il2CppType* GenInst_Pointer_t1211758263_0_0_0_Builder_t2701542133_0_0_0_Types[] = { &Pointer_t1211758263_0_0_0, &Builder_t2701542133_0_0_0 };
extern const Il2CppGenericInst GenInst_Pointer_t1211758263_0_0_0_Builder_t2701542133_0_0_0 = { 2, GenInst_Pointer_t1211758263_0_0_0_Builder_t2701542133_0_0_0_Types };
extern const Il2CppType GyroscopeEvent_t182225200_0_0_0;
extern const Il2CppType Builder_t33558588_0_0_0;
static const Il2CppType* GenInst_GyroscopeEvent_t182225200_0_0_0_Builder_t33558588_0_0_0_Types[] = { &GyroscopeEvent_t182225200_0_0_0, &Builder_t33558588_0_0_0 };
extern const Il2CppGenericInst GenInst_GyroscopeEvent_t182225200_0_0_0_Builder_t33558588_0_0_0 = { 2, GenInst_GyroscopeEvent_t182225200_0_0_0_Builder_t33558588_0_0_0_Types };
extern const Il2CppType AccelerometerEvent_t1893725728_0_0_0;
extern const Il2CppType Builder_t1480486140_0_0_0;
static const Il2CppType* GenInst_AccelerometerEvent_t1893725728_0_0_0_Builder_t1480486140_0_0_0_Types[] = { &AccelerometerEvent_t1893725728_0_0_0, &Builder_t1480486140_0_0_0 };
extern const Il2CppGenericInst GenInst_AccelerometerEvent_t1893725728_0_0_0_Builder_t1480486140_0_0_0 = { 2, GenInst_AccelerometerEvent_t1893725728_0_0_0_Builder_t1480486140_0_0_0_Types };
extern const Il2CppType DepthMapEvent_t1516604558_0_0_0;
extern const Il2CppType Builder_t3483346914_0_0_0;
static const Il2CppType* GenInst_DepthMapEvent_t1516604558_0_0_0_Builder_t3483346914_0_0_0_Types[] = { &DepthMapEvent_t1516604558_0_0_0, &Builder_t3483346914_0_0_0 };
extern const Il2CppGenericInst GenInst_DepthMapEvent_t1516604558_0_0_0_Builder_t3483346914_0_0_0 = { 2, GenInst_DepthMapEvent_t1516604558_0_0_0_Builder_t3483346914_0_0_0_Types };
extern const Il2CppType OrientationEvent_t2038376807_0_0_0;
extern const Il2CppType Builder_t2561526853_0_0_0;
static const Il2CppType* GenInst_OrientationEvent_t2038376807_0_0_0_Builder_t2561526853_0_0_0_Types[] = { &OrientationEvent_t2038376807_0_0_0, &Builder_t2561526853_0_0_0 };
extern const Il2CppGenericInst GenInst_OrientationEvent_t2038376807_0_0_0_Builder_t2561526853_0_0_0 = { 2, GenInst_OrientationEvent_t2038376807_0_0_0_Builder_t2561526853_0_0_0_Types };
extern const Il2CppType KeyEvent_t639576718_0_0_0;
extern const Il2CppType Builder_t2056133158_0_0_0;
static const Il2CppType* GenInst_KeyEvent_t639576718_0_0_0_Builder_t2056133158_0_0_0_Types[] = { &KeyEvent_t639576718_0_0_0, &Builder_t2056133158_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEvent_t639576718_0_0_0_Builder_t2056133158_0_0_0 = { 2, GenInst_KeyEvent_t639576718_0_0_0_Builder_t2056133158_0_0_0_Types };
extern const Il2CppType IGvrPointerHoverHandler_t1683868601_0_0_0;
static const Il2CppType* GenInst_IGvrPointerHoverHandler_t1683868601_0_0_0_Types[] = { &IGvrPointerHoverHandler_t1683868601_0_0_0 };
extern const Il2CppGenericInst GenInst_IGvrPointerHoverHandler_t1683868601_0_0_0 = { 1, GenInst_IGvrPointerHoverHandler_t1683868601_0_0_0_Types };
extern const Il2CppType GvrBasePointer_t2150122635_0_0_0;
static const Il2CppType* GenInst_GvrBasePointer_t2150122635_0_0_0_GvrBasePointer_t2150122635_0_0_0_Types[] = { &GvrBasePointer_t2150122635_0_0_0, &GvrBasePointer_t2150122635_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrBasePointer_t2150122635_0_0_0_GvrBasePointer_t2150122635_0_0_0 = { 2, GenInst_GvrBasePointer_t2150122635_0_0_0_GvrBasePointer_t2150122635_0_0_0_Types };
static const Il2CppType* GenInst_GvrBasePointer_t2150122635_0_0_0_Types[] = { &GvrBasePointer_t2150122635_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrBasePointer_t2150122635_0_0_0 = { 1, GenInst_GvrBasePointer_t2150122635_0_0_0_Types };
static const Il2CppType* GenInst_GvrBasePointer_t2150122635_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &GvrBasePointer_t2150122635_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrBasePointer_t2150122635_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_GvrBasePointer_t2150122635_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Texture2D_t3542995729_0_0_0;
static const Il2CppType* GenInst_Texture2D_t3542995729_0_0_0_Types[] = { &Texture2D_t3542995729_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t3542995729_0_0_0 = { 1, GenInst_Texture2D_t3542995729_0_0_0_Types };
extern const Il2CppType Texture_t2243626319_0_0_0;
static const Il2CppType* GenInst_Texture_t2243626319_0_0_0_Types[] = { &Texture_t2243626319_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t2243626319_0_0_0 = { 1, GenInst_Texture_t2243626319_0_0_0_Types };
extern const Il2CppType Action_1_t1873676830_0_0_0;
static const Il2CppType* GenInst_Action_1_t1873676830_0_0_0_Types[] = { &Action_1_t1873676830_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t1873676830_0_0_0 = { 1, GenInst_Action_1_t1873676830_0_0_0_Types };
extern const Il2CppType Action_2_t4234541925_0_0_0;
static const Il2CppType* GenInst_Action_2_t4234541925_0_0_0_Types[] = { &Action_2_t4234541925_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4234541925_0_0_0 = { 1, GenInst_Action_2_t4234541925_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2281509423_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Dictionary_2_t2281509423_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Dictionary_2_t2281509423_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Dictionary_2_t2281509423_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Dictionary_2_t2281509423_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Dictionary_2_t2281509423_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Dictionary_2_t2281509423_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Dictionary_2_t2281509423_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Dictionary_2_t2281509423_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2281509423_0_0_0_Types[] = { &Dictionary_2_t2281509423_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2281509423_0_0_0 = { 1, GenInst_Dictionary_2_t2281509423_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3925882070_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3925882070_0_0_0_Types[] = { &KeyValuePair_2_t3925882070_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3925882070_0_0_0 = { 1, GenInst_KeyValuePair_2_t3925882070_0_0_0_Types };
extern const Il2CppType Sample_t3185432476_0_0_0;
static const Il2CppType* GenInst_Sample_t3185432476_0_0_0_Types[] = { &Sample_t3185432476_0_0_0 };
extern const Il2CppGenericInst GenInst_Sample_t3185432476_0_0_0 = { 1, GenInst_Sample_t3185432476_0_0_0_Types };
extern const Il2CppType Log_t3604182180_0_0_0;
static const Il2CppType* GenInst_Log_t3604182180_0_0_0_Types[] = { &Log_t3604182180_0_0_0 };
extern const Il2CppGenericInst GenInst_Log_t3604182180_0_0_0 = { 1, GenInst_Log_t3604182180_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Log_t3604182180_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &Log_t3604182180_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_Log_t3604182180_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_Log_t3604182180_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1223994146_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t1223994146_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t1223994146_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t1223994146_0_0_0 = { 2, GenInst_String_t_0_0_0_Dictionary_2_t1223994146_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Log_t3604182180_0_0_0_Types[] = { &String_t_0_0_0, &Log_t3604182180_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Log_t3604182180_0_0_0 = { 2, GenInst_String_t_0_0_0_Log_t3604182180_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t1223994146_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t1223994146_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t1223994146_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Dictionary_2_t1223994146_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Log_t3604182180_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Log_t3604182180_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Log_t3604182180_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Log_t3604182180_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1223994146_0_0_0_Types[] = { &Dictionary_2_t1223994146_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1223994146_0_0_0 = { 1, GenInst_Dictionary_2_t1223994146_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t896118630_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t896118630_0_0_0_Types[] = { &KeyValuePair_2_t896118630_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t896118630_0_0_0 = { 1, GenInst_KeyValuePair_2_t896118630_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3276306664_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3276306664_0_0_0_Types[] = { &KeyValuePair_2_t3276306664_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3276306664_0_0_0 = { 1, GenInst_KeyValuePair_2_t3276306664_0_0_0_Types };
static const Il2CppType* GenInst_PurchaseFailureReason_t1322959839_0_0_0_Types[] = { &PurchaseFailureReason_t1322959839_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchaseFailureReason_t1322959839_0_0_0 = { 1, GenInst_PurchaseFailureReason_t1322959839_0_0_0_Types };
extern const Il2CppType Type_t1530480861_0_0_0;
static const Il2CppType* GenInst_Type_t1530480861_0_0_0_Types[] = { &Type_t1530480861_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t1530480861_0_0_0 = { 1, GenInst_Type_t1530480861_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Type_t1530480861_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2590619014_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2590619014_0_0_0_Types[] = { &KeyValuePair_2_t2590619014_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2590619014_0_0_0 = { 1, GenInst_KeyValuePair_2_t2590619014_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Type_t1530480861_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Type_t1530480861_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Type_t1530480861_0_0_0, &Type_t1530480861_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Type_t1530480861_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Type_t1530480861_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Type_t1530480861_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_KeyValuePair_2_t2590619014_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Type_t1530480861_0_0_0, &KeyValuePair_2_t2590619014_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_KeyValuePair_2_t2590619014_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_KeyValuePair_2_t2590619014_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4048664256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1730553742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types[] = { &Array_Sort_m1730553742_gp_0_0_0_0, &Array_Sort_m1730553742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3106198730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3106198730_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types[] = { &Array_Sort_m3106198730_gp_0_0_0_0, &Array_Sort_m3106198730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2090966156_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0, &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0, &Array_Sort_m1985772939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2736815140_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types[] = { &Array_Sort_m2736815140_gp_0_0_0_0, &Array_Sort_m2736815140_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2468799988_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2468799988_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types[] = { &Array_Sort_m2468799988_gp_0_0_0_0, &Array_Sort_m2468799988_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2587948790_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0, &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0, &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m52621935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types[] = { &Array_Sort_m52621935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m52621935_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3546416104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types[] = { &Array_Sort_m3546416104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3546416104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0, &Array_qsort_m533480027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m940423571_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m940423571_gp_0_0_0_0_Types[] = { &Array_compare_m940423571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m940423571_gp_0_0_0_0 = { 1, GenInst_Array_compare_m940423571_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m565008110_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types[] = { &Array_qsort_m565008110_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m565008110_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1201602141_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types[] = { &Array_Resize_m1201602141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1201602141_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2783802133_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2783802133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m3775633118_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types[] = { &Array_ForEach_m3775633118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3775633118_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1734974082_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1734974082_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1734974082_gp_0_0_0_0, &Array_ConvertAll_m1734974082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m934773128_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m934773128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m3202023711_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m3202023711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m352384762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m352384762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1593955424_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1593955424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1546138173_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1546138173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1082322798_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1082322798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m525402987_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m525402987_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3577113407_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3577113407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1033585031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1033585031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3052238307_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3052238307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1306290405_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1306290405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2825795862_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2825795862_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2841140625_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2841140625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3304283431_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3304283431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3860096562_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3860096562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m2100440379_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m2100440379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m982349212_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types[] = { &Array_FindAll_m982349212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m982349212_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1825464757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types[] = { &Array_Exists_m1825464757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1825464757_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1258056624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1258056624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m2529971459_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types[] = { &Array_Find_m2529971459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m2529971459_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3929249453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types[] = { &Array_FindLast_m3929249453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3929249453_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3582267753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { &IList_1_t3737699284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { &ICollection_1_t1552160836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { &Nullable_1_t1398937014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { &Comparer_1_t1036860714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { &DefaultComparer_t3074655092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1787398723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3895203923_gp_0_0_0_0, &ShimEnumerator_t3895203923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { &Enumerator_t2089681430_gp_0_0_0_0, &Enumerator_t2089681430_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { &KeyValuePair_2_t3434615342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0, &Enumerator_t83320710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_0_0_0_0, &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2066709010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { &DefaultComparer_t1766400012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { &KeyValuePair_2_t4174120762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0, &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1988958766_gp_0_0_0_0, &KeyValuePair_2_t1988958766_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { &Enumerator_t1292967705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { &Collection_1_t686054069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { &ArraySegment_1_t1001032761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t1458930734_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types[] = { &Queue_1_t1458930734_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t1458930734_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4000919638_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types[] = { &Enumerator_t4000919638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4000919638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { &Stack_1_t4016656541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { &Enumerator_t546412149_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { &HashSet_1_t2624254809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { &Enumerator_t2109956843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { &PrimeHelper_t3424417428_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Aggregate_m964332100_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Types[] = { &Enumerable_Aggregate_m964332100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0 = { 1, GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Types[] = { &Enumerable_Aggregate_m964332100_gp_0_0_0_0, &Enumerable_Aggregate_m964332100_gp_0_0_0_0, &Enumerable_Aggregate_m964332100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0 = { 3, GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m665396702_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types[] = { &Enumerable_Any_m665396702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m665396702_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m1284016302_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m1284016302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m4622279_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m4622279_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m1561720045_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types[] = { &Enumerable_Count_m1561720045_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m136242780_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Types[] = { &Enumerable_Count_m136242780_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m136242780_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Count_m136242780_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0, &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m3508258668_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Types[] = { &Enumerable_Select_m3508258668_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m3508258668_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0_Types[] = { &Enumerable_Select_m3508258668_gp_0_0_0_0, &Int32_t2071877448_0_0_0, &Enumerable_Select_m3508258668_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0 = { 3, GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m3508258668_gp_1_0_0_0_Types[] = { &Enumerable_Select_m3508258668_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m3508258668_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m3508258668_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0_Types[] = { &Enumerable_Select_m3508258668_gp_0_0_0_0, &Enumerable_Select_m3508258668_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0, &Int32_t2071877448_0_0_0, &Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 = { 3, GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Skip_m3101762585_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Skip_m3101762585_gp_0_0_0_0_Types[] = { &Enumerable_Skip_m3101762585_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Skip_m3101762585_gp_0_0_0_0 = { 1, GenInst_Enumerable_Skip_m3101762585_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0_Types[] = { &Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Take_m169782875_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Take_m169782875_gp_0_0_0_0_Types[] = { &Enumerable_Take_m169782875_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Take_m169782875_gp_0_0_0_0 = { 1, GenInst_Enumerable_Take_m169782875_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0_Types[] = { &Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m2343256994_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m2343256994_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3027976024_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3027976024_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_0_0_0_0, &Enumerable_ToDictionary_m3027976024_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3027976024_gp_2_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_0_0_0_0, &Enumerable_ToDictionary_m3027976024_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m3027976024_gp_1_0_0_0, &Enumerable_ToDictionary_m3027976024_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m2810079530_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2810079530_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m2810079530_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m2810079530_gp_0_0_0_0, &Enumerable_ToDictionary_m2810079530_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2810079530_gp_1_0_0_0, &Enumerable_ToDictionary_m2810079530_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3284215677_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m3284215677_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0, &Enumerable_ToDictionary_m3284215677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_1_0_0_0, &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0, &Enumerable_ToDictionary_m3284215677_gp_1_0_0_0, &Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0 = { 3, GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m261161385_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m261161385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m2409552823_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Function_1_t1491613575_gp_0_0_0_0;
static const Il2CppType* GenInst_Function_1_t1491613575_gp_0_0_0_0_Function_1_t1491613575_gp_0_0_0_0_Types[] = { &Function_1_t1491613575_gp_0_0_0_0, &Function_1_t1491613575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Function_1_t1491613575_gp_0_0_0_0_Function_1_t1491613575_gp_0_0_0_0 = { 2, GenInst_Function_1_t1491613575_gp_0_0_0_0_Function_1_t1491613575_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Function_1_t1491613575_gp_0_0_0_0_Types[] = { &Function_1_t1491613575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Function_1_t1491613575_gp_0_0_0_0 = { 1, GenInst_Function_1_t1491613575_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Int32_t2071877448_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0, &Int32_t2071877448_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Int32_t2071877448_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 = { 3, GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Int32_t2071877448_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0_Types[] = { &U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0 = { 1, GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0_Types[] = { &U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0 = { 1, GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_Call_m1094633808_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_Call_m1094633808_gp_0_0_0_0_Types[] = { &AndroidJavaObject_Call_m1094633808_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_Call_m1094633808_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_Call_m1094633808_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0_Types[] = { &AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__Call_m4019607101_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__Call_m4019607101_gp_0_0_0_0_Types[] = { &AndroidJavaObject__Call_m4019607101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__Call_m4019607101_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__Call_m4019607101_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0_Types[] = { &AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m3417738402_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m825036157_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m825036157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m3873375864_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m1600202230_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3990064736_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3990064736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2051523689_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2051523689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2621570726_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2621570726_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m3101579087_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m3101579087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m3999848894_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m4171325764_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types };
extern const Il2CppType Resources_FindObjectsOfTypeAll_m3225124216_gp_0_0_0_0;
static const Il2CppType* GenInst_Resources_FindObjectsOfTypeAll_m3225124216_gp_0_0_0_0_Types[] = { &Resources_FindObjectsOfTypeAll_m3225124216_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Resources_FindObjectsOfTypeAll_m3225124216_gp_0_0_0_0 = { 1, GenInst_Resources_FindObjectsOfTypeAll_m3225124216_gp_0_0_0_0_Types };
extern const Il2CppType Resources_LoadAll_m4114724026_gp_0_0_0_0;
static const Il2CppType* GenInst_Resources_LoadAll_m4114724026_gp_0_0_0_0_Types[] = { &Resources_LoadAll_m4114724026_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Resources_LoadAll_m4114724026_gp_0_0_0_0 = { 1, GenInst_Resources_LoadAll_m4114724026_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m2530741872_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types[] = { &Object_Instantiate_m2530741872_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m894835059_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m894835059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types };
extern const Il2CppType GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types[] = { &GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 = { 1, GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types };
extern const Il2CppType AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0;
static const Il2CppType* GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types[] = { &AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 = { 1, GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types };
extern const Il2CppType CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0;
static const Il2CppType* GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types[] = { &CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 = { 1, GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types };
extern const Il2CppType ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0;
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0, &ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0 = { 2, GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1319939458_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1319939458_0_0_0_Types[] = { &KeyValuePair_2_t1319939458_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1319939458_0_0_0 = { 1, GenInst_KeyValuePair_2_t1319939458_0_0_0_Types };
extern const Il2CppType _AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0;
static const Il2CppType* GenInst__AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0_Types[] = { &_AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst__AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0 = { 1, GenInst__AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { &InvokableCall_1_t476640868_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { &UnityAction_1_t2490859068_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0, &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0, &InvokableCall_3_t3608808750_gp_1_0_0_0, &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0, &InvokableCall_4_t879925395_gp_1_0_0_0, &InvokableCall_4_t879925395_gp_2_0_0_0, &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t224769006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { &UnityEvent_1_t4075366602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { &UnityEvent_2_t4075366599_gp_0_0_0_0, &UnityEvent_2_t4075366599_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { &UnityEvent_3_t4075366600_gp_0_0_0_0, &UnityEvent_3_t4075366600_gp_1_0_0_0, &UnityEvent_3_t4075366600_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { &UnityEvent_4_t4075366597_gp_0_0_0_0, &UnityEvent_4_t4075366597_gp_1_0_0_0, &UnityEvent_4_t4075366597_gp_2_0_0_0, &UnityEvent_4_t4075366597_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
extern const Il2CppType ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0;
static const Il2CppType* GenInst_ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0_Types[] = { &ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0 = { 1, GenInst_ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0_Types };
extern const Il2CppType AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0;
static const Il2CppType* GenInst_AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0_Types[] = { &AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0 = { 1, GenInst_AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0_Types };
extern const Il2CppType AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0;
static const Il2CppType* GenInst_AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0_Types[] = { &AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0 = { 1, GenInst_AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m1961163955_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2584777480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { &ListPool_1_t1984115411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2000868992_0_0_0;
static const Il2CppType* GenInst_List_1_t2000868992_0_0_0_Types[] = { &List_1_t2000868992_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { &ObjectPool_1_t4265859154_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
extern const Il2CppType FakeStore_StartUI_m935561654_gp_0_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_FakeStore_StartUI_m935561654_gp_0_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &FakeStore_StartUI_m935561654_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_FakeStore_StartUI_m935561654_gp_0_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_FakeStore_StartUI_m935561654_gp_0_0_0_0_Types };
extern const Il2CppType UIFakeStore_StartUI_m1214015556_gp_0_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &UIFakeStore_StartUI_m1214015556_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0_Types[] = { &UIFakeStore_StartUI_m1214015556_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0 = { 1, GenInst_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0_Types };
extern const Il2CppType U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0_Types };
extern const Il2CppType UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0_Types[] = { &UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0 = { 1, GenInst_UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0_Types };
extern const Il2CppType UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0_Types[] = { &UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0 = { 1, GenInst_UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0_Types };
extern const Il2CppType CodedInputStream_ReadEnum_m2252621261_gp_0_0_0_0;
static const Il2CppType* GenInst_CodedInputStream_ReadEnum_m2252621261_gp_0_0_0_0_Types[] = { &CodedInputStream_ReadEnum_m2252621261_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CodedInputStream_ReadEnum_m2252621261_gp_0_0_0_0 = { 1, GenInst_CodedInputStream_ReadEnum_m2252621261_gp_0_0_0_0_Types };
extern const Il2CppType CodedInputStream_ReadMessageArray_m2595769771_gp_0_0_0_0;
static const Il2CppType* GenInst_CodedInputStream_ReadMessageArray_m2595769771_gp_0_0_0_0_Types[] = { &CodedInputStream_ReadMessageArray_m2595769771_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CodedInputStream_ReadMessageArray_m2595769771_gp_0_0_0_0 = { 1, GenInst_CodedInputStream_ReadMessageArray_m2595769771_gp_0_0_0_0_Types };
extern const Il2CppType GeneratedBuilderLite_2_t1057568736_gp_0_0_0_0;
extern const Il2CppType GeneratedBuilderLite_2_t1057568736_gp_1_0_0_0;
static const Il2CppType* GenInst_GeneratedBuilderLite_2_t1057568736_gp_0_0_0_0_GeneratedBuilderLite_2_t1057568736_gp_1_0_0_0_Types[] = { &GeneratedBuilderLite_2_t1057568736_gp_0_0_0_0, &GeneratedBuilderLite_2_t1057568736_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneratedBuilderLite_2_t1057568736_gp_0_0_0_0_GeneratedBuilderLite_2_t1057568736_gp_1_0_0_0 = { 2, GenInst_GeneratedBuilderLite_2_t1057568736_gp_0_0_0_0_GeneratedBuilderLite_2_t1057568736_gp_1_0_0_0_Types };
extern const Il2CppType ICodedOutputStream_WriteMessageArray_m3043404257_gp_0_0_0_0;
static const Il2CppType* GenInst_ICodedOutputStream_WriteMessageArray_m3043404257_gp_0_0_0_0_Types[] = { &ICodedOutputStream_WriteMessageArray_m3043404257_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICodedOutputStream_WriteMessageArray_m3043404257_gp_0_0_0_0 = { 1, GenInst_ICodedOutputStream_WriteMessageArray_m3043404257_gp_0_0_0_0_Types };
extern const Il2CppType CodedOutputStream_WriteMessageArray_m2075822722_gp_0_0_0_0;
static const Il2CppType* GenInst_CodedOutputStream_WriteMessageArray_m2075822722_gp_0_0_0_0_Types[] = { &CodedOutputStream_WriteMessageArray_m2075822722_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CodedOutputStream_WriteMessageArray_m2075822722_gp_0_0_0_0 = { 1, GenInst_CodedOutputStream_WriteMessageArray_m2075822722_gp_0_0_0_0_Types };
extern const Il2CppType SerializationSurrogate_t2069264526_gp_0_0_0_0;
extern const Il2CppType SerializationSurrogate_t2069264526_gp_1_0_0_0;
static const Il2CppType* GenInst_SerializationSurrogate_t2069264526_gp_0_0_0_0_SerializationSurrogate_t2069264526_gp_1_0_0_0_Types[] = { &SerializationSurrogate_t2069264526_gp_0_0_0_0, &SerializationSurrogate_t2069264526_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializationSurrogate_t2069264526_gp_0_0_0_0_SerializationSurrogate_t2069264526_gp_1_0_0_0 = { 2, GenInst_SerializationSurrogate_t2069264526_gp_0_0_0_0_SerializationSurrogate_t2069264526_gp_1_0_0_0_Types };
extern const Il2CppType AbstractMessageLite_2_t3719087021_gp_0_0_0_0;
extern const Il2CppType AbstractMessageLite_2_t3719087021_gp_1_0_0_0;
static const Il2CppType* GenInst_AbstractMessageLite_2_t3719087021_gp_0_0_0_0_AbstractMessageLite_2_t3719087021_gp_1_0_0_0_Types[] = { &AbstractMessageLite_2_t3719087021_gp_0_0_0_0, &AbstractMessageLite_2_t3719087021_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractMessageLite_2_t3719087021_gp_0_0_0_0_AbstractMessageLite_2_t3719087021_gp_1_0_0_0 = { 2, GenInst_AbstractMessageLite_2_t3719087021_gp_0_0_0_0_AbstractMessageLite_2_t3719087021_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_AbstractMessageLite_2_t3719087021_gp_0_0_0_0_Types[] = { &AbstractMessageLite_2_t3719087021_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractMessageLite_2_t3719087021_gp_0_0_0_0 = { 1, GenInst_AbstractMessageLite_2_t3719087021_gp_0_0_0_0_Types };
extern const Il2CppType PopsicleList_1_t3726111090_gp_0_0_0_0;
static const Il2CppType* GenInst_PopsicleList_1_t3726111090_gp_0_0_0_0_Types[] = { &PopsicleList_1_t3726111090_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PopsicleList_1_t3726111090_gp_0_0_0_0 = { 1, GenInst_PopsicleList_1_t3726111090_gp_0_0_0_0_Types };
extern const Il2CppType Lists_AsReadOnly_m1817066548_gp_0_0_0_0;
static const Il2CppType* GenInst_Lists_AsReadOnly_m1817066548_gp_0_0_0_0_Types[] = { &Lists_AsReadOnly_m1817066548_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Lists_AsReadOnly_m1817066548_gp_0_0_0_0 = { 1, GenInst_Lists_AsReadOnly_m1817066548_gp_0_0_0_0_Types };
extern const Il2CppType Lists_1_t3840821546_gp_0_0_0_0;
static const Il2CppType* GenInst_Lists_1_t3840821546_gp_0_0_0_0_Types[] = { &Lists_1_t3840821546_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Lists_1_t3840821546_gp_0_0_0_0 = { 1, GenInst_Lists_1_t3840821546_gp_0_0_0_0_Types };
extern const Il2CppType IPopsicleList_1_t2252478525_gp_0_0_0_0;
static const Il2CppType* GenInst_IPopsicleList_1_t2252478525_gp_0_0_0_0_Types[] = { &IPopsicleList_1_t2252478525_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IPopsicleList_1_t2252478525_gp_0_0_0_0 = { 1, GenInst_IPopsicleList_1_t2252478525_gp_0_0_0_0_Types };
extern const Il2CppType LimitedInputStream_t2315138058_gp_0_0_0_0;
extern const Il2CppType LimitedInputStream_t2315138058_gp_1_0_0_0;
static const Il2CppType* GenInst_LimitedInputStream_t2315138058_gp_0_0_0_0_LimitedInputStream_t2315138058_gp_1_0_0_0_Types[] = { &LimitedInputStream_t2315138058_gp_0_0_0_0, &LimitedInputStream_t2315138058_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LimitedInputStream_t2315138058_gp_0_0_0_0_LimitedInputStream_t2315138058_gp_1_0_0_0 = { 2, GenInst_LimitedInputStream_t2315138058_gp_0_0_0_0_LimitedInputStream_t2315138058_gp_1_0_0_0_Types };
extern const Il2CppType SerializationSurrogate_t4192918730_gp_0_0_0_0;
extern const Il2CppType SerializationSurrogate_t4192918730_gp_1_0_0_0;
static const Il2CppType* GenInst_SerializationSurrogate_t4192918730_gp_0_0_0_0_SerializationSurrogate_t4192918730_gp_1_0_0_0_Types[] = { &SerializationSurrogate_t4192918730_gp_0_0_0_0, &SerializationSurrogate_t4192918730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializationSurrogate_t4192918730_gp_0_0_0_0_SerializationSurrogate_t4192918730_gp_1_0_0_0 = { 2, GenInst_SerializationSurrogate_t4192918730_gp_0_0_0_0_SerializationSurrogate_t4192918730_gp_1_0_0_0_Types };
extern const Il2CppType AbstractBuilderLite_2_t2037345479_gp_0_0_0_0;
extern const Il2CppType AbstractBuilderLite_2_t2037345479_gp_1_0_0_0;
static const Il2CppType* GenInst_AbstractBuilderLite_2_t2037345479_gp_0_0_0_0_AbstractBuilderLite_2_t2037345479_gp_1_0_0_0_Types[] = { &AbstractBuilderLite_2_t2037345479_gp_0_0_0_0, &AbstractBuilderLite_2_t2037345479_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractBuilderLite_2_t2037345479_gp_0_0_0_0_AbstractBuilderLite_2_t2037345479_gp_1_0_0_0 = { 2, GenInst_AbstractBuilderLite_2_t2037345479_gp_0_0_0_0_AbstractBuilderLite_2_t2037345479_gp_1_0_0_0_Types };
extern const Il2CppType ThrowHelper_ThrowIfAnyNull_m3282078172_gp_0_0_0_0;
static const Il2CppType* GenInst_ThrowHelper_ThrowIfAnyNull_m3282078172_gp_0_0_0_0_Types[] = { &ThrowHelper_ThrowIfAnyNull_m3282078172_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThrowHelper_ThrowIfAnyNull_m3282078172_gp_0_0_0_0 = { 1, GenInst_ThrowHelper_ThrowIfAnyNull_m3282078172_gp_0_0_0_0_Types };
extern const Il2CppType GeneratedMessageLite_2_t3914197900_gp_0_0_0_0;
extern const Il2CppType GeneratedMessageLite_2_t3914197900_gp_1_0_0_0;
static const Il2CppType* GenInst_GeneratedMessageLite_2_t3914197900_gp_0_0_0_0_GeneratedMessageLite_2_t3914197900_gp_1_0_0_0_Types[] = { &GeneratedMessageLite_2_t3914197900_gp_0_0_0_0, &GeneratedMessageLite_2_t3914197900_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneratedMessageLite_2_t3914197900_gp_0_0_0_0_GeneratedMessageLite_2_t3914197900_gp_1_0_0_0 = { 2, GenInst_GeneratedMessageLite_2_t3914197900_gp_0_0_0_0_GeneratedMessageLite_2_t3914197900_gp_1_0_0_0_Types };
extern const Il2CppType GeneratedMessageLite_2_PrintField_m3342867738_gp_0_0_0_0;
static const Il2CppType* GenInst_GeneratedMessageLite_2_PrintField_m3342867738_gp_0_0_0_0_Types[] = { &GeneratedMessageLite_2_PrintField_m3342867738_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneratedMessageLite_2_PrintField_m3342867738_gp_0_0_0_0 = { 1, GenInst_GeneratedMessageLite_2_PrintField_m3342867738_gp_0_0_0_0_Types };
extern const Il2CppType IBuilderLite_2_t3465575570_gp_0_0_0_0;
extern const Il2CppType IBuilderLite_2_t3465575570_gp_1_0_0_0;
static const Il2CppType* GenInst_IBuilderLite_2_t3465575570_gp_0_0_0_0_IBuilderLite_2_t3465575570_gp_1_0_0_0_Types[] = { &IBuilderLite_2_t3465575570_gp_0_0_0_0, &IBuilderLite_2_t3465575570_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IBuilderLite_2_t3465575570_gp_0_0_0_0_IBuilderLite_2_t3465575570_gp_1_0_0_0 = { 2, GenInst_IBuilderLite_2_t3465575570_gp_0_0_0_0_IBuilderLite_2_t3465575570_gp_1_0_0_0_Types };
extern const Il2CppType EnumParser_1_t2560453525_gp_0_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_EnumParser_1_t2560453525_gp_0_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &EnumParser_1_t2560453525_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_EnumParser_1_t2560453525_gp_0_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_EnumParser_1_t2560453525_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_EnumParser_1_t2560453525_gp_0_0_0_0_Types[] = { &EnumParser_1_t2560453525_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumParser_1_t2560453525_gp_0_0_0_0 = { 1, GenInst_EnumParser_1_t2560453525_gp_0_0_0_0_Types };
extern const Il2CppType IMessageLite_2_t1142469830_gp_0_0_0_0;
static const Il2CppType* GenInst_IMessageLite_2_t1142469830_gp_0_0_0_0_Types[] = { &IMessageLite_2_t1142469830_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IMessageLite_2_t1142469830_gp_0_0_0_0 = { 1, GenInst_IMessageLite_2_t1142469830_gp_0_0_0_0_Types };
extern const Il2CppType IMessageLite_2_t1142469830_gp_1_0_0_0;
static const Il2CppType* GenInst_IMessageLite_2_t1142469830_gp_0_0_0_0_IMessageLite_2_t1142469830_gp_1_0_0_0_Types[] = { &IMessageLite_2_t1142469830_gp_0_0_0_0, &IMessageLite_2_t1142469830_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IMessageLite_2_t1142469830_gp_0_0_0_0_IMessageLite_2_t1142469830_gp_1_0_0_0 = { 2, GenInst_IMessageLite_2_t1142469830_gp_0_0_0_0_IMessageLite_2_t1142469830_gp_1_0_0_0_Types };
extern const Il2CppType ICodedInputStream_ReadMessageArray_m14575878_gp_0_0_0_0;
static const Il2CppType* GenInst_ICodedInputStream_ReadMessageArray_m14575878_gp_0_0_0_0_Types[] = { &ICodedInputStream_ReadMessageArray_m14575878_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICodedInputStream_ReadMessageArray_m14575878_gp_0_0_0_0 = { 1, GenInst_ICodedInputStream_ReadMessageArray_m14575878_gp_0_0_0_0_Types };
extern const Il2CppType ApiConnector_RequestApi_m1716423800_gp_0_0_0_0;
static const Il2CppType* GenInst_ApiConnector_RequestApi_m1716423800_gp_0_0_0_0_Types[] = { &ApiConnector_RequestApi_m1716423800_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiConnector_RequestApi_m1716423800_gp_0_0_0_0 = { 1, GenInst_ApiConnector_RequestApi_m1716423800_gp_0_0_0_0_Types };
extern const Il2CppType U3CRequestApiU3Ec__AnonStorey1_1_t317839536_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRequestApiU3Ec__AnonStorey1_1_t317839536_gp_0_0_0_0_Types[] = { &U3CRequestApiU3Ec__AnonStorey1_1_t317839536_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRequestApiU3Ec__AnonStorey1_1_t317839536_gp_0_0_0_0 = { 1, GenInst_U3CRequestApiU3Ec__AnonStorey1_1_t317839536_gp_0_0_0_0_Types };
extern const Il2CppType ApiRequest_1_t797614342_gp_0_0_0_0;
static const Il2CppType* GenInst_ApiRequest_1_t797614342_gp_0_0_0_0_Types[] = { &ApiRequest_1_t797614342_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiRequest_1_t797614342_gp_0_0_0_0 = { 1, GenInst_ApiRequest_1_t797614342_gp_0_0_0_0_Types };
extern const Il2CppType U3CDoRequestU3Ec__Iterator0_t2682958910_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDoRequestU3Ec__Iterator0_t2682958910_gp_0_0_0_0_Types[] = { &U3CDoRequestU3Ec__Iterator0_t2682958910_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDoRequestU3Ec__Iterator0_t2682958910_gp_0_0_0_0 = { 1, GenInst_U3CDoRequestU3Ec__Iterator0_t2682958910_gp_0_0_0_0_Types };
extern const Il2CppType DataLoader_1_t2670374248_gp_0_0_0_0;
static const Il2CppType* GenInst_DataLoader_1_t2670374248_gp_0_0_0_0_Types[] = { &DataLoader_1_t2670374248_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DataLoader_1_t2670374248_gp_0_0_0_0 = { 1, GenInst_DataLoader_1_t2670374248_gp_0_0_0_0_Types };
extern const Il2CppType IDatasWrapper_1_t4104787928_gp_0_0_0_0;
static const Il2CppType* GenInst_IDatasWrapper_1_t4104787928_gp_0_0_0_0_Types[] = { &IDatasWrapper_1_t4104787928_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDatasWrapper_1_t4104787928_gp_0_0_0_0 = { 1, GenInst_IDatasWrapper_1_t4104787928_gp_0_0_0_0_Types };
extern const Il2CppType GvrDropdown_GetOrAddComponent_m1130649418_gp_0_0_0_0;
static const Il2CppType* GenInst_GvrDropdown_GetOrAddComponent_m1130649418_gp_0_0_0_0_Types[] = { &GvrDropdown_GetOrAddComponent_m1130649418_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrDropdown_GetOrAddComponent_m1130649418_gp_0_0_0_0 = { 1, GenInst_GvrDropdown_GetOrAddComponent_m1130649418_gp_0_0_0_0_Types };
extern const Il2CppType MultiKeyDictionary_3_t1255703599_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t3964470880_0_0_0;
static const Il2CppType* GenInst_MultiKeyDictionary_3_t1255703599_gp_0_0_0_0_Dictionary_2_t3964470880_0_0_0_Types[] = { &MultiKeyDictionary_3_t1255703599_gp_0_0_0_0, &Dictionary_2_t3964470880_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiKeyDictionary_3_t1255703599_gp_0_0_0_0_Dictionary_2_t3964470880_0_0_0 = { 2, GenInst_MultiKeyDictionary_3_t1255703599_gp_0_0_0_0_Dictionary_2_t3964470880_0_0_0_Types };
extern const Il2CppType MultiKeyDictionary_3_t1255703599_gp_1_0_0_0;
extern const Il2CppType MultiKeyDictionary_3_t1255703599_gp_2_0_0_0;
static const Il2CppType* GenInst_MultiKeyDictionary_3_t1255703599_gp_1_0_0_0_MultiKeyDictionary_3_t1255703599_gp_2_0_0_0_Types[] = { &MultiKeyDictionary_3_t1255703599_gp_1_0_0_0, &MultiKeyDictionary_3_t1255703599_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiKeyDictionary_3_t1255703599_gp_1_0_0_0_MultiKeyDictionary_3_t1255703599_gp_2_0_0_0 = { 2, GenInst_MultiKeyDictionary_3_t1255703599_gp_1_0_0_0_MultiKeyDictionary_3_t1255703599_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_GeneratedMessageLite_2_t3914197900_gp_0_0_0_0_Types[] = { &GeneratedMessageLite_2_t3914197900_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneratedMessageLite_2_t3914197900_gp_0_0_0_0 = { 1, GenInst_GeneratedMessageLite_2_t3914197900_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_MotionEvent_t4072706903_0_0_0_Types[] = { &MotionEvent_t4072706903_0_0_0 };
extern const Il2CppGenericInst GenInst_MotionEvent_t4072706903_0_0_0 = { 1, GenInst_MotionEvent_t4072706903_0_0_0_Types };
static const Il2CppType* GenInst_GyroscopeEvent_t182225200_0_0_0_Types[] = { &GyroscopeEvent_t182225200_0_0_0 };
extern const Il2CppGenericInst GenInst_GyroscopeEvent_t182225200_0_0_0 = { 1, GenInst_GyroscopeEvent_t182225200_0_0_0_Types };
static const Il2CppType* GenInst_AccelerometerEvent_t1893725728_0_0_0_Types[] = { &AccelerometerEvent_t1893725728_0_0_0 };
extern const Il2CppGenericInst GenInst_AccelerometerEvent_t1893725728_0_0_0 = { 1, GenInst_AccelerometerEvent_t1893725728_0_0_0_Types };
static const Il2CppType* GenInst_DepthMapEvent_t1516604558_0_0_0_Types[] = { &DepthMapEvent_t1516604558_0_0_0 };
extern const Il2CppGenericInst GenInst_DepthMapEvent_t1516604558_0_0_0 = { 1, GenInst_DepthMapEvent_t1516604558_0_0_0_Types };
static const Il2CppType* GenInst_OrientationEvent_t2038376807_0_0_0_Types[] = { &OrientationEvent_t2038376807_0_0_0 };
extern const Il2CppGenericInst GenInst_OrientationEvent_t2038376807_0_0_0 = { 1, GenInst_OrientationEvent_t2038376807_0_0_0_Types };
static const Il2CppType* GenInst_KeyEvent_t639576718_0_0_0_Types[] = { &KeyEvent_t639576718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEvent_t639576718_0_0_0 = { 1, GenInst_KeyEvent_t639576718_0_0_0_Types };
static const Il2CppType* GenInst_PhoneEvent_t2572128318_0_0_0_Types[] = { &PhoneEvent_t2572128318_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneEvent_t2572128318_0_0_0 = { 1, GenInst_PhoneEvent_t2572128318_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t453129793_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t453129793_0_0_0_Types[] = { &KeyValuePair_2_t453129793_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t453129793_0_0_0 = { 1, GenInst_KeyValuePair_2_t453129793_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { &DefaultExecutionOrder_t2717914595_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
extern const Il2CppType GUILayer_t3254902478_0_0_0;
static const Il2CppType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { &GUILayer_t3254902478_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_String_t_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_String_t_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType AsyncUtil_t423752048_0_0_0;
static const Il2CppType* GenInst_AsyncUtil_t423752048_0_0_0_Types[] = { &AsyncUtil_t423752048_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncUtil_t423752048_0_0_0 = { 1, GenInst_AsyncUtil_t423752048_0_0_0_Types };
extern const Il2CppType EventSystem_t3466835263_0_0_0;
static const Il2CppType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { &EventSystem_t3466835263_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { &AxisEventData_t1524870173_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { &SpriteRenderer_t1209076198_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
extern const Il2CppType Scrollbar_t3248359358_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t3248359358_0_0_0_Types[] = { &Scrollbar_t3248359358_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t3248359358_0_0_0 = { 1, GenInst_Scrollbar_t3248359358_0_0_0_Types };
extern const Il2CppType InputField_t1631627530_0_0_0;
static const Il2CppType* GenInst_InputField_t1631627530_0_0_0_Types[] = { &InputField_t1631627530_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t1631627530_0_0_0 = { 1, GenInst_InputField_t1631627530_0_0_0_Types };
extern const Il2CppType ScrollRect_t1199013257_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t1199013257_0_0_0_Types[] = { &ScrollRect_t1199013257_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t1199013257_0_0_0 = { 1, GenInst_ScrollRect_t1199013257_0_0_0_Types };
extern const Il2CppType Dropdown_t1985816271_0_0_0;
static const Il2CppType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { &Dropdown_t1985816271_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { &GraphicRaycaster_t410733016_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { &CanvasRenderer_t261436805_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
extern const Il2CppType Corner_t1077473318_0_0_0;
static const Il2CppType* GenInst_Corner_t1077473318_0_0_0_Types[] = { &Corner_t1077473318_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
extern const Il2CppType Axis_t1431825778_0_0_0;
static const Il2CppType* GenInst_Axis_t1431825778_0_0_0_Types[] = { &Axis_t1431825778_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
extern const Il2CppType Constraint_t3558160636_0_0_0;
static const Il2CppType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { &Constraint_t3558160636_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { &SubmitEvent_t907918422_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { &OnChangeEvent_t2863344003_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { &OnValidateInput_t1946318473_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { &LayoutElement_t2808691390_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
extern const Il2CppType RectOffset_t3387826427_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
extern const Il2CppType TextAnchor_t112990806_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { &TextAnchor_t112990806_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { &AnimationTriggers_t3244928895_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
extern const Il2CppType Animator_t69676727_0_0_0;
static const Il2CppType* GenInst_Animator_t69676727_0_0_0_Types[] = { &Animator_t69676727_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
extern const Il2CppType ProductCatalog_t2667590766_0_0_0;
static const Il2CppType* GenInst_ProductCatalog_t2667590766_0_0_0_Types[] = { &ProductCatalog_t2667590766_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductCatalog_t2667590766_0_0_0 = { 1, GenInst_ProductCatalog_t2667590766_0_0_0_Types };
extern const Il2CppType IAmazonExtensions_t3890253245_0_0_0;
static const Il2CppType* GenInst_IAmazonExtensions_t3890253245_0_0_0_Types[] = { &IAmazonExtensions_t3890253245_0_0_0 };
extern const Il2CppGenericInst GenInst_IAmazonExtensions_t3890253245_0_0_0 = { 1, GenInst_IAmazonExtensions_t3890253245_0_0_0_Types };
extern const Il2CppType IAmazonConfiguration_t3016942165_0_0_0;
static const Il2CppType* GenInst_IAmazonConfiguration_t3016942165_0_0_0_Types[] = { &IAmazonConfiguration_t3016942165_0_0_0 };
extern const Il2CppGenericInst GenInst_IAmazonConfiguration_t3016942165_0_0_0 = { 1, GenInst_IAmazonConfiguration_t3016942165_0_0_0_Types };
extern const Il2CppType ISamsungAppsExtensions_t3429739537_0_0_0;
static const Il2CppType* GenInst_ISamsungAppsExtensions_t3429739537_0_0_0_Types[] = { &ISamsungAppsExtensions_t3429739537_0_0_0 };
extern const Il2CppGenericInst GenInst_ISamsungAppsExtensions_t3429739537_0_0_0 = { 1, GenInst_ISamsungAppsExtensions_t3429739537_0_0_0_Types };
extern const Il2CppType ISamsungAppsConfiguration_t4066821689_0_0_0;
static const Il2CppType* GenInst_ISamsungAppsConfiguration_t4066821689_0_0_0_Types[] = { &ISamsungAppsConfiguration_t4066821689_0_0_0 };
extern const Il2CppGenericInst GenInst_ISamsungAppsConfiguration_t4066821689_0_0_0 = { 1, GenInst_ISamsungAppsConfiguration_t4066821689_0_0_0_Types };
extern const Il2CppType UnityUtil_t166323129_0_0_0;
static const Il2CppType* GenInst_UnityUtil_t166323129_0_0_0_Types[] = { &UnityUtil_t166323129_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_t166323129_0_0_0 = { 1, GenInst_UnityUtil_t166323129_0_0_0_Types };
extern const Il2CppType IAppleExtensions_t1627764765_0_0_0;
static const Il2CppType* GenInst_IAppleExtensions_t1627764765_0_0_0_Types[] = { &IAppleExtensions_t1627764765_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppleExtensions_t1627764765_0_0_0 = { 1, GenInst_IAppleExtensions_t1627764765_0_0_0_Types };
extern const Il2CppType IAppleConfiguration_t3277762425_0_0_0;
static const Il2CppType* GenInst_IAppleConfiguration_t3277762425_0_0_0_Types[] = { &IAppleConfiguration_t3277762425_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppleConfiguration_t3277762425_0_0_0 = { 1, GenInst_IAppleConfiguration_t3277762425_0_0_0_Types };
extern const Il2CppType IMicrosoftConfiguration_t1212838845_0_0_0;
static const Il2CppType* GenInst_IMicrosoftConfiguration_t1212838845_0_0_0_Types[] = { &IMicrosoftConfiguration_t1212838845_0_0_0 };
extern const Il2CppGenericInst GenInst_IMicrosoftConfiguration_t1212838845_0_0_0 = { 1, GenInst_IMicrosoftConfiguration_t1212838845_0_0_0_Types };
extern const Il2CppType IGooglePlayConfiguration_t2615679878_0_0_0;
static const Il2CppType* GenInst_IGooglePlayConfiguration_t2615679878_0_0_0_Types[] = { &IGooglePlayConfiguration_t2615679878_0_0_0 };
extern const Il2CppGenericInst GenInst_IGooglePlayConfiguration_t2615679878_0_0_0 = { 1, GenInst_IGooglePlayConfiguration_t2615679878_0_0_0_Types };
extern const Il2CppType ITizenStoreConfiguration_t2900348728_0_0_0;
static const Il2CppType* GenInst_ITizenStoreConfiguration_t2900348728_0_0_0_Types[] = { &ITizenStoreConfiguration_t2900348728_0_0_0 };
extern const Il2CppGenericInst GenInst_ITizenStoreConfiguration_t2900348728_0_0_0 = { 1, GenInst_ITizenStoreConfiguration_t2900348728_0_0_0_Types };
extern const Il2CppType IAndroidStoreSelection_t3134941501_0_0_0;
static const Il2CppType* GenInst_IAndroidStoreSelection_t3134941501_0_0_0_Types[] = { &IAndroidStoreSelection_t3134941501_0_0_0 };
extern const Il2CppGenericInst GenInst_IAndroidStoreSelection_t3134941501_0_0_0 = { 1, GenInst_IAndroidStoreSelection_t3134941501_0_0_0_Types };
extern const Il2CppType IMoolahConfiguration_t3241385415_0_0_0;
static const Il2CppType* GenInst_IMoolahConfiguration_t3241385415_0_0_0_Types[] = { &IMoolahConfiguration_t3241385415_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoolahConfiguration_t3241385415_0_0_0 = { 1, GenInst_IMoolahConfiguration_t3241385415_0_0_0_Types };
extern const Il2CppType IMoolahExtension_t3195861654_0_0_0;
static const Il2CppType* GenInst_IMoolahExtension_t3195861654_0_0_0_Types[] = { &IMoolahExtension_t3195861654_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoolahExtension_t3195861654_0_0_0 = { 1, GenInst_IMoolahExtension_t3195861654_0_0_0_Types };
extern const Il2CppType IMicrosoftExtensions_t1101930285_0_0_0;
static const Il2CppType* GenInst_IMicrosoftExtensions_t1101930285_0_0_0_Types[] = { &IMicrosoftExtensions_t1101930285_0_0_0 };
extern const Il2CppGenericInst GenInst_IMicrosoftExtensions_t1101930285_0_0_0 = { 1, GenInst_IMicrosoftExtensions_t1101930285_0_0_0_Types };
extern const Il2CppType MoolahStoreImpl_t4206626141_0_0_0;
static const Il2CppType* GenInst_MoolahStoreImpl_t4206626141_0_0_0_Types[] = { &MoolahStoreImpl_t4206626141_0_0_0 };
extern const Il2CppGenericInst GenInst_MoolahStoreImpl_t4206626141_0_0_0 = { 1, GenInst_MoolahStoreImpl_t4206626141_0_0_0_Types };
extern const Il2CppType LifecycleNotifier_t1057582876_0_0_0;
static const Il2CppType* GenInst_LifecycleNotifier_t1057582876_0_0_0_Types[] = { &LifecycleNotifier_t1057582876_0_0_0 };
extern const Il2CppGenericInst GenInst_LifecycleNotifier_t1057582876_0_0_0 = { 1, GenInst_LifecycleNotifier_t1057582876_0_0_0_Types };
extern const Il2CppType StandaloneInputModule_t70867863_0_0_0;
static const Il2CppType* GenInst_StandaloneInputModule_t70867863_0_0_0_Types[] = { &StandaloneInputModule_t70867863_0_0_0 };
extern const Il2CppGenericInst GenInst_StandaloneInputModule_t70867863_0_0_0 = { 1, GenInst_StandaloneInputModule_t70867863_0_0_0_Types };
extern const Il2CppType GlobalConfig_t3080413471_0_0_0;
static const Il2CppType* GenInst_GlobalConfig_t3080413471_0_0_0_Types[] = { &GlobalConfig_t3080413471_0_0_0 };
extern const Il2CppGenericInst GenInst_GlobalConfig_t3080413471_0_0_0 = { 1, GenInst_GlobalConfig_t3080413471_0_0_0_Types };
extern const Il2CppType ApiConnector_t2569785041_0_0_0;
static const Il2CppType* GenInst_ApiConnector_t2569785041_0_0_0_Types[] = { &ApiConnector_t2569785041_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiConnector_t2569785041_0_0_0 = { 1, GenInst_ApiConnector_t2569785041_0_0_0_Types };
extern const Il2CppType TextAsset_t3973159845_0_0_0;
static const Il2CppType* GenInst_TextAsset_t3973159845_0_0_0_Types[] = { &TextAsset_t3973159845_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAsset_t3973159845_0_0_0 = { 1, GenInst_TextAsset_t3973159845_0_0_0_Types };
extern const Il2CppType AssetBundleManager_t364944953_0_0_0;
static const Il2CppType* GenInst_AssetBundleManager_t364944953_0_0_0_Types[] = { &AssetBundleManager_t364944953_0_0_0 };
extern const Il2CppGenericInst GenInst_AssetBundleManager_t364944953_0_0_0 = { 1, GenInst_AssetBundleManager_t364944953_0_0_0_Types };
extern const Il2CppType VaeBuildSetting_t3353517300_0_0_0;
static const Il2CppType* GenInst_VaeBuildSetting_t3353517300_0_0_0_Types[] = { &VaeBuildSetting_t3353517300_0_0_0 };
extern const Il2CppGenericInst GenInst_VaeBuildSetting_t3353517300_0_0_0 = { 1, GenInst_VaeBuildSetting_t3353517300_0_0_0_Types };
extern const Il2CppType Recorder_Instance_t330262942_0_0_0;
static const Il2CppType* GenInst_Recorder_Instance_t330262942_0_0_0_Types[] = { &Recorder_Instance_t330262942_0_0_0 };
extern const Il2CppGenericInst GenInst_Recorder_Instance_t330262942_0_0_0 = { 1, GenInst_Recorder_Instance_t330262942_0_0_0_Types };
extern const Il2CppType AudioMixer_t3244290001_0_0_0;
static const Il2CppType* GenInst_AudioMixer_t3244290001_0_0_0_Types[] = { &AudioMixer_t3244290001_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioMixer_t3244290001_0_0_0 = { 1, GenInst_AudioMixer_t3244290001_0_0_0_Types };
extern const Il2CppType InputSpectrum_t3811795451_0_0_0;
static const Il2CppType* GenInst_InputSpectrum_t3811795451_0_0_0_Types[] = { &InputSpectrum_t3811795451_0_0_0 };
extern const Il2CppGenericInst GenInst_InputSpectrum_t3811795451_0_0_0 = { 1, GenInst_InputSpectrum_t3811795451_0_0_0_Types };
extern const Il2CppType VRHandleButton_t3511644738_0_0_0;
static const Il2CppType* GenInst_VRHandleButton_t3511644738_0_0_0_Types[] = { &VRHandleButton_t3511644738_0_0_0 };
extern const Il2CppGenericInst GenInst_VRHandleButton_t3511644738_0_0_0 = { 1, GenInst_VRHandleButton_t3511644738_0_0_0_Types };
extern const Il2CppType BaseChapterSettingIniter_t2856709253_0_0_0;
static const Il2CppType* GenInst_BaseChapterSettingIniter_t2856709253_0_0_0_Types[] = { &BaseChapterSettingIniter_t2856709253_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseChapterSettingIniter_t2856709253_0_0_0 = { 1, GenInst_BaseChapterSettingIniter_t2856709253_0_0_0_Types };
extern const Il2CppType Purchaser_t1674559779_0_0_0;
static const Il2CppType* GenInst_Purchaser_t1674559779_0_0_0_Types[] = { &Purchaser_t1674559779_0_0_0 };
extern const Il2CppGenericInst GenInst_Purchaser_t1674559779_0_0_0 = { 1, GenInst_Purchaser_t1674559779_0_0_0_Types };
extern const Il2CppType ManagerInstance_t2189493790_0_0_0;
static const Il2CppType* GenInst_ManagerInstance_t2189493790_0_0_0_Types[] = { &ManagerInstance_t2189493790_0_0_0 };
extern const Il2CppGenericInst GenInst_ManagerInstance_t2189493790_0_0_0 = { 1, GenInst_ManagerInstance_t2189493790_0_0_0_Types };
extern const Il2CppType ChapterTopConfig_t174003822_0_0_0;
static const Il2CppType* GenInst_ChapterTopConfig_t174003822_0_0_0_Types[] = { &ChapterTopConfig_t174003822_0_0_0 };
extern const Il2CppGenericInst GenInst_ChapterTopConfig_t174003822_0_0_0 = { 1, GenInst_ChapterTopConfig_t174003822_0_0_0_Types };
extern const Il2CppType ChapterLearningItemView_t1984184315_0_0_0;
static const Il2CppType* GenInst_ChapterLearningItemView_t1984184315_0_0_0_Types[] = { &ChapterLearningItemView_t1984184315_0_0_0 };
extern const Il2CppGenericInst GenInst_ChapterLearningItemView_t1984184315_0_0_0 = { 1, GenInst_ChapterLearningItemView_t1984184315_0_0_0_Types };
extern const Il2CppType ColorSettingAsset_t2075258157_0_0_0;
static const Il2CppType* GenInst_ColorSettingAsset_t2075258157_0_0_0_Types[] = { &ColorSettingAsset_t2075258157_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorSettingAsset_t2075258157_0_0_0 = { 1, GenInst_ColorSettingAsset_t2075258157_0_0_0_Types };
extern const Il2CppType ChapterResultSettingLoader_t4141859061_0_0_0;
static const Il2CppType* GenInst_ChapterResultSettingLoader_t4141859061_0_0_0_Types[] = { &ChapterResultSettingLoader_t4141859061_0_0_0 };
extern const Il2CppGenericInst GenInst_ChapterResultSettingLoader_t4141859061_0_0_0 = { 1, GenInst_ChapterResultSettingLoader_t4141859061_0_0_0_Types };
extern const Il2CppType ButtonSound_t506092895_0_0_0;
static const Il2CppType* GenInst_ButtonSound_t506092895_0_0_0_Types[] = { &ButtonSound_t506092895_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonSound_t506092895_0_0_0 = { 1, GenInst_ButtonSound_t506092895_0_0_0_Types };
extern const Il2CppType PopupOpener_t1646050995_0_0_0;
static const Il2CppType* GenInst_PopupOpener_t1646050995_0_0_0_Types[] = { &PopupOpener_t1646050995_0_0_0 };
extern const Il2CppGenericInst GenInst_PopupOpener_t1646050995_0_0_0 = { 1, GenInst_PopupOpener_t1646050995_0_0_0_Types };
extern const Il2CppType Animation_t2068071072_0_0_0;
static const Il2CppType* GenInst_Animation_t2068071072_0_0_0_Types[] = { &Animation_t2068071072_0_0_0 };
extern const Il2CppGenericInst GenInst_Animation_t2068071072_0_0_0 = { 1, GenInst_Animation_t2068071072_0_0_0_Types };
extern const Il2CppType SoundManager_t1001218626_0_0_0;
static const Il2CppType* GenInst_SoundManager_t1001218626_0_0_0_Types[] = { &SoundManager_t1001218626_0_0_0 };
extern const Il2CppGenericInst GenInst_SoundManager_t1001218626_0_0_0 = { 1, GenInst_SoundManager_t1001218626_0_0_0_Types };
extern const Il2CppType SoundManager_t695211022_0_0_0;
static const Il2CppType* GenInst_SoundManager_t695211022_0_0_0_Types[] = { &SoundManager_t695211022_0_0_0 };
extern const Il2CppGenericInst GenInst_SoundManager_t695211022_0_0_0 = { 1, GenInst_SoundManager_t695211022_0_0_0_Types };
extern const Il2CppType CommandSheet_t2769384292_0_0_0;
static const Il2CppType* GenInst_CommandSheet_t2769384292_0_0_0_Types[] = { &CommandSheet_t2769384292_0_0_0 };
extern const Il2CppGenericInst GenInst_CommandSheet_t2769384292_0_0_0 = { 1, GenInst_CommandSheet_t2769384292_0_0_0_Types };
extern const Il2CppType ConversationSelectionTextView_t4281204247_0_0_0;
static const Il2CppType* GenInst_ConversationSelectionTextView_t4281204247_0_0_0_Types[] = { &ConversationSelectionTextView_t4281204247_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationSelectionTextView_t4281204247_0_0_0 = { 1, GenInst_ConversationSelectionTextView_t4281204247_0_0_0_Types };
extern const Il2CppType ConversationSelectionButtonView_t265815670_0_0_0;
static const Il2CppType* GenInst_ConversationSelectionButtonView_t265815670_0_0_0_Types[] = { &ConversationSelectionButtonView_t265815670_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationSelectionButtonView_t265815670_0_0_0 = { 1, GenInst_ConversationSelectionButtonView_t265815670_0_0_0_Types };
extern const Il2CppType VRConversationSelectionButtonView_t1568012346_0_0_0;
static const Il2CppType* GenInst_VRConversationSelectionButtonView_t1568012346_0_0_0_Types[] = { &VRConversationSelectionButtonView_t1568012346_0_0_0 };
extern const Il2CppGenericInst GenInst_VRConversationSelectionButtonView_t1568012346_0_0_0 = { 1, GenInst_VRConversationSelectionButtonView_t1568012346_0_0_0_Types };
extern const Il2CppType DataStorage_t2091384907_0_0_0;
static const Il2CppType* GenInst_DataStorage_t2091384907_0_0_0_Types[] = { &DataStorage_t2091384907_0_0_0 };
extern const Il2CppGenericInst GenInst_DataStorage_t2091384907_0_0_0 = { 1, GenInst_DataStorage_t2091384907_0_0_0_Types };
extern const Il2CppType DisableWithTimeCountdown_t622718188_0_0_0;
static const Il2CppType* GenInst_DisableWithTimeCountdown_t622718188_0_0_0_Types[] = { &DisableWithTimeCountdown_t622718188_0_0_0 };
extern const Il2CppGenericInst GenInst_DisableWithTimeCountdown_t622718188_0_0_0 = { 1, GenInst_DisableWithTimeCountdown_t622718188_0_0_0_Types };
extern const Il2CppType TextKaraoke_t2450194175_0_0_0;
static const Il2CppType* GenInst_TextKaraoke_t2450194175_0_0_0_Types[] = { &TextKaraoke_t2450194175_0_0_0 };
extern const Il2CppGenericInst GenInst_TextKaraoke_t2450194175_0_0_0 = { 1, GenInst_TextKaraoke_t2450194175_0_0_0_Types };
extern const Il2CppType SphereCollider_t1662511355_0_0_0;
static const Il2CppType* GenInst_SphereCollider_t1662511355_0_0_0_Types[] = { &SphereCollider_t1662511355_0_0_0 };
extern const Il2CppGenericInst GenInst_SphereCollider_t1662511355_0_0_0 = { 1, GenInst_SphereCollider_t1662511355_0_0_0_Types };
extern const Il2CppType EmulatorManager_t3364249716_0_0_0;
static const Il2CppType* GenInst_EmulatorManager_t3364249716_0_0_0_Types[] = { &EmulatorManager_t3364249716_0_0_0 };
extern const Il2CppGenericInst GenInst_EmulatorManager_t3364249716_0_0_0 = { 1, GenInst_EmulatorManager_t3364249716_0_0_0_Types };
extern const Il2CppType EmulatorClientSocket_t2001911543_0_0_0;
static const Il2CppType* GenInst_EmulatorClientSocket_t2001911543_0_0_0_Types[] = { &EmulatorClientSocket_t2001911543_0_0_0 };
extern const Il2CppGenericInst GenInst_EmulatorClientSocket_t2001911543_0_0_0 = { 1, GenInst_EmulatorClientSocket_t2001911543_0_0_0_Types };
extern const Il2CppType GvrAudioListener_t1521766837_0_0_0;
static const Il2CppType* GenInst_GvrAudioListener_t1521766837_0_0_0_Types[] = { &GvrAudioListener_t1521766837_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrAudioListener_t1521766837_0_0_0 = { 1, GenInst_GvrAudioListener_t1521766837_0_0_0_Types };
extern const Il2CppType GvrPointerGraphicRaycaster_t1649506702_0_0_0;
static const Il2CppType* GenInst_GvrPointerGraphicRaycaster_t1649506702_0_0_0_Types[] = { &GvrPointerGraphicRaycaster_t1649506702_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrPointerGraphicRaycaster_t1649506702_0_0_0 = { 1, GenInst_GvrPointerGraphicRaycaster_t1649506702_0_0_0_Types };
extern const Il2CppType StereoController_t3144380552_0_0_0;
static const Il2CppType* GenInst_StereoController_t3144380552_0_0_0_Types[] = { &StereoController_t3144380552_0_0_0 };
extern const Il2CppGenericInst GenInst_StereoController_t3144380552_0_0_0 = { 1, GenInst_StereoController_t3144380552_0_0_0_Types };
extern const Il2CppType StereoRenderEffect_t958489249_0_0_0;
static const Il2CppType* GenInst_StereoRenderEffect_t958489249_0_0_0_Types[] = { &StereoRenderEffect_t958489249_0_0_0 };
extern const Il2CppGenericInst GenInst_StereoRenderEffect_t958489249_0_0_0 = { 1, GenInst_StereoRenderEffect_t958489249_0_0_0_Types };
extern const Il2CppType Skybox_t2033495038_0_0_0;
static const Il2CppType* GenInst_Skybox_t2033495038_0_0_0_Types[] = { &Skybox_t2033495038_0_0_0 };
extern const Il2CppGenericInst GenInst_Skybox_t2033495038_0_0_0 = { 1, GenInst_Skybox_t2033495038_0_0_0_Types };
extern const Il2CppType Collider_t3497673348_0_0_0;
static const Il2CppType* GenInst_Collider_t3497673348_0_0_0_Types[] = { &Collider_t3497673348_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t3497673348_0_0_0 = { 1, GenInst_Collider_t3497673348_0_0_0_Types };
extern const Il2CppType Renderer_t257310565_0_0_0;
static const Il2CppType* GenInst_Renderer_t257310565_0_0_0_Types[] = { &Renderer_t257310565_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t257310565_0_0_0 = { 1, GenInst_Renderer_t257310565_0_0_0_Types };
extern const Il2CppType MeshFilter_t3026937449_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t3026937449_0_0_0_Types[] = { &MeshFilter_t3026937449_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t3026937449_0_0_0 = { 1, GenInst_MeshFilter_t3026937449_0_0_0_Types };
extern const Il2CppType GvrVideoPlayerTexture_t673526704_0_0_0;
static const Il2CppType* GenInst_GvrVideoPlayerTexture_t673526704_0_0_0_Types[] = { &GvrVideoPlayerTexture_t673526704_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrVideoPlayerTexture_t673526704_0_0_0 = { 1, GenInst_GvrVideoPlayerTexture_t673526704_0_0_0_Types };
extern const Il2CppType GvrViewer_t2583885279_0_0_0;
static const Il2CppType* GenInst_GvrViewer_t2583885279_0_0_0_Types[] = { &GvrViewer_t2583885279_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrViewer_t2583885279_0_0_0 = { 1, GenInst_GvrViewer_t2583885279_0_0_0_Types };
extern const Il2CppType GvrPreRender_t2074710158_0_0_0;
static const Il2CppType* GenInst_GvrPreRender_t2074710158_0_0_0_Types[] = { &GvrPreRender_t2074710158_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrPreRender_t2074710158_0_0_0 = { 1, GenInst_GvrPreRender_t2074710158_0_0_0_Types };
extern const Il2CppType GvrPostRender_t3118402863_0_0_0;
static const Il2CppType* GenInst_GvrPostRender_t3118402863_0_0_0_Types[] = { &GvrPostRender_t3118402863_0_0_0 };
extern const Il2CppGenericInst GenInst_GvrPostRender_t3118402863_0_0_0 = { 1, GenInst_GvrPostRender_t3118402863_0_0_0_Types };
extern const Il2CppType HelpPopup_t3079166541_0_0_0;
static const Il2CppType* GenInst_HelpPopup_t3079166541_0_0_0_Types[] = { &HelpPopup_t3079166541_0_0_0 };
extern const Il2CppGenericInst GenInst_HelpPopup_t3079166541_0_0_0 = { 1, GenInst_HelpPopup_t3079166541_0_0_0_Types };
extern const Il2CppType InvokerModule_t4256524346_0_0_0;
static const Il2CppType* GenInst_InvokerModule_t4256524346_0_0_0_Types[] = { &InvokerModule_t4256524346_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokerModule_t4256524346_0_0_0 = { 1, GenInst_InvokerModule_t4256524346_0_0_0_Types };
extern const Il2CppType IAP_UIScript_t1776166942_0_0_0;
static const Il2CppType* GenInst_IAP_UIScript_t1776166942_0_0_0_Types[] = { &IAP_UIScript_t1776166942_0_0_0 };
extern const Il2CppGenericInst GenInst_IAP_UIScript_t1776166942_0_0_0 = { 1, GenInst_IAP_UIScript_t1776166942_0_0_0_Types };
extern const Il2CppType HomeCotroller_t2470068823_0_0_0;
static const Il2CppType* GenInst_HomeCotroller_t2470068823_0_0_0_Types[] = { &HomeCotroller_t2470068823_0_0_0 };
extern const Il2CppGenericInst GenInst_HomeCotroller_t2470068823_0_0_0 = { 1, GenInst_HomeCotroller_t2470068823_0_0_0_Types };
extern const Il2CppType SettingBasedKaraokeTextColor_t3573755485_0_0_0;
static const Il2CppType* GenInst_SettingBasedKaraokeTextColor_t3573755485_0_0_0_Types[] = { &SettingBasedKaraokeTextColor_t3573755485_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingBasedKaraokeTextColor_t3573755485_0_0_0 = { 1, GenInst_SettingBasedKaraokeTextColor_t3573755485_0_0_0_Types };
extern const Il2CppType Listening1QuestionView_t2345876901_0_0_0;
static const Il2CppType* GenInst_Listening1QuestionView_t2345876901_0_0_0_Types[] = { &Listening1QuestionView_t2345876901_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening1QuestionView_t2345876901_0_0_0 = { 1, GenInst_Listening1QuestionView_t2345876901_0_0_0_Types };
extern const Il2CppType Listening1AnswerHelper_t2804121578_0_0_0;
static const Il2CppType* GenInst_Listening1AnswerHelper_t2804121578_0_0_0_Types[] = { &Listening1AnswerHelper_t2804121578_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening1AnswerHelper_t2804121578_0_0_0 = { 1, GenInst_Listening1AnswerHelper_t2804121578_0_0_0_Types };
extern const Il2CppType Listening2QuestionView_t3734381824_0_0_0;
static const Il2CppType* GenInst_Listening2QuestionView_t3734381824_0_0_0_Types[] = { &Listening2QuestionView_t3734381824_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening2QuestionView_t3734381824_0_0_0 = { 1, GenInst_Listening2QuestionView_t3734381824_0_0_0_Types };
extern const Il2CppType Listening2AnswerHelper_t4121473615_0_0_0;
static const Il2CppType* GenInst_Listening2AnswerHelper_t4121473615_0_0_0_Types[] = { &Listening2AnswerHelper_t4121473615_0_0_0 };
extern const Il2CppGenericInst GenInst_Listening2AnswerHelper_t4121473615_0_0_0 = { 1, GenInst_Listening2AnswerHelper_t4121473615_0_0_0_Types };
extern const Il2CppType ResultStatisticController_t2929782681_0_0_0;
static const Il2CppType* GenInst_ResultStatisticController_t2929782681_0_0_0_Types[] = { &ResultStatisticController_t2929782681_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultStatisticController_t2929782681_0_0_0 = { 1, GenInst_ResultStatisticController_t2929782681_0_0_0_Types };
extern const Il2CppType SceneAndScreenHelper_t3943722385_0_0_0;
static const Il2CppType* GenInst_SceneAndScreenHelper_t3943722385_0_0_0_Types[] = { &SceneAndScreenHelper_t3943722385_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneAndScreenHelper_t3943722385_0_0_0 = { 1, GenInst_SceneAndScreenHelper_t3943722385_0_0_0_Types };
extern const Il2CppType TrackShowHelpAtFistTimeScript_t2844513732_0_0_0;
static const Il2CppType* GenInst_TrackShowHelpAtFistTimeScript_t2844513732_0_0_0_Types[] = { &TrackShowHelpAtFistTimeScript_t2844513732_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackShowHelpAtFistTimeScript_t2844513732_0_0_0 = { 1, GenInst_TrackShowHelpAtFistTimeScript_t2844513732_0_0_0_Types };
extern const Il2CppType CharacterScript_t1308706256_0_0_0;
static const Il2CppType* GenInst_CharacterScript_t1308706256_0_0_0_Types[] = { &CharacterScript_t1308706256_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterScript_t1308706256_0_0_0 = { 1, GenInst_CharacterScript_t1308706256_0_0_0_Types };
extern const Il2CppType ChapterSelector_t191193704_0_0_0;
static const Il2CppType* GenInst_ChapterSelector_t191193704_0_0_0_Types[] = { &ChapterSelector_t191193704_0_0_0 };
extern const Il2CppGenericInst GenInst_ChapterSelector_t191193704_0_0_0 = { 1, GenInst_ChapterSelector_t191193704_0_0_0_Types };
extern const Il2CppType PlayerTalkModule_t3356839145_0_0_0;
static const Il2CppType* GenInst_PlayerTalkModule_t3356839145_0_0_0_Types[] = { &PlayerTalkModule_t3356839145_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerTalkModule_t3356839145_0_0_0 = { 1, GenInst_PlayerTalkModule_t3356839145_0_0_0_Types };
extern const Il2CppType OtherTalkModule_t2158105276_0_0_0;
static const Il2CppType* GenInst_OtherTalkModule_t2158105276_0_0_0_Types[] = { &OtherTalkModule_t2158105276_0_0_0 };
extern const Il2CppGenericInst GenInst_OtherTalkModule_t2158105276_0_0_0 = { 1, GenInst_OtherTalkModule_t2158105276_0_0_0_Types };
extern const Il2CppType PopupScript_t535420507_0_0_0;
static const Il2CppType* GenInst_PopupScript_t535420507_0_0_0_Types[] = { &PopupScript_t535420507_0_0_0 };
extern const Il2CppGenericInst GenInst_PopupScript_t535420507_0_0_0 = { 1, GenInst_PopupScript_t535420507_0_0_0_Types };
extern const Il2CppType GUISkin_t1436893342_0_0_0;
static const Il2CppType* GenInst_GUISkin_t1436893342_0_0_0_Types[] = { &GUISkin_t1436893342_0_0_0 };
extern const Il2CppGenericInst GenInst_GUISkin_t1436893342_0_0_0 = { 1, GenInst_GUISkin_t1436893342_0_0_0_Types };
extern const Il2CppType ReporterGUI_t402918452_0_0_0;
static const Il2CppType* GenInst_ReporterGUI_t402918452_0_0_0_Types[] = { &ReporterGUI_t402918452_0_0_0 };
extern const Il2CppGenericInst GenInst_ReporterGUI_t402918452_0_0_0 = { 1, GenInst_ReporterGUI_t402918452_0_0_0_Types };
extern const Il2CppType Reporter_t3561640551_0_0_0;
static const Il2CppType* GenInst_Reporter_t3561640551_0_0_0_Types[] = { &Reporter_t3561640551_0_0_0 };
extern const Il2CppGenericInst GenInst_Reporter_t3561640551_0_0_0 = { 1, GenInst_Reporter_t3561640551_0_0_0_Types };
extern const Il2CppType ResultSaver_t3290295394_0_0_0;
static const Il2CppType* GenInst_ResultSaver_t3290295394_0_0_0_Types[] = { &ResultSaver_t3290295394_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultSaver_t3290295394_0_0_0 = { 1, GenInst_ResultSaver_t3290295394_0_0_0_Types };
extern const Il2CppType SceneController_t38942716_0_0_0;
static const Il2CppType* GenInst_SceneController_t38942716_0_0_0_Types[] = { &SceneController_t38942716_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneController_t38942716_0_0_0 = { 1, GenInst_SceneController_t38942716_0_0_0_Types };
extern const Il2CppType VocabularyController_t843712768_0_0_0;
static const Il2CppType* GenInst_VocabularyController_t843712768_0_0_0_Types[] = { &VocabularyController_t843712768_0_0_0 };
extern const Il2CppGenericInst GenInst_VocabularyController_t843712768_0_0_0 = { 1, GenInst_VocabularyController_t843712768_0_0_0_Types };
extern const Il2CppType Sentence2QuestionView_t1942347884_0_0_0;
static const Il2CppType* GenInst_Sentence2QuestionView_t1942347884_0_0_0_Types[] = { &Sentence2QuestionView_t1942347884_0_0_0 };
extern const Il2CppGenericInst GenInst_Sentence2QuestionView_t1942347884_0_0_0 = { 1, GenInst_Sentence2QuestionView_t1942347884_0_0_0_Types };
extern const Il2CppType SentenceQuestionView_t1478559372_0_0_0;
static const Il2CppType* GenInst_SentenceQuestionView_t1478559372_0_0_0_Types[] = { &SentenceQuestionView_t1478559372_0_0_0 };
extern const Il2CppGenericInst GenInst_SentenceQuestionView_t1478559372_0_0_0 = { 1, GenInst_SentenceQuestionView_t1478559372_0_0_0_Types };
extern const Il2CppType KaraokeTextEffect_t1279919556_0_0_0;
static const Il2CppType* GenInst_KaraokeTextEffect_t1279919556_0_0_0_Types[] = { &KaraokeTextEffect_t1279919556_0_0_0 };
extern const Il2CppGenericInst GenInst_KaraokeTextEffect_t1279919556_0_0_0 = { 1, GenInst_KaraokeTextEffect_t1279919556_0_0_0_Types };
extern const Il2CppType VRConversationSelectionView_t764630138_0_0_0;
static const Il2CppType* GenInst_VRConversationSelectionView_t764630138_0_0_0_Types[] = { &VRConversationSelectionView_t764630138_0_0_0 };
extern const Il2CppGenericInst GenInst_VRConversationSelectionView_t764630138_0_0_0 = { 1, GenInst_VRConversationSelectionView_t764630138_0_0_0_Types };
extern const Il2CppType IconSoundAnimation_t302426630_0_0_0;
static const Il2CppType* GenInst_IconSoundAnimation_t302426630_0_0_0_Types[] = { &IconSoundAnimation_t302426630_0_0_0 };
extern const Il2CppGenericInst GenInst_IconSoundAnimation_t302426630_0_0_0 = { 1, GenInst_IconSoundAnimation_t302426630_0_0_0_Types };
extern const Il2CppType CallHelpInSceneScript_t2709929151_0_0_0;
static const Il2CppType* GenInst_CallHelpInSceneScript_t2709929151_0_0_0_Types[] = { &CallHelpInSceneScript_t2709929151_0_0_0 };
extern const Il2CppGenericInst GenInst_CallHelpInSceneScript_t2709929151_0_0_0 = { 1, GenInst_CallHelpInSceneScript_t2709929151_0_0_0_Types };
extern const Il2CppType ConversationLogicController_t3911886281_0_0_0;
static const Il2CppType* GenInst_ConversationLogicController_t3911886281_0_0_0_Types[] = { &ConversationLogicController_t3911886281_0_0_0 };
extern const Il2CppGenericInst GenInst_ConversationLogicController_t3911886281_0_0_0 = { 1, GenInst_ConversationLogicController_t3911886281_0_0_0_Types };
extern const Il2CppType ResetData_t4114871043_0_0_0;
static const Il2CppType* GenInst_ResetData_t4114871043_0_0_0_Types[] = { &ResetData_t4114871043_0_0_0 };
extern const Il2CppGenericInst GenInst_ResetData_t4114871043_0_0_0 = { 1, GenInst_ResetData_t4114871043_0_0_0_Types };
extern const Il2CppType SelectionController_t1038360966_0_0_0;
static const Il2CppType* GenInst_SelectionController_t1038360966_0_0_0_Types[] = { &SelectionController_t1038360966_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectionController_t1038360966_0_0_0 = { 1, GenInst_SelectionController_t1038360966_0_0_0_Types };
extern const Il2CppType DialogController_t3845653284_0_0_0;
static const Il2CppType* GenInst_DialogController_t3845653284_0_0_0_Types[] = { &DialogController_t3845653284_0_0_0 };
extern const Il2CppGenericInst GenInst_DialogController_t3845653284_0_0_0 = { 1, GenInst_DialogController_t3845653284_0_0_0_Types };
extern const Il2CppType ResultRateController_t2087696941_0_0_0;
static const Il2CppType* GenInst_ResultRateController_t2087696941_0_0_0_Types[] = { &ResultRateController_t2087696941_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultRateController_t2087696941_0_0_0 = { 1, GenInst_ResultRateController_t2087696941_0_0_0_Types };
extern const Il2CppType VideoControlsManager_t3010523296_0_0_0;
static const Il2CppType* GenInst_VideoControlsManager_t3010523296_0_0_0_Types[] = { &VideoControlsManager_t3010523296_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoControlsManager_t3010523296_0_0_0 = { 1, GenInst_VideoControlsManager_t3010523296_0_0_0_Types };
extern const Il2CppType PopupController_t681110_0_0_0;
static const Il2CppType* GenInst_PopupController_t681110_0_0_0_Types[] = { &PopupController_t681110_0_0_0 };
extern const Il2CppGenericInst GenInst_PopupController_t681110_0_0_0 = { 1, GenInst_PopupController_t681110_0_0_0_Types };
extern const Il2CppType VocabularyQuestionView_t1631345783_0_0_0;
static const Il2CppType* GenInst_VocabularyQuestionView_t1631345783_0_0_0_Types[] = { &VocabularyQuestionView_t1631345783_0_0_0 };
extern const Il2CppGenericInst GenInst_VocabularyQuestionView_t1631345783_0_0_0 = { 1, GenInst_VocabularyQuestionView_t1631345783_0_0_0_Types };
extern const Il2CppType BaseConversationController_t808907754_0_0_0;
static const Il2CppType* GenInst_BaseConversationController_t808907754_0_0_0_Types[] = { &BaseConversationController_t808907754_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseConversationController_t808907754_0_0_0 = { 1, GenInst_BaseConversationController_t808907754_0_0_0_Types };
static const Il2CppType* GenInst_PurchaseSession_t2088054391_0_0_0_PurchaseSession_t2088054391_0_0_0_Types[] = { &PurchaseSession_t2088054391_0_0_0, &PurchaseSession_t2088054391_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchaseSession_t2088054391_0_0_0_PurchaseSession_t2088054391_0_0_0 = { 2, GenInst_PurchaseSession_t2088054391_0_0_0_PurchaseSession_t2088054391_0_0_0_Types };
static const Il2CppType* GenInst_Pointer_t3000685002_0_0_0_Pointer_t3000685002_0_0_0_Types[] = { &Pointer_t3000685002_0_0_0, &Pointer_t3000685002_0_0_0 };
extern const Il2CppGenericInst GenInst_Pointer_t3000685002_0_0_0_Pointer_t3000685002_0_0_0 = { 2, GenInst_Pointer_t3000685002_0_0_0_Pointer_t3000685002_0_0_0_Types };
static const Il2CppType* GenInst_Type_t1530480861_0_0_0_Type_t1530480861_0_0_0_Types[] = { &Type_t1530480861_0_0_0, &Type_t1530480861_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t1530480861_0_0_0_Type_t1530480861_0_0_0 = { 2, GenInst_Type_t1530480861_0_0_0_Type_t1530480861_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0, &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0, &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0, &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0, &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0, &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0 = { 2, GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types };
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0, &RuntimePlatform_t1869584967_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0 = { 2, GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0, &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0, &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0, &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
static const Il2CppType* GenInst_ExtensionIntPair_t3093161221_0_0_0_ExtensionIntPair_t3093161221_0_0_0_Types[] = { &ExtensionIntPair_t3093161221_0_0_0, &ExtensionIntPair_t3093161221_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtensionIntPair_t3093161221_0_0_0_ExtensionIntPair_t3093161221_0_0_0 = { 2, GenInst_ExtensionIntPair_t3093161221_0_0_0_ExtensionIntPair_t3093161221_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2164262055_0_0_0_KeyValuePair_2_t2164262055_0_0_0_Types[] = { &KeyValuePair_2_t2164262055_0_0_0, &KeyValuePair_2_t2164262055_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2164262055_0_0_0_KeyValuePair_2_t2164262055_0_0_0 = { 2, GenInst_KeyValuePair_2_t2164262055_0_0_0_KeyValuePair_2_t2164262055_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2164262055_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2164262055_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2164262055_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2164262055_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Type_t1530480861_0_0_0_Il2CppObject_0_0_0_Types[] = { &Type_t1530480861_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t1530480861_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Type_t1530480861_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2590619014_0_0_0_KeyValuePair_2_t2590619014_0_0_0_Types[] = { &KeyValuePair_2_t2590619014_0_0_0, &KeyValuePair_2_t2590619014_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2590619014_0_0_0_KeyValuePair_2_t2590619014_0_0_0 = { 2, GenInst_KeyValuePair_2_t2590619014_0_0_0_KeyValuePair_2_t2590619014_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2590619014_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2590619014_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2590619014_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2590619014_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1683227291_0_0_0_KeyValuePair_2_t1683227291_0_0_0_Types[] = { &KeyValuePair_2_t1683227291_0_0_0, &KeyValuePair_2_t1683227291_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1683227291_0_0_0_KeyValuePair_2_t1683227291_0_0_0 = { 2, GenInst_KeyValuePair_2_t1683227291_0_0_0_KeyValuePair_2_t1683227291_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1683227291_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1683227291_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1683227291_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1683227291_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t566713506_0_0_0_KeyValuePair_2_t566713506_0_0_0_Types[] = { &KeyValuePair_2_t566713506_0_0_0, &KeyValuePair_2_t566713506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t566713506_0_0_0_KeyValuePair_2_t566713506_0_0_0 = { 2, GenInst_KeyValuePair_2_t566713506_0_0_0_KeyValuePair_2_t566713506_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t566713506_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t566713506_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t566713506_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t566713506_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_AndroidStore_t3203055206_0_0_0_AndroidStore_t3203055206_0_0_0_Types[] = { &AndroidStore_t3203055206_0_0_0, &AndroidStore_t3203055206_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidStore_t3203055206_0_0_0_AndroidStore_t3203055206_0_0_0 = { 2, GenInst_AndroidStore_t3203055206_0_0_0_AndroidStore_t3203055206_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1056] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IComparable_1_t991353265_0_0_0,
	&GenInst_IEquatable_1_t1363496211_0_0_0,
	&GenInst_ValueType_t3507792607_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_IFormattable_t1523031934_0_0_0,
	&GenInst_IComparable_1_t3903716671_0_0_0,
	&GenInst_IEquatable_1_t4275859617_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_IComparable_1_t1614887608_0_0_0,
	&GenInst_IEquatable_1_t1987030554_0_0_0,
	&GenInst_IComparable_1_t3981521244_0_0_0,
	&GenInst_IEquatable_1_t58696894_0_0_0,
	&GenInst_IComparable_1_t1219976363_0_0_0,
	&GenInst_IEquatable_1_t1592119309_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_IComparable_1_t3908349155_0_0_0,
	&GenInst_IEquatable_1_t4280492101_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_IComparable_1_t2818721834_0_0_0,
	&GenInst_IEquatable_1_t3190864780_0_0_0,
	&GenInst_IComparable_1_t446068841_0_0_0,
	&GenInst_IEquatable_1_t818211787_0_0_0,
	&GenInst_IComparable_1_t1578117841_0_0_0,
	&GenInst_IEquatable_1_t1950260787_0_0_0,
	&GenInst_IComparable_1_t2286256772_0_0_0,
	&GenInst_IEquatable_1_t2658399718_0_0_0,
	&GenInst_IComparable_1_t2740917260_0_0_0,
	&GenInst_IEquatable_1_t3113060206_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_IComparable_1_t1362446645_0_0_0,
	&GenInst_IEquatable_1_t1734589591_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_IComparable_1_t2525044892_0_0_0,
	&GenInst_IEquatable_1_t2897187838_0_0_0,
	&GenInst_IComparable_1_t2556540300_0_0_0,
	&GenInst_IEquatable_1_t2928683246_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_IComparable_1_t967130876_0_0_0,
	&GenInst_IEquatable_1_t1339273822_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_Enum_t2459695545_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_WaitHandle_t677569169_0_0_0,
	&GenInst_IDisposable_t2427283555_0_0_0,
	&GenInst_MarshalByRefObject_t1285298191_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_Guid_t2533601593_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_Cookie_t3154017544_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_jvalue_t3412352577_0_0_0,
	&GenInst_AndroidJavaObject_t4251328308_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_Touch_t407273883_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_AudioMixerGroup_t959546644_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LayoutCache_t3120781045_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_Event_t3028476042_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3799506081_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0,
	&GenInst_ConstructorDelegate_t3084043859_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0,
	&GenInst_GetDelegate_t352281633_0_0_0,
	&GenInst_IDictionary_2_t266144316_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0,
	&GenInst_Type_t_0_0_0_SetDelegate_t4206365109_0_0_0,
	&GenInst_KeyValuePair_2_t3901068228_0_0_0,
	&GenInst_IDictionary_2_t3814930911_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_KeyValuePair_2_t1683227291_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t3084043859_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2778746978_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t266144316_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4255814731_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3814930911_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3509634030_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t352281633_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t24406117_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t1683227291_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t3901068228_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3573192712_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_SignerInfo_t4122348804_0_0_0,
	&GenInst_X509Cert_t481809278_0_0_0,
	&GenInst_AppleInAppPurchaseReceipt_t3271698749_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0,
	&GenInst_HashSet_1_t275936122_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0,
	&GenInst_Type_t_0_0_0_IStoreConfiguration_t2978822016_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IStoreConfiguration_t2978822016_0_0_0,
	&GenInst_KeyValuePair_2_t2673525135_0_0_0,
	&GenInst_IPurchasingModule_t4085676839_0_0_0,
	&GenInst_Product_t1203687971_0_0_0,
	&GenInst_String_t_0_0_0_Product_t1203687971_0_0_0,
	&GenInst_String_t_0_0_0_Product_t1203687971_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t875812455_0_0_0,
	&GenInst_Product_t1203687971_0_0_0_String_t_0_0_0,
	&GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0,
	&GenInst_Type_t_0_0_0_IStoreExtension_t1396898229_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IStoreExtension_t1396898229_0_0_0,
	&GenInst_KeyValuePair_2_t1091601348_0_0_0,
	&GenInst_InitializationFailureReason_t2954032642_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0_Product_t1203687971_0_0_0,
	&GenInst_ProductDescription_t3318267523_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_BaseInput_t621514313_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_HashSet_1_t2984649583_0_0_0,
	&GenInst_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IndexedSet_1_t286373651_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0,
	&GenInst_WinProductDescription_t1075111405_0_0_0,
	&GenInst_IPurchaseReceipt_t2402701844_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0_WinProductDescription_t1075111405_0_0_0,
	&GenInst_LoginResultState_t2459016979_0_0_0_String_t_0_0_0,
	&GenInst_LoginResultState_t2459016979_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FastRegisterError_t341731807_0_0_0_String_t_0_0_0,
	&GenInst_FastRegisterError_t341731807_0_0_0_Il2CppObject_0_0_0,
	&GenInst_RestoreTransactionIDState_t2487303652_0_0_0,
	&GenInst_String_t_0_0_0_ValidateReceiptState_t4359597_0_0_0_String_t_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ValidateReceiptState_t4359597_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_RequestPayOutState_t3537434082_0_0_0_String_t_0_0_0,
	&GenInst_Il2CppObject_0_0_0_RequestPayOutState_t3537434082_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_String_t_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PurchaseFailureReason_t1322959839_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProductCatalogItem_t977711995_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_InitializationFailureReason_t2954032642_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_PurchaseFailureReason_t1322959839_0_0_0,
	&GenInst_StoreID_t471452324_0_0_0,
	&GenInst_LocalizedProductDescription_t1525635964_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t566713506_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_AndroidStore_t3203055206_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t566713506_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4201451740_0_0_0,
	&GenInst_ProductDefinition_t1942475268_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Action_t3226471752_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0,
	&GenInst_Action_1_t3627374100_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Dictionary_2_t3531071141_0_0_0,
	&GenInst_String_t_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0,
	&GenInst_String_t_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IGeneratedExtensionLite_t1616291879_0_0_0,
	&GenInst_KeyValuePair_2_t1288416363_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Dictionary_2_t3531071141_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t3531071141_0_0_0,
	&GenInst_KeyValuePair_2_t880476491_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2164262055_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0,
	&GenInst_IEquatable_1_t1002176094_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_ExtensionIntPair_t3093161221_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2164262055_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0_IGeneratedExtensionLite_t1616291879_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1091104639_0_0_0,
	&GenInst_ByteString_t3153909979_0_0_0,
	&GenInst_FieldWithTarget_t2256174789_0_0_0,
	&GenInst_FieldWithRemoteSettingsKey_t2620356393_0_0_0,
	&GenInst_Product_t1203687971_0_0_0_PurchaseFailureReason_t1322959839_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PurchaseFailureReason_t1322959839_0_0_0,
	&GenInst_IAPButton_t3077837360_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0,
	&GenInst_ICanvasRaycastFilter_t1367822892_0_0_0,
	&GenInst_MaskableGraphic_t540192618_0_0_0,
	&GenInst_IMaskable_t1431842707_0_0_0,
	&GenInst_IMaterialModifier_t3028564983_0_0_0,
	&GenInst_LearningData_t1664811342_0_0_0,
	&GenInst_RegisterResponse_t410466074_0_0_0,
	&GenInst_ChangeDeviceResponse_t284910379_0_0_0,
	&GenInst_LearningLogResponse_t112900141_0_0_0,
	&GenInst_LearningLogData_t2500481692_0_0_0,
	&GenInst_LearningChapterLogData_t1224380751_0_0_0,
	&GenInst_AssetBundle_t2054978754_0_0_0,
	&GenInst_AssetBundleLoader_t639004779_0_0_0,
	&GenInst_Band_t1120454049_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_PostureCharacter_t580023905_0_0_0,
	&GenInst_StateEmotionCharacter_t3036603695_0_0_0,
	&GenInst_SelectButton_t132497280_0_0_0,
	&GenInst_String_t_0_0_0_Object_t1021602117_0_0_0,
	&GenInst_String_t_0_0_0_Object_t1021602117_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t693726601_0_0_0,
	&GenInst_ChapterTopSetting_t2509872352_0_0_0,
	&GenInst_ConversationState_t3360496668_0_0_0,
	&GenInst_Cell_t3051913968_0_0_0,
	&GenInst_IComparable_1_t588785895_0_0_0,
	&GenInst_LessonValue_t1076530945_0_0_0,
	&GenInst_GraphValue_t2739901601_0_0_0,
	&GenInst_ViewTextUI_t4118675828_0_0_0,
	&GenInst_MyPageChapterController_t2166380602_0_0_0,
	&GenInst_Column_t1930583302_0_0_0,
	&GenInst_ChapterMyPageScript_t3593529611_0_0_0,
	&GenInst_ConversationSelectionData_t4090008535_0_0_0,
	&GenInst_Param_t4123818474_0_0_0,
	&GenInst_String_t_0_0_0_AudioClip_t1932558630_0_0_0,
	&GenInst_String_t_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AudioClip_t1932558630_0_0_0,
	&GenInst_KeyValuePair_2_t1604683114_0_0_0,
	&GenInst_ConversationTalkData_t1570298305_0_0_0,
	&GenInst_IConversationModule_t2617440232_0_0_0,
	&GenInst_ConversationCommand_t3660105836_0_0_0,
	&GenInst_ISubtextItem_t4284961295_0_0_0,
	&GenInst_CharacterSelectionView_t2239851166_0_0_0,
	&GenInst_ConversationSceneSelectionView_t407867144_0_0_0,
	&GenInst_ExcerciseSceneSelectionView_t3932267486_0_0_0,
	&GenInst_ConversationHistoryItemView_t3846761105_0_0_0,
	&GenInst_ChapterItem_t2155291334_0_0_0,
	&GenInst_CachedDragInfo_t1136705792_0_0_0,
	&GenInst_EditUsernameResponse_t1388559799_0_0_0,
	&GenInst_UserInfoResponse_t693113528_0_0_0,
	&GenInst_TransferCodeResponse_t1278852611_0_0_0,
	&GenInst_ResetDataResponse_t2526516544_0_0_0,
	&GenInst_PurchaseSession_t2088054391_0_0_0,
	&GenInst_Listening1QuestionData_t3381069024_0_0_0,
	&GenInst_TrueFalseAnswerSelection_t3406544577_0_0_0,
	&GenInst_TrueFalseAnswerSelection_t3406544577_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_SelectableItem_t2941161729_0_0_0,
	&GenInst_Listening1OnplayQuestion_t3534871625_0_0_0,
	&GenInst_Listening2QuestionData_t2443975119_0_0_0,
	&GenInst_Listening2SelectableItem_t492261174_0_0_0,
	&GenInst_Listening2OnplayQuestion_t3701806074_0_0_0,
	&GenInst_Dialog_t1378192732_0_0_0,
	&GenInst_DisableWithTimeAudioName_t4015415354_0_0_0,
	&GenInst_DialogPanel_t1568014038_0_0_0,
	&GenInst_ResultSaveResponse_t3438979681_0_0_0,
	&GenInst_ZipSoundItem_t558808959_0_0_0,
	&GenInst_PushZipResponse_t1905049400_0_0_0,
	&GenInst_SentenceStructureIdiomQuestionData_t3251732102_0_0_0,
	&GenInst_SentenceStructureIdiomOnPlayQuestion_t936886987_0_0_0,
	&GenInst_SentenceListeningSpellingQuestionData_t3011907248_0_0_0,
	&GenInst_SentenceListeningSpellingOnplayQuestion_t2514545327_0_0_0,
	&GenInst_AudioSource_t1135106623_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1428657631_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_OpenHelpScript_t3207620564_0_0_0,
	&GenInst_ManageATutorial_t1512139496_0_0_0,
	&GenInst_TrackIndexObject_t1096184091_0_0_0,
	&GenInst_SelectionItem_t610427083_0_0_0,
	&GenInst_SelectItem_t2844432199_0_0_0,
	&GenInst_VocabularyQuestionData_t2021074976_0_0_0,
	&GenInst_VocabularyOnplayQuestion_t3519225043_0_0_0,
	&GenInst_RawImage_t2749640213_0_0_0,
	&GenInst_Slider_t297367283_0_0_0,
	&GenInst_ScrubberEvents_t2429506345_0_0_0,
	&GenInst_GvrEye_t3930157106_0_0_0,
	&GenInst_GvrEye_t3930157106_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_GvrEye_t3930157106_0_0_0_GvrHead_t3923315805_0_0_0,
	&GenInst_GvrHead_t3923315805_0_0_0,
	&GenInst_GvrAudioRoom_t1253442178_0_0_0,
	&GenInst_PhoneEvent_t2572128318_0_0_0_Builder_t2537253112_0_0_0,
	&GenInst_EmulatorConfig_t616150261_0_0_0,
	&GenInst_Pointer_t3000685002_0_0_0,
	&GenInst_Pointer_t1211758263_0_0_0,
	&GenInst_MotionEvent_t4072706903_0_0_0_Builder_t3452538341_0_0_0,
	&GenInst_Pointer_t1211758263_0_0_0_Builder_t2701542133_0_0_0,
	&GenInst_GyroscopeEvent_t182225200_0_0_0_Builder_t33558588_0_0_0,
	&GenInst_AccelerometerEvent_t1893725728_0_0_0_Builder_t1480486140_0_0_0,
	&GenInst_DepthMapEvent_t1516604558_0_0_0_Builder_t3483346914_0_0_0,
	&GenInst_OrientationEvent_t2038376807_0_0_0_Builder_t2561526853_0_0_0,
	&GenInst_KeyEvent_t639576718_0_0_0_Builder_t2056133158_0_0_0,
	&GenInst_IGvrPointerHoverHandler_t1683868601_0_0_0,
	&GenInst_GvrBasePointer_t2150122635_0_0_0_GvrBasePointer_t2150122635_0_0_0,
	&GenInst_GvrBasePointer_t2150122635_0_0_0,
	&GenInst_GvrBasePointer_t2150122635_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Texture2D_t3542995729_0_0_0,
	&GenInst_Texture_t2243626319_0_0_0,
	&GenInst_Action_1_t1873676830_0_0_0,
	&GenInst_Action_2_t4234541925_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Dictionary_2_t2281509423_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Dictionary_2_t2281509423_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2281509423_0_0_0,
	&GenInst_KeyValuePair_2_t3925882070_0_0_0,
	&GenInst_Sample_t3185432476_0_0_0,
	&GenInst_Log_t3604182180_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_Log_t3604182180_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t1223994146_0_0_0,
	&GenInst_String_t_0_0_0_Log_t3604182180_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t1223994146_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Log_t3604182180_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t1223994146_0_0_0,
	&GenInst_KeyValuePair_2_t896118630_0_0_0,
	&GenInst_KeyValuePair_2_t3276306664_0_0_0,
	&GenInst_PurchaseFailureReason_t1322959839_0_0_0,
	&GenInst_Type_t1530480861_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0,
	&GenInst_KeyValuePair_2_t2590619014_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_Type_t1530480861_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Type_t1530480861_0_0_0_KeyValuePair_2_t2590619014_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0,
	&GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0,
	&GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0,
	&GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0,
	&GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m52621935_gp_0_0_0_0,
	&GenInst_Array_Sort_m3546416104_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0,
	&GenInst_Array_compare_m940423571_gp_0_0_0_0,
	&GenInst_Array_qsort_m565008110_gp_0_0_0_0,
	&GenInst_Array_Resize_m1201602141_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3775633118_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0,
	&GenInst_Array_FindAll_m982349212_gp_0_0_0_0,
	&GenInst_Array_Exists_m1825464757_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0,
	&GenInst_Array_Find_m2529971459_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3929249453_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_Queue_1_t1458930734_gp_0_0_0_0,
	&GenInst_Enumerator_t4000919638_gp_0_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0,
	&GenInst_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0_Enumerable_Aggregate_m964332100_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m665396702_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m136242780_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m3508258668_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m3508258668_gp_0_0_0_0_Enumerable_Select_m3508258668_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Int32_t2071877448_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2992137322_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2992137322_gp_1_0_0_0,
	&GenInst_Enumerable_Skip_m3101762585_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0,
	&GenInst_Enumerable_Take_m169782875_gp_0_0_0_0,
	&GenInst_Enumerable_CreateTakeIterator_m1267606521_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_0_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3027976024_gp_1_0_0_0_Enumerable_ToDictionary_m3027976024_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2810079530_gp_1_0_0_0_Enumerable_ToDictionary_m2810079530_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0_Enumerable_ToDictionary_m3284215677_gp_1_0_0_0_Enumerable_ToDictionary_m3284215677_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Function_1_t1491613575_gp_0_0_0_0_Function_1_t1491613575_gp_0_0_0_0,
	&GenInst_Function_1_t1491613575_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_Int32_t2071877448_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator11_2_t3738345311_gp_1_0_0_0,
	&GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0,
	&GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4103837823_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_AndroidJavaObject_Call_m1094633808_gp_0_0_0_0,
	&GenInst_AndroidJavaObject_CallStatic_m946265290_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__Call_m4019607101_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__CallStatic_m1525952853_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_ConvertFromJNIArray_m2082383440_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_GetMethodID_m2221772144_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0,
	&GenInst_Resources_FindObjectsOfTypeAll_m3225124216_gp_0_0_0_0,
	&GenInst_Resources_LoadAll_m4114724026_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0,
	&GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0,
	&GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0,
	&GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2624498409_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2624498409_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1319939458_0_0_0,
	&GenInst__AndroidJNIHelper_GetMethodID_m656615819_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_ConfigurationBuilder_Configure_m2155711039_gp_0_0_0_0,
	&GenInst_AbstractPurchasingModule_BindExtension_m1993884112_gp_0_0_0_0,
	&GenInst_AbstractPurchasingModule_BindConfiguration_m3787044503_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_FakeStore_StartUI_m935561654_gp_0_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0,
	&GenInst_UIFakeStore_StartUI_m1214015556_gp_0_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_U3CStartUIU3Ec__AnonStorey0_1_t3316954766_gp_0_0_0_0,
	&GenInst_UnityUtil_GetAnyComponentsOfType_m709219744_gp_0_0_0_0,
	&GenInst_UnityUtil_LoadResourceInstanceOfType_m463261387_gp_0_0_0_0,
	&GenInst_CodedInputStream_ReadEnum_m2252621261_gp_0_0_0_0,
	&GenInst_CodedInputStream_ReadMessageArray_m2595769771_gp_0_0_0_0,
	&GenInst_GeneratedBuilderLite_2_t1057568736_gp_0_0_0_0_GeneratedBuilderLite_2_t1057568736_gp_1_0_0_0,
	&GenInst_ICodedOutputStream_WriteMessageArray_m3043404257_gp_0_0_0_0,
	&GenInst_CodedOutputStream_WriteMessageArray_m2075822722_gp_0_0_0_0,
	&GenInst_SerializationSurrogate_t2069264526_gp_0_0_0_0_SerializationSurrogate_t2069264526_gp_1_0_0_0,
	&GenInst_AbstractMessageLite_2_t3719087021_gp_0_0_0_0_AbstractMessageLite_2_t3719087021_gp_1_0_0_0,
	&GenInst_AbstractMessageLite_2_t3719087021_gp_0_0_0_0,
	&GenInst_PopsicleList_1_t3726111090_gp_0_0_0_0,
	&GenInst_Lists_AsReadOnly_m1817066548_gp_0_0_0_0,
	&GenInst_Lists_1_t3840821546_gp_0_0_0_0,
	&GenInst_IPopsicleList_1_t2252478525_gp_0_0_0_0,
	&GenInst_LimitedInputStream_t2315138058_gp_0_0_0_0_LimitedInputStream_t2315138058_gp_1_0_0_0,
	&GenInst_SerializationSurrogate_t4192918730_gp_0_0_0_0_SerializationSurrogate_t4192918730_gp_1_0_0_0,
	&GenInst_AbstractBuilderLite_2_t2037345479_gp_0_0_0_0_AbstractBuilderLite_2_t2037345479_gp_1_0_0_0,
	&GenInst_ThrowHelper_ThrowIfAnyNull_m3282078172_gp_0_0_0_0,
	&GenInst_GeneratedMessageLite_2_t3914197900_gp_0_0_0_0_GeneratedMessageLite_2_t3914197900_gp_1_0_0_0,
	&GenInst_GeneratedMessageLite_2_PrintField_m3342867738_gp_0_0_0_0,
	&GenInst_IBuilderLite_2_t3465575570_gp_0_0_0_0_IBuilderLite_2_t3465575570_gp_1_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_EnumParser_1_t2560453525_gp_0_0_0_0,
	&GenInst_EnumParser_1_t2560453525_gp_0_0_0_0,
	&GenInst_IMessageLite_2_t1142469830_gp_0_0_0_0,
	&GenInst_IMessageLite_2_t1142469830_gp_0_0_0_0_IMessageLite_2_t1142469830_gp_1_0_0_0,
	&GenInst_ICodedInputStream_ReadMessageArray_m14575878_gp_0_0_0_0,
	&GenInst_ApiConnector_RequestApi_m1716423800_gp_0_0_0_0,
	&GenInst_U3CRequestApiU3Ec__AnonStorey1_1_t317839536_gp_0_0_0_0,
	&GenInst_ApiRequest_1_t797614342_gp_0_0_0_0,
	&GenInst_U3CDoRequestU3Ec__Iterator0_t2682958910_gp_0_0_0_0,
	&GenInst_DataLoader_1_t2670374248_gp_0_0_0_0,
	&GenInst_IDatasWrapper_1_t4104787928_gp_0_0_0_0,
	&GenInst_GvrDropdown_GetOrAddComponent_m1130649418_gp_0_0_0_0,
	&GenInst_MultiKeyDictionary_3_t1255703599_gp_0_0_0_0_Dictionary_2_t3964470880_0_0_0,
	&GenInst_MultiKeyDictionary_3_t1255703599_gp_1_0_0_0_MultiKeyDictionary_3_t1255703599_gp_2_0_0_0,
	&GenInst_GeneratedMessageLite_2_t3914197900_gp_0_0_0_0,
	&GenInst_MotionEvent_t4072706903_0_0_0,
	&GenInst_GyroscopeEvent_t182225200_0_0_0,
	&GenInst_AccelerometerEvent_t1893725728_0_0_0,
	&GenInst_DepthMapEvent_t1516604558_0_0_0,
	&GenInst_OrientationEvent_t2038376807_0_0_0,
	&GenInst_KeyEvent_t639576718_0_0_0,
	&GenInst_PhoneEvent_t2572128318_0_0_0,
	&GenInst_KeyValuePair_2_t453129793_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_String_t_0_0_0,
	&GenInst_AsyncUtil_t423752048_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_Scrollbar_t3248359358_0_0_0,
	&GenInst_InputField_t1631627530_0_0_0,
	&GenInst_ScrollRect_t1199013257_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_ProductCatalog_t2667590766_0_0_0,
	&GenInst_IAmazonExtensions_t3890253245_0_0_0,
	&GenInst_IAmazonConfiguration_t3016942165_0_0_0,
	&GenInst_ISamsungAppsExtensions_t3429739537_0_0_0,
	&GenInst_ISamsungAppsConfiguration_t4066821689_0_0_0,
	&GenInst_UnityUtil_t166323129_0_0_0,
	&GenInst_IAppleExtensions_t1627764765_0_0_0,
	&GenInst_IAppleConfiguration_t3277762425_0_0_0,
	&GenInst_IMicrosoftConfiguration_t1212838845_0_0_0,
	&GenInst_IGooglePlayConfiguration_t2615679878_0_0_0,
	&GenInst_ITizenStoreConfiguration_t2900348728_0_0_0,
	&GenInst_IAndroidStoreSelection_t3134941501_0_0_0,
	&GenInst_IMoolahConfiguration_t3241385415_0_0_0,
	&GenInst_IMoolahExtension_t3195861654_0_0_0,
	&GenInst_IMicrosoftExtensions_t1101930285_0_0_0,
	&GenInst_MoolahStoreImpl_t4206626141_0_0_0,
	&GenInst_LifecycleNotifier_t1057582876_0_0_0,
	&GenInst_StandaloneInputModule_t70867863_0_0_0,
	&GenInst_GlobalConfig_t3080413471_0_0_0,
	&GenInst_ApiConnector_t2569785041_0_0_0,
	&GenInst_TextAsset_t3973159845_0_0_0,
	&GenInst_AssetBundleManager_t364944953_0_0_0,
	&GenInst_VaeBuildSetting_t3353517300_0_0_0,
	&GenInst_Recorder_Instance_t330262942_0_0_0,
	&GenInst_AudioMixer_t3244290001_0_0_0,
	&GenInst_InputSpectrum_t3811795451_0_0_0,
	&GenInst_VRHandleButton_t3511644738_0_0_0,
	&GenInst_BaseChapterSettingIniter_t2856709253_0_0_0,
	&GenInst_Purchaser_t1674559779_0_0_0,
	&GenInst_ManagerInstance_t2189493790_0_0_0,
	&GenInst_ChapterTopConfig_t174003822_0_0_0,
	&GenInst_ChapterLearningItemView_t1984184315_0_0_0,
	&GenInst_ColorSettingAsset_t2075258157_0_0_0,
	&GenInst_ChapterResultSettingLoader_t4141859061_0_0_0,
	&GenInst_ButtonSound_t506092895_0_0_0,
	&GenInst_PopupOpener_t1646050995_0_0_0,
	&GenInst_Animation_t2068071072_0_0_0,
	&GenInst_SoundManager_t1001218626_0_0_0,
	&GenInst_SoundManager_t695211022_0_0_0,
	&GenInst_CommandSheet_t2769384292_0_0_0,
	&GenInst_ConversationSelectionTextView_t4281204247_0_0_0,
	&GenInst_ConversationSelectionButtonView_t265815670_0_0_0,
	&GenInst_VRConversationSelectionButtonView_t1568012346_0_0_0,
	&GenInst_DataStorage_t2091384907_0_0_0,
	&GenInst_DisableWithTimeCountdown_t622718188_0_0_0,
	&GenInst_TextKaraoke_t2450194175_0_0_0,
	&GenInst_SphereCollider_t1662511355_0_0_0,
	&GenInst_EmulatorManager_t3364249716_0_0_0,
	&GenInst_EmulatorClientSocket_t2001911543_0_0_0,
	&GenInst_GvrAudioListener_t1521766837_0_0_0,
	&GenInst_GvrPointerGraphicRaycaster_t1649506702_0_0_0,
	&GenInst_StereoController_t3144380552_0_0_0,
	&GenInst_StereoRenderEffect_t958489249_0_0_0,
	&GenInst_Skybox_t2033495038_0_0_0,
	&GenInst_Collider_t3497673348_0_0_0,
	&GenInst_Renderer_t257310565_0_0_0,
	&GenInst_MeshFilter_t3026937449_0_0_0,
	&GenInst_GvrVideoPlayerTexture_t673526704_0_0_0,
	&GenInst_GvrViewer_t2583885279_0_0_0,
	&GenInst_GvrPreRender_t2074710158_0_0_0,
	&GenInst_GvrPostRender_t3118402863_0_0_0,
	&GenInst_HelpPopup_t3079166541_0_0_0,
	&GenInst_InvokerModule_t4256524346_0_0_0,
	&GenInst_IAP_UIScript_t1776166942_0_0_0,
	&GenInst_HomeCotroller_t2470068823_0_0_0,
	&GenInst_SettingBasedKaraokeTextColor_t3573755485_0_0_0,
	&GenInst_Listening1QuestionView_t2345876901_0_0_0,
	&GenInst_Listening1AnswerHelper_t2804121578_0_0_0,
	&GenInst_Listening2QuestionView_t3734381824_0_0_0,
	&GenInst_Listening2AnswerHelper_t4121473615_0_0_0,
	&GenInst_ResultStatisticController_t2929782681_0_0_0,
	&GenInst_SceneAndScreenHelper_t3943722385_0_0_0,
	&GenInst_TrackShowHelpAtFistTimeScript_t2844513732_0_0_0,
	&GenInst_CharacterScript_t1308706256_0_0_0,
	&GenInst_ChapterSelector_t191193704_0_0_0,
	&GenInst_PlayerTalkModule_t3356839145_0_0_0,
	&GenInst_OtherTalkModule_t2158105276_0_0_0,
	&GenInst_PopupScript_t535420507_0_0_0,
	&GenInst_GUISkin_t1436893342_0_0_0,
	&GenInst_ReporterGUI_t402918452_0_0_0,
	&GenInst_Reporter_t3561640551_0_0_0,
	&GenInst_ResultSaver_t3290295394_0_0_0,
	&GenInst_SceneController_t38942716_0_0_0,
	&GenInst_VocabularyController_t843712768_0_0_0,
	&GenInst_Sentence2QuestionView_t1942347884_0_0_0,
	&GenInst_SentenceQuestionView_t1478559372_0_0_0,
	&GenInst_KaraokeTextEffect_t1279919556_0_0_0,
	&GenInst_VRConversationSelectionView_t764630138_0_0_0,
	&GenInst_IconSoundAnimation_t302426630_0_0_0,
	&GenInst_CallHelpInSceneScript_t2709929151_0_0_0,
	&GenInst_ConversationLogicController_t3911886281_0_0_0,
	&GenInst_ResetData_t4114871043_0_0_0,
	&GenInst_SelectionController_t1038360966_0_0_0,
	&GenInst_DialogController_t3845653284_0_0_0,
	&GenInst_ResultRateController_t2087696941_0_0_0,
	&GenInst_VideoControlsManager_t3010523296_0_0_0,
	&GenInst_PopupController_t681110_0_0_0,
	&GenInst_VocabularyQuestionView_t1631345783_0_0_0,
	&GenInst_BaseConversationController_t808907754_0_0_0,
	&GenInst_PurchaseSession_t2088054391_0_0_0_PurchaseSession_t2088054391_0_0_0,
	&GenInst_Pointer_t3000685002_0_0_0_Pointer_t3000685002_0_0_0,
	&GenInst_Type_t1530480861_0_0_0_Type_t1530480861_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_ExtensionIntPair_t3093161221_0_0_0_ExtensionIntPair_t3093161221_0_0_0,
	&GenInst_KeyValuePair_2_t2164262055_0_0_0_KeyValuePair_2_t2164262055_0_0_0,
	&GenInst_KeyValuePair_2_t2164262055_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Type_t1530480861_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2590619014_0_0_0_KeyValuePair_2_t2590619014_0_0_0,
	&GenInst_KeyValuePair_2_t2590619014_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1683227291_0_0_0_KeyValuePair_2_t1683227291_0_0_0,
	&GenInst_KeyValuePair_2_t1683227291_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t566713506_0_0_0_KeyValuePair_2_t566713506_0_0_0,
	&GenInst_KeyValuePair_2_t566713506_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AndroidStore_t3203055206_0_0_0_AndroidStore_t3203055206_0_0_0,
};
