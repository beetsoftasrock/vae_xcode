﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_UIPoly4199244354.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Graph
struct  Graph_t2735713688  : public UIPolygon_t4199244354
{
public:
	// UnityEngine.Transform Graph::linesRadius
	Transform_t3275118058 * ___linesRadius_35;
	// UnityEngine.Transform Graph::vertexName
	Transform_t3275118058 * ___vertexName_36;

public:
	inline static int32_t get_offset_of_linesRadius_35() { return static_cast<int32_t>(offsetof(Graph_t2735713688, ___linesRadius_35)); }
	inline Transform_t3275118058 * get_linesRadius_35() const { return ___linesRadius_35; }
	inline Transform_t3275118058 ** get_address_of_linesRadius_35() { return &___linesRadius_35; }
	inline void set_linesRadius_35(Transform_t3275118058 * value)
	{
		___linesRadius_35 = value;
		Il2CppCodeGenWriteBarrier(&___linesRadius_35, value);
	}

	inline static int32_t get_offset_of_vertexName_36() { return static_cast<int32_t>(offsetof(Graph_t2735713688, ___vertexName_36)); }
	inline Transform_t3275118058 * get_vertexName_36() const { return ___vertexName_36; }
	inline Transform_t3275118058 ** get_address_of_vertexName_36() { return &___vertexName_36; }
	inline void set_vertexName_36(Transform_t3275118058 * value)
	{
		___vertexName_36 = value;
		Il2CppCodeGenWriteBarrier(&___vertexName_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
