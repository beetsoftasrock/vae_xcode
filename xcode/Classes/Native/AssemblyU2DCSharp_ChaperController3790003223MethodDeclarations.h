﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChaperController
struct ChaperController_t3790003223;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void ChaperController::.ctor()
extern "C"  void ChaperController__ctor_m2338381628 (ChaperController_t3790003223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChaperController::StudyVocabulary()
extern "C"  void ChaperController_StudyVocabulary_m474775239 (ChaperController_t3790003223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChaperController::PlayPractice()
extern "C"  void ChaperController_PlayPractice_m1316262957 (ChaperController_t3790003223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChaperController::OnFinishVocabulary(UnityEngine.GameObject)
extern "C"  void ChaperController_OnFinishVocabulary_m723155622 (ChaperController_t3790003223 * __this, GameObject_t1756533147 * ___vocab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChaperController::Start()
extern "C"  void ChaperController_Start_m3387738552 (ChaperController_t3790003223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
