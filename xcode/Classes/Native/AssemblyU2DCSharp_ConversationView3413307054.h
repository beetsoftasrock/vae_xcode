﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationView
struct  ConversationView_t3413307054  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ConversationView::levelSelectView
	GameObject_t1756533147 * ___levelSelectView_2;
	// UnityEngine.GameObject ConversationView::conversationView
	GameObject_t1756533147 * ___conversationView_3;
	// UnityEngine.GameObject ConversationView::completedView
	GameObject_t1756533147 * ___completedView_4;
	// UnityEngine.GameObject ConversationView::resultView
	GameObject_t1756533147 * ___resultView_5;

public:
	inline static int32_t get_offset_of_levelSelectView_2() { return static_cast<int32_t>(offsetof(ConversationView_t3413307054, ___levelSelectView_2)); }
	inline GameObject_t1756533147 * get_levelSelectView_2() const { return ___levelSelectView_2; }
	inline GameObject_t1756533147 ** get_address_of_levelSelectView_2() { return &___levelSelectView_2; }
	inline void set_levelSelectView_2(GameObject_t1756533147 * value)
	{
		___levelSelectView_2 = value;
		Il2CppCodeGenWriteBarrier(&___levelSelectView_2, value);
	}

	inline static int32_t get_offset_of_conversationView_3() { return static_cast<int32_t>(offsetof(ConversationView_t3413307054, ___conversationView_3)); }
	inline GameObject_t1756533147 * get_conversationView_3() const { return ___conversationView_3; }
	inline GameObject_t1756533147 ** get_address_of_conversationView_3() { return &___conversationView_3; }
	inline void set_conversationView_3(GameObject_t1756533147 * value)
	{
		___conversationView_3 = value;
		Il2CppCodeGenWriteBarrier(&___conversationView_3, value);
	}

	inline static int32_t get_offset_of_completedView_4() { return static_cast<int32_t>(offsetof(ConversationView_t3413307054, ___completedView_4)); }
	inline GameObject_t1756533147 * get_completedView_4() const { return ___completedView_4; }
	inline GameObject_t1756533147 ** get_address_of_completedView_4() { return &___completedView_4; }
	inline void set_completedView_4(GameObject_t1756533147 * value)
	{
		___completedView_4 = value;
		Il2CppCodeGenWriteBarrier(&___completedView_4, value);
	}

	inline static int32_t get_offset_of_resultView_5() { return static_cast<int32_t>(offsetof(ConversationView_t3413307054, ___resultView_5)); }
	inline GameObject_t1756533147 * get_resultView_5() const { return ___resultView_5; }
	inline GameObject_t1756533147 ** get_address_of_resultView_5() { return &___resultView_5; }
	inline void set_resultView_5(GameObject_t1756533147 * value)
	{
		___resultView_5 = value;
		Il2CppCodeGenWriteBarrier(&___resultView_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
