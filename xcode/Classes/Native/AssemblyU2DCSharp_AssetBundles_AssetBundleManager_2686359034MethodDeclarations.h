﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<IsLoadedBundle>c__AnonStorey5
struct U3CIsLoadedBundleU3Ec__AnonStorey5_t2686359034;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754.h"

// System.Void AssetBundles.AssetBundleManager/<IsLoadedBundle>c__AnonStorey5::.ctor()
extern "C"  void U3CIsLoadedBundleU3Ec__AnonStorey5__ctor_m1010507899 (U3CIsLoadedBundleU3Ec__AnonStorey5_t2686359034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager/<IsLoadedBundle>c__AnonStorey5::<>m__0(UnityEngine.AssetBundle)
extern "C"  bool U3CIsLoadedBundleU3Ec__AnonStorey5_U3CU3Em__0_m494879163 (U3CIsLoadedBundleU3Ec__AnonStorey5_t2686359034 * __this, AssetBundle_t2054978754 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
