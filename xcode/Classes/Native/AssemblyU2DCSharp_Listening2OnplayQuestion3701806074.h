﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Listening2QuestionView
struct Listening2QuestionView_t3734381824;
// Listening2QuestionData
struct Listening2QuestionData_t2443975119;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3194695850;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;

#include "AssemblyU2DCSharp_BaseOnplayQuestion1717064182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening2OnplayQuestion
struct  Listening2OnplayQuestion_t3701806074  : public BaseOnplayQuestion_t1717064182
{
public:
	// System.String Listening2OnplayQuestion::soundPath
	String_t* ___soundPath_2;
	// Listening2QuestionView Listening2OnplayQuestion::_view
	Listening2QuestionView_t3734381824 * ____view_3;
	// Listening2QuestionData Listening2OnplayQuestion::<questionData>k__BackingField
	Listening2QuestionData_t2443975119 * ___U3CquestionDataU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<System.Boolean> Listening2OnplayQuestion::<currentAnswer>k__BackingField
	List_1_t3194695850 * ___U3CcurrentAnswerU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.Int32> Listening2OnplayQuestion::<currentIndexAnswer>k__BackingField
	List_1_t1440998580 * ___U3CcurrentIndexAnswerU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_soundPath_2() { return static_cast<int32_t>(offsetof(Listening2OnplayQuestion_t3701806074, ___soundPath_2)); }
	inline String_t* get_soundPath_2() const { return ___soundPath_2; }
	inline String_t** get_address_of_soundPath_2() { return &___soundPath_2; }
	inline void set_soundPath_2(String_t* value)
	{
		___soundPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_2, value);
	}

	inline static int32_t get_offset_of__view_3() { return static_cast<int32_t>(offsetof(Listening2OnplayQuestion_t3701806074, ____view_3)); }
	inline Listening2QuestionView_t3734381824 * get__view_3() const { return ____view_3; }
	inline Listening2QuestionView_t3734381824 ** get_address_of__view_3() { return &____view_3; }
	inline void set__view_3(Listening2QuestionView_t3734381824 * value)
	{
		____view_3 = value;
		Il2CppCodeGenWriteBarrier(&____view_3, value);
	}

	inline static int32_t get_offset_of_U3CquestionDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Listening2OnplayQuestion_t3701806074, ___U3CquestionDataU3Ek__BackingField_4)); }
	inline Listening2QuestionData_t2443975119 * get_U3CquestionDataU3Ek__BackingField_4() const { return ___U3CquestionDataU3Ek__BackingField_4; }
	inline Listening2QuestionData_t2443975119 ** get_address_of_U3CquestionDataU3Ek__BackingField_4() { return &___U3CquestionDataU3Ek__BackingField_4; }
	inline void set_U3CquestionDataU3Ek__BackingField_4(Listening2QuestionData_t2443975119 * value)
	{
		___U3CquestionDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionDataU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CcurrentAnswerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Listening2OnplayQuestion_t3701806074, ___U3CcurrentAnswerU3Ek__BackingField_5)); }
	inline List_1_t3194695850 * get_U3CcurrentAnswerU3Ek__BackingField_5() const { return ___U3CcurrentAnswerU3Ek__BackingField_5; }
	inline List_1_t3194695850 ** get_address_of_U3CcurrentAnswerU3Ek__BackingField_5() { return &___U3CcurrentAnswerU3Ek__BackingField_5; }
	inline void set_U3CcurrentAnswerU3Ek__BackingField_5(List_1_t3194695850 * value)
	{
		___U3CcurrentAnswerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentAnswerU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CcurrentIndexAnswerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Listening2OnplayQuestion_t3701806074, ___U3CcurrentIndexAnswerU3Ek__BackingField_6)); }
	inline List_1_t1440998580 * get_U3CcurrentIndexAnswerU3Ek__BackingField_6() const { return ___U3CcurrentIndexAnswerU3Ek__BackingField_6; }
	inline List_1_t1440998580 ** get_address_of_U3CcurrentIndexAnswerU3Ek__BackingField_6() { return &___U3CcurrentIndexAnswerU3Ek__BackingField_6; }
	inline void set_U3CcurrentIndexAnswerU3Ek__BackingField_6(List_1_t1440998580 * value)
	{
		___U3CcurrentIndexAnswerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentIndexAnswerU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
