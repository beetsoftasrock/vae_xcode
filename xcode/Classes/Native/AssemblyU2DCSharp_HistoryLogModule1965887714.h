﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationLogicController
struct ConversationLogicController_t3911886281;
// HistoryConversationDialog
struct HistoryConversationDialog_t3676407505;
// PlayerTalkModule
struct PlayerTalkModule_t3356839145;
// OtherTalkModule
struct OtherTalkModule_t2158105276;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HistoryLogModule
struct  HistoryLogModule_t1965887714  : public MonoBehaviour_t1158329972
{
public:
	// ConversationLogicController HistoryLogModule::clc
	ConversationLogicController_t3911886281 * ___clc_2;
	// HistoryConversationDialog HistoryLogModule::historyDialog
	HistoryConversationDialog_t3676407505 * ___historyDialog_3;
	// PlayerTalkModule HistoryLogModule::playerTalkModule
	PlayerTalkModule_t3356839145 * ___playerTalkModule_4;
	// OtherTalkModule HistoryLogModule::otherTalkModule
	OtherTalkModule_t2158105276 * ___otherTalkModule_5;

public:
	inline static int32_t get_offset_of_clc_2() { return static_cast<int32_t>(offsetof(HistoryLogModule_t1965887714, ___clc_2)); }
	inline ConversationLogicController_t3911886281 * get_clc_2() const { return ___clc_2; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_2() { return &___clc_2; }
	inline void set_clc_2(ConversationLogicController_t3911886281 * value)
	{
		___clc_2 = value;
		Il2CppCodeGenWriteBarrier(&___clc_2, value);
	}

	inline static int32_t get_offset_of_historyDialog_3() { return static_cast<int32_t>(offsetof(HistoryLogModule_t1965887714, ___historyDialog_3)); }
	inline HistoryConversationDialog_t3676407505 * get_historyDialog_3() const { return ___historyDialog_3; }
	inline HistoryConversationDialog_t3676407505 ** get_address_of_historyDialog_3() { return &___historyDialog_3; }
	inline void set_historyDialog_3(HistoryConversationDialog_t3676407505 * value)
	{
		___historyDialog_3 = value;
		Il2CppCodeGenWriteBarrier(&___historyDialog_3, value);
	}

	inline static int32_t get_offset_of_playerTalkModule_4() { return static_cast<int32_t>(offsetof(HistoryLogModule_t1965887714, ___playerTalkModule_4)); }
	inline PlayerTalkModule_t3356839145 * get_playerTalkModule_4() const { return ___playerTalkModule_4; }
	inline PlayerTalkModule_t3356839145 ** get_address_of_playerTalkModule_4() { return &___playerTalkModule_4; }
	inline void set_playerTalkModule_4(PlayerTalkModule_t3356839145 * value)
	{
		___playerTalkModule_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerTalkModule_4, value);
	}

	inline static int32_t get_offset_of_otherTalkModule_5() { return static_cast<int32_t>(offsetof(HistoryLogModule_t1965887714, ___otherTalkModule_5)); }
	inline OtherTalkModule_t2158105276 * get_otherTalkModule_5() const { return ___otherTalkModule_5; }
	inline OtherTalkModule_t2158105276 ** get_address_of_otherTalkModule_5() { return &___otherTalkModule_5; }
	inline void set_otherTalkModule_5(OtherTalkModule_t2158105276 * value)
	{
		___otherTalkModule_5 = value;
		Il2CppCodeGenWriteBarrier(&___otherTalkModule_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
