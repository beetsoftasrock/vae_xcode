﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ChapterSelector
struct ChapterSelector_t191193704;
// ChapterItem
struct ChapterItem_t2155291334;
// System.Collections.Generic.List`1<ChapterItem>
struct List_1_t1524412466;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Collections.Generic.List`1<CachedDragInfo>
struct List_1_t505826924;
// HomeCotroller
struct HomeCotroller_t2470068823;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterSelector
struct  ChapterSelector_t191193704  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ChapterSelector::widthStep
	float ___widthStep_4;
	// ChapterItem ChapterSelector::selectedItem
	ChapterItem_t2155291334 * ___selectedItem_5;
	// System.Collections.Generic.List`1<ChapterItem> ChapterSelector::selectItems
	List_1_t1524412466 * ___selectItems_6;
	// UnityEngine.RectTransform ChapterSelector::container
	RectTransform_t3349966182 * ___container_7;
	// UnityEngine.Coroutine ChapterSelector::_currentCoroutine
	Coroutine_t2299508840 * ____currentCoroutine_8;
	// UnityEngine.Camera ChapterSelector::mapCamera
	Camera_t189460977 * ___mapCamera_9;
	// System.Collections.Generic.List`1<CachedDragInfo> ChapterSelector::_dragDatas
	List_1_t505826924 * ____dragDatas_12;
	// ChapterItem ChapterSelector::chapItemOrigin
	ChapterItem_t2155291334 * ___chapItemOrigin_13;
	// HomeCotroller ChapterSelector::homeController
	HomeCotroller_t2470068823 * ___homeController_14;
	// UnityEngine.Sprite ChapterSelector::spriteCommingsoon
	Sprite_t309593783 * ___spriteCommingsoon_15;
	// UnityEngine.Transform ChapterSelector::mapLocations
	Transform_t3275118058 * ___mapLocations_16;
	// System.Single ChapterSelector::xOrigin
	float ___xOrigin_17;
	// System.Boolean ChapterSelector::isTracked
	bool ___isTracked_18;

public:
	inline static int32_t get_offset_of_widthStep_4() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___widthStep_4)); }
	inline float get_widthStep_4() const { return ___widthStep_4; }
	inline float* get_address_of_widthStep_4() { return &___widthStep_4; }
	inline void set_widthStep_4(float value)
	{
		___widthStep_4 = value;
	}

	inline static int32_t get_offset_of_selectedItem_5() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___selectedItem_5)); }
	inline ChapterItem_t2155291334 * get_selectedItem_5() const { return ___selectedItem_5; }
	inline ChapterItem_t2155291334 ** get_address_of_selectedItem_5() { return &___selectedItem_5; }
	inline void set_selectedItem_5(ChapterItem_t2155291334 * value)
	{
		___selectedItem_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectedItem_5, value);
	}

	inline static int32_t get_offset_of_selectItems_6() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___selectItems_6)); }
	inline List_1_t1524412466 * get_selectItems_6() const { return ___selectItems_6; }
	inline List_1_t1524412466 ** get_address_of_selectItems_6() { return &___selectItems_6; }
	inline void set_selectItems_6(List_1_t1524412466 * value)
	{
		___selectItems_6 = value;
		Il2CppCodeGenWriteBarrier(&___selectItems_6, value);
	}

	inline static int32_t get_offset_of_container_7() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___container_7)); }
	inline RectTransform_t3349966182 * get_container_7() const { return ___container_7; }
	inline RectTransform_t3349966182 ** get_address_of_container_7() { return &___container_7; }
	inline void set_container_7(RectTransform_t3349966182 * value)
	{
		___container_7 = value;
		Il2CppCodeGenWriteBarrier(&___container_7, value);
	}

	inline static int32_t get_offset_of__currentCoroutine_8() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ____currentCoroutine_8)); }
	inline Coroutine_t2299508840 * get__currentCoroutine_8() const { return ____currentCoroutine_8; }
	inline Coroutine_t2299508840 ** get_address_of__currentCoroutine_8() { return &____currentCoroutine_8; }
	inline void set__currentCoroutine_8(Coroutine_t2299508840 * value)
	{
		____currentCoroutine_8 = value;
		Il2CppCodeGenWriteBarrier(&____currentCoroutine_8, value);
	}

	inline static int32_t get_offset_of_mapCamera_9() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___mapCamera_9)); }
	inline Camera_t189460977 * get_mapCamera_9() const { return ___mapCamera_9; }
	inline Camera_t189460977 ** get_address_of_mapCamera_9() { return &___mapCamera_9; }
	inline void set_mapCamera_9(Camera_t189460977 * value)
	{
		___mapCamera_9 = value;
		Il2CppCodeGenWriteBarrier(&___mapCamera_9, value);
	}

	inline static int32_t get_offset_of__dragDatas_12() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ____dragDatas_12)); }
	inline List_1_t505826924 * get__dragDatas_12() const { return ____dragDatas_12; }
	inline List_1_t505826924 ** get_address_of__dragDatas_12() { return &____dragDatas_12; }
	inline void set__dragDatas_12(List_1_t505826924 * value)
	{
		____dragDatas_12 = value;
		Il2CppCodeGenWriteBarrier(&____dragDatas_12, value);
	}

	inline static int32_t get_offset_of_chapItemOrigin_13() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___chapItemOrigin_13)); }
	inline ChapterItem_t2155291334 * get_chapItemOrigin_13() const { return ___chapItemOrigin_13; }
	inline ChapterItem_t2155291334 ** get_address_of_chapItemOrigin_13() { return &___chapItemOrigin_13; }
	inline void set_chapItemOrigin_13(ChapterItem_t2155291334 * value)
	{
		___chapItemOrigin_13 = value;
		Il2CppCodeGenWriteBarrier(&___chapItemOrigin_13, value);
	}

	inline static int32_t get_offset_of_homeController_14() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___homeController_14)); }
	inline HomeCotroller_t2470068823 * get_homeController_14() const { return ___homeController_14; }
	inline HomeCotroller_t2470068823 ** get_address_of_homeController_14() { return &___homeController_14; }
	inline void set_homeController_14(HomeCotroller_t2470068823 * value)
	{
		___homeController_14 = value;
		Il2CppCodeGenWriteBarrier(&___homeController_14, value);
	}

	inline static int32_t get_offset_of_spriteCommingsoon_15() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___spriteCommingsoon_15)); }
	inline Sprite_t309593783 * get_spriteCommingsoon_15() const { return ___spriteCommingsoon_15; }
	inline Sprite_t309593783 ** get_address_of_spriteCommingsoon_15() { return &___spriteCommingsoon_15; }
	inline void set_spriteCommingsoon_15(Sprite_t309593783 * value)
	{
		___spriteCommingsoon_15 = value;
		Il2CppCodeGenWriteBarrier(&___spriteCommingsoon_15, value);
	}

	inline static int32_t get_offset_of_mapLocations_16() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___mapLocations_16)); }
	inline Transform_t3275118058 * get_mapLocations_16() const { return ___mapLocations_16; }
	inline Transform_t3275118058 ** get_address_of_mapLocations_16() { return &___mapLocations_16; }
	inline void set_mapLocations_16(Transform_t3275118058 * value)
	{
		___mapLocations_16 = value;
		Il2CppCodeGenWriteBarrier(&___mapLocations_16, value);
	}

	inline static int32_t get_offset_of_xOrigin_17() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___xOrigin_17)); }
	inline float get_xOrigin_17() const { return ___xOrigin_17; }
	inline float* get_address_of_xOrigin_17() { return &___xOrigin_17; }
	inline void set_xOrigin_17(float value)
	{
		___xOrigin_17 = value;
	}

	inline static int32_t get_offset_of_isTracked_18() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704, ___isTracked_18)); }
	inline bool get_isTracked_18() const { return ___isTracked_18; }
	inline bool* get_address_of_isTracked_18() { return &___isTracked_18; }
	inline void set_isTracked_18(bool value)
	{
		___isTracked_18 = value;
	}
};

struct ChapterSelector_t191193704_StaticFields
{
public:
	// ChapterSelector ChapterSelector::<Instance>k__BackingField
	ChapterSelector_t191193704 * ___U3CInstanceU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ChapterSelector_t191193704_StaticFields, ___U3CInstanceU3Ek__BackingField_3)); }
	inline ChapterSelector_t191193704 * get_U3CInstanceU3Ek__BackingField_3() const { return ___U3CInstanceU3Ek__BackingField_3; }
	inline ChapterSelector_t191193704 ** get_address_of_U3CInstanceU3Ek__BackingField_3() { return &___U3CInstanceU3Ek__BackingField_3; }
	inline void set_U3CInstanceU3Ek__BackingField_3(ChapterSelector_t191193704 * value)
	{
		___U3CInstanceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
