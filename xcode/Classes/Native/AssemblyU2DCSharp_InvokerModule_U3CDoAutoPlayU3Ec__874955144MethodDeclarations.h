﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InvokerModule/<DoAutoPlay>c__Iterator2
struct U3CDoAutoPlayU3Ec__Iterator2_t874955144;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void InvokerModule/<DoAutoPlay>c__Iterator2::.ctor()
extern "C"  void U3CDoAutoPlayU3Ec__Iterator2__ctor_m3710090637 (U3CDoAutoPlayU3Ec__Iterator2_t874955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InvokerModule/<DoAutoPlay>c__Iterator2::MoveNext()
extern "C"  bool U3CDoAutoPlayU3Ec__Iterator2_MoveNext_m338052683 (U3CDoAutoPlayU3Ec__Iterator2_t874955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InvokerModule/<DoAutoPlay>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoAutoPlayU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2149661851 (U3CDoAutoPlayU3Ec__Iterator2_t874955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InvokerModule/<DoAutoPlay>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoAutoPlayU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2844444691 (U3CDoAutoPlayU3Ec__Iterator2_t874955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule/<DoAutoPlay>c__Iterator2::Dispose()
extern "C"  void U3CDoAutoPlayU3Ec__Iterator2_Dispose_m269127818 (U3CDoAutoPlayU3Ec__Iterator2_t874955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule/<DoAutoPlay>c__Iterator2::Reset()
extern "C"  void U3CDoAutoPlayU3Ec__Iterator2_Reset_m1099703332 (U3CDoAutoPlayU3Ec__Iterator2_t874955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
