﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollViewExtendControl
struct  ScrollViewExtendControl_t924978223  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ScrollViewExtendControl::defaultMoveTimeout
	float ___defaultMoveTimeout_2;
	// System.Single ScrollViewExtendControl::moveSpeed
	float ___moveSpeed_3;
	// UnityEngine.UI.ScrollRect ScrollViewExtendControl::scrollRect
	ScrollRect_t1199013257 * ___scrollRect_4;

public:
	inline static int32_t get_offset_of_defaultMoveTimeout_2() { return static_cast<int32_t>(offsetof(ScrollViewExtendControl_t924978223, ___defaultMoveTimeout_2)); }
	inline float get_defaultMoveTimeout_2() const { return ___defaultMoveTimeout_2; }
	inline float* get_address_of_defaultMoveTimeout_2() { return &___defaultMoveTimeout_2; }
	inline void set_defaultMoveTimeout_2(float value)
	{
		___defaultMoveTimeout_2 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_3() { return static_cast<int32_t>(offsetof(ScrollViewExtendControl_t924978223, ___moveSpeed_3)); }
	inline float get_moveSpeed_3() const { return ___moveSpeed_3; }
	inline float* get_address_of_moveSpeed_3() { return &___moveSpeed_3; }
	inline void set_moveSpeed_3(float value)
	{
		___moveSpeed_3 = value;
	}

	inline static int32_t get_offset_of_scrollRect_4() { return static_cast<int32_t>(offsetof(ScrollViewExtendControl_t924978223, ___scrollRect_4)); }
	inline ScrollRect_t1199013257 * get_scrollRect_4() const { return ___scrollRect_4; }
	inline ScrollRect_t1199013257 ** get_address_of_scrollRect_4() { return &___scrollRect_4; }
	inline void set_scrollRect_4(ScrollRect_t1199013257 * value)
	{
		___scrollRect_4 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
