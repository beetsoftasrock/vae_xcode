﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadChapterScript
struct  LoadChapterScript_t2140695782  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform LoadChapterScript::selectVR
	Transform_t3275118058 * ___selectVR_2;
	// UnityEngine.Transform LoadChapterScript::selectChapter
	Transform_t3275118058 * ___selectChapter_3;

public:
	inline static int32_t get_offset_of_selectVR_2() { return static_cast<int32_t>(offsetof(LoadChapterScript_t2140695782, ___selectVR_2)); }
	inline Transform_t3275118058 * get_selectVR_2() const { return ___selectVR_2; }
	inline Transform_t3275118058 ** get_address_of_selectVR_2() { return &___selectVR_2; }
	inline void set_selectVR_2(Transform_t3275118058 * value)
	{
		___selectVR_2 = value;
		Il2CppCodeGenWriteBarrier(&___selectVR_2, value);
	}

	inline static int32_t get_offset_of_selectChapter_3() { return static_cast<int32_t>(offsetof(LoadChapterScript_t2140695782, ___selectChapter_3)); }
	inline Transform_t3275118058 * get_selectChapter_3() const { return ___selectChapter_3; }
	inline Transform_t3275118058 ** get_address_of_selectChapter_3() { return &___selectChapter_3; }
	inline void set_selectChapter_3(Transform_t3275118058 * value)
	{
		___selectChapter_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectChapter_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
