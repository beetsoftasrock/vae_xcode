﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2243626319;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic540192618.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIPolygon
struct  UIPolygon_t4199244354  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.Texture UnityEngine.UI.Extensions.UIPolygon::m_Texture
	Texture_t2243626319 * ___m_Texture_28;
	// System.Boolean UnityEngine.UI.Extensions.UIPolygon::fill
	bool ___fill_29;
	// System.Single UnityEngine.UI.Extensions.UIPolygon::thickness
	float ___thickness_30;
	// System.Int32 UnityEngine.UI.Extensions.UIPolygon::sides
	int32_t ___sides_31;
	// System.Single UnityEngine.UI.Extensions.UIPolygon::rotation
	float ___rotation_32;
	// System.Single[] UnityEngine.UI.Extensions.UIPolygon::VerticesDistances
	SingleU5BU5D_t577127397* ___VerticesDistances_33;
	// System.Single UnityEngine.UI.Extensions.UIPolygon::size
	float ___size_34;

public:
	inline static int32_t get_offset_of_m_Texture_28() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___m_Texture_28)); }
	inline Texture_t2243626319 * get_m_Texture_28() const { return ___m_Texture_28; }
	inline Texture_t2243626319 ** get_address_of_m_Texture_28() { return &___m_Texture_28; }
	inline void set_m_Texture_28(Texture_t2243626319 * value)
	{
		___m_Texture_28 = value;
		Il2CppCodeGenWriteBarrier(&___m_Texture_28, value);
	}

	inline static int32_t get_offset_of_fill_29() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___fill_29)); }
	inline bool get_fill_29() const { return ___fill_29; }
	inline bool* get_address_of_fill_29() { return &___fill_29; }
	inline void set_fill_29(bool value)
	{
		___fill_29 = value;
	}

	inline static int32_t get_offset_of_thickness_30() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___thickness_30)); }
	inline float get_thickness_30() const { return ___thickness_30; }
	inline float* get_address_of_thickness_30() { return &___thickness_30; }
	inline void set_thickness_30(float value)
	{
		___thickness_30 = value;
	}

	inline static int32_t get_offset_of_sides_31() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___sides_31)); }
	inline int32_t get_sides_31() const { return ___sides_31; }
	inline int32_t* get_address_of_sides_31() { return &___sides_31; }
	inline void set_sides_31(int32_t value)
	{
		___sides_31 = value;
	}

	inline static int32_t get_offset_of_rotation_32() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___rotation_32)); }
	inline float get_rotation_32() const { return ___rotation_32; }
	inline float* get_address_of_rotation_32() { return &___rotation_32; }
	inline void set_rotation_32(float value)
	{
		___rotation_32 = value;
	}

	inline static int32_t get_offset_of_VerticesDistances_33() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___VerticesDistances_33)); }
	inline SingleU5BU5D_t577127397* get_VerticesDistances_33() const { return ___VerticesDistances_33; }
	inline SingleU5BU5D_t577127397** get_address_of_VerticesDistances_33() { return &___VerticesDistances_33; }
	inline void set_VerticesDistances_33(SingleU5BU5D_t577127397* value)
	{
		___VerticesDistances_33 = value;
		Il2CppCodeGenWriteBarrier(&___VerticesDistances_33, value);
	}

	inline static int32_t get_offset_of_size_34() { return static_cast<int32_t>(offsetof(UIPolygon_t4199244354, ___size_34)); }
	inline float get_size_34() const { return ___size_34; }
	inline float* get_address_of_size_34() { return &___size_34; }
	inline void set_size_34(float value)
	{
		___size_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
