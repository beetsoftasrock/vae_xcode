﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserInfoResponse
struct  UserInfoResponse_t693113528  : public Il2CppObject
{
public:
	// System.Int32 UserInfoResponse::userId
	int32_t ___userId_0;
	// System.String UserInfoResponse::userName
	String_t* ___userName_1;
	// System.String UserInfoResponse::payment
	String_t* ___payment_2;
	// System.String UserInfoResponse::paymentExpireDate
	String_t* ___paymentExpireDate_3;

public:
	inline static int32_t get_offset_of_userId_0() { return static_cast<int32_t>(offsetof(UserInfoResponse_t693113528, ___userId_0)); }
	inline int32_t get_userId_0() const { return ___userId_0; }
	inline int32_t* get_address_of_userId_0() { return &___userId_0; }
	inline void set_userId_0(int32_t value)
	{
		___userId_0 = value;
	}

	inline static int32_t get_offset_of_userName_1() { return static_cast<int32_t>(offsetof(UserInfoResponse_t693113528, ___userName_1)); }
	inline String_t* get_userName_1() const { return ___userName_1; }
	inline String_t** get_address_of_userName_1() { return &___userName_1; }
	inline void set_userName_1(String_t* value)
	{
		___userName_1 = value;
		Il2CppCodeGenWriteBarrier(&___userName_1, value);
	}

	inline static int32_t get_offset_of_payment_2() { return static_cast<int32_t>(offsetof(UserInfoResponse_t693113528, ___payment_2)); }
	inline String_t* get_payment_2() const { return ___payment_2; }
	inline String_t** get_address_of_payment_2() { return &___payment_2; }
	inline void set_payment_2(String_t* value)
	{
		___payment_2 = value;
		Il2CppCodeGenWriteBarrier(&___payment_2, value);
	}

	inline static int32_t get_offset_of_paymentExpireDate_3() { return static_cast<int32_t>(offsetof(UserInfoResponse_t693113528, ___paymentExpireDate_3)); }
	inline String_t* get_paymentExpireDate_3() const { return ___paymentExpireDate_3; }
	inline String_t** get_address_of_paymentExpireDate_3() { return &___paymentExpireDate_3; }
	inline void set_paymentExpireDate_3(String_t* value)
	{
		___paymentExpireDate_3 = value;
		Il2CppCodeGenWriteBarrier(&___paymentExpireDate_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
