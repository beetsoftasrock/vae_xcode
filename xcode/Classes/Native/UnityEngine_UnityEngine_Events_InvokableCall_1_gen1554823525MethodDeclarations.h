﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`1<ConversationLogicController/ConversationState>
struct InvokableCall_1_t1554823525;
// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<ConversationLogicController/ConversationState>
struct UnityAction_1_t432115123;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"

// System.Void UnityEngine.Events.InvokableCall`1<ConversationLogicController/ConversationState>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m1954223813_gshared (InvokableCall_1_t1554823525 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define InvokableCall_1__ctor_m1954223813(__this, ___target0, ___theFunction1, method) ((  void (*) (InvokableCall_1_t1554823525 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m1954223813_gshared)(__this, ___target0, ___theFunction1, method)
// System.Void UnityEngine.Events.InvokableCall`1<ConversationLogicController/ConversationState>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m351836765_gshared (InvokableCall_1_t1554823525 * __this, UnityAction_1_t432115123 * ___action0, const MethodInfo* method);
#define InvokableCall_1__ctor_m351836765(__this, ___action0, method) ((  void (*) (InvokableCall_1_t1554823525 *, UnityAction_1_t432115123 *, const MethodInfo*))InvokableCall_1__ctor_m351836765_gshared)(__this, ___action0, method)
// System.Void UnityEngine.Events.InvokableCall`1<ConversationLogicController/ConversationState>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m2123730662_gshared (InvokableCall_1_t1554823525 * __this, UnityAction_1_t432115123 * ___value0, const MethodInfo* method);
#define InvokableCall_1_add_Delegate_m2123730662(__this, ___value0, method) ((  void (*) (InvokableCall_1_t1554823525 *, UnityAction_1_t432115123 *, const MethodInfo*))InvokableCall_1_add_Delegate_m2123730662_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`1<ConversationLogicController/ConversationState>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1223645395_gshared (InvokableCall_1_t1554823525 * __this, UnityAction_1_t432115123 * ___value0, const MethodInfo* method);
#define InvokableCall_1_remove_Delegate_m1223645395(__this, ___value0, method) ((  void (*) (InvokableCall_1_t1554823525 *, UnityAction_1_t432115123 *, const MethodInfo*))InvokableCall_1_remove_Delegate_m1223645395_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Events.InvokableCall`1<ConversationLogicController/ConversationState>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m2671737604_gshared (InvokableCall_1_t1554823525 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
#define InvokableCall_1_Invoke_m2671737604(__this, ___args0, method) ((  void (*) (InvokableCall_1_t1554823525 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))InvokableCall_1_Invoke_m2671737604_gshared)(__this, ___args0, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<ConversationLogicController/ConversationState>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m3635782032_gshared (InvokableCall_1_t1554823525 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method);
#define InvokableCall_1_Find_m3635782032(__this, ___targetObj0, ___method1, method) ((  bool (*) (InvokableCall_1_t1554823525 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m3635782032_gshared)(__this, ___targetObj0, ___method1, method)
