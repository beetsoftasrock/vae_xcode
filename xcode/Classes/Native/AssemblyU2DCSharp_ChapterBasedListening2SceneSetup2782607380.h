﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Listening2DataLoader
struct Listening2DataLoader_t3991471186;
// Listening2SceneController
struct Listening2SceneController_t3779648327;

#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterBasedListening2SceneSetup
struct  ChapterBasedListening2SceneSetup_t2782607380  : public GlobalBasedSetupModule_t1655890765
{
public:
	// Listening2DataLoader ChapterBasedListening2SceneSetup::dataLoader
	Listening2DataLoader_t3991471186 * ___dataLoader_3;
	// Listening2SceneController ChapterBasedListening2SceneSetup::sceneController
	Listening2SceneController_t3779648327 * ___sceneController_4;

public:
	inline static int32_t get_offset_of_dataLoader_3() { return static_cast<int32_t>(offsetof(ChapterBasedListening2SceneSetup_t2782607380, ___dataLoader_3)); }
	inline Listening2DataLoader_t3991471186 * get_dataLoader_3() const { return ___dataLoader_3; }
	inline Listening2DataLoader_t3991471186 ** get_address_of_dataLoader_3() { return &___dataLoader_3; }
	inline void set_dataLoader_3(Listening2DataLoader_t3991471186 * value)
	{
		___dataLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_3, value);
	}

	inline static int32_t get_offset_of_sceneController_4() { return static_cast<int32_t>(offsetof(ChapterBasedListening2SceneSetup_t2782607380, ___sceneController_4)); }
	inline Listening2SceneController_t3779648327 * get_sceneController_4() const { return ___sceneController_4; }
	inline Listening2SceneController_t3779648327 ** get_address_of_sceneController_4() { return &___sceneController_4; }
	inline void set_sceneController_4(Listening2SceneController_t3779648327 * value)
	{
		___sceneController_4 = value;
		Il2CppCodeGenWriteBarrier(&___sceneController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
