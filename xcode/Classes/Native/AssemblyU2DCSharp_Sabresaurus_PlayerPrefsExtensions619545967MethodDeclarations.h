﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::SetupProvider()
extern "C"  void SimpleEncryption_SetupProvider_m1804221147 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptString(System.String)
extern "C"  String_t* SimpleEncryption_EncryptString_m2634087872 (Il2CppObject * __this /* static, unused */, String_t* ___sourceString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptString(System.String)
extern "C"  String_t* SimpleEncryption_DecryptString_m1120958856 (Il2CppObject * __this /* static, unused */, String_t* ___sourceString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptFloat(System.Single)
extern "C"  String_t* SimpleEncryption_EncryptFloat_m2722438738 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptInt(System.Int32)
extern "C"  String_t* SimpleEncryption_EncryptInt_m43811697 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptFloat(System.String)
extern "C"  float SimpleEncryption_DecryptFloat_m3534434832 (Il2CppObject * __this /* static, unused */, String_t* ___sourceString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptInt(System.String)
extern "C"  int32_t SimpleEncryption_DecryptInt_m558639071 (Il2CppObject * __this /* static, unused */, String_t* ___sourceString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::.cctor()
extern "C"  void SimpleEncryption__cctor_m1687683488 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
