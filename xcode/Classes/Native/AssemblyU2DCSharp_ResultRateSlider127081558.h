﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultRateSlider
struct  ResultRateSlider_t127081558  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Slider ResultRateSlider::sliderTimeRate
	Slider_t297367283 * ___sliderTimeRate_2;
	// UnityEngine.UI.Text ResultRateSlider::percentText
	Text_t356221433 * ___percentText_3;
	// UnityEngine.RectTransform ResultRateSlider::fillCenter
	RectTransform_t3349966182 * ___fillCenter_4;
	// UnityEngine.RectTransform ResultRateSlider::percentTextLowContainer
	RectTransform_t3349966182 * ___percentTextLowContainer_5;
	// UnityEngine.UI.Slider ResultRateSlider::sliderFIT
	Slider_t297367283 * ___sliderFIT_6;
	// System.Single ResultRateSlider::lenghtSliderTimeRate
	float ___lenghtSliderTimeRate_7;

public:
	inline static int32_t get_offset_of_sliderTimeRate_2() { return static_cast<int32_t>(offsetof(ResultRateSlider_t127081558, ___sliderTimeRate_2)); }
	inline Slider_t297367283 * get_sliderTimeRate_2() const { return ___sliderTimeRate_2; }
	inline Slider_t297367283 ** get_address_of_sliderTimeRate_2() { return &___sliderTimeRate_2; }
	inline void set_sliderTimeRate_2(Slider_t297367283 * value)
	{
		___sliderTimeRate_2 = value;
		Il2CppCodeGenWriteBarrier(&___sliderTimeRate_2, value);
	}

	inline static int32_t get_offset_of_percentText_3() { return static_cast<int32_t>(offsetof(ResultRateSlider_t127081558, ___percentText_3)); }
	inline Text_t356221433 * get_percentText_3() const { return ___percentText_3; }
	inline Text_t356221433 ** get_address_of_percentText_3() { return &___percentText_3; }
	inline void set_percentText_3(Text_t356221433 * value)
	{
		___percentText_3 = value;
		Il2CppCodeGenWriteBarrier(&___percentText_3, value);
	}

	inline static int32_t get_offset_of_fillCenter_4() { return static_cast<int32_t>(offsetof(ResultRateSlider_t127081558, ___fillCenter_4)); }
	inline RectTransform_t3349966182 * get_fillCenter_4() const { return ___fillCenter_4; }
	inline RectTransform_t3349966182 ** get_address_of_fillCenter_4() { return &___fillCenter_4; }
	inline void set_fillCenter_4(RectTransform_t3349966182 * value)
	{
		___fillCenter_4 = value;
		Il2CppCodeGenWriteBarrier(&___fillCenter_4, value);
	}

	inline static int32_t get_offset_of_percentTextLowContainer_5() { return static_cast<int32_t>(offsetof(ResultRateSlider_t127081558, ___percentTextLowContainer_5)); }
	inline RectTransform_t3349966182 * get_percentTextLowContainer_5() const { return ___percentTextLowContainer_5; }
	inline RectTransform_t3349966182 ** get_address_of_percentTextLowContainer_5() { return &___percentTextLowContainer_5; }
	inline void set_percentTextLowContainer_5(RectTransform_t3349966182 * value)
	{
		___percentTextLowContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___percentTextLowContainer_5, value);
	}

	inline static int32_t get_offset_of_sliderFIT_6() { return static_cast<int32_t>(offsetof(ResultRateSlider_t127081558, ___sliderFIT_6)); }
	inline Slider_t297367283 * get_sliderFIT_6() const { return ___sliderFIT_6; }
	inline Slider_t297367283 ** get_address_of_sliderFIT_6() { return &___sliderFIT_6; }
	inline void set_sliderFIT_6(Slider_t297367283 * value)
	{
		___sliderFIT_6 = value;
		Il2CppCodeGenWriteBarrier(&___sliderFIT_6, value);
	}

	inline static int32_t get_offset_of_lenghtSliderTimeRate_7() { return static_cast<int32_t>(offsetof(ResultRateSlider_t127081558, ___lenghtSliderTimeRate_7)); }
	inline float get_lenghtSliderTimeRate_7() const { return ___lenghtSliderTimeRate_7; }
	inline float* get_address_of_lenghtSliderTimeRate_7() { return &___lenghtSliderTimeRate_7; }
	inline void set_lenghtSliderTimeRate_7(float value)
	{
		___lenghtSliderTimeRate_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
