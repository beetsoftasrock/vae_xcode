﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CallHelpInSceneScript
struct  CallHelpInSceneScript_t2709929151  : public MonoBehaviour_t1158329972
{
public:
	// System.String CallHelpInSceneScript::prefixNameHelpFor
	String_t* ___prefixNameHelpFor_2;
	// System.String CallHelpInSceneScript::bundleName
	String_t* ___bundleName_3;

public:
	inline static int32_t get_offset_of_prefixNameHelpFor_2() { return static_cast<int32_t>(offsetof(CallHelpInSceneScript_t2709929151, ___prefixNameHelpFor_2)); }
	inline String_t* get_prefixNameHelpFor_2() const { return ___prefixNameHelpFor_2; }
	inline String_t** get_address_of_prefixNameHelpFor_2() { return &___prefixNameHelpFor_2; }
	inline void set_prefixNameHelpFor_2(String_t* value)
	{
		___prefixNameHelpFor_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefixNameHelpFor_2, value);
	}

	inline static int32_t get_offset_of_bundleName_3() { return static_cast<int32_t>(offsetof(CallHelpInSceneScript_t2709929151, ___bundleName_3)); }
	inline String_t* get_bundleName_3() const { return ___bundleName_3; }
	inline String_t** get_address_of_bundleName_3() { return &___bundleName_3; }
	inline void set_bundleName_3(String_t* value)
	{
		___bundleName_3 = value;
		Il2CppCodeGenWriteBarrier(&___bundleName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
