﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// CharacterSelector
struct CharacterSelector_t2041732578;

#include "AssemblyU2DCSharp_UIController2029583246.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationController
struct  ConversationController_t3486580231  : public UIController_t2029583246
{
public:
	// UnityEngine.UI.Button ConversationController::btnStartMenu
	Button_t2872111280 * ___btnStartMenu_2;
	// UnityEngine.UI.Image ConversationController::imgBg
	Image_t2042527209 * ___imgBg_3;
	// UnityEngine.UI.Image ConversationController::imgCharacter
	Image_t2042527209 * ___imgCharacter_4;
	// UnityEngine.GameObject ConversationController::conversationSelector
	GameObject_t1756533147 * ___conversationSelector_5;
	// CharacterSelector ConversationController::characterSelector
	CharacterSelector_t2041732578 * ___characterSelector_6;
	// UnityEngine.GameObject ConversationController::dialogPrefab
	GameObject_t1756533147 * ___dialogPrefab_7;

public:
	inline static int32_t get_offset_of_btnStartMenu_2() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231, ___btnStartMenu_2)); }
	inline Button_t2872111280 * get_btnStartMenu_2() const { return ___btnStartMenu_2; }
	inline Button_t2872111280 ** get_address_of_btnStartMenu_2() { return &___btnStartMenu_2; }
	inline void set_btnStartMenu_2(Button_t2872111280 * value)
	{
		___btnStartMenu_2 = value;
		Il2CppCodeGenWriteBarrier(&___btnStartMenu_2, value);
	}

	inline static int32_t get_offset_of_imgBg_3() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231, ___imgBg_3)); }
	inline Image_t2042527209 * get_imgBg_3() const { return ___imgBg_3; }
	inline Image_t2042527209 ** get_address_of_imgBg_3() { return &___imgBg_3; }
	inline void set_imgBg_3(Image_t2042527209 * value)
	{
		___imgBg_3 = value;
		Il2CppCodeGenWriteBarrier(&___imgBg_3, value);
	}

	inline static int32_t get_offset_of_imgCharacter_4() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231, ___imgCharacter_4)); }
	inline Image_t2042527209 * get_imgCharacter_4() const { return ___imgCharacter_4; }
	inline Image_t2042527209 ** get_address_of_imgCharacter_4() { return &___imgCharacter_4; }
	inline void set_imgCharacter_4(Image_t2042527209 * value)
	{
		___imgCharacter_4 = value;
		Il2CppCodeGenWriteBarrier(&___imgCharacter_4, value);
	}

	inline static int32_t get_offset_of_conversationSelector_5() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231, ___conversationSelector_5)); }
	inline GameObject_t1756533147 * get_conversationSelector_5() const { return ___conversationSelector_5; }
	inline GameObject_t1756533147 ** get_address_of_conversationSelector_5() { return &___conversationSelector_5; }
	inline void set_conversationSelector_5(GameObject_t1756533147 * value)
	{
		___conversationSelector_5 = value;
		Il2CppCodeGenWriteBarrier(&___conversationSelector_5, value);
	}

	inline static int32_t get_offset_of_characterSelector_6() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231, ___characterSelector_6)); }
	inline CharacterSelector_t2041732578 * get_characterSelector_6() const { return ___characterSelector_6; }
	inline CharacterSelector_t2041732578 ** get_address_of_characterSelector_6() { return &___characterSelector_6; }
	inline void set_characterSelector_6(CharacterSelector_t2041732578 * value)
	{
		___characterSelector_6 = value;
		Il2CppCodeGenWriteBarrier(&___characterSelector_6, value);
	}

	inline static int32_t get_offset_of_dialogPrefab_7() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231, ___dialogPrefab_7)); }
	inline GameObject_t1756533147 * get_dialogPrefab_7() const { return ___dialogPrefab_7; }
	inline GameObject_t1756533147 ** get_address_of_dialogPrefab_7() { return &___dialogPrefab_7; }
	inline void set_dialogPrefab_7(GameObject_t1756533147 * value)
	{
		___dialogPrefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___dialogPrefab_7, value);
	}
};

struct ConversationController_t3486580231_StaticFields
{
public:
	// UnityEngine.UI.Button ConversationController::btnModel
	Button_t2872111280 * ___btnModel_8;
	// System.Boolean ConversationController::isStarted
	bool ___isStarted_9;
	// System.Boolean ConversationController::isSelectedConversation
	bool ___isSelectedConversation_10;

public:
	inline static int32_t get_offset_of_btnModel_8() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231_StaticFields, ___btnModel_8)); }
	inline Button_t2872111280 * get_btnModel_8() const { return ___btnModel_8; }
	inline Button_t2872111280 ** get_address_of_btnModel_8() { return &___btnModel_8; }
	inline void set_btnModel_8(Button_t2872111280 * value)
	{
		___btnModel_8 = value;
		Il2CppCodeGenWriteBarrier(&___btnModel_8, value);
	}

	inline static int32_t get_offset_of_isStarted_9() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231_StaticFields, ___isStarted_9)); }
	inline bool get_isStarted_9() const { return ___isStarted_9; }
	inline bool* get_address_of_isStarted_9() { return &___isStarted_9; }
	inline void set_isStarted_9(bool value)
	{
		___isStarted_9 = value;
	}

	inline static int32_t get_offset_of_isSelectedConversation_10() { return static_cast<int32_t>(offsetof(ConversationController_t3486580231_StaticFields, ___isSelectedConversation_10)); }
	inline bool get_isSelectedConversation_10() const { return ___isSelectedConversation_10; }
	inline bool* get_address_of_isSelectedConversation_10() { return &___isSelectedConversation_10; }
	inline void set_isSelectedConversation_10(bool value)
	{
		___isSelectedConversation_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
