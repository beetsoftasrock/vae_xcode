﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedChapterTopSceneSetup
struct ChapterBasedChapterTopSceneSetup_t675472893;
// ChapterTopConfig
struct ChapterTopConfig_t174003822;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChapterTopConfig174003822.h"

// System.Void ChapterBasedChapterTopSceneSetup::.ctor()
extern "C"  void ChapterBasedChapterTopSceneSetup__ctor_m465952032 (ChapterBasedChapterTopSceneSetup_t675472893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedChapterTopSceneSetup::Awake()
extern "C"  void ChapterBasedChapterTopSceneSetup_Awake_m2870243357 (ChapterBasedChapterTopSceneSetup_t675472893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChapterTopConfig ChapterBasedChapterTopSceneSetup::GetConfigData(System.Int32)
extern "C"  ChapterTopConfig_t174003822 * ChapterBasedChapterTopSceneSetup_GetConfigData_m2255212688 (ChapterBasedChapterTopSceneSetup_t675472893 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedChapterTopSceneSetup::ApplyConfigData(ChapterTopConfig)
extern "C"  void ChapterBasedChapterTopSceneSetup_ApplyConfigData_m937607724 (ChapterBasedChapterTopSceneSetup_t675472893 * __this, ChapterTopConfig_t174003822 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
