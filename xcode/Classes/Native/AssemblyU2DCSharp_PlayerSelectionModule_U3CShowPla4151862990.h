﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationSelectionGroupData
struct ConversationSelectionGroupData_t3437723178;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;
// PlayerSelectionModule
struct PlayerSelectionModule_t1458090443;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0
struct  U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990  : public Il2CppObject
{
public:
	// System.Boolean PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0::<cachedShowPlayerTalkDialog>__0
	bool ___U3CcachedShowPlayerTalkDialogU3E__0_0;
	// System.Boolean PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0::<cachedShowOtherTalkDialog>__1
	bool ___U3CcachedShowOtherTalkDialogU3E__1_1;
	// ConversationSelectionGroupData PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0::data
	ConversationSelectionGroupData_t3437723178 * ___data_2;
	// ConversationSelectionData PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0::<selected>__2
	ConversationSelectionData_t4090008535 * ___U3CselectedU3E__2_3;
	// PlayerSelectionModule PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0::$this
	PlayerSelectionModule_t1458090443 * ___U24this_4;
	// System.Object PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 PlayerSelectionModule/<ShowPlayerSelections>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CcachedShowPlayerTalkDialogU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990, ___U3CcachedShowPlayerTalkDialogU3E__0_0)); }
	inline bool get_U3CcachedShowPlayerTalkDialogU3E__0_0() const { return ___U3CcachedShowPlayerTalkDialogU3E__0_0; }
	inline bool* get_address_of_U3CcachedShowPlayerTalkDialogU3E__0_0() { return &___U3CcachedShowPlayerTalkDialogU3E__0_0; }
	inline void set_U3CcachedShowPlayerTalkDialogU3E__0_0(bool value)
	{
		___U3CcachedShowPlayerTalkDialogU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcachedShowOtherTalkDialogU3E__1_1() { return static_cast<int32_t>(offsetof(U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990, ___U3CcachedShowOtherTalkDialogU3E__1_1)); }
	inline bool get_U3CcachedShowOtherTalkDialogU3E__1_1() const { return ___U3CcachedShowOtherTalkDialogU3E__1_1; }
	inline bool* get_address_of_U3CcachedShowOtherTalkDialogU3E__1_1() { return &___U3CcachedShowOtherTalkDialogU3E__1_1; }
	inline void set_U3CcachedShowOtherTalkDialogU3E__1_1(bool value)
	{
		___U3CcachedShowOtherTalkDialogU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990, ___data_2)); }
	inline ConversationSelectionGroupData_t3437723178 * get_data_2() const { return ___data_2; }
	inline ConversationSelectionGroupData_t3437723178 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(ConversationSelectionGroupData_t3437723178 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier(&___data_2, value);
	}

	inline static int32_t get_offset_of_U3CselectedU3E__2_3() { return static_cast<int32_t>(offsetof(U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990, ___U3CselectedU3E__2_3)); }
	inline ConversationSelectionData_t4090008535 * get_U3CselectedU3E__2_3() const { return ___U3CselectedU3E__2_3; }
	inline ConversationSelectionData_t4090008535 ** get_address_of_U3CselectedU3E__2_3() { return &___U3CselectedU3E__2_3; }
	inline void set_U3CselectedU3E__2_3(ConversationSelectionData_t4090008535 * value)
	{
		___U3CselectedU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CselectedU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990, ___U24this_4)); }
	inline PlayerSelectionModule_t1458090443 * get_U24this_4() const { return ___U24this_4; }
	inline PlayerSelectionModule_t1458090443 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(PlayerSelectionModule_t1458090443 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
