﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApiConnector/<RequestApi>c__AnonStorey1`1<System.Object>
struct U3CRequestApiU3Ec__AnonStorey1_1_t4122123012;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void ApiConnector/<RequestApi>c__AnonStorey1`1<System.Object>::.ctor()
extern "C"  void U3CRequestApiU3Ec__AnonStorey1_1__ctor_m3410384123_gshared (U3CRequestApiU3Ec__AnonStorey1_1_t4122123012 * __this, const MethodInfo* method);
#define U3CRequestApiU3Ec__AnonStorey1_1__ctor_m3410384123(__this, method) ((  void (*) (U3CRequestApiU3Ec__AnonStorey1_1_t4122123012 *, const MethodInfo*))U3CRequestApiU3Ec__AnonStorey1_1__ctor_m3410384123_gshared)(__this, method)
// System.Void ApiConnector/<RequestApi>c__AnonStorey1`1<System.Object>::<>m__0(T)
extern "C"  void U3CRequestApiU3Ec__AnonStorey1_1_U3CU3Em__0_m2266340076_gshared (U3CRequestApiU3Ec__AnonStorey1_1_t4122123012 * __this, Il2CppObject * ___response0, const MethodInfo* method);
#define U3CRequestApiU3Ec__AnonStorey1_1_U3CU3Em__0_m2266340076(__this, ___response0, method) ((  void (*) (U3CRequestApiU3Ec__AnonStorey1_1_t4122123012 *, Il2CppObject *, const MethodInfo*))U3CRequestApiU3Ec__AnonStorey1_1_U3CU3Em__0_m2266340076_gshared)(__this, ___response0, method)
