﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalConfig
struct GlobalConfig_t3080413471;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalConfig::.ctor()
extern "C"  void GlobalConfig__ctor_m1802161260 (GlobalConfig_t3080413471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
