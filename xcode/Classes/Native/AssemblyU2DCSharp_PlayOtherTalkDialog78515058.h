﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// KaraokeTextEffect
struct KaraokeTextEffect_t1279919556;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayOtherTalkDialog
struct  PlayOtherTalkDialog_t78515058  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayOtherTalkDialog::_showSubText
	bool ____showSubText_2;
	// KaraokeTextEffect PlayOtherTalkDialog::enTextEffect
	KaraokeTextEffect_t1279919556 * ___enTextEffect_3;
	// KaraokeTextEffect PlayOtherTalkDialog::jpTextEffect
	KaraokeTextEffect_t1279919556 * ___jpTextEffect_4;

public:
	inline static int32_t get_offset_of__showSubText_2() { return static_cast<int32_t>(offsetof(PlayOtherTalkDialog_t78515058, ____showSubText_2)); }
	inline bool get__showSubText_2() const { return ____showSubText_2; }
	inline bool* get_address_of__showSubText_2() { return &____showSubText_2; }
	inline void set__showSubText_2(bool value)
	{
		____showSubText_2 = value;
	}

	inline static int32_t get_offset_of_enTextEffect_3() { return static_cast<int32_t>(offsetof(PlayOtherTalkDialog_t78515058, ___enTextEffect_3)); }
	inline KaraokeTextEffect_t1279919556 * get_enTextEffect_3() const { return ___enTextEffect_3; }
	inline KaraokeTextEffect_t1279919556 ** get_address_of_enTextEffect_3() { return &___enTextEffect_3; }
	inline void set_enTextEffect_3(KaraokeTextEffect_t1279919556 * value)
	{
		___enTextEffect_3 = value;
		Il2CppCodeGenWriteBarrier(&___enTextEffect_3, value);
	}

	inline static int32_t get_offset_of_jpTextEffect_4() { return static_cast<int32_t>(offsetof(PlayOtherTalkDialog_t78515058, ___jpTextEffect_4)); }
	inline KaraokeTextEffect_t1279919556 * get_jpTextEffect_4() const { return ___jpTextEffect_4; }
	inline KaraokeTextEffect_t1279919556 ** get_address_of_jpTextEffect_4() { return &___jpTextEffect_4; }
	inline void set_jpTextEffect_4(KaraokeTextEffect_t1279919556 * value)
	{
		___jpTextEffect_4 = value;
		Il2CppCodeGenWriteBarrier(&___jpTextEffect_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
