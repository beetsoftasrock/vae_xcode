﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetProductID
struct GetProductID_t3571953046;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GetProductID::.ctor()
extern "C"  void GetProductID__ctor_m1969839327 (GetProductID_t3571953046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GetProductID::get_ProductID()
extern "C"  String_t* GetProductID_get_ProductID_m2700077565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetProductID::set_ProductID(System.String)
extern "C"  void GetProductID_set_ProductID_m321131134 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetProductID::Awake()
extern "C"  void GetProductID_Awake_m3855027698 (GetProductID_t3571953046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetProductID::HandleRemoteUpdate()
extern "C"  void GetProductID_HandleRemoteUpdate_m86517244 (GetProductID_t3571953046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
