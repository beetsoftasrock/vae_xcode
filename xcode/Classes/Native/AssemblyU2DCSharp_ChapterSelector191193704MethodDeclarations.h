﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterSelector
struct ChapterSelector_t191193704;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// ChapterItem
struct ChapterItem_t2155291334;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChapterSelector191193704.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void ChapterSelector::.ctor()
extern "C"  void ChapterSelector__ctor_m3556486475 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChapterSelector ChapterSelector::get_Instance()
extern "C"  ChapterSelector_t191193704 * ChapterSelector_get_Instance_m1448229938 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::set_Instance(ChapterSelector)
extern "C"  void ChapterSelector_set_Instance_m602018867 (Il2CppObject * __this /* static, unused */, ChapterSelector_t191193704 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::UpdateCache(System.Single,UnityEngine.Vector2)
extern "C"  void ChapterSelector_UpdateCache_m3803770939 (ChapterSelector_t191193704 * __this, float ___time0, Vector2_t2243707579  ___mousePosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::Awake()
extern "C"  void ChapterSelector_Awake_m2338852084 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::Update()
extern "C"  void ChapterSelector_Update_m1611428902 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::OnEnable()
extern "C"  void ChapterSelector_OnEnable_m543715887 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChapterSelector::onEnable()
extern "C"  Il2CppObject * ChapterSelector_onEnable_m2531061713 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChapterSelector::refreshItemsState()
extern "C"  Il2CppObject * ChapterSelector_refreshItemsState_m2214187241 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::RefreshItemsState()
extern "C"  void ChapterSelector_RefreshItemsState_m3546415479 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::JumpToCommingsoon()
extern "C"  void ChapterSelector_JumpToCommingsoon_m981863303 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::JumpToChapter(System.Int32)
extern "C"  void ChapterSelector_JumpToChapter_m852290212 (ChapterSelector_t191193704 * __this, int32_t ___chapterId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChapterSelector::jumpToChapter(System.Int32)
extern "C"  Il2CppObject * ChapterSelector_jumpToChapter_m759869548 (ChapterSelector_t191193704 * __this, int32_t ___chapterId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ChapterSelector_OnEndDrag_m3428764129 (ChapterSelector_t191193704 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ChapterSelector_OnDrag_m816726496 (ChapterSelector_t191193704 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ChapterSelector_OnBeginDrag_m2416990613 (ChapterSelector_t191193704 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChapterItem ChapterSelector::GetNearestChapterItem()
extern "C"  ChapterItem_t2155291334 * ChapterSelector_GetNearestChapterItem_m2357516072 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChapterSelector::DofocusSelected()
extern "C"  Il2CppObject * ChapterSelector_DofocusSelected_m4123495251 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector::UpdateMap()
extern "C"  void ChapterSelector_UpdateMap_m3016317474 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChapterItem ChapterSelector::GetCurrentChapterNearestChapter()
extern "C"  ChapterItem_t2155291334 * ChapterSelector_GetCurrentChapterNearestChapter_m1197739101 (ChapterSelector_t191193704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
