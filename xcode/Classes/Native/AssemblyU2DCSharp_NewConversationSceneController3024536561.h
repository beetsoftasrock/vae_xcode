﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationSceneSelector
struct ConversationSceneSelector_t2926116424;
// CharacterSelector
struct CharacterSelector_t2041732578;

#include "AssemblyU2DCSharp_BaseConversationController808907754.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewConversationSceneController
struct  NewConversationSceneController_t3024536561  : public BaseConversationController_t808907754
{
public:
	// ConversationSceneSelector NewConversationSceneController::sceneSelector
	ConversationSceneSelector_t2926116424 * ___sceneSelector_25;
	// CharacterSelector NewConversationSceneController::characterSelector
	CharacterSelector_t2041732578 * ___characterSelector_26;

public:
	inline static int32_t get_offset_of_sceneSelector_25() { return static_cast<int32_t>(offsetof(NewConversationSceneController_t3024536561, ___sceneSelector_25)); }
	inline ConversationSceneSelector_t2926116424 * get_sceneSelector_25() const { return ___sceneSelector_25; }
	inline ConversationSceneSelector_t2926116424 ** get_address_of_sceneSelector_25() { return &___sceneSelector_25; }
	inline void set_sceneSelector_25(ConversationSceneSelector_t2926116424 * value)
	{
		___sceneSelector_25 = value;
		Il2CppCodeGenWriteBarrier(&___sceneSelector_25, value);
	}

	inline static int32_t get_offset_of_characterSelector_26() { return static_cast<int32_t>(offsetof(NewConversationSceneController_t3024536561, ___characterSelector_26)); }
	inline CharacterSelector_t2041732578 * get_characterSelector_26() const { return ___characterSelector_26; }
	inline CharacterSelector_t2041732578 ** get_address_of_characterSelector_26() { return &___characterSelector_26; }
	inline void set_characterSelector_26(CharacterSelector_t2041732578 * value)
	{
		___characterSelector_26 = value;
		Il2CppCodeGenWriteBarrier(&___characterSelector_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
