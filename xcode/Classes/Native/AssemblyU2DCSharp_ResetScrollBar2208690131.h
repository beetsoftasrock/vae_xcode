﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResetScrollBar
struct  ResetScrollBar_t2208690131  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Scrollbar ResetScrollBar::bar
	Scrollbar_t3248359358 * ___bar_2;

public:
	inline static int32_t get_offset_of_bar_2() { return static_cast<int32_t>(offsetof(ResetScrollBar_t2208690131, ___bar_2)); }
	inline Scrollbar_t3248359358 * get_bar_2() const { return ___bar_2; }
	inline Scrollbar_t3248359358 ** get_address_of_bar_2() { return &___bar_2; }
	inline void set_bar_2(Scrollbar_t3248359358 * value)
	{
		___bar_2 = value;
		Il2CppCodeGenWriteBarrier(&___bar_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
