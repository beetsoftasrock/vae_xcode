﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExcerciseCommandLoader
struct ExcerciseCommandLoader_t1886015525;

#include "codegen/il2cpp-codegen.h"

// System.Void ExcerciseCommandLoader::.ctor()
extern "C"  void ExcerciseCommandLoader__ctor_m1261591760 (ExcerciseCommandLoader_t1886015525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExcerciseCommandLoader::get_currentLevel()
extern "C"  int32_t ExcerciseCommandLoader_get_currentLevel_m2148996260 (ExcerciseCommandLoader_t1886015525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseCommandLoader::set_currentLevel(System.Int32)
extern "C"  void ExcerciseCommandLoader_set_currentLevel_m510736403 (ExcerciseCommandLoader_t1886015525 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseCommandLoader::LoadLevel(System.Int32)
extern "C"  void ExcerciseCommandLoader_LoadLevel_m1095484213 (ExcerciseCommandLoader_t1886015525 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
