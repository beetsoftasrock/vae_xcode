﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectCommand
struct  EffectCommand_t708799566  : public ConversationCommand_t3660105836
{
public:
	// System.String EffectCommand::_imageName
	String_t* ____imageName_0;
	// System.String EffectCommand::_role
	String_t* ____role_1;
	// UnityEngine.Sprite EffectCommand::_preThinkingSprite
	Sprite_t309593783 * ____preThinkingSprite_2;
	// System.Int32 EffectCommand::_preCurrentPoint
	int32_t ____preCurrentPoint_3;

public:
	inline static int32_t get_offset_of__imageName_0() { return static_cast<int32_t>(offsetof(EffectCommand_t708799566, ____imageName_0)); }
	inline String_t* get__imageName_0() const { return ____imageName_0; }
	inline String_t** get_address_of__imageName_0() { return &____imageName_0; }
	inline void set__imageName_0(String_t* value)
	{
		____imageName_0 = value;
		Il2CppCodeGenWriteBarrier(&____imageName_0, value);
	}

	inline static int32_t get_offset_of__role_1() { return static_cast<int32_t>(offsetof(EffectCommand_t708799566, ____role_1)); }
	inline String_t* get__role_1() const { return ____role_1; }
	inline String_t** get_address_of__role_1() { return &____role_1; }
	inline void set__role_1(String_t* value)
	{
		____role_1 = value;
		Il2CppCodeGenWriteBarrier(&____role_1, value);
	}

	inline static int32_t get_offset_of__preThinkingSprite_2() { return static_cast<int32_t>(offsetof(EffectCommand_t708799566, ____preThinkingSprite_2)); }
	inline Sprite_t309593783 * get__preThinkingSprite_2() const { return ____preThinkingSprite_2; }
	inline Sprite_t309593783 ** get_address_of__preThinkingSprite_2() { return &____preThinkingSprite_2; }
	inline void set__preThinkingSprite_2(Sprite_t309593783 * value)
	{
		____preThinkingSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&____preThinkingSprite_2, value);
	}

	inline static int32_t get_offset_of__preCurrentPoint_3() { return static_cast<int32_t>(offsetof(EffectCommand_t708799566, ____preCurrentPoint_3)); }
	inline int32_t get__preCurrentPoint_3() const { return ____preCurrentPoint_3; }
	inline int32_t* get_address_of__preCurrentPoint_3() { return &____preCurrentPoint_3; }
	inline void set__preCurrentPoint_3(int32_t value)
	{
		____preCurrentPoint_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
