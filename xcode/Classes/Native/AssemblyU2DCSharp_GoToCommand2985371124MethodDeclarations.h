﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToCommand
struct GoToCommand_t2985371124;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationLogicController
struct ConversationLogicController_t3911886281;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ConversationLogicController3911886281.h"

// System.Void GoToCommand::.ctor(System.String)
extern "C"  void GoToCommand__ctor_m2998123485 (GoToCommand_t2985371124 * __this, String_t* ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GoToCommand::AutoExecute(ConversationLogicController)
extern "C"  Il2CppObject * GoToCommand_AutoExecute_m3507376672 (GoToCommand_t2985371124 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoToCommand::GetRole()
extern "C"  String_t* GoToCommand_GetRole_m2446016938 (GoToCommand_t2985371124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GoToCommand::Execute(ConversationLogicController)
extern "C"  Il2CppObject * GoToCommand_Execute_m2812840689 (GoToCommand_t2985371124 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToCommand::Revert(ConversationLogicController)
extern "C"  void GoToCommand_Revert_m2466601552 (GoToCommand_t2985371124 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
