﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedConversationSceneSetup
struct ChapterBasedConversationSceneSetup_t3163186770;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedConversationSceneSetup::.ctor()
extern "C"  void ChapterBasedConversationSceneSetup__ctor_m3667657763 (ChapterBasedConversationSceneSetup_t3163186770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChapterBasedConversationSceneSetup::get_lessonName()
extern "C"  String_t* ChapterBasedConversationSceneSetup_get_lessonName_m660782110 (ChapterBasedConversationSceneSetup_t3163186770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedConversationSceneSetup::Awake()
extern "C"  void ChapterBasedConversationSceneSetup_Awake_m1260618990 (ChapterBasedConversationSceneSetup_t3163186770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
