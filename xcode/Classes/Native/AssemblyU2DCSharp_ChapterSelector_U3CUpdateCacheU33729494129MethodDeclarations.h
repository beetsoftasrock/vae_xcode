﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterSelector/<UpdateCache>c__AnonStorey4
struct U3CUpdateCacheU3Ec__AnonStorey4_t3729494129;
// CachedDragInfo
struct CachedDragInfo_t1136705792;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CachedDragInfo1136705792.h"

// System.Void ChapterSelector/<UpdateCache>c__AnonStorey4::.ctor()
extern "C"  void U3CUpdateCacheU3Ec__AnonStorey4__ctor_m3566136064 (U3CUpdateCacheU3Ec__AnonStorey4_t3729494129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterSelector/<UpdateCache>c__AnonStorey4::<>m__0(CachedDragInfo)
extern "C"  bool U3CUpdateCacheU3Ec__AnonStorey4_U3CU3Em__0_m2980184445 (U3CUpdateCacheU3Ec__AnonStorey4_t3729494129 * __this, CachedDragInfo_t1136705792 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
