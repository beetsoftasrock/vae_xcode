﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.beetsoft.utility.SoundManager/BundleSoundName
struct BundleSoundName_t552765882;
struct BundleSoundName_t552765882_marshaled_pinvoke;
struct BundleSoundName_t552765882_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct BundleSoundName_t552765882;
struct BundleSoundName_t552765882_marshaled_pinvoke;

extern "C" void BundleSoundName_t552765882_marshal_pinvoke(const BundleSoundName_t552765882& unmarshaled, BundleSoundName_t552765882_marshaled_pinvoke& marshaled);
extern "C" void BundleSoundName_t552765882_marshal_pinvoke_back(const BundleSoundName_t552765882_marshaled_pinvoke& marshaled, BundleSoundName_t552765882& unmarshaled);
extern "C" void BundleSoundName_t552765882_marshal_pinvoke_cleanup(BundleSoundName_t552765882_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct BundleSoundName_t552765882;
struct BundleSoundName_t552765882_marshaled_com;

extern "C" void BundleSoundName_t552765882_marshal_com(const BundleSoundName_t552765882& unmarshaled, BundleSoundName_t552765882_marshaled_com& marshaled);
extern "C" void BundleSoundName_t552765882_marshal_com_back(const BundleSoundName_t552765882_marshaled_com& marshaled, BundleSoundName_t552765882& unmarshaled);
extern "C" void BundleSoundName_t552765882_marshal_com_cleanup(BundleSoundName_t552765882_marshaled_com& marshaled);
