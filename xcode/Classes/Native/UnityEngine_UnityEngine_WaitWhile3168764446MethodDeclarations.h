﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WaitWhile
struct WaitWhile_t3168764446;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.WaitWhile::.ctor(System.Func`1<System.Boolean>)
extern "C"  void WaitWhile__ctor_m32968642 (WaitWhile_t3168764446 * __this, Func_1_t1485000104 * ___predicate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WaitWhile::get_keepWaiting()
extern "C"  bool WaitWhile_get_keepWaiting_m246373616 (WaitWhile_t3168764446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
