﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionDialogVR
struct ConversationSelectionDialogVR_t87316163;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationSelectionGroupData
struct ConversationSelectionGroupData_t3437723178;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationSelectionGroupData3437723178.h"
#include "AssemblyU2DCSharp_ConversationSelectionData4090008535.h"

// System.Void ConversationSelectionDialogVR::.ctor()
extern "C"  void ConversationSelectionDialogVR__ctor_m1688419886 (ConversationSelectionDialogVR_t87316163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogVR::Clear()
extern "C"  void ConversationSelectionDialogVR_Clear_m2854371451 (ConversationSelectionDialogVR_t87316163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ConversationSelectionDialogVR::ShowSelection(ConversationSelectionGroupData)
extern "C"  Il2CppObject * ConversationSelectionDialogVR_ShowSelection_m3518352937 (ConversationSelectionDialogVR_t87316163 * __this, ConversationSelectionGroupData_t3437723178 * ___gdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogVR::ShowSelectionPanel(ConversationSelectionGroupData)
extern "C"  void ConversationSelectionDialogVR_ShowSelectionPanel_m3500076497 (ConversationSelectionDialogVR_t87316163 * __this, ConversationSelectionGroupData_t3437723178 * ___gdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogVR::SetSelected(ConversationSelectionData)
extern "C"  void ConversationSelectionDialogVR_SetSelected_m3145949862 (ConversationSelectionDialogVR_t87316163 * __this, ConversationSelectionData_t4090008535 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationSelectionData ConversationSelectionDialogVR::GetSelected()
extern "C"  ConversationSelectionData_t4090008535 * ConversationSelectionDialogVR_GetSelected_m53007103 (ConversationSelectionDialogVR_t87316163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
