﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Sentence2DataLoader
struct Sentence2DataLoader_t844185122;
// Sentence2SceneController
struct Sentence2SceneController_t1874814399;

#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterBasedSentence2SceneSetup
struct  ChapterBasedSentence2SceneSetup_t1868199748  : public GlobalBasedSetupModule_t1655890765
{
public:
	// Sentence2DataLoader ChapterBasedSentence2SceneSetup::dataLoader
	Sentence2DataLoader_t844185122 * ___dataLoader_3;
	// Sentence2SceneController ChapterBasedSentence2SceneSetup::sceneController
	Sentence2SceneController_t1874814399 * ___sceneController_4;

public:
	inline static int32_t get_offset_of_dataLoader_3() { return static_cast<int32_t>(offsetof(ChapterBasedSentence2SceneSetup_t1868199748, ___dataLoader_3)); }
	inline Sentence2DataLoader_t844185122 * get_dataLoader_3() const { return ___dataLoader_3; }
	inline Sentence2DataLoader_t844185122 ** get_address_of_dataLoader_3() { return &___dataLoader_3; }
	inline void set_dataLoader_3(Sentence2DataLoader_t844185122 * value)
	{
		___dataLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_3, value);
	}

	inline static int32_t get_offset_of_sceneController_4() { return static_cast<int32_t>(offsetof(ChapterBasedSentence2SceneSetup_t1868199748, ___sceneController_4)); }
	inline Sentence2SceneController_t1874814399 * get_sceneController_4() const { return ___sceneController_4; }
	inline Sentence2SceneController_t1874814399 ** get_address_of_sceneController_4() { return &___sceneController_4; }
	inline void set_sceneController_4(Sentence2SceneController_t1874814399 * value)
	{
		___sceneController_4 = value;
		Il2CppCodeGenWriteBarrier(&___sceneController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
