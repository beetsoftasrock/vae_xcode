﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionButtonView
struct ConversationSelectionButtonView_t265815670;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationSelectionData4090008535.h"

// System.Void ConversationSelectionButtonView::.ctor()
extern "C"  void ConversationSelectionButtonView__ctor_m3558285833 (ConversationSelectionButtonView_t265815670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionButtonView::SetData(System.Int32,ConversationSelectionData)
extern "C"  void ConversationSelectionButtonView_SetData_m471056683 (ConversationSelectionButtonView_t265815670 * __this, int32_t ___id0, ConversationSelectionData_t4090008535 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionButtonView::OnClick()
extern "C"  void ConversationSelectionButtonView_OnClick_m3112154690 (ConversationSelectionButtonView_t265815670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
