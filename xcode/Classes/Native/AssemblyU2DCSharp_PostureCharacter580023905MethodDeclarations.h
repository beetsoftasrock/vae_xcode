﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PostureCharacter
struct PostureCharacter_t580023905;

#include "codegen/il2cpp-codegen.h"

// System.Void PostureCharacter::.ctor()
extern "C"  void PostureCharacter__ctor_m924593066 (PostureCharacter_t580023905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
