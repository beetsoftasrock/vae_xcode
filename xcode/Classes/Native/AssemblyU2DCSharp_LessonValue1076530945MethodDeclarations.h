﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LessonValue
struct LessonValue_t1076530945;

#include "codegen/il2cpp-codegen.h"

// System.Void LessonValue::.ctor()
extern "C"  void LessonValue__ctor_m126195860 (LessonValue_t1076530945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
