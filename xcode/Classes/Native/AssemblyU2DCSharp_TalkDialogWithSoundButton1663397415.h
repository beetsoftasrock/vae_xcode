﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;

#include "AssemblyU2DCSharp_TalkDialog1412511810.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkDialogWithSoundButton
struct  TalkDialogWithSoundButton_t1663397415  : public TalkDialog_t1412511810
{
public:
	// UnityEngine.UI.Button TalkDialogWithSoundButton::soundButton
	Button_t2872111280 * ___soundButton_6;

public:
	inline static int32_t get_offset_of_soundButton_6() { return static_cast<int32_t>(offsetof(TalkDialogWithSoundButton_t1663397415, ___soundButton_6)); }
	inline Button_t2872111280 * get_soundButton_6() const { return ___soundButton_6; }
	inline Button_t2872111280 ** get_address_of_soundButton_6() { return &___soundButton_6; }
	inline void set_soundButton_6(Button_t2872111280 * value)
	{
		___soundButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___soundButton_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
