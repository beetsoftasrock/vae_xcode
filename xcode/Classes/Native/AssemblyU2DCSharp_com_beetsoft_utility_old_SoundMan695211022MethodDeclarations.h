﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.beetsoft.utility_old.SoundManager
struct SoundManager_t695211022;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"

// System.Void com.beetsoft.utility_old.SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m2144231178 (SoundManager_t695211022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::StopAllOneShoot()
extern "C"  void SoundManager_StopAllOneShoot_m3141939934 (SoundManager_t695211022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip com.beetsoft.utility_old.SoundManager::TestGetAudioClip(System.String)
extern "C"  AudioClip_t1932558630 * SoundManager_TestGetAudioClip_m313983416 (SoundManager_t695211022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::ClearDicSoundCommand()
extern "C"  void SoundManager_ClearDicSoundCommand_m1196040275 (SoundManager_t695211022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip com.beetsoft.utility_old.SoundManager::GetAudioClip(System.String)
extern "C"  AudioClip_t1932558630 * SoundManager_GetAudioClip_m1429092356 (SoundManager_t695211022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::LoadSound(System.String)
extern "C"  void SoundManager_LoadSound_m2208345607 (SoundManager_t695211022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::UnloadSound(System.String)
extern "C"  void SoundManager_UnloadSound_m2228983970 (SoundManager_t695211022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::UnloadAllSound()
extern "C"  void SoundManager_UnloadAllSound_m3228545427 (SoundManager_t695211022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::PlayBGM(System.String)
extern "C"  void SoundManager_PlayBGM_m3387945188 (SoundManager_t695211022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::StopBGM()
extern "C"  void SoundManager_StopBGM_m4058878188 (SoundManager_t695211022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::PlayOneShoot(UnityEngine.AudioClip)
extern "C"  void SoundManager_PlayOneShoot_m1637000210 (SoundManager_t695211022 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::PlayOneShoot(System.String)
extern "C"  void SoundManager_PlayOneShoot_m1147782573 (SoundManager_t695211022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::PlaySingle(System.String)
extern "C"  void SoundManager_PlaySingle_m1543931788 (SoundManager_t695211022 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::StopAll()
extern "C"  void SoundManager_StopAll_m1379633217 (SoundManager_t695211022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// com.beetsoft.utility_old.SoundManager com.beetsoft.utility_old.SoundManager::get_instance()
extern "C"  SoundManager_t695211022 * SoundManager_get_instance_m2766968434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator com.beetsoft.utility_old.SoundManager::LoadSoundFromExFile(System.String)
extern "C"  Il2CppObject * SoundManager_LoadSoundFromExFile_m1013522456 (SoundManager_t695211022 * __this, String_t* ___namePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager::OnDestroy()
extern "C"  void SoundManager_OnDestroy_m201489215 (SoundManager_t695211022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
