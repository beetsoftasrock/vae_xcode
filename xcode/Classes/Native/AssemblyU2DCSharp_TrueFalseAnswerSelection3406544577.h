﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrueFalseAnswerSelection
struct  TrueFalseAnswerSelection_t3406544577  : public Il2CppObject
{
public:
	// System.String TrueFalseAnswerSelection::answerText
	String_t* ___answerText_0;
	// System.Boolean TrueFalseAnswerSelection::isCorrect
	bool ___isCorrect_1;

public:
	inline static int32_t get_offset_of_answerText_0() { return static_cast<int32_t>(offsetof(TrueFalseAnswerSelection_t3406544577, ___answerText_0)); }
	inline String_t* get_answerText_0() const { return ___answerText_0; }
	inline String_t** get_address_of_answerText_0() { return &___answerText_0; }
	inline void set_answerText_0(String_t* value)
	{
		___answerText_0 = value;
		Il2CppCodeGenWriteBarrier(&___answerText_0, value);
	}

	inline static int32_t get_offset_of_isCorrect_1() { return static_cast<int32_t>(offsetof(TrueFalseAnswerSelection_t3406544577, ___isCorrect_1)); }
	inline bool get_isCorrect_1() const { return ___isCorrect_1; }
	inline bool* get_address_of_isCorrect_1() { return &___isCorrect_1; }
	inline void set_isCorrect_1(bool value)
	{
		___isCorrect_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
