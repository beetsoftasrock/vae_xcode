﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Action
struct Action_t3226471752;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterMyPageScript/<waitFadeOut>c__Iterator0
struct  U3CwaitFadeOutU3Ec__Iterator0_t1364814582  : public Il2CppObject
{
public:
	// UnityEngine.GameObject ChapterMyPageScript/<waitFadeOut>c__Iterator0::<fadingObj>__0
	GameObject_t1756533147 * ___U3CfadingObjU3E__0_0;
	// UnityEngine.UI.Image ChapterMyPageScript/<waitFadeOut>c__Iterator0::<fadingImg>__1
	Image_t2042527209 * ___U3CfadingImgU3E__1_1;
	// System.Action ChapterMyPageScript/<waitFadeOut>c__Iterator0::onFinish
	Action_t3226471752 * ___onFinish_2;
	// System.Object ChapterMyPageScript/<waitFadeOut>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean ChapterMyPageScript/<waitFadeOut>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 ChapterMyPageScript/<waitFadeOut>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CfadingObjU3E__0_0() { return static_cast<int32_t>(offsetof(U3CwaitFadeOutU3Ec__Iterator0_t1364814582, ___U3CfadingObjU3E__0_0)); }
	inline GameObject_t1756533147 * get_U3CfadingObjU3E__0_0() const { return ___U3CfadingObjU3E__0_0; }
	inline GameObject_t1756533147 ** get_address_of_U3CfadingObjU3E__0_0() { return &___U3CfadingObjU3E__0_0; }
	inline void set_U3CfadingObjU3E__0_0(GameObject_t1756533147 * value)
	{
		___U3CfadingObjU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfadingObjU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CfadingImgU3E__1_1() { return static_cast<int32_t>(offsetof(U3CwaitFadeOutU3Ec__Iterator0_t1364814582, ___U3CfadingImgU3E__1_1)); }
	inline Image_t2042527209 * get_U3CfadingImgU3E__1_1() const { return ___U3CfadingImgU3E__1_1; }
	inline Image_t2042527209 ** get_address_of_U3CfadingImgU3E__1_1() { return &___U3CfadingImgU3E__1_1; }
	inline void set_U3CfadingImgU3E__1_1(Image_t2042527209 * value)
	{
		___U3CfadingImgU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfadingImgU3E__1_1, value);
	}

	inline static int32_t get_offset_of_onFinish_2() { return static_cast<int32_t>(offsetof(U3CwaitFadeOutU3Ec__Iterator0_t1364814582, ___onFinish_2)); }
	inline Action_t3226471752 * get_onFinish_2() const { return ___onFinish_2; }
	inline Action_t3226471752 ** get_address_of_onFinish_2() { return &___onFinish_2; }
	inline void set_onFinish_2(Action_t3226471752 * value)
	{
		___onFinish_2 = value;
		Il2CppCodeGenWriteBarrier(&___onFinish_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CwaitFadeOutU3Ec__Iterator0_t1364814582, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CwaitFadeOutU3Ec__Iterator0_t1364814582, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CwaitFadeOutU3Ec__Iterator0_t1364814582, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
