﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence2DataLoader
struct Sentence2DataLoader_t844185122;
// IDatasWrapper`1<SentenceListeningSpellingQuestionData>
struct IDatasWrapper_1_t859699533;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Sentence2DataLoader::.ctor()
extern "C"  void Sentence2DataLoader__ctor_m888740465 (Sentence2DataLoader_t844185122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IDatasWrapper`1<SentenceListeningSpellingQuestionData> Sentence2DataLoader::get_datasWrapper()
extern "C"  Il2CppObject* Sentence2DataLoader_get_datasWrapper_m203678941 (Sentence2DataLoader_t844185122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2DataLoader::set_datasWrapper(IDatasWrapper`1<SentenceListeningSpellingQuestionData>)
extern "C"  void Sentence2DataLoader_set_datasWrapper_m4268100286 (Sentence2DataLoader_t844185122 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Sentence2DataLoader::GetQuestionBundleText()
extern "C"  String_t* Sentence2DataLoader_GetQuestionBundleText_m2775901501 (Sentence2DataLoader_t844185122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IDatasWrapper`1<SentenceListeningSpellingQuestionData> Sentence2DataLoader::GetDatasWrapper()
extern "C"  Il2CppObject* Sentence2DataLoader_GetDatasWrapper_m3547603792 (Sentence2DataLoader_t844185122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2DataLoader::Awake()
extern "C"  void Sentence2DataLoader_Awake_m1190406290 (Sentence2DataLoader_t844185122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
