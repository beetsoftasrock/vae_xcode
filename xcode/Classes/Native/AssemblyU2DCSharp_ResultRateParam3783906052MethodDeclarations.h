﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultRateParam
struct ResultRateParam_t3783906052;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultRateParam::.ctor()
extern "C"  void ResultRateParam__ctor_m4171487187 (ResultRateParam_t3783906052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
