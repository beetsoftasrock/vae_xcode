﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2Popup
struct Listening2Popup_t2917639739;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening2Popup::.ctor()
extern "C"  void Listening2Popup__ctor_m2999414044 (Listening2Popup_t2917639739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2Popup::ClearAnswerTexts()
extern "C"  void Listening2Popup_ClearAnswerTexts_m3433365957 (Listening2Popup_t2917639739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
