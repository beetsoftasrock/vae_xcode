﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterResultSettingLoader
struct ChapterResultSettingLoader_t4141859061;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ChapterResultSettingLoader::.ctor()
extern "C"  void ChapterResultSettingLoader__ctor_m3383927838 (ChapterResultSettingLoader_t4141859061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterResultSettingLoader::set_chapter(System.Int32)
extern "C"  void ChapterResultSettingLoader_set_chapter_m1774782445 (ChapterResultSettingLoader_t4141859061 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ChapterResultSettingLoader::get_chapter()
extern "C"  int32_t ChapterResultSettingLoader_get_chapter_m1816435010 (ChapterResultSettingLoader_t4141859061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterResultSettingLoader::UpdateView()
extern "C"  void ChapterResultSettingLoader_UpdateView_m3162874292 (ChapterResultSettingLoader_t4141859061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color ChapterResultSettingLoader::GetChapterColor(System.Int32)
extern "C"  Color_t2020392075  ChapterResultSettingLoader_GetChapterColor_m2556266226 (ChapterResultSettingLoader_t4141859061 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterResultSettingLoader::.cctor()
extern "C"  void ChapterResultSettingLoader__cctor_m3256550543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
