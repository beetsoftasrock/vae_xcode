﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedTabTitleText
struct ChapterBasedTabTitleText_t2163882332;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedTabTitleText::.ctor()
extern "C"  void ChapterBasedTabTitleText__ctor_m2232598309 (ChapterBasedTabTitleText_t2163882332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedTabTitleText::Start()
extern "C"  void ChapterBasedTabTitleText_Start_m2260333501 (ChapterBasedTabTitleText_t2163882332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
