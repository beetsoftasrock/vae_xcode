﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t1942475268;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.Purchaser/<ProcessPurchase>c__AnonStorey2
struct  U3CProcessPurchaseU3Ec__AnonStorey2_t2428326241  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition Beetsoft.VAE.Purchaser/<ProcessPurchase>c__AnonStorey2::productDefinition
	ProductDefinition_t1942475268 * ___productDefinition_0;

public:
	inline static int32_t get_offset_of_productDefinition_0() { return static_cast<int32_t>(offsetof(U3CProcessPurchaseU3Ec__AnonStorey2_t2428326241, ___productDefinition_0)); }
	inline ProductDefinition_t1942475268 * get_productDefinition_0() const { return ___productDefinition_0; }
	inline ProductDefinition_t1942475268 ** get_address_of_productDefinition_0() { return &___productDefinition_0; }
	inline void set_productDefinition_0(ProductDefinition_t1942475268 * value)
	{
		___productDefinition_0 = value;
		Il2CppCodeGenWriteBarrier(&___productDefinition_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
