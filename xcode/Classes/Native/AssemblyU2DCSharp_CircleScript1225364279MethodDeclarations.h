﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CircleScript
struct CircleScript_t1225364279;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void CircleScript::.ctor()
extern "C"  void CircleScript__ctor_m639716658 (CircleScript_t1225364279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CircleScript::get_Value()
extern "C"  float CircleScript_get_Value_m3773392082 (CircleScript_t1225364279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircleScript::set_Value(System.Single)
extern "C"  void CircleScript_set_Value_m2008140413 (CircleScript_t1225364279 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircleScript::Start()
extern "C"  void CircleScript_Start_m427894774 (CircleScript_t1225364279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircleScript::Update()
extern "C"  void CircleScript_Update_m3310049027 (CircleScript_t1225364279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircleScript::UpdateHeadPosition(UnityEngine.Transform,System.Single)
extern "C"  void CircleScript_UpdateHeadPosition_m2625156842 (CircleScript_t1225364279 * __this, Transform_t3275118058 * ___tr0, float ___angle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 CircleScript::Position(System.Single)
extern "C"  Vector2_t2243707579  CircleScript_Position_m1635069905 (CircleScript_t1225364279 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircleScript::InitHead()
extern "C"  void CircleScript_InitHead_m1590453624 (CircleScript_t1225364279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
