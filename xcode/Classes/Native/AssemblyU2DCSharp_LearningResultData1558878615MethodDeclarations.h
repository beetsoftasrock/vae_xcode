﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearningResultData
struct LearningResultData_t1558878615;

#include "codegen/il2cpp-codegen.h"

// System.Void LearningResultData::.ctor()
extern "C"  void LearningResultData__ctor_m1909322250 (LearningResultData_t1558878615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
