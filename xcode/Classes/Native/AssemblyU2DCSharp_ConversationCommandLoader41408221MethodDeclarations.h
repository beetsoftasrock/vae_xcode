﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationCommandLoader
struct ConversationCommandLoader_t41408221;
// System.String
struct String_t;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ConversationCommandLoader::.ctor()
extern "C"  void ConversationCommandLoader__ctor_m335608578 (ConversationCommandLoader_t41408221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ConversationCommandLoader::get_currentLevel()
extern "C"  int32_t ConversationCommandLoader_get_currentLevel_m2965915474 (ConversationCommandLoader_t41408221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationCommandLoader::set_currentLevel(System.Int32)
extern "C"  void ConversationCommandLoader_set_currentLevel_m928301891 (ConversationCommandLoader_t41408221 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationCommandLoader::LoadLevel(System.Int32,System.String)
extern "C"  void ConversationCommandLoader_LoadLevel_m814295591 (ConversationCommandLoader_t41408221 * __this, int32_t ___level0, String_t* ___playerRole1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CommandSheet/Param> ConversationCommandLoader::GetLevelData(System.Int32)
extern "C"  List_1_t3492939606 * ConversationCommandLoader_GetLevelData_m4220307546 (ConversationCommandLoader_t41408221 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
