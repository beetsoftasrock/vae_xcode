﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadChapterScript
struct LoadChapterScript_t2140695782;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadChapterScript::.ctor()
extern "C"  void LoadChapterScript__ctor_m3918127235 (LoadChapterScript_t2140695782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadChapterScript::Start()
extern "C"  void LoadChapterScript_Start_m3436406871 (LoadChapterScript_t2140695782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadChapterScript::LoadChapter(System.Int32)
extern "C"  void LoadChapterScript_LoadChapter_m542862565 (LoadChapterScript_t2140695782 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadChapterScript::ChangeColor()
extern "C"  void LoadChapterScript_ChangeColor_m692145330 (LoadChapterScript_t2140695782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadChapterScript::UnloadChapter(System.Int32)
extern "C"  void LoadChapterScript_UnloadChapter_m1593400484 (LoadChapterScript_t2140695782 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
