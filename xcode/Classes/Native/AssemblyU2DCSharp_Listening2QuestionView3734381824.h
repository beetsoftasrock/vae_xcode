﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Collections.Generic.List`1<Listening2SelectableItem>
struct List_1_t4156349602;
// AnswerSound
struct AnswerSound_t4040477861;
// QuestionScenePopup
struct QuestionScenePopup_t792224618;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t390723249;

#include "AssemblyU2DCSharp_BaseQuestionView79922856.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening2QuestionView
struct  Listening2QuestionView_t3734381824  : public BaseQuestionView_t79922856
{
public:
	// UnityEngine.GameObject Listening2QuestionView::answerViewPrefab
	GameObject_t1756533147 * ___answerViewPrefab_3;
	// UnityEngine.GameObject Listening2QuestionView::answerContainer
	GameObject_t1756533147 * ___answerContainer_4;
	// UnityEngine.UI.Text Listening2QuestionView::questionText
	Text_t356221433 * ___questionText_5;
	// UnityEngine.UI.Button Listening2QuestionView::questionSoundButton
	Button_t2872111280 * ___questionSoundButton_6;
	// UnityEngine.UI.Button Listening2QuestionView::summitButton
	Button_t2872111280 * ___summitButton_7;
	// System.Collections.Generic.List`1<Listening2SelectableItem> Listening2QuestionView::answers
	List_1_t4156349602 * ___answers_8;
	// AnswerSound Listening2QuestionView::answerSound
	AnswerSound_t4040477861 * ___answerSound_9;
	// QuestionScenePopup Listening2QuestionView::popup
	QuestionScenePopup_t792224618 * ___popup_10;
	// System.Collections.Generic.List`1<UnityEngine.Object> Listening2QuestionView::tempList
	List_1_t390723249 * ___tempList_11;

public:
	inline static int32_t get_offset_of_answerViewPrefab_3() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___answerViewPrefab_3)); }
	inline GameObject_t1756533147 * get_answerViewPrefab_3() const { return ___answerViewPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_answerViewPrefab_3() { return &___answerViewPrefab_3; }
	inline void set_answerViewPrefab_3(GameObject_t1756533147 * value)
	{
		___answerViewPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___answerViewPrefab_3, value);
	}

	inline static int32_t get_offset_of_answerContainer_4() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___answerContainer_4)); }
	inline GameObject_t1756533147 * get_answerContainer_4() const { return ___answerContainer_4; }
	inline GameObject_t1756533147 ** get_address_of_answerContainer_4() { return &___answerContainer_4; }
	inline void set_answerContainer_4(GameObject_t1756533147 * value)
	{
		___answerContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___answerContainer_4, value);
	}

	inline static int32_t get_offset_of_questionText_5() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___questionText_5)); }
	inline Text_t356221433 * get_questionText_5() const { return ___questionText_5; }
	inline Text_t356221433 ** get_address_of_questionText_5() { return &___questionText_5; }
	inline void set_questionText_5(Text_t356221433 * value)
	{
		___questionText_5 = value;
		Il2CppCodeGenWriteBarrier(&___questionText_5, value);
	}

	inline static int32_t get_offset_of_questionSoundButton_6() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___questionSoundButton_6)); }
	inline Button_t2872111280 * get_questionSoundButton_6() const { return ___questionSoundButton_6; }
	inline Button_t2872111280 ** get_address_of_questionSoundButton_6() { return &___questionSoundButton_6; }
	inline void set_questionSoundButton_6(Button_t2872111280 * value)
	{
		___questionSoundButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___questionSoundButton_6, value);
	}

	inline static int32_t get_offset_of_summitButton_7() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___summitButton_7)); }
	inline Button_t2872111280 * get_summitButton_7() const { return ___summitButton_7; }
	inline Button_t2872111280 ** get_address_of_summitButton_7() { return &___summitButton_7; }
	inline void set_summitButton_7(Button_t2872111280 * value)
	{
		___summitButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___summitButton_7, value);
	}

	inline static int32_t get_offset_of_answers_8() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___answers_8)); }
	inline List_1_t4156349602 * get_answers_8() const { return ___answers_8; }
	inline List_1_t4156349602 ** get_address_of_answers_8() { return &___answers_8; }
	inline void set_answers_8(List_1_t4156349602 * value)
	{
		___answers_8 = value;
		Il2CppCodeGenWriteBarrier(&___answers_8, value);
	}

	inline static int32_t get_offset_of_answerSound_9() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___answerSound_9)); }
	inline AnswerSound_t4040477861 * get_answerSound_9() const { return ___answerSound_9; }
	inline AnswerSound_t4040477861 ** get_address_of_answerSound_9() { return &___answerSound_9; }
	inline void set_answerSound_9(AnswerSound_t4040477861 * value)
	{
		___answerSound_9 = value;
		Il2CppCodeGenWriteBarrier(&___answerSound_9, value);
	}

	inline static int32_t get_offset_of_popup_10() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___popup_10)); }
	inline QuestionScenePopup_t792224618 * get_popup_10() const { return ___popup_10; }
	inline QuestionScenePopup_t792224618 ** get_address_of_popup_10() { return &___popup_10; }
	inline void set_popup_10(QuestionScenePopup_t792224618 * value)
	{
		___popup_10 = value;
		Il2CppCodeGenWriteBarrier(&___popup_10, value);
	}

	inline static int32_t get_offset_of_tempList_11() { return static_cast<int32_t>(offsetof(Listening2QuestionView_t3734381824, ___tempList_11)); }
	inline List_1_t390723249 * get_tempList_11() const { return ___tempList_11; }
	inline List_1_t390723249 ** get_address_of_tempList_11() { return &___tempList_11; }
	inline void set_tempList_11(List_1_t390723249 * value)
	{
		___tempList_11 = value;
		Il2CppCodeGenWriteBarrier(&___tempList_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
