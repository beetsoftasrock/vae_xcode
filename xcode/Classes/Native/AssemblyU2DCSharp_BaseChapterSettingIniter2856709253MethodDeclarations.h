﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseChapterSettingIniter
struct BaseChapterSettingIniter_t2856709253;
// BaseSetting
struct BaseSetting_t2575616157;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseChapterSettingIniter::.ctor()
extern "C"  void BaseChapterSettingIniter__ctor_m2498517646 (BaseChapterSettingIniter_t2856709253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseSetting BaseChapterSettingIniter::get_setting()
extern "C"  BaseSetting_t2575616157 * BaseChapterSettingIniter_get_setting_m1266183431 (BaseChapterSettingIniter_t2856709253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseChapterSettingIniter::LoadSetting()
extern "C"  void BaseChapterSettingIniter_LoadSetting_m3974079850 (BaseChapterSettingIniter_t2856709253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
