﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action
struct Action_t3226471752;
// UnityEngine.WWW
struct WWW_t2919945039;
// AssetBundles.AssetBundleManager
struct AssetBundleManager_t364944953;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2
struct  U3CloadAssetBundleU3Ec__Iterator2_t3037061210  : public Il2CppObject
{
public:
	// System.String AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::bundleName
	String_t* ___bundleName_0;
	// System.Boolean AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::<hasVariant>__0
	bool ___U3ChasVariantU3E__0_1;
	// System.String[] AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::<splited>__1
	StringU5BU5D_t1642385972* ___U3CsplitedU3E__1_2;
	// System.Action`1<System.Single> AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::onProcess
	Action_1_t1878309314 * ___onProcess_3;
	// System.Action`1<System.String> AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::onError
	Action_1_t1831019615 * ___onError_4;
	// System.String AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::<stdName>__2
	String_t* ___U3CstdNameU3E__2_5;
	// System.Action AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::onSuccess
	Action_t3226471752 * ___onSuccess_6;
	// UnityEngine.WWW AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::<download>__3
	WWW_t2919945039 * ___U3CdownloadU3E__3_7;
	// AssetBundles.AssetBundleManager AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::$this
	AssetBundleManager_t364944953 * ___U24this_8;
	// System.Object AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::$disposing
	bool ___U24disposing_10;
	// System.Int32 AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_bundleName_0() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___bundleName_0)); }
	inline String_t* get_bundleName_0() const { return ___bundleName_0; }
	inline String_t** get_address_of_bundleName_0() { return &___bundleName_0; }
	inline void set_bundleName_0(String_t* value)
	{
		___bundleName_0 = value;
		Il2CppCodeGenWriteBarrier(&___bundleName_0, value);
	}

	inline static int32_t get_offset_of_U3ChasVariantU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___U3ChasVariantU3E__0_1)); }
	inline bool get_U3ChasVariantU3E__0_1() const { return ___U3ChasVariantU3E__0_1; }
	inline bool* get_address_of_U3ChasVariantU3E__0_1() { return &___U3ChasVariantU3E__0_1; }
	inline void set_U3ChasVariantU3E__0_1(bool value)
	{
		___U3ChasVariantU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CsplitedU3E__1_2() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___U3CsplitedU3E__1_2)); }
	inline StringU5BU5D_t1642385972* get_U3CsplitedU3E__1_2() const { return ___U3CsplitedU3E__1_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CsplitedU3E__1_2() { return &___U3CsplitedU3E__1_2; }
	inline void set_U3CsplitedU3E__1_2(StringU5BU5D_t1642385972* value)
	{
		___U3CsplitedU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsplitedU3E__1_2, value);
	}

	inline static int32_t get_offset_of_onProcess_3() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___onProcess_3)); }
	inline Action_1_t1878309314 * get_onProcess_3() const { return ___onProcess_3; }
	inline Action_1_t1878309314 ** get_address_of_onProcess_3() { return &___onProcess_3; }
	inline void set_onProcess_3(Action_1_t1878309314 * value)
	{
		___onProcess_3 = value;
		Il2CppCodeGenWriteBarrier(&___onProcess_3, value);
	}

	inline static int32_t get_offset_of_onError_4() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___onError_4)); }
	inline Action_1_t1831019615 * get_onError_4() const { return ___onError_4; }
	inline Action_1_t1831019615 ** get_address_of_onError_4() { return &___onError_4; }
	inline void set_onError_4(Action_1_t1831019615 * value)
	{
		___onError_4 = value;
		Il2CppCodeGenWriteBarrier(&___onError_4, value);
	}

	inline static int32_t get_offset_of_U3CstdNameU3E__2_5() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___U3CstdNameU3E__2_5)); }
	inline String_t* get_U3CstdNameU3E__2_5() const { return ___U3CstdNameU3E__2_5; }
	inline String_t** get_address_of_U3CstdNameU3E__2_5() { return &___U3CstdNameU3E__2_5; }
	inline void set_U3CstdNameU3E__2_5(String_t* value)
	{
		___U3CstdNameU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstdNameU3E__2_5, value);
	}

	inline static int32_t get_offset_of_onSuccess_6() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___onSuccess_6)); }
	inline Action_t3226471752 * get_onSuccess_6() const { return ___onSuccess_6; }
	inline Action_t3226471752 ** get_address_of_onSuccess_6() { return &___onSuccess_6; }
	inline void set_onSuccess_6(Action_t3226471752 * value)
	{
		___onSuccess_6 = value;
		Il2CppCodeGenWriteBarrier(&___onSuccess_6, value);
	}

	inline static int32_t get_offset_of_U3CdownloadU3E__3_7() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___U3CdownloadU3E__3_7)); }
	inline WWW_t2919945039 * get_U3CdownloadU3E__3_7() const { return ___U3CdownloadU3E__3_7; }
	inline WWW_t2919945039 ** get_address_of_U3CdownloadU3E__3_7() { return &___U3CdownloadU3E__3_7; }
	inline void set_U3CdownloadU3E__3_7(WWW_t2919945039 * value)
	{
		___U3CdownloadU3E__3_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdownloadU3E__3_7, value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___U24this_8)); }
	inline AssetBundleManager_t364944953 * get_U24this_8() const { return ___U24this_8; }
	inline AssetBundleManager_t364944953 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(AssetBundleManager_t364944953 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_8, value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator2_t3037061210, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
