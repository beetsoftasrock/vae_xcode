﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayTalkDialog/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t4017322827;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ReplayTalkDialog/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m3562481868 (U3CPlayU3Ec__Iterator0_t4017322827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayTalkDialog/<Play>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m3991154464 (U3CPlayU3Ec__Iterator0_t4017322827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ReplayTalkDialog/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3674149222 (U3CPlayU3Ec__Iterator0_t4017322827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ReplayTalkDialog/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m485933438 (U3CPlayU3Ec__Iterator0_t4017322827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayTalkDialog/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m843228933 (U3CPlayU3Ec__Iterator0_t4017322827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayTalkDialog/<Play>c__Iterator0::Reset()
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m4151452343 (U3CPlayU3Ec__Iterator0_t4017322827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
