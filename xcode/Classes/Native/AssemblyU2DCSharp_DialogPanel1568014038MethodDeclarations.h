﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DialogPanel
struct DialogPanel_t1568014038;
// Dialog
struct Dialog_t1378192732;
// System.String
struct String_t;
// System.Action`1<DialogPanel>
struct Action_1_t1369813420;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Dialog1378192732.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_DialogPanel1568014038.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"

// System.Void DialogPanel::.ctor()
extern "C"  void DialogPanel__ctor_m1036309373 (DialogPanel_t1568014038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::OnEnable()
extern "C"  void DialogPanel_OnEnable_m3116753181 (DialogPanel_t1568014038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::UpdateContent()
extern "C"  void DialogPanel_UpdateContent_m1191616373 (DialogPanel_t1568014038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::UpdateContent(Dialog)
extern "C"  void DialogPanel_UpdateContent_m461017153 (DialogPanel_t1568014038 * __this, Dialog_t1378192732 * ___dialog0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::UpdateContent(System.String,System.String)
extern "C"  void DialogPanel_UpdateContent_m1203520013 (DialogPanel_t1568014038 * __this, String_t* ___content0, String_t* ___subContent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::PlaySound(System.String)
extern "C"  void DialogPanel_PlaySound_m1967840906 (DialogPanel_t1568014038 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::PlaySound(System.String,System.Action`1<DialogPanel>)
extern "C"  void DialogPanel_PlaySound_m3358237276 (DialogPanel_t1568014038 * __this, String_t* ___name0, Action_1_t1369813420 * ___Onfinish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::PlaySoundAndDisable(System.String,DialogPanel)
extern "C"  void DialogPanel_PlaySoundAndDisable_m1601465593 (DialogPanel_t1568014038 * __this, String_t* ___name0, DialogPanel_t1568014038 * ___currentDialog1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DialogPanel::PlayVoice(System.String,System.Action`1<DialogPanel>)
extern "C"  Il2CppObject * DialogPanel_PlayVoice_m3423150007 (DialogPanel_t1568014038 * __this, String_t* ___name0, Action_1_t1369813420 * ___OnFinish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DialogPanel::PlayVoiceAndDisable(System.String,DialogPanel)
extern "C"  Il2CppObject * DialogPanel_PlayVoiceAndDisable_m1453191694 (DialogPanel_t1568014038 * __this, String_t* ___name0, DialogPanel_t1568014038 * ___currentDialog1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::StartKaraoke(UnityEngine.AudioSource)
extern "C"  void DialogPanel_StartKaraoke_m3418249437 (DialogPanel_t1568014038 * __this, AudioSource_t1135106623 * ___audioSource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel::StartKaraoke()
extern "C"  void DialogPanel_StartKaraoke_m1524608001 (DialogPanel_t1568014038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
