﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>
struct KeyCollection_t2595447308;
// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>
struct Dictionary_2_t111949537;
// System.Collections.Generic.IEnumerator`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>
struct IEnumerator_1_t568685048;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair[]
struct ExtensionIntPairU5BU5D_t1445074248;

#include "codegen/il2cpp-codegen.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Exte3093161221.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2801452975.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m157823507_gshared (KeyCollection_t2595447308 * __this, Dictionary_2_t111949537 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m157823507(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2595447308 *, Dictionary_2_t111949537 *, const MethodInfo*))KeyCollection__ctor_m157823507_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151094689_gshared (KeyCollection_t2595447308 * __this, ExtensionIntPair_t3093161221  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151094689(__this, ___item0, method) ((  void (*) (KeyCollection_t2595447308 *, ExtensionIntPair_t3093161221 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151094689_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2542448756_gshared (KeyCollection_t2595447308 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2542448756(__this, method) ((  void (*) (KeyCollection_t2595447308 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2542448756_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m609886271_gshared (KeyCollection_t2595447308 * __this, ExtensionIntPair_t3093161221  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m609886271(__this, ___item0, method) ((  bool (*) (KeyCollection_t2595447308 *, ExtensionIntPair_t3093161221 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m609886271_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m481603688_gshared (KeyCollection_t2595447308 * __this, ExtensionIntPair_t3093161221  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m481603688(__this, ___item0, method) ((  bool (*) (KeyCollection_t2595447308 *, ExtensionIntPair_t3093161221 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m481603688_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m841782026_gshared (KeyCollection_t2595447308 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m841782026(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2595447308 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m841782026_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m894372018_gshared (KeyCollection_t2595447308 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m894372018(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2595447308 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m894372018_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2524436037_gshared (KeyCollection_t2595447308 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2524436037(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2595447308 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2524436037_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2353914802_gshared (KeyCollection_t2595447308 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2353914802(__this, method) ((  bool (*) (KeyCollection_t2595447308 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2353914802_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2285549930_gshared (KeyCollection_t2595447308 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2285549930(__this, method) ((  bool (*) (KeyCollection_t2595447308 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2285549930_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2305589458_gshared (KeyCollection_t2595447308 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2305589458(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2595447308 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2305589458_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1489653396_gshared (KeyCollection_t2595447308 * __this, ExtensionIntPairU5BU5D_t1445074248* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m1489653396(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2595447308 *, ExtensionIntPairU5BU5D_t1445074248*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1489653396_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2801452975  KeyCollection_GetEnumerator_m1220293775_gshared (KeyCollection_t2595447308 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1220293775(__this, method) ((  Enumerator_t2801452975  (*) (KeyCollection_t2595447308 *, const MethodInfo*))KeyCollection_GetEnumerator_m1220293775_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m776027594_gshared (KeyCollection_t2595447308 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m776027594(__this, method) ((  int32_t (*) (KeyCollection_t2595447308 *, const MethodInfo*))KeyCollection_get_Count_m776027594_gshared)(__this, method)
