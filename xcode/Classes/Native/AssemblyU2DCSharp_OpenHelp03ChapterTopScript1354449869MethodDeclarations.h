﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelp03ChapterTopScript
struct OpenHelp03ChapterTopScript_t1354449869;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void OpenHelp03ChapterTopScript::.ctor()
extern "C"  void OpenHelp03ChapterTopScript__ctor_m3224474568 (OpenHelp03ChapterTopScript_t1354449869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp03ChapterTopScript::Awake()
extern "C"  void OpenHelp03ChapterTopScript_Awake_m817923213 (OpenHelp03ChapterTopScript_t1354449869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp03ChapterTopScript::Start()
extern "C"  void OpenHelp03ChapterTopScript_Start_m2175692472 (OpenHelp03ChapterTopScript_t1354449869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp03ChapterTopScript::OnEnable()
extern "C"  void OpenHelp03ChapterTopScript_OnEnable_m409786680 (OpenHelp03ChapterTopScript_t1354449869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp03ChapterTopScript::GetObjectsBeFront(UnityEngine.Transform)
extern "C"  void OpenHelp03ChapterTopScript_GetObjectsBeFront_m772849305 (OpenHelp03ChapterTopScript_t1354449869 * __this, Transform_t3275118058 * ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
