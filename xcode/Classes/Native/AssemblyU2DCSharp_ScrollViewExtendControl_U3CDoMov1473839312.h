﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScrollViewExtendControl
struct ScrollViewExtendControl_t924978223;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollViewExtendControl/<DoMoveContent>c__Iterator0
struct  U3CDoMoveContentU3Ec__Iterator0_t1473839312  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 ScrollViewExtendControl/<DoMoveContent>c__Iterator0::amount
	Vector3_t2243707580  ___amount_0;
	// UnityEngine.Vector3 ScrollViewExtendControl/<DoMoveContent>c__Iterator0::<des>__0
	Vector3_t2243707580  ___U3CdesU3E__0_1;
	// System.Single ScrollViewExtendControl/<DoMoveContent>c__Iterator0::<moveTimeout>__1
	float ___U3CmoveTimeoutU3E__1_2;
	// ScrollViewExtendControl ScrollViewExtendControl/<DoMoveContent>c__Iterator0::$this
	ScrollViewExtendControl_t924978223 * ___U24this_3;
	// System.Object ScrollViewExtendControl/<DoMoveContent>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean ScrollViewExtendControl/<DoMoveContent>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 ScrollViewExtendControl/<DoMoveContent>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_amount_0() { return static_cast<int32_t>(offsetof(U3CDoMoveContentU3Ec__Iterator0_t1473839312, ___amount_0)); }
	inline Vector3_t2243707580  get_amount_0() const { return ___amount_0; }
	inline Vector3_t2243707580 * get_address_of_amount_0() { return &___amount_0; }
	inline void set_amount_0(Vector3_t2243707580  value)
	{
		___amount_0 = value;
	}

	inline static int32_t get_offset_of_U3CdesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoMoveContentU3Ec__Iterator0_t1473839312, ___U3CdesU3E__0_1)); }
	inline Vector3_t2243707580  get_U3CdesU3E__0_1() const { return ___U3CdesU3E__0_1; }
	inline Vector3_t2243707580 * get_address_of_U3CdesU3E__0_1() { return &___U3CdesU3E__0_1; }
	inline void set_U3CdesU3E__0_1(Vector3_t2243707580  value)
	{
		___U3CdesU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CmoveTimeoutU3E__1_2() { return static_cast<int32_t>(offsetof(U3CDoMoveContentU3Ec__Iterator0_t1473839312, ___U3CmoveTimeoutU3E__1_2)); }
	inline float get_U3CmoveTimeoutU3E__1_2() const { return ___U3CmoveTimeoutU3E__1_2; }
	inline float* get_address_of_U3CmoveTimeoutU3E__1_2() { return &___U3CmoveTimeoutU3E__1_2; }
	inline void set_U3CmoveTimeoutU3E__1_2(float value)
	{
		___U3CmoveTimeoutU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDoMoveContentU3Ec__Iterator0_t1473839312, ___U24this_3)); }
	inline ScrollViewExtendControl_t924978223 * get_U24this_3() const { return ___U24this_3; }
	inline ScrollViewExtendControl_t924978223 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ScrollViewExtendControl_t924978223 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDoMoveContentU3Ec__Iterator0_t1473839312, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDoMoveContentU3Ec__Iterator0_t1473839312, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDoMoveContentU3Ec__Iterator0_t1473839312, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
