﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_BaseData450384115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VocabularyQuestionData
struct  VocabularyQuestionData_t2021074976  : public BaseData_t450384115
{
public:
	// System.String VocabularyQuestionData::<questionText>k__BackingField
	String_t* ___U3CquestionTextU3Ek__BackingField_0;
	// System.String VocabularyQuestionData::<questionSound>k__BackingField
	String_t* ___U3CquestionSoundU3Ek__BackingField_1;
	// System.String VocabularyQuestionData::<answerText0>k__BackingField
	String_t* ___U3CanswerText0U3Ek__BackingField_2;
	// System.String VocabularyQuestionData::<answerText1>k__BackingField
	String_t* ___U3CanswerText1U3Ek__BackingField_3;
	// System.String VocabularyQuestionData::<answerText2>k__BackingField
	String_t* ___U3CanswerText2U3Ek__BackingField_4;
	// System.String VocabularyQuestionData::<answerText3>k__BackingField
	String_t* ___U3CanswerText3U3Ek__BackingField_5;
	// System.Int32 VocabularyQuestionData::<correctAnswer>k__BackingField
	int32_t ___U3CcorrectAnswerU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CquestionTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VocabularyQuestionData_t2021074976, ___U3CquestionTextU3Ek__BackingField_0)); }
	inline String_t* get_U3CquestionTextU3Ek__BackingField_0() const { return ___U3CquestionTextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CquestionTextU3Ek__BackingField_0() { return &___U3CquestionTextU3Ek__BackingField_0; }
	inline void set_U3CquestionTextU3Ek__BackingField_0(String_t* value)
	{
		___U3CquestionTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionTextU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CquestionSoundU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VocabularyQuestionData_t2021074976, ___U3CquestionSoundU3Ek__BackingField_1)); }
	inline String_t* get_U3CquestionSoundU3Ek__BackingField_1() const { return ___U3CquestionSoundU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CquestionSoundU3Ek__BackingField_1() { return &___U3CquestionSoundU3Ek__BackingField_1; }
	inline void set_U3CquestionSoundU3Ek__BackingField_1(String_t* value)
	{
		___U3CquestionSoundU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionSoundU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CanswerText0U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VocabularyQuestionData_t2021074976, ___U3CanswerText0U3Ek__BackingField_2)); }
	inline String_t* get_U3CanswerText0U3Ek__BackingField_2() const { return ___U3CanswerText0U3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CanswerText0U3Ek__BackingField_2() { return &___U3CanswerText0U3Ek__BackingField_2; }
	inline void set_U3CanswerText0U3Ek__BackingField_2(String_t* value)
	{
		___U3CanswerText0U3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanswerText0U3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CanswerText1U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VocabularyQuestionData_t2021074976, ___U3CanswerText1U3Ek__BackingField_3)); }
	inline String_t* get_U3CanswerText1U3Ek__BackingField_3() const { return ___U3CanswerText1U3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CanswerText1U3Ek__BackingField_3() { return &___U3CanswerText1U3Ek__BackingField_3; }
	inline void set_U3CanswerText1U3Ek__BackingField_3(String_t* value)
	{
		___U3CanswerText1U3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanswerText1U3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CanswerText2U3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VocabularyQuestionData_t2021074976, ___U3CanswerText2U3Ek__BackingField_4)); }
	inline String_t* get_U3CanswerText2U3Ek__BackingField_4() const { return ___U3CanswerText2U3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CanswerText2U3Ek__BackingField_4() { return &___U3CanswerText2U3Ek__BackingField_4; }
	inline void set_U3CanswerText2U3Ek__BackingField_4(String_t* value)
	{
		___U3CanswerText2U3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanswerText2U3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CanswerText3U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VocabularyQuestionData_t2021074976, ___U3CanswerText3U3Ek__BackingField_5)); }
	inline String_t* get_U3CanswerText3U3Ek__BackingField_5() const { return ___U3CanswerText3U3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CanswerText3U3Ek__BackingField_5() { return &___U3CanswerText3U3Ek__BackingField_5; }
	inline void set_U3CanswerText3U3Ek__BackingField_5(String_t* value)
	{
		___U3CanswerText3U3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanswerText3U3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CcorrectAnswerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(VocabularyQuestionData_t2021074976, ___U3CcorrectAnswerU3Ek__BackingField_6)); }
	inline int32_t get_U3CcorrectAnswerU3Ek__BackingField_6() const { return ___U3CcorrectAnswerU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CcorrectAnswerU3Ek__BackingField_6() { return &___U3CcorrectAnswerU3Ek__BackingField_6; }
	inline void set_U3CcorrectAnswerU3Ek__BackingField_6(int32_t value)
	{
		___U3CcorrectAnswerU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
