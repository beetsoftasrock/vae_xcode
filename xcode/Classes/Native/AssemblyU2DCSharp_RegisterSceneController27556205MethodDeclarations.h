﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegisterSceneController
struct RegisterSceneController_t27556205;
// System.String
struct String_t;
// System.Action`1<RegisterResponse>
struct Action_1_t212265456;
// System.Action
struct Action_t3226471752;
// RegisterResponse
struct RegisterResponse_t410466074;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_RegisterResponse410466074.h"

// System.Void RegisterSceneController::.ctor()
extern "C"  void RegisterSceneController__ctor_m1763597522 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::SummitNickname()
extern "C"  void RegisterSceneController_SummitNickname_m760518527 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::LockSummit()
extern "C"  void RegisterSceneController_LockSummit_m2163832568 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::UnlockSummit()
extern "C"  void RegisterSceneController_UnlockSummit_m569654703 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::Register(System.String,System.Action`1<RegisterResponse>,System.Action)
extern "C"  void RegisterSceneController_Register_m1565984940 (RegisterSceneController_t27556205 * __this, String_t* ___username0, Action_1_t212265456 * ___requestSuccessCallback1, Action_t3226471752 * ___requestErrorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::HandleRegisterReponse(RegisterResponse)
extern "C"  void RegisterSceneController_HandleRegisterReponse_m890228585 (RegisterSceneController_t27556205 * __this, RegisterResponse_t410466074 * ___rrd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::HandleRegisterError()
extern "C"  void RegisterSceneController_HandleRegisterError_m391028991 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::SummitAccount()
extern "C"  void RegisterSceneController_SummitAccount_m3334785328 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::NextScene()
extern "C"  void RegisterSceneController_NextScene_m3806258537 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::Awake()
extern "C"  void RegisterSceneController_Awake_m309048105 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RegisterSceneController::HasJapanCharacter(System.String)
extern "C"  bool RegisterSceneController_HasJapanCharacter_m2185740989 (RegisterSceneController_t27556205 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::OpenLoginScene()
extern "C"  void RegisterSceneController_OpenLoginScene_m3922826829 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::<SummitNickname>m__0(RegisterResponse)
extern "C"  void RegisterSceneController_U3CSummitNicknameU3Em__0_m1583626894 (RegisterSceneController_t27556205 * __this, RegisterResponse_t410466074 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneController::<SummitNickname>m__1()
extern "C"  void RegisterSceneController_U3CSummitNicknameU3Em__1_m449007061 (RegisterSceneController_t27556205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
