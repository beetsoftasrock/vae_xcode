﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerTalkModule
struct PlayerTalkModule_t3356839145;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void PlayerTalkModule::.ctor()
extern "C"  void PlayerTalkModule__ctor_m1256524286 (PlayerTalkModule_t3356839145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule::OnConversationReset()
extern "C"  void PlayerTalkModule_OnConversationReset_m2506201345 (PlayerTalkModule_t3356839145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule::OnConversationReplay()
extern "C"  void PlayerTalkModule_OnConversationReplay_m3088024875 (PlayerTalkModule_t3356839145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationTalkData PlayerTalkModule::get_currentPlayerTalkData()
extern "C"  ConversationTalkData_t1570298305 * PlayerTalkModule_get_currentPlayerTalkData_m1573962081 (PlayerTalkModule_t3356839145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule::set_currentPlayerTalkData(ConversationTalkData)
extern "C"  void PlayerTalkModule_set_currentPlayerTalkData_m1404315600 (PlayerTalkModule_t3356839145 * __this, ConversationTalkData_t1570298305 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerTalkModule::ShowPlayerTalkDialog(ConversationTalkData,UnityEngine.Transform)
extern "C"  Il2CppObject * PlayerTalkModule_ShowPlayerTalkDialog_m880178070 (PlayerTalkModule_t3356839145 * __this, ConversationTalkData_t1570298305 * ___data0, Transform_t3275118058 * ___dialogTransform1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule::PlayDefaultTalk()
extern "C"  void PlayerTalkModule_PlayDefaultTalk_m2623709185 (PlayerTalkModule_t3356839145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerTalkModule::PlaySampleTalkEffect(ConversationTalkData)
extern "C"  Il2CppObject * PlayerTalkModule_PlaySampleTalkEffect_m2391720752 (PlayerTalkModule_t3356839145 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerTalkModule::PlayPlayerAutoTalkEffect(ConversationTalkData)
extern "C"  Il2CppObject * PlayerTalkModule_PlayPlayerAutoTalkEffect_m4181444264 (PlayerTalkModule_t3356839145 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerTalkModule::PlayPlayerRecordedTalkEffect()
extern "C"  Il2CppObject * PlayerTalkModule_PlayPlayerRecordedTalkEffect_m312205788 (PlayerTalkModule_t3356839145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule::SetPlayerTalkData(ConversationTalkData)
extern "C"  void PlayerTalkModule_SetPlayerTalkData_m2856121130 (PlayerTalkModule_t3356839145 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
