﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Beetsoft.VAE.ManagerInstance
struct ManagerInstance_t2189493790;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip>
struct Dictionary_2_t3847337892;
// System.String
struct String_t;
// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.SoundManager
struct  SoundManager_t4159196980  : public Il2CppObject
{
public:

public:
};

struct SoundManager_t4159196980_StaticFields
{
public:
	// Beetsoft.VAE.ManagerInstance Beetsoft.VAE.SoundManager::instance
	ManagerInstance_t2189493790 * ___instance_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip> Beetsoft.VAE.SoundManager::loadedClips
	Dictionary_2_t3847337892 * ___loadedClips_1;
	// System.String Beetsoft.VAE.SoundManager::curSinglePlaying
	String_t* ___curSinglePlaying_2;
	// System.String Beetsoft.VAE.SoundManager::curBgMusicPlaying
	String_t* ___curBgMusicPlaying_3;
	// System.Action Beetsoft.VAE.SoundManager::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(SoundManager_t4159196980_StaticFields, ___instance_0)); }
	inline ManagerInstance_t2189493790 * get_instance_0() const { return ___instance_0; }
	inline ManagerInstance_t2189493790 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(ManagerInstance_t2189493790 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}

	inline static int32_t get_offset_of_loadedClips_1() { return static_cast<int32_t>(offsetof(SoundManager_t4159196980_StaticFields, ___loadedClips_1)); }
	inline Dictionary_2_t3847337892 * get_loadedClips_1() const { return ___loadedClips_1; }
	inline Dictionary_2_t3847337892 ** get_address_of_loadedClips_1() { return &___loadedClips_1; }
	inline void set_loadedClips_1(Dictionary_2_t3847337892 * value)
	{
		___loadedClips_1 = value;
		Il2CppCodeGenWriteBarrier(&___loadedClips_1, value);
	}

	inline static int32_t get_offset_of_curSinglePlaying_2() { return static_cast<int32_t>(offsetof(SoundManager_t4159196980_StaticFields, ___curSinglePlaying_2)); }
	inline String_t* get_curSinglePlaying_2() const { return ___curSinglePlaying_2; }
	inline String_t** get_address_of_curSinglePlaying_2() { return &___curSinglePlaying_2; }
	inline void set_curSinglePlaying_2(String_t* value)
	{
		___curSinglePlaying_2 = value;
		Il2CppCodeGenWriteBarrier(&___curSinglePlaying_2, value);
	}

	inline static int32_t get_offset_of_curBgMusicPlaying_3() { return static_cast<int32_t>(offsetof(SoundManager_t4159196980_StaticFields, ___curBgMusicPlaying_3)); }
	inline String_t* get_curBgMusicPlaying_3() const { return ___curBgMusicPlaying_3; }
	inline String_t** get_address_of_curBgMusicPlaying_3() { return &___curBgMusicPlaying_3; }
	inline void set_curBgMusicPlaying_3(String_t* value)
	{
		___curBgMusicPlaying_3 = value;
		Il2CppCodeGenWriteBarrier(&___curBgMusicPlaying_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(SoundManager_t4159196980_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
