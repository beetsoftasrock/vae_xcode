﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedButtonRecord
struct SettingBasedButtonRecord_t3548636674;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedButtonRecord::.ctor()
extern "C"  void SettingBasedButtonRecord__ctor_m1610788955 (SettingBasedButtonRecord_t3548636674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedButtonRecord::Start()
extern "C"  void SettingBasedButtonRecord_Start_m858036431 (SettingBasedButtonRecord_t3548636674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
