﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HomeCotroller
struct HomeCotroller_t2470068823;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuDragHandler
struct  MenuDragHandler_t1912038821  : public MonoBehaviour_t1158329972
{
public:
	// HomeCotroller MenuDragHandler::homeController
	HomeCotroller_t2470068823 * ___homeController_2;
	// UnityEngine.RectTransform MenuDragHandler::target
	RectTransform_t3349966182 * ___target_3;
	// UnityEngine.Vector2 MenuDragHandler::_startMousePosition
	Vector2_t2243707579  ____startMousePosition_4;
	// UnityEngine.Vector2 MenuDragHandler::_startPosition
	Vector2_t2243707579  ____startPosition_5;

public:
	inline static int32_t get_offset_of_homeController_2() { return static_cast<int32_t>(offsetof(MenuDragHandler_t1912038821, ___homeController_2)); }
	inline HomeCotroller_t2470068823 * get_homeController_2() const { return ___homeController_2; }
	inline HomeCotroller_t2470068823 ** get_address_of_homeController_2() { return &___homeController_2; }
	inline void set_homeController_2(HomeCotroller_t2470068823 * value)
	{
		___homeController_2 = value;
		Il2CppCodeGenWriteBarrier(&___homeController_2, value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(MenuDragHandler_t1912038821, ___target_3)); }
	inline RectTransform_t3349966182 * get_target_3() const { return ___target_3; }
	inline RectTransform_t3349966182 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(RectTransform_t3349966182 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier(&___target_3, value);
	}

	inline static int32_t get_offset_of__startMousePosition_4() { return static_cast<int32_t>(offsetof(MenuDragHandler_t1912038821, ____startMousePosition_4)); }
	inline Vector2_t2243707579  get__startMousePosition_4() const { return ____startMousePosition_4; }
	inline Vector2_t2243707579 * get_address_of__startMousePosition_4() { return &____startMousePosition_4; }
	inline void set__startMousePosition_4(Vector2_t2243707579  value)
	{
		____startMousePosition_4 = value;
	}

	inline static int32_t get_offset_of__startPosition_5() { return static_cast<int32_t>(offsetof(MenuDragHandler_t1912038821, ____startPosition_5)); }
	inline Vector2_t2243707579  get__startPosition_5() const { return ____startPosition_5; }
	inline Vector2_t2243707579 * get_address_of__startPosition_5() { return &____startPosition_5; }
	inline void set__startPosition_5(Vector2_t2243707579  value)
	{
		____startPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
