﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReporterGUI
struct ReporterGUI_t402918452;

#include "codegen/il2cpp-codegen.h"

// System.Void ReporterGUI::.ctor()
extern "C"  void ReporterGUI__ctor_m2946879881 (ReporterGUI_t402918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterGUI::Awake()
extern "C"  void ReporterGUI_Awake_m1250195632 (ReporterGUI_t402918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterGUI::OnGUI()
extern "C"  void ReporterGUI_OnGUI_m1915247867 (ReporterGUI_t402918452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
