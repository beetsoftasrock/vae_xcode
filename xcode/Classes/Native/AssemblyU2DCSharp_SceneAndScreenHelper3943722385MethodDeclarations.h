﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneAndScreenHelper
struct SceneAndScreenHelper_t3943722385;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SceneAndScreenHelper::.ctor()
extern "C"  void SceneAndScreenHelper__ctor_m2467017604 (SceneAndScreenHelper_t3943722385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndScreenHelper::ChangeScene(System.String)
extern "C"  void SceneAndScreenHelper_ChangeScene_m993753818 (SceneAndScreenHelper_t3943722385 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndScreenHelper::ChangeVrScene(System.String)
extern "C"  void SceneAndScreenHelper_ChangeVrScene_m167362190 (SceneAndScreenHelper_t3943722385 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndScreenHelper::ChangeScene(System.Int32)
extern "C"  void SceneAndScreenHelper_ChangeScene_m4229186141 (SceneAndScreenHelper_t3943722385 * __this, int32_t ___sceneBuildIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndScreenHelper::RotateToLandscapeLeft()
extern "C"  void SceneAndScreenHelper_RotateToLandscapeLeft_m3605078350 (SceneAndScreenHelper_t3943722385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndScreenHelper::RotateToLandscapeRight()
extern "C"  void SceneAndScreenHelper_RotateToLandscapeRight_m1964679021 (SceneAndScreenHelper_t3943722385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndScreenHelper::Portrait()
extern "C"  void SceneAndScreenHelper_Portrait_m1506338415 (SceneAndScreenHelper_t3943722385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndScreenHelper::SetExerciseAutoPlay(System.Boolean)
extern "C"  void SceneAndScreenHelper_SetExerciseAutoPlay_m1252555282 (SceneAndScreenHelper_t3943722385 * __this, bool ___auto0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndScreenHelper::SetConversationAutoPlay(System.Boolean)
extern "C"  void SceneAndScreenHelper_SetConversationAutoPlay_m1496193819 (SceneAndScreenHelper_t3943722385 * __this, bool ___auto0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
