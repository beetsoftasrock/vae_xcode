﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey9
struct U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062;

#include "codegen/il2cpp-codegen.h"

// System.Void AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey9::.ctor()
extern "C"  void U3CloadAssetBundlesU3Ec__AnonStorey9__ctor_m509371343 (U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey9::<>m__0(System.Single)
extern "C"  void U3CloadAssetBundlesU3Ec__AnonStorey9_U3CU3Em__0_m3524001189 (U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062 * __this, float ___currentProg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
