﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionTextView
struct ConversationSelectionTextView_t4281204247;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ConversationSelectionTextView::.ctor()
extern "C"  void ConversationSelectionTextView__ctor_m1530317888 (ConversationSelectionTextView_t4281204247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionTextView::SetText(System.String,System.String)
extern "C"  void ConversationSelectionTextView_SetText_m1992696201 (ConversationSelectionTextView_t4281204247 * __this, String_t* ___textEN0, String_t* ___textJP1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionTextView::SetText(System.String,System.String,System.Boolean)
extern "C"  void ConversationSelectionTextView_SetText_m129047622 (ConversationSelectionTextView_t4281204247 * __this, String_t* ___textEN0, String_t* ___textJP1, bool ___showJp2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionTextView::SetShowJP(System.Boolean)
extern "C"  void ConversationSelectionTextView_SetShowJP_m3619344106 (ConversationSelectionTextView_t4281204247 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionTextView::UpdateText()
extern "C"  void ConversationSelectionTextView_UpdateText_m1419485464 (ConversationSelectionTextView_t4281204247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
