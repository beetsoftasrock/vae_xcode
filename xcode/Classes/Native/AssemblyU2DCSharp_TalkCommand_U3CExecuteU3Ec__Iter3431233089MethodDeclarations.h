﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TalkCommand/<Execute>c__Iterator1
struct U3CExecuteU3Ec__Iterator1_t3431233089;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TalkCommand/<Execute>c__Iterator1::.ctor()
extern "C"  void U3CExecuteU3Ec__Iterator1__ctor_m3976842590 (U3CExecuteU3Ec__Iterator1_t3431233089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TalkCommand/<Execute>c__Iterator1::MoveNext()
extern "C"  bool U3CExecuteU3Ec__Iterator1_MoveNext_m3191830386 (U3CExecuteU3Ec__Iterator1_t3431233089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TalkCommand/<Execute>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4050452860 (U3CExecuteU3Ec__Iterator1_t3431233089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TalkCommand/<Execute>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3404410916 (U3CExecuteU3Ec__Iterator1_t3431233089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkCommand/<Execute>c__Iterator1::Dispose()
extern "C"  void U3CExecuteU3Ec__Iterator1_Dispose_m3943413395 (U3CExecuteU3Ec__Iterator1_t3431233089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkCommand/<Execute>c__Iterator1::Reset()
extern "C"  void U3CExecuteU3Ec__Iterator1_Reset_m2290846945 (U3CExecuteU3Ec__Iterator1_t3431233089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
