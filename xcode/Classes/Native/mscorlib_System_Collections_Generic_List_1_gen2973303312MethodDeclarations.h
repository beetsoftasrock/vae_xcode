﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Reporter/Log>::.ctor()
#define List_1__ctor_m1926329339(__this, method) ((  void (*) (List_1_t2973303312 *, const MethodInfo*))List_1__ctor_m4167524594_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3451740339(__this, ___collection0, method) ((  void (*) (List_1_t2973303312 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::.ctor(System.Int32)
#define List_1__ctor_m3810462973(__this, ___capacity0, method) ((  void (*) (List_1_t2973303312 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::.ctor(T[],System.Int32)
#define List_1__ctor_m1760526863(__this, ___data0, ___size1, method) ((  void (*) (List_1_t2973303312 *, LogU5BU5D_t1859948621*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::.cctor()
#define List_1__cctor_m2138363155(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Reporter/Log>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m503646740(__this, method) ((  Il2CppObject* (*) (List_1_t2973303312 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m582482334(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2973303312 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m655858361(__this, method) ((  Il2CppObject * (*) (List_1_t2973303312 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1996978754(__this, ___item0, method) ((  int32_t (*) (List_1_t2973303312 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m4082752696(__this, ___item0, method) ((  bool (*) (List_1_t2973303312 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2551110904(__this, ___item0, method) ((  int32_t (*) (List_1_t2973303312 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1135510349(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2973303312 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3639240557(__this, ___item0, method) ((  void (*) (List_1_t2973303312 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3521931561(__this, method) ((  bool (*) (List_1_t2973303312 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2513562222(__this, method) ((  bool (*) (List_1_t2973303312 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Reporter/Log>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3090472552(__this, method) ((  Il2CppObject * (*) (List_1_t2973303312 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2511791545(__this, method) ((  bool (*) (List_1_t2973303312 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m876247450(__this, method) ((  bool (*) (List_1_t2973303312 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m996227573(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2973303312 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1924443244(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2973303312 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(T)
#define List_1_Add_m3717950679(__this, ___item0, method) ((  void (*) (List_1_t2973303312 *, Log_t3604182180 *, const MethodInfo*))List_1_Add_m2984365430_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m219430036(__this, ___newCount0, method) ((  void (*) (List_1_t2973303312 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3752979845(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2973303312 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1099155372(__this, ___collection0, method) ((  void (*) (List_1_t2973303312 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m569660892(__this, ___enumerable0, method) ((  void (*) (List_1_t2973303312 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3537443277(__this, ___collection0, method) ((  void (*) (List_1_t2973303312 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Reporter/Log>::AsReadOnly()
#define List_1_AsReadOnly_m1073404458(__this, method) ((  ReadOnlyCollection_1_t3789967872 * (*) (List_1_t2973303312 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Clear()
#define List_1_Clear_m1629823038(__this, method) ((  void (*) (List_1_t2973303312 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::Contains(T)
#define List_1_Contains_m2954711195(__this, ___item0, method) ((  bool (*) (List_1_t2973303312 *, Log_t3604182180 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CopyTo(T[])
#define List_1_CopyTo_m2691620094(__this, ___array0, method) ((  void (*) (List_1_t2973303312 *, LogU5BU5D_t1859948621*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3237114633(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2973303312 *, LogU5BU5D_t1859948621*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m1911288433(__this, ___match0, method) ((  bool (*) (List_1_t2973303312 *, Predicate_1_t2047152295 *, const MethodInfo*))List_1_Exists_m1496178069_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<Reporter/Log>::Find(System.Predicate`1<T>)
#define List_1_Find_m2179953131(__this, ___match0, method) ((  Log_t3604182180 * (*) (List_1_t2973303312 *, Predicate_1_t2047152295 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3870113582(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2047152295 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Reporter/Log>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m1895110726(__this, ___match0, method) ((  List_1_t2973303312 * (*) (List_1_t2973303312 *, Predicate_1_t2047152295 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Reporter/Log>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m368618990(__this, ___match0, method) ((  List_1_t2973303312 * (*) (List_1_t2973303312 *, Predicate_1_t2047152295 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Reporter/Log>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m709329918(__this, ___match0, method) ((  List_1_t2973303312 * (*) (List_1_t2973303312 *, Predicate_1_t2047152295 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m76475768(__this, ___match0, method) ((  int32_t (*) (List_1_t2973303312 *, Predicate_1_t2047152295 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m4088838595(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2973303312 *, int32_t, int32_t, Predicate_1_t2047152295 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Reporter/Log>::GetEnumerator()
#define List_1_GetEnumerator_m353403698(__this, method) ((  Enumerator_t2508032986  (*) (List_1_t2973303312 *, const MethodInfo*))List_1_GetEnumerator_m1854899495_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Reporter/Log>::GetRange(System.Int32,System.Int32)
#define List_1_GetRange_m156558578(__this, ___index0, ___count1, method) ((  List_1_t2973303312 * (*) (List_1_t2973303312 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1944954844_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::IndexOf(T)
#define List_1_IndexOf_m1133367003(__this, ___item0, method) ((  int32_t (*) (List_1_t2973303312 *, Log_t3604182180 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m874037314(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2973303312 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2404165269(__this, ___index0, method) ((  void (*) (List_1_t2973303312 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Insert(System.Int32,T)
#define List_1_Insert_m2921573764(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2973303312 *, int32_t, Log_t3604182180 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m594095055(__this, ___collection0, method) ((  void (*) (List_1_t2973303312 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::Remove(T)
#define List_1_Remove_m3106033226(__this, ___item0, method) ((  bool (*) (List_1_t2973303312 *, Log_t3604182180 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3663313130(__this, ___match0, method) ((  int32_t (*) (List_1_t2973303312 *, Predicate_1_t2047152295 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3516134696(__this, ___index0, method) ((  void (*) (List_1_t2973303312 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m40324385(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2973303312 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Reverse()
#define List_1_Reverse_m2442919640(__this, method) ((  void (*) (List_1_t2973303312 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Sort()
#define List_1_Sort_m1049454472(__this, method) ((  void (*) (List_1_t2973303312 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3416821044(__this, ___comparer0, method) ((  void (*) (List_1_t2973303312 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2644720779(__this, ___comparison0, method) ((  void (*) (List_1_t2973303312 *, Comparison_1_t570953735 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Reporter/Log>::ToArray()
#define List_1_ToArray_m1302422887(__this, method) ((  LogU5BU5D_t1859948621* (*) (List_1_t2973303312 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::TrimExcess()
#define List_1_TrimExcess_m318498385(__this, method) ((  void (*) (List_1_t2973303312 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Capacity()
#define List_1_get_Capacity_m315900339(__this, method) ((  int32_t (*) (List_1_t2973303312 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1417560204(__this, ___value0, method) ((  void (*) (List_1_t2973303312 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count()
#define List_1_get_Count_m3680072003(__this, method) ((  int32_t (*) (List_1_t2973303312 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// T System.Collections.Generic.List`1<Reporter/Log>::get_Item(System.Int32)
#define List_1_get_Item_m3098359268(__this, ___index0, method) ((  Log_t3604182180 * (*) (List_1_t2973303312 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::set_Item(System.Int32,T)
#define List_1_set_Item_m666049121(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2973303312 *, int32_t, Log_t3604182180 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
