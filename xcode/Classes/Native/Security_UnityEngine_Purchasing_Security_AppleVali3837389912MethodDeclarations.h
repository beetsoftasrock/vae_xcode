﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.AppleValidator
struct AppleValidator_t3837389912;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Purchasing.Security.AppleReceipt
struct AppleReceipt_t3991411794;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.AppleValidator::.ctor(System.Byte[])
extern "C"  void AppleValidator__ctor_m1455195731 (AppleValidator_t3837389912 * __this, ByteU5BU5D_t3397334013* ___appleRootCertificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleValidator::Validate(System.Byte[])
extern "C"  AppleReceipt_t3991411794 * AppleValidator_Validate_m6433925 (AppleValidator_t3837389912 * __this, ByteU5BU5D_t3397334013* ___receiptData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
