﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationVRSwitcher/<GoToVRMode>c__AnonStorey1
struct U3CGoToVRModeU3Ec__AnonStorey1_t3564574473;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationVRSwitcher/<GoToVRMode>c__AnonStorey1::.ctor()
extern "C"  void U3CGoToVRModeU3Ec__AnonStorey1__ctor_m3673263022 (U3CGoToVRModeU3Ec__AnonStorey1_t3564574473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher/<GoToVRMode>c__AnonStorey1::<>m__0()
extern "C"  void U3CGoToVRModeU3Ec__AnonStorey1_U3CU3Em__0_m4075039171 (U3CGoToVRModeU3Ec__AnonStorey1_t3564574473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
