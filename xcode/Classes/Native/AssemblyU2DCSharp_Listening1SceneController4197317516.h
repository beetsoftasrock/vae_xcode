﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Listenning1DataLoader
struct Listenning1DataLoader_t2634637697;
// System.Collections.Generic.List`1<Listening1QuestionData>
struct List_1_t2750190156;
// System.Collections.Generic.List`1<Listening1OnplayQuestion>
struct List_1_t2903992757;
// Listening1QuestionView
struct Listening1QuestionView_t2345876901;
// Listening1OnplayQuestion
struct Listening1OnplayQuestion_t3534871625;
// System.String
struct String_t;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening1SceneController
struct  Listening1SceneController_t4197317516  : public MonoBehaviour_t1158329972
{
public:
	// Listenning1DataLoader Listening1SceneController::dataLoader
	Listenning1DataLoader_t2634637697 * ___dataLoader_2;
	// System.Collections.Generic.List`1<Listening1QuestionData> Listening1SceneController::questionDatas
	List_1_t2750190156 * ___questionDatas_3;
	// System.Collections.Generic.List`1<Listening1OnplayQuestion> Listening1SceneController::onPlayQuestions
	List_1_t2903992757 * ___onPlayQuestions_4;
	// Listening1QuestionView Listening1SceneController::questionView
	Listening1QuestionView_t2345876901 * ___questionView_5;
	// Listening1OnplayQuestion Listening1SceneController::_currentQuestion
	Listening1OnplayQuestion_t3534871625 * ____currentQuestion_6;
	// System.String Listening1SceneController::soundPath
	String_t* ___soundPath_7;
	// System.DateTime Listening1SceneController::startTime
	DateTime_t693205669  ___startTime_8;

public:
	inline static int32_t get_offset_of_dataLoader_2() { return static_cast<int32_t>(offsetof(Listening1SceneController_t4197317516, ___dataLoader_2)); }
	inline Listenning1DataLoader_t2634637697 * get_dataLoader_2() const { return ___dataLoader_2; }
	inline Listenning1DataLoader_t2634637697 ** get_address_of_dataLoader_2() { return &___dataLoader_2; }
	inline void set_dataLoader_2(Listenning1DataLoader_t2634637697 * value)
	{
		___dataLoader_2 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_2, value);
	}

	inline static int32_t get_offset_of_questionDatas_3() { return static_cast<int32_t>(offsetof(Listening1SceneController_t4197317516, ___questionDatas_3)); }
	inline List_1_t2750190156 * get_questionDatas_3() const { return ___questionDatas_3; }
	inline List_1_t2750190156 ** get_address_of_questionDatas_3() { return &___questionDatas_3; }
	inline void set_questionDatas_3(List_1_t2750190156 * value)
	{
		___questionDatas_3 = value;
		Il2CppCodeGenWriteBarrier(&___questionDatas_3, value);
	}

	inline static int32_t get_offset_of_onPlayQuestions_4() { return static_cast<int32_t>(offsetof(Listening1SceneController_t4197317516, ___onPlayQuestions_4)); }
	inline List_1_t2903992757 * get_onPlayQuestions_4() const { return ___onPlayQuestions_4; }
	inline List_1_t2903992757 ** get_address_of_onPlayQuestions_4() { return &___onPlayQuestions_4; }
	inline void set_onPlayQuestions_4(List_1_t2903992757 * value)
	{
		___onPlayQuestions_4 = value;
		Il2CppCodeGenWriteBarrier(&___onPlayQuestions_4, value);
	}

	inline static int32_t get_offset_of_questionView_5() { return static_cast<int32_t>(offsetof(Listening1SceneController_t4197317516, ___questionView_5)); }
	inline Listening1QuestionView_t2345876901 * get_questionView_5() const { return ___questionView_5; }
	inline Listening1QuestionView_t2345876901 ** get_address_of_questionView_5() { return &___questionView_5; }
	inline void set_questionView_5(Listening1QuestionView_t2345876901 * value)
	{
		___questionView_5 = value;
		Il2CppCodeGenWriteBarrier(&___questionView_5, value);
	}

	inline static int32_t get_offset_of__currentQuestion_6() { return static_cast<int32_t>(offsetof(Listening1SceneController_t4197317516, ____currentQuestion_6)); }
	inline Listening1OnplayQuestion_t3534871625 * get__currentQuestion_6() const { return ____currentQuestion_6; }
	inline Listening1OnplayQuestion_t3534871625 ** get_address_of__currentQuestion_6() { return &____currentQuestion_6; }
	inline void set__currentQuestion_6(Listening1OnplayQuestion_t3534871625 * value)
	{
		____currentQuestion_6 = value;
		Il2CppCodeGenWriteBarrier(&____currentQuestion_6, value);
	}

	inline static int32_t get_offset_of_soundPath_7() { return static_cast<int32_t>(offsetof(Listening1SceneController_t4197317516, ___soundPath_7)); }
	inline String_t* get_soundPath_7() const { return ___soundPath_7; }
	inline String_t** get_address_of_soundPath_7() { return &___soundPath_7; }
	inline void set_soundPath_7(String_t* value)
	{
		___soundPath_7 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_7, value);
	}

	inline static int32_t get_offset_of_startTime_8() { return static_cast<int32_t>(offsetof(Listening1SceneController_t4197317516, ___startTime_8)); }
	inline DateTime_t693205669  get_startTime_8() const { return ___startTime_8; }
	inline DateTime_t693205669 * get_address_of_startTime_8() { return &___startTime_8; }
	inline void set_startTime_8(DateTime_t693205669  value)
	{
		___startTime_8 = value;
	}
};

struct Listening1SceneController_t4197317516_StaticFields
{
public:
	// System.Func`1<System.Boolean> Listening1SceneController::<>f__mg$cache0
	Func_1_t1485000104 * ___U3CU3Ef__mgU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_9() { return static_cast<int32_t>(offsetof(Listening1SceneController_t4197317516_StaticFields, ___U3CU3Ef__mgU24cache0_9)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__mgU24cache0_9() const { return ___U3CU3Ef__mgU24cache0_9; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__mgU24cache0_9() { return &___U3CU3Ef__mgU24cache0_9; }
	inline void set_U3CU3Ef__mgU24cache0_9(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__mgU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
