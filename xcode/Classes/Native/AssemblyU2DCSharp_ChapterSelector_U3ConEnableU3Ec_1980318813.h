﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChapterItem
struct ChapterItem_t2155291334;
// ChapterSelector/<onEnable>c__Iterator0
struct U3ConEnableU3Ec__Iterator0_t2146627610;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterSelector/<onEnable>c__Iterator0/<onEnable>c__AnonStorey5
struct  U3ConEnableU3Ec__AnonStorey5_t1980318813  : public Il2CppObject
{
public:
	// ChapterItem ChapterSelector/<onEnable>c__Iterator0/<onEnable>c__AnonStorey5::chapItem
	ChapterItem_t2155291334 * ___chapItem_0;
	// System.Int32 ChapterSelector/<onEnable>c__Iterator0/<onEnable>c__AnonStorey5::realIndex
	int32_t ___realIndex_1;
	// ChapterSelector/<onEnable>c__Iterator0 ChapterSelector/<onEnable>c__Iterator0/<onEnable>c__AnonStorey5::<>f__ref$0
	U3ConEnableU3Ec__Iterator0_t2146627610 * ___U3CU3Ef__refU240_2;

public:
	inline static int32_t get_offset_of_chapItem_0() { return static_cast<int32_t>(offsetof(U3ConEnableU3Ec__AnonStorey5_t1980318813, ___chapItem_0)); }
	inline ChapterItem_t2155291334 * get_chapItem_0() const { return ___chapItem_0; }
	inline ChapterItem_t2155291334 ** get_address_of_chapItem_0() { return &___chapItem_0; }
	inline void set_chapItem_0(ChapterItem_t2155291334 * value)
	{
		___chapItem_0 = value;
		Il2CppCodeGenWriteBarrier(&___chapItem_0, value);
	}

	inline static int32_t get_offset_of_realIndex_1() { return static_cast<int32_t>(offsetof(U3ConEnableU3Ec__AnonStorey5_t1980318813, ___realIndex_1)); }
	inline int32_t get_realIndex_1() const { return ___realIndex_1; }
	inline int32_t* get_address_of_realIndex_1() { return &___realIndex_1; }
	inline void set_realIndex_1(int32_t value)
	{
		___realIndex_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3ConEnableU3Ec__AnonStorey5_t1980318813, ___U3CU3Ef__refU240_2)); }
	inline U3ConEnableU3Ec__Iterator0_t2146627610 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3ConEnableU3Ec__Iterator0_t2146627610 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3ConEnableU3Ec__Iterator0_t2146627610 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
