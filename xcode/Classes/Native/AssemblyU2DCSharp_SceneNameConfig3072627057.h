﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SceneName904752595.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneNameConfig
struct  SceneNameConfig_t3072627057  : public Il2CppObject
{
public:

public:
};

struct SceneNameConfig_t3072627057_StaticFields
{
public:
	// SceneName SceneNameConfig::NEW_VR_SCENCE_CALL
	int32_t ___NEW_VR_SCENCE_CALL_19;
	// SceneName SceneNameConfig::VR_CHAPTERTOP
	int32_t ___VR_CHAPTERTOP_20;

public:
	inline static int32_t get_offset_of_NEW_VR_SCENCE_CALL_19() { return static_cast<int32_t>(offsetof(SceneNameConfig_t3072627057_StaticFields, ___NEW_VR_SCENCE_CALL_19)); }
	inline int32_t get_NEW_VR_SCENCE_CALL_19() const { return ___NEW_VR_SCENCE_CALL_19; }
	inline int32_t* get_address_of_NEW_VR_SCENCE_CALL_19() { return &___NEW_VR_SCENCE_CALL_19; }
	inline void set_NEW_VR_SCENCE_CALL_19(int32_t value)
	{
		___NEW_VR_SCENCE_CALL_19 = value;
	}

	inline static int32_t get_offset_of_VR_CHAPTERTOP_20() { return static_cast<int32_t>(offsetof(SceneNameConfig_t3072627057_StaticFields, ___VR_CHAPTERTOP_20)); }
	inline int32_t get_VR_CHAPTERTOP_20() const { return ___VR_CHAPTERTOP_20; }
	inline int32_t* get_address_of_VR_CHAPTERTOP_20() { return &___VR_CHAPTERTOP_20; }
	inline void set_VR_CHAPTERTOP_20(int32_t value)
	{
		___VR_CHAPTERTOP_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
