﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening1Popup
struct Listening1Popup_t1745092138;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening1Popup::.ctor()
extern "C"  void Listening1Popup__ctor_m1709242849 (Listening1Popup_t1745092138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1Popup::AddAnswerTexts(System.Collections.Generic.List`1<System.String>)
extern "C"  void Listening1Popup_AddAnswerTexts_m3350687666 (Listening1Popup_t1745092138 * __this, List_1_t1398341365 * ___answerTexts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1Popup::ClearAnswerTexts()
extern "C"  void Listening1Popup_ClearAnswerTexts_m2425203680 (Listening1Popup_t1745092138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
