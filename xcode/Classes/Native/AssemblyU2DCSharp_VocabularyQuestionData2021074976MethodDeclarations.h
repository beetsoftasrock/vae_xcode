﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VocabularyQuestionData
struct VocabularyQuestionData_t2021074976;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VocabularyQuestionData::.ctor()
extern "C"  void VocabularyQuestionData__ctor_m2035405671 (VocabularyQuestionData_t2021074976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VocabularyQuestionData::get_questionText()
extern "C"  String_t* VocabularyQuestionData_get_questionText_m2749402668 (VocabularyQuestionData_t2021074976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionData::set_questionText(System.String)
extern "C"  void VocabularyQuestionData_set_questionText_m3440756551 (VocabularyQuestionData_t2021074976 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VocabularyQuestionData::get_questionSound()
extern "C"  String_t* VocabularyQuestionData_get_questionSound_m2194616072 (VocabularyQuestionData_t2021074976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionData::set_questionSound(System.String)
extern "C"  void VocabularyQuestionData_set_questionSound_m2073262017 (VocabularyQuestionData_t2021074976 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VocabularyQuestionData::get_answerText0()
extern "C"  String_t* VocabularyQuestionData_get_answerText0_m379419772 (VocabularyQuestionData_t2021074976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionData::set_answerText0(System.String)
extern "C"  void VocabularyQuestionData_set_answerText0_m369379033 (VocabularyQuestionData_t2021074976 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VocabularyQuestionData::get_answerText1()
extern "C"  String_t* VocabularyQuestionData_get_answerText1_m379419739 (VocabularyQuestionData_t2021074976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionData::set_answerText1(System.String)
extern "C"  void VocabularyQuestionData_set_answerText1_m1087794490 (VocabularyQuestionData_t2021074976 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VocabularyQuestionData::get_answerText2()
extern "C"  String_t* VocabularyQuestionData_get_answerText2_m379419838 (VocabularyQuestionData_t2021074976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionData::set_answerText2(System.String)
extern "C"  void VocabularyQuestionData_set_answerText2_m1243492507 (VocabularyQuestionData_t2021074976 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VocabularyQuestionData::get_answerText3()
extern "C"  String_t* VocabularyQuestionData_get_answerText3_m379419805 (VocabularyQuestionData_t2021074976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionData::set_answerText3(System.String)
extern "C"  void VocabularyQuestionData_set_answerText3_m1754714876 (VocabularyQuestionData_t2021074976 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VocabularyQuestionData::get_correctAnswer()
extern "C"  int32_t VocabularyQuestionData_get_correctAnswer_m2807570458 (VocabularyQuestionData_t2021074976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionData::set_correctAnswer(System.Int32)
extern "C"  void VocabularyQuestionData_set_correctAnswer_m1690310793 (VocabularyQuestionData_t2021074976 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
