﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterAnimCommand
struct CharacterAnimCommand_t3374959163;
// System.String
struct String_t;
// ConversationLogicController
struct ConversationLogicController_t3911886281;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ConversationLogicController3911886281.h"

// System.Void CharacterAnimCommand::.ctor(System.String,System.String,System.String,System.String,System.String,System.Single)
extern "C"  void CharacterAnimCommand__ctor_m3914082789 (CharacterAnimCommand_t3374959163 * __this, String_t* ____characterName0, String_t* ____postureTypeName1, String_t* ____eyesTypeName2, String_t* ____mouthTypeName3, String_t* ____spawnPostion4, float ____wait5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterAnimCommand::Revert(ConversationLogicController)
extern "C"  void CharacterAnimCommand_Revert_m1612597131 (CharacterAnimCommand_t3374959163 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CharacterAnimCommand::AutoExecute(ConversationLogicController)
extern "C"  Il2CppObject * CharacterAnimCommand_AutoExecute_m1065929223 (CharacterAnimCommand_t3374959163 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CharacterAnimCommand::Execute(ConversationLogicController)
extern "C"  Il2CppObject * CharacterAnimCommand_Execute_m1705355672 (CharacterAnimCommand_t3374959163 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CharacterAnimCommand::GetRole()
extern "C"  String_t* CharacterAnimCommand_GetRole_m3657976329 (CharacterAnimCommand_t3374959163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
