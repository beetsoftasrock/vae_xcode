﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonMike/<IESavingVoice>c__Iterator0
struct U3CIESavingVoiceU3Ec__Iterator0_t1871642022;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonMike/<IESavingVoice>c__Iterator0::.ctor()
extern "C"  void U3CIESavingVoiceU3Ec__Iterator0__ctor_m2999830881 (U3CIESavingVoiceU3Ec__Iterator0_t1871642022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonMike/<IESavingVoice>c__Iterator0::MoveNext()
extern "C"  bool U3CIESavingVoiceU3Ec__Iterator0_MoveNext_m3166285695 (U3CIESavingVoiceU3Ec__Iterator0_t1871642022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ButtonMike/<IESavingVoice>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIESavingVoiceU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m184879759 (U3CIESavingVoiceU3Ec__Iterator0_t1871642022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ButtonMike/<IESavingVoice>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIESavingVoiceU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2517859991 (U3CIESavingVoiceU3Ec__Iterator0_t1871642022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMike/<IESavingVoice>c__Iterator0::Dispose()
extern "C"  void U3CIESavingVoiceU3Ec__Iterator0_Dispose_m638556784 (U3CIESavingVoiceU3Ec__Iterator0_t1871642022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMike/<IESavingVoice>c__Iterator0::Reset()
extern "C"  void U3CIESavingVoiceU3Ec__Iterator0_Reset_m1038852410 (U3CIESavingVoiceU3Ec__Iterator0_t1871642022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
