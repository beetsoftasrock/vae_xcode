﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalConfig
struct GlobalConfig_t3080413471;
// DataStorage
struct DataStorage_t2091384907;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataStorage
struct  DataStorage_t2091384907  : public MonoBehaviour_t1158329972
{
public:
	// GlobalConfig DataStorage::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_2;

public:
	inline static int32_t get_offset_of_globalConfig_2() { return static_cast<int32_t>(offsetof(DataStorage_t2091384907, ___globalConfig_2)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_2() const { return ___globalConfig_2; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_2() { return &___globalConfig_2; }
	inline void set_globalConfig_2(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_2 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_2, value);
	}
};

struct DataStorage_t2091384907_StaticFields
{
public:
	// DataStorage DataStorage::_instance
	DataStorage_t2091384907 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(DataStorage_t2091384907_StaticFields, ____instance_3)); }
	inline DataStorage_t2091384907 * get__instance_3() const { return ____instance_3; }
	inline DataStorage_t2091384907 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(DataStorage_t2091384907 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
