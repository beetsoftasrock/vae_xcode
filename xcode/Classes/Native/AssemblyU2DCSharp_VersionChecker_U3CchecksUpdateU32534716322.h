﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VersionChecker/<checksUpdate>c__AnonStorey1
struct  U3CchecksUpdateU3Ec__AnonStorey1_t2534716322  : public Il2CppObject
{
public:
	// System.String VersionChecker/<checksUpdate>c__AnonStorey1::storeUrl
	String_t* ___storeUrl_0;

public:
	inline static int32_t get_offset_of_storeUrl_0() { return static_cast<int32_t>(offsetof(U3CchecksUpdateU3Ec__AnonStorey1_t2534716322, ___storeUrl_0)); }
	inline String_t* get_storeUrl_0() const { return ___storeUrl_0; }
	inline String_t** get_address_of_storeUrl_0() { return &___storeUrl_0; }
	inline void set_storeUrl_0(String_t* value)
	{
		___storeUrl_0 = value;
		Il2CppCodeGenWriteBarrier(&___storeUrl_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
