﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterScript/<LerpCharacterToSpawnPosition>c__Iterator3
struct U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterScript/<LerpCharacterToSpawnPosition>c__Iterator3::.ctor()
extern "C"  void U3CLerpCharacterToSpawnPositionU3Ec__Iterator3__ctor_m1028458869 (U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterScript/<LerpCharacterToSpawnPosition>c__Iterator3::MoveNext()
extern "C"  bool U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_MoveNext_m1803399983 (U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterScript/<LerpCharacterToSpawnPosition>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2985100567 (U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterScript/<LerpCharacterToSpawnPosition>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m389587983 (U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript/<LerpCharacterToSpawnPosition>c__Iterator3::Dispose()
extern "C"  void U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_Dispose_m3106755806 (U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript/<LerpCharacterToSpawnPosition>c__Iterator3::Reset()
extern "C"  void U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_Reset_m4248083748 (U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
