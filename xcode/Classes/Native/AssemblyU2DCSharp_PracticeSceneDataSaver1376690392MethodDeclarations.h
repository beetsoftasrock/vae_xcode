﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PracticeSceneDataSaver
struct PracticeSceneDataSaver_t1376690392;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"

// System.Void PracticeSceneDataSaver::.ctor()
extern "C"  void PracticeSceneDataSaver__ctor_m3373178429 (PracticeSceneDataSaver_t1376690392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeSceneDataSaver::SaveRecord()
extern "C"  void PracticeSceneDataSaver_SaveRecord_m3152846021 (PracticeSceneDataSaver_t1376690392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeSceneDataSaver::PushZip(System.String[],System.String)
extern "C"  void PracticeSceneDataSaver_PushZip_m45210682 (PracticeSceneDataSaver_t1376690392 * __this, StringU5BU5D_t1642385972* ___soundFileNames0, String_t* ___zipFilePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeSceneDataSaver::SavePercent(System.Single,System.Single,System.Action,System.Action)
extern "C"  void PracticeSceneDataSaver_SavePercent_m2694244775 (PracticeSceneDataSaver_t1376690392 * __this, float ___totalTimeRecorded0, float ___totalTimeSoundTemplate1, Action_t3226471752 * ___saveCompletedCallback2, Action_t3226471752 * ___saveErrorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
