﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryLogModule
struct HistoryLogModule_t1965887714;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void HistoryLogModule::.ctor()
extern "C"  void HistoryLogModule__ctor_m181295803 (HistoryLogModule_t1965887714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryLogModule::OnConversationReset()
extern "C"  void HistoryLogModule_OnConversationReset_m2778536060 (HistoryLogModule_t1965887714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryLogModule::OnConversationReplay()
extern "C"  void HistoryLogModule_OnConversationReplay_m3160042928 (HistoryLogModule_t1965887714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HistoryLogModule::AppendTalkDialogs()
extern "C"  Il2CppObject * HistoryLogModule_AppendTalkDialogs_m4059883898 (HistoryLogModule_t1965887714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HistoryLogModule::AppendTalkHistoryDialog(ConversationTalkData,UnityEngine.Transform)
extern "C"  Il2CppObject * HistoryLogModule_AppendTalkHistoryDialog_m757433811 (HistoryLogModule_t1965887714 * __this, ConversationTalkData_t1570298305 * ___data0, Transform_t3275118058 * ___dialogTransform1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HistoryLogModule::MoveDialogToStartPosition()
extern "C"  Il2CppObject * HistoryLogModule_MoveDialogToStartPosition_m373855094 (HistoryLogModule_t1965887714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
