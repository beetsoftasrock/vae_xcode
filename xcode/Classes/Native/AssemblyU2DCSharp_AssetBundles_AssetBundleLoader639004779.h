﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// GlobalConfig
struct GlobalConfig_t3080413471;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoader
struct  AssetBundleLoader_t639004779  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text AssetBundles.AssetBundleLoader::text_Status
	Text_t356221433 * ___text_Status_5;
	// UnityEngine.UI.Text AssetBundles.AssetBundleLoader::text_OverallProg
	Text_t356221433 * ___text_OverallProg_6;
	// UnityEngine.UI.Slider AssetBundles.AssetBundleLoader::slider_OverallProg
	Slider_t297367283 * ___slider_OverallProg_7;
	// UnityEngine.GameObject AssetBundles.AssetBundleLoader::group_Loading
	GameObject_t1756533147 * ___group_Loading_8;
	// UnityEngine.GameObject AssetBundles.AssetBundleLoader::group_Message
	GameObject_t1756533147 * ___group_Message_9;
	// UnityEngine.GameObject AssetBundles.AssetBundleLoader::group_Error
	GameObject_t1756533147 * ___group_Error_10;
	// UnityEngine.UI.Image AssetBundles.AssetBundleLoader::behindOverlay
	Image_t2042527209 * ___behindOverlay_11;
	// System.Collections.Generic.List`1<System.String> AssetBundles.AssetBundleLoader::needUpdateBundles
	List_1_t1398341365 * ___needUpdateBundles_12;
	// System.Action AssetBundles.AssetBundleLoader::onCompleted
	Action_t3226471752 * ___onCompleted_13;

public:
	inline static int32_t get_offset_of_text_Status_5() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___text_Status_5)); }
	inline Text_t356221433 * get_text_Status_5() const { return ___text_Status_5; }
	inline Text_t356221433 ** get_address_of_text_Status_5() { return &___text_Status_5; }
	inline void set_text_Status_5(Text_t356221433 * value)
	{
		___text_Status_5 = value;
		Il2CppCodeGenWriteBarrier(&___text_Status_5, value);
	}

	inline static int32_t get_offset_of_text_OverallProg_6() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___text_OverallProg_6)); }
	inline Text_t356221433 * get_text_OverallProg_6() const { return ___text_OverallProg_6; }
	inline Text_t356221433 ** get_address_of_text_OverallProg_6() { return &___text_OverallProg_6; }
	inline void set_text_OverallProg_6(Text_t356221433 * value)
	{
		___text_OverallProg_6 = value;
		Il2CppCodeGenWriteBarrier(&___text_OverallProg_6, value);
	}

	inline static int32_t get_offset_of_slider_OverallProg_7() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___slider_OverallProg_7)); }
	inline Slider_t297367283 * get_slider_OverallProg_7() const { return ___slider_OverallProg_7; }
	inline Slider_t297367283 ** get_address_of_slider_OverallProg_7() { return &___slider_OverallProg_7; }
	inline void set_slider_OverallProg_7(Slider_t297367283 * value)
	{
		___slider_OverallProg_7 = value;
		Il2CppCodeGenWriteBarrier(&___slider_OverallProg_7, value);
	}

	inline static int32_t get_offset_of_group_Loading_8() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___group_Loading_8)); }
	inline GameObject_t1756533147 * get_group_Loading_8() const { return ___group_Loading_8; }
	inline GameObject_t1756533147 ** get_address_of_group_Loading_8() { return &___group_Loading_8; }
	inline void set_group_Loading_8(GameObject_t1756533147 * value)
	{
		___group_Loading_8 = value;
		Il2CppCodeGenWriteBarrier(&___group_Loading_8, value);
	}

	inline static int32_t get_offset_of_group_Message_9() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___group_Message_9)); }
	inline GameObject_t1756533147 * get_group_Message_9() const { return ___group_Message_9; }
	inline GameObject_t1756533147 ** get_address_of_group_Message_9() { return &___group_Message_9; }
	inline void set_group_Message_9(GameObject_t1756533147 * value)
	{
		___group_Message_9 = value;
		Il2CppCodeGenWriteBarrier(&___group_Message_9, value);
	}

	inline static int32_t get_offset_of_group_Error_10() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___group_Error_10)); }
	inline GameObject_t1756533147 * get_group_Error_10() const { return ___group_Error_10; }
	inline GameObject_t1756533147 ** get_address_of_group_Error_10() { return &___group_Error_10; }
	inline void set_group_Error_10(GameObject_t1756533147 * value)
	{
		___group_Error_10 = value;
		Il2CppCodeGenWriteBarrier(&___group_Error_10, value);
	}

	inline static int32_t get_offset_of_behindOverlay_11() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___behindOverlay_11)); }
	inline Image_t2042527209 * get_behindOverlay_11() const { return ___behindOverlay_11; }
	inline Image_t2042527209 ** get_address_of_behindOverlay_11() { return &___behindOverlay_11; }
	inline void set_behindOverlay_11(Image_t2042527209 * value)
	{
		___behindOverlay_11 = value;
		Il2CppCodeGenWriteBarrier(&___behindOverlay_11, value);
	}

	inline static int32_t get_offset_of_needUpdateBundles_12() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___needUpdateBundles_12)); }
	inline List_1_t1398341365 * get_needUpdateBundles_12() const { return ___needUpdateBundles_12; }
	inline List_1_t1398341365 ** get_address_of_needUpdateBundles_12() { return &___needUpdateBundles_12; }
	inline void set_needUpdateBundles_12(List_1_t1398341365 * value)
	{
		___needUpdateBundles_12 = value;
		Il2CppCodeGenWriteBarrier(&___needUpdateBundles_12, value);
	}

	inline static int32_t get_offset_of_onCompleted_13() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779, ___onCompleted_13)); }
	inline Action_t3226471752 * get_onCompleted_13() const { return ___onCompleted_13; }
	inline Action_t3226471752 ** get_address_of_onCompleted_13() { return &___onCompleted_13; }
	inline void set_onCompleted_13(Action_t3226471752 * value)
	{
		___onCompleted_13 = value;
		Il2CppCodeGenWriteBarrier(&___onCompleted_13, value);
	}
};

struct AssetBundleLoader_t639004779_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> AssetBundles.AssetBundleLoader::<AvailableChapters>k__BackingField
	List_1_t1398341365 * ___U3CAvailableChaptersU3Ek__BackingField_2;
	// System.Boolean AssetBundles.AssetBundleLoader::<IsDoneFirstDownload>k__BackingField
	bool ___U3CIsDoneFirstDownloadU3Ek__BackingField_3;
	// GlobalConfig AssetBundles.AssetBundleLoader::_globalConfig
	GlobalConfig_t3080413471 * ____globalConfig_4;

public:
	inline static int32_t get_offset_of_U3CAvailableChaptersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779_StaticFields, ___U3CAvailableChaptersU3Ek__BackingField_2)); }
	inline List_1_t1398341365 * get_U3CAvailableChaptersU3Ek__BackingField_2() const { return ___U3CAvailableChaptersU3Ek__BackingField_2; }
	inline List_1_t1398341365 ** get_address_of_U3CAvailableChaptersU3Ek__BackingField_2() { return &___U3CAvailableChaptersU3Ek__BackingField_2; }
	inline void set_U3CAvailableChaptersU3Ek__BackingField_2(List_1_t1398341365 * value)
	{
		___U3CAvailableChaptersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAvailableChaptersU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CIsDoneFirstDownloadU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779_StaticFields, ___U3CIsDoneFirstDownloadU3Ek__BackingField_3)); }
	inline bool get_U3CIsDoneFirstDownloadU3Ek__BackingField_3() const { return ___U3CIsDoneFirstDownloadU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsDoneFirstDownloadU3Ek__BackingField_3() { return &___U3CIsDoneFirstDownloadU3Ek__BackingField_3; }
	inline void set_U3CIsDoneFirstDownloadU3Ek__BackingField_3(bool value)
	{
		___U3CIsDoneFirstDownloadU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of__globalConfig_4() { return static_cast<int32_t>(offsetof(AssetBundleLoader_t639004779_StaticFields, ____globalConfig_4)); }
	inline GlobalConfig_t3080413471 * get__globalConfig_4() const { return ____globalConfig_4; }
	inline GlobalConfig_t3080413471 ** get_address_of__globalConfig_4() { return &____globalConfig_4; }
	inline void set__globalConfig_4(GlobalConfig_t3080413471 * value)
	{
		____globalConfig_4 = value;
		Il2CppCodeGenWriteBarrier(&____globalConfig_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
