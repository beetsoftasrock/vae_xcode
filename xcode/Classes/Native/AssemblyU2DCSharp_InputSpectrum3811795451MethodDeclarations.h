﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputSpectrum
struct InputSpectrum_t3811795451;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void InputSpectrum::.ctor()
extern "C"  void InputSpectrum__ctor_m357552226 (InputSpectrum_t3811795451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color InputSpectrum::get_StylizedColor()
extern "C"  Color_t2020392075  InputSpectrum_get_StylizedColor_m8294867 (InputSpectrum_t3811795451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputSpectrum::set_StylizedColor(UnityEngine.Color)
extern "C"  void InputSpectrum_set_StylizedColor_m35255422 (InputSpectrum_t3811795451 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputSpectrum::Start()
extern "C"  void InputSpectrum_Start_m160453270 (InputSpectrum_t3811795451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputSpectrum::OnEnable()
extern "C"  void InputSpectrum_OnEnable_m3336934486 (InputSpectrum_t3811795451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputSpectrum::Update()
extern "C"  void InputSpectrum_Update_m3707739103 (InputSpectrum_t3811795451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputSpectrum::SetSamples(System.Single[])
extern "C"  void InputSpectrum_SetSamples_m2544176708 (InputSpectrum_t3811795451 * __this, SingleU5BU5D_t577127397* ___samples0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
