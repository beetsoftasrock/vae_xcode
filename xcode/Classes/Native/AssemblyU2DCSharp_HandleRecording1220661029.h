﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.String
struct String_t;
// AudioRecorder.Recorder
struct Recorder_t2874686128;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HandleRecording
struct  HandleRecording_t1220661029  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource HandleRecording::audioSource
	AudioSource_t1135106623 * ___audioSource_2;
	// System.String HandleRecording::pathSaved
	String_t* ___pathSaved_3;
	// AudioRecorder.Recorder HandleRecording::recorder
	Recorder_t2874686128 * ___recorder_4;

public:
	inline static int32_t get_offset_of_audioSource_2() { return static_cast<int32_t>(offsetof(HandleRecording_t1220661029, ___audioSource_2)); }
	inline AudioSource_t1135106623 * get_audioSource_2() const { return ___audioSource_2; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_2() { return &___audioSource_2; }
	inline void set_audioSource_2(AudioSource_t1135106623 * value)
	{
		___audioSource_2 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_2, value);
	}

	inline static int32_t get_offset_of_pathSaved_3() { return static_cast<int32_t>(offsetof(HandleRecording_t1220661029, ___pathSaved_3)); }
	inline String_t* get_pathSaved_3() const { return ___pathSaved_3; }
	inline String_t** get_address_of_pathSaved_3() { return &___pathSaved_3; }
	inline void set_pathSaved_3(String_t* value)
	{
		___pathSaved_3 = value;
		Il2CppCodeGenWriteBarrier(&___pathSaved_3, value);
	}

	inline static int32_t get_offset_of_recorder_4() { return static_cast<int32_t>(offsetof(HandleRecording_t1220661029, ___recorder_4)); }
	inline Recorder_t2874686128 * get_recorder_4() const { return ___recorder_4; }
	inline Recorder_t2874686128 ** get_address_of_recorder_4() { return &___recorder_4; }
	inline void set_recorder_4(Recorder_t2874686128 * value)
	{
		___recorder_4 = value;
		Il2CppCodeGenWriteBarrier(&___recorder_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
