﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PageHelpScript
struct PageHelpScript_t1232253699;

#include "codegen/il2cpp-codegen.h"

// System.Void PageHelpScript::.ctor()
extern "C"  void PageHelpScript__ctor_m2615183730 (PageHelpScript_t1232253699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PageHelpScript::CreatePageHelp(System.Int32)
extern "C"  void PageHelpScript_CreatePageHelp_m3893998949 (PageHelpScript_t1232253699 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PageHelpScript::ShowActiveHelp(System.Int32)
extern "C"  void PageHelpScript_ShowActiveHelp_m2217587937 (PageHelpScript_t1232253699 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
