﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.Purchaser/<OnPurchaseFailed>c__AnonStorey3
struct U3COnPurchaseFailedU3Ec__AnonStorey3_t1880872493;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"

// System.Void Beetsoft.VAE.Purchaser/<OnPurchaseFailed>c__AnonStorey3::.ctor()
extern "C"  void U3COnPurchaseFailedU3Ec__AnonStorey3__ctor_m255784872 (U3COnPurchaseFailedU3Ec__AnonStorey3_t1880872493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.Purchaser/<OnPurchaseFailed>c__AnonStorey3::<>m__0(Beetsoft.VAE.Purchaser/PurchaseSession)
extern "C"  bool U3COnPurchaseFailedU3Ec__AnonStorey3_U3CU3Em__0_m3258103954 (U3COnPurchaseFailedU3Ec__AnonStorey3_t1880872493 * __this, PurchaseSession_t2088054391  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
