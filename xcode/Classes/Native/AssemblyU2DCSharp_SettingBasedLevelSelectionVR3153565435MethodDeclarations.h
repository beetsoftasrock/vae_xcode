﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedLevelSelectionVR
struct SettingBasedLevelSelectionVR_t3153565435;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedLevelSelectionVR::.ctor()
extern "C"  void SettingBasedLevelSelectionVR__ctor_m1569326978 (SettingBasedLevelSelectionVR_t3153565435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedLevelSelectionVR::Start()
extern "C"  void SettingBasedLevelSelectionVR_Start_m3392382822 (SettingBasedLevelSelectionVR_t3153565435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
