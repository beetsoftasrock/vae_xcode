﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HelpPopupBroadCastEvent
struct HelpPopupBroadCastEvent_t2836716150;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpPopup
struct  HelpPopup_t3079166541  : public MonoBehaviour_t1158329972
{
public:
	// System.String HelpPopup::identify
	String_t* ___identify_3;
	// System.Int32 HelpPopup::currentSprite
	int32_t ___currentSprite_4;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> HelpPopup::sprites
	List_1_t3973682211 * ___sprites_5;
	// UnityEngine.UI.Image HelpPopup::mainImage
	Image_t2042527209 * ___mainImage_6;

public:
	inline static int32_t get_offset_of_identify_3() { return static_cast<int32_t>(offsetof(HelpPopup_t3079166541, ___identify_3)); }
	inline String_t* get_identify_3() const { return ___identify_3; }
	inline String_t** get_address_of_identify_3() { return &___identify_3; }
	inline void set_identify_3(String_t* value)
	{
		___identify_3 = value;
		Il2CppCodeGenWriteBarrier(&___identify_3, value);
	}

	inline static int32_t get_offset_of_currentSprite_4() { return static_cast<int32_t>(offsetof(HelpPopup_t3079166541, ___currentSprite_4)); }
	inline int32_t get_currentSprite_4() const { return ___currentSprite_4; }
	inline int32_t* get_address_of_currentSprite_4() { return &___currentSprite_4; }
	inline void set_currentSprite_4(int32_t value)
	{
		___currentSprite_4 = value;
	}

	inline static int32_t get_offset_of_sprites_5() { return static_cast<int32_t>(offsetof(HelpPopup_t3079166541, ___sprites_5)); }
	inline List_1_t3973682211 * get_sprites_5() const { return ___sprites_5; }
	inline List_1_t3973682211 ** get_address_of_sprites_5() { return &___sprites_5; }
	inline void set_sprites_5(List_1_t3973682211 * value)
	{
		___sprites_5 = value;
		Il2CppCodeGenWriteBarrier(&___sprites_5, value);
	}

	inline static int32_t get_offset_of_mainImage_6() { return static_cast<int32_t>(offsetof(HelpPopup_t3079166541, ___mainImage_6)); }
	inline Image_t2042527209 * get_mainImage_6() const { return ___mainImage_6; }
	inline Image_t2042527209 ** get_address_of_mainImage_6() { return &___mainImage_6; }
	inline void set_mainImage_6(Image_t2042527209 * value)
	{
		___mainImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___mainImage_6, value);
	}
};

struct HelpPopup_t3079166541_StaticFields
{
public:
	// HelpPopupBroadCastEvent HelpPopup::openEventBroadCast
	HelpPopupBroadCastEvent_t2836716150 * ___openEventBroadCast_2;

public:
	inline static int32_t get_offset_of_openEventBroadCast_2() { return static_cast<int32_t>(offsetof(HelpPopup_t3079166541_StaticFields, ___openEventBroadCast_2)); }
	inline HelpPopupBroadCastEvent_t2836716150 * get_openEventBroadCast_2() const { return ___openEventBroadCast_2; }
	inline HelpPopupBroadCastEvent_t2836716150 ** get_address_of_openEventBroadCast_2() { return &___openEventBroadCast_2; }
	inline void set_openEventBroadCast_2(HelpPopupBroadCastEvent_t2836716150 * value)
	{
		___openEventBroadCast_2 = value;
		Il2CppCodeGenWriteBarrier(&___openEventBroadCast_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
