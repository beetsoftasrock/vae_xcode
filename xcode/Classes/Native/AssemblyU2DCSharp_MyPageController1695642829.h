﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// LessonValue[]
struct LessonValueU5BU5D_t2514548700;
// System.Collections.Generic.List`1<MyPageChapterController>
struct List_1_t1535501734;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageController
struct  MyPageController_t1695642829  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 MyPageController::totalChapter
	int32_t ___totalChapter_2;
	// UnityEngine.Transform MyPageController::containChapter
	Transform_t3275118058 * ___containChapter_3;
	// UnityEngine.GameObject MyPageController::myPageChapter
	GameObject_t1756533147 * ___myPageChapter_4;
	// LessonValue[] MyPageController::lessonValue
	LessonValueU5BU5D_t2514548700* ___lessonValue_5;
	// System.Collections.Generic.List`1<MyPageChapterController> MyPageController::lstMyPageChapter
	List_1_t1535501734 * ___lstMyPageChapter_6;

public:
	inline static int32_t get_offset_of_totalChapter_2() { return static_cast<int32_t>(offsetof(MyPageController_t1695642829, ___totalChapter_2)); }
	inline int32_t get_totalChapter_2() const { return ___totalChapter_2; }
	inline int32_t* get_address_of_totalChapter_2() { return &___totalChapter_2; }
	inline void set_totalChapter_2(int32_t value)
	{
		___totalChapter_2 = value;
	}

	inline static int32_t get_offset_of_containChapter_3() { return static_cast<int32_t>(offsetof(MyPageController_t1695642829, ___containChapter_3)); }
	inline Transform_t3275118058 * get_containChapter_3() const { return ___containChapter_3; }
	inline Transform_t3275118058 ** get_address_of_containChapter_3() { return &___containChapter_3; }
	inline void set_containChapter_3(Transform_t3275118058 * value)
	{
		___containChapter_3 = value;
		Il2CppCodeGenWriteBarrier(&___containChapter_3, value);
	}

	inline static int32_t get_offset_of_myPageChapter_4() { return static_cast<int32_t>(offsetof(MyPageController_t1695642829, ___myPageChapter_4)); }
	inline GameObject_t1756533147 * get_myPageChapter_4() const { return ___myPageChapter_4; }
	inline GameObject_t1756533147 ** get_address_of_myPageChapter_4() { return &___myPageChapter_4; }
	inline void set_myPageChapter_4(GameObject_t1756533147 * value)
	{
		___myPageChapter_4 = value;
		Il2CppCodeGenWriteBarrier(&___myPageChapter_4, value);
	}

	inline static int32_t get_offset_of_lessonValue_5() { return static_cast<int32_t>(offsetof(MyPageController_t1695642829, ___lessonValue_5)); }
	inline LessonValueU5BU5D_t2514548700* get_lessonValue_5() const { return ___lessonValue_5; }
	inline LessonValueU5BU5D_t2514548700** get_address_of_lessonValue_5() { return &___lessonValue_5; }
	inline void set_lessonValue_5(LessonValueU5BU5D_t2514548700* value)
	{
		___lessonValue_5 = value;
		Il2CppCodeGenWriteBarrier(&___lessonValue_5, value);
	}

	inline static int32_t get_offset_of_lstMyPageChapter_6() { return static_cast<int32_t>(offsetof(MyPageController_t1695642829, ___lstMyPageChapter_6)); }
	inline List_1_t1535501734 * get_lstMyPageChapter_6() const { return ___lstMyPageChapter_6; }
	inline List_1_t1535501734 ** get_address_of_lstMyPageChapter_6() { return &___lstMyPageChapter_6; }
	inline void set_lstMyPageChapter_6(List_1_t1535501734 * value)
	{
		___lstMyPageChapter_6 = value;
		Il2CppCodeGenWriteBarrier(&___lstMyPageChapter_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
