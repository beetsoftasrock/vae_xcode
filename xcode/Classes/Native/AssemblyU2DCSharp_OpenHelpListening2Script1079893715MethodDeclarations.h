﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelpListening2Script
struct OpenHelpListening2Script_t1079893715;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenHelpListening2Script::.ctor()
extern "C"  void OpenHelpListening2Script__ctor_m179248916 (OpenHelpListening2Script_t1079893715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpListening2Script::OnEnable()
extern "C"  void OpenHelpListening2Script_OnEnable_m185862976 (OpenHelpListening2Script_t1079893715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
