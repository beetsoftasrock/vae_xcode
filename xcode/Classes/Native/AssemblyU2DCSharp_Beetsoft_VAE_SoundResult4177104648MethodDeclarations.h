﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.SoundResult
struct SoundResult_t4177104648;

#include "codegen/il2cpp-codegen.h"

// System.Void Beetsoft.VAE.SoundResult::.ctor()
extern "C"  void SoundResult__ctor_m4061067183 (SoundResult_t4177104648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
