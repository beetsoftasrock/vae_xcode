﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// GlobalConfig
struct GlobalConfig_t3080413471;
// System.Collections.Generic.List`1<ChapterMyPageScript>
struct List_1_t2962650743;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageManagerScript
struct  MyPageManagerScript_t3054960093  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform MyPageManagerScript::chapterContainer
	Transform_t3275118058 * ___chapterContainer_2;
	// UnityEngine.GameObject MyPageManagerScript::prefabChapter
	GameObject_t1756533147 * ___prefabChapter_3;
	// GlobalConfig MyPageManagerScript::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_4;
	// System.Collections.Generic.List`1<ChapterMyPageScript> MyPageManagerScript::_resultViews
	List_1_t2962650743 * ____resultViews_5;

public:
	inline static int32_t get_offset_of_chapterContainer_2() { return static_cast<int32_t>(offsetof(MyPageManagerScript_t3054960093, ___chapterContainer_2)); }
	inline Transform_t3275118058 * get_chapterContainer_2() const { return ___chapterContainer_2; }
	inline Transform_t3275118058 ** get_address_of_chapterContainer_2() { return &___chapterContainer_2; }
	inline void set_chapterContainer_2(Transform_t3275118058 * value)
	{
		___chapterContainer_2 = value;
		Il2CppCodeGenWriteBarrier(&___chapterContainer_2, value);
	}

	inline static int32_t get_offset_of_prefabChapter_3() { return static_cast<int32_t>(offsetof(MyPageManagerScript_t3054960093, ___prefabChapter_3)); }
	inline GameObject_t1756533147 * get_prefabChapter_3() const { return ___prefabChapter_3; }
	inline GameObject_t1756533147 ** get_address_of_prefabChapter_3() { return &___prefabChapter_3; }
	inline void set_prefabChapter_3(GameObject_t1756533147 * value)
	{
		___prefabChapter_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefabChapter_3, value);
	}

	inline static int32_t get_offset_of_globalConfig_4() { return static_cast<int32_t>(offsetof(MyPageManagerScript_t3054960093, ___globalConfig_4)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_4() const { return ___globalConfig_4; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_4() { return &___globalConfig_4; }
	inline void set_globalConfig_4(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_4 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_4, value);
	}

	inline static int32_t get_offset_of__resultViews_5() { return static_cast<int32_t>(offsetof(MyPageManagerScript_t3054960093, ____resultViews_5)); }
	inline List_1_t2962650743 * get__resultViews_5() const { return ____resultViews_5; }
	inline List_1_t2962650743 ** get_address_of__resultViews_5() { return &____resultViews_5; }
	inline void set__resultViews_5(List_1_t2962650743 * value)
	{
		____resultViews_5 = value;
		Il2CppCodeGenWriteBarrier(&____resultViews_5, value);
	}
};

struct MyPageManagerScript_t3054960093_StaticFields
{
public:
	// System.Action MyPageManagerScript::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(MyPageManagerScript_t3054960093_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
