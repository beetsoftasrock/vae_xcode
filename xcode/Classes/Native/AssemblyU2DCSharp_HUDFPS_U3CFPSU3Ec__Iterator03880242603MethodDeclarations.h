﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HUDFPS/<FPS>c__Iterator0
struct U3CFPSU3Ec__Iterator0_t3880242603;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HUDFPS/<FPS>c__Iterator0::.ctor()
extern "C"  void U3CFPSU3Ec__Iterator0__ctor_m2026456842 (U3CFPSU3Ec__Iterator0_t3880242603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HUDFPS/<FPS>c__Iterator0::MoveNext()
extern "C"  bool U3CFPSU3Ec__Iterator0_MoveNext_m665946202 (U3CFPSU3Ec__Iterator0_t3880242603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HUDFPS/<FPS>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFPSU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2189194170 (U3CFPSU3Ec__Iterator0_t3880242603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HUDFPS/<FPS>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFPSU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1653181746 (U3CFPSU3Ec__Iterator0_t3880242603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUDFPS/<FPS>c__Iterator0::Dispose()
extern "C"  void U3CFPSU3Ec__Iterator0_Dispose_m2665854505 (U3CFPSU3Ec__Iterator0_t3880242603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUDFPS/<FPS>c__Iterator0::Reset()
extern "C"  void U3CFPSU3Ec__Iterator0_Reset_m1189320739 (U3CFPSU3Ec__Iterator0_t3880242603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
