﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IconSoundAnimation
struct IconSoundAnimation_t302426630;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void IconSoundAnimation::.ctor()
extern "C"  void IconSoundAnimation__ctor_m1876869057 (IconSoundAnimation_t302426630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconSoundAnimation::Awake()
extern "C"  void IconSoundAnimation_Awake_m4288516342 (IconSoundAnimation_t302426630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconSoundAnimation::OnEnable()
extern "C"  void IconSoundAnimation_OnEnable_m1163412017 (IconSoundAnimation_t302426630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IconSoundAnimation::DoAnimation()
extern "C"  Il2CppObject * IconSoundAnimation_DoAnimation_m679280024 (IconSoundAnimation_t302426630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconSoundAnimation::PlayAnimation()
extern "C"  void IconSoundAnimation_PlayAnimation_m914598919 (IconSoundAnimation_t302426630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconSoundAnimation::StopAnimation()
extern "C"  void IconSoundAnimation_StopAnimation_m1411782919 (IconSoundAnimation_t302426630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
