﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StopEffectCommand
struct StopEffectCommand_t3016203262;
// System.String
struct String_t;
// ConversationLogicController
struct ConversationLogicController_t3911886281;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ConversationLogicController3911886281.h"

// System.Void StopEffectCommand::.ctor(System.String,System.String)
extern "C"  void StopEffectCommand__ctor_m1858799411 (StopEffectCommand_t3016203262 * __this, String_t* ___role0, String_t* ___imageName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StopEffectCommand::Revert(ConversationLogicController)
extern "C"  void StopEffectCommand_Revert_m49921526 (StopEffectCommand_t3016203262 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String StopEffectCommand::GetRole()
extern "C"  String_t* StopEffectCommand_GetRole_m2366948684 (StopEffectCommand_t3016203262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StopEffectCommand::Execute(ConversationLogicController)
extern "C"  Il2CppObject * StopEffectCommand_Execute_m1500218821 (StopEffectCommand_t3016203262 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StopEffectCommand::AutoExecute(ConversationLogicController)
extern "C"  Il2CppObject * StopEffectCommand_AutoExecute_m2980956014 (StopEffectCommand_t3016203262 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
