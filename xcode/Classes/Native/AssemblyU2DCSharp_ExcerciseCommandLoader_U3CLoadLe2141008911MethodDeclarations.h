﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExcerciseCommandLoader/<LoadLevel>c__AnonStorey0
struct U3CLoadLevelU3Ec__AnonStorey0_t2141008911;
// CommandSheet/Param
struct Param_t4123818474;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CommandSheet_Param4123818474.h"

// System.Void ExcerciseCommandLoader/<LoadLevel>c__AnonStorey0::.ctor()
extern "C"  void U3CLoadLevelU3Ec__AnonStorey0__ctor_m3641541136 (U3CLoadLevelU3Ec__AnonStorey0_t2141008911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExcerciseCommandLoader/<LoadLevel>c__AnonStorey0::<>m__0(CommandSheet/Param)
extern "C"  bool U3CLoadLevelU3Ec__AnonStorey0_U3CU3Em__0_m899016489 (U3CLoadLevelU3Ec__AnonStorey0_t2141008911 * __this, Param_t4123818474 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
