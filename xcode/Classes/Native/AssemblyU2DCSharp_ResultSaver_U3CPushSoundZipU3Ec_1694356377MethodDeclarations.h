﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultSaver/<PushSoundZip>c__AnonStorey1
struct U3CPushSoundZipU3Ec__AnonStorey1_t1694356377;
// PushZipResponse
struct PushZipResponse_t1905049400;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PushZipResponse1905049400.h"

// System.Void ResultSaver/<PushSoundZip>c__AnonStorey1::.ctor()
extern "C"  void U3CPushSoundZipU3Ec__AnonStorey1__ctor_m973756218 (U3CPushSoundZipU3Ec__AnonStorey1_t1694356377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultSaver/<PushSoundZip>c__AnonStorey1::<>m__0(PushZipResponse)
extern "C"  void U3CPushSoundZipU3Ec__AnonStorey1_U3CU3Em__0_m2235219561 (U3CPushSoundZipU3Ec__AnonStorey1_t1694356377 * __this, PushZipResponse_t1905049400 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultSaver/<PushSoundZip>c__AnonStorey1::<>m__1()
extern "C"  void U3CPushSoundZipU3Ec__AnonStorey1_U3CU3Em__1_m2139586190 (U3CPushSoundZipU3Ec__AnonStorey1_t1694356377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
