﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<ConversationSelectionData>
struct List_1_t3459129667;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSelectionGroupData
struct  ConversationSelectionGroupData_t3437723178  : public Il2CppObject
{
public:
	// System.String ConversationSelectionGroupData::role
	String_t* ___role_0;
	// System.Collections.Generic.List`1<ConversationSelectionData> ConversationSelectionGroupData::selectionDatas
	List_1_t3459129667 * ___selectionDatas_1;

public:
	inline static int32_t get_offset_of_role_0() { return static_cast<int32_t>(offsetof(ConversationSelectionGroupData_t3437723178, ___role_0)); }
	inline String_t* get_role_0() const { return ___role_0; }
	inline String_t** get_address_of_role_0() { return &___role_0; }
	inline void set_role_0(String_t* value)
	{
		___role_0 = value;
		Il2CppCodeGenWriteBarrier(&___role_0, value);
	}

	inline static int32_t get_offset_of_selectionDatas_1() { return static_cast<int32_t>(offsetof(ConversationSelectionGroupData_t3437723178, ___selectionDatas_1)); }
	inline List_1_t3459129667 * get_selectionDatas_1() const { return ___selectionDatas_1; }
	inline List_1_t3459129667 ** get_address_of_selectionDatas_1() { return &___selectionDatas_1; }
	inline void set_selectionDatas_1(List_1_t3459129667 * value)
	{
		___selectionDatas_1 = value;
		Il2CppCodeGenWriteBarrier(&___selectionDatas_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
