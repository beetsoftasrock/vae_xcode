﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.beetsoft.utility_old.SoundManagerFactory
struct SoundManagerFactory_t947862142;
// com.beetsoft.utility_old.ISoundManager
struct ISoundManager_t2689189611;

#include "codegen/il2cpp-codegen.h"

// System.Void com.beetsoft.utility_old.SoundManagerFactory::.ctor()
extern "C"  void SoundManagerFactory__ctor_m3027944290 (SoundManagerFactory_t947862142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// com.beetsoft.utility_old.ISoundManager com.beetsoft.utility_old.SoundManagerFactory::GetSoundManager()
extern "C"  Il2CppObject * SoundManagerFactory_GetSoundManager_m2226781503 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
