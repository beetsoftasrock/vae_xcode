﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VrCameraController
struct  VrCameraController_t2570380955  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text VrCameraController::txt1
	Text_t356221433 * ___txt1_2;
	// UnityEngine.UI.Text VrCameraController::txt2
	Text_t356221433 * ___txt2_3;
	// UnityEngine.UI.Text VrCameraController::txtMainCamera
	Text_t356221433 * ___txtMainCamera_4;
	// UnityEngine.UI.Text VrCameraController::txtDebug
	Text_t356221433 * ___txtDebug_5;
	// UnityEngine.Camera VrCameraController::camera
	Camera_t189460977 * ___camera_6;
	// System.Single VrCameraController::limitLeft
	float ___limitLeft_7;
	// System.Single VrCameraController::limitRight
	float ___limitRight_8;
	// System.Single VrCameraController::limitUp
	float ___limitUp_9;
	// System.Single VrCameraController::limitDown
	float ___limitDown_10;
	// UnityEngine.Vector3 VrCameraController::head
	Vector3_t2243707580  ___head_11;
	// UnityEngine.Vector3 VrCameraController::headStart
	Vector3_t2243707580  ___headStart_12;

public:
	inline static int32_t get_offset_of_txt1_2() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___txt1_2)); }
	inline Text_t356221433 * get_txt1_2() const { return ___txt1_2; }
	inline Text_t356221433 ** get_address_of_txt1_2() { return &___txt1_2; }
	inline void set_txt1_2(Text_t356221433 * value)
	{
		___txt1_2 = value;
		Il2CppCodeGenWriteBarrier(&___txt1_2, value);
	}

	inline static int32_t get_offset_of_txt2_3() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___txt2_3)); }
	inline Text_t356221433 * get_txt2_3() const { return ___txt2_3; }
	inline Text_t356221433 ** get_address_of_txt2_3() { return &___txt2_3; }
	inline void set_txt2_3(Text_t356221433 * value)
	{
		___txt2_3 = value;
		Il2CppCodeGenWriteBarrier(&___txt2_3, value);
	}

	inline static int32_t get_offset_of_txtMainCamera_4() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___txtMainCamera_4)); }
	inline Text_t356221433 * get_txtMainCamera_4() const { return ___txtMainCamera_4; }
	inline Text_t356221433 ** get_address_of_txtMainCamera_4() { return &___txtMainCamera_4; }
	inline void set_txtMainCamera_4(Text_t356221433 * value)
	{
		___txtMainCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___txtMainCamera_4, value);
	}

	inline static int32_t get_offset_of_txtDebug_5() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___txtDebug_5)); }
	inline Text_t356221433 * get_txtDebug_5() const { return ___txtDebug_5; }
	inline Text_t356221433 ** get_address_of_txtDebug_5() { return &___txtDebug_5; }
	inline void set_txtDebug_5(Text_t356221433 * value)
	{
		___txtDebug_5 = value;
		Il2CppCodeGenWriteBarrier(&___txtDebug_5, value);
	}

	inline static int32_t get_offset_of_camera_6() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___camera_6)); }
	inline Camera_t189460977 * get_camera_6() const { return ___camera_6; }
	inline Camera_t189460977 ** get_address_of_camera_6() { return &___camera_6; }
	inline void set_camera_6(Camera_t189460977 * value)
	{
		___camera_6 = value;
		Il2CppCodeGenWriteBarrier(&___camera_6, value);
	}

	inline static int32_t get_offset_of_limitLeft_7() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___limitLeft_7)); }
	inline float get_limitLeft_7() const { return ___limitLeft_7; }
	inline float* get_address_of_limitLeft_7() { return &___limitLeft_7; }
	inline void set_limitLeft_7(float value)
	{
		___limitLeft_7 = value;
	}

	inline static int32_t get_offset_of_limitRight_8() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___limitRight_8)); }
	inline float get_limitRight_8() const { return ___limitRight_8; }
	inline float* get_address_of_limitRight_8() { return &___limitRight_8; }
	inline void set_limitRight_8(float value)
	{
		___limitRight_8 = value;
	}

	inline static int32_t get_offset_of_limitUp_9() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___limitUp_9)); }
	inline float get_limitUp_9() const { return ___limitUp_9; }
	inline float* get_address_of_limitUp_9() { return &___limitUp_9; }
	inline void set_limitUp_9(float value)
	{
		___limitUp_9 = value;
	}

	inline static int32_t get_offset_of_limitDown_10() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___limitDown_10)); }
	inline float get_limitDown_10() const { return ___limitDown_10; }
	inline float* get_address_of_limitDown_10() { return &___limitDown_10; }
	inline void set_limitDown_10(float value)
	{
		___limitDown_10 = value;
	}

	inline static int32_t get_offset_of_head_11() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___head_11)); }
	inline Vector3_t2243707580  get_head_11() const { return ___head_11; }
	inline Vector3_t2243707580 * get_address_of_head_11() { return &___head_11; }
	inline void set_head_11(Vector3_t2243707580  value)
	{
		___head_11 = value;
	}

	inline static int32_t get_offset_of_headStart_12() { return static_cast<int32_t>(offsetof(VrCameraController_t2570380955, ___headStart_12)); }
	inline Vector3_t2243707580  get_headStart_12() const { return ___headStart_12; }
	inline Vector3_t2243707580 * get_address_of_headStart_12() { return &___headStart_12; }
	inline void set_headStart_12(Vector3_t2243707580  value)
	{
		___headStart_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
