﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResultStatisticView
struct ResultStatisticView_t1911483820;
// ResultStatisticChart
struct ResultStatisticChart_t3759290537;
// ResultStatisticParam
struct ResultStatisticParam_t3727789074;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultStatisticController
struct  ResultStatisticController_t2929782681  : public MonoBehaviour_t1158329972
{
public:
	// ResultStatisticView ResultStatisticController::view
	ResultStatisticView_t1911483820 * ___view_2;
	// ResultStatisticChart ResultStatisticController::chart
	ResultStatisticChart_t3759290537 * ___chart_3;
	// ResultStatisticParam ResultStatisticController::param
	ResultStatisticParam_t3727789074 * ___param_4;
	// System.String ResultStatisticController::nameLesson
	String_t* ___nameLesson_5;

public:
	inline static int32_t get_offset_of_view_2() { return static_cast<int32_t>(offsetof(ResultStatisticController_t2929782681, ___view_2)); }
	inline ResultStatisticView_t1911483820 * get_view_2() const { return ___view_2; }
	inline ResultStatisticView_t1911483820 ** get_address_of_view_2() { return &___view_2; }
	inline void set_view_2(ResultStatisticView_t1911483820 * value)
	{
		___view_2 = value;
		Il2CppCodeGenWriteBarrier(&___view_2, value);
	}

	inline static int32_t get_offset_of_chart_3() { return static_cast<int32_t>(offsetof(ResultStatisticController_t2929782681, ___chart_3)); }
	inline ResultStatisticChart_t3759290537 * get_chart_3() const { return ___chart_3; }
	inline ResultStatisticChart_t3759290537 ** get_address_of_chart_3() { return &___chart_3; }
	inline void set_chart_3(ResultStatisticChart_t3759290537 * value)
	{
		___chart_3 = value;
		Il2CppCodeGenWriteBarrier(&___chart_3, value);
	}

	inline static int32_t get_offset_of_param_4() { return static_cast<int32_t>(offsetof(ResultStatisticController_t2929782681, ___param_4)); }
	inline ResultStatisticParam_t3727789074 * get_param_4() const { return ___param_4; }
	inline ResultStatisticParam_t3727789074 ** get_address_of_param_4() { return &___param_4; }
	inline void set_param_4(ResultStatisticParam_t3727789074 * value)
	{
		___param_4 = value;
		Il2CppCodeGenWriteBarrier(&___param_4, value);
	}

	inline static int32_t get_offset_of_nameLesson_5() { return static_cast<int32_t>(offsetof(ResultStatisticController_t2929782681, ___nameLesson_5)); }
	inline String_t* get_nameLesson_5() const { return ___nameLesson_5; }
	inline String_t** get_address_of_nameLesson_5() { return &___nameLesson_5; }
	inline void set_nameLesson_5(String_t* value)
	{
		___nameLesson_5 = value;
		Il2CppCodeGenWriteBarrier(&___nameLesson_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
