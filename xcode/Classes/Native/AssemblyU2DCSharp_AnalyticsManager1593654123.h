﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnalyticsManager
struct AnalyticsManager_t1593654123;
// GlobalConfig
struct GlobalConfig_t3080413471;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsManager
struct  AnalyticsManager_t1593654123  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct AnalyticsManager_t1593654123_StaticFields
{
public:
	// AnalyticsManager AnalyticsManager::instance
	AnalyticsManager_t1593654123 * ___instance_2;
	// GlobalConfig AnalyticsManager::_globalConfig
	GlobalConfig_t3080413471 * ____globalConfig_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(AnalyticsManager_t1593654123_StaticFields, ___instance_2)); }
	inline AnalyticsManager_t1593654123 * get_instance_2() const { return ___instance_2; }
	inline AnalyticsManager_t1593654123 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AnalyticsManager_t1593654123 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of__globalConfig_3() { return static_cast<int32_t>(offsetof(AnalyticsManager_t1593654123_StaticFields, ____globalConfig_3)); }
	inline GlobalConfig_t3080413471 * get__globalConfig_3() const { return ____globalConfig_3; }
	inline GlobalConfig_t3080413471 ** get_address_of__globalConfig_3() { return &____globalConfig_3; }
	inline void set__globalConfig_3(GlobalConfig_t3080413471 * value)
	{
		____globalConfig_3 = value;
		Il2CppCodeGenWriteBarrier(&____globalConfig_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
