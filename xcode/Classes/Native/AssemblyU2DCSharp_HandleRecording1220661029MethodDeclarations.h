﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HandleRecording
struct HandleRecording_t1220661029;
// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"

// System.Void HandleRecording::.ctor()
extern "C"  void HandleRecording__ctor_m3326858544 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::OnEnable()
extern "C"  void HandleRecording_OnEnable_m1470355136 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::OnDisable()
extern "C"  void HandleRecording_OnDisable_m65248425 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::StartRecording()
extern "C"  void HandleRecording_StartRecording_m3845378673 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::StopRecording()
extern "C"  void HandleRecording_StopRecording_m4273354933 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::SaveRecording()
extern "C"  void HandleRecording_SaveRecording_m3095314376 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::ResetRecord()
extern "C"  void HandleRecording_ResetRecord_m758604558 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::PlayRecorded()
extern "C"  void HandleRecording_PlayRecorded_m53199560 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::Start()
extern "C"  void HandleRecording_Start_m2597790592 (HandleRecording_t1220661029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::OnInit(System.Boolean)
extern "C"  void HandleRecording_OnInit_m4054645600 (HandleRecording_t1220661029 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::OnError(System.String)
extern "C"  void HandleRecording_OnError_m527243019 (HandleRecording_t1220661029 * __this, String_t* ___errMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::OnRecordingFinish(UnityEngine.AudioClip)
extern "C"  void HandleRecording_OnRecordingFinish_m1300851856 (HandleRecording_t1220661029 * __this, AudioClip_t1932558630 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandleRecording::OnRecordingSaved(System.String)
extern "C"  void HandleRecording_OnRecordingSaved_m4238606871 (HandleRecording_t1220661029 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
