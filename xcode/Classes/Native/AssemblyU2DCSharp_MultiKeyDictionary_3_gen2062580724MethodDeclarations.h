﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MultiKeyDictionary_3_gen2642076887MethodDeclarations.h"

// System.Void MultiKeyDictionary`3<System.String,System.String,Reporter/Log>::.ctor()
#define MultiKeyDictionary_3__ctor_m1506240650(__this, method) ((  void (*) (MultiKeyDictionary_3_t2062580724 *, const MethodInfo*))MultiKeyDictionary_3__ctor_m2884643606_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2<T2,T3> MultiKeyDictionary`3<System.String,System.String,Reporter/Log>::get_Item(T1)
#define MultiKeyDictionary_3_get_Item_m1394430522(__this, ___key0, method) ((  Dictionary_2_t1223994146 * (*) (MultiKeyDictionary_3_t2062580724 *, String_t*, const MethodInfo*))MultiKeyDictionary_3_get_Item_m186052900_gshared)(__this, ___key0, method)
// System.Boolean MultiKeyDictionary`3<System.String,System.String,Reporter/Log>::ContainsKey(T1,T2)
#define MultiKeyDictionary_3_ContainsKey_m3562637801(__this, ___key10, ___key21, method) ((  bool (*) (MultiKeyDictionary_3_t2062580724 *, String_t*, String_t*, const MethodInfo*))MultiKeyDictionary_3_ContainsKey_m2647488417_gshared)(__this, ___key10, ___key21, method)
