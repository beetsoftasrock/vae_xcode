﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3647166229(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3276306664 *, String_t*, Log_t3604182180 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::get_Key()
#define KeyValuePair_2_get_Key_m3999180315(__this, method) ((  String_t* (*) (KeyValuePair_2_t3276306664 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2075307916(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3276306664 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::get_Value()
#define KeyValuePair_2_get_Value_m3017665307(__this, method) ((  Log_t3604182180 * (*) (KeyValuePair_2_t3276306664 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2354230300(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3276306664 *, Log_t3604182180 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::ToString()
#define KeyValuePair_2_ToString_m3756615570(__this, method) ((  String_t* (*) (KeyValuePair_2_t3276306664 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
