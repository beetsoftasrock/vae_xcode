﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Enum
struct Enum_t2459695545;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Enum2459695545.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Boolean PlayerPrefsUtility::IsEncryptedKey(System.String)
extern "C"  bool PlayerPrefsUtility_IsEncryptedKey_m1053079699 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayerPrefsUtility::DecryptKey(System.String)
extern "C"  String_t* PlayerPrefsUtility_DecryptKey_m3431852559 (Il2CppObject * __this /* static, unused */, String_t* ___encryptedKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::SetEncryptedFloat(System.String,System.Single)
extern "C"  void PlayerPrefsUtility_SetEncryptedFloat_m3597413281 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::SetEncryptedInt(System.String,System.Int32)
extern "C"  void PlayerPrefsUtility_SetEncryptedInt_m1804400150 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::SetEncryptedString(System.String,System.String)
extern "C"  void PlayerPrefsUtility_SetEncryptedString_m781937783 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerPrefsUtility::GetEncryptedValue(System.String,System.String)
extern "C"  Il2CppObject * PlayerPrefsUtility_GetEncryptedValue_m3482207668 (Il2CppObject * __this /* static, unused */, String_t* ___encryptedKey0, String_t* ___encryptedValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PlayerPrefsUtility::GetEncryptedFloat(System.String,System.Single)
extern "C"  float PlayerPrefsUtility_GetEncryptedFloat_m4094507839 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayerPrefsUtility::GetEncryptedInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefsUtility_GetEncryptedInt_m734528552 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayerPrefsUtility::GetEncryptedString(System.String,System.String)
extern "C"  String_t* PlayerPrefsUtility_GetEncryptedString_m2839884170 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::SetBool(System.String,System.Boolean)
extern "C"  void PlayerPrefsUtility_SetBool_m2328355135 (Il2CppObject * __this /* static, unused */, String_t* ___key0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsUtility::GetBool(System.String,System.Boolean)
extern "C"  bool PlayerPrefsUtility_GetBool_m3642735395 (Il2CppObject * __this /* static, unused */, String_t* ___key0, bool ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::SetEnum(System.String,System.Enum)
extern "C"  void PlayerPrefsUtility_SetEnum_m3732505213 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Enum_t2459695545 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerPrefsUtility::GetEnum(System.String,System.Type,System.Object)
extern "C"  Il2CppObject * PlayerPrefsUtility_GetEnum_m3821519537 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Type_t * ___enumType1, Il2CppObject * ___defaultValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::SetDateTime(System.String,System.DateTime)
extern "C"  void PlayerPrefsUtility_SetDateTime_m904454141 (Il2CppObject * __this /* static, unused */, String_t* ___key0, DateTime_t693205669  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PlayerPrefsUtility::GetDateTime(System.String,System.DateTime)
extern "C"  DateTime_t693205669  PlayerPrefsUtility_GetDateTime_m3809683394 (Il2CppObject * __this /* static, unused */, String_t* ___key0, DateTime_t693205669  ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsUtility::SetTimeSpan(System.String,System.TimeSpan)
extern "C"  void PlayerPrefsUtility_SetTimeSpan_m1774994493 (Il2CppObject * __this /* static, unused */, String_t* ___key0, TimeSpan_t3430258949  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan PlayerPrefsUtility::GetTimeSpan(System.String,System.TimeSpan)
extern "C"  TimeSpan_t3430258949  PlayerPrefsUtility_GetTimeSpan_m607976478 (Il2CppObject * __this /* static, unused */, String_t* ___key0, TimeSpan_t3430258949  ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
