﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAP_UIScript/<WaitFromValidateIAP>c__Iterator0
struct U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void IAP_UIScript/<WaitFromValidateIAP>c__Iterator0::.ctor()
extern "C"  void U3CWaitFromValidateIAPU3Ec__Iterator0__ctor_m2530478652 (U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IAP_UIScript/<WaitFromValidateIAP>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitFromValidateIAPU3Ec__Iterator0_MoveNext_m4040401676 (U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IAP_UIScript/<WaitFromValidateIAP>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitFromValidateIAPU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1471826768 (U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IAP_UIScript/<WaitFromValidateIAP>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitFromValidateIAPU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3999072648 (U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript/<WaitFromValidateIAP>c__Iterator0::Dispose()
extern "C"  void U3CWaitFromValidateIAPU3Ec__Iterator0_Dispose_m98292775 (U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript/<WaitFromValidateIAP>c__Iterator0::Reset()
extern "C"  void U3CWaitFromValidateIAPU3Ec__Iterator0_Reset_m849653693 (U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
