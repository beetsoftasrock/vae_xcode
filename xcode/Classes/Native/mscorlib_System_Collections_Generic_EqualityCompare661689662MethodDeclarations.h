﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct EqualityComparer_1_t661689662;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m750496854_gshared (EqualityComparer_1_t661689662 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m750496854(__this, method) ((  void (*) (EqualityComparer_1_t661689662 *, const MethodInfo*))EqualityComparer_1__ctor_m750496854_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3574040039_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3574040039(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3574040039_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2901496075_gshared (EqualityComparer_1_t661689662 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2901496075(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t661689662 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2901496075_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2014851537_gshared (EqualityComparer_1_t661689662 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2014851537(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t661689662 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2014851537_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Default()
extern "C"  EqualityComparer_1_t661689662 * EqualityComparer_1_get_Default_m2485223998_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2485223998(__this /* static, unused */, method) ((  EqualityComparer_1_t661689662 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2485223998_gshared)(__this /* static, unused */, method)
