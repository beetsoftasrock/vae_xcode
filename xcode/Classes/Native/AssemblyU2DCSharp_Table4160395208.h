﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Column[]
struct ColumnU5BU5D_t2406440995;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Table
struct  Table_t4160395208  : public Il2CppObject
{
public:
	// Column[] Table::columns
	ColumnU5BU5D_t2406440995* ___columns_0;

public:
	inline static int32_t get_offset_of_columns_0() { return static_cast<int32_t>(offsetof(Table_t4160395208, ___columns_0)); }
	inline ColumnU5BU5D_t2406440995* get_columns_0() const { return ___columns_0; }
	inline ColumnU5BU5D_t2406440995** get_address_of_columns_0() { return &___columns_0; }
	inline void set_columns_0(ColumnU5BU5D_t2406440995* value)
	{
		___columns_0 = value;
		Il2CppCodeGenWriteBarrier(&___columns_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
