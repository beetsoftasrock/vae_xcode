﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopConfig
struct ChapterTopConfig_t174003822;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterTopConfig::.ctor()
extern "C"  void ChapterTopConfig__ctor_m121009415 (ChapterTopConfig_t174003822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
