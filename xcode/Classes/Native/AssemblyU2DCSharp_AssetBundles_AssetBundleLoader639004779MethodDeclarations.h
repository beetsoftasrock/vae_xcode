﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleLoader
struct AssetBundleLoader_t639004779;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// GlobalConfig
struct GlobalConfig_t3080413471;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AssetBundles.AssetBundleLoader::.ctor()
extern "C"  void AssetBundleLoader__ctor_m3756457463 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> AssetBundles.AssetBundleLoader::get_AvailableChapters()
extern "C"  List_1_t1398341365 * AssetBundleLoader_get_AvailableChapters_m1161994024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::set_AvailableChapters(System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetBundleLoader_set_AvailableChapters_m2948411857 (Il2CppObject * __this /* static, unused */, List_1_t1398341365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleLoader::get_IsDoneFirstDownload()
extern "C"  bool AssetBundleLoader_get_IsDoneFirstDownload_m1090732552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::set_IsDoneFirstDownload(System.Boolean)
extern "C"  void AssetBundleLoader_set_IsDoneFirstDownload_m2264204301 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalConfig AssetBundles.AssetBundleLoader::get_globalConfig()
extern "C"  GlobalConfig_t3080413471 * AssetBundleLoader_get_globalConfig_m3295788317 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AssetBundles.AssetBundleLoader::get_CurrentOpenedChapter()
extern "C"  int32_t AssetBundleLoader_get_CurrentOpenedChapter_m3772612165 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::set_CurrentOpenedChapter(System.Int32)
extern "C"  void AssetBundleLoader_set_CurrentOpenedChapter_m2710202894 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::ForceShow(System.Boolean,System.Action)
extern "C"  void AssetBundleLoader_ForceShow_m205635295 (Il2CppObject * __this /* static, unused */, bool ___showConfirm0, Action_t3226471752 * ___onCompleted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::Start()
extern "C"  void AssetBundleLoader_Start_m2564749939 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::Update()
extern "C"  void AssetBundleLoader_Update_m1487209414 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::resetViewToBegin()
extern "C"  void AssetBundleLoader_resetViewToBegin_m3178357843 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::listingChapters()
extern "C"  void AssetBundleLoader_listingChapters_m508873079 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AssetBundles.AssetBundleLoader::forceShow()
extern "C"  Il2CppObject * AssetBundleLoader_forceShow_m1996378479 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::onProcess(System.Single)
extern "C"  void AssetBundleLoader_onProcess_m1708253014 (AssetBundleLoader_t639004779 * __this, float ___overallProg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::onSuccess()
extern "C"  void AssetBundleLoader_onSuccess_m3677718909 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::onError(System.String)
extern "C"  void AssetBundleLoader_onError_m2875381076 (AssetBundleLoader_t639004779 * __this, String_t* ___errMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::OnClickClose()
extern "C"  void AssetBundleLoader_OnClickClose_m4058831118 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader::OnClickRetry()
extern "C"  void AssetBundleLoader_OnClickRetry_m2421356808 (AssetBundleLoader_t639004779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
