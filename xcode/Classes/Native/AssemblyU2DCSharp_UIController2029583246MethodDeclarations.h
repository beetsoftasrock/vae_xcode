﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIController
struct UIController_t2029583246;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void UIController::.ctor()
extern "C"  void UIController__ctor_m4279849967 (UIController_t2029583246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::Start()
extern "C"  void UIController_Start_m1806544859 (UIController_t2029583246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::Update()
extern "C"  void UIController_Update_m1711433880 (UIController_t2029583246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::SetCharacter2D(System.String,UnityEngine.Vector3)
extern "C"  void UIController_SetCharacter2D_m2380900813 (UIController_t2029583246 * __this, String_t* ___prefabs0, Vector3_t2243707580  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
