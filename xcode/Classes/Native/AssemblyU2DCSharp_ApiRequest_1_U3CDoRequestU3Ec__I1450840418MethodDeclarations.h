﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApiRequest`1/<DoRequest>c__Iterator0<System.Object>
struct U3CDoRequestU3Ec__Iterator0_t1450840418;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ApiRequest`1/<DoRequest>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CDoRequestU3Ec__Iterator0__ctor_m2351552645_gshared (U3CDoRequestU3Ec__Iterator0_t1450840418 * __this, const MethodInfo* method);
#define U3CDoRequestU3Ec__Iterator0__ctor_m2351552645(__this, method) ((  void (*) (U3CDoRequestU3Ec__Iterator0_t1450840418 *, const MethodInfo*))U3CDoRequestU3Ec__Iterator0__ctor_m2351552645_gshared)(__this, method)
// System.Boolean ApiRequest`1/<DoRequest>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CDoRequestU3Ec__Iterator0_MoveNext_m357290071_gshared (U3CDoRequestU3Ec__Iterator0_t1450840418 * __this, const MethodInfo* method);
#define U3CDoRequestU3Ec__Iterator0_MoveNext_m357290071(__this, method) ((  bool (*) (U3CDoRequestU3Ec__Iterator0_t1450840418 *, const MethodInfo*))U3CDoRequestU3Ec__Iterator0_MoveNext_m357290071_gshared)(__this, method)
// System.Object ApiRequest`1/<DoRequest>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoRequestU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m539114923_gshared (U3CDoRequestU3Ec__Iterator0_t1450840418 * __this, const MethodInfo* method);
#define U3CDoRequestU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m539114923(__this, method) ((  Il2CppObject * (*) (U3CDoRequestU3Ec__Iterator0_t1450840418 *, const MethodInfo*))U3CDoRequestU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m539114923_gshared)(__this, method)
// System.Object ApiRequest`1/<DoRequest>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoRequestU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3021541187_gshared (U3CDoRequestU3Ec__Iterator0_t1450840418 * __this, const MethodInfo* method);
#define U3CDoRequestU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3021541187(__this, method) ((  Il2CppObject * (*) (U3CDoRequestU3Ec__Iterator0_t1450840418 *, const MethodInfo*))U3CDoRequestU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3021541187_gshared)(__this, method)
// System.Void ApiRequest`1/<DoRequest>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CDoRequestU3Ec__Iterator0_Dispose_m95331084_gshared (U3CDoRequestU3Ec__Iterator0_t1450840418 * __this, const MethodInfo* method);
#define U3CDoRequestU3Ec__Iterator0_Dispose_m95331084(__this, method) ((  void (*) (U3CDoRequestU3Ec__Iterator0_t1450840418 *, const MethodInfo*))U3CDoRequestU3Ec__Iterator0_Dispose_m95331084_gshared)(__this, method)
// System.Void ApiRequest`1/<DoRequest>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CDoRequestU3Ec__Iterator0_Reset_m439082266_gshared (U3CDoRequestU3Ec__Iterator0_t1450840418 * __this, const MethodInfo* method);
#define U3CDoRequestU3Ec__Iterator0_Reset_m439082266(__this, method) ((  void (*) (U3CDoRequestU3Ec__Iterator0_t1450840418 *, const MethodInfo*))U3CDoRequestU3Ec__Iterator0_Reset_m439082266_gshared)(__this, method)
