﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedSentence2SceneSetup
struct ChapterBasedSentence2SceneSetup_t1868199748;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedSentence2SceneSetup::.ctor()
extern "C"  void ChapterBasedSentence2SceneSetup__ctor_m353310649 (ChapterBasedSentence2SceneSetup_t1868199748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedSentence2SceneSetup::Awake()
extern "C"  void ChapterBasedSentence2SceneSetup_Awake_m421429072 (ChapterBasedSentence2SceneSetup_t1868199748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
