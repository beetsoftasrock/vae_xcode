﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayerTalkModule_U3CPlayPlayerRec863409196.h"
#include "AssemblyU2DCSharp_SubtextControlModule4093059790.h"
#include "AssemblyU2DCSharp_ThinkingEffectModule4011786459.h"
#include "AssemblyU2DCSharp_VoiceRecordModule679113955.h"
#include "AssemblyU2DCSharp_VoiceRecordModule_U3CDoReplayReco381392127.h"
#include "AssemblyU2DCSharp_VoiceRecordModuleNormal1880663460.h"
#include "AssemblyU2DCSharp_PracticeTextBasedCharacterSelect3916885040.h"
#include "AssemblyU2DCSharp_ScreenOriSupport692417341.h"
#include "AssemblyU2DCSharp_OnCharacterSelectionViewClickedE3002519456.h"
#include "AssemblyU2DCSharp_CharacterSelectionView2239851166.h"
#include "AssemblyU2DCSharp_CharacterSelector2041732578.h"
#include "AssemblyU2DCSharp_CharacterSelector_CharacterChang2435892010.h"
#include "AssemblyU2DCSharp_ConversationHistoryItemView3846761105.h"
#include "AssemblyU2DCSharp_ConversationHistoryItemView_Conv3133779843.h"
#include "AssemblyU2DCSharp_OnConversationSceneSelectionView1625773638.h"
#include "AssemblyU2DCSharp_ConversationSceneSelectionView407867144.h"
#include "AssemblyU2DCSharp_ConversationSceneSelector2926116424.h"
#include "AssemblyU2DCSharp_SelectionButtonSelectedEvent3088917173.h"
#include "AssemblyU2DCSharp_ConversationSelectionButtonView265815670.h"
#include "AssemblyU2DCSharp_ConversationSelectionDialog2460570659.h"
#include "AssemblyU2DCSharp_ConversationSelectionDialogNorna1475688335.h"
#include "AssemblyU2DCSharp_ConversationSelectionDialogNorna3447868340.h"
#include "AssemblyU2DCSharp_ConversationSelectionDialogVR87316163.h"
#include "AssemblyU2DCSharp_ConversationSelectionDialogVR_U3C892918678.h"
#include "AssemblyU2DCSharp_ConversationSelectionTextView4281204247.h"
#include "AssemblyU2DCSharp_OnExcerciseSceneSelectionViewCli3939024814.h"
#include "AssemblyU2DCSharp_ExcerciseSceneSelectionView3932267486.h"
#include "AssemblyU2DCSharp_ExcerciseSceneSelector1138496382.h"
#include "AssemblyU2DCSharp_HistoryConversationDialog3676407505.h"
#include "AssemblyU2DCSharp_HistoryConversationDialog_U3CLerp925026203.h"
#include "AssemblyU2DCSharp_HistoryConversationDialog_U3CAdd1274435845.h"
#include "AssemblyU2DCSharp_HistoryConversationDialog_U3CRem3256731473.h"
#include "AssemblyU2DCSharp_HistoryConversationDialog_U3CMov3283600098.h"
#include "AssemblyU2DCSharp_IconSoundAnimation302426630.h"
#include "AssemblyU2DCSharp_IconSoundAnimation_U3CDoAnimatio3928869029.h"
#include "AssemblyU2DCSharp_OtherThinkingView677091083.h"
#include "AssemblyU2DCSharp_PlayOtherTalkDialog78515058.h"
#include "AssemblyU2DCSharp_PlayOtherTalkDialog_U3CPlayOtherT157632481.h"
#include "AssemblyU2DCSharp_PlayOtherTalkDialog_U3CPlayU3Ec_3847971905.h"
#include "AssemblyU2DCSharp_PlayOtherTalkDialog_U3CPlayKaraO3334740973.h"
#include "AssemblyU2DCSharp_ReplayTalkDialog3732871695.h"
#include "AssemblyU2DCSharp_ReplayTalkDialog_U3CPlayU3Ec__It4017322827.h"
#include "AssemblyU2DCSharp_ReplayTalkDialog_U3CFakePlayU3Ec3881532539.h"
#include "AssemblyU2DCSharp_ReplayTalkDialog_U3CPlayKaraOkeE3474948290.h"
#include "AssemblyU2DCSharp_ScrollViewExtendControl924978223.h"
#include "AssemblyU2DCSharp_ScrollViewExtendControl_U3CDoMov1473839312.h"
#include "AssemblyU2DCSharp_TalkDialog1412511810.h"
#include "AssemblyU2DCSharp_TalkDialogWithSoundButton1663397415.h"
#include "AssemblyU2DCSharp_VRConversationSelectionButtonVie1568012346.h"
#include "AssemblyU2DCSharp_VoiceRecordingDialog3950460413.h"
#include "AssemblyU2DCSharp_VoiceRecordingDialog_U3CPlayU3Ec2469087745.h"
#include "AssemblyU2DCSharp_VoiceRecordingDialog_U3CPlayKara4221776619.h"
#include "AssemblyU2DCSharp_ConversationAutoplayHelper1693449450.h"
#include "AssemblyU2DCSharp_CrossSceneParam3804493877.h"
#include "AssemblyU2DCSharp_ExerciseAutoplayHelper2248779473.h"
#include "AssemblyU2DCSharp_KaraokeTextEffect1279919556.h"
#include "AssemblyU2DCSharp_KaraokeTextEffect_U3CPlayEffectU4283951911.h"
#include "AssemblyU2DCSharp_SoundRecorder3431875919.h"
#include "AssemblyU2DCSharp_ConversationVRSwitcher368413640.h"
#include "AssemblyU2DCSharp_ConversationVRSwitcher_U3CGoToNo3065078691.h"
#include "AssemblyU2DCSharp_ConversationVRSwitcher_U3CGoToVR3564574473.h"
#include "AssemblyU2DCSharp_ConversationVRSwitcher_U3CQuitVRM289205990.h"
#include "AssemblyU2DCSharp_ExerciseVRSwitcher1425704337.h"
#include "AssemblyU2DCSharp_ExerciseVRSwitcher_U3CGoToNormal2614165850.h"
#include "AssemblyU2DCSharp_ExerciseVRSwitcher_U3CGoToVRMode3426300496.h"
#include "AssemblyU2DCSharp_ExerciseVRSwitcher_U3CQuitVRMode2941453283.h"
#include "AssemblyU2DCSharp_PracticeVRSwitcher654240754.h"
#include "AssemblyU2DCSharp_PracticeVRSwitcher_U3CGoToNormal4246182585.h"
#include "AssemblyU2DCSharp_PracticeVRSwitcher_U3CGoToVRMode1203783803.h"
#include "AssemblyU2DCSharp_PracticeVRSwitcher_U3CQuitVRMode1152556888.h"
#include "AssemblyU2DCSharp_ChapterItem2155291334.h"
#include "AssemblyU2DCSharp_CachedDragInfo1136705792.h"
#include "AssemblyU2DCSharp_ChapterSelector191193704.h"
#include "AssemblyU2DCSharp_ChapterSelector_U3CUpdateCacheU33729494129.h"
#include "AssemblyU2DCSharp_ChapterSelector_U3ConEnableU3Ec_2146627610.h"
#include "AssemblyU2DCSharp_ChapterSelector_U3ConEnableU3Ec_1980318813.h"
#include "AssemblyU2DCSharp_ChapterSelector_U3CrefreshItemsS4260695815.h"
#include "AssemblyU2DCSharp_ChapterSelector_U3CjumpToChapter1172627506.h"
#include "AssemblyU2DCSharp_ChapterSelector_U3CDofocusSelecte945102547.h"
#include "AssemblyU2DCSharp_EditUserNameScript1012132347.h"
#include "AssemblyU2DCSharp_HomeCotroller2470068823.h"
#include "AssemblyU2DCSharp_HomeCotroller_State4150594815.h"
#include "AssemblyU2DCSharp_HomeCotroller_U3CUpdateCacheU3Ec2625163487.h"
#include "AssemblyU2DCSharp_HomeCotroller_U3CMovingCloseU3Ec1094654469.h"
#include "AssemblyU2DCSharp_HomeCotroller_U3CMovingOpenU3Ec__326153120.h"
#include "AssemblyU2DCSharp_HomeCotroller_U3CMovingU3Ec__Ite2354585089.h"
#include "AssemblyU2DCSharp_InputCodeScript3672565358.h"
#include "AssemblyU2DCSharp_MenuDragHandler1912038821.h"
#include "AssemblyU2DCSharp_ResetData4114871043.h"
#include "AssemblyU2DCSharp_ClickImmediate319099361.h"
#include "AssemblyU2DCSharp_IAP_Popup2375878299.h"
#include "AssemblyU2DCSharp_IAP_UIScript1776166942.h"
#include "AssemblyU2DCSharp_IAP_UIScript_U3CWaitFromValidate2555116557.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser1674559779.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_U3CStartU4187929873.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (U3CPlayPlayerRecordedTalkEffectU3Ec__Iterator3_t863409196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[5] = 
{
	U3CPlayPlayerRecordedTalkEffectU3Ec__Iterator3_t863409196::get_offset_of_U3CclipU3E__0_0(),
	U3CPlayPlayerRecordedTalkEffectU3Ec__Iterator3_t863409196::get_offset_of_U24this_1(),
	U3CPlayPlayerRecordedTalkEffectU3Ec__Iterator3_t863409196::get_offset_of_U24current_2(),
	U3CPlayPlayerRecordedTalkEffectU3Ec__Iterator3_t863409196::get_offset_of_U24disposing_3(),
	U3CPlayPlayerRecordedTalkEffectU3Ec__Iterator3_t863409196::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (SubtextControlModule_t4093059790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[1] = 
{
	SubtextControlModule_t4093059790::get_offset_of__isShowing_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (ThinkingEffectModule_t4011786459), -1, sizeof(ThinkingEffectModule_t4011786459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2404[4] = 
{
	ThinkingEffectModule_t4011786459::get_offset_of_clc_2(),
	ThinkingEffectModule_t4011786459::get_offset_of_U3CpreThinkingSpriteU3Ek__BackingField_3(),
	ThinkingEffectModule_t4011786459::get_offset_of_thinkingView_4(),
	ThinkingEffectModule_t4011786459_StaticFields::get_offset_of__globalConfig_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (VoiceRecordModule_t679113955), -1, sizeof(VoiceRecordModule_t679113955_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2406[11] = 
{
	VoiceRecordModule_t679113955::get_offset_of_bcc_2(),
	VoiceRecordModule_t679113955::get_offset_of_clc_3(),
	VoiceRecordModule_t679113955::get_offset_of_playerTalkModule_4(),
	VoiceRecordModule_t679113955::get_offset_of_voiceRecordingDialog_5(),
	VoiceRecordModule_t679113955::get_offset_of_replayRecordedButton_6(),
	VoiceRecordModule_t679113955::get_offset_of_bottomButtons_7(),
	VoiceRecordModule_t679113955::get_offset_of_soundRecorder_8(),
	VoiceRecordModule_t679113955::get_offset_of_replayTalkDialog_9(),
	VoiceRecordModule_t679113955_StaticFields::get_offset_of_trackCountRecord_10(),
	VoiceRecordModule_t679113955::get_offset_of_startRecordTime_11(),
	VoiceRecordModule_t679113955::get_offset_of__replayCo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (U3CDoReplayRecordedU3Ec__Iterator0_t381392127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[4] = 
{
	U3CDoReplayRecordedU3Ec__Iterator0_t381392127::get_offset_of_U24this_0(),
	U3CDoReplayRecordedU3Ec__Iterator0_t381392127::get_offset_of_U24current_1(),
	U3CDoReplayRecordedU3Ec__Iterator0_t381392127::get_offset_of_U24disposing_2(),
	U3CDoReplayRecordedU3Ec__Iterator0_t381392127::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (VoiceRecordModuleNormal_t1880663460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (PracticeTextBasedCharacterSelect_t3916885040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[4] = 
{
	0,
	0,
	PracticeTextBasedCharacterSelect_t3916885040::get_offset_of_characterSelector_4(),
	PracticeTextBasedCharacterSelect_t3916885040::get_offset_of_text_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (ScreenOriSupport_t692417341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[1] = 
{
	ScreenOriSupport_t692417341::get_offset_of_isPortrait_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (OnCharacterSelectionViewClickedEvent_t3002519456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (CharacterSelectionView_t2239851166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[11] = 
{
	CharacterSelectionView_t2239851166::get_offset_of_charracterId_2(),
	CharacterSelectionView_t2239851166::get_offset_of_selectedImage_3(),
	CharacterSelectionView_t2239851166::get_offset_of_normalImage_4(),
	CharacterSelectionView_t2239851166::get_offset_of_textBackGroundImage_5(),
	CharacterSelectionView_t2239851166::get_offset_of_selectedBackgroundColor_6(),
	CharacterSelectionView_t2239851166::get_offset_of_normalBackgroundColor_7(),
	CharacterSelectionView_t2239851166::get_offset_of_selectedTextColor_8(),
	CharacterSelectionView_t2239851166::get_offset_of_normalTextColor_9(),
	CharacterSelectionView_t2239851166::get_offset_of_nameText_10(),
	CharacterSelectionView_t2239851166::get_offset_of__selected_11(),
	CharacterSelectionView_t2239851166::get_offset_of_onViewClicked_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (CharacterSelector_t2041732578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[3] = 
{
	CharacterSelector_t2041732578::get_offset_of_onCharacterChanged_2(),
	CharacterSelector_t2041732578::get_offset_of_characterSelectionViews_3(),
	CharacterSelector_t2041732578::get_offset_of__selectedCharacterId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (CharacterChangeEvent_t2435892010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (ConversationHistoryItemView_t3846761105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[10] = 
{
	ConversationHistoryItemView_t3846761105::get_offset_of_onHistoryItemClicked_2(),
	ConversationHistoryItemView_t3846761105::get_offset_of_data_3(),
	ConversationHistoryItemView_t3846761105::get_offset_of_conversationImg_4(),
	ConversationHistoryItemView_t3846761105::get_offset_of_textEn_5(),
	ConversationHistoryItemView_t3846761105::get_offset_of_textJp_6(),
	ConversationHistoryItemView_t3846761105::get_offset_of_role0Sprite_7(),
	ConversationHistoryItemView_t3846761105::get_offset_of_role1Sprite_8(),
	ConversationHistoryItemView_t3846761105::get_offset_of_role0ColorText_9(),
	ConversationHistoryItemView_t3846761105::get_offset_of_role1ColorText_10(),
	ConversationHistoryItemView_t3846761105::get_offset_of__isPlayerRole_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (ConversationHistoryItemClickedEvent_t3133779843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (OnConversationSceneSelectionViewClickedEvent_t1625773638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (ConversationSceneSelectionView_t407867144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[11] = 
{
	ConversationSceneSelectionView_t407867144::get_offset_of_sceneId_2(),
	ConversationSceneSelectionView_t407867144::get_offset_of_levelText_3(),
	ConversationSceneSelectionView_t407867144::get_offset_of_modeText_4(),
	ConversationSceneSelectionView_t407867144::get_offset_of_subText_5(),
	ConversationSceneSelectionView_t407867144::get_offset_of_backGroundImage_6(),
	ConversationSceneSelectionView_t407867144::get_offset_of_backgroundImageMode0_7(),
	ConversationSceneSelectionView_t407867144::get_offset_of_backgroundImageMode1_8(),
	ConversationSceneSelectionView_t407867144::get_offset_of_backgroundSelected_9(),
	ConversationSceneSelectionView_t407867144::get_offset_of_backgroundDefault_10(),
	ConversationSceneSelectionView_t407867144::get_offset_of__selected_11(),
	ConversationSceneSelectionView_t407867144::get_offset_of_onViewClicked_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (ConversationSceneSelector_t2926116424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[3] = 
{
	ConversationSceneSelector_t2926116424::get_offset_of_selectionViews_2(),
	ConversationSceneSelector_t2926116424::get_offset_of__currentSelectedId_3(),
	ConversationSceneSelector_t2926116424::get_offset_of__currentSelected_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (SelectionButtonSelectedEvent_t3088917173), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (ConversationSelectionButtonView_t265815670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[3] = 
{
	ConversationSelectionButtonView_t265815670::get_offset_of_onButtonSelected_2(),
	ConversationSelectionButtonView_t265815670::get_offset_of__data_3(),
	ConversationSelectionButtonView_t265815670::get_offset_of_buttonIdText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (ConversationSelectionDialog_t2460570659), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (ConversationSelectionDialogNornal_t1475688335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[8] = 
{
	ConversationSelectionDialogNornal_t1475688335::get_offset_of__showSubText_2(),
	ConversationSelectionDialogNornal_t1475688335::get_offset_of__selectionTexts_3(),
	ConversationSelectionDialogNornal_t1475688335::get_offset_of__selectionButtons_4(),
	ConversationSelectionDialogNornal_t1475688335::get_offset_of_selectionTextContainer_5(),
	ConversationSelectionDialogNornal_t1475688335::get_offset_of_selectionButtonContainer_6(),
	ConversationSelectionDialogNornal_t1475688335::get_offset_of_selectionTextPrefab_7(),
	ConversationSelectionDialogNornal_t1475688335::get_offset_of_selectionButtonPrefab_8(),
	ConversationSelectionDialogNornal_t1475688335::get_offset_of__selectedData_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (U3CShowSelectionU3Ec__Iterator0_t3447868340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[5] = 
{
	U3CShowSelectionU3Ec__Iterator0_t3447868340::get_offset_of_gdata_0(),
	U3CShowSelectionU3Ec__Iterator0_t3447868340::get_offset_of_U24this_1(),
	U3CShowSelectionU3Ec__Iterator0_t3447868340::get_offset_of_U24current_2(),
	U3CShowSelectionU3Ec__Iterator0_t3447868340::get_offset_of_U24disposing_3(),
	U3CShowSelectionU3Ec__Iterator0_t3447868340::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (ConversationSelectionDialogVR_t87316163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[4] = 
{
	ConversationSelectionDialogVR_t87316163::get_offset_of__selectionButtons_2(),
	ConversationSelectionDialogVR_t87316163::get_offset_of__selectedData_3(),
	ConversationSelectionDialogVR_t87316163::get_offset_of_selectionButtonContainer_4(),
	ConversationSelectionDialogVR_t87316163::get_offset_of_selectionButtonPrefab_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (U3CShowSelectionU3Ec__Iterator0_t892918678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[5] = 
{
	U3CShowSelectionU3Ec__Iterator0_t892918678::get_offset_of_gdata_0(),
	U3CShowSelectionU3Ec__Iterator0_t892918678::get_offset_of_U24this_1(),
	U3CShowSelectionU3Ec__Iterator0_t892918678::get_offset_of_U24current_2(),
	U3CShowSelectionU3Ec__Iterator0_t892918678::get_offset_of_U24disposing_3(),
	U3CShowSelectionU3Ec__Iterator0_t892918678::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (ConversationSelectionTextView_t4281204247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[6] = 
{
	0,
	ConversationSelectionTextView_t4281204247::get_offset_of_text_3(),
	ConversationSelectionTextView_t4281204247::get_offset_of_subText_4(),
	ConversationSelectionTextView_t4281204247::get_offset_of__textEN_5(),
	ConversationSelectionTextView_t4281204247::get_offset_of__textJP_6(),
	ConversationSelectionTextView_t4281204247::get_offset_of__showJP_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (OnExcerciseSceneSelectionViewClickedEvent_t3939024814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (ExcerciseSceneSelectionView_t3932267486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2429[9] = 
{
	ExcerciseSceneSelectionView_t3932267486::get_offset_of_sceneId_2(),
	ExcerciseSceneSelectionView_t3932267486::get_offset_of_selectedSprite_3(),
	ExcerciseSceneSelectionView_t3932267486::get_offset_of_normalSprite_4(),
	ExcerciseSceneSelectionView_t3932267486::get_offset_of_levelName_5(),
	ExcerciseSceneSelectionView_t3932267486::get_offset_of_levelCompleted_6(),
	ExcerciseSceneSelectionView_t3932267486::get_offset_of_levelFill_7(),
	ExcerciseSceneSelectionView_t3932267486::get_offset_of__selected_8(),
	ExcerciseSceneSelectionView_t3932267486::get_offset_of__completed_9(),
	ExcerciseSceneSelectionView_t3932267486::get_offset_of_onViewClicked_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (ExcerciseSceneSelector_t1138496382), -1, sizeof(ExcerciseSceneSelector_t1138496382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2430[5] = 
{
	ExcerciseSceneSelector_t1138496382::get_offset_of_globalConfig_2(),
	ExcerciseSceneSelector_t1138496382::get_offset_of_selectionViews_3(),
	ExcerciseSceneSelector_t1138496382::get_offset_of__currentSelectedId_4(),
	ExcerciseSceneSelector_t1138496382::get_offset_of__currentSelected_5(),
	ExcerciseSceneSelector_t1138496382_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (HistoryConversationDialog_t3676407505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[7] = 
{
	HistoryConversationDialog_t3676407505::get_offset_of__showSubText_2(),
	HistoryConversationDialog_t3676407505::get_offset_of_conversationHistoryItemPrefab_3(),
	HistoryConversationDialog_t3676407505::get_offset_of_viewPort_4(),
	HistoryConversationDialog_t3676407505::get_offset_of_pointAnchor_5(),
	HistoryConversationDialog_t3676407505::get_offset_of__historyItems_6(),
	HistoryConversationDialog_t3676407505::get_offset_of_speedLerp_7(),
	HistoryConversationDialog_t3676407505::get_offset_of_timeLerp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (U3CLerpItemU3Ec__Iterator0_t925026203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[13] = 
{
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_item_0(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_startLerpTransform_1(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U3CitemRectU3E__0_2(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U3CoriRectU3E__1_3(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U3CdistanceU3E__2_4(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U3CpassedTimeU3E__3_5(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U3CtrickTargetU3E__4_6(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U3CtrickImgU3E__5_7(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U3CcurrentDistanceU3E__6_8(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U24this_9(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U24current_10(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U24disposing_11(),
	U3CLerpItemU3Ec__Iterator0_t925026203::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (U3CAddConversationSentenceU3Ec__Iterator1_t1274435845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[10] = 
{
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_U3CitemU3E__0_0(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_dialogTransform_1(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_isPlayerRole_2(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_data_3(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_textEn_4(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_textJp_5(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_U24this_6(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_U24current_7(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_U24disposing_8(),
	U3CAddConversationSentenceU3Ec__Iterator1_t1274435845::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (U3CRemoveHistoryItemU3Ec__AnonStorey3_t3256731473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[1] = 
{
	U3CRemoveHistoryItemU3Ec__AnonStorey3_t3256731473::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[10] = 
{
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U3CspeedLerpU3E__0_0(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U3CrectTransformU3E__1_1(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U3CtargetAnchoredPositionU3E__2_2(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U3CstartAnchoredPositionU3E__3_3(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U3CtimeCalculateU3E__4_4(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U3CpassedTimeU3E__5_5(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U24this_6(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U24current_7(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U24disposing_8(),
	U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (IconSoundAnimation_t302426630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[4] = 
{
	IconSoundAnimation_t302426630::get_offset_of_autoStart_2(),
	IconSoundAnimation_t302426630::get_offset_of__currentAnimationCoroutine_3(),
	IconSoundAnimation_t302426630::get_offset_of__targetImage_4(),
	IconSoundAnimation_t302426630::get_offset_of_animationSprites_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (U3CDoAnimationU3Ec__Iterator0_t3928869029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[5] = 
{
	U3CDoAnimationU3Ec__Iterator0_t3928869029::get_offset_of_U3CcurrentU3E__0_0(),
	U3CDoAnimationU3Ec__Iterator0_t3928869029::get_offset_of_U24this_1(),
	U3CDoAnimationU3Ec__Iterator0_t3928869029::get_offset_of_U24current_2(),
	U3CDoAnimationU3Ec__Iterator0_t3928869029::get_offset_of_U24disposing_3(),
	U3CDoAnimationU3Ec__Iterator0_t3928869029::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (OtherThinkingView_t677091083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[1] = 
{
	OtherThinkingView_t677091083::get_offset_of_thinkingImg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (PlayOtherTalkDialog_t78515058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[3] = 
{
	PlayOtherTalkDialog_t78515058::get_offset_of__showSubText_2(),
	PlayOtherTalkDialog_t78515058::get_offset_of_enTextEffect_3(),
	PlayOtherTalkDialog_t78515058::get_offset_of_jpTextEffect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[7] = 
{
	U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481::get_offset_of_pathName_0(),
	U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481::get_offset_of_talkData_1(),
	U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481::get_offset_of_callback_2(),
	U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481::get_offset_of_U24this_3(),
	U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481::get_offset_of_U24current_4(),
	U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481::get_offset_of_U24disposing_5(),
	U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (U3CPlayU3Ec__Iterator1_t3847971905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[9] = 
{
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_pathName_0(),
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_U3CclipU3E__0_1(),
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_callback_2(),
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_talkData_3(),
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_U3CkaraokeTimeU3E__1_4(),
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_U24this_5(),
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_U24current_6(),
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_U24disposing_7(),
	U3CPlayU3Ec__Iterator1_t3847971905::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (U3CPlayKaraOkeEffectU3Ec__Iterator2_t3334740973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[5] = 
{
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3334740973::get_offset_of_karaokeTextEffect_0(),
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3334740973::get_offset_of_time_1(),
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3334740973::get_offset_of_U24current_2(),
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3334740973::get_offset_of_U24disposing_3(),
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3334740973::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (ReplayTalkDialog_t3732871695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[4] = 
{
	ReplayTalkDialog_t3732871695::get_offset_of__showSubText_2(),
	ReplayTalkDialog_t3732871695::get_offset_of_soundRecorder_3(),
	ReplayTalkDialog_t3732871695::get_offset_of_enTextEffect_4(),
	ReplayTalkDialog_t3732871695::get_offset_of_jpTextEffect_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (U3CPlayU3Ec__Iterator0_t4017322827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[12] = 
{
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_clip_0(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_callback_1(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_resourcePathName_2(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_U3CresourceClipU3E__0_3(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_talkData_4(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_U3CkaraokeTimeU3E__1_5(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_U3Cco0U3E__2_6(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_U3Cco1U3E__3_7(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_U24this_8(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_U24current_9(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_U24disposing_10(),
	U3CPlayU3Ec__Iterator0_t4017322827::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (U3CFakePlayU3Ec__Iterator1_t3881532539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[9] = 
{
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_pathName_0(),
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_U3CclipU3E__0_1(),
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_callback_2(),
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_talkData_3(),
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_U3CkaraokeTimeU3E__1_4(),
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_U24this_5(),
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_U24current_6(),
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_U24disposing_7(),
	U3CFakePlayU3Ec__Iterator1_t3881532539::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[5] = 
{
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290::get_offset_of_karaokeTextEffect_0(),
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290::get_offset_of_time_1(),
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290::get_offset_of_U24current_2(),
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290::get_offset_of_U24disposing_3(),
	U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (ScrollViewExtendControl_t924978223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[3] = 
{
	ScrollViewExtendControl_t924978223::get_offset_of_defaultMoveTimeout_2(),
	ScrollViewExtendControl_t924978223::get_offset_of_moveSpeed_3(),
	ScrollViewExtendControl_t924978223::get_offset_of_scrollRect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (U3CDoMoveContentU3Ec__Iterator0_t1473839312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[7] = 
{
	U3CDoMoveContentU3Ec__Iterator0_t1473839312::get_offset_of_amount_0(),
	U3CDoMoveContentU3Ec__Iterator0_t1473839312::get_offset_of_U3CdesU3E__0_1(),
	U3CDoMoveContentU3Ec__Iterator0_t1473839312::get_offset_of_U3CmoveTimeoutU3E__1_2(),
	U3CDoMoveContentU3Ec__Iterator0_t1473839312::get_offset_of_U24this_3(),
	U3CDoMoveContentU3Ec__Iterator0_t1473839312::get_offset_of_U24current_4(),
	U3CDoMoveContentU3Ec__Iterator0_t1473839312::get_offset_of_U24disposing_5(),
	U3CDoMoveContentU3Ec__Iterator0_t1473839312::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (TalkDialog_t1412511810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[4] = 
{
	TalkDialog_t1412511810::get_offset_of__showSubText_2(),
	TalkDialog_t1412511810::get_offset_of_textEN_3(),
	TalkDialog_t1412511810::get_offset_of_textJP_4(),
	TalkDialog_t1412511810::get_offset_of_U3CdataU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (TalkDialogWithSoundButton_t1663397415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[1] = 
{
	TalkDialogWithSoundButton_t1663397415::get_offset_of_soundButton_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (VRConversationSelectionButtonView_t1568012346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[2] = 
{
	VRConversationSelectionButtonView_t1568012346::get_offset_of_onButtonSelected_5(),
	VRConversationSelectionButtonView_t1568012346::get_offset_of_infoText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (VoiceRecordingDialog_t3950460413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[3] = 
{
	VoiceRecordingDialog_t3950460413::get_offset_of__showSubText_2(),
	VoiceRecordingDialog_t3950460413::get_offset_of_enTextEffect_3(),
	VoiceRecordingDialog_t3950460413::get_offset_of_jpTextEffect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (U3CPlayU3Ec__Iterator0_t2469087745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[9] = 
{
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_pathName_0(),
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_U3CclipU3E__0_1(),
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_talkData_2(),
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_callback_3(),
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_U3CkaraokeTimeU3E__1_4(),
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_U24this_5(),
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_U24current_6(),
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_U24disposing_7(),
	U3CPlayU3Ec__Iterator0_t2469087745::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (U3CPlayKaraOkeEffectU3Ec__Iterator1_t4221776619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[5] = 
{
	U3CPlayKaraOkeEffectU3Ec__Iterator1_t4221776619::get_offset_of_karaokeTextEffect_0(),
	U3CPlayKaraOkeEffectU3Ec__Iterator1_t4221776619::get_offset_of_time_1(),
	U3CPlayKaraOkeEffectU3Ec__Iterator1_t4221776619::get_offset_of_U24current_2(),
	U3CPlayKaraOkeEffectU3Ec__Iterator1_t4221776619::get_offset_of_U24disposing_3(),
	U3CPlayKaraOkeEffectU3Ec__Iterator1_t4221776619::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (ConversationAutoplayHelper_t1693449450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[1] = 
{
	ConversationAutoplayHelper_t1693449450::get_offset_of_controller_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (CrossSceneParam_t3804493877), -1, sizeof(CrossSceneParam_t3804493877_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2457[2] = 
{
	CrossSceneParam_t3804493877_StaticFields::get_offset_of_autoPlayExercise_0(),
	CrossSceneParam_t3804493877_StaticFields::get_offset_of_autoPlayConversation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (ExerciseAutoplayHelper_t2248779473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2458[1] = 
{
	ExerciseAutoplayHelper_t2248779473::get_offset_of_controller_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (KaraokeTextEffect_t1279919556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[4] = 
{
	KaraokeTextEffect_t1279919556::get_offset_of_colorKaraoke_2(),
	KaraokeTextEffect_t1279919556::get_offset_of__effectText_3(),
	KaraokeTextEffect_t1279919556::get_offset_of_audioSource_4(),
	KaraokeTextEffect_t1279919556::get_offset_of_delayStart_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (U3CPlayEffectU3Ec__Iterator0_t4283951911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2460[11] = 
{
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U3CcontentU3E__0_0(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_time_1(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U3CtimeSpaceU3E__1_2(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U3CtempU3E__2_3(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U3Ctemp2U3E__3_4(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U3CjU3E__4_5(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U3CpassedTimeU3E__5_6(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U24this_7(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U24current_8(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U24disposing_9(),
	U3CPlayEffectU3Ec__Iterator0_t4283951911::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (SoundRecorder_t3431875919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[5] = 
{
	0,
	SoundRecorder_t3431875919::get_offset_of_audioSource_3(),
	SoundRecorder_t3431875919::get_offset_of__preSavedPath_4(),
	SoundRecorder_t3431875919::get_offset_of__recordedClipDictionary_5(),
	SoundRecorder_t3431875919::get_offset_of_recorder_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (ConversationVRSwitcher_t368413640), -1, sizeof(ConversationVRSwitcher_t368413640_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2462[1] = 
{
	ConversationVRSwitcher_t368413640_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (U3CGoToNormalModeU3Ec__AnonStorey0_t3065078691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[1] = 
{
	U3CGoToNormalModeU3Ec__AnonStorey0_t3065078691::get_offset_of_parentPopupQuit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (U3CGoToVRModeU3Ec__AnonStorey1_t3564574473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[2] = 
{
	U3CGoToVRModeU3Ec__AnonStorey1_t3564574473::get_offset_of_parentPopupQuit_0(),
	U3CGoToVRModeU3Ec__AnonStorey1_t3564574473::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (U3CQuitVRModeU3Ec__AnonStorey2_t289205990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[2] = 
{
	U3CQuitVRModeU3Ec__AnonStorey2_t289205990::get_offset_of_parentPopupQuit_0(),
	U3CQuitVRModeU3Ec__AnonStorey2_t289205990::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (ExerciseVRSwitcher_t1425704337), -1, sizeof(ExerciseVRSwitcher_t1425704337_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2466[1] = 
{
	ExerciseVRSwitcher_t1425704337_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (U3CGoToNormalModeU3Ec__AnonStorey0_t2614165850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2467[1] = 
{
	U3CGoToNormalModeU3Ec__AnonStorey0_t2614165850::get_offset_of_parentPopupQuit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (U3CGoToVRModeU3Ec__AnonStorey1_t3426300496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[2] = 
{
	U3CGoToVRModeU3Ec__AnonStorey1_t3426300496::get_offset_of_parentPopupQuit_0(),
	U3CGoToVRModeU3Ec__AnonStorey1_t3426300496::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (U3CQuitVRModeU3Ec__AnonStorey2_t2941453283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[2] = 
{
	U3CQuitVRModeU3Ec__AnonStorey2_t2941453283::get_offset_of_parentPopupQuit_0(),
	U3CQuitVRModeU3Ec__AnonStorey2_t2941453283::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (PracticeVRSwitcher_t654240754), -1, sizeof(PracticeVRSwitcher_t654240754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2470[1] = 
{
	PracticeVRSwitcher_t654240754_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (U3CGoToNormalModeU3Ec__AnonStorey0_t4246182585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[1] = 
{
	U3CGoToNormalModeU3Ec__AnonStorey0_t4246182585::get_offset_of_parentPopupQuit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (U3CGoToVRModeU3Ec__AnonStorey1_t1203783803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[2] = 
{
	U3CGoToVRModeU3Ec__AnonStorey1_t1203783803::get_offset_of_parentPopupQuit_0(),
	U3CGoToVRModeU3Ec__AnonStorey1_t1203783803::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (U3CQuitVRModeU3Ec__AnonStorey2_t1152556888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[2] = 
{
	U3CQuitVRModeU3Ec__AnonStorey2_t1152556888::get_offset_of_parentPopupQuit_0(),
	U3CQuitVRModeU3Ec__AnonStorey2_t1152556888::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (ChapterItem_t2155291334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[8] = 
{
	ChapterItem_t2155291334::get_offset_of_sizeEffectRange_2(),
	0,
	0,
	ChapterItem_t2155291334::get_offset_of_container_5(),
	ChapterItem_t2155291334::get_offset_of_mapLocation_6(),
	ChapterItem_t2155291334::get_offset_of_icon_Lock_7(),
	ChapterItem_t2155291334::get_offset_of_icon_New_8(),
	ChapterItem_t2155291334::get_offset_of_image_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (CachedDragInfo_t1136705792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[2] = 
{
	CachedDragInfo_t1136705792::get_offset_of_time_0(),
	CachedDragInfo_t1136705792::get_offset_of_mousePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (ChapterSelector_t191193704), -1, sizeof(ChapterSelector_t191193704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2476[17] = 
{
	0,
	ChapterSelector_t191193704_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_3(),
	ChapterSelector_t191193704::get_offset_of_widthStep_4(),
	ChapterSelector_t191193704::get_offset_of_selectedItem_5(),
	ChapterSelector_t191193704::get_offset_of_selectItems_6(),
	ChapterSelector_t191193704::get_offset_of_container_7(),
	ChapterSelector_t191193704::get_offset_of__currentCoroutine_8(),
	ChapterSelector_t191193704::get_offset_of_mapCamera_9(),
	0,
	0,
	ChapterSelector_t191193704::get_offset_of__dragDatas_12(),
	ChapterSelector_t191193704::get_offset_of_chapItemOrigin_13(),
	ChapterSelector_t191193704::get_offset_of_homeController_14(),
	ChapterSelector_t191193704::get_offset_of_spriteCommingsoon_15(),
	ChapterSelector_t191193704::get_offset_of_mapLocations_16(),
	ChapterSelector_t191193704::get_offset_of_xOrigin_17(),
	ChapterSelector_t191193704::get_offset_of_isTracked_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (U3CUpdateCacheU3Ec__AnonStorey4_t3729494129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[1] = 
{
	U3CUpdateCacheU3Ec__AnonStorey4_t3729494129::get_offset_of_dragData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (U3ConEnableU3Ec__Iterator0_t2146627610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[5] = 
{
	U3ConEnableU3Ec__Iterator0_t2146627610::get_offset_of_U3CmapLocU3E__0_0(),
	U3ConEnableU3Ec__Iterator0_t2146627610::get_offset_of_U24this_1(),
	U3ConEnableU3Ec__Iterator0_t2146627610::get_offset_of_U24current_2(),
	U3ConEnableU3Ec__Iterator0_t2146627610::get_offset_of_U24disposing_3(),
	U3ConEnableU3Ec__Iterator0_t2146627610::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (U3ConEnableU3Ec__AnonStorey5_t1980318813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[3] = 
{
	U3ConEnableU3Ec__AnonStorey5_t1980318813::get_offset_of_chapItem_0(),
	U3ConEnableU3Ec__AnonStorey5_t1980318813::get_offset_of_realIndex_1(),
	U3ConEnableU3Ec__AnonStorey5_t1980318813::get_offset_of_U3CU3Ef__refU240_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (U3CrefreshItemsStateU3Ec__Iterator1_t4260695815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[4] = 
{
	U3CrefreshItemsStateU3Ec__Iterator1_t4260695815::get_offset_of_U24this_0(),
	U3CrefreshItemsStateU3Ec__Iterator1_t4260695815::get_offset_of_U24current_1(),
	U3CrefreshItemsStateU3Ec__Iterator1_t4260695815::get_offset_of_U24disposing_2(),
	U3CrefreshItemsStateU3Ec__Iterator1_t4260695815::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (U3CjumpToChapterU3Ec__Iterator2_t1172627506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[6] = 
{
	U3CjumpToChapterU3Ec__Iterator2_t1172627506::get_offset_of_chapterId_0(),
	U3CjumpToChapterU3Ec__Iterator2_t1172627506::get_offset_of_U3CtargetU3E__0_1(),
	U3CjumpToChapterU3Ec__Iterator2_t1172627506::get_offset_of_U24this_2(),
	U3CjumpToChapterU3Ec__Iterator2_t1172627506::get_offset_of_U24current_3(),
	U3CjumpToChapterU3Ec__Iterator2_t1172627506::get_offset_of_U24disposing_4(),
	U3CjumpToChapterU3Ec__Iterator2_t1172627506::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (U3CDofocusSelectedU3Ec__Iterator3_t945102547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[7] = 
{
	U3CDofocusSelectedU3Ec__Iterator3_t945102547::get_offset_of_U3CdefaultmovetimeU3E__0_0(),
	U3CDofocusSelectedU3Ec__Iterator3_t945102547::get_offset_of_U3CpassedTimeU3E__1_1(),
	U3CDofocusSelectedU3Ec__Iterator3_t945102547::get_offset_of_U3CxU3E__2_2(),
	U3CDofocusSelectedU3Ec__Iterator3_t945102547::get_offset_of_U24this_3(),
	U3CDofocusSelectedU3Ec__Iterator3_t945102547::get_offset_of_U24current_4(),
	U3CDofocusSelectedU3Ec__Iterator3_t945102547::get_offset_of_U24disposing_5(),
	U3CDofocusSelectedU3Ec__Iterator3_t945102547::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (EditUserNameScript_t1012132347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	EditUserNameScript_t1012132347::get_offset_of_popupEdit_8(),
	EditUserNameScript_t1012132347::get_offset_of_txtUserName_9(),
	EditUserNameScript_t1012132347::get_offset_of_txtID_10(),
	EditUserNameScript_t1012132347::get_offset_of_txtResult_11(),
	EditUserNameScript_t1012132347::get_offset_of_inputField_12(),
	EditUserNameScript_t1012132347::get_offset_of_username_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (HomeCotroller_t2470068823), -1, sizeof(HomeCotroller_t2470068823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2484[26] = 
{
	HomeCotroller_t2470068823::get_offset_of_globalConfig_2(),
	HomeCotroller_t2470068823::get_offset_of_buildSetting_3(),
	HomeCotroller_t2470068823::get_offset_of_doneState_4(),
	HomeCotroller_t2470068823::get_offset_of_prefabTutorial_5(),
	HomeCotroller_t2470068823::get_offset_of_txtUserName_6(),
	HomeCotroller_t2470068823::get_offset_of_speedMoving_7(),
	HomeCotroller_t2470068823::get_offset_of__isMoving_8(),
	HomeCotroller_t2470068823::get_offset_of_imgOverlay_9(),
	HomeCotroller_t2470068823::get_offset_of__desPosOpen_10(),
	HomeCotroller_t2470068823::get_offset_of__desPosClose_11(),
	HomeCotroller_t2470068823::get_offset_of_popupPolicy_12(),
	HomeCotroller_t2470068823::get_offset_of_MenuPanel_13(),
	HomeCotroller_t2470068823::get_offset_of_bg_14(),
	HomeCotroller_t2470068823::get_offset_of_userIdTxt_15(),
	HomeCotroller_t2470068823::get_offset_of_text_Version_16(),
	HomeCotroller_t2470068823::get_offset_of_touchPositions_17(),
	HomeCotroller_t2470068823::get_offset_of_iapUiScript_18(),
	0,
	0,
	HomeCotroller_t2470068823::get_offset_of__startMousePosition_21(),
	HomeCotroller_t2470068823::get_offset_of__dragDatas_22(),
	HomeCotroller_t2470068823::get_offset_of__currentCoroutine_23(),
	HomeCotroller_t2470068823::get_offset_of_target_24(),
	HomeCotroller_t2470068823::get_offset_of__startPosition_25(),
	HomeCotroller_t2470068823_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_26(),
	HomeCotroller_t2470068823_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (State_t4150594815)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2485[3] = 
{
	State_t4150594815::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (U3CUpdateCacheU3Ec__AnonStorey3_t2625163487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[1] = 
{
	U3CUpdateCacheU3Ec__AnonStorey3_t2625163487::get_offset_of_dragData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (U3CMovingCloseU3Ec__Iterator0_t1094654469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[4] = 
{
	U3CMovingCloseU3Ec__Iterator0_t1094654469::get_offset_of_U24this_0(),
	U3CMovingCloseU3Ec__Iterator0_t1094654469::get_offset_of_U24current_1(),
	U3CMovingCloseU3Ec__Iterator0_t1094654469::get_offset_of_U24disposing_2(),
	U3CMovingCloseU3Ec__Iterator0_t1094654469::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (U3CMovingOpenU3Ec__Iterator1_t326153120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[4] = 
{
	U3CMovingOpenU3Ec__Iterator1_t326153120::get_offset_of_U24this_0(),
	U3CMovingOpenU3Ec__Iterator1_t326153120::get_offset_of_U24current_1(),
	U3CMovingOpenU3Ec__Iterator1_t326153120::get_offset_of_U24disposing_2(),
	U3CMovingOpenU3Ec__Iterator1_t326153120::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (U3CMovingU3Ec__Iterator2_t2354585089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[5] = 
{
	U3CMovingU3Ec__Iterator2_t2354585089::get_offset_of_destination_0(),
	U3CMovingU3Ec__Iterator2_t2354585089::get_offset_of_U24this_1(),
	U3CMovingU3Ec__Iterator2_t2354585089::get_offset_of_U24current_2(),
	U3CMovingU3Ec__Iterator2_t2354585089::get_offset_of_U24disposing_3(),
	U3CMovingU3Ec__Iterator2_t2354585089::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (InputCodeScript_t3672565358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[3] = 
{
	InputCodeScript_t3672565358::get_offset_of_popupID_2(),
	InputCodeScript_t3672565358::get_offset_of_inputCode_3(),
	InputCodeScript_t3672565358::get_offset_of_close_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (MenuDragHandler_t1912038821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[4] = 
{
	MenuDragHandler_t1912038821::get_offset_of_homeController_2(),
	MenuDragHandler_t1912038821::get_offset_of_target_3(),
	MenuDragHandler_t1912038821::get_offset_of__startMousePosition_4(),
	MenuDragHandler_t1912038821::get_offset_of__startPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (ResetData_t4114871043), -1, sizeof(ResetData_t4114871043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2492[6] = 
{
	ResetData_t4114871043::get_offset_of_popupResetData_2(),
	ResetData_t4114871043::get_offset_of_popupYesNo_3(),
	ResetData_t4114871043::get_offset_of_popupConfirm_4(),
	ResetData_t4114871043::get_offset_of_popupResetDone_5(),
	ResetData_t4114871043::get_offset_of_U3CisResetDoneU3Ek__BackingField_6(),
	ResetData_t4114871043_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (ClickImmediate_t319099361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[1] = 
{
	ClickImmediate_t319099361::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (IAP_Popup_t2375878299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[5] = 
{
	IAP_Popup_t2375878299::get_offset_of_buttonsGroup_2(),
	IAP_Popup_t2375878299::get_offset_of_buttonBuyFull_3(),
	IAP_Popup_t2375878299::get_offset_of_buttonRestore_4(),
	IAP_Popup_t2375878299::get_offset_of_textPurchased_5(),
	IAP_Popup_t2375878299::get_offset_of_msgRestoreFail_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (IAP_UIScript_t1776166942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[3] = 
{
	IAP_UIScript_t1776166942::get_offset_of_popupIAP_2(),
	IAP_UIScript_t1776166942::get_offset_of_contentScrollViewChapterHome_3(),
	IAP_UIScript_t1776166942::get_offset_of_homeController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[3] = 
{
	U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557::get_offset_of_U24current_0(),
	U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557::get_offset_of_U24disposing_1(),
	U3CWaitFromValidateIAPU3Ec__Iterator0_t2555116557::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (Purchaser_t1674559779), -1, sizeof(Purchaser_t1674559779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2497[9] = 
{
	Purchaser_t1674559779_StaticFields::get_offset_of_PRODUCT_FULL_VERSION_2(),
	Purchaser_t1674559779_StaticFields::get_offset_of_instance_3(),
	Purchaser_t1674559779_StaticFields::get_offset_of__ValidatedProducts_4(),
	Purchaser_t1674559779_StaticFields::get_offset_of_U3CDoneValidatedU3Ek__BackingField_5(),
	Purchaser_t1674559779::get_offset_of_storeController_6(),
	Purchaser_t1674559779::get_offset_of_storeExtensionProvider_7(),
	Purchaser_t1674559779::get_offset_of_validator_8(),
	Purchaser_t1674559779::get_offset_of_sessions_9(),
	Purchaser_t1674559779_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (PurchaseSession_t2088054391)+ sizeof (Il2CppObject), sizeof(PurchaseSession_t2088054391_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2498[3] = 
{
	PurchaseSession_t2088054391::get_offset_of_productId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PurchaseSession_t2088054391::get_offset_of_onSuccess_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PurchaseSession_t2088054391::get_offset_of_onFailure_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (U3CStartU3Ec__Iterator0_t4187929873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[8] = 
{
	U3CStartU3Ec__Iterator0_t4187929873::get_offset_of_U3CappIdentifierU3E__0_0(),
	U3CStartU3Ec__Iterator0_t4187929873::get_offset_of_U3CproductCatalogU3E__1_1(),
	U3CStartU3Ec__Iterator0_t4187929873::get_offset_of_U3CbuilderU3E__2_2(),
	U3CStartU3Ec__Iterator0_t4187929873::get_offset_of_U24locvar0_3(),
	U3CStartU3Ec__Iterator0_t4187929873::get_offset_of_U24this_4(),
	U3CStartU3Ec__Iterator0_t4187929873::get_offset_of_U24current_5(),
	U3CStartU3Ec__Iterator0_t4187929873::get_offset_of_U24disposing_6(),
	U3CStartU3Ec__Iterator0_t4187929873::get_offset_of_U24PC_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
