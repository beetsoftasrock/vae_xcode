﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CommandSheet
struct CommandSheet_t2769384292;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrelearningSetting
struct  PrelearningSetting_t219472927  : public Il2CppObject
{
public:
	// CommandSheet PrelearningSetting::commandSheet
	CommandSheet_t2769384292 * ___commandSheet_0;
	// System.String PrelearningSetting::soundResourcePath
	String_t* ___soundResourcePath_1;

public:
	inline static int32_t get_offset_of_commandSheet_0() { return static_cast<int32_t>(offsetof(PrelearningSetting_t219472927, ___commandSheet_0)); }
	inline CommandSheet_t2769384292 * get_commandSheet_0() const { return ___commandSheet_0; }
	inline CommandSheet_t2769384292 ** get_address_of_commandSheet_0() { return &___commandSheet_0; }
	inline void set_commandSheet_0(CommandSheet_t2769384292 * value)
	{
		___commandSheet_0 = value;
		Il2CppCodeGenWriteBarrier(&___commandSheet_0, value);
	}

	inline static int32_t get_offset_of_soundResourcePath_1() { return static_cast<int32_t>(offsetof(PrelearningSetting_t219472927, ___soundResourcePath_1)); }
	inline String_t* get_soundResourcePath_1() const { return ___soundResourcePath_1; }
	inline String_t** get_address_of_soundResourcePath_1() { return &___soundResourcePath_1; }
	inline void set_soundResourcePath_1(String_t* value)
	{
		___soundResourcePath_1 = value;
		Il2CppCodeGenWriteBarrier(&___soundResourcePath_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
