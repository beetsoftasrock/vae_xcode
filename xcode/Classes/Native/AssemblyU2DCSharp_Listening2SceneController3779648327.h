﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Listening2DataLoader
struct Listening2DataLoader_t3991471186;
// System.Collections.Generic.List`1<Listening2QuestionData>
struct List_1_t1813096251;
// System.Collections.Generic.List`1<Listening2OnplayQuestion>
struct List_1_t3070927206;
// Listening2QuestionView
struct Listening2QuestionView_t3734381824;
// System.String
struct String_t;
// Listening2OnplayQuestion
struct Listening2OnplayQuestion_t3701806074;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening2SceneController
struct  Listening2SceneController_t3779648327  : public MonoBehaviour_t1158329972
{
public:
	// Listening2DataLoader Listening2SceneController::dataLoader
	Listening2DataLoader_t3991471186 * ___dataLoader_2;
	// System.Collections.Generic.List`1<Listening2QuestionData> Listening2SceneController::questionDatas
	List_1_t1813096251 * ___questionDatas_3;
	// System.Collections.Generic.List`1<Listening2OnplayQuestion> Listening2SceneController::onPlayQuestions
	List_1_t3070927206 * ___onPlayQuestions_4;
	// Listening2QuestionView Listening2SceneController::questionView
	Listening2QuestionView_t3734381824 * ___questionView_5;
	// System.String Listening2SceneController::soundPath
	String_t* ___soundPath_6;
	// Listening2OnplayQuestion Listening2SceneController::_currentQuestion
	Listening2OnplayQuestion_t3701806074 * ____currentQuestion_7;
	// UnityEngine.GameObject Listening2SceneController::resultGo
	GameObject_t1756533147 * ___resultGo_8;

public:
	inline static int32_t get_offset_of_dataLoader_2() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327, ___dataLoader_2)); }
	inline Listening2DataLoader_t3991471186 * get_dataLoader_2() const { return ___dataLoader_2; }
	inline Listening2DataLoader_t3991471186 ** get_address_of_dataLoader_2() { return &___dataLoader_2; }
	inline void set_dataLoader_2(Listening2DataLoader_t3991471186 * value)
	{
		___dataLoader_2 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_2, value);
	}

	inline static int32_t get_offset_of_questionDatas_3() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327, ___questionDatas_3)); }
	inline List_1_t1813096251 * get_questionDatas_3() const { return ___questionDatas_3; }
	inline List_1_t1813096251 ** get_address_of_questionDatas_3() { return &___questionDatas_3; }
	inline void set_questionDatas_3(List_1_t1813096251 * value)
	{
		___questionDatas_3 = value;
		Il2CppCodeGenWriteBarrier(&___questionDatas_3, value);
	}

	inline static int32_t get_offset_of_onPlayQuestions_4() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327, ___onPlayQuestions_4)); }
	inline List_1_t3070927206 * get_onPlayQuestions_4() const { return ___onPlayQuestions_4; }
	inline List_1_t3070927206 ** get_address_of_onPlayQuestions_4() { return &___onPlayQuestions_4; }
	inline void set_onPlayQuestions_4(List_1_t3070927206 * value)
	{
		___onPlayQuestions_4 = value;
		Il2CppCodeGenWriteBarrier(&___onPlayQuestions_4, value);
	}

	inline static int32_t get_offset_of_questionView_5() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327, ___questionView_5)); }
	inline Listening2QuestionView_t3734381824 * get_questionView_5() const { return ___questionView_5; }
	inline Listening2QuestionView_t3734381824 ** get_address_of_questionView_5() { return &___questionView_5; }
	inline void set_questionView_5(Listening2QuestionView_t3734381824 * value)
	{
		___questionView_5 = value;
		Il2CppCodeGenWriteBarrier(&___questionView_5, value);
	}

	inline static int32_t get_offset_of_soundPath_6() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327, ___soundPath_6)); }
	inline String_t* get_soundPath_6() const { return ___soundPath_6; }
	inline String_t** get_address_of_soundPath_6() { return &___soundPath_6; }
	inline void set_soundPath_6(String_t* value)
	{
		___soundPath_6 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_6, value);
	}

	inline static int32_t get_offset_of__currentQuestion_7() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327, ____currentQuestion_7)); }
	inline Listening2OnplayQuestion_t3701806074 * get__currentQuestion_7() const { return ____currentQuestion_7; }
	inline Listening2OnplayQuestion_t3701806074 ** get_address_of__currentQuestion_7() { return &____currentQuestion_7; }
	inline void set__currentQuestion_7(Listening2OnplayQuestion_t3701806074 * value)
	{
		____currentQuestion_7 = value;
		Il2CppCodeGenWriteBarrier(&____currentQuestion_7, value);
	}

	inline static int32_t get_offset_of_resultGo_8() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327, ___resultGo_8)); }
	inline GameObject_t1756533147 * get_resultGo_8() const { return ___resultGo_8; }
	inline GameObject_t1756533147 ** get_address_of_resultGo_8() { return &___resultGo_8; }
	inline void set_resultGo_8(GameObject_t1756533147 * value)
	{
		___resultGo_8 = value;
		Il2CppCodeGenWriteBarrier(&___resultGo_8, value);
	}
};

struct Listening2SceneController_t3779648327_StaticFields
{
public:
	// System.Func`1<System.Boolean> Listening2SceneController::<>f__mg$cache0
	Func_1_t1485000104 * ___U3CU3Ef__mgU24cache0_9;
	// UnityEngine.Events.UnityAction Listening2SceneController::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_10;
	// UnityEngine.Events.UnityAction Listening2SceneController::<>f__am$cache1
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache1_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_9() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327_StaticFields, ___U3CU3Ef__mgU24cache0_9)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__mgU24cache0_9() const { return ___U3CU3Ef__mgU24cache0_9; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__mgU24cache0_9() { return &___U3CU3Ef__mgU24cache0_9; }
	inline void set_U3CU3Ef__mgU24cache0_9(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__mgU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_11() { return static_cast<int32_t>(offsetof(Listening2SceneController_t3779648327_StaticFields, ___U3CU3Ef__amU24cache1_11)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache1_11() const { return ___U3CU3Ef__amU24cache1_11; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache1_11() { return &___U3CU3Ef__amU24cache1_11; }
	inline void set_U3CU3Ef__amU24cache1_11(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
