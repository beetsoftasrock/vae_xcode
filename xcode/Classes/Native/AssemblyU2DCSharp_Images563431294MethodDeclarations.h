﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Images
struct Images_t563431294;

#include "codegen/il2cpp-codegen.h"

// System.Void Images::.ctor()
extern "C"  void Images__ctor_m1689480523 (Images_t563431294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
