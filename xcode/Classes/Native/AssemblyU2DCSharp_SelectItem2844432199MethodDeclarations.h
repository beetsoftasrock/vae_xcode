﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectItem
struct SelectItem_t2844432199;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectItem::.ctor()
extern "C"  void SelectItem__ctor_m1677739706 (SelectItem_t2844432199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectItem::Start()
extern "C"  void SelectItem_Start_m3115163334 (SelectItem_t2844432199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectItem::Flip()
extern "C"  void SelectItem_Flip_m149524241 (SelectItem_t2844432199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectItem::OnSelect()
extern "C"  void SelectItem_OnSelect_m3477799347 (SelectItem_t2844432199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
