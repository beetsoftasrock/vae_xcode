﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupScript
struct PopupScript_t535420507;
// UnityEngine.Canvas
struct Canvas_t209405766;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"

// System.Void PopupScript::.ctor()
extern "C"  void PopupScript__ctor_m858638256 (PopupScript_t535420507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScript::Start()
extern "C"  void PopupScript_Start_m14884236 (PopupScript_t535420507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScript::Open(UnityEngine.Canvas)
extern "C"  void PopupScript_Open_m3674867813 (PopupScript_t535420507 * __this, Canvas_t209405766 * ___canvas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScript::Close()
extern "C"  void PopupScript_Close_m502847676 (PopupScript_t535420507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PopupScript::RunPopupDestroy()
extern "C"  Il2CppObject * PopupScript_RunPopupDestroy_m990274141 (PopupScript_t535420507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScript::AddBackground()
extern "C"  void PopupScript_AddBackground_m1463753617 (PopupScript_t535420507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
