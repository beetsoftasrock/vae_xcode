﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultStatisticParam
struct ResultStatisticParam_t3727789074;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultStatisticParam::.ctor()
extern "C"  void ResultStatisticParam__ctor_m2582245751 (ResultStatisticParam_t3727789074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
