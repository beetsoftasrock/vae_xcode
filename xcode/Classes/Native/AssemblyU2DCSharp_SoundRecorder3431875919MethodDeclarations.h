﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundRecorder
struct SoundRecorder_t3431875919;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"

// System.Void SoundRecorder::.ctor()
extern "C"  void SoundRecorder__ctor_m2247923804 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::Awake()
extern "C"  void SoundRecorder_Awake_m2458145035 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::OnDestroy()
extern "C"  void SoundRecorder_OnDestroy_m2783817553 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::StartRecording()
extern "C"  void SoundRecorder_StartRecording_m3808052407 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::StopRecording()
extern "C"  void SoundRecorder_StopRecording_m3609573031 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::Clear()
extern "C"  void SoundRecorder_Clear_m2472129263 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::SaveRecording(System.String,System.String,System.Single)
extern "C"  void SoundRecorder_SaveRecording_m2992426559 (SoundRecorder_t3431875919 * __this, String_t* ___savePath0, String_t* ___name1, float ___recordTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SoundRecorder::SaveRecordedsToFile()
extern "C"  List_1_t1398341365 * SoundRecorder_SaveRecordedsToFile_m1208395814 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SoundRecorder::GetSoundNames()
extern "C"  List_1_t1398341365 * SoundRecorder_GetSoundNames_m3552931606 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::Reset()
extern "C"  void SoundRecorder_Reset_m4005093303 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::ResetRecord()
extern "C"  void SoundRecorder_ResetRecord_m1234930282 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::PlayRecorded(System.String,System.String)
extern "C"  void SoundRecorder_PlayRecorded_m2716683534 (SoundRecorder_t3431875919 * __this, String_t* ___savePath0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip SoundRecorder::GetRecordedClip(System.String,System.String)
extern "C"  AudioClip_t1932558630 * SoundRecorder_GetRecordedClip_m3856580802 (SoundRecorder_t3431875919 * __this, String_t* ___savePath0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::PlayRecorded()
extern "C"  void SoundRecorder_PlayRecorded_m1174010232 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::Start()
extern "C"  void SoundRecorder_Start_m1455633856 (SoundRecorder_t3431875919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::OnInit(System.Boolean)
extern "C"  void SoundRecorder_OnInit_m1540177356 (SoundRecorder_t3431875919 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::OnError(System.String)
extern "C"  void SoundRecorder_OnError_m1382046257 (SoundRecorder_t3431875919 * __this, String_t* ___errMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::OnRecordingFinish(UnityEngine.AudioClip)
extern "C"  void SoundRecorder_OnRecordingFinish_m1295317688 (SoundRecorder_t3431875919 * __this, AudioClip_t1932558630 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundRecorder::OnRecordingSaved(System.String)
extern "C"  void SoundRecorder_OnRecordingSaved_m1510778409 (SoundRecorder_t3431875919 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip SoundRecorder::TrimSilence(UnityEngine.AudioClip,System.Single)
extern "C"  AudioClip_t1932558630 * SoundRecorder_TrimSilence_m3589269353 (SoundRecorder_t3431875919 * __this, AudioClip_t1932558630 * ___clip0, float ___min1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip SoundRecorder::TrimSilence(UnityEngine.AudioClip,System.Single,System.Single)
extern "C"  AudioClip_t1932558630 * SoundRecorder_TrimSilence_m1306628126 (SoundRecorder_t3431875919 * __this, AudioClip_t1932558630 * ___clip0, float ___min1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip SoundRecorder::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32)
extern "C"  AudioClip_t1932558630 * SoundRecorder_TrimSilence_m3632576725 (SoundRecorder_t3431875919 * __this, List_1_t1445631064 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip SoundRecorder::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32,System.Boolean)
extern "C"  AudioClip_t1932558630 * SoundRecorder_TrimSilence_m2831035792 (SoundRecorder_t3431875919 * __this, List_1_t1445631064 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, bool ___stream4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
