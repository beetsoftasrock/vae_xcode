﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveContent
struct MoveContent_t4217566478;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void MoveContent::.ctor()
extern "C"  void MoveContent__ctor_m3721026203 (MoveContent_t4217566478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveContent::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MoveContent_OnPointerEnter_m2235384969 (MoveContent_t4217566478 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveContent::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MoveContent_OnPointerExit_m3200494669 (MoveContent_t4217566478 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveContent::Update()
extern "C"  void MoveContent_Update_m1875822552 (MoveContent_t4217566478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveContent::MoveHorizontalContentPane(System.Single)
extern "C"  void MoveContent_MoveHorizontalContentPane_m2023893294 (MoveContent_t4217566478 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveContent::MoveVerticalContentPane(System.Single)
extern "C"  void MoveContent_MoveVerticalContentPane_m3074835872 (MoveContent_t4217566478 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
