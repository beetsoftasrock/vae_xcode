﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_UriTypeConverter3912970448.h"
#include "System_System_Net_Security_LocalCertificateSelecti3696771181.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "System_System_Net_BindIPEndPoint635820671.h"
#include "System_System_Net_HttpContinueDelegate2713047268.h"
#include "System_System_Text_RegularExpressions_MatchEvaluato710107290.h"
#include "System_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1703410334.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayT116038554.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3672778802.h"
#include "System_Core_U3CModuleU3E3783534214.h"
#include "System_Core_System_Runtime_CompilerServices_Extens1840441203.h"
#include "System_Core_Locale4255929014.h"
#include "System_Core_System_MonoTODOAttribute3487514019.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder3965881084.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr1394030013.h"
#include "System_Core_System_Linq_Check578192424.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Security_Cryptography_Aes2354947465.h"
#include "System_Core_System_Security_Cryptography_AesManage3721278648.h"
#include "System_Core_System_Security_Cryptography_AesTransf3733702461.h"
#include "System_Core_System_Action3226471752.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242844921915.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A116038562.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242038352954.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242672183894.h"
#include "Purchasing_Common_U3CModuleU3E3783534214.h"
#include "Purchasing_Common_MiniJSON_Json4020371770.h"
#include "Purchasing_Common_MiniJSON_Json_Parser1915358011.h"
#include "Purchasing_Common_MiniJSON_Json_Parser_TOKEN2182318091.h"
#include "Purchasing_Common_MiniJSON_Json_Serializer4088787656.h"
#include "Purchasing_Common_MiniJson1660571557.h"
#include "Purchasing_Common_UnityEngine_Purchasing_UnityPurc2635187846.h"
#include "UnityEngine_U3CModuleU3E3783534214.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583.h"
#include "UnityEngine_UnityEngine_jvalue3412352577.h"
#include "UnityEngine_UnityEngine_AndroidJNIHelper3746577466.h"
#include "UnityEngine_UnityEngine_AndroidJNI362636628.h"
#include "UnityEngine_UnityEngine_Application354826772.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413.h"
#include "UnityEngine_UnityEngine_UserAuthorization3217794812.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1038783543.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2674559435.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754.h"
#include "UnityEngine_UnityEngine_AssetBundleManifest1328741589.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction1786092740.h"
#include "UnityEngine_UnityEngine_WaitWhile3168764446.h"
#include "UnityEngine_UnityEngine_WaitUntil722209015.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_Caching2648050249.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1903422412.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1057617917.h"
#include "UnityEngine_UnityEngine_CullingGroup1091689465.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2480912210.h"
#include "UnityEngine_UnityEngine_CursorLockMode3372615096.h"
#include "UnityEngine_UnityEngine_Cursor873194084.h"
#include "UnityEngine_UnityEngine_DebugLogHandler865810509.h"
#include "UnityEngine_UnityEngine_Debug1368543263.h"
#include "UnityEngine_UnityEngine_Display3666191348.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDel3423469815.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2156144444.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1170095138.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_453887929.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Gizmos2256232573.h"
#include "UnityEngine_UnityEngine_Gradient3600583008.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Skybox2033495038.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArgumen1708329234.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (UriTypeConverter_t3912970448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (LocalCertificateSelectionCallback_t3696771181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (RemoteCertificateValidationCallback_t2756269959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (BindIPEndPoint_t635820671), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (HttpContinueDelegate_t2713047268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (MatchEvaluator_t710107290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305139), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1306[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D2_1(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D3_2(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D4_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (U24ArrayTypeU2416_t1703410336)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410336 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (U24ArrayTypeU24128_t116038555)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t116038555 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (U24ArrayTypeU2412_t3672778806)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778806 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (U3CModuleU3E_t3783534217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (ExtensionAttribute_t1840441203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (Locale_t4255929017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (MonoTODOAttribute_t3487514021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (KeyBuilder_t3965881086), -1, sizeof(KeyBuilder_t3965881086_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1314[1] = 
{
	KeyBuilder_t3965881086_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (SymmetricTransform_t1394030014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1315[12] = 
{
	SymmetricTransform_t1394030014::get_offset_of_algo_0(),
	SymmetricTransform_t1394030014::get_offset_of_encrypt_1(),
	SymmetricTransform_t1394030014::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t1394030014::get_offset_of_temp_3(),
	SymmetricTransform_t1394030014::get_offset_of_temp2_4(),
	SymmetricTransform_t1394030014::get_offset_of_workBuff_5(),
	SymmetricTransform_t1394030014::get_offset_of_workout_6(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t1394030014::get_offset_of_m_disposed_9(),
	SymmetricTransform_t1394030014::get_offset_of_lastBlock_10(),
	SymmetricTransform_t1394030014::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1316[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1317[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1318[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1319[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (Check_t578192424), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (Enumerable_t2148412300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1322[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1323[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1324[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1325[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1326[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1327[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (Aes_t2354947465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (AesManaged_t3721278648), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (AesTransform_t3733702461), -1, sizeof(AesTransform_t3733702461_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1330[14] = 
{
	AesTransform_t3733702461::get_offset_of_expandedKey_12(),
	AesTransform_t3733702461::get_offset_of_Nk_13(),
	AesTransform_t3733702461::get_offset_of_Nr_14(),
	AesTransform_t3733702461_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t3733702461_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T0_18(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T1_19(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T2_20(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T3_21(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (Action_t3226471752), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1337[12] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (U24ArrayTypeU24136_t2844921916)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t2844921916 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (U24ArrayTypeU24120_t116038563)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t116038563 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (U24ArrayTypeU24256_t2038352956)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352956 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (U24ArrayTypeU241024_t2672183895)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t2672183895 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (Json_t4020371770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (Parser_t1915358011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1344[1] = 
{
	Parser_t1915358011::get_offset_of_json_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (TOKEN_t2182318091)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1345[13] = 
{
	TOKEN_t2182318091::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (Serializer_t4088787656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1346[1] = 
{
	Serializer_t4088787656::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (MiniJson_t1660571557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (UnityPurchasingCallback_t2635187846), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (AndroidJavaObject_t4251328308), -1, sizeof(AndroidJavaObject_t4251328308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1351[5] = 
{
	AndroidJavaObject_t4251328308_StaticFields::get_offset_of_enableDebugPrints_0(),
	AndroidJavaObject_t4251328308::get_offset_of_m_disposed_1(),
	AndroidJavaObject_t4251328308::get_offset_of_m_jobject_2(),
	AndroidJavaObject_t4251328308::get_offset_of_m_jclass_3(),
	AndroidJavaObject_t4251328308_StaticFields::get_offset_of_s_JavaLangClass_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (AndroidJavaClass_t2973420583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (jvalue_t3412352577)+ sizeof (Il2CppObject), sizeof(jvalue_t3412352577_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1353[9] = 
{
	jvalue_t3412352577::get_offset_of_z_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3412352577::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3412352577::get_offset_of_c_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3412352577::get_offset_of_s_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3412352577::get_offset_of_i_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3412352577::get_offset_of_j_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3412352577::get_offset_of_f_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3412352577::get_offset_of_d_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3412352577::get_offset_of_l_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (AndroidJNIHelper_t3746577466), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (AndroidJNI_t362636628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (Application_t354826772), -1, sizeof(Application_t354826772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1356[2] = 
{
	Application_t354826772_StaticFields::get_offset_of_s_LogCallbackHandler_0(),
	Application_t354826772_StaticFields::get_offset_of_s_LogCallbackHandlerThreaded_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (LogCallback_t1867914413), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (UserAuthorization_t3217794812)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1358[3] = 
{
	UserAuthorization_t3217794812::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (AssetBundleCreateRequest_t1038783543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (AssetBundleRequest_t2674559435), sizeof(AssetBundleRequest_t2674559435_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (AssetBundle_t2054978754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (AssetBundleManifest_t1328741589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (AsyncOperation_t3814632279), sizeof(AsyncOperation_t3814632279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1363[1] = 
{
	AsyncOperation_t3814632279::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (SystemInfo_t2353426895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (WaitForSeconds_t3839502067), sizeof(WaitForSeconds_t3839502067_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1365[1] = 
{
	WaitForSeconds_t3839502067::get_offset_of_m_Seconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (WaitForFixedUpdate_t3968615785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (WaitForEndOfFrame_t1785723201), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (CustomYieldInstruction_t1786092740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (WaitWhile_t3168764446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1369[1] = 
{
	WaitWhile_t3168764446::get_offset_of_m_Predicate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (WaitUntil_t722209015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1370[1] = 
{
	WaitUntil_t722209015::get_offset_of_m_Predicate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (Coroutine_t2299508840), sizeof(Coroutine_t2299508840_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1371[1] = 
{
	Coroutine_t2299508840::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (ScriptableObject_t1975622470), sizeof(ScriptableObject_t1975622470_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (Behaviour_t955675639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (Caching_t2648050249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (Camera_t189460977), -1, sizeof(Camera_t189460977_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1375[3] = 
{
	Camera_t189460977_StaticFields::get_offset_of_onPreCull_2(),
	Camera_t189460977_StaticFields::get_offset_of_onPreRender_3(),
	Camera_t189460977_StaticFields::get_offset_of_onPostRender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (CameraCallback_t834278767), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (Component_t3819376471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (UnhandledExceptionHandler_t1903422412), -1, sizeof(UnhandledExceptionHandler_t1903422412_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1378[1] = 
{
	UnhandledExceptionHandler_t1903422412_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (CullingGroupEvent_t1057617917)+ sizeof (Il2CppObject), sizeof(CullingGroupEvent_t1057617917 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1379[3] = 
{
	CullingGroupEvent_t1057617917::get_offset_of_m_Index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t1057617917::get_offset_of_m_PrevState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t1057617917::get_offset_of_m_ThisState_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (CullingGroup_t1091689465), sizeof(CullingGroup_t1091689465_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1380[2] = 
{
	CullingGroup_t1091689465::get_offset_of_m_Ptr_0(),
	CullingGroup_t1091689465::get_offset_of_m_OnStateChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (StateChanged_t2480912210), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (CursorLockMode_t3372615096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1382[4] = 
{
	CursorLockMode_t3372615096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (Cursor_t873194084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (DebugLogHandler_t865810509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (Debug_t1368543263), -1, sizeof(Debug_t1368543263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1385[1] = 
{
	Debug_t1368543263_StaticFields::get_offset_of_s_Logger_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (Display_t3666191348), -1, sizeof(Display_t3666191348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1386[4] = 
{
	Display_t3666191348::get_offset_of_nativeDisplay_0(),
	Display_t3666191348_StaticFields::get_offset_of_displays_1(),
	Display_t3666191348_StaticFields::get_offset_of__mainDisplay_2(),
	Display_t3666191348_StaticFields::get_offset_of_onDisplaysUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (DisplaysUpdatedDelegate_t3423469815), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (GameCenterPlatform_t2156144444), -1, sizeof(GameCenterPlatform_t2156144444_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1388[7] = 
{
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_AuthenticateCallback_0(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_adCache_1(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_friends_2(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_users_3(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_ResetAchievements_4(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_m_LocalUser_5(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_m_GcBoards_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1389[1] = 
{
	U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (GcLeaderboard_t453887929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1390[2] = 
{
	GcLeaderboard_t453887929::get_offset_of_m_InternalLeaderboard_0(),
	GcLeaderboard_t453887929::get_offset_of_m_GenericLeaderboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (GameObject_t1756533147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (Gizmos_t2256232573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (Gradient_t3600583008), sizeof(Gradient_t3600583008_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1393[1] = 
{
	Gradient_t3600583008::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (QualitySettings_t3238033062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (MeshFilter_t3026937449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (Renderer_t257310565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (Skybox_t2033495038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (LineRenderer_t849157671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (InternalDrawTextureArguments_t1708329234)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1399[10] = 
{
	InternalDrawTextureArguments_t1708329234::get_offset_of_screenRect_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_texture_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_sourceRect_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_leftBorder_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_rightBorder_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_topBorder_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_bottomBorder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_color_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_mat_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t1708329234::get_offset_of_pass_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
