﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeCotroller/<UpdateCache>c__AnonStorey3
struct U3CUpdateCacheU3Ec__AnonStorey3_t2625163487;
// CachedDragInfo
struct CachedDragInfo_t1136705792;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CachedDragInfo1136705792.h"

// System.Void HomeCotroller/<UpdateCache>c__AnonStorey3::.ctor()
extern "C"  void U3CUpdateCacheU3Ec__AnonStorey3__ctor_m2606334186 (U3CUpdateCacheU3Ec__AnonStorey3_t2625163487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HomeCotroller/<UpdateCache>c__AnonStorey3::<>m__0(CachedDragInfo)
extern "C"  bool U3CUpdateCacheU3Ec__AnonStorey3_U3CU3Em__0_m2381936111 (U3CUpdateCacheU3Ec__AnonStorey3_t2625163487 * __this, CachedDragInfo_t1136705792 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
