﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Graph/<LerpChangeDistance>c__Iterator0
struct U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Graph/<LerpChangeDistance>c__Iterator0::.ctor()
extern "C"  void U3CLerpChangeDistanceU3Ec__Iterator0__ctor_m1953774581 (U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Graph/<LerpChangeDistance>c__Iterator0::MoveNext()
extern "C"  bool U3CLerpChangeDistanceU3Ec__Iterator0_MoveNext_m2295865703 (U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Graph/<LerpChangeDistance>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLerpChangeDistanceU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2453794487 (U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Graph/<LerpChangeDistance>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLerpChangeDistanceU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1242677807 (U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Graph/<LerpChangeDistance>c__Iterator0::Dispose()
extern "C"  void U3CLerpChangeDistanceU3Ec__Iterator0_Dispose_m1565343544 (U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Graph/<LerpChangeDistance>c__Iterator0::Reset()
extern "C"  void U3CLerpChangeDistanceU3Ec__Iterator0_Reset_m266184494 (U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
