﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AudioRecorder.Recorder_Instance
struct Recorder_Instance_t330262942;
// AudioRecorder.Recorder/OnInit
struct OnInit_t1674205282;
// AudioRecorder.Recorder/OnFinish
struct OnFinish_t2736449411;
// AudioRecorder.Recorder/OnError
struct OnError_t1220768278;
// AudioRecorder.Recorder/OnSaved
struct OnSaved_t836722345;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRecorder.Recorder
struct  Recorder_t2874686128  : public Il2CppObject
{
public:
	// System.Int32 AudioRecorder.Recorder::minFreq
	int32_t ___minFreq_6;
	// System.Int32 AudioRecorder.Recorder::maxFreq
	int32_t ___maxFreq_7;
	// System.Boolean AudioRecorder.Recorder::isRecorded
	bool ___isRecorded_8;
	// UnityEngine.AudioClip AudioRecorder.Recorder::clip
	AudioClip_t1932558630 * ___clip_9;
	// System.Boolean AudioRecorder.Recorder::isRecording
	bool ___isRecording_10;
	// System.Boolean AudioRecorder.Recorder::isReady
	bool ___isReady_11;

public:
	inline static int32_t get_offset_of_minFreq_6() { return static_cast<int32_t>(offsetof(Recorder_t2874686128, ___minFreq_6)); }
	inline int32_t get_minFreq_6() const { return ___minFreq_6; }
	inline int32_t* get_address_of_minFreq_6() { return &___minFreq_6; }
	inline void set_minFreq_6(int32_t value)
	{
		___minFreq_6 = value;
	}

	inline static int32_t get_offset_of_maxFreq_7() { return static_cast<int32_t>(offsetof(Recorder_t2874686128, ___maxFreq_7)); }
	inline int32_t get_maxFreq_7() const { return ___maxFreq_7; }
	inline int32_t* get_address_of_maxFreq_7() { return &___maxFreq_7; }
	inline void set_maxFreq_7(int32_t value)
	{
		___maxFreq_7 = value;
	}

	inline static int32_t get_offset_of_isRecorded_8() { return static_cast<int32_t>(offsetof(Recorder_t2874686128, ___isRecorded_8)); }
	inline bool get_isRecorded_8() const { return ___isRecorded_8; }
	inline bool* get_address_of_isRecorded_8() { return &___isRecorded_8; }
	inline void set_isRecorded_8(bool value)
	{
		___isRecorded_8 = value;
	}

	inline static int32_t get_offset_of_clip_9() { return static_cast<int32_t>(offsetof(Recorder_t2874686128, ___clip_9)); }
	inline AudioClip_t1932558630 * get_clip_9() const { return ___clip_9; }
	inline AudioClip_t1932558630 ** get_address_of_clip_9() { return &___clip_9; }
	inline void set_clip_9(AudioClip_t1932558630 * value)
	{
		___clip_9 = value;
		Il2CppCodeGenWriteBarrier(&___clip_9, value);
	}

	inline static int32_t get_offset_of_isRecording_10() { return static_cast<int32_t>(offsetof(Recorder_t2874686128, ___isRecording_10)); }
	inline bool get_isRecording_10() const { return ___isRecording_10; }
	inline bool* get_address_of_isRecording_10() { return &___isRecording_10; }
	inline void set_isRecording_10(bool value)
	{
		___isRecording_10 = value;
	}

	inline static int32_t get_offset_of_isReady_11() { return static_cast<int32_t>(offsetof(Recorder_t2874686128, ___isReady_11)); }
	inline bool get_isReady_11() const { return ___isReady_11; }
	inline bool* get_address_of_isReady_11() { return &___isReady_11; }
	inline void set_isReady_11(bool value)
	{
		___isReady_11 = value;
	}
};

struct Recorder_t2874686128_StaticFields
{
public:
	// AudioRecorder.Recorder_Instance AudioRecorder.Recorder::_instance
	Recorder_Instance_t330262942 * ____instance_0;
	// AudioRecorder.Recorder/OnInit AudioRecorder.Recorder::onInitInvoker
	OnInit_t1674205282 * ___onInitInvoker_2;
	// AudioRecorder.Recorder/OnFinish AudioRecorder.Recorder::onFinishInvoker
	OnFinish_t2736449411 * ___onFinishInvoker_3;
	// AudioRecorder.Recorder/OnError AudioRecorder.Recorder::onErrorInvoker
	OnError_t1220768278 * ___onErrorInvoker_4;
	// AudioRecorder.Recorder/OnSaved AudioRecorder.Recorder::onSavedInvoker
	OnSaved_t836722345 * ___onSavedInvoker_5;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(Recorder_t2874686128_StaticFields, ____instance_0)); }
	inline Recorder_Instance_t330262942 * get__instance_0() const { return ____instance_0; }
	inline Recorder_Instance_t330262942 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(Recorder_Instance_t330262942 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}

	inline static int32_t get_offset_of_onInitInvoker_2() { return static_cast<int32_t>(offsetof(Recorder_t2874686128_StaticFields, ___onInitInvoker_2)); }
	inline OnInit_t1674205282 * get_onInitInvoker_2() const { return ___onInitInvoker_2; }
	inline OnInit_t1674205282 ** get_address_of_onInitInvoker_2() { return &___onInitInvoker_2; }
	inline void set_onInitInvoker_2(OnInit_t1674205282 * value)
	{
		___onInitInvoker_2 = value;
		Il2CppCodeGenWriteBarrier(&___onInitInvoker_2, value);
	}

	inline static int32_t get_offset_of_onFinishInvoker_3() { return static_cast<int32_t>(offsetof(Recorder_t2874686128_StaticFields, ___onFinishInvoker_3)); }
	inline OnFinish_t2736449411 * get_onFinishInvoker_3() const { return ___onFinishInvoker_3; }
	inline OnFinish_t2736449411 ** get_address_of_onFinishInvoker_3() { return &___onFinishInvoker_3; }
	inline void set_onFinishInvoker_3(OnFinish_t2736449411 * value)
	{
		___onFinishInvoker_3 = value;
		Il2CppCodeGenWriteBarrier(&___onFinishInvoker_3, value);
	}

	inline static int32_t get_offset_of_onErrorInvoker_4() { return static_cast<int32_t>(offsetof(Recorder_t2874686128_StaticFields, ___onErrorInvoker_4)); }
	inline OnError_t1220768278 * get_onErrorInvoker_4() const { return ___onErrorInvoker_4; }
	inline OnError_t1220768278 ** get_address_of_onErrorInvoker_4() { return &___onErrorInvoker_4; }
	inline void set_onErrorInvoker_4(OnError_t1220768278 * value)
	{
		___onErrorInvoker_4 = value;
		Il2CppCodeGenWriteBarrier(&___onErrorInvoker_4, value);
	}

	inline static int32_t get_offset_of_onSavedInvoker_5() { return static_cast<int32_t>(offsetof(Recorder_t2874686128_StaticFields, ___onSavedInvoker_5)); }
	inline OnSaved_t836722345 * get_onSavedInvoker_5() const { return ___onSavedInvoker_5; }
	inline OnSaved_t836722345 ** get_address_of_onSavedInvoker_5() { return &___onSavedInvoker_5; }
	inline void set_onSavedInvoker_5(OnSaved_t836722345 * value)
	{
		___onSavedInvoker_5 = value;
		Il2CppCodeGenWriteBarrier(&___onSavedInvoker_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
