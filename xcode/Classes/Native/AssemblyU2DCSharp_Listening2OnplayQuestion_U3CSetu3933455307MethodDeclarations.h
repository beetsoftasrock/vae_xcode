﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2OnplayQuestion/<SetupEventHandler>c__AnonStorey0
struct U3CSetupEventHandlerU3Ec__AnonStorey0_t3933455307;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening2OnplayQuestion/<SetupEventHandler>c__AnonStorey0::.ctor()
extern "C"  void U3CSetupEventHandlerU3Ec__AnonStorey0__ctor_m136282290 (U3CSetupEventHandlerU3Ec__AnonStorey0_t3933455307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion/<SetupEventHandler>c__AnonStorey0::<>m__0()
extern "C"  void U3CSetupEventHandlerU3Ec__AnonStorey0_U3CU3Em__0_m3773706677 (U3CSetupEventHandlerU3Ec__AnonStorey0_t3933455307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
