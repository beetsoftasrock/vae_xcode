﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedImageColor
struct SettingBasedImageColor_t3812849895;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedImageColor::.ctor()
extern "C"  void SettingBasedImageColor__ctor_m1979455130 (SettingBasedImageColor_t3812849895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedImageColor::Start()
extern "C"  void SettingBasedImageColor_Start_m608278374 (SettingBasedImageColor_t3812849895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
