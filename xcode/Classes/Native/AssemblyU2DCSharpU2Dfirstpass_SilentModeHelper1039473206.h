﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SilentModeHelper
struct  SilentModeHelper_t1039473206  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct SilentModeHelper_t1039473206_StaticFields
{
public:
	// System.Boolean SilentModeHelper::_isActiveSilent
	bool ____isActiveSilent_2;

public:
	inline static int32_t get_offset_of__isActiveSilent_2() { return static_cast<int32_t>(offsetof(SilentModeHelper_t1039473206_StaticFields, ____isActiveSilent_2)); }
	inline bool get__isActiveSilent_2() const { return ____isActiveSilent_2; }
	inline bool* get_address_of__isActiveSilent_2() { return &____isActiveSilent_2; }
	inline void set__isActiveSilent_2(bool value)
	{
		____isActiveSilent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
