﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationHistoryItemView/ConversationHistoryItemClickedEvent
struct ConversationHistoryItemClickedEvent_t3133779843;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationHistoryItemView/ConversationHistoryItemClickedEvent::.ctor()
extern "C"  void ConversationHistoryItemClickedEvent__ctor_m1278922172 (ConversationHistoryItemClickedEvent_t3133779843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
