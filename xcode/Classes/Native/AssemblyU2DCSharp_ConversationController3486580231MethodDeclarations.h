﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationController
struct ConversationController_t3486580231;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void ConversationController::.ctor()
extern "C"  void ConversationController__ctor_m3493704142 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::Start()
extern "C"  void ConversationController_Start_m356317746 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::OnStart()
extern "C"  void ConversationController_OnStart_m589702019 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::SetSelectedBackgroundViewer()
extern "C"  void ConversationController_SetSelectedBackgroundViewer_m3322233469 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::SetSelectedCharacterViewer()
extern "C"  void ConversationController_SetSelectedCharacterViewer_m360989986 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::SetIsVR(System.Boolean)
extern "C"  void ConversationController_SetIsVR_m2454360381 (ConversationController_t3486580231 * __this, bool ___isVR0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::SetIsChangingMode(System.Boolean)
extern "C"  void ConversationController_SetIsChangingMode_m3435333809 (ConversationController_t3486580231 * __this, bool ___isChangingMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::OnBack()
extern "C"  void ConversationController_OnBack_m3546929642 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::OnBackVR()
extern "C"  void ConversationController_OnBackVR_m3879666626 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::QuiteVRMode(UnityEngine.Transform)
extern "C"  void ConversationController_QuiteVRMode_m2000613888 (ConversationController_t3486580231 * __this, Transform_t3275118058 * ___popupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::YesQuitVRMode()
extern "C"  void ConversationController_YesQuitVRMode_m3138998875 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::NoQuitVRMode(UnityEngine.Transform)
extern "C"  void ConversationController_NoQuitVRMode_m3657330156 (ConversationController_t3486580231 * __this, Transform_t3275118058 * ___popupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::SetActiveChangingMode(System.Boolean)
extern "C"  void ConversationController_SetActiveChangingMode_m1066877483 (Il2CppObject * __this /* static, unused */, bool ___isValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController::<Start>m__0()
extern "C"  void ConversationController_U3CStartU3Em__0_m2100285337 (ConversationController_t3486580231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
