﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultRateController
struct ResultRateController_t2087696941;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResultRateController::.ctor()
extern "C"  void ResultRateController__ctor_m3351888442 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::Awake()
extern "C"  void ResultRateController_Awake_m1466338221 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::Start()
extern "C"  void ResultRateController_Start_m1043873210 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::TestValue()
extern "C"  void ResultRateController_TestValue_m3526934093 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::GetValueFromData(System.String)
extern "C"  void ResultRateController_GetValueFromData_m3424086361 (ResultRateController_t2087696941 * __this, String_t* ___nameLesson0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::SetNameLesson(System.String)
extern "C"  void ResultRateController_SetNameLesson_m4022776579 (ResultRateController_t2087696941 * __this, String_t* ___nameType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::SetNameCharacter(System.String)
extern "C"  void ResultRateController_SetNameCharacter_m630667484 (ResultRateController_t2087696941 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::SetTotalTimeSound(System.Single,System.Single)
extern "C"  void ResultRateController_SetTotalTimeSound_m1026878966 (ResultRateController_t2087696941 * __this, float ___totalTimeRecorded0, float ___totalTimeSoundTemplate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResultRateController::isToday()
extern "C"  bool ResultRateController_isToday_m1286173105 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::SetComment(System.String)
extern "C"  void ResultRateController_SetComment_m500696027 (ResultRateController_t2087696941 * __this, String_t* ___comment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::SetTimeComplete(System.Single,System.String)
extern "C"  void ResultRateController_SetTimeComplete_m1912759157 (ResultRateController_t2087696941 * __this, float ___timeLearnToday0, String_t* ____nameLesson1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::SetFrequencyLearn(System.Int32,System.Int32)
extern "C"  void ResultRateController_SetFrequencyLearn_m257310730 (ResultRateController_t2087696941 * __this, int32_t ___today0, int32_t ___total1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::SetTimeLearn(System.Single,System.Single)
extern "C"  void ResultRateController_SetTimeLearn_m1094128179 (ResultRateController_t2087696941 * __this, float ___today0, float ___total1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::ShowResult()
extern "C"  void ResultRateController_ShowResult_m1896647876 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::ShowValueSlider()
extern "C"  void ResultRateController_ShowValueSlider_m3342931841 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::ShowFrequencyLearn()
extern "C"  void ResultRateController_ShowFrequencyLearn_m1158442675 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateController::ShowTimeLearn()
extern "C"  void ResultRateController_ShowTimeLearn_m1467704324 (ResultRateController_t2087696941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ResultRateController::timeLearnFromHours(System.Single)
extern "C"  String_t* ResultRateController_timeLearnFromHours_m1542634138 (ResultRateController_t2087696941 * __this, float ___timeOfHours0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
