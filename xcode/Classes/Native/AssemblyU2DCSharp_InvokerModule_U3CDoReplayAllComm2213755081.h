﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationCommand
struct ConversationCommand_t3660105836;
// InvokerModule
struct InvokerModule_t4256524346;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2563956642.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvokerModule/<DoReplayAllCommand>c__Iterator1
struct  U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<ConversationCommand> InvokerModule/<DoReplayAllCommand>c__Iterator1::$locvar0
	Enumerator_t2563956642  ___U24locvar0_0;
	// ConversationCommand InvokerModule/<DoReplayAllCommand>c__Iterator1::<command>__0
	ConversationCommand_t3660105836 * ___U3CcommandU3E__0_1;
	// InvokerModule InvokerModule/<DoReplayAllCommand>c__Iterator1::$this
	InvokerModule_t4256524346 * ___U24this_2;
	// System.Object InvokerModule/<DoReplayAllCommand>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean InvokerModule/<DoReplayAllCommand>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 InvokerModule/<DoReplayAllCommand>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081, ___U24locvar0_0)); }
	inline Enumerator_t2563956642  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2563956642 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2563956642  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcommandU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081, ___U3CcommandU3E__0_1)); }
	inline ConversationCommand_t3660105836 * get_U3CcommandU3E__0_1() const { return ___U3CcommandU3E__0_1; }
	inline ConversationCommand_t3660105836 ** get_address_of_U3CcommandU3E__0_1() { return &___U3CcommandU3E__0_1; }
	inline void set_U3CcommandU3E__0_1(ConversationCommand_t3660105836 * value)
	{
		___U3CcommandU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommandU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081, ___U24this_2)); }
	inline InvokerModule_t4256524346 * get_U24this_2() const { return ___U24this_2; }
	inline InvokerModule_t4256524346 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(InvokerModule_t4256524346 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
