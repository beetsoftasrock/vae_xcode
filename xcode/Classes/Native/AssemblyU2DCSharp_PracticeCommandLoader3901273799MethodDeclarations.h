﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PracticeCommandLoader
struct PracticeCommandLoader_t3901273799;

#include "codegen/il2cpp-codegen.h"

// System.Void PracticeCommandLoader::.ctor()
extern "C"  void PracticeCommandLoader__ctor_m4281112368 (PracticeCommandLoader_t3901273799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeCommandLoader::LoadData()
extern "C"  void PracticeCommandLoader_LoadData_m2722206710 (PracticeCommandLoader_t3901273799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
