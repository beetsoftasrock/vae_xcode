﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopController/<unloadBundleData>c__Iterator0
struct U3CunloadBundleDataU3Ec__Iterator0_t676242841;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterTopController/<unloadBundleData>c__Iterator0::.ctor()
extern "C"  void U3CunloadBundleDataU3Ec__Iterator0__ctor_m117504280 (U3CunloadBundleDataU3Ec__Iterator0_t676242841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterTopController/<unloadBundleData>c__Iterator0::MoveNext()
extern "C"  bool U3CunloadBundleDataU3Ec__Iterator0_MoveNext_m1965404380 (U3CunloadBundleDataU3Ec__Iterator0_t676242841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterTopController/<unloadBundleData>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CunloadBundleDataU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2468564170 (U3CunloadBundleDataU3Ec__Iterator0_t676242841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterTopController/<unloadBundleData>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CunloadBundleDataU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2012981106 (U3CunloadBundleDataU3Ec__Iterator0_t676242841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController/<unloadBundleData>c__Iterator0::Dispose()
extern "C"  void U3CunloadBundleDataU3Ec__Iterator0_Dispose_m58248251 (U3CunloadBundleDataU3Ec__Iterator0_t676242841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController/<unloadBundleData>c__Iterator0::Reset()
extern "C"  void U3CunloadBundleDataU3Ec__Iterator0_Reset_m3413014621 (U3CunloadBundleDataU3Ec__Iterator0_t676242841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
