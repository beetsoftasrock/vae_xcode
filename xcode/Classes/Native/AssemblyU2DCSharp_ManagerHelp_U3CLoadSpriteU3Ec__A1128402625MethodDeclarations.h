﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManagerHelp/<LoadSprite>c__AnonStorey0
struct U3CLoadSpriteU3Ec__AnonStorey0_t1128402625;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"

// System.Void ManagerHelp/<LoadSprite>c__AnonStorey0::.ctor()
extern "C"  void U3CLoadSpriteU3Ec__AnonStorey0__ctor_m3983213330 (U3CLoadSpriteU3Ec__AnonStorey0_t1128402625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ManagerHelp/<LoadSprite>c__AnonStorey0::<>m__0(UnityEngine.Sprite)
extern "C"  bool U3CLoadSpriteU3Ec__AnonStorey0_U3CU3Em__0_m3569478261 (U3CLoadSpriteU3Ec__AnonStorey0_t1128402625 * __this, Sprite_t309593783 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
