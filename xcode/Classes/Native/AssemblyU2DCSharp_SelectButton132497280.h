﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectButton
struct  SelectButton_t132497280  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text SelectButton::text
	Text_t356221433 * ___text_2;
	// UnityEngine.Color SelectButton::_colorTextDisable
	Color_t2020392075  ____colorTextDisable_3;
	// UnityEngine.Color SelectButton::_colorTextEnable
	Color_t2020392075  ____colorTextEnable_4;
	// UnityEngine.UI.Image SelectButton::image
	Image_t2042527209 * ___image_5;
	// UnityEngine.Color SelectButton::_colorImageDisable
	Color_t2020392075  ____colorImageDisable_6;
	// UnityEngine.Color SelectButton::_colorImageEnable
	Color_t2020392075  ____colorImageEnable_7;
	// UnityEngine.Color SelectButton::_mainColor
	Color_t2020392075  ____mainColor_8;
	// System.Boolean SelectButton::_selected
	bool ____selected_9;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(SelectButton_t132497280, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of__colorTextDisable_3() { return static_cast<int32_t>(offsetof(SelectButton_t132497280, ____colorTextDisable_3)); }
	inline Color_t2020392075  get__colorTextDisable_3() const { return ____colorTextDisable_3; }
	inline Color_t2020392075 * get_address_of__colorTextDisable_3() { return &____colorTextDisable_3; }
	inline void set__colorTextDisable_3(Color_t2020392075  value)
	{
		____colorTextDisable_3 = value;
	}

	inline static int32_t get_offset_of__colorTextEnable_4() { return static_cast<int32_t>(offsetof(SelectButton_t132497280, ____colorTextEnable_4)); }
	inline Color_t2020392075  get__colorTextEnable_4() const { return ____colorTextEnable_4; }
	inline Color_t2020392075 * get_address_of__colorTextEnable_4() { return &____colorTextEnable_4; }
	inline void set__colorTextEnable_4(Color_t2020392075  value)
	{
		____colorTextEnable_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(SelectButton_t132497280, ___image_5)); }
	inline Image_t2042527209 * get_image_5() const { return ___image_5; }
	inline Image_t2042527209 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(Image_t2042527209 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_5, value);
	}

	inline static int32_t get_offset_of__colorImageDisable_6() { return static_cast<int32_t>(offsetof(SelectButton_t132497280, ____colorImageDisable_6)); }
	inline Color_t2020392075  get__colorImageDisable_6() const { return ____colorImageDisable_6; }
	inline Color_t2020392075 * get_address_of__colorImageDisable_6() { return &____colorImageDisable_6; }
	inline void set__colorImageDisable_6(Color_t2020392075  value)
	{
		____colorImageDisable_6 = value;
	}

	inline static int32_t get_offset_of__colorImageEnable_7() { return static_cast<int32_t>(offsetof(SelectButton_t132497280, ____colorImageEnable_7)); }
	inline Color_t2020392075  get__colorImageEnable_7() const { return ____colorImageEnable_7; }
	inline Color_t2020392075 * get_address_of__colorImageEnable_7() { return &____colorImageEnable_7; }
	inline void set__colorImageEnable_7(Color_t2020392075  value)
	{
		____colorImageEnable_7 = value;
	}

	inline static int32_t get_offset_of__mainColor_8() { return static_cast<int32_t>(offsetof(SelectButton_t132497280, ____mainColor_8)); }
	inline Color_t2020392075  get__mainColor_8() const { return ____mainColor_8; }
	inline Color_t2020392075 * get_address_of__mainColor_8() { return &____mainColor_8; }
	inline void set__mainColor_8(Color_t2020392075  value)
	{
		____mainColor_8 = value;
	}

	inline static int32_t get_offset_of__selected_9() { return static_cast<int32_t>(offsetof(SelectButton_t132497280, ____selected_9)); }
	inline bool get__selected_9() const { return ____selected_9; }
	inline bool* get_address_of__selected_9() { return &____selected_9; }
	inline void set__selected_9(bool value)
	{
		____selected_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
