﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening1QuestionData
struct Listening1QuestionData_t3381069024;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Listening1QuestionData::.ctor()
extern "C"  void Listening1QuestionData__ctor_m678784481 (Listening1QuestionData_t3381069024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Listening1QuestionData::get_questionText()
extern "C"  String_t* Listening1QuestionData_get_questionText_m621952220 (Listening1QuestionData_t3381069024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1QuestionData::set_questionText(System.String)
extern "C"  void Listening1QuestionData_set_questionText_m1509843941 (Listening1QuestionData_t3381069024 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Listening1QuestionData::get_questionSound()
extern "C"  String_t* Listening1QuestionData_get_questionSound_m1110862916 (Listening1QuestionData_t3381069024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1QuestionData::set_questionSound(System.String)
extern "C"  void Listening1QuestionData_set_questionSound_m2421558027 (Listening1QuestionData_t3381069024 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
