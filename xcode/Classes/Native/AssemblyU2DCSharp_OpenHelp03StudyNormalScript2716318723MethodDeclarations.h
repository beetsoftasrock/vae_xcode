﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelp03StudyNormalScript
struct OpenHelp03StudyNormalScript_t2716318723;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenHelp03StudyNormalScript::.ctor()
extern "C"  void OpenHelp03StudyNormalScript__ctor_m38429498 (OpenHelp03StudyNormalScript_t2716318723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp03StudyNormalScript::OnEnable()
extern "C"  void OpenHelp03StudyNormalScript_OnEnable_m3913322782 (OpenHelp03StudyNormalScript_t2716318723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OpenHelp03StudyNormalScript::DelayInitOnclick()
extern "C"  Il2CppObject * OpenHelp03StudyNormalScript_DelayInitOnclick_m2379515404 (OpenHelp03StudyNormalScript_t2716318723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
