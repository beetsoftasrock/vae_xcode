﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip>
struct Dictionary_2_t3847337892;
// AudioRecorder.Recorder
struct Recorder_t2874686128;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundRecorder
struct  SoundRecorder_t3431875919  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource SoundRecorder::audioSource
	AudioSource_t1135106623 * ___audioSource_3;
	// System.String SoundRecorder::_preSavedPath
	String_t* ____preSavedPath_4;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip> SoundRecorder::_recordedClipDictionary
	Dictionary_2_t3847337892 * ____recordedClipDictionary_5;
	// AudioRecorder.Recorder SoundRecorder::recorder
	Recorder_t2874686128 * ___recorder_6;

public:
	inline static int32_t get_offset_of_audioSource_3() { return static_cast<int32_t>(offsetof(SoundRecorder_t3431875919, ___audioSource_3)); }
	inline AudioSource_t1135106623 * get_audioSource_3() const { return ___audioSource_3; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_3() { return &___audioSource_3; }
	inline void set_audioSource_3(AudioSource_t1135106623 * value)
	{
		___audioSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_3, value);
	}

	inline static int32_t get_offset_of__preSavedPath_4() { return static_cast<int32_t>(offsetof(SoundRecorder_t3431875919, ____preSavedPath_4)); }
	inline String_t* get__preSavedPath_4() const { return ____preSavedPath_4; }
	inline String_t** get_address_of__preSavedPath_4() { return &____preSavedPath_4; }
	inline void set__preSavedPath_4(String_t* value)
	{
		____preSavedPath_4 = value;
		Il2CppCodeGenWriteBarrier(&____preSavedPath_4, value);
	}

	inline static int32_t get_offset_of__recordedClipDictionary_5() { return static_cast<int32_t>(offsetof(SoundRecorder_t3431875919, ____recordedClipDictionary_5)); }
	inline Dictionary_2_t3847337892 * get__recordedClipDictionary_5() const { return ____recordedClipDictionary_5; }
	inline Dictionary_2_t3847337892 ** get_address_of__recordedClipDictionary_5() { return &____recordedClipDictionary_5; }
	inline void set__recordedClipDictionary_5(Dictionary_2_t3847337892 * value)
	{
		____recordedClipDictionary_5 = value;
		Il2CppCodeGenWriteBarrier(&____recordedClipDictionary_5, value);
	}

	inline static int32_t get_offset_of_recorder_6() { return static_cast<int32_t>(offsetof(SoundRecorder_t3431875919, ___recorder_6)); }
	inline Recorder_t2874686128 * get_recorder_6() const { return ___recorder_6; }
	inline Recorder_t2874686128 ** get_address_of_recorder_6() { return &___recorder_6; }
	inline void set_recorder_6(Recorder_t2874686128 * value)
	{
		___recorder_6 = value;
		Il2CppCodeGenWriteBarrier(&___recorder_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
