﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelpMyPageScript
struct OpenHelpMyPageScript_t1712211883;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenHelpMyPageScript::.ctor()
extern "C"  void OpenHelpMyPageScript__ctor_m3106601096 (OpenHelpMyPageScript_t1712211883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpMyPageScript::Start()
extern "C"  void OpenHelpMyPageScript_Start_m473830220 (OpenHelpMyPageScript_t1712211883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpMyPageScript::OnEnable()
extern "C"  void OpenHelpMyPageScript_OnEnable_m2201082924 (OpenHelpMyPageScript_t1712211883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpMyPageScript::ShowMe()
extern "C"  void OpenHelpMyPageScript_ShowMe_m4092444081 (OpenHelpMyPageScript_t1712211883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpMyPageScript::ResetObjectsBeFront()
extern "C"  void OpenHelpMyPageScript_ResetObjectsBeFront_m1298386679 (OpenHelpMyPageScript_t1712211883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
