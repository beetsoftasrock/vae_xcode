﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalConfig
struct GlobalConfig_t3080413471;
// NewExerciseSceneController
struct NewExerciseSceneController_t2240697240;

#include "AssemblyU2DCSharp_ConversationDataSaver1206770280.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExerciseSceneDataSaver
struct  ExerciseSceneDataSaver_t2652365465  : public ConversationDataSaver_t1206770280
{
public:
	// GlobalConfig ExerciseSceneDataSaver::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_2;
	// NewExerciseSceneController ExerciseSceneDataSaver::exerciseSceneController
	NewExerciseSceneController_t2240697240 * ___exerciseSceneController_3;

public:
	inline static int32_t get_offset_of_globalConfig_2() { return static_cast<int32_t>(offsetof(ExerciseSceneDataSaver_t2652365465, ___globalConfig_2)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_2() const { return ___globalConfig_2; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_2() { return &___globalConfig_2; }
	inline void set_globalConfig_2(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_2 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_2, value);
	}

	inline static int32_t get_offset_of_exerciseSceneController_3() { return static_cast<int32_t>(offsetof(ExerciseSceneDataSaver_t2652365465, ___exerciseSceneController_3)); }
	inline NewExerciseSceneController_t2240697240 * get_exerciseSceneController_3() const { return ___exerciseSceneController_3; }
	inline NewExerciseSceneController_t2240697240 ** get_address_of_exerciseSceneController_3() { return &___exerciseSceneController_3; }
	inline void set_exerciseSceneController_3(NewExerciseSceneController_t2240697240 * value)
	{
		___exerciseSceneController_3 = value;
		Il2CppCodeGenWriteBarrier(&___exerciseSceneController_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
