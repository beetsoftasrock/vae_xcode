﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconSoundAnimation
struct  IconSoundAnimation_t302426630  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean IconSoundAnimation::autoStart
	bool ___autoStart_2;
	// UnityEngine.Coroutine IconSoundAnimation::_currentAnimationCoroutine
	Coroutine_t2299508840 * ____currentAnimationCoroutine_3;
	// UnityEngine.UI.Image IconSoundAnimation::_targetImage
	Image_t2042527209 * ____targetImage_4;
	// UnityEngine.Sprite[] IconSoundAnimation::animationSprites
	SpriteU5BU5D_t3359083662* ___animationSprites_5;

public:
	inline static int32_t get_offset_of_autoStart_2() { return static_cast<int32_t>(offsetof(IconSoundAnimation_t302426630, ___autoStart_2)); }
	inline bool get_autoStart_2() const { return ___autoStart_2; }
	inline bool* get_address_of_autoStart_2() { return &___autoStart_2; }
	inline void set_autoStart_2(bool value)
	{
		___autoStart_2 = value;
	}

	inline static int32_t get_offset_of__currentAnimationCoroutine_3() { return static_cast<int32_t>(offsetof(IconSoundAnimation_t302426630, ____currentAnimationCoroutine_3)); }
	inline Coroutine_t2299508840 * get__currentAnimationCoroutine_3() const { return ____currentAnimationCoroutine_3; }
	inline Coroutine_t2299508840 ** get_address_of__currentAnimationCoroutine_3() { return &____currentAnimationCoroutine_3; }
	inline void set__currentAnimationCoroutine_3(Coroutine_t2299508840 * value)
	{
		____currentAnimationCoroutine_3 = value;
		Il2CppCodeGenWriteBarrier(&____currentAnimationCoroutine_3, value);
	}

	inline static int32_t get_offset_of__targetImage_4() { return static_cast<int32_t>(offsetof(IconSoundAnimation_t302426630, ____targetImage_4)); }
	inline Image_t2042527209 * get__targetImage_4() const { return ____targetImage_4; }
	inline Image_t2042527209 ** get_address_of__targetImage_4() { return &____targetImage_4; }
	inline void set__targetImage_4(Image_t2042527209 * value)
	{
		____targetImage_4 = value;
		Il2CppCodeGenWriteBarrier(&____targetImage_4, value);
	}

	inline static int32_t get_offset_of_animationSprites_5() { return static_cast<int32_t>(offsetof(IconSoundAnimation_t302426630, ___animationSprites_5)); }
	inline SpriteU5BU5D_t3359083662* get_animationSprites_5() const { return ___animationSprites_5; }
	inline SpriteU5BU5D_t3359083662** get_address_of_animationSprites_5() { return &___animationSprites_5; }
	inline void set_animationSprites_5(SpriteU5BU5D_t3359083662* value)
	{
		___animationSprites_5 = value;
		Il2CppCodeGenWriteBarrier(&___animationSprites_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
