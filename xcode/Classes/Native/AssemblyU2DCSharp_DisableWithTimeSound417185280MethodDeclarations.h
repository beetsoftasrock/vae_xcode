﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisableWithTimeSound
struct DisableWithTimeSound_t417185280;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void DisableWithTimeSound::.ctor()
extern "C"  void DisableWithTimeSound__ctor_m709569451 (DisableWithTimeSound_t417185280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeSound::OnEnable()
extern "C"  void DisableWithTimeSound_OnEnable_m2036883199 (DisableWithTimeSound_t417185280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeSound::StartCountdown()
extern "C"  void DisableWithTimeSound_StartCountdown_m3188106440 (DisableWithTimeSound_t417185280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DisableWithTimeSound::PlayAudioAndDisable()
extern "C"  Il2CppObject * DisableWithTimeSound_PlayAudioAndDisable_m2165302416 (DisableWithTimeSound_t417185280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
