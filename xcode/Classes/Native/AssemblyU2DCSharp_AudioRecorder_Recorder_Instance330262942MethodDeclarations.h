﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioRecorder.Recorder_Instance
struct Recorder_Instance_t330262942;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"

// System.Void AudioRecorder.Recorder_Instance::.ctor()
extern "C"  void Recorder_Instance__ctor_m3586875113 (Recorder_Instance_t330262942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AudioRecorder.Recorder_Instance::get_Frequency()
extern "C"  int32_t Recorder_Instance_get_Frequency_m3068576998 (Recorder_Instance_t330262942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder_Instance::set_Frequency(System.Int32)
extern "C"  void Recorder_Instance_set_Frequency_m1432444237 (Recorder_Instance_t330262942 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder_Instance::Start()
extern "C"  void Recorder_Instance_Start_m1257751497 (Recorder_Instance_t330262942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder_Instance::FixedUpdate()
extern "C"  void Recorder_Instance_FixedUpdate_m2035887178 (Recorder_Instance_t330262942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AudioRecorder.Recorder_Instance::playRecordedClip()
extern "C"  Il2CppObject * Recorder_Instance_playRecordedClip_m3638587821 (Recorder_Instance_t330262942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder_Instance::PlayRecordedClip(UnityEngine.AudioClip)
extern "C"  void Recorder_Instance_PlayRecordedClip_m2889516862 (Recorder_Instance_t330262942 * __this, AudioClip_t1932558630 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder_Instance::syncingMicrophone()
extern "C"  void Recorder_Instance_syncingMicrophone_m2050401624 (Recorder_Instance_t330262942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
