﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>
struct Dictionary_2_t111949537;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2801452975.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Exte3093161221.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3008487712_gshared (Enumerator_t2801452975 * __this, Dictionary_2_t111949537 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3008487712(__this, ___host0, method) ((  void (*) (Enumerator_t2801452975 *, Dictionary_2_t111949537 *, const MethodInfo*))Enumerator__ctor_m3008487712_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m864833445_gshared (Enumerator_t2801452975 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m864833445(__this, method) ((  Il2CppObject * (*) (Enumerator_t2801452975 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m864833445_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3589903269_gshared (Enumerator_t2801452975 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3589903269(__this, method) ((  void (*) (Enumerator_t2801452975 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3589903269_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m540389384_gshared (Enumerator_t2801452975 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m540389384(__this, method) ((  void (*) (Enumerator_t2801452975 *, const MethodInfo*))Enumerator_Dispose_m540389384_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2062245181_gshared (Enumerator_t2801452975 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2062245181(__this, method) ((  bool (*) (Enumerator_t2801452975 *, const MethodInfo*))Enumerator_MoveNext_m2062245181_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object>::get_Current()
extern "C"  ExtensionIntPair_t3093161221  Enumerator_get_Current_m1384498119_gshared (Enumerator_t2801452975 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1384498119(__this, method) ((  ExtensionIntPair_t3093161221  (*) (Enumerator_t2801452975 *, const MethodInfo*))Enumerator_get_Current_m1384498119_gshared)(__this, method)
