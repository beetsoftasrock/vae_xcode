﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen1608648320.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationHistoryItemView/ConversationHistoryItemClickedEvent
struct  ConversationHistoryItemClickedEvent_t3133779843  : public UnityEvent_1_t1608648320
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
