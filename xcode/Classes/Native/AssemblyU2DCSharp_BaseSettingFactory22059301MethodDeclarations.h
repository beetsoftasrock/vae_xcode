﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseSettingFactory
struct BaseSettingFactory_t22059301;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseSettingFactory::.ctor()
extern "C"  void BaseSettingFactory__ctor_m905082606 (BaseSettingFactory_t22059301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
