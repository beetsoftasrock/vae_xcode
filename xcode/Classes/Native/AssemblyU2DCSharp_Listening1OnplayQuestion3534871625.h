﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Listening1QuestionView
struct Listening1QuestionView_t2345876901;
// Listening1QuestionData
struct Listening1QuestionData_t3381069024;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3194695850;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Func`2<TrueFalseAnswerSelection,System.Boolean>
struct Func_2_t2599095338;

#include "AssemblyU2DCSharp_BaseOnplayQuestion1717064182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening1OnplayQuestion
struct  Listening1OnplayQuestion_t3534871625  : public BaseOnplayQuestion_t1717064182
{
public:
	// System.String Listening1OnplayQuestion::soundPath
	String_t* ___soundPath_2;
	// Listening1QuestionView Listening1OnplayQuestion::_view
	Listening1QuestionView_t2345876901 * ____view_3;
	// Listening1QuestionData Listening1OnplayQuestion::<questionData>k__BackingField
	Listening1QuestionData_t3381069024 * ___U3CquestionDataU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<System.Boolean> Listening1OnplayQuestion::<currentAnswer>k__BackingField
	List_1_t3194695850 * ___U3CcurrentAnswerU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.Int32> Listening1OnplayQuestion::<currentIndexAnswer>k__BackingField
	List_1_t1440998580 * ___U3CcurrentIndexAnswerU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_soundPath_2() { return static_cast<int32_t>(offsetof(Listening1OnplayQuestion_t3534871625, ___soundPath_2)); }
	inline String_t* get_soundPath_2() const { return ___soundPath_2; }
	inline String_t** get_address_of_soundPath_2() { return &___soundPath_2; }
	inline void set_soundPath_2(String_t* value)
	{
		___soundPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_2, value);
	}

	inline static int32_t get_offset_of__view_3() { return static_cast<int32_t>(offsetof(Listening1OnplayQuestion_t3534871625, ____view_3)); }
	inline Listening1QuestionView_t2345876901 * get__view_3() const { return ____view_3; }
	inline Listening1QuestionView_t2345876901 ** get_address_of__view_3() { return &____view_3; }
	inline void set__view_3(Listening1QuestionView_t2345876901 * value)
	{
		____view_3 = value;
		Il2CppCodeGenWriteBarrier(&____view_3, value);
	}

	inline static int32_t get_offset_of_U3CquestionDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Listening1OnplayQuestion_t3534871625, ___U3CquestionDataU3Ek__BackingField_4)); }
	inline Listening1QuestionData_t3381069024 * get_U3CquestionDataU3Ek__BackingField_4() const { return ___U3CquestionDataU3Ek__BackingField_4; }
	inline Listening1QuestionData_t3381069024 ** get_address_of_U3CquestionDataU3Ek__BackingField_4() { return &___U3CquestionDataU3Ek__BackingField_4; }
	inline void set_U3CquestionDataU3Ek__BackingField_4(Listening1QuestionData_t3381069024 * value)
	{
		___U3CquestionDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionDataU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CcurrentAnswerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Listening1OnplayQuestion_t3534871625, ___U3CcurrentAnswerU3Ek__BackingField_5)); }
	inline List_1_t3194695850 * get_U3CcurrentAnswerU3Ek__BackingField_5() const { return ___U3CcurrentAnswerU3Ek__BackingField_5; }
	inline List_1_t3194695850 ** get_address_of_U3CcurrentAnswerU3Ek__BackingField_5() { return &___U3CcurrentAnswerU3Ek__BackingField_5; }
	inline void set_U3CcurrentAnswerU3Ek__BackingField_5(List_1_t3194695850 * value)
	{
		___U3CcurrentAnswerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentAnswerU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CcurrentIndexAnswerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Listening1OnplayQuestion_t3534871625, ___U3CcurrentIndexAnswerU3Ek__BackingField_6)); }
	inline List_1_t1440998580 * get_U3CcurrentIndexAnswerU3Ek__BackingField_6() const { return ___U3CcurrentIndexAnswerU3Ek__BackingField_6; }
	inline List_1_t1440998580 ** get_address_of_U3CcurrentIndexAnswerU3Ek__BackingField_6() { return &___U3CcurrentIndexAnswerU3Ek__BackingField_6; }
	inline void set_U3CcurrentIndexAnswerU3Ek__BackingField_6(List_1_t1440998580 * value)
	{
		___U3CcurrentIndexAnswerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentIndexAnswerU3Ek__BackingField_6, value);
	}
};

struct Listening1OnplayQuestion_t3534871625_StaticFields
{
public:
	// System.Func`2<TrueFalseAnswerSelection,System.Boolean> Listening1OnplayQuestion::<>f__am$cache0
	Func_2_t2599095338 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(Listening1OnplayQuestion_t3534871625_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t2599095338 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t2599095338 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t2599095338 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
