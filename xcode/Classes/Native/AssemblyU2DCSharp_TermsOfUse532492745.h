﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TermsOfUse
struct  TermsOfUse_t532492745  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform TermsOfUse::parentRect
	RectTransform_t3349966182 * ___parentRect_2;
	// UnityEngine.RectTransform TermsOfUse::rect
	RectTransform_t3349966182 * ___rect_3;
	// System.Single TermsOfUse::additionSpacing
	float ___additionSpacing_4;

public:
	inline static int32_t get_offset_of_parentRect_2() { return static_cast<int32_t>(offsetof(TermsOfUse_t532492745, ___parentRect_2)); }
	inline RectTransform_t3349966182 * get_parentRect_2() const { return ___parentRect_2; }
	inline RectTransform_t3349966182 ** get_address_of_parentRect_2() { return &___parentRect_2; }
	inline void set_parentRect_2(RectTransform_t3349966182 * value)
	{
		___parentRect_2 = value;
		Il2CppCodeGenWriteBarrier(&___parentRect_2, value);
	}

	inline static int32_t get_offset_of_rect_3() { return static_cast<int32_t>(offsetof(TermsOfUse_t532492745, ___rect_3)); }
	inline RectTransform_t3349966182 * get_rect_3() const { return ___rect_3; }
	inline RectTransform_t3349966182 ** get_address_of_rect_3() { return &___rect_3; }
	inline void set_rect_3(RectTransform_t3349966182 * value)
	{
		___rect_3 = value;
		Il2CppCodeGenWriteBarrier(&___rect_3, value);
	}

	inline static int32_t get_offset_of_additionSpacing_4() { return static_cast<int32_t>(offsetof(TermsOfUse_t532492745, ___additionSpacing_4)); }
	inline float get_additionSpacing_4() const { return ___additionSpacing_4; }
	inline float* get_address_of_additionSpacing_4() { return &___additionSpacing_4; }
	inline void set_additionSpacing_4(float value)
	{
		___additionSpacing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
