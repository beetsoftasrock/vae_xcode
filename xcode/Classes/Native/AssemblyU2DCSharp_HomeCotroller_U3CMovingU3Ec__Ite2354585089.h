﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HomeCotroller
struct HomeCotroller_t2470068823;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeCotroller/<Moving>c__Iterator2
struct  U3CMovingU3Ec__Iterator2_t2354585089  : public Il2CppObject
{
public:
	// UnityEngine.Vector2 HomeCotroller/<Moving>c__Iterator2::destination
	Vector2_t2243707579  ___destination_0;
	// HomeCotroller HomeCotroller/<Moving>c__Iterator2::$this
	HomeCotroller_t2470068823 * ___U24this_1;
	// System.Object HomeCotroller/<Moving>c__Iterator2::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean HomeCotroller/<Moving>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 HomeCotroller/<Moving>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_destination_0() { return static_cast<int32_t>(offsetof(U3CMovingU3Ec__Iterator2_t2354585089, ___destination_0)); }
	inline Vector2_t2243707579  get_destination_0() const { return ___destination_0; }
	inline Vector2_t2243707579 * get_address_of_destination_0() { return &___destination_0; }
	inline void set_destination_0(Vector2_t2243707579  value)
	{
		___destination_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CMovingU3Ec__Iterator2_t2354585089, ___U24this_1)); }
	inline HomeCotroller_t2470068823 * get_U24this_1() const { return ___U24this_1; }
	inline HomeCotroller_t2470068823 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HomeCotroller_t2470068823 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CMovingU3Ec__Iterator2_t2354585089, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CMovingU3Ec__Iterator2_t2354585089, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CMovingU3Ec__Iterator2_t2354585089, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
