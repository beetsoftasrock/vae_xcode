﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.Misc.LessionName
struct LessionName_t3189920656;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Beetsoft.VAE.Misc.LessionName::.ctor()
extern "C"  void LessionName__ctor_m3416413617 (LessionName_t3189920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Misc.LessionName::Start()
extern "C"  void LessionName_Start_m4142176617 (LessionName_t3189920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Misc.LessionName::Update()
extern "C"  void LessionName_Update_m2570885126 (LessionName_t3189920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.Misc.LessionName::<Start>m__0(System.String)
extern "C"  bool LessionName_U3CStartU3Em__0_m3465777942 (Il2CppObject * __this /* static, unused */, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
