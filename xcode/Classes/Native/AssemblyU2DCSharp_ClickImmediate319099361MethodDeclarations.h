﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickImmediate
struct ClickImmediate_t319099361;

#include "codegen/il2cpp-codegen.h"

// System.Void ClickImmediate::.ctor()
extern "C"  void ClickImmediate__ctor_m3265686428 (ClickImmediate_t319099361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickImmediate::Start()
extern "C"  void ClickImmediate_Start_m3917717708 (ClickImmediate_t319099361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickImmediate::<Start>m__0()
extern "C"  void ClickImmediate_U3CStartU3Em__0_m4267088307 (ClickImmediate_t319099361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
