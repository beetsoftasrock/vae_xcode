﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// EditorBoard
struct EditorBoard_t2982825677;
// System.String
struct String_t;
// IAPDemo
struct IAPDemo_t823941185;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t1627764765;
// System.Object
struct Il2CppObject;
// UnityEngine.Purchasing.ISamsungAppsExtensions
struct ISamsungAppsExtensions_t3429739537;
// UnityEngine.Purchasing.IMoolahExtension
struct IMoolahExtension_t3195861654;
// UnityEngine.Purchasing.IMicrosoftExtensions
struct IMicrosoftExtensions_t1101930285;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;
// UnityEngine.Purchasing.IMicrosoftConfiguration
struct IMicrosoftConfiguration_t1212838845;
// UnityEngine.Purchasing.IGooglePlayConfiguration
struct IGooglePlayConfiguration_t2615679878;
// UnityEngine.Purchasing.IMoolahConfiguration
struct IMoolahConfiguration_t3241385415;
// UnityEngine.Purchasing.IAmazonConfiguration
struct IAmazonConfiguration_t3016942165;
// UnityEngine.Purchasing.ISamsungAppsConfiguration
struct ISamsungAppsConfiguration_t4066821689;
// UnityEngine.Purchasing.ITizenStoreConfiguration
struct ITizenStoreConfiguration_t2900348728;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>
struct IEnumerable_1_t1495815016;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// IAPDemo/<ProcessPurchase>c__AnonStorey0
struct U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627;
// IOSBoard
struct IOSBoard_t2012373677;
// iPhoneSpeaker
struct iPhoneSpeaker_t1703012646;
// SilentModeHelper
struct SilentModeHelper_t1039473206;
// UniClipboard
struct UniClipboard_t2267012608;
// IBoard
struct IBoard_t1215419429;
// UnityEngine.Analytics.DriveableProperty
struct DriveableProperty_t2641992127;
// System.Collections.Generic.List`1<UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey>
struct List_1_t1989477525;
// UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey
struct FieldWithRemoteSettingsKey_t2620356393;
// UnityEngine.Object
struct Object_t1021602117;
// System.Type
struct Type_t;
// UnityEngine.Analytics.RemoteSettings
struct RemoteSettings_t3121907247;
// UnityEngine.Purchasing.DemoInventory
struct DemoInventory_t2756307745;
// UnityEngine.Purchasing.IAPButton
struct IAPButton_t3077837360;
// UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager
struct IAPButtonStoreManager_t911589174;
// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t4018783659;
// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t2813769101;
// UnityEngine.Purchasing.Security.AppleTangle
struct AppleTangle_t53875121;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Purchasing.Security.GooglePlayTangle
struct GooglePlayTangle_t2749524914;
// ZipUtil
struct ZipUtil_t2998729745;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1500295812.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1500295812MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa2731437128.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa2731437128MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EditorBoard2982825677.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EditorBoard2982825677MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GUIUtility3275770671MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo823941185.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo823941185MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1005487353MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine.Purchasing_ArrayTypes.h"
#include "mscorlib_System_Decimal724701077.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299.h"
#include "mscorlib_System_Action_1_gen1005487353.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CProcessPu1218622627MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3793203716MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CProcessPu1218622627.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "Stores_UnityEngine_Purchasing_RequestPayOutState3537434082.h"
#include "System_Core_System_Action_3_gen3793203716.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit3301441281MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "Stores_UnityEngine_Purchasing_AndroidStore3203055206.h"
#include "Stores_UnityEngine_Purchasing_CloudMoolahMode206291964.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge275936122.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsMode2214306743.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve2203087800.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "Stores_UnityEngine_Purchasing_LoginResultState2459016979.h"
#include "Stores_UnityEngine_Purchasing_FastRegisterError341731807.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2289103034MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_RestoreTransactionID2487303652.h"
#include "mscorlib_System_Action_1_gen2289103034.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen1831019615MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2383075575MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1831019615.h"
#include "System_Core_System_Action_2_gen2383075575.h"
#include "System_Core_System_Action_2_gen2420917363MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2420917363.h"
#include "System_Core_System_Action_3_gen681716397MethodDeclarations.h"
#include "System_Core_System_Action_3_gen681716397.h"
#include "Stores_UnityEngine_Purchasing_ValidateReceiptState4359597.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSBoard2012373677.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IOSBoard2012373677MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iPhoneSpeaker1703012646.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iPhoneSpeaker1703012646MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SilentModeHelper1039473206.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SilentModeHelper1039473206MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniClipboard2267012608.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniClipboard2267012608MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Analytic2641992127.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Analytic2641992127MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1989477525.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Analytic2620356393.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Analytic2620356393MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Analytic3121907247.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Analytic3121907247MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RemoteSettings392466225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1989477525MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2756307745.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2756307745MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3077837360.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3077837360MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin593747732.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin911589174MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin911589174.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen1242037986MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi4018783659.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1414078924MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2813769101.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin593747732MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2446958492MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem977711995MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem977711995.h"
#include "Stores_UnityEngine_Purchasing_StoreID471452324.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2446958492.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1981688166.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1981688166MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi4018783659MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2813769101MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasing53875121.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasing53875121MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato2878230988MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2749524914.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2749524914MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ZipUtil2998729745.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ZipUtil2998729745MethodDeclarations.h"

// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<System.Object>()
extern "C"  Il2CppObject * IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IExtensionProvider_GetExtension_TisIl2CppObject_m925367407(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IAppleExtensions>()
#define IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.ISamsungAppsExtensions>()
#define IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IMoolahExtension>()
#define IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IMicrosoftExtensions>()
#define IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<System.Object>()
extern "C"  Il2CppObject * ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared (ConfigurationBuilder_t1298400415 * __this, const MethodInfo* method);
#define ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IMicrosoftConfiguration>()
#define ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IGooglePlayConfiguration>()
#define ConfigurationBuilder_Configure_TisIGooglePlayConfiguration_t2615679878_m3137646553(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IMoolahConfiguration>()
#define ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IAmazonConfiguration>()
#define ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.ISamsungAppsConfiguration>()
#define ConfigurationBuilder_Configure_TisISamsungAppsConfiguration_t4066821689_m2324730914(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.ITizenStoreConfiguration>()
#define ConfigurationBuilder_Configure_TisITizenStoreConfiguration_t2900348728_m1247129795(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Dropdown>()
#define GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(__this, method) ((  Dropdown_t1985816271 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m3862106414(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m4280536079(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2872111280_m3197608264(__this, method) ((  Button_t2872111280 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EditorBoard::.ctor()
extern "C"  void EditorBoard__ctor_m1092619784 (EditorBoard_t2982825677 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EditorBoard::SetText(System.String)
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern const uint32_t EditorBoard_SetText_m162367667_MetadataUsageId;
extern "C"  void EditorBoard_SetText_m162367667 (EditorBoard_t2982825677 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EditorBoard_SetText_m162367667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		GUIUtility_set_systemCopyBuffer_m2040945785(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String EditorBoard::GetText()
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern const uint32_t EditorBoard_GetText_m1275062826_MetadataUsageId;
extern "C"  String_t* EditorBoard_GetText_m1275062826 (EditorBoard_t2982825677 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EditorBoard_GetText_m1275062826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m396434174(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void IAPDemo::.ctor()
extern "C"  void IAPDemo__ctor_m3267573714 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		__this->set_m_IsLoggedIn_13((bool)0);
		__this->set_m_SelectedItemIndex_14((-1));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1005487353_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t2420267500_il2cpp_TypeInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781_MethodInfo_var;
extern const MethodInfo* IAPDemo_OnDeferred_m801123048_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2354061435_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m288507185_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral820171765;
extern Il2CppCodeGenString* _stringLiteral1220271277;
extern Il2CppCodeGenString* _stringLiteral4277465922;
extern const uint32_t IAPDemo_OnInitialized_m2628601748_MetadataUsageId;
extern "C"  void IAPDemo_OnInitialized_m2628601748 (IAPDemo_t823941185 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnInitialized_m2628601748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	ProductU5BU5D_t728099314* V_1 = NULL;
	int32_t V_2 = 0;
	Decimal_t724701077  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Product_t1203687971 * V_5 = NULL;
	String_t* V_6 = NULL;
	{
		Il2CppObject * L_0 = ___controller0;
		__this->set_m_Controller_2(L_0);
		Il2CppObject * L_1 = ___extensions1;
		NullCheck(L_1);
		Il2CppObject * L_2 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var, L_1);
		__this->set_m_AppleExtensions_3(L_2);
		Il2CppObject * L_3 = ___extensions1;
		NullCheck(L_3);
		Il2CppObject * L_4 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459_MethodInfo_var, L_3);
		__this->set_m_SamsungExtensions_5(L_4);
		Il2CppObject * L_5 = ___extensions1;
		NullCheck(L_5);
		Il2CppObject * L_6 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670_MethodInfo_var, L_5);
		__this->set_m_MoolahExtensions_4(L_6);
		Il2CppObject * L_7 = ___extensions1;
		NullCheck(L_7);
		Il2CppObject * L_8 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781_MethodInfo_var, L_7);
		__this->set_m_MicrosoftExtensions_6(L_8);
		Il2CppObject * L_9 = ___controller0;
		NullCheck(L_9);
		ProductCollection_t3600019299 * L_10 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_10);
		ProductU5BU5D_t728099314* L_11 = ProductCollection_get_all_m3364167965(L_10, /*hidden argument*/NULL);
		IAPDemo_InitUI_m3350388155(__this, (Il2CppObject*)(Il2CppObject*)L_11, /*hidden argument*/NULL);
		Il2CppObject * L_12 = __this->get_m_AppleExtensions_3();
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)IAPDemo_OnDeferred_m801123048_MethodInfo_var);
		Action_1_t1005487353 * L_14 = (Action_1_t1005487353 *)il2cpp_codegen_object_new(Action_1_t1005487353_il2cpp_TypeInfo_var);
		Action_1__ctor_m2354061435(L_14, __this, L_13, /*hidden argument*/Action_1__ctor_m2354061435_MethodInfo_var);
		NullCheck(L_12);
		InterfaceActionInvoker1< Action_1_t1005487353 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_12, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral820171765, /*hidden argument*/NULL);
		Il2CppObject * L_15 = ___controller0;
		NullCheck(L_15);
		ProductCollection_t3600019299 * L_16 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_15);
		NullCheck(L_16);
		ProductU5BU5D_t728099314* L_17 = ProductCollection_get_all_m3364167965(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		V_2 = 0;
		goto IL_0110;
	}

IL_007e:
	{
		ProductU5BU5D_t728099314* L_18 = V_1;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		Product_t1203687971 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_0 = L_21;
		Product_t1203687971 * L_22 = V_0;
		NullCheck(L_22);
		bool L_23 = Product_get_availableToPurchase_m3285924692(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_010b;
		}
	}
	{
		StringU5BU5D_t1642385972* L_24 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		Product_t1203687971 * L_25 = V_0;
		NullCheck(L_25);
		ProductMetadata_t1573242544 * L_26 = Product_get_metadata_m263398044(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = ProductMetadata_get_localizedTitle_m1010599344(L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_27);
		StringU5BU5D_t1642385972* L_28 = L_24;
		Product_t1203687971 * L_29 = V_0;
		NullCheck(L_29);
		ProductMetadata_t1573242544 * L_30 = Product_get_metadata_m263398044(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31 = ProductMetadata_get_localizedDescription_m3665253400(L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_31);
		StringU5BU5D_t1642385972* L_32 = L_28;
		Product_t1203687971 * L_33 = V_0;
		NullCheck(L_33);
		ProductMetadata_t1573242544 * L_34 = Product_get_metadata_m263398044(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		String_t* L_35 = ProductMetadata_get_isoCurrencyCode_m2098971654(L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_35);
		StringU5BU5D_t1642385972* L_36 = L_32;
		Product_t1203687971 * L_37 = V_0;
		NullCheck(L_37);
		ProductMetadata_t1573242544 * L_38 = Product_get_metadata_m263398044(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Decimal_t724701077  L_39 = ProductMetadata_get_localizedPrice_m158316127(L_38, /*hidden argument*/NULL);
		V_3 = L_39;
		String_t* L_40 = Decimal_ToString_m759431975((&V_3), /*hidden argument*/NULL);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_40);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_40);
		StringU5BU5D_t1642385972* L_41 = L_36;
		Product_t1203687971 * L_42 = V_0;
		NullCheck(L_42);
		ProductMetadata_t1573242544 * L_43 = Product_get_metadata_m263398044(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		String_t* L_44 = ProductMetadata_get_localizedPriceString_m2216629954(L_43, /*hidden argument*/NULL);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_44);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_44);
		StringU5BU5D_t1642385972* L_45 = L_41;
		Product_t1203687971 * L_46 = V_0;
		NullCheck(L_46);
		String_t* L_47 = Product_get_transactionID_m2023701007(L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_47);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_47);
		StringU5BU5D_t1642385972* L_48 = L_45;
		Product_t1203687971 * L_49 = V_0;
		NullCheck(L_49);
		String_t* L_50 = Product_get_receipt_m1045158498(L_49, /*hidden argument*/NULL);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_50);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)L_50);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral1220271277, L_48, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
	}

IL_010b:
	{
		int32_t L_52 = V_2;
		V_2 = ((int32_t)((int32_t)L_52+(int32_t)1));
	}

IL_0110:
	{
		int32_t L_53 = V_2;
		ProductU5BU5D_t728099314* L_54 = V_1;
		NullCheck(L_54);
		if ((((int32_t)L_53) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_54)->max_length)))))))
		{
			goto IL_007e;
		}
	}
	{
		Il2CppObject * L_55 = __this->get_m_Controller_2();
		NullCheck(L_55);
		ProductCollection_t3600019299 * L_56 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_55);
		NullCheck(L_56);
		ProductU5BU5D_t728099314* L_57 = ProductCollection_get_all_m3364167965(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_013a;
		}
	}
	{
		__this->set_m_SelectedItemIndex_14(0);
	}

IL_013a:
	{
		V_4 = 0;
		goto IL_01ad;
	}

IL_0142:
	{
		Il2CppObject * L_58 = __this->get_m_Controller_2();
		NullCheck(L_58);
		ProductCollection_t3600019299 * L_59 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_58);
		NullCheck(L_59);
		ProductU5BU5D_t728099314* L_60 = ProductCollection_get_all_m3364167965(L_59, /*hidden argument*/NULL);
		int32_t L_61 = V_4;
		NullCheck(L_60);
		int32_t L_62 = L_61;
		Product_t1203687971 * L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		V_5 = L_63;
		Product_t1203687971 * L_64 = V_5;
		NullCheck(L_64);
		ProductMetadata_t1573242544 * L_65 = Product_get_metadata_m263398044(L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		String_t* L_66 = ProductMetadata_get_localizedTitle_m1010599344(L_65, /*hidden argument*/NULL);
		Product_t1203687971 * L_67 = V_5;
		NullCheck(L_67);
		ProductMetadata_t1573242544 * L_68 = Product_get_metadata_m263398044(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		String_t* L_69 = ProductMetadata_get_localizedPriceString_m2216629954(L_68, /*hidden argument*/NULL);
		Product_t1203687971 * L_70 = V_5;
		NullCheck(L_70);
		ProductMetadata_t1573242544 * L_71 = Product_get_metadata_m263398044(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		Decimal_t724701077  L_72 = ProductMetadata_get_localizedPrice_m158316127(L_71, /*hidden argument*/NULL);
		Decimal_t724701077  L_73 = L_72;
		Il2CppObject * L_74 = Box(Decimal_t724701077_il2cpp_TypeInfo_var, &L_73);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_75 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral4277465922, L_66, L_69, L_74, /*hidden argument*/NULL);
		V_6 = L_75;
		Dropdown_t1985816271 * L_76 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_76);
		List_1_t1789388632 * L_77 = Dropdown_get_options_m2669836220(L_76, /*hidden argument*/NULL);
		int32_t L_78 = V_4;
		String_t* L_79 = V_6;
		OptionData_t2420267500 * L_80 = (OptionData_t2420267500 *)il2cpp_codegen_object_new(OptionData_t2420267500_il2cpp_TypeInfo_var);
		OptionData__ctor_m743450704(L_80, L_79, /*hidden argument*/NULL);
		NullCheck(L_77);
		List_1_set_Item_m288507185(L_77, L_78, L_80, /*hidden argument*/List_1_set_Item_m288507185_MethodInfo_var);
		int32_t L_81 = V_4;
		V_4 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_01ad:
	{
		int32_t L_82 = V_4;
		Il2CppObject * L_83 = __this->get_m_Controller_2();
		NullCheck(L_83);
		ProductCollection_t3600019299 * L_84 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_83);
		NullCheck(L_84);
		ProductU5BU5D_t728099314* L_85 = ProductCollection_get_all_m3364167965(L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		if ((((int32_t)L_82) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_85)->max_length)))))))
		{
			goto IL_0142;
		}
	}
	{
		Dropdown_t1985816271 * L_86 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_86);
		Dropdown_RefreshShownValue_m3113581237(L_86, /*hidden argument*/NULL);
		IAPDemo_UpdateHistoryUI_m2607563357(__this, /*hidden argument*/NULL);
		IAPDemo_LogProductDefinitions_m1239761007(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult IAPDemo::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3793203716_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahExtension_t3195861654_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CProcessPurchaseU3Ec__AnonStorey0_U3CU3Em__0_m4275133472_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1884466958_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1546350049;
extern Il2CppCodeGenString* _stringLiteral1530557140;
extern const uint32_t IAPDemo_ProcessPurchase_m3949646312_MetadataUsageId;
extern "C"  int32_t IAPDemo_ProcessPurchase_m3949646312 (IAPDemo_t823941185 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_ProcessPurchase_m3949646312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_0 = (U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 *)il2cpp_codegen_object_new(U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627_il2cpp_TypeInfo_var);
		U3CProcessPurchaseU3Ec__AnonStorey0__ctor_m357671038(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_1 = V_0;
		PurchaseEventArgs_t547992434 * L_2 = ___e0;
		NullCheck(L_1);
		L_1->set_e_0(L_2);
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_4 = V_0;
		NullCheck(L_4);
		PurchaseEventArgs_t547992434 * L_5 = L_4->get_e_0();
		NullCheck(L_5);
		Product_t1203687971 * L_6 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		ProductDefinition_t1942475268 * L_7 = Product_get_definition_m2035415516(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = ProductDefinition_get_id_m264072292(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1546350049, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_10 = V_0;
		NullCheck(L_10);
		PurchaseEventArgs_t547992434 * L_11 = L_10->get_e_0();
		NullCheck(L_11);
		Product_t1203687971 * L_12 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = Product_get_receipt_m1045158498(L_12, /*hidden argument*/NULL);
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1530557140, L_13, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_15 = V_0;
		NullCheck(L_15);
		PurchaseEventArgs_t547992434 * L_16 = L_15->get_e_0();
		NullCheck(L_16);
		Product_t1203687971 * L_17 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = Product_get_transactionID_m2023701007(L_17, /*hidden argument*/NULL);
		__this->set_m_LastTransationID_10(L_18);
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_19 = V_0;
		NullCheck(L_19);
		PurchaseEventArgs_t547992434 * L_20 = L_19->get_e_0();
		NullCheck(L_20);
		Product_t1203687971 * L_21 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = Product_get_receipt_m1045158498(L_21, /*hidden argument*/NULL);
		__this->set_m_LastReceipt_11(L_22);
		__this->set_m_PurchaseInProgress_15((bool)0);
		bool L_23 = __this->get_m_IsCloudMoolahStoreSelected_9();
		if (!L_23)
		{
			goto IL_00bf;
		}
	}
	{
		Il2CppObject * L_24 = __this->get_m_MoolahExtensions_4();
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_25 = V_0;
		NullCheck(L_25);
		PurchaseEventArgs_t547992434 * L_26 = L_25->get_e_0();
		NullCheck(L_26);
		Product_t1203687971 * L_27 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		String_t* L_28 = Product_get_transactionID_m2023701007(L_27, /*hidden argument*/NULL);
		U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * L_29 = V_0;
		IntPtr_t L_30;
		L_30.set_m_value_0((void*)(void*)U3CProcessPurchaseU3Ec__AnonStorey0_U3CU3Em__0_m4275133472_MethodInfo_var);
		Action_3_t3793203716 * L_31 = (Action_3_t3793203716 *)il2cpp_codegen_object_new(Action_3_t3793203716_il2cpp_TypeInfo_var);
		Action_3__ctor_m1884466958(L_31, L_29, L_30, /*hidden argument*/Action_3__ctor_m1884466958_MethodInfo_var);
		NullCheck(L_24);
		InterfaceActionInvoker2< String_t*, Action_3_t3793203716 * >::Invoke(4 /* System.Void UnityEngine.Purchasing.IMoolahExtension::RequestPayOut(System.String,System.Action`3<System.String,UnityEngine.Purchasing.RequestPayOutState,System.String>) */, IMoolahExtension_t3195861654_il2cpp_TypeInfo_var, L_24, L_28, L_31);
	}

IL_00bf:
	{
		IAPDemo_UpdateHistoryUI_m2607563357(__this, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_00cc;
	}

IL_00cc:
	{
		int32_t L_32 = V_1;
		return L_32;
	}
}
// System.Void IAPDemo::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral830689752;
extern const uint32_t IAPDemo_OnPurchaseFailed_m2053066303_MetadataUsageId;
extern "C"  void IAPDemo_OnPurchaseFailed_m2053066303 (IAPDemo_t823941185 * __this, Product_t1203687971 * ___item0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnPurchaseFailed_m2053066303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___item0;
		NullCheck(L_0);
		ProductDefinition_t1942475268 * L_1 = Product_get_definition_m2035415516(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_id_m264072292(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral830689752, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___r1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &L_5);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_m_PurchaseInProgress_15((bool)0);
		return;
	}
}
// System.Void IAPDemo::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1657966450;
extern Il2CppCodeGenString* _stringLiteral1153205404;
extern Il2CppCodeGenString* _stringLiteral3815334426;
extern Il2CppCodeGenString* _stringLiteral417931313;
extern const uint32_t IAPDemo_OnInitializeFailed_m1037377075_MetadataUsageId;
extern "C"  void IAPDemo_OnInitializeFailed_m1037377075 (IAPDemo_t823941185 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnInitializeFailed_m1037377075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1657966450, /*hidden argument*/NULL);
		int32_t L_0 = ___error0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_1 = ___error0;
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_2 = ___error0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0051;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1153205404, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3815334426, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral417931313, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_0051:
	{
		return;
	}
}
// System.Void IAPDemo::Awake()
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var;
extern Il2CppClass* IMicrosoftConfiguration_t1212838845_il2cpp_TypeInfo_var;
extern Il2CppClass* IGooglePlayConfiguration_t2615679878_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahConfiguration_t3241385415_il2cpp_TypeInfo_var;
extern Il2CppClass* IDs_t3808979560_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* IAmazonConfiguration_t3016942165_il2cpp_TypeInfo_var;
extern Il2CppClass* ISamsungAppsConfiguration_t4066821689_il2cpp_TypeInfo_var;
extern Il2CppClass* ITizenStoreConfiguration_t2900348728_il2cpp_TypeInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIGooglePlayConfiguration_t2615679878_m3137646553_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisISamsungAppsConfiguration_t4066821689_m2324730914_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisITizenStoreConfiguration_t2900348728_m1247129795_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2453939300;
extern Il2CppCodeGenString* _stringLiteral579628896;
extern Il2CppCodeGenString* _stringLiteral3822513892;
extern Il2CppCodeGenString* _stringLiteral1830547213;
extern Il2CppCodeGenString* _stringLiteral2946374258;
extern Il2CppCodeGenString* _stringLiteral2700534657;
extern Il2CppCodeGenString* _stringLiteral473744409;
extern Il2CppCodeGenString* _stringLiteral859071123;
extern Il2CppCodeGenString* _stringLiteral1461038519;
extern Il2CppCodeGenString* _stringLiteral1645711892;
extern Il2CppCodeGenString* _stringLiteral968850961;
extern Il2CppCodeGenString* _stringLiteral862026094;
extern Il2CppCodeGenString* _stringLiteral2396058710;
extern Il2CppCodeGenString* _stringLiteral4189921875;
extern Il2CppCodeGenString* _stringLiteral1070925405;
extern Il2CppCodeGenString* _stringLiteral1346993374;
extern Il2CppCodeGenString* _stringLiteral1233259296;
extern Il2CppCodeGenString* _stringLiteral2315729507;
extern Il2CppCodeGenString* _stringLiteral784127634;
extern Il2CppCodeGenString* _stringLiteral1451147361;
extern const uint32_t IAPDemo_Awake_m1872671099_MetadataUsageId;
extern "C"  void IAPDemo_Awake_m1872671099 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_Awake_m1872671099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StandardPurchasingModule_t4003664591 * V_0 = NULL;
	ConfigurationBuilder_t1298400415 * V_1 = NULL;
	IDs_t3808979560 * V_2 = NULL;
	IAPDemo_t823941185 * G_B2_0 = NULL;
	IAPDemo_t823941185 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	IAPDemo_t823941185 * G_B3_1 = NULL;
	IAPDemo_t823941185 * G_B5_0 = NULL;
	IAPDemo_t823941185 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	IAPDemo_t823941185 * G_B6_1 = NULL;
	IAPDemo_t823941185 * G_B8_0 = NULL;
	IAPDemo_t823941185 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	IAPDemo_t823941185 * G_B9_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_0 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		StandardPurchasingModule_t4003664591 * L_1 = V_0;
		NullCheck(L_1);
		StandardPurchasingModule_set_useFakeStoreUIMode_m3278247934(L_1, 1, /*hidden argument*/NULL);
		StandardPurchasingModule_t4003664591 * L_2 = V_0;
		ConfigurationBuilder_t1298400415 * L_3 = ConfigurationBuilder_Instance_m4243979412(NULL /*static, unused*/, L_2, ((IPurchasingModuleU5BU5D_t4128245854*)SZArrayNew(IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_1 = L_3;
		ConfigurationBuilder_t1298400415 * L_4 = V_1;
		NullCheck(L_4);
		Il2CppObject * L_5 = ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418(L_4, /*hidden argument*/ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418_MethodInfo_var);
		NullCheck(L_5);
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void UnityEngine.Purchasing.IMicrosoftConfiguration::set_useMockBillingSystem(System.Boolean) */, IMicrosoftConfiguration_t1212838845_il2cpp_TypeInfo_var, L_5, (bool)1);
		ConfigurationBuilder_t1298400415 * L_6 = V_1;
		NullCheck(L_6);
		Il2CppObject * L_7 = ConfigurationBuilder_Configure_TisIGooglePlayConfiguration_t2615679878_m3137646553(L_6, /*hidden argument*/ConfigurationBuilder_Configure_TisIGooglePlayConfiguration_t2615679878_m3137646553_MethodInfo_var);
		NullCheck(L_7);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void UnityEngine.Purchasing.IGooglePlayConfiguration::SetPublicKey(System.String) */, IGooglePlayConfiguration_t2615679878_il2cpp_TypeInfo_var, L_7, _stringLiteral2453939300);
		int32_t L_8 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)11)))))
		{
			G_B2_0 = __this;
			goto IL_004f;
		}
	}
	{
		StandardPurchasingModule_t4003664591 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = StandardPurchasingModule_get_androidStore_m1436760510(L_9, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0050;
	}

IL_004f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0050:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_IsGooglePlayStoreSelected_7((bool)G_B3_0);
		ConfigurationBuilder_t1298400415 * L_11 = V_1;
		NullCheck(L_11);
		Il2CppObject * L_12 = ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946(L_11, /*hidden argument*/ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946_MethodInfo_var);
		NullCheck(L_12);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void UnityEngine.Purchasing.IMoolahConfiguration::set_appKey(System.String) */, IMoolahConfiguration_t3241385415_il2cpp_TypeInfo_var, L_12, _stringLiteral579628896);
		ConfigurationBuilder_t1298400415 * L_13 = V_1;
		NullCheck(L_13);
		Il2CppObject * L_14 = ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946(L_13, /*hidden argument*/ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946_MethodInfo_var);
		NullCheck(L_14);
		InterfaceActionInvoker1< String_t* >::Invoke(1 /* System.Void UnityEngine.Purchasing.IMoolahConfiguration::set_hashKey(System.String) */, IMoolahConfiguration_t3241385415_il2cpp_TypeInfo_var, L_14, _stringLiteral3822513892);
		ConfigurationBuilder_t1298400415 * L_15 = V_1;
		NullCheck(L_15);
		Il2CppObject * L_16 = ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946(L_15, /*hidden argument*/ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946_MethodInfo_var);
		NullCheck(L_16);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UnityEngine.Purchasing.IMoolahConfiguration::SetMode(UnityEngine.Purchasing.CloudMoolahMode) */, IMoolahConfiguration_t3241385415_il2cpp_TypeInfo_var, L_16, 1);
		int32_t L_17 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)11)))))
		{
			G_B5_0 = __this;
			goto IL_0099;
		}
	}
	{
		StandardPurchasingModule_t4003664591 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = StandardPurchasingModule_get_androidStore_m1436760510(L_18, /*hidden argument*/NULL);
		G_B6_0 = ((((int32_t)L_19) == ((int32_t)2))? 1 : 0);
		G_B6_1 = G_B4_0;
		goto IL_009a;
	}

IL_0099:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_009a:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_m_IsCloudMoolahStoreSelected_9((bool)G_B6_0);
		ConfigurationBuilder_t1298400415 * L_20 = V_1;
		IDs_t3808979560 * L_21 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_21, /*hidden argument*/NULL);
		V_2 = L_21;
		IDs_t3808979560 * L_22 = V_2;
		StringU5BU5D_t1642385972* L_23 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral2700534657);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_22);
		IDs_Add_m217445704(L_22, _stringLiteral2946374258, L_23, /*hidden argument*/NULL);
		IDs_t3808979560 * L_24 = V_2;
		StringU5BU5D_t1642385972* L_25 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral859071123);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral859071123);
		NullCheck(L_24);
		IDs_Add_m217445704(L_24, _stringLiteral473744409, L_25, /*hidden argument*/NULL);
		IDs_t3808979560 * L_26 = V_2;
		StringU5BU5D_t1642385972* L_27 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral1645711892);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1645711892);
		NullCheck(L_26);
		IDs_Add_m217445704(L_26, _stringLiteral1461038519, L_27, /*hidden argument*/NULL);
		IDs_t3808979560 * L_28 = V_2;
		NullCheck(L_20);
		ConfigurationBuilder_AddProduct_m918244722(L_20, _stringLiteral1830547213, 0, L_28, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_29 = V_1;
		IDs_t3808979560 * L_30 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_30, /*hidden argument*/NULL);
		V_2 = L_30;
		IDs_t3808979560 * L_31 = V_2;
		StringU5BU5D_t1642385972* L_32 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral2700534657);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_31);
		IDs_Add_m217445704(L_31, _stringLiteral862026094, L_32, /*hidden argument*/NULL);
		IDs_t3808979560 * L_33 = V_2;
		StringU5BU5D_t1642385972* L_34 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, _stringLiteral859071123);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral859071123);
		NullCheck(L_33);
		IDs_Add_m217445704(L_33, _stringLiteral2396058710, L_34, /*hidden argument*/NULL);
		IDs_t3808979560 * L_35 = V_2;
		StringU5BU5D_t1642385972* L_36 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, _stringLiteral1645711892);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1645711892);
		NullCheck(L_35);
		IDs_Add_m217445704(L_35, _stringLiteral4189921875, L_36, /*hidden argument*/NULL);
		IDs_t3808979560 * L_37 = V_2;
		NullCheck(L_29);
		ConfigurationBuilder_AddProduct_m918244722(L_29, _stringLiteral968850961, 0, L_37, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_38 = V_1;
		IDs_t3808979560 * L_39 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_39, /*hidden argument*/NULL);
		V_2 = L_39;
		IDs_t3808979560 * L_40 = V_2;
		StringU5BU5D_t1642385972* L_41 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, _stringLiteral2700534657);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_40);
		IDs_Add_m217445704(L_40, _stringLiteral1346993374, L_41, /*hidden argument*/NULL);
		IDs_t3808979560 * L_42 = V_2;
		StringU5BU5D_t1642385972* L_43 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, _stringLiteral859071123);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral859071123);
		NullCheck(L_42);
		IDs_Add_m217445704(L_42, _stringLiteral1233259296, L_43, /*hidden argument*/NULL);
		IDs_t3808979560 * L_44 = V_2;
		NullCheck(L_38);
		ConfigurationBuilder_AddProduct_m918244722(L_38, _stringLiteral1070925405, 1, L_44, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_45 = V_1;
		IDs_t3808979560 * L_46 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_46, /*hidden argument*/NULL);
		V_2 = L_46;
		IDs_t3808979560 * L_47 = V_2;
		StringU5BU5D_t1642385972* L_48 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, _stringLiteral2700534657);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_47);
		IDs_Add_m217445704(L_47, _stringLiteral784127634, L_48, /*hidden argument*/NULL);
		IDs_t3808979560 * L_49 = V_2;
		NullCheck(L_45);
		ConfigurationBuilder_AddProduct_m918244722(L_45, _stringLiteral2315729507, 2, L_49, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_50 = V_1;
		NullCheck(L_50);
		Il2CppObject * L_51 = ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324(L_50, /*hidden argument*/ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324_MethodInfo_var);
		ConfigurationBuilder_t1298400415 * L_52 = V_1;
		NullCheck(L_52);
		HashSet_1_t275936122 * L_53 = ConfigurationBuilder_get_products_m3201377931(L_52, /*hidden argument*/NULL);
		NullCheck(L_51);
		InterfaceActionInvoker1< HashSet_1_t275936122 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IAmazonConfiguration::WriteSandboxJSON(System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>) */, IAmazonConfiguration_t3016942165_il2cpp_TypeInfo_var, L_51, L_53);
		ConfigurationBuilder_t1298400415 * L_54 = V_1;
		NullCheck(L_54);
		Il2CppObject * L_55 = ConfigurationBuilder_Configure_TisISamsungAppsConfiguration_t4066821689_m2324730914(L_54, /*hidden argument*/ConfigurationBuilder_Configure_TisISamsungAppsConfiguration_t4066821689_m2324730914_MethodInfo_var);
		NullCheck(L_55);
		InterfaceActionInvoker1< int32_t >::Invoke(0 /* System.Void UnityEngine.Purchasing.ISamsungAppsConfiguration::SetMode(UnityEngine.Purchasing.SamsungAppsMode) */, ISamsungAppsConfiguration_t4066821689_il2cpp_TypeInfo_var, L_55, 1);
		int32_t L_56 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = __this;
		if ((!(((uint32_t)L_56) == ((uint32_t)((int32_t)11)))))
		{
			G_B8_0 = __this;
			goto IL_0205;
		}
	}
	{
		StandardPurchasingModule_t4003664591 * L_57 = V_0;
		NullCheck(L_57);
		int32_t L_58 = StandardPurchasingModule_get_androidStore_m1436760510(L_57, /*hidden argument*/NULL);
		G_B9_0 = ((((int32_t)L_58) == ((int32_t)3))? 1 : 0);
		G_B9_1 = G_B7_0;
		goto IL_0206;
	}

IL_0205:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_0206:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_m_IsSamsungAppsStoreSelected_8((bool)G_B9_0);
		ConfigurationBuilder_t1298400415 * L_59 = V_1;
		NullCheck(L_59);
		Il2CppObject * L_60 = ConfigurationBuilder_Configure_TisITizenStoreConfiguration_t2900348728_m1247129795(L_59, /*hidden argument*/ConfigurationBuilder_Configure_TisITizenStoreConfiguration_t2900348728_m1247129795_MethodInfo_var);
		NullCheck(L_60);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void UnityEngine.Purchasing.ITizenStoreConfiguration::SetGroupId(System.String) */, ITizenStoreConfiguration_t2900348728_il2cpp_TypeInfo_var, L_60, _stringLiteral1451147361);
		ConfigurationBuilder_t1298400415 * L_61 = V_1;
		UnityPurchasing_Initialize_m1012234273(NULL /*static, unused*/, __this, L_61, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::OnTransactionsRestored(System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1900125737;
extern const uint32_t IAPDemo_OnTransactionsRestored_m2633548551_MetadataUsageId;
extern "C"  void IAPDemo_OnTransactionsRestored_m2633548551 (IAPDemo_t823941185 * __this, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnTransactionsRestored_m2633548551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1900125737, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::OnDeferred(UnityEngine.Purchasing.Product)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1205201122;
extern const uint32_t IAPDemo_OnDeferred_m801123048_MetadataUsageId;
extern "C"  void IAPDemo_OnDeferred_m801123048 (IAPDemo_t823941185 * __this, Product_t1203687971 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnDeferred_m801123048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___item0;
		NullCheck(L_0);
		ProductDefinition_t1942475268 * L_1 = Product_get_definition_m2035415516(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_id_m264072292(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1205201122, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::InitUI(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>)
extern Il2CppClass* IEnumerable_1_t1495815016_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2974179094_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductType_t2754455291_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t2420267500_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3438463199_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3486116920_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__0_m3848246332_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m4197675336_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m404084168_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__1_m905417126_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__2_m905417093_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__3_m905417060_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__4_m905417027_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__5_m905416994_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4265067804;
extern const uint32_t IAPDemo_InitUI_m3350388155_MetadataUsageId;
extern "C"  void IAPDemo_InitUI_m3350388155 (IAPDemo_t823941185 * __this, Il2CppObject* ___items0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_InitUI_m3350388155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dropdown_t1985816271 * L_0 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		__this->set_m_InteractableSelectable_16(L_0);
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)8)))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_2 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)((int32_t)31))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_4 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)18))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_5 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)19))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_6 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)((int32_t)20))))
		{
			goto IL_007c;
		}
	}
	{
		bool L_7 = __this->get_m_IsSamsungAppsStoreSelected_8();
		if (L_7)
		{
			goto IL_007c;
		}
	}
	{
		bool L_8 = __this->get_m_IsCloudMoolahStoreSelected_9();
		if (L_8)
		{
			goto IL_007c;
		}
	}
	{
		Button_t2872111280 * L_9 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_007c:
	{
		Button_t2872111280 * L_11 = IAPDemo_GetRegisterButton_m1221450815(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		bool L_13 = __this->get_m_IsCloudMoolahStoreSelected_9();
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, L_13, /*hidden argument*/NULL);
		Button_t2872111280 * L_14 = IAPDemo_GetLoginButton_m4237916829(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(L_14, /*hidden argument*/NULL);
		bool L_16 = __this->get_m_IsCloudMoolahStoreSelected_9();
		NullCheck(L_15);
		GameObject_SetActive_m2887581199(L_15, L_16, /*hidden argument*/NULL);
		Button_t2872111280 * L_17 = IAPDemo_GetValidateButton_m3687892462(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m3105766835(L_17, /*hidden argument*/NULL);
		bool L_19 = __this->get_m_IsCloudMoolahStoreSelected_9();
		NullCheck(L_18);
		GameObject_SetActive_m2887581199(L_18, L_19, /*hidden argument*/NULL);
		Il2CppObject* L_20 = ___items0;
		NullCheck(L_20);
		Il2CppObject* L_21 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>::GetEnumerator() */, IEnumerable_1_t1495815016_il2cpp_TypeInfo_var, L_20);
		V_1 = L_21;
	}

IL_00c6:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0110;
		}

IL_00cb:
		{
			Il2CppObject* L_22 = V_1;
			NullCheck(L_22);
			Product_t1203687971 * L_23 = InterfaceFuncInvoker0< Product_t1203687971 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.Product>::get_Current() */, IEnumerator_1_t2974179094_il2cpp_TypeInfo_var, L_22);
			V_0 = L_23;
			Product_t1203687971 * L_24 = V_0;
			NullCheck(L_24);
			ProductDefinition_t1942475268 * L_25 = Product_get_definition_m2035415516(L_24, /*hidden argument*/NULL);
			NullCheck(L_25);
			String_t* L_26 = ProductDefinition_get_id_m264072292(L_25, /*hidden argument*/NULL);
			Product_t1203687971 * L_27 = V_0;
			NullCheck(L_27);
			ProductDefinition_t1942475268 * L_28 = Product_get_definition_m2035415516(L_27, /*hidden argument*/NULL);
			NullCheck(L_28);
			int32_t L_29 = ProductDefinition_get_type_m590914665(L_28, /*hidden argument*/NULL);
			int32_t L_30 = L_29;
			Il2CppObject * L_31 = Box(ProductType_t2754455291_il2cpp_TypeInfo_var, &L_30);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_32 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4265067804, L_26, L_31, /*hidden argument*/NULL);
			V_2 = L_32;
			Dropdown_t1985816271 * L_33 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
			NullCheck(L_33);
			List_1_t1789388632 * L_34 = Dropdown_get_options_m2669836220(L_33, /*hidden argument*/NULL);
			String_t* L_35 = V_2;
			OptionData_t2420267500 * L_36 = (OptionData_t2420267500 *)il2cpp_codegen_object_new(OptionData_t2420267500_il2cpp_TypeInfo_var);
			OptionData__ctor_m743450704(L_36, L_35, /*hidden argument*/NULL);
			NullCheck(L_34);
			List_1_Add_m3486116920(L_34, L_36, /*hidden argument*/List_1_Add_m3486116920_MethodInfo_var);
		}

IL_0110:
		{
			Il2CppObject* L_37 = V_1;
			NullCheck(L_37);
			bool L_38 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_37);
			if (L_38)
			{
				goto IL_00cb;
			}
		}

IL_011b:
		{
			IL2CPP_LEAVE(0x12D, FINALLY_0120);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0120;
	}

FINALLY_0120:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_39 = V_1;
			if (!L_39)
			{
				goto IL_012c;
			}
		}

IL_0126:
		{
			Il2CppObject* L_40 = V_1;
			NullCheck(L_40);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_40);
		}

IL_012c:
		{
			IL2CPP_END_FINALLY(288)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(288)
	{
		IL2CPP_JUMP_TBL(0x12D, IL_012d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_012d:
	{
		Dropdown_t1985816271 * L_41 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		Dropdown_RefreshShownValue_m3113581237(L_41, /*hidden argument*/NULL);
		Dropdown_t1985816271 * L_42 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		DropdownEvent_t2203087800 * L_43 = Dropdown_get_onValueChanged_m3334401942(L_42, /*hidden argument*/NULL);
		IntPtr_t L_44;
		L_44.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__0_m3848246332_MethodInfo_var);
		UnityAction_1_t3438463199 * L_45 = (UnityAction_1_t3438463199 *)il2cpp_codegen_object_new(UnityAction_1_t3438463199_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4197675336(L_45, __this, L_44, /*hidden argument*/UnityAction_1__ctor_m4197675336_MethodInfo_var);
		NullCheck(L_43);
		UnityEvent_1_AddListener_m404084168(L_43, L_45, /*hidden argument*/UnityEvent_1_AddListener_m404084168_MethodInfo_var);
		Button_t2872111280 * L_46 = IAPDemo_GetBuyButton_m514831370(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		ButtonClickedEvent_t2455055323 * L_47 = Button_get_onClick_m1595880935(L_46, /*hidden argument*/NULL);
		IntPtr_t L_48;
		L_48.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__1_m905417126_MethodInfo_var);
		UnityAction_t4025899511 * L_49 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_49, __this, L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		UnityEvent_AddListener_m1596810379(L_47, L_49, /*hidden argument*/NULL);
		Button_t2872111280 * L_50 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_51 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_50, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_019f;
		}
	}
	{
		Button_t2872111280 * L_52 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		ButtonClickedEvent_t2455055323 * L_53 = Button_get_onClick_m1595880935(L_52, /*hidden argument*/NULL);
		IntPtr_t L_54;
		L_54.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__2_m905417093_MethodInfo_var);
		UnityAction_t4025899511 * L_55 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_55, __this, L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		UnityEvent_AddListener_m1596810379(L_53, L_55, /*hidden argument*/NULL);
	}

IL_019f:
	{
		Button_t2872111280 * L_56 = IAPDemo_GetRegisterButton_m1221450815(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_57 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_56, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_01ce;
		}
	}
	{
		Button_t2872111280 * L_58 = IAPDemo_GetRegisterButton_m1221450815(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		ButtonClickedEvent_t2455055323 * L_59 = Button_get_onClick_m1595880935(L_58, /*hidden argument*/NULL);
		IntPtr_t L_60;
		L_60.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__3_m905417060_MethodInfo_var);
		UnityAction_t4025899511 * L_61 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_61, __this, L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		UnityEvent_AddListener_m1596810379(L_59, L_61, /*hidden argument*/NULL);
	}

IL_01ce:
	{
		Button_t2872111280 * L_62 = IAPDemo_GetLoginButton_m4237916829(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_63 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_62, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_01fd;
		}
	}
	{
		Button_t2872111280 * L_64 = IAPDemo_GetLoginButton_m4237916829(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		ButtonClickedEvent_t2455055323 * L_65 = Button_get_onClick_m1595880935(L_64, /*hidden argument*/NULL);
		IntPtr_t L_66;
		L_66.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__4_m905417027_MethodInfo_var);
		UnityAction_t4025899511 * L_67 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_67, __this, L_66, /*hidden argument*/NULL);
		NullCheck(L_65);
		UnityEvent_AddListener_m1596810379(L_65, L_67, /*hidden argument*/NULL);
	}

IL_01fd:
	{
		Button_t2872111280 * L_68 = IAPDemo_GetValidateButton_m3687892462(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_69 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_68, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_022c;
		}
	}
	{
		Button_t2872111280 * L_70 = IAPDemo_GetValidateButton_m3687892462(__this, /*hidden argument*/NULL);
		NullCheck(L_70);
		ButtonClickedEvent_t2455055323 * L_71 = Button_get_onClick_m1595880935(L_70, /*hidden argument*/NULL);
		IntPtr_t L_72;
		L_72.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__5_m905416994_MethodInfo_var);
		UnityAction_t4025899511 * L_73 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_73, __this, L_72, /*hidden argument*/NULL);
		NullCheck(L_71);
		UnityEvent_AddListener_m1596810379(L_71, L_73, /*hidden argument*/NULL);
	}

IL_022c:
	{
		return;
	}
}
// System.Void IAPDemo::LoginResult(UnityEngine.Purchasing.LoginResultState,System.String)
extern Il2CppClass* LoginResultState_t2459016979_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral402234061;
extern Il2CppCodeGenString* _stringLiteral2995257247;
extern const uint32_t IAPDemo_LoginResult_m3307439430_MetadataUsageId;
extern "C"  void IAPDemo_LoginResult_m3307439430 (IAPDemo_t823941185 * __this, int32_t ___state0, String_t* ___errorMsg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_LoginResult_m3307439430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___state0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		__this->set_m_IsLoggedIn_13((bool)1);
		goto IL_001e;
	}

IL_0015:
	{
		__this->set_m_IsLoggedIn_13((bool)0);
	}

IL_001e:
	{
		Il2CppObject * L_1 = Box(LoginResultState_t2459016979_il2cpp_TypeInfo_var, (&___state0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		String_t* L_3 = ___errorMsg1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral402234061, L_2, _stringLiteral2995257247, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::RegisterSucceeded(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1459558501;
extern const uint32_t IAPDemo_RegisterSucceeded_m3917157810_MetadataUsageId;
extern "C"  void IAPDemo_RegisterSucceeded_m3917157810 (IAPDemo_t823941185 * __this, String_t* ___cmUserName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_RegisterSucceeded_m3917157810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___cmUserName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1459558501, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___cmUserName0;
		__this->set_m_CloudMoolahUserName_12(L_2);
		return;
	}
}
// System.Void IAPDemo::RegisterFailed(UnityEngine.Purchasing.FastRegisterError,System.String)
extern Il2CppClass* FastRegisterError_t341731807_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral684661973;
extern Il2CppCodeGenString* _stringLiteral4270924376;
extern const uint32_t IAPDemo_RegisterFailed_m391306130_MetadataUsageId;
extern "C"  void IAPDemo_RegisterFailed_m391306130 (IAPDemo_t823941185 * __this, int32_t ___error0, String_t* ___errorMessage1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_RegisterFailed_m391306130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = Box(FastRegisterError_t341731807_il2cpp_TypeInfo_var, (&___error0));
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		String_t* L_2 = ___errorMessage1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral684661973, L_1, _stringLiteral4270924376, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::UpdateHistoryUI()
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral676257233;
extern Il2CppCodeGenString* _stringLiteral1576255139;
extern Il2CppCodeGenString* _stringLiteral2162321594;
extern const uint32_t IAPDemo_UpdateHistoryUI_m2607563357_MetadataUsageId;
extern "C"  void IAPDemo_UpdateHistoryUI_m2607563357 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_UpdateHistoryUI_m2607563357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Product_t1203687971 * V_2 = NULL;
	ProductU5BU5D_t728099314* V_3 = NULL;
	int32_t V_4 = 0;
	bool V_5 = false;
	{
		Il2CppObject * L_0 = __this->get_m_Controller_2();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00a8;
	}

IL_0012:
	{
		V_0 = _stringLiteral676257233;
		V_1 = _stringLiteral1576255139;
		Il2CppObject * L_1 = __this->get_m_Controller_2();
		NullCheck(L_1);
		ProductCollection_t3600019299 * L_2 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		ProductU5BU5D_t728099314* L_3 = ProductCollection_get_all_m3364167965(L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		V_4 = 0;
		goto IL_0084;
	}

IL_0038:
	{
		ProductU5BU5D_t728099314* L_4 = V_3;
		int32_t L_5 = V_4;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Product_t1203687971 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		String_t* L_8 = V_0;
		Product_t1203687971 * L_9 = V_2;
		NullCheck(L_9);
		ProductDefinition_t1942475268 * L_10 = Product_get_definition_m2035415516(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = ProductDefinition_get_id_m264072292(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, L_8, _stringLiteral2162321594, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13 = V_1;
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, L_13, _stringLiteral2162321594, /*hidden argument*/NULL);
		V_1 = L_14;
		String_t* L_15 = V_1;
		Product_t1203687971 * L_16 = V_2;
		NullCheck(L_16);
		bool L_17 = Product_get_hasReceipt_m617723237(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		String_t* L_18 = Boolean_ToString_m1253164328((&V_5), /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		int32_t L_20 = V_4;
		V_4 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0084:
	{
		int32_t L_21 = V_4;
		ProductU5BU5D_t728099314* L_22 = V_3;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_0038;
		}
	}
	{
		Text_t356221433 * L_23 = IAPDemo_GetText_m3382056873(__this, (bool)0, /*hidden argument*/NULL);
		String_t* L_24 = V_0;
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_23, L_24);
		Text_t356221433 * L_25 = IAPDemo_GetText_m3382056873(__this, (bool)1, /*hidden argument*/NULL);
		String_t* L_26 = V_1;
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_26);
	}

IL_00a8:
	{
		return;
	}
}
// System.Void IAPDemo::UpdateInteractable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t IAPDemo_UpdateInteractable_m2228859499_MetadataUsageId;
extern "C"  void IAPDemo_UpdateInteractable_m2228859499 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_UpdateInteractable_m2228859499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Selectable_t1490392188 * L_0 = __this->get_m_InteractableSelectable_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		goto IL_0087;
	}

IL_0018:
	{
		Il2CppObject * L_2 = __this->get_m_Controller_2();
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		Selectable_t1490392188 * L_4 = __this->get_m_InteractableSelectable_16();
		NullCheck(L_4);
		bool L_5 = Selectable_get_interactable_m1725029500(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)L_5)))
		{
			goto IL_0087;
		}
	}
	{
		Button_t2872111280 * L_6 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Button_t2872111280 * L_8 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		bool L_9 = V_0;
		NullCheck(L_8);
		Selectable_set_interactable_m63718297(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0056:
	{
		Button_t2872111280 * L_10 = IAPDemo_GetBuyButton_m514831370(__this, /*hidden argument*/NULL);
		bool L_11 = V_0;
		NullCheck(L_10);
		Selectable_set_interactable_m63718297(L_10, L_11, /*hidden argument*/NULL);
		Dropdown_t1985816271 * L_12 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		bool L_13 = V_0;
		NullCheck(L_12);
		Selectable_set_interactable_m63718297(L_12, L_13, /*hidden argument*/NULL);
		Button_t2872111280 * L_14 = IAPDemo_GetRegisterButton_m1221450815(__this, /*hidden argument*/NULL);
		bool L_15 = V_0;
		NullCheck(L_14);
		Selectable_set_interactable_m63718297(L_14, L_15, /*hidden argument*/NULL);
		Button_t2872111280 * L_16 = IAPDemo_GetLoginButton_m4237916829(__this, /*hidden argument*/NULL);
		bool L_17 = V_0;
		NullCheck(L_16);
		Selectable_set_interactable_m63718297(L_16, L_17, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Void IAPDemo::Update()
extern "C"  void IAPDemo_Update_m412123535 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		IAPDemo_UpdateInteractable_m2228859499(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Dropdown IAPDemo::GetDropdown()
extern const MethodInfo* GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1625601915;
extern const uint32_t IAPDemo_GetDropdown_m1658389538_MetadataUsageId;
extern "C"  Dropdown_t1985816271 * IAPDemo_GetDropdown_m1658389538 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetDropdown_m1658389538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dropdown_t1985816271 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1625601915, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dropdown_t1985816271 * L_1 = GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(L_0, /*hidden argument*/GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var);
		V_0 = L_1;
		goto IL_0016;
	}

IL_0016:
	{
		Dropdown_t1985816271 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.UI.Button IAPDemo::GetBuyButton()
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2309168590;
extern const uint32_t IAPDemo_GetBuyButton_m514831370_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetBuyButton_m514831370 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetBuyButton_m514831370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2309168590, /*hidden argument*/NULL);
		NullCheck(L_0);
		Button_t2872111280 * L_1 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_0, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_0 = L_1;
		goto IL_0016;
	}

IL_0016:
	{
		Button_t2872111280 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.UI.Button IAPDemo::GetRestoreButton()
extern Il2CppCodeGenString* _stringLiteral1335657580;
extern const uint32_t IAPDemo_GetRestoreButton_m3830435552_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetRestoreButton_m3830435552 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetRestoreButton_m3830435552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		Button_t2872111280 * L_0 = IAPDemo_GetButton_m892847222(__this, _stringLiteral1335657580, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Button_t2872111280 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.UI.Button IAPDemo::GetRegisterButton()
extern Il2CppCodeGenString* _stringLiteral24124625;
extern const uint32_t IAPDemo_GetRegisterButton_m1221450815_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetRegisterButton_m1221450815 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetRegisterButton_m1221450815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		Button_t2872111280 * L_0 = IAPDemo_GetButton_m892847222(__this, _stringLiteral24124625, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Button_t2872111280 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.UI.Button IAPDemo::GetLoginButton()
extern Il2CppCodeGenString* _stringLiteral4283667535;
extern const uint32_t IAPDemo_GetLoginButton_m4237916829_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetLoginButton_m4237916829 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetLoginButton_m4237916829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		Button_t2872111280 * L_0 = IAPDemo_GetButton_m892847222(__this, _stringLiteral4283667535, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Button_t2872111280 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.UI.Button IAPDemo::GetValidateButton()
extern Il2CppCodeGenString* _stringLiteral3570268284;
extern const uint32_t IAPDemo_GetValidateButton_m3687892462_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetValidateButton_m3687892462 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetValidateButton_m3687892462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		Button_t2872111280 * L_0 = IAPDemo_GetButton_m892847222(__this, _stringLiteral3570268284, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Button_t2872111280 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.UI.Button IAPDemo::GetButton(System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern const uint32_t IAPDemo_GetButton_m892847222_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetButton_m892847222 (IAPDemo_t823941185 * __this, String_t* ___buttonName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetButton_m892847222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Button_t2872111280 * V_1 = NULL;
	{
		String_t* L_0 = ___buttonName0;
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		Button_t2872111280 * L_5 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_4, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_1 = L_5;
		goto IL_0029;
	}

IL_0021:
	{
		V_1 = (Button_t2872111280 *)NULL;
		goto IL_0029;
	}

IL_0029:
	{
		Button_t2872111280 * L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.UI.Text IAPDemo::GetText(System.Boolean)
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3611362783;
extern Il2CppCodeGenString* _stringLiteral3611362761;
extern const uint32_t IAPDemo_GetText_m3382056873_MetadataUsageId;
extern "C"  Text_t356221433 * IAPDemo_GetText_m3382056873 (IAPDemo_t823941185 * __this, bool ___right0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetText_m3382056873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Text_t356221433 * V_1 = NULL;
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = ___right0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = _stringLiteral3611362783;
		goto IL_0016;
	}

IL_0011:
	{
		G_B3_0 = _stringLiteral3611362761;
	}

IL_0016:
	{
		V_0 = G_B3_0;
		String_t* L_1 = V_0;
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t356221433 * L_3 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_1 = L_3;
		goto IL_0028;
	}

IL_0028:
	{
		Text_t356221433 * L_4 = V_1;
		return L_4;
	}
}
// System.Void IAPDemo::LogProductDefinitions()
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductType_t2754455291_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2284497275;
extern const uint32_t IAPDemo_LogProductDefinitions_m1239761007_MetadataUsageId;
extern "C"  void IAPDemo_LogProductDefinitions_m1239761007 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_LogProductDefinitions_m1239761007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ProductU5BU5D_t728099314* V_0 = NULL;
	Product_t1203687971 * V_1 = NULL;
	ProductU5BU5D_t728099314* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Il2CppObject * L_0 = __this->get_m_Controller_2();
		NullCheck(L_0);
		ProductCollection_t3600019299 * L_1 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_0);
		NullCheck(L_1);
		ProductU5BU5D_t728099314* L_2 = ProductCollection_get_all_m3364167965(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ProductU5BU5D_t728099314* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0065;
	}

IL_001c:
	{
		ProductU5BU5D_t728099314* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Product_t1203687971 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		Product_t1203687971 * L_8 = V_1;
		NullCheck(L_8);
		ProductDefinition_t1942475268 * L_9 = Product_get_definition_m2035415516(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = ProductDefinition_get_id_m264072292(L_9, /*hidden argument*/NULL);
		Product_t1203687971 * L_11 = V_1;
		NullCheck(L_11);
		ProductDefinition_t1942475268 * L_12 = Product_get_definition_m2035415516(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = ProductDefinition_get_storeSpecificId_m2251287741(L_12, /*hidden argument*/NULL);
		Product_t1203687971 * L_14 = V_1;
		NullCheck(L_14);
		ProductDefinition_t1942475268 * L_15 = Product_get_definition_m2035415516(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = ProductDefinition_get_type_m590914665(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		Il2CppObject * L_17 = Box(ProductType_t2754455291_il2cpp_TypeInfo_var, (&V_4));
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral2284497275, L_10, L_13, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_3;
		V_3 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0065:
	{
		int32_t L_21 = V_3;
		ProductU5BU5D_t728099314* L_22 = V_2;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__0(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral253251037;
extern const uint32_t IAPDemo_U3CInitUIU3Em__0_m3848246332_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__0_m3848246332 (IAPDemo_t823941185 * __this, int32_t ___selectedItem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__0_m3848246332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___selectedItem0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral253251037, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___selectedItem0;
		__this->set_m_SelectedItemIndex_14(L_4);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__1()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1306873581;
extern Il2CppCodeGenString* _stringLiteral3696959560;
extern Il2CppCodeGenString* _stringLiteral466078066;
extern const uint32_t IAPDemo_U3CInitUIU3Em__1_m905417126_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__1_m905417126 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__1_m905417126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_PurchaseInProgress_15();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1306873581, /*hidden argument*/NULL);
		goto IL_006c;
	}

IL_001c:
	{
		bool L_1 = __this->get_m_IsCloudMoolahStoreSelected_9();
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		bool L_2 = __this->get_m_IsLoggedIn_13();
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3696959560, /*hidden argument*/NULL);
	}

IL_003e:
	{
		__this->set_m_PurchaseInProgress_15((bool)1);
		Il2CppObject * L_3 = __this->get_m_Controller_2();
		Il2CppObject * L_4 = __this->get_m_Controller_2();
		NullCheck(L_4);
		ProductCollection_t3600019299 * L_5 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		ProductU5BU5D_t728099314* L_6 = ProductCollection_get_all_m3364167965(L_5, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_m_SelectedItemIndex_14();
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Product_t1203687971 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_3);
		InterfaceActionInvoker2< Product_t1203687971 *, String_t* >::Invoke(1 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(UnityEngine.Purchasing.Product,System.String) */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_3, L_9, _stringLiteral466078066);
	}

IL_006c:
	{
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__2()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2289103034_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahExtension_t3195861654_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* ISamsungAppsExtensions_t3429739537_il2cpp_TypeInfo_var;
extern Il2CppClass* IMicrosoftExtensions_t1101930285_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__6_m494375636_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3424424418_MethodInfo_var;
extern const MethodInfo* IAPDemo_OnTransactionsRestored_m2633548551_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m309821356_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral778054244;
extern const uint32_t IAPDemo_U3CInitUIU3Em__2_m905417093_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__2_m905417093 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__2_m905417093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_IsCloudMoolahStoreSelected_9();
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		bool L_1 = __this->get_m_IsLoggedIn_13();
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral778054244, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_0029:
	{
		Il2CppObject * L_2 = __this->get_m_MoolahExtensions_4();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__6_m494375636_MethodInfo_var);
		Action_1_t2289103034 * L_4 = (Action_1_t2289103034 *)il2cpp_codegen_object_new(Action_1_t2289103034_il2cpp_TypeInfo_var);
		Action_1__ctor_m3424424418(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m3424424418_MethodInfo_var);
		NullCheck(L_2);
		InterfaceActionInvoker1< Action_1_t2289103034 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.IMoolahExtension::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>) */, IMoolahExtension_t3195861654_il2cpp_TypeInfo_var, L_2, L_4);
	}

IL_0042:
	{
		goto IL_00c0;
	}

IL_0048:
	{
		bool L_5 = __this->get_m_IsSamsungAppsStoreSelected_8();
		if (!L_5)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppObject * L_6 = __this->get_m_SamsungExtensions_5();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IAPDemo_OnTransactionsRestored_m2633548551_MethodInfo_var);
		Action_1_t3627374100 * L_8 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_6);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.ISamsungAppsExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, ISamsungAppsExtensions_t3429739537_il2cpp_TypeInfo_var, L_6, L_8);
		goto IL_00c0;
	}

IL_0071:
	{
		int32_t L_9 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)18))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_10 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)((int32_t)19))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_11 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_00a7;
		}
	}

IL_0095:
	{
		Il2CppObject * L_12 = __this->get_m_MicrosoftExtensions_6();
		NullCheck(L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.Purchasing.IMicrosoftExtensions::RestoreTransactions() */, IMicrosoftExtensions_t1101930285_il2cpp_TypeInfo_var, L_12);
		goto IL_00c0;
	}

IL_00a7:
	{
		Il2CppObject * L_13 = __this->get_m_AppleExtensions_3();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)IAPDemo_OnTransactionsRestored_m2633548551_MethodInfo_var);
		Action_1_t3627374100 * L_15 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_15, __this, L_14, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_13);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(1 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_13, L_15);
	}

IL_00c0:
	{
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__3()
extern Il2CppClass* Action_1_t1831019615_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t2383075575_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahExtension_t3195861654_il2cpp_TypeInfo_var;
extern const MethodInfo* IAPDemo_RegisterSucceeded_m3917157810_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1193014778_MethodInfo_var;
extern const MethodInfo* IAPDemo_RegisterFailed_m391306130_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m1697322934_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4086102453;
extern const uint32_t IAPDemo_U3CInitUIU3Em__3_m905417060_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__3_m905417060 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__3_m905417060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_m_MoolahExtensions_4();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IAPDemo_RegisterSucceeded_m3917157810_MethodInfo_var);
		Action_1_t1831019615 * L_2 = (Action_1_t1831019615 *)il2cpp_codegen_object_new(Action_1_t1831019615_il2cpp_TypeInfo_var);
		Action_1__ctor_m1193014778(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m1193014778_MethodInfo_var);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IAPDemo_RegisterFailed_m391306130_MethodInfo_var);
		Action_2_t2383075575 * L_4 = (Action_2_t2383075575 *)il2cpp_codegen_object_new(Action_2_t2383075575_il2cpp_TypeInfo_var);
		Action_2__ctor_m1697322934(L_4, __this, L_3, /*hidden argument*/Action_2__ctor_m1697322934_MethodInfo_var);
		NullCheck(L_0);
		InterfaceActionInvoker3< String_t*, Action_1_t1831019615 *, Action_2_t2383075575 * >::Invoke(1 /* System.Void UnityEngine.Purchasing.IMoolahExtension::FastRegister(System.String,System.Action`1<System.String>,System.Action`2<UnityEngine.Purchasing.FastRegisterError,System.String>) */, IMoolahExtension_t3195861654_il2cpp_TypeInfo_var, L_0, _stringLiteral4086102453, L_2, L_4);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__4()
extern Il2CppClass* Action_2_t2420917363_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahExtension_t3195861654_il2cpp_TypeInfo_var;
extern const MethodInfo* IAPDemo_LoginResult_m3307439430_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m1793236776_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4086102453;
extern const uint32_t IAPDemo_U3CInitUIU3Em__4_m905417027_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__4_m905417027 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__4_m905417027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_m_MoolahExtensions_4();
		String_t* L_1 = __this->get_m_CloudMoolahUserName_12();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)IAPDemo_LoginResult_m3307439430_MethodInfo_var);
		Action_2_t2420917363 * L_3 = (Action_2_t2420917363 *)il2cpp_codegen_object_new(Action_2_t2420917363_il2cpp_TypeInfo_var);
		Action_2__ctor_m1793236776(L_3, __this, L_2, /*hidden argument*/Action_2__ctor_m1793236776_MethodInfo_var);
		NullCheck(L_0);
		InterfaceActionInvoker3< String_t*, String_t*, Action_2_t2420917363 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IMoolahExtension::Login(System.String,System.String,System.Action`2<UnityEngine.Purchasing.LoginResultState,System.String>) */, IMoolahExtension_t3195861654_il2cpp_TypeInfo_var, L_0, L_1, _stringLiteral4086102453, L_3);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__5()
extern Il2CppClass* IAPDemo_t823941185_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t681716397_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahExtension_t3195861654_il2cpp_TypeInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__7_m3126507202_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m2668586083_MethodInfo_var;
extern const uint32_t IAPDemo_U3CInitUIU3Em__5_m905416994_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__5_m905416994 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__5_m905416994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Il2CppObject * G_B2_2 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	Il2CppObject * G_B1_2 = NULL;
	{
		Il2CppObject * L_0 = __this->get_m_MoolahExtensions_4();
		String_t* L_1 = __this->get_m_LastTransationID_10();
		String_t* L_2 = __this->get_m_LastReceipt_11();
		Action_3_t681716397 * L_3 = ((IAPDemo_t823941185_StaticFields*)IAPDemo_t823941185_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_17();
		G_B1_0 = L_2;
		G_B1_1 = L_1;
		G_B1_2 = L_0;
		if (L_3)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_1;
			G_B2_2 = L_0;
			goto IL_002b;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__7_m3126507202_MethodInfo_var);
		Action_3_t681716397 * L_5 = (Action_3_t681716397 *)il2cpp_codegen_object_new(Action_3_t681716397_il2cpp_TypeInfo_var);
		Action_3__ctor_m2668586083(L_5, NULL, L_4, /*hidden argument*/Action_3__ctor_m2668586083_MethodInfo_var);
		((IAPDemo_t823941185_StaticFields*)IAPDemo_t823941185_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_17(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_002b:
	{
		Action_3_t681716397 * L_6 = ((IAPDemo_t823941185_StaticFields*)IAPDemo_t823941185_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_17();
		NullCheck(G_B2_2);
		InterfaceActionInvoker3< String_t*, String_t*, Action_3_t681716397 * >::Invoke(3 /* System.Void UnityEngine.Purchasing.IMoolahExtension::ValidateReceipt(System.String,System.String,System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>) */, IMoolahExtension_t3195861654_il2cpp_TypeInfo_var, G_B2_2, G_B2_1, G_B2_0, L_6);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__6(UnityEngine.Purchasing.RestoreTransactionIDState)
extern Il2CppClass* RestoreTransactionIDState_t2487303652_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral509298699;
extern const uint32_t IAPDemo_U3CInitUIU3Em__6_m494375636_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__6_m494375636 (IAPDemo_t823941185 * __this, int32_t ___restoreTransactionIDState0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__6_m494375636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = Box(RestoreTransactionIDState_t2487303652_il2cpp_TypeInfo_var, (&___restoreTransactionIDState0));
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral509298699, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___restoreTransactionIDState0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_4 = ___restoreTransactionIDState0;
		G_B3_0 = ((((int32_t)((((int32_t)L_4) == ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		V_0 = (bool)G_B3_0;
		bool L_5 = V_0;
		IAPDemo_OnTransactionsRestored_m2633548551(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__7(System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* ValidateReceiptState_t4359597_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral195375549;
extern Il2CppCodeGenString* _stringLiteral1584141689;
extern Il2CppCodeGenString* _stringLiteral1170894105;
extern const uint32_t IAPDemo_U3CInitUIU3Em__7_m3126507202_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__7_m3126507202 (Il2CppObject * __this /* static, unused */, String_t* ___transactionID0, int32_t ___state1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__7_m3126507202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral195375549);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral195375549);
		StringU5BU5D_t1642385972* L_1 = L_0;
		String_t* L_2 = ___transactionID0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral1584141689);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1584141689);
		StringU5BU5D_t1642385972* L_4 = L_3;
		Il2CppObject * L_5 = Box(ValidateReceiptState_t4359597_il2cpp_TypeInfo_var, (&___state1));
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_6);
		StringU5BU5D_t1642385972* L_7 = L_4;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1170894105);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1170894105);
		StringU5BU5D_t1642385972* L_8 = L_7;
		String_t* L_9 = ___message2;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m626692867(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/<ProcessPurchase>c__AnonStorey0::.ctor()
extern "C"  void U3CProcessPurchaseU3Ec__AnonStorey0__ctor_m357671038 (U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/<ProcessPurchase>c__AnonStorey0::<>m__0(System.String,UnityEngine.Purchasing.RequestPayOutState,System.String)
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* RequestPayOutState_t3537434082_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1940085627;
extern Il2CppCodeGenString* _stringLiteral3896397081;
extern Il2CppCodeGenString* _stringLiteral136910681;
extern const uint32_t U3CProcessPurchaseU3Ec__AnonStorey0_U3CU3Em__0_m4275133472_MetadataUsageId;
extern "C"  void U3CProcessPurchaseU3Ec__AnonStorey0_U3CU3Em__0_m4275133472 (U3CProcessPurchaseU3Ec__AnonStorey0_t1218622627 * __this, String_t* ___transactionID0, int32_t ___state1, String_t* ___message2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CProcessPurchaseU3Ec__AnonStorey0_U3CU3Em__0_m4275133472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___state1;
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		IAPDemo_t823941185 * L_1 = __this->get_U24this_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = L_1->get_m_Controller_2();
		PurchaseEventArgs_t547992434 * L_3 = __this->get_e_0();
		NullCheck(L_3);
		Product_t1203687971 * L_4 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker1< Product_t1203687971 * >::Invoke(4 /* System.Void UnityEngine.Purchasing.IStoreController::ConfirmPendingPurchase(UnityEngine.Purchasing.Product) */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_2, L_4);
		goto IL_0064;
	}

IL_0029:
	{
		ObjectU5BU5D_t3614634134* L_5 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral1940085627);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1940085627);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		String_t* L_7 = ___transactionID0;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_6;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral3896397081);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3896397081);
		ObjectU5BU5D_t3614634134* L_9 = L_8;
		int32_t L_10 = ___state1;
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(RequestPayOutState_t3537434082_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = L_9;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral136910681);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral136910681);
		ObjectU5BU5D_t3614634134* L_14 = L_13;
		String_t* L_15 = ___message2;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void IOSBoard::.ctor()
extern "C"  void IOSBoard__ctor_m2544918228 (IOSBoard_t2012373677 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SetText_(char*);
// System.Void IOSBoard::SetText_(System.String)
extern "C"  void IOSBoard_SetText__m3816762528 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___str0' to native representation
	char* ____str0_marshaled = NULL;
	____str0_marshaled = il2cpp_codegen_marshal_string(___str0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SetText_)(____str0_marshaled);

	// Marshaling cleanup of parameter '___str0' native representation
	il2cpp_codegen_marshal_free(____str0_marshaled);
	____str0_marshaled = NULL;

}
extern "C" char* DEFAULT_CALL GetText_();
// System.String IOSBoard::GetText_()
extern "C"  String_t* IOSBoard_GetText__m2143813767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(GetText_)();

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// System.Void IOSBoard::SetText(System.String)
extern "C"  void IOSBoard_SetText_m4028622803 (IOSBoard_t2012373677 * __this, String_t* ___str0, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_1 = ___str0;
		IOSBoard_SetText__m3816762528(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.String IOSBoard::GetText()
extern "C"  String_t* IOSBoard_GetText_m1072243192 (IOSBoard_t2012373677 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = IOSBoard_GetText__m2143813767(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void iPhoneSpeaker::.ctor()
extern "C"  void iPhoneSpeaker__ctor_m486314007 (iPhoneSpeaker_t1703012646 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _forceToSpeaker();
// System.Void iPhoneSpeaker::_forceToSpeaker()
extern "C"  void iPhoneSpeaker__forceToSpeaker_m2973046869 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_forceToSpeaker)();

}
// System.Void iPhoneSpeaker::ForceToSpeaker()
extern "C"  void iPhoneSpeaker_ForceToSpeaker_m3276938204 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0013;
		}
	}
	{
		iPhoneSpeaker__forceToSpeaker_m2973046869(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void SilentModeHelper::.ctor()
extern "C"  void SilentModeHelper__ctor_m961678781 (SilentModeHelper_t1039473206 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _activeSilentMode();
// System.Void SilentModeHelper::_activeSilentMode()
extern "C"  void SilentModeHelper__activeSilentMode_m535670354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_activeSilentMode)();

}
extern "C" void DEFAULT_CALL _deactiveSilentMode();
// System.Void SilentModeHelper::_deactiveSilentMode()
extern "C"  void SilentModeHelper__deactiveSilentMode_m582413417 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_deactiveSilentMode)();

}
// System.Boolean SilentModeHelper::isActiveSilent()
extern Il2CppClass* SilentModeHelper_t1039473206_il2cpp_TypeInfo_var;
extern const uint32_t SilentModeHelper_isActiveSilent_m533583534_MetadataUsageId;
extern "C"  bool SilentModeHelper_isActiveSilent_m533583534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SilentModeHelper_isActiveSilent_m533583534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		bool L_0 = ((SilentModeHelper_t1039473206_StaticFields*)SilentModeHelper_t1039473206_il2cpp_TypeInfo_var->static_fields)->get__isActiveSilent_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void SilentModeHelper::ActiveSilentMode()
extern Il2CppClass* SilentModeHelper_t1039473206_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4015229964;
extern const uint32_t SilentModeHelper_ActiveSilentMode_m2511133439_MetadataUsageId;
extern "C"  void SilentModeHelper_ActiveSilentMode_m2511133439 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SilentModeHelper_ActiveSilentMode_m2511133439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SilentModeHelper_t1039473206_StaticFields*)SilentModeHelper_t1039473206_il2cpp_TypeInfo_var->static_fields)->set__isActiveSilent_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4015229964, /*hidden argument*/NULL);
		SilentModeHelper__activeSilentMode_m535670354(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SilentModeHelper::DeactiveSilentMode()
extern Il2CppClass* SilentModeHelper_t1039473206_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral674672215;
extern const uint32_t SilentModeHelper_DeactiveSilentMode_m2234628724_MetadataUsageId;
extern "C"  void SilentModeHelper_DeactiveSilentMode_m2234628724 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SilentModeHelper_DeactiveSilentMode_m2234628724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SilentModeHelper_t1039473206_StaticFields*)SilentModeHelper_t1039473206_il2cpp_TypeInfo_var->static_fields)->set__isActiveSilent_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral674672215, /*hidden argument*/NULL);
		SilentModeHelper__deactiveSilentMode_m582413417(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniClipboard::.ctor()
extern "C"  void UniClipboard__ctor_m4127140487 (UniClipboard_t2267012608 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBoard UniClipboard::get_board()
extern Il2CppClass* UniClipboard_t2267012608_il2cpp_TypeInfo_var;
extern Il2CppClass* IOSBoard_t2012373677_il2cpp_TypeInfo_var;
extern const uint32_t UniClipboard_get_board_m80537946_MetadataUsageId;
extern "C"  Il2CppObject * UniClipboard_get_board_m80537946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniClipboard_get_board_m80537946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ((UniClipboard_t2267012608_StaticFields*)UniClipboard_t2267012608_il2cpp_TypeInfo_var->static_fields)->get__board_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IOSBoard_t2012373677 * L_1 = (IOSBoard_t2012373677 *)il2cpp_codegen_object_new(IOSBoard_t2012373677_il2cpp_TypeInfo_var);
		IOSBoard__ctor_m2544918228(L_1, /*hidden argument*/NULL);
		((UniClipboard_t2267012608_StaticFields*)UniClipboard_t2267012608_il2cpp_TypeInfo_var->static_fields)->set__board_0(L_1);
	}

IL_0017:
	{
		Il2CppObject * L_2 = ((UniClipboard_t2267012608_StaticFields*)UniClipboard_t2267012608_il2cpp_TypeInfo_var->static_fields)->get__board_0();
		V_0 = L_2;
		goto IL_0022;
	}

IL_0022:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// System.Void UniClipboard::SetText(System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IBoard_t1215419429_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3365152975;
extern const uint32_t UniClipboard_SetText_m2485813056_MetadataUsageId;
extern "C"  void UniClipboard_SetText_m2485813056 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniClipboard_SetText_m2485813056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3365152975, /*hidden argument*/NULL);
		Il2CppObject * L_0 = UniClipboard_get_board_m80537946(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___str0;
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void IBoard::SetText(System.String) */, IBoard_t1215419429_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.String UniClipboard::GetText()
extern Il2CppClass* IBoard_t1215419429_il2cpp_TypeInfo_var;
extern const uint32_t UniClipboard_GetText_m10305417_MetadataUsageId;
extern "C"  String_t* UniClipboard_GetText_m10305417 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UniClipboard_GetText_m10305417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = UniClipboard_get_board_m80537946(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String IBoard::GetText() */, IBoard_t1215419429_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Analytics.DriveableProperty::.ctor()
extern "C"  void DriveableProperty__ctor_m1388005943 (DriveableProperty_t2641992127 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey> UnityEngine.Analytics.DriveableProperty::get_fields()
extern "C"  List_1_t1989477525 * DriveableProperty_get_fields_m3958983649 (DriveableProperty_t2641992127 * __this, const MethodInfo* method)
{
	List_1_t1989477525 * V_0 = NULL;
	{
		List_1_t1989477525 * L_0 = __this->get_m_Fields_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		List_1_t1989477525 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Analytics.DriveableProperty::set_fields(System.Collections.Generic.List`1<UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey>)
extern "C"  void DriveableProperty_set_fields_m2028066638 (DriveableProperty_t2641992127 * __this, List_1_t1989477525 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1989477525 * L_0 = DriveableProperty_get_fields_m3958983649(__this, /*hidden argument*/NULL);
		__this->set_m_Fields_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::.ctor()
extern "C"  void FieldWithRemoteSettingsKey__ctor_m360199534 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::get_target()
extern "C"  Object_t1021602117 * FieldWithRemoteSettingsKey_get_target_m1374164995 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method)
{
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = __this->get_m_Target_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t1021602117 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::set_target(UnityEngine.Object)
extern "C"  void FieldWithRemoteSettingsKey_set_target_m3009102684 (FieldWithRemoteSettingsKey_t2620356393 * __this, Object_t1021602117 * ___value0, const MethodInfo* method)
{
	{
		Object_t1021602117 * L_0 = FieldWithRemoteSettingsKey_get_target_m1374164995(__this, /*hidden argument*/NULL);
		__this->set_m_Target_0(L_0);
		return;
	}
}
// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::get_fieldPath()
extern "C"  String_t* FieldWithRemoteSettingsKey_get_fieldPath_m325365421 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_FieldPath_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::set_fieldPath(System.String)
extern "C"  void FieldWithRemoteSettingsKey_set_fieldPath_m1486844300 (FieldWithRemoteSettingsKey_t2620356393 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_FieldPath_1(L_0);
		return;
	}
}
// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::get_rsKeyName()
extern "C"  String_t* FieldWithRemoteSettingsKey_get_rsKeyName_m299883495 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_RSKeyName_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::set_rsKeyName(System.String)
extern "C"  void FieldWithRemoteSettingsKey_set_rsKeyName_m655532302 (FieldWithRemoteSettingsKey_t2620356393 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = FieldWithRemoteSettingsKey_get_rsKeyName_m299883495(__this, /*hidden argument*/NULL);
		__this->set_m_RSKeyName_2(L_0);
		return;
	}
}
// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::get_type()
extern "C"  String_t* FieldWithRemoteSettingsKey_get_type_m1923228720 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_Type_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::set_type(System.String)
extern "C"  void FieldWithRemoteSettingsKey_set_type_m2857900137 (FieldWithRemoteSettingsKey_t2620356393 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = FieldWithRemoteSettingsKey_get_type_m1923228720(__this, /*hidden argument*/NULL);
		__this->set_m_Type_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::SetValue(System.Object)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FieldWithRemoteSettingsKey_SetValue_m1404583385_MetadataUsageId;
extern "C"  void FieldWithRemoteSettingsKey_SetValue_m1404583385 (FieldWithRemoteSettingsKey_t2620356393 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FieldWithRemoteSettingsKey_SetValue_m1404583385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	int32_t V_3 = 0;
	PropertyInfo_t * V_4 = NULL;
	FieldInfo_t * V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t1021602117 * L_0 = __this->get_m_Target_0();
		V_0 = L_0;
		String_t* L_1 = __this->get_m_FieldPath_1();
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)46));
		NullCheck(L_1);
		StringU5BU5D_t1642385972* L_3 = String_Split_m3326265864(L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		V_3 = 0;
		goto IL_006f;
	}

IL_0027:
	{
		StringU5BU5D_t1642385972* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Object_GetType_m191970594(L_8, /*hidden argument*/NULL);
		String_t* L_10 = V_1;
		NullCheck(L_9);
		PropertyInfo_t * L_11 = Type_GetProperty_m808359402(L_9, L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		PropertyInfo_t * L_12 = V_4;
		Il2CppObject * L_13 = V_0;
		Il2CppObject * L_14 = ___val0;
		NullCheck(L_12);
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(24 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, L_12, L_13, L_14, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		goto IL_006a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_004b;
		throw e;
	}

CATCH_004b:
	{ // begin catch(System.Object)
		Il2CppObject * L_15 = V_0;
		NullCheck(L_15);
		Type_t * L_16 = Object_GetType_m191970594(L_15, /*hidden argument*/NULL);
		String_t* L_17 = V_1;
		NullCheck(L_16);
		FieldInfo_t * L_18 = Type_GetField_m3036413258(L_16, L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		FieldInfo_t * L_19 = V_5;
		Il2CppObject * L_20 = V_0;
		Il2CppObject * L_21 = ___val0;
		NullCheck(L_19);
		FieldInfo_SetValue_m2504255891(L_19, L_20, L_21, /*hidden argument*/NULL);
		goto IL_006a;
	} // end catch (depth: 1)

IL_006a:
	{
		int32_t L_22 = V_3;
		V_3 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_006f:
	{
		int32_t L_23 = V_3;
		StringU5BU5D_t1642385972* L_24 = V_2;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		return;
	}
}
// System.Type UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::GetTypeOfField()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FieldWithRemoteSettingsKey_GetTypeOfField_m928225135_MetadataUsageId;
extern "C"  Type_t * FieldWithRemoteSettingsKey_GetTypeOfField_m928225135 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FieldWithRemoteSettingsKey_GetTypeOfField_m928225135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	int32_t V_3 = 0;
	PropertyInfo_t * V_4 = NULL;
	FieldInfo_t * V_5 = NULL;
	Type_t * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t1021602117 * L_0 = __this->get_m_Target_0();
		V_0 = L_0;
		String_t* L_1 = __this->get_m_FieldPath_1();
		CharU5BU5D_t1328083999* L_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)46));
		NullCheck(L_1);
		StringU5BU5D_t1642385972* L_3 = String_Split_m3326265864(L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		V_3 = 0;
		goto IL_006f;
	}

IL_0027:
	{
		StringU5BU5D_t1642385972* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Object_GetType_m191970594(L_8, /*hidden argument*/NULL);
		String_t* L_10 = V_1;
		NullCheck(L_9);
		PropertyInfo_t * L_11 = Type_GetProperty_m808359402(L_9, L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		PropertyInfo_t * L_12 = V_4;
		Il2CppObject * L_13 = V_0;
		NullCheck(L_12);
		Il2CppObject * L_14 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_12, L_13, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		V_0 = L_14;
		goto IL_006a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_004b;
		throw e;
	}

CATCH_004b:
	{ // begin catch(System.Object)
		Il2CppObject * L_15 = V_0;
		NullCheck(L_15);
		Type_t * L_16 = Object_GetType_m191970594(L_15, /*hidden argument*/NULL);
		String_t* L_17 = V_1;
		NullCheck(L_16);
		FieldInfo_t * L_18 = Type_GetField_m3036413258(L_16, L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		FieldInfo_t * L_19 = V_5;
		Il2CppObject * L_20 = V_0;
		NullCheck(L_19);
		Il2CppObject * L_21 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_19, L_20);
		V_0 = L_21;
		goto IL_006a;
	} // end catch (depth: 1)

IL_006a:
	{
		int32_t L_22 = V_3;
		V_3 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_006f:
	{
		int32_t L_23 = V_3;
		StringU5BU5D_t1642385972* L_24 = V_2;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Il2CppObject * L_25 = V_0;
		NullCheck(L_25);
		Type_t * L_26 = Object_GetType_m191970594(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		goto IL_0085;
	}

IL_0085:
	{
		Type_t * L_27 = V_6;
		return L_27;
	}
}
// System.Void UnityEngine.Analytics.RemoteSettings::.ctor()
extern Il2CppClass* DriveableProperty_t2641992127_il2cpp_TypeInfo_var;
extern const uint32_t RemoteSettings__ctor_m171676155_MetadataUsageId;
extern "C"  void RemoteSettings__ctor_m171676155 (RemoteSettings_t3121907247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoteSettings__ctor_m171676155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DriveableProperty_t2641992127 * L_0 = (DriveableProperty_t2641992127 *)il2cpp_codegen_object_new(DriveableProperty_t2641992127_il2cpp_TypeInfo_var);
		DriveableProperty__ctor_m1388005943(L_0, /*hidden argument*/NULL);
		__this->set_m_DriveableProperty_2(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Analytics.DriveableProperty UnityEngine.Analytics.RemoteSettings::get_DP()
extern "C"  DriveableProperty_t2641992127 * RemoteSettings_get_DP_m4259510491 (RemoteSettings_t3121907247 * __this, const MethodInfo* method)
{
	DriveableProperty_t2641992127 * V_0 = NULL;
	{
		DriveableProperty_t2641992127 * L_0 = __this->get_m_DriveableProperty_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		DriveableProperty_t2641992127 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Analytics.RemoteSettings::set_DP(UnityEngine.Analytics.DriveableProperty)
extern "C"  void RemoteSettings_set_DP_m1524225954 (RemoteSettings_t3121907247 * __this, DriveableProperty_t2641992127 * ___value0, const MethodInfo* method)
{
	{
		DriveableProperty_t2641992127 * L_0 = ___value0;
		__this->set_m_DriveableProperty_2(L_0);
		return;
	}
}
// System.Void UnityEngine.Analytics.RemoteSettings::Start()
extern Il2CppClass* UpdatedEventHandler_t3033456180_il2cpp_TypeInfo_var;
extern const MethodInfo* RemoteSettings_RemoteSettingsUpdated_m1172715871_MethodInfo_var;
extern const uint32_t RemoteSettings_Start_m3812251055_MetadataUsageId;
extern "C"  void RemoteSettings_Start_m3812251055 (RemoteSettings_t3121907247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoteSettings_Start_m3812251055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RemoteSettings_RemoteSettingsUpdated_m1172715871(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)RemoteSettings_RemoteSettingsUpdated_m1172715871_MethodInfo_var);
		UpdatedEventHandler_t3033456180 * L_1 = (UpdatedEventHandler_t3033456180 *)il2cpp_codegen_object_new(UpdatedEventHandler_t3033456180_il2cpp_TypeInfo_var);
		UpdatedEventHandler__ctor_m1393569768(L_1, __this, L_0, /*hidden argument*/NULL);
		RemoteSettings_add_Updated_m337456019(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Analytics.RemoteSettings::RemoteSettingsUpdated()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1668379915_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1516139818_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral204391430;
extern Il2CppCodeGenString* _stringLiteral613657440;
extern Il2CppCodeGenString* _stringLiteral3068682575;
extern Il2CppCodeGenString* _stringLiteral3311014053;
extern const uint32_t RemoteSettings_RemoteSettingsUpdated_m1172715871_MetadataUsageId;
extern "C"  void RemoteSettings_RemoteSettingsUpdated_m1172715871 (RemoteSettings_t3121907247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoteSettings_RemoteSettingsUpdated_m1172715871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	FieldWithRemoteSettingsKey_t2620356393 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0121;
	}

IL_0008:
	{
		DriveableProperty_t2641992127 * L_0 = __this->get_m_DriveableProperty_2();
		NullCheck(L_0);
		List_1_t1989477525 * L_1 = DriveableProperty_get_fields_m3958983649(L_0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		FieldWithRemoteSettingsKey_t2620356393 * L_3 = List_1_get_Item_m1668379915(L_1, L_2, /*hidden argument*/List_1_get_Item_m1668379915_MethodInfo_var);
		V_1 = L_3;
		FieldWithRemoteSettingsKey_t2620356393 * L_4 = V_1;
		NullCheck(L_4);
		String_t* L_5 = FieldWithRemoteSettingsKey_get_rsKeyName_m299883495(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_011c;
		}
	}
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_7 = V_1;
		NullCheck(L_7);
		String_t* L_8 = FieldWithRemoteSettingsKey_get_rsKeyName_m299883495(L_7, /*hidden argument*/NULL);
		bool L_9 = RemoteSettings_HasKey_m4137791587(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_011c;
		}
	}
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_10 = V_1;
		NullCheck(L_10);
		Object_t1021602117 * L_11 = FieldWithRemoteSettingsKey_get_target_m1374164995(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_011c;
		}
	}
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_13 = V_1;
		NullCheck(L_13);
		String_t* L_14 = FieldWithRemoteSettingsKey_get_fieldPath_m325365421(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_011c;
		}
	}
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_16 = V_1;
		NullCheck(L_16);
		String_t* L_17 = FieldWithRemoteSettingsKey_get_type_m1923228720(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, _stringLiteral204391430, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_008f;
		}
	}
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_19 = V_1;
		FieldWithRemoteSettingsKey_t2620356393 * L_20 = V_1;
		NullCheck(L_20);
		String_t* L_21 = FieldWithRemoteSettingsKey_get_rsKeyName_m299883495(L_20, /*hidden argument*/NULL);
		bool L_22 = RemoteSettings_GetBool_m3999689170(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		bool L_23 = L_22;
		Il2CppObject * L_24 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_19);
		FieldWithRemoteSettingsKey_SetValue_m1404583385(L_19, L_24, /*hidden argument*/NULL);
		goto IL_011b;
	}

IL_008f:
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = FieldWithRemoteSettingsKey_get_type_m1923228720(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_26, _stringLiteral613657440, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00c1;
		}
	}
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_28 = V_1;
		FieldWithRemoteSettingsKey_t2620356393 * L_29 = V_1;
		NullCheck(L_29);
		String_t* L_30 = FieldWithRemoteSettingsKey_get_rsKeyName_m299883495(L_29, /*hidden argument*/NULL);
		float L_31 = RemoteSettings_GetFloat_m3325352824(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		float L_32 = L_31;
		Il2CppObject * L_33 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_28);
		FieldWithRemoteSettingsKey_SetValue_m1404583385(L_28, L_33, /*hidden argument*/NULL);
		goto IL_011b;
	}

IL_00c1:
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_34 = V_1;
		NullCheck(L_34);
		String_t* L_35 = FieldWithRemoteSettingsKey_get_type_m1923228720(L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_35, _stringLiteral3068682575, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00f3;
		}
	}
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_37 = V_1;
		FieldWithRemoteSettingsKey_t2620356393 * L_38 = V_1;
		NullCheck(L_38);
		String_t* L_39 = FieldWithRemoteSettingsKey_get_rsKeyName_m299883495(L_38, /*hidden argument*/NULL);
		int32_t L_40 = RemoteSettings_GetInt_m2058337813(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		int32_t L_41 = L_40;
		Il2CppObject * L_42 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_37);
		FieldWithRemoteSettingsKey_SetValue_m1404583385(L_37, L_42, /*hidden argument*/NULL);
		goto IL_011b;
	}

IL_00f3:
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_43 = V_1;
		NullCheck(L_43);
		String_t* L_44 = FieldWithRemoteSettingsKey_get_type_m1923228720(L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_45 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_44, _stringLiteral3311014053, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_011b;
		}
	}
	{
		FieldWithRemoteSettingsKey_t2620356393 * L_46 = V_1;
		FieldWithRemoteSettingsKey_t2620356393 * L_47 = V_1;
		NullCheck(L_47);
		String_t* L_48 = FieldWithRemoteSettingsKey_get_rsKeyName_m299883495(L_47, /*hidden argument*/NULL);
		String_t* L_49 = RemoteSettings_GetString_m1879132784(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		NullCheck(L_46);
		FieldWithRemoteSettingsKey_SetValue_m1404583385(L_46, L_49, /*hidden argument*/NULL);
	}

IL_011b:
	{
	}

IL_011c:
	{
		int32_t L_50 = V_0;
		V_0 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_0121:
	{
		int32_t L_51 = V_0;
		DriveableProperty_t2641992127 * L_52 = __this->get_m_DriveableProperty_2();
		NullCheck(L_52);
		List_1_t1989477525 * L_53 = DriveableProperty_get_fields_m3958983649(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		int32_t L_54 = List_1_get_Count_m1516139818(L_53, /*hidden argument*/List_1_get_Count_m1516139818_MethodInfo_var);
		if ((((int32_t)L_51) < ((int32_t)L_54)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.DemoInventory::.ctor()
extern "C"  void DemoInventory__ctor_m3811379471 (DemoInventory_t2756307745 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.DemoInventory::Fulfill(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1830547213;
extern Il2CppCodeGenString* _stringLiteral852982118;
extern Il2CppCodeGenString* _stringLiteral2538700183;
extern const uint32_t DemoInventory_Fulfill_m2814084525_MetadataUsageId;
extern "C"  void DemoInventory_Fulfill_m2814084525 (DemoInventory_t2756307745 * __this, String_t* ___productId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DemoInventory_Fulfill_m2814084525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___productId0;
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_1 = ___productId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1830547213, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		goto IL_002b;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral852982118, /*hidden argument*/NULL);
		goto IL_0040;
	}

IL_002b:
	{
		String_t* L_3 = ___productId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2538700183, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0040;
	}

IL_0040:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::.ctor()
extern "C"  void IAPButton__ctor_m3388164132 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	{
		__this->set_buttonType_3(0);
		__this->set_consumePurchase_4((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2872111280_m3197608264_MethodInfo_var;
extern const MethodInfo* IAPButton_PurchaseProduct_m225290346_MethodInfo_var;
extern const MethodInfo* IAPButton_Restore_m2361172162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1042601999;
extern Il2CppCodeGenString* _stringLiteral624664449;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern const uint32_t IAPButton_Start_m3426314436_MetadataUsageId;
extern "C"  void IAPButton_Start_m3426314436 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_Start_m3426314436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		Button_t2872111280 * L_0 = Component_GetComponent_TisButton_t2872111280_m3197608264(__this, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3197608264_MethodInfo_var);
		V_0 = L_0;
		int32_t L_1 = __this->get_buttonType_3();
		if (L_1)
		{
			goto IL_008b;
		}
	}
	{
		Button_t2872111280 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		Button_t2872111280 * L_4 = V_0;
		NullCheck(L_4);
		ButtonClickedEvent_t2455055323 * L_5 = Button_get_onClick_m1595880935(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IAPButton_PurchaseProduct_m225290346_MethodInfo_var);
		UnityAction_t4025899511 * L_7 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityEvent_AddListener_m1596810379(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0038:
	{
		String_t* L_8 = __this->get_productId_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1042601999, /*hidden argument*/NULL);
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_10 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_11 = __this->get_productId_2();
		NullCheck(L_10);
		bool L_12 = IAPButtonStoreManager_HasProductInCatalog_m3023623296(L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0085;
		}
	}
	{
		String_t* L_13 = __this->get_productId_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral624664449, L_13, _stringLiteral372029312, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0085:
	{
		goto IL_00bd;
	}

IL_008b:
	{
		int32_t L_15 = __this->get_buttonType_3();
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_00bd;
		}
	}
	{
		Button_t2872111280 * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00bc;
		}
	}
	{
		Button_t2872111280 * L_18 = V_0;
		NullCheck(L_18);
		ButtonClickedEvent_t2455055323 * L_19 = Button_get_onClick_m1595880935(L_18, /*hidden argument*/NULL);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IAPButton_Restore_m2361172162_MethodInfo_var);
		UnityAction_t4025899511 * L_21 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_21, __this, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		UnityEvent_AddListener_m1596810379(L_19, L_21, /*hidden argument*/NULL);
	}

IL_00bc:
	{
	}

IL_00bd:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::OnEnable()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPButton_OnEnable_m935239492_MetadataUsageId;
extern "C"  void IAPButton_OnEnable_m935239492 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_OnEnable_m935239492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_buttonType_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_1 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IAPButtonStoreManager_AddButton_m3524258615(L_1, __this, /*hidden argument*/NULL);
		IAPButton_UpdateText_m1500550460(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::OnDisable()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPButton_OnDisable_m1628155281_MetadataUsageId;
extern "C"  void IAPButton_OnDisable_m1628155281 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_OnDisable_m1628155281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_buttonType_3();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_1 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IAPButtonStoreManager_RemoveButton_m2931652138(L_1, __this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::PurchaseProduct()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1407997759;
extern const uint32_t IAPButton_PurchaseProduct_m225290346_MetadataUsageId;
extern "C"  void IAPButton_PurchaseProduct_m225290346 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_PurchaseProduct_m225290346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_buttonType_3();
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_1 = __this->get_productId_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1407997759, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_3 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = __this->get_productId_2();
		NullCheck(L_3);
		IAPButtonStoreManager_InitiatePurchase_m374565683(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::Restore()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern Il2CppClass* IMicrosoftExtensions_t1101930285_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* ISamsungAppsExtensions_t3429739537_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2289103034_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahExtension_t3195861654_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimePlatform_t1869584967_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var;
extern const MethodInfo* IAPButton_OnTransactionsRestored_m3924036197_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m309821356_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670_MethodInfo_var;
extern const MethodInfo* IAPButton_U3CRestoreU3Em__0_m4194234704_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3424424418_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1163488363;
extern const uint32_t IAPButton_Restore_m2361172162_MetadataUsageId;
extern "C"  void IAPButton_Restore_m2361172162 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_Restore_m2361172162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_buttonType_3();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0141;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)18))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_2 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)19))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_004d;
		}
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_4 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Il2CppObject * L_5 = IAPButtonStoreManager_get_ExtensionProvider_m473841661(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppObject * L_6 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781_MethodInfo_var, L_5);
		NullCheck(L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.Purchasing.IMicrosoftExtensions::RestoreTransactions() */, IMicrosoftExtensions_t1101930285_il2cpp_TypeInfo_var, L_6);
		goto IL_0140;
	}

IL_004d:
	{
		int32_t L_7 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)8)))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_8 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_9 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)31)))))
		{
			goto IL_0096;
		}
	}

IL_006f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_10 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Il2CppObject * L_11 = IAPButtonStoreManager_get_ExtensionProvider_m473841661(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Il2CppObject * L_12 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var, L_11);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)IAPButton_OnTransactionsRestored_m3924036197_MethodInfo_var);
		Action_1_t3627374100 * L_14 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_14, __this, L_13, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_12);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(1 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_12, L_14);
		goto IL_0140;
	}

IL_0096:
	{
		int32_t L_15 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00d9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_16 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = StandardPurchasingModule_get_androidStore_m1436760510(L_16, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_00d9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_18 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Il2CppObject * L_19 = IAPButtonStoreManager_get_ExtensionProvider_m473841661(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Il2CppObject * L_20 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459_MethodInfo_var, L_19);
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)IAPButton_OnTransactionsRestored_m3924036197_MethodInfo_var);
		Action_1_t3627374100 * L_22 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_22, __this, L_21, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_20);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.ISamsungAppsExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, ISamsungAppsExtensions_t3429739537_il2cpp_TypeInfo_var, L_20, L_22);
		goto IL_0140;
	}

IL_00d9:
	{
		int32_t L_23 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_011c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_24 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = StandardPurchasingModule_get_androidStore_m1436760510(L_24, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)2))))
		{
			goto IL_011c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_26 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		Il2CppObject * L_27 = IAPButtonStoreManager_get_ExtensionProvider_m473841661(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Il2CppObject * L_28 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670_MethodInfo_var, L_27);
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)IAPButton_U3CRestoreU3Em__0_m4194234704_MethodInfo_var);
		Action_1_t2289103034 * L_30 = (Action_1_t2289103034 *)il2cpp_codegen_object_new(Action_1_t2289103034_il2cpp_TypeInfo_var);
		Action_1__ctor_m3424424418(L_30, __this, L_29, /*hidden argument*/Action_1__ctor_m3424424418_MethodInfo_var);
		NullCheck(L_28);
		InterfaceActionInvoker1< Action_1_t2289103034 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.IMoolahExtension::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>) */, IMoolahExtension_t3195861654_il2cpp_TypeInfo_var, L_28, L_30);
		goto IL_0140;
	}

IL_011c:
	{
		int32_t L_31 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_31;
		Il2CppObject * L_32 = Box(RuntimePlatform_t1869584967_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m2596409543(NULL /*static, unused*/, L_33, _stringLiteral1163488363, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
	}

IL_0140:
	{
	}

IL_0141:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::OnTransactionsRestored(System.Boolean)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3057734245;
extern const uint32_t IAPButton_OnTransactionsRestored_m3924036197_MetadataUsageId;
extern "C"  void IAPButton_OnTransactionsRestored_m3924036197 (IAPButton_t3077837360 * __this, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_OnTransactionsRestored_m3924036197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___success0;
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3057734245, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult UnityEngine.Purchasing.IAPButton::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEvent_1_Invoke_m3308421134_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3952801405;
extern const uint32_t IAPButton_ProcessPurchase_m705789944_MetadataUsageId;
extern "C"  int32_t IAPButton_ProcessPurchase_m705789944 (IAPButton_t3077837360 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_ProcessPurchase_m705789944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		PurchaseEventArgs_t547992434 * L_0 = ___e0;
		PurchaseEventArgs_t547992434 * L_1 = ___e0;
		NullCheck(L_1);
		Product_t1203687971 * L_2 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		ProductDefinition_t1942475268 * L_3 = Product_get_definition_m2035415516(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = ProductDefinition_get_id_m264072292(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3952801405, L_0, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		OnPurchaseCompletedEvent_t4018783659 * L_6 = __this->get_onPurchaseComplete_5();
		PurchaseEventArgs_t547992434 * L_7 = ___e0;
		NullCheck(L_7);
		Product_t1203687971 * L_8 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityEvent_1_Invoke_m3308421134(L_6, L_8, /*hidden argument*/UnityEvent_1_Invoke_m3308421134_MethodInfo_var);
		bool L_9 = __this->get_consumePurchase_4();
		if (!L_9)
		{
			goto IL_0043;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B3_0 = 1;
	}

IL_0044:
	{
		V_0 = G_B3_0;
		goto IL_004a;
	}

IL_004a:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEvent_2_Invoke_m3835768897_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral480745;
extern const uint32_t IAPButton_OnPurchaseFailed_m2148672501_MetadataUsageId;
extern "C"  void IAPButton_OnPurchaseFailed_m2148672501 (IAPButton_t3077837360 * __this, Product_t1203687971 * ___product0, int32_t ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_OnPurchaseFailed_m2148672501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___product0;
		int32_t L_1 = ___reason1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral480745, L_0, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		OnPurchaseFailedEvent_t2813769101 * L_5 = __this->get_onPurchaseFailed_6();
		Product_t1203687971 * L_6 = ___product0;
		int32_t L_7 = ___reason1;
		NullCheck(L_5);
		UnityEvent_2_Invoke_m3835768897(L_5, L_6, L_7, /*hidden argument*/UnityEvent_2_Invoke_m3835768897_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::UpdateText()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t IAPButton_UpdateText_m1500550460_MetadataUsageId;
extern "C"  void IAPButton_UpdateText_m1500550460 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_UpdateText_m1500550460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_0 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = __this->get_productId_2();
		NullCheck(L_0);
		Product_t1203687971 * L_2 = IAPButtonStoreManager_GetProduct_m1505647265(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Product_t1203687971 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0095;
		}
	}
	{
		Text_t356221433 * L_4 = __this->get_titleText_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		Text_t356221433 * L_6 = __this->get_titleText_7();
		Product_t1203687971 * L_7 = V_0;
		NullCheck(L_7);
		ProductMetadata_t1573242544 * L_8 = Product_get_metadata_m263398044(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = ProductMetadata_get_localizedTitle_m1010599344(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
	}

IL_0042:
	{
		Text_t356221433 * L_10 = __this->get_descriptionText_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006b;
		}
	}
	{
		Text_t356221433 * L_12 = __this->get_descriptionText_8();
		Product_t1203687971 * L_13 = V_0;
		NullCheck(L_13);
		ProductMetadata_t1573242544 * L_14 = Product_get_metadata_m263398044(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = ProductMetadata_get_localizedDescription_m3665253400(L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_15);
	}

IL_006b:
	{
		Text_t356221433 * L_16 = __this->get_priceText_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0094;
		}
	}
	{
		Text_t356221433 * L_18 = __this->get_priceText_9();
		Product_t1203687971 * L_19 = V_0;
		NullCheck(L_19);
		ProductMetadata_t1573242544 * L_20 = Product_get_metadata_m263398044(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = ProductMetadata_get_localizedPriceString_m2216629954(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_21);
	}

IL_0094:
	{
	}

IL_0095:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::<Restore>m__0(UnityEngine.Purchasing.RestoreTransactionIDState)
extern "C"  void IAPButton_U3CRestoreU3Em__0_m4194234704 (IAPButton_t3077837360 * __this, int32_t ___restoreTransactionIDState0, const MethodInfo* method)
{
	IAPButton_t3077837360 * G_B2_0 = NULL;
	IAPButton_t3077837360 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	IAPButton_t3077837360 * G_B3_1 = NULL;
	{
		int32_t L_0 = ___restoreTransactionIDState0;
		G_B1_0 = __this;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			G_B2_0 = __this;
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___restoreTransactionIDState0;
		G_B3_0 = ((((int32_t)((((int32_t)L_1) == ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0013:
	{
		NullCheck(G_B3_1);
		IAPButton_OnTransactionsRestored_m3924036197(G_B3_1, (bool)G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::.ctor()
extern Il2CppClass* List_1_t2446958492_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1269839040_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2748203118_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t1423527629_il2cpp_TypeInfo_var;
extern Il2CppClass* IDs_t3808979560_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t763579369_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2241943447_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1288022886_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager__ctor_m3083407869_MetadataUsageId;
extern "C"  void IAPButtonStoreManager__ctor_m3083407869 (IAPButtonStoreManager_t911589174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager__ctor_m3083407869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StandardPurchasingModule_t4003664591 * V_0 = NULL;
	ConfigurationBuilder_t1298400415 * V_1 = NULL;
	ProductCatalogItem_t977711995 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	IDs_t3808979560 * V_4 = NULL;
	StoreID_t471452324 * V_5 = NULL;
	Il2CppObject* V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2446958492 * L_0 = (List_1_t2446958492 *)il2cpp_codegen_object_new(List_1_t2446958492_il2cpp_TypeInfo_var);
		List_1__ctor_m1288022886(L_0, /*hidden argument*/List_1__ctor_m1288022886_MethodInfo_var);
		__this->set_activeButtons_2(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ProductCatalog_t2667590766 * L_1 = ProductCatalog_LoadDefaultCatalog_m589345558(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_catalog_1(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_2 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		StandardPurchasingModule_t4003664591 * L_3 = V_0;
		NullCheck(L_3);
		StandardPurchasingModule_set_useFakeStoreUIMode_m3278247934(L_3, 1, /*hidden argument*/NULL);
		StandardPurchasingModule_t4003664591 * L_4 = V_0;
		ConfigurationBuilder_t1298400415 * L_5 = ConfigurationBuilder_Instance_m4243979412(NULL /*static, unused*/, L_4, ((IPurchasingModuleU5BU5D_t4128245854*)SZArrayNew(IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_1 = L_5;
		ProductCatalog_t2667590766 * L_6 = __this->get_catalog_1();
		NullCheck(L_6);
		Il2CppObject* L_7 = ProductCatalog_get_allProducts_m1511651788(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductCatalogItem>::GetEnumerator() */, IEnumerable_1_t1269839040_il2cpp_TypeInfo_var, L_7);
		V_3 = L_8;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00fc;
		}

IL_004e:
		{
			Il2CppObject* L_9 = V_3;
			NullCheck(L_9);
			ProductCatalogItem_t977711995 * L_10 = InterfaceFuncInvoker0< ProductCatalogItem_t977711995 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductCatalogItem>::get_Current() */, IEnumerator_1_t2748203118_il2cpp_TypeInfo_var, L_9);
			V_2 = L_10;
			ProductCatalogItem_t977711995 * L_11 = V_2;
			NullCheck(L_11);
			Il2CppObject* L_12 = ProductCatalogItem_get_allStoreIDs_m2785218113(L_11, /*hidden argument*/NULL);
			NullCheck(L_12);
			int32_t L_13 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.StoreID>::get_Count() */, ICollection_1_t1423527629_il2cpp_TypeInfo_var, L_12);
			if ((((int32_t)L_13) <= ((int32_t)0)))
			{
				goto IL_00e6;
			}
		}

IL_0067:
		{
			IDs_t3808979560 * L_14 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
			IDs__ctor_m2525416339(L_14, /*hidden argument*/NULL);
			V_4 = L_14;
			ProductCatalogItem_t977711995 * L_15 = V_2;
			NullCheck(L_15);
			Il2CppObject* L_16 = ProductCatalogItem_get_allStoreIDs_m2785218113(L_15, /*hidden argument*/NULL);
			NullCheck(L_16);
			Il2CppObject* L_17 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.StoreID>::GetEnumerator() */, IEnumerable_1_t763579369_il2cpp_TypeInfo_var, L_16);
			V_6 = L_17;
		}

IL_007d:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00ab;
			}

IL_0082:
			{
				Il2CppObject* L_18 = V_6;
				NullCheck(L_18);
				StoreID_t471452324 * L_19 = InterfaceFuncInvoker0< StoreID_t471452324 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.StoreID>::get_Current() */, IEnumerator_1_t2241943447_il2cpp_TypeInfo_var, L_18);
				V_5 = L_19;
				IDs_t3808979560 * L_20 = V_4;
				StoreID_t471452324 * L_21 = V_5;
				NullCheck(L_21);
				String_t* L_22 = L_21->get_id_1();
				StringU5BU5D_t1642385972* L_23 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
				StoreID_t471452324 * L_24 = V_5;
				NullCheck(L_24);
				String_t* L_25 = L_24->get_store_0();
				NullCheck(L_23);
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				NullCheck(L_20);
				IDs_Add_m217445704(L_20, L_22, L_23, /*hidden argument*/NULL);
			}

IL_00ab:
			{
				Il2CppObject* L_26 = V_6;
				NullCheck(L_26);
				bool L_27 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_26);
				if (L_27)
				{
					goto IL_0082;
				}
			}

IL_00b7:
			{
				IL2CPP_LEAVE(0xCB, FINALLY_00bc);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_00bc;
		}

FINALLY_00bc:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_28 = V_6;
				if (!L_28)
				{
					goto IL_00ca;
				}
			}

IL_00c3:
			{
				Il2CppObject* L_29 = V_6;
				NullCheck(L_29);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_29);
			}

IL_00ca:
			{
				IL2CPP_END_FINALLY(188)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(188)
		{
			IL2CPP_JUMP_TBL(0xCB, IL_00cb)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_00cb:
		{
			ConfigurationBuilder_t1298400415 * L_30 = V_1;
			ProductCatalogItem_t977711995 * L_31 = V_2;
			NullCheck(L_31);
			String_t* L_32 = L_31->get_id_0();
			ProductCatalogItem_t977711995 * L_33 = V_2;
			NullCheck(L_33);
			int32_t L_34 = L_33->get_type_1();
			IDs_t3808979560 * L_35 = V_4;
			NullCheck(L_30);
			ConfigurationBuilder_AddProduct_m918244722(L_30, L_32, L_34, L_35, /*hidden argument*/NULL);
			goto IL_00fb;
		}

IL_00e6:
		{
			ConfigurationBuilder_t1298400415 * L_36 = V_1;
			ProductCatalogItem_t977711995 * L_37 = V_2;
			NullCheck(L_37);
			String_t* L_38 = L_37->get_id_0();
			ProductCatalogItem_t977711995 * L_39 = V_2;
			NullCheck(L_39);
			int32_t L_40 = L_39->get_type_1();
			NullCheck(L_36);
			ConfigurationBuilder_AddProduct_m3779153393(L_36, L_38, L_40, /*hidden argument*/NULL);
		}

IL_00fb:
		{
		}

IL_00fc:
		{
			Il2CppObject* L_41 = V_3;
			NullCheck(L_41);
			bool L_42 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_41);
			if (L_42)
			{
				goto IL_004e;
			}
		}

IL_0107:
		{
			IL2CPP_LEAVE(0x119, FINALLY_010c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_010c;
	}

FINALLY_010c:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_43 = V_3;
			if (!L_43)
			{
				goto IL_0118;
			}
		}

IL_0112:
		{
			Il2CppObject* L_44 = V_3;
			NullCheck(L_44);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_44);
		}

IL_0118:
		{
			IL2CPP_END_FINALLY(268)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(268)
	{
		IL2CPP_JUMP_TBL(0x119, IL_0119)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0119:
	{
		ConfigurationBuilder_t1298400415 * L_45 = V_1;
		UnityPurchasing_Initialize_m1012234273(NULL /*static, unused*/, __this, L_45, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::get_Instance()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager_get_Instance_m3910054168_MetadataUsageId;
extern "C"  IAPButtonStoreManager_t911589174 * IAPButtonStoreManager_get_Instance_m3910054168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_get_Instance_m3910054168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IAPButtonStoreManager_t911589174 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_0 = ((IAPButtonStoreManager_t911589174_StaticFields*)IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var->static_fields)->get_instance_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		IAPButtonStoreManager_t911589174 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Purchasing.IStoreController UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::get_StoreController()
extern "C"  Il2CppObject * IAPButtonStoreManager_get_StoreController_m527834269 (IAPButtonStoreManager_t911589174 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_controller_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Purchasing.IExtensionProvider UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::get_ExtensionProvider()
extern "C"  Il2CppObject * IAPButtonStoreManager_get_ExtensionProvider_m473841661 (IAPButtonStoreManager_t911589174 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_extensions_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::HasProductInCatalog(System.String)
extern Il2CppClass* IEnumerable_1_t1269839040_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2748203118_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager_HasProductInCatalog_m3023623296_MetadataUsageId;
extern "C"  bool IAPButtonStoreManager_HasProductInCatalog_m3023623296 (IAPButtonStoreManager_t911589174 * __this, String_t* ___productID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_HasProductInCatalog_m3023623296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ProductCatalogItem_t977711995 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ProductCatalog_t2667590766 * L_0 = __this->get_catalog_1();
		NullCheck(L_0);
		Il2CppObject* L_1 = ProductCatalog_get_allProducts_m1511651788(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductCatalogItem>::GetEnumerator() */, IEnumerable_1_t1269839040_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003a;
		}

IL_0018:
		{
			Il2CppObject* L_3 = V_1;
			NullCheck(L_3);
			ProductCatalogItem_t977711995 * L_4 = InterfaceFuncInvoker0< ProductCatalogItem_t977711995 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductCatalogItem>::get_Current() */, IEnumerator_1_t2748203118_il2cpp_TypeInfo_var, L_3);
			V_0 = L_4;
			ProductCatalogItem_t977711995 * L_5 = V_0;
			NullCheck(L_5);
			String_t* L_6 = L_5->get_id_0();
			String_t* L_7 = ___productID0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0039;
			}
		}

IL_0031:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x5E, FINALLY_004a);
		}

IL_0039:
		{
		}

IL_003a:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0018;
			}
		}

IL_0045:
		{
			IL2CPP_LEAVE(0x57, FINALLY_004a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_11 = V_1;
			if (!L_11)
			{
				goto IL_0056;
			}
		}

IL_0050:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_12);
		}

IL_0056:
		{
			IL2CPP_END_FINALLY(74)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0057:
	{
		V_2 = (bool)0;
		goto IL_005e;
	}

IL_005e:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// UnityEngine.Purchasing.Product UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::GetProduct(System.String)
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager_GetProduct_m1505647265_MetadataUsageId;
extern "C"  Product_t1203687971 * IAPButtonStoreManager_GetProduct_m1505647265 (IAPButtonStoreManager_t911589174 * __this, String_t* ___productID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_GetProduct_m1505647265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_controller_3();
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_controller_3();
		NullCheck(L_1);
		ProductCollection_t3600019299 * L_2 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_1);
		String_t* L_3 = ___productID0;
		NullCheck(L_2);
		Product_t1203687971 * L_4 = ProductCollection_WithID_m3999574440(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002b;
	}

IL_0024:
	{
		V_0 = (Product_t1203687971 *)NULL;
		goto IL_002b;
	}

IL_002b:
	{
		Product_t1203687971 * L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::AddButton(UnityEngine.Purchasing.IAPButton)
extern const MethodInfo* List_1_Add_m3151945002_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager_AddButton_m3524258615_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_AddButton_m3524258615 (IAPButtonStoreManager_t911589174 * __this, IAPButton_t3077837360 * ___button0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_AddButton_m3524258615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2446958492 * L_0 = __this->get_activeButtons_2();
		IAPButton_t3077837360 * L_1 = ___button0;
		NullCheck(L_0);
		List_1_Add_m3151945002(L_0, L_1, /*hidden argument*/List_1_Add_m3151945002_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::RemoveButton(UnityEngine.Purchasing.IAPButton)
extern const MethodInfo* List_1_Remove_m4067983529_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager_RemoveButton_m2931652138_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_RemoveButton_m2931652138 (IAPButtonStoreManager_t911589174 * __this, IAPButton_t3077837360 * ___button0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_RemoveButton_m2931652138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2446958492 * L_0 = __this->get_activeButtons_2();
		IAPButton_t3077837360 * L_1 = ___button0;
		NullCheck(L_0);
		List_1_Remove_m4067983529(L_0, L_1, /*hidden argument*/List_1_Remove_m4067983529_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::InitiatePurchase(System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral606075875;
extern const uint32_t IAPButtonStoreManager_InitiatePurchase_m374565683_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_InitiatePurchase_m374565683 (IAPButtonStoreManager_t911589174 * __this, String_t* ___productID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_InitiatePurchase_m374565683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_controller_3();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral606075875, /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001c:
	{
		Il2CppObject * L_1 = __this->get_controller_3();
		String_t* L_2 = ___productID0;
		NullCheck(L_1);
		InterfaceActionInvoker1< String_t* >::Invoke(3 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(System.String) */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_1, L_2);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern const MethodInfo* List_1_GetEnumerator_m1699546439_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3656253947_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m708814495_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1115631095_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager_OnInitialized_m3699105403_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_OnInitialized_m3699105403 (IAPButtonStoreManager_t911589174 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_OnInitialized_m3699105403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IAPButton_t3077837360 * V_0 = NULL;
	Enumerator_t1981688166  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___controller0;
		__this->set_controller_3(L_0);
		Il2CppObject * L_1 = ___extensions1;
		__this->set_extensions_4(L_1);
		List_1_t2446958492 * L_2 = __this->get_activeButtons_2();
		NullCheck(L_2);
		Enumerator_t1981688166  L_3 = List_1_GetEnumerator_m1699546439(L_2, /*hidden argument*/List_1_GetEnumerator_m1699546439_MethodInfo_var);
		V_1 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_0021:
		{
			IAPButton_t3077837360 * L_4 = Enumerator_get_Current_m3656253947((&V_1), /*hidden argument*/Enumerator_get_Current_m3656253947_MethodInfo_var);
			V_0 = L_4;
			IAPButton_t3077837360 * L_5 = V_0;
			NullCheck(L_5);
			IAPButton_UpdateText_m1500550460(L_5, /*hidden argument*/NULL);
		}

IL_0031:
		{
			bool L_6 = Enumerator_MoveNext_m708814495((&V_1), /*hidden argument*/Enumerator_MoveNext_m708814495_MethodInfo_var);
			if (L_6)
			{
				goto IL_0021;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1115631095((&V_1), /*hidden argument*/Enumerator_Dispose_m1115631095_MethodInfo_var);
		IL2CPP_END_FINALLY(66)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4122218072;
extern const uint32_t IAPButtonStoreManager_OnInitializeFailed_m2396353392_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_OnInitializeFailed_m2396353392 (IAPButtonStoreManager_t911589174 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_OnInitializeFailed_m2396353392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, (&___error0));
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral4122218072, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1699546439_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3656253947_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m708814495_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1115631095_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager_ProcessPurchase_m2373817739_MetadataUsageId;
extern "C"  int32_t IAPButtonStoreManager_ProcessPurchase_m2373817739 (IAPButtonStoreManager_t911589174 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_ProcessPurchase_m2373817739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IAPButton_t3077837360 * V_0 = NULL;
	Enumerator_t1981688166  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2446958492 * L_0 = __this->get_activeButtons_2();
		NullCheck(L_0);
		Enumerator_t1981688166  L_1 = List_1_GetEnumerator_m1699546439(L_0, /*hidden argument*/List_1_GetEnumerator_m1699546439_MethodInfo_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004b;
		}

IL_0013:
		{
			IAPButton_t3077837360 * L_2 = Enumerator_get_Current_m3656253947((&V_1), /*hidden argument*/Enumerator_get_Current_m3656253947_MethodInfo_var);
			V_0 = L_2;
			IAPButton_t3077837360 * L_3 = V_0;
			NullCheck(L_3);
			String_t* L_4 = L_3->get_productId_2();
			PurchaseEventArgs_t547992434 * L_5 = ___e0;
			NullCheck(L_5);
			Product_t1203687971 * L_6 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			ProductDefinition_t1942475268 * L_7 = Product_get_definition_m2035415516(L_6, /*hidden argument*/NULL);
			NullCheck(L_7);
			String_t* L_8 = ProductDefinition_get_id_m264072292(L_7, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_004a;
			}
		}

IL_003c:
		{
			IAPButton_t3077837360 * L_10 = V_0;
			PurchaseEventArgs_t547992434 * L_11 = ___e0;
			NullCheck(L_10);
			int32_t L_12 = IAPButton_ProcessPurchase_m705789944(L_10, L_11, /*hidden argument*/NULL);
			V_2 = L_12;
			IL2CPP_LEAVE(0x71, FINALLY_005c);
		}

IL_004a:
		{
		}

IL_004b:
		{
			bool L_13 = Enumerator_MoveNext_m708814495((&V_1), /*hidden argument*/Enumerator_MoveNext_m708814495_MethodInfo_var);
			if (L_13)
			{
				goto IL_0013;
			}
		}

IL_0057:
		{
			IL2CPP_LEAVE(0x6A, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1115631095((&V_1), /*hidden argument*/Enumerator_Dispose_m1115631095_MethodInfo_var);
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006a:
	{
		V_2 = 0;
		goto IL_0071;
	}

IL_0071:
	{
		int32_t L_14 = V_2;
		return L_14;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1699546439_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3656253947_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m708814495_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1115631095_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager_OnPurchaseFailed_m2419505534_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_OnPurchaseFailed_m2419505534 (IAPButtonStoreManager_t911589174 * __this, Product_t1203687971 * ___product0, int32_t ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_OnPurchaseFailed_m2419505534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IAPButton_t3077837360 * V_0 = NULL;
	Enumerator_t1981688166  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2446958492 * L_0 = __this->get_activeButtons_2();
		NullCheck(L_0);
		Enumerator_t1981688166  L_1 = List_1_GetEnumerator_m1699546439(L_0, /*hidden argument*/List_1_GetEnumerator_m1699546439_MethodInfo_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0013:
		{
			IAPButton_t3077837360 * L_2 = Enumerator_get_Current_m3656253947((&V_1), /*hidden argument*/Enumerator_get_Current_m3656253947_MethodInfo_var);
			V_0 = L_2;
			IAPButton_t3077837360 * L_3 = V_0;
			NullCheck(L_3);
			String_t* L_4 = L_3->get_productId_2();
			Product_t1203687971 * L_5 = ___product0;
			NullCheck(L_5);
			ProductDefinition_t1942475268 * L_6 = Product_get_definition_m2035415516(L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			String_t* L_7 = ProductDefinition_get_id_m264072292(L_6, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_0037:
		{
			IAPButton_t3077837360 * L_9 = V_0;
			Product_t1203687971 * L_10 = ___product0;
			int32_t L_11 = ___reason1;
			NullCheck(L_9);
			IAPButton_OnPurchaseFailed_m2148672501(L_9, L_10, L_11, /*hidden argument*/NULL);
		}

IL_0041:
		{
		}

IL_0042:
		{
			bool L_12 = Enumerator_MoveNext_m708814495((&V_1), /*hidden argument*/Enumerator_MoveNext_m708814495_MethodInfo_var);
			if (L_12)
			{
				goto IL_0013;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1115631095((&V_1), /*hidden argument*/Enumerator_Dispose_m1115631095_MethodInfo_var);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0061:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::.cctor()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager__cctor_m2118049118_MetadataUsageId;
extern "C"  void IAPButtonStoreManager__cctor_m2118049118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager__cctor_m2118049118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IAPButtonStoreManager_t911589174 * L_0 = (IAPButtonStoreManager_t911589174 *)il2cpp_codegen_object_new(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager__ctor_m3083407869(L_0, /*hidden argument*/NULL);
		((IAPButtonStoreManager_t911589174_StaticFields*)IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var->static_fields)->set_instance_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent::.ctor()
extern const MethodInfo* UnityEvent_1__ctor_m4041951455_MethodInfo_var;
extern const uint32_t OnPurchaseCompletedEvent__ctor_m3087740950_MetadataUsageId;
extern "C"  void OnPurchaseCompletedEvent__ctor_m3087740950 (OnPurchaseCompletedEvent_t4018783659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPurchaseCompletedEvent__ctor_m3087740950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m4041951455(__this, /*hidden argument*/UnityEvent_1__ctor_m4041951455_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent::.ctor()
extern const MethodInfo* UnityEvent_2__ctor_m2157306554_MethodInfo_var;
extern const uint32_t OnPurchaseFailedEvent__ctor_m1226889690_MetadataUsageId;
extern "C"  void OnPurchaseFailedEvent__ctor_m1226889690 (OnPurchaseFailedEvent_t2813769101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPurchaseFailedEvent__ctor_m1226889690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_2__ctor_m2157306554(__this, /*hidden argument*/UnityEvent_2__ctor_m2157306554_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleTangle::.ctor()
extern "C"  void AppleTangle__ctor_m1226795583 (AppleTangle_t53875121 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.AppleTangle::Data()
extern Il2CppClass* AppleTangle_t53875121_il2cpp_TypeInfo_var;
extern const uint32_t AppleTangle_Data_m3366264679_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* AppleTangle_Data_m3366264679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleTangle_Data_m3366264679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppleTangle_t53875121_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_data_0();
		Int32U5BU5D_t3030399641* L_1 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_order_1();
		int32_t L_2 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_key_2();
		ByteU5BU5D_t3397334013* L_3 = Obfuscator_DeObfuscate_m4253116256(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001b;
	}

IL_001b:
	{
		ByteU5BU5D_t3397334013* L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleTangle::.cctor()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* AppleTangle_t53875121_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral3336953437;
extern const uint32_t AppleTangle__cctor_m3030347248_MetadataUsageId;
extern "C"  void AppleTangle__cctor_m3030347248 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleTangle__cctor_m3030347248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, _stringLiteral3336953437, /*hidden argument*/NULL);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_data_0(L_0);
		Int32U5BU5D_t3030399641* L_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)61)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0_FieldInfo_var), /*hidden argument*/NULL);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_order_1(L_1);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_key_2(((int32_t)55));
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.ctor()
extern "C"  void GooglePlayTangle__ctor_m1761951308 (GooglePlayTangle_t2749524914 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.GooglePlayTangle::Data()
extern Il2CppClass* GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var;
extern const uint32_t GooglePlayTangle_Data_m2557246410_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* GooglePlayTangle_Data_m2557246410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GooglePlayTangle_Data_m2557246410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_data_0();
		Int32U5BU5D_t3030399641* L_1 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_order_1();
		int32_t L_2 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_key_2();
		ByteU5BU5D_t3397334013* L_3 = Obfuscator_DeObfuscate_m4253116256(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001b;
	}

IL_001b:
	{
		ByteU5BU5D_t3397334013* L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.cctor()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral3385890250;
extern const uint32_t GooglePlayTangle__cctor_m4030200525_MetadataUsageId;
extern "C"  void GooglePlayTangle__cctor_m4030200525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GooglePlayTangle__cctor_m4030200525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, _stringLiteral3385890250, /*hidden argument*/NULL);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_data_0(L_0);
		Int32U5BU5D_t3030399641* L_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)15)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1_FieldInfo_var), /*hidden argument*/NULL);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_order_1(L_1);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_key_2(((int32_t)127));
		return;
	}
}
// System.Void ZipUtil::.ctor()
extern "C"  void ZipUtil__ctor_m3230271356 (ZipUtil_t2998729745 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL unzip(char*, char*);
// System.Void ZipUtil::unzip(System.String,System.String)
extern "C"  void ZipUtil_unzip_m397363156 (Il2CppObject * __this /* static, unused */, String_t* ___zipFilePath0, String_t* ___location1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___zipFilePath0' to native representation
	char* ____zipFilePath0_marshaled = NULL;
	____zipFilePath0_marshaled = il2cpp_codegen_marshal_string(___zipFilePath0);

	// Marshaling of parameter '___location1' to native representation
	char* ____location1_marshaled = NULL;
	____location1_marshaled = il2cpp_codegen_marshal_string(___location1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(unzip)(____zipFilePath0_marshaled, ____location1_marshaled);

	// Marshaling cleanup of parameter '___zipFilePath0' native representation
	il2cpp_codegen_marshal_free(____zipFilePath0_marshaled);
	____zipFilePath0_marshaled = NULL;

	// Marshaling cleanup of parameter '___location1' native representation
	il2cpp_codegen_marshal_free(____location1_marshaled);
	____location1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL zip(char*);
// System.Void ZipUtil::zip(System.String)
extern "C"  void ZipUtil_zip_m1670743627 (Il2CppObject * __this /* static, unused */, String_t* ___zipFilePath0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___zipFilePath0' to native representation
	char* ____zipFilePath0_marshaled = NULL;
	____zipFilePath0_marshaled = il2cpp_codegen_marshal_string(___zipFilePath0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(zip)(____zipFilePath0_marshaled);

	// Marshaling cleanup of parameter '___zipFilePath0' native representation
	il2cpp_codegen_marshal_free(____zipFilePath0_marshaled);
	____zipFilePath0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL addZipFile(char*);
// System.Void ZipUtil::addZipFile(System.String)
extern "C"  void ZipUtil_addZipFile_m3783422008 (Il2CppObject * __this /* static, unused */, String_t* ___addFile0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___addFile0' to native representation
	char* ____addFile0_marshaled = NULL;
	____addFile0_marshaled = il2cpp_codegen_marshal_string(___addFile0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(addZipFile)(____addFile0_marshaled);

	// Marshaling cleanup of parameter '___addFile0' native representation
	il2cpp_codegen_marshal_free(____addFile0_marshaled);
	____addFile0_marshaled = NULL;

}
// System.Void ZipUtil::Unzip(System.String,System.String)
extern "C"  void ZipUtil_Unzip_m775988660 (Il2CppObject * __this /* static, unused */, String_t* ___zipFilePath0, String_t* ___location1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___zipFilePath0;
		String_t* L_1 = ___location1;
		ZipUtil_unzip_m397363156(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZipUtil::Zip(System.String,System.String[])
extern "C"  void ZipUtil_Zip_m2650750703 (Il2CppObject * __this /* static, unused */, String_t* ___zipFileName0, StringU5BU5D_t1642385972* ___files1, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringU5BU5D_t1642385972* L_0 = ___files1;
		V_1 = L_0;
		V_2 = 0;
		goto IL_001b;
	}

IL_000b:
	{
		StringU5BU5D_t1642385972* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		String_t* L_5 = V_0;
		ZipUtil_addZipFile_m3783422008(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001b:
	{
		int32_t L_7 = V_2;
		StringU5BU5D_t1642385972* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		String_t* L_9 = ___zipFileName0;
		ZipUtil_zip_m1670743627(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
