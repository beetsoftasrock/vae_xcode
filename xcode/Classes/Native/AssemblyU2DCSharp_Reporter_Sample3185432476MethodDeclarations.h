﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reporter/Sample
struct Sample_t3185432476;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Reporter/Sample::.ctor()
extern "C"  void Sample__ctor_m3461062767 (Sample_t3185432476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Reporter/Sample::MemSize()
extern "C"  float Sample_MemSize_m133922829 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Reporter/Sample::GetSceneName()
extern "C"  String_t* Sample_GetSceneName_m390914921 (Sample_t3185432476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
