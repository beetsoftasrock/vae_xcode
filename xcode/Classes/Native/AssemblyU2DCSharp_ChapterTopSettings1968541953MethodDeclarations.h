﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopSettings
struct ChapterTopSettings_t1968541953;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterTopSettings::.ctor()
extern "C"  void ChapterTopSettings__ctor_m949549952 (ChapterTopSettings_t1968541953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
