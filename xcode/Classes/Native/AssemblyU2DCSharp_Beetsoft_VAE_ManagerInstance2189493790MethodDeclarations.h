﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.ManagerInstance
struct ManagerInstance_t2189493790;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// Beetsoft.VAE.SoundResult
struct SoundResult_t4177104648;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_SoundResult4177104648.h"

// System.Void Beetsoft.VAE.ManagerInstance::.ctor()
extern "C"  void ManagerInstance__ctor_m1925718115 (ManagerInstance_t2189493790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.ManagerInstance::IsPlaying(UnityEngine.AudioClip)
extern "C"  bool ManagerInstance_IsPlaying_m330183800 (ManagerInstance_t2189493790 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Beetsoft.VAE.SoundResult Beetsoft.VAE.ManagerInstance::Play(UnityEngine.AudioClip,System.Boolean,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  SoundResult_t4177104648 * ManagerInstance_Play_m837425661 (ManagerInstance_t2189493790 * __this, AudioClip_t1932558630 * ___clip0, bool ___loop1, Vector3_t2243707580  ___position2, float ___delay3, float ___volume4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.ManagerInstance::Stop(UnityEngine.AudioClip,System.Single)
extern "C"  void ManagerInstance_Stop_m2796061045 (ManagerInstance_t2189493790 * __this, AudioClip_t1932558630 * ___audioClip0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Beetsoft.VAE.ManagerInstance::trackingPlay(UnityEngine.AudioSource,Beetsoft.VAE.SoundResult)
extern "C"  Il2CppObject * ManagerInstance_trackingPlay_m2703806080 (ManagerInstance_t2189493790 * __this, AudioSource_t1135106623 * ___targetSource0, SoundResult_t4177104648 * ___soundResult1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Beetsoft.VAE.ManagerInstance::stopDelayed(UnityEngine.AudioClip,System.Single)
extern "C"  Il2CppObject * ManagerInstance_stopDelayed_m3927730823 (ManagerInstance_t2189493790 * __this, AudioClip_t1932558630 * ___audioClip0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.ManagerInstance::<Play>m__0(UnityEngine.AudioSource)
extern "C"  bool ManagerInstance_U3CPlayU3Em__0_m2947603618 (Il2CppObject * __this /* static, unused */, AudioSource_t1135106623 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
