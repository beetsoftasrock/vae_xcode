﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VocabularyController
struct VocabularyController_t843712768;
// System.String
struct String_t;
// SelectItem
struct SelectItem_t2844432199;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_SelectItem2844432199.h"

// System.Void VocabularyController::.ctor()
extern "C"  void VocabularyController__ctor_m2095714889 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::Awake()
extern "C"  void VocabularyController_Awake_m212456060 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::Start()
extern "C"  void VocabularyController_Start_m1022706401 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::Update()
extern "C"  void VocabularyController_Update_m4007787402 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::Play(System.String)
extern "C"  void VocabularyController_Play_m1367170687 (VocabularyController_t843712768 * __this, String_t* ___groupName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::OnSubmit()
extern "C"  void VocabularyController_OnSubmit_m2158483298 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::SetPopupData(System.Boolean,System.Int32)
extern "C"  void VocabularyController_SetPopupData_m3206640547 (VocabularyController_t843712768 * __this, bool ___result0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::Next()
extern "C"  void VocabularyController_Next_m205291074 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::LoadPopup()
extern "C"  void VocabularyController_LoadPopup_m141771261 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::SetData()
extern "C"  void VocabularyController_SetData_m3419577099 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::PlayVoice()
extern "C"  void VocabularyController_PlayVoice_m4237851179 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::OnSelectItem(SelectItem)
extern "C"  void VocabularyController_OnSelectItem_m1026334384 (VocabularyController_t843712768 * __this, SelectItem_t2844432199 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::reshuffle(System.String[])
extern "C"  void VocabularyController_reshuffle_m2881479337 (VocabularyController_t843712768 * __this, StringU5BU5D_t1642385972* ___texts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::Shuffle(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void VocabularyController_Shuffle_m5661782 (Il2CppObject * __this /* static, unused */, List_1_t3492939606 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyController::OnBack()
extern "C"  void VocabularyController_OnBack_m3087931699 (VocabularyController_t843712768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
