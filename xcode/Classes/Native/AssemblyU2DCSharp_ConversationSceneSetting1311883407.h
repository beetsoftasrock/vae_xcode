﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// CommandSheet
struct CommandSheet_t2769384292;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSceneSetting
struct  ConversationSceneSetting_t1311883407  : public Il2CppObject
{
public:
	// UnityEngine.Sprite ConversationSceneSetting::backgroundSprite
	Sprite_t309593783 * ___backgroundSprite_0;
	// CommandSheet ConversationSceneSetting::commandSheet
	CommandSheet_t2769384292 * ___commandSheet_1;
	// System.String ConversationSceneSetting::audioResourcePath
	String_t* ___audioResourcePath_2;
	// System.String ConversationSceneSetting::environmentPath
	String_t* ___environmentPath_3;

public:
	inline static int32_t get_offset_of_backgroundSprite_0() { return static_cast<int32_t>(offsetof(ConversationSceneSetting_t1311883407, ___backgroundSprite_0)); }
	inline Sprite_t309593783 * get_backgroundSprite_0() const { return ___backgroundSprite_0; }
	inline Sprite_t309593783 ** get_address_of_backgroundSprite_0() { return &___backgroundSprite_0; }
	inline void set_backgroundSprite_0(Sprite_t309593783 * value)
	{
		___backgroundSprite_0 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_0, value);
	}

	inline static int32_t get_offset_of_commandSheet_1() { return static_cast<int32_t>(offsetof(ConversationSceneSetting_t1311883407, ___commandSheet_1)); }
	inline CommandSheet_t2769384292 * get_commandSheet_1() const { return ___commandSheet_1; }
	inline CommandSheet_t2769384292 ** get_address_of_commandSheet_1() { return &___commandSheet_1; }
	inline void set_commandSheet_1(CommandSheet_t2769384292 * value)
	{
		___commandSheet_1 = value;
		Il2CppCodeGenWriteBarrier(&___commandSheet_1, value);
	}

	inline static int32_t get_offset_of_audioResourcePath_2() { return static_cast<int32_t>(offsetof(ConversationSceneSetting_t1311883407, ___audioResourcePath_2)); }
	inline String_t* get_audioResourcePath_2() const { return ___audioResourcePath_2; }
	inline String_t** get_address_of_audioResourcePath_2() { return &___audioResourcePath_2; }
	inline void set_audioResourcePath_2(String_t* value)
	{
		___audioResourcePath_2 = value;
		Il2CppCodeGenWriteBarrier(&___audioResourcePath_2, value);
	}

	inline static int32_t get_offset_of_environmentPath_3() { return static_cast<int32_t>(offsetof(ConversationSceneSetting_t1311883407, ___environmentPath_3)); }
	inline String_t* get_environmentPath_3() const { return ___environmentPath_3; }
	inline String_t** get_address_of_environmentPath_3() { return &___environmentPath_3; }
	inline void set_environmentPath_3(String_t* value)
	{
		___environmentPath_3 = value;
		Il2CppCodeGenWriteBarrier(&___environmentPath_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
