﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CommandSheet
struct CommandSheet_t2769384292;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;

#include "AssemblyU2DCSharp_CommandLoader2460237222.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseConversationCommandLoader
struct  BaseConversationCommandLoader_t1091304408  : public CommandLoader_t2460237222
{
public:
	// CommandSheet BaseConversationCommandLoader::dataSheet
	CommandSheet_t2769384292 * ___dataSheet_2;
	// System.Collections.Generic.List`1<CommandSheet/Param> BaseConversationCommandLoader::levelSheet
	List_1_t3492939606 * ___levelSheet_3;
	// System.Int32 BaseConversationCommandLoader::currentPoint
	int32_t ___currentPoint_4;

public:
	inline static int32_t get_offset_of_dataSheet_2() { return static_cast<int32_t>(offsetof(BaseConversationCommandLoader_t1091304408, ___dataSheet_2)); }
	inline CommandSheet_t2769384292 * get_dataSheet_2() const { return ___dataSheet_2; }
	inline CommandSheet_t2769384292 ** get_address_of_dataSheet_2() { return &___dataSheet_2; }
	inline void set_dataSheet_2(CommandSheet_t2769384292 * value)
	{
		___dataSheet_2 = value;
		Il2CppCodeGenWriteBarrier(&___dataSheet_2, value);
	}

	inline static int32_t get_offset_of_levelSheet_3() { return static_cast<int32_t>(offsetof(BaseConversationCommandLoader_t1091304408, ___levelSheet_3)); }
	inline List_1_t3492939606 * get_levelSheet_3() const { return ___levelSheet_3; }
	inline List_1_t3492939606 ** get_address_of_levelSheet_3() { return &___levelSheet_3; }
	inline void set_levelSheet_3(List_1_t3492939606 * value)
	{
		___levelSheet_3 = value;
		Il2CppCodeGenWriteBarrier(&___levelSheet_3, value);
	}

	inline static int32_t get_offset_of_currentPoint_4() { return static_cast<int32_t>(offsetof(BaseConversationCommandLoader_t1091304408, ___currentPoint_4)); }
	inline int32_t get_currentPoint_4() const { return ___currentPoint_4; }
	inline int32_t* get_address_of_currentPoint_4() { return &___currentPoint_4; }
	inline void set_currentPoint_4(int32_t value)
	{
		___currentPoint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
