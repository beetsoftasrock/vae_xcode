﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KaraokeTextEffect
struct  KaraokeTextEffect_t1279919556  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color KaraokeTextEffect::colorKaraoke
	Color_t2020392075  ___colorKaraoke_2;
	// UnityEngine.UI.Text KaraokeTextEffect::_effectText
	Text_t356221433 * ____effectText_3;
	// UnityEngine.AudioSource KaraokeTextEffect::audioSource
	AudioSource_t1135106623 * ___audioSource_4;
	// System.Single KaraokeTextEffect::delayStart
	float ___delayStart_5;

public:
	inline static int32_t get_offset_of_colorKaraoke_2() { return static_cast<int32_t>(offsetof(KaraokeTextEffect_t1279919556, ___colorKaraoke_2)); }
	inline Color_t2020392075  get_colorKaraoke_2() const { return ___colorKaraoke_2; }
	inline Color_t2020392075 * get_address_of_colorKaraoke_2() { return &___colorKaraoke_2; }
	inline void set_colorKaraoke_2(Color_t2020392075  value)
	{
		___colorKaraoke_2 = value;
	}

	inline static int32_t get_offset_of__effectText_3() { return static_cast<int32_t>(offsetof(KaraokeTextEffect_t1279919556, ____effectText_3)); }
	inline Text_t356221433 * get__effectText_3() const { return ____effectText_3; }
	inline Text_t356221433 ** get_address_of__effectText_3() { return &____effectText_3; }
	inline void set__effectText_3(Text_t356221433 * value)
	{
		____effectText_3 = value;
		Il2CppCodeGenWriteBarrier(&____effectText_3, value);
	}

	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(KaraokeTextEffect_t1279919556, ___audioSource_4)); }
	inline AudioSource_t1135106623 * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t1135106623 * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_4, value);
	}

	inline static int32_t get_offset_of_delayStart_5() { return static_cast<int32_t>(offsetof(KaraokeTextEffect_t1279919556, ___delayStart_5)); }
	inline float get_delayStart_5() const { return ___delayStart_5; }
	inline float* get_address_of_delayStart_5() { return &___delayStart_5; }
	inline void set_delayStart_5(float value)
	{
		___delayStart_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
