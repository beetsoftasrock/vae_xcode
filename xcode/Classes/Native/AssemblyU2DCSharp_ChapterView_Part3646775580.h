﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterView/Part
struct  Part_t3646775580 
{
public:
	// UnityEngine.UI.Text ChapterView/Part::txtNameLesson
	Text_t356221433 * ___txtNameLesson_0;
	// UnityEngine.UI.Text ChapterView/Part::txtPercent
	Text_t356221433 * ___txtPercent_1;
	// UnityEngine.UI.Button ChapterView/Part::btnStudy
	Button_t2872111280 * ___btnStudy_2;
	// UnityEngine.UI.Button ChapterView/Part::btnAuto
	Button_t2872111280 * ___btnAuto_3;
	// UnityEngine.UI.Slider ChapterView/Part::slider
	Slider_t297367283 * ___slider_4;

public:
	inline static int32_t get_offset_of_txtNameLesson_0() { return static_cast<int32_t>(offsetof(Part_t3646775580, ___txtNameLesson_0)); }
	inline Text_t356221433 * get_txtNameLesson_0() const { return ___txtNameLesson_0; }
	inline Text_t356221433 ** get_address_of_txtNameLesson_0() { return &___txtNameLesson_0; }
	inline void set_txtNameLesson_0(Text_t356221433 * value)
	{
		___txtNameLesson_0 = value;
		Il2CppCodeGenWriteBarrier(&___txtNameLesson_0, value);
	}

	inline static int32_t get_offset_of_txtPercent_1() { return static_cast<int32_t>(offsetof(Part_t3646775580, ___txtPercent_1)); }
	inline Text_t356221433 * get_txtPercent_1() const { return ___txtPercent_1; }
	inline Text_t356221433 ** get_address_of_txtPercent_1() { return &___txtPercent_1; }
	inline void set_txtPercent_1(Text_t356221433 * value)
	{
		___txtPercent_1 = value;
		Il2CppCodeGenWriteBarrier(&___txtPercent_1, value);
	}

	inline static int32_t get_offset_of_btnStudy_2() { return static_cast<int32_t>(offsetof(Part_t3646775580, ___btnStudy_2)); }
	inline Button_t2872111280 * get_btnStudy_2() const { return ___btnStudy_2; }
	inline Button_t2872111280 ** get_address_of_btnStudy_2() { return &___btnStudy_2; }
	inline void set_btnStudy_2(Button_t2872111280 * value)
	{
		___btnStudy_2 = value;
		Il2CppCodeGenWriteBarrier(&___btnStudy_2, value);
	}

	inline static int32_t get_offset_of_btnAuto_3() { return static_cast<int32_t>(offsetof(Part_t3646775580, ___btnAuto_3)); }
	inline Button_t2872111280 * get_btnAuto_3() const { return ___btnAuto_3; }
	inline Button_t2872111280 ** get_address_of_btnAuto_3() { return &___btnAuto_3; }
	inline void set_btnAuto_3(Button_t2872111280 * value)
	{
		___btnAuto_3 = value;
		Il2CppCodeGenWriteBarrier(&___btnAuto_3, value);
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(Part_t3646775580, ___slider_4)); }
	inline Slider_t297367283 * get_slider_4() const { return ___slider_4; }
	inline Slider_t297367283 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t297367283 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier(&___slider_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ChapterView/Part
struct Part_t3646775580_marshaled_pinvoke
{
	Text_t356221433 * ___txtNameLesson_0;
	Text_t356221433 * ___txtPercent_1;
	Button_t2872111280 * ___btnStudy_2;
	Button_t2872111280 * ___btnAuto_3;
	Slider_t297367283 * ___slider_4;
};
// Native definition for COM marshalling of ChapterView/Part
struct Part_t3646775580_marshaled_com
{
	Text_t356221433 * ___txtNameLesson_0;
	Text_t356221433 * ___txtPercent_1;
	Button_t2872111280 * ___btnStudy_2;
	Button_t2872111280 * ___btnAuto_3;
	Slider_t297367283 * ___slider_4;
};
