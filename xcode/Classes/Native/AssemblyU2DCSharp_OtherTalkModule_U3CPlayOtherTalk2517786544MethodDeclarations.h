﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OtherTalkModule/<PlayOtherTalkEffect>c__Iterator1
struct U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OtherTalkModule/<PlayOtherTalkEffect>c__Iterator1::.ctor()
extern "C"  void U3CPlayOtherTalkEffectU3Ec__Iterator1__ctor_m2045933411 (U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OtherTalkModule/<PlayOtherTalkEffect>c__Iterator1::MoveNext()
extern "C"  bool U3CPlayOtherTalkEffectU3Ec__Iterator1_MoveNext_m2300415153 (U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OtherTalkModule/<PlayOtherTalkEffect>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayOtherTalkEffectU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m46381633 (U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OtherTalkModule/<PlayOtherTalkEffect>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayOtherTalkEffectU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m373465001 (U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherTalkModule/<PlayOtherTalkEffect>c__Iterator1::Dispose()
extern "C"  void U3CPlayOtherTalkEffectU3Ec__Iterator1_Dispose_m3809289474 (U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherTalkModule/<PlayOtherTalkEffect>c__Iterator1::Reset()
extern "C"  void U3CPlayOtherTalkEffectU3Ec__Iterator1_Reset_m1913099632 (U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherTalkModule/<PlayOtherTalkEffect>c__Iterator1::<>m__0()
extern "C"  void U3CPlayOtherTalkEffectU3Ec__Iterator1_U3CU3Em__0_m2447774990 (U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
