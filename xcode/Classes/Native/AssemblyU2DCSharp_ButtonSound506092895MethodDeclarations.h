﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonSound
struct ButtonSound_t506092895;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void ButtonSound::.ctor()
extern "C"  void ButtonSound__ctor_m3076438386 (ButtonSound_t506092895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonSound::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ButtonSound_OnPointerClick_m191109806 (ButtonSound_t506092895 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonSound::Awake()
extern "C"  void ButtonSound_Awake_m1858769147 (ButtonSound_t506092895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
