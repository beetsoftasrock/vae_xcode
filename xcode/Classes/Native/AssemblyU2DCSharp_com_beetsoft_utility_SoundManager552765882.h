﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// com.beetsoft.utility.SoundManager/BundleSoundName
struct  BundleSoundName_t552765882 
{
public:
	// System.String com.beetsoft.utility.SoundManager/BundleSoundName::bundleName
	String_t* ___bundleName_0;
	// System.String com.beetsoft.utility.SoundManager/BundleSoundName::soundName
	String_t* ___soundName_1;

public:
	inline static int32_t get_offset_of_bundleName_0() { return static_cast<int32_t>(offsetof(BundleSoundName_t552765882, ___bundleName_0)); }
	inline String_t* get_bundleName_0() const { return ___bundleName_0; }
	inline String_t** get_address_of_bundleName_0() { return &___bundleName_0; }
	inline void set_bundleName_0(String_t* value)
	{
		___bundleName_0 = value;
		Il2CppCodeGenWriteBarrier(&___bundleName_0, value);
	}

	inline static int32_t get_offset_of_soundName_1() { return static_cast<int32_t>(offsetof(BundleSoundName_t552765882, ___soundName_1)); }
	inline String_t* get_soundName_1() const { return ___soundName_1; }
	inline String_t** get_address_of_soundName_1() { return &___soundName_1; }
	inline void set_soundName_1(String_t* value)
	{
		___soundName_1 = value;
		Il2CppCodeGenWriteBarrier(&___soundName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of com.beetsoft.utility.SoundManager/BundleSoundName
struct BundleSoundName_t552765882_marshaled_pinvoke
{
	char* ___bundleName_0;
	char* ___soundName_1;
};
// Native definition for COM marshalling of com.beetsoft.utility.SoundManager/BundleSoundName
struct BundleSoundName_t552765882_marshaled_com
{
	Il2CppChar* ___bundleName_0;
	Il2CppChar* ___soundName_1;
};
