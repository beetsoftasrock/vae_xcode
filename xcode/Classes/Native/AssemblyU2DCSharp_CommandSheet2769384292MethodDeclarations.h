﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CommandSheet
struct CommandSheet_t2769384292;

#include "codegen/il2cpp-codegen.h"

// System.Void CommandSheet::.ctor()
extern "C"  void CommandSheet__ctor_m311571483 (CommandSheet_t2769384292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
