﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseOnplayQuestion
struct BaseOnplayQuestion_t1717064182;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseOnplayQuestion::.ctor()
extern "C"  void BaseOnplayQuestion__ctor_m3944764841 (BaseOnplayQuestion_t1717064182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BaseOnplayQuestion::GetSumScoreCorrect()
extern "C"  int32_t BaseOnplayQuestion_GetSumScoreCorrect_m4127825456 (BaseOnplayQuestion_t1717064182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseOnplayQuestion::Completed()
extern "C"  void BaseOnplayQuestion_Completed_m128737136 (BaseOnplayQuestion_t1717064182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BaseOnplayQuestion::get_isCompleted()
extern "C"  bool BaseOnplayQuestion_get_isCompleted_m111277655 (BaseOnplayQuestion_t1717064182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseOnplayQuestion::set_isCompleted(System.Boolean)
extern "C"  void BaseOnplayQuestion_set_isCompleted_m480175330 (BaseOnplayQuestion_t1717064182 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
