﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopSettingFactory
struct ChapterTopSettingFactory_t1128648570;
// BaseSetting
struct BaseSetting_t2575616157;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterTopSettingFactory::.ctor()
extern "C"  void ChapterTopSettingFactory__ctor_m1339152423 (ChapterTopSettingFactory_t1128648570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseSetting ChapterTopSettingFactory::GetSetting(System.Int32)
extern "C"  BaseSetting_t2575616157 * ChapterTopSettingFactory_GetSetting_m2929138452 (ChapterTopSettingFactory_t1128648570 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
