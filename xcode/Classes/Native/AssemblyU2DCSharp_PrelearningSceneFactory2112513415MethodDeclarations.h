﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PrelearningSceneFactory
struct PrelearningSceneFactory_t2112513415;
// BaseSetting
struct BaseSetting_t2575616157;
// PrelearningSetting
struct PrelearningSetting_t219472927;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PrelearningSceneFactory::.ctor()
extern "C"  void PrelearningSceneFactory__ctor_m167275308 (PrelearningSceneFactory_t2112513415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseSetting PrelearningSceneFactory::GetSetting(System.Int32)
extern "C"  BaseSetting_t2575616157 * PrelearningSceneFactory_GetSetting_m2066458291 (PrelearningSceneFactory_t2112513415 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PrelearningSetting PrelearningSceneFactory::GetPrelearningSceneSetting(System.String,System.Int32)
extern "C"  PrelearningSetting_t219472927 * PrelearningSceneFactory_GetPrelearningSceneSetting_m2539759212 (Il2CppObject * __this /* static, unused */, String_t* ___lessonName0, int32_t ___chapter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
