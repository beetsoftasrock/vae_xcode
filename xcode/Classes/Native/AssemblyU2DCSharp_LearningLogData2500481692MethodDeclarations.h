﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearningLogData
struct LearningLogData_t2500481692;

#include "codegen/il2cpp-codegen.h"

// System.Void LearningLogData::.ctor()
extern "C"  void LearningLogData__ctor_m1695961103 (LearningLogData_t2500481692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
