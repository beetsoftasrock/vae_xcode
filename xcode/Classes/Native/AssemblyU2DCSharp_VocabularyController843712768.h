﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectItem[]
struct SelectItemU5BU5D_t1017141182;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// CommandSheet
struct CommandSheet_t2769384292;
// UnityEngine.UI.Text
struct Text_t356221433;
// SelectItem
struct SelectItem_t2844432199;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action
struct Action_t3226471752;

#include "AssemblyU2DCSharp_UIController2029583246.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VocabularyController
struct  VocabularyController_t843712768  : public UIController_t2029583246
{
public:
	// SelectItem[] VocabularyController::selectItems
	SelectItemU5BU5D_t1017141182* ___selectItems_2;
	// System.Collections.Generic.List`1<CommandSheet/Param> VocabularyController::vocabularys
	List_1_t3492939606 * ___vocabularys_3;
	// CommandSheet VocabularyController::vocabComand
	CommandSheet_t2769384292 * ___vocabComand_4;
	// System.Int32 VocabularyController::index
	int32_t ___index_5;
	// System.Int32 VocabularyController::count
	int32_t ___count_6;
	// UnityEngine.UI.Text VocabularyController::quesion
	Text_t356221433 * ___quesion_7;
	// SelectItem VocabularyController::currentItem
	SelectItem_t2844432199 * ___currentItem_8;
	// System.String VocabularyController::currentAnswer
	String_t* ___currentAnswer_9;
	// System.String VocabularyController::currentText
	String_t* ___currentText_10;
	// System.String VocabularyController::currentSubText
	String_t* ___currentSubText_11;
	// System.String VocabularyController::vocabVoicePath
	String_t* ___vocabVoicePath_12;
	// UnityEngine.AudioSource VocabularyController::sound
	AudioSource_t1135106623 * ___sound_13;
	// UnityEngine.GameObject VocabularyController::resultPopup
	GameObject_t1756533147 * ___resultPopup_14;
	// System.Action VocabularyController::OnFinish
	Action_t3226471752 * ___OnFinish_15;

public:
	inline static int32_t get_offset_of_selectItems_2() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___selectItems_2)); }
	inline SelectItemU5BU5D_t1017141182* get_selectItems_2() const { return ___selectItems_2; }
	inline SelectItemU5BU5D_t1017141182** get_address_of_selectItems_2() { return &___selectItems_2; }
	inline void set_selectItems_2(SelectItemU5BU5D_t1017141182* value)
	{
		___selectItems_2 = value;
		Il2CppCodeGenWriteBarrier(&___selectItems_2, value);
	}

	inline static int32_t get_offset_of_vocabularys_3() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___vocabularys_3)); }
	inline List_1_t3492939606 * get_vocabularys_3() const { return ___vocabularys_3; }
	inline List_1_t3492939606 ** get_address_of_vocabularys_3() { return &___vocabularys_3; }
	inline void set_vocabularys_3(List_1_t3492939606 * value)
	{
		___vocabularys_3 = value;
		Il2CppCodeGenWriteBarrier(&___vocabularys_3, value);
	}

	inline static int32_t get_offset_of_vocabComand_4() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___vocabComand_4)); }
	inline CommandSheet_t2769384292 * get_vocabComand_4() const { return ___vocabComand_4; }
	inline CommandSheet_t2769384292 ** get_address_of_vocabComand_4() { return &___vocabComand_4; }
	inline void set_vocabComand_4(CommandSheet_t2769384292 * value)
	{
		___vocabComand_4 = value;
		Il2CppCodeGenWriteBarrier(&___vocabComand_4, value);
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}

	inline static int32_t get_offset_of_quesion_7() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___quesion_7)); }
	inline Text_t356221433 * get_quesion_7() const { return ___quesion_7; }
	inline Text_t356221433 ** get_address_of_quesion_7() { return &___quesion_7; }
	inline void set_quesion_7(Text_t356221433 * value)
	{
		___quesion_7 = value;
		Il2CppCodeGenWriteBarrier(&___quesion_7, value);
	}

	inline static int32_t get_offset_of_currentItem_8() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___currentItem_8)); }
	inline SelectItem_t2844432199 * get_currentItem_8() const { return ___currentItem_8; }
	inline SelectItem_t2844432199 ** get_address_of_currentItem_8() { return &___currentItem_8; }
	inline void set_currentItem_8(SelectItem_t2844432199 * value)
	{
		___currentItem_8 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_8, value);
	}

	inline static int32_t get_offset_of_currentAnswer_9() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___currentAnswer_9)); }
	inline String_t* get_currentAnswer_9() const { return ___currentAnswer_9; }
	inline String_t** get_address_of_currentAnswer_9() { return &___currentAnswer_9; }
	inline void set_currentAnswer_9(String_t* value)
	{
		___currentAnswer_9 = value;
		Il2CppCodeGenWriteBarrier(&___currentAnswer_9, value);
	}

	inline static int32_t get_offset_of_currentText_10() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___currentText_10)); }
	inline String_t* get_currentText_10() const { return ___currentText_10; }
	inline String_t** get_address_of_currentText_10() { return &___currentText_10; }
	inline void set_currentText_10(String_t* value)
	{
		___currentText_10 = value;
		Il2CppCodeGenWriteBarrier(&___currentText_10, value);
	}

	inline static int32_t get_offset_of_currentSubText_11() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___currentSubText_11)); }
	inline String_t* get_currentSubText_11() const { return ___currentSubText_11; }
	inline String_t** get_address_of_currentSubText_11() { return &___currentSubText_11; }
	inline void set_currentSubText_11(String_t* value)
	{
		___currentSubText_11 = value;
		Il2CppCodeGenWriteBarrier(&___currentSubText_11, value);
	}

	inline static int32_t get_offset_of_vocabVoicePath_12() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___vocabVoicePath_12)); }
	inline String_t* get_vocabVoicePath_12() const { return ___vocabVoicePath_12; }
	inline String_t** get_address_of_vocabVoicePath_12() { return &___vocabVoicePath_12; }
	inline void set_vocabVoicePath_12(String_t* value)
	{
		___vocabVoicePath_12 = value;
		Il2CppCodeGenWriteBarrier(&___vocabVoicePath_12, value);
	}

	inline static int32_t get_offset_of_sound_13() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___sound_13)); }
	inline AudioSource_t1135106623 * get_sound_13() const { return ___sound_13; }
	inline AudioSource_t1135106623 ** get_address_of_sound_13() { return &___sound_13; }
	inline void set_sound_13(AudioSource_t1135106623 * value)
	{
		___sound_13 = value;
		Il2CppCodeGenWriteBarrier(&___sound_13, value);
	}

	inline static int32_t get_offset_of_resultPopup_14() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___resultPopup_14)); }
	inline GameObject_t1756533147 * get_resultPopup_14() const { return ___resultPopup_14; }
	inline GameObject_t1756533147 ** get_address_of_resultPopup_14() { return &___resultPopup_14; }
	inline void set_resultPopup_14(GameObject_t1756533147 * value)
	{
		___resultPopup_14 = value;
		Il2CppCodeGenWriteBarrier(&___resultPopup_14, value);
	}

	inline static int32_t get_offset_of_OnFinish_15() { return static_cast<int32_t>(offsetof(VocabularyController_t843712768, ___OnFinish_15)); }
	inline Action_t3226471752 * get_OnFinish_15() const { return ___OnFinish_15; }
	inline Action_t3226471752 ** get_address_of_OnFinish_15() { return &___OnFinish_15; }
	inline void set_OnFinish_15(Action_t3226471752 * value)
	{
		___OnFinish_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnFinish_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
