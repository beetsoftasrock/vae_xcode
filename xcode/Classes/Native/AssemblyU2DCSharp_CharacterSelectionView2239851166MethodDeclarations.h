﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterSelectionView
struct CharacterSelectionView_t2239851166;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterSelectionView::.ctor()
extern "C"  void CharacterSelectionView__ctor_m279709931 (CharacterSelectionView_t2239851166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterSelectionView::get_selected()
extern "C"  bool CharacterSelectionView_get_selected_m2315642515 (CharacterSelectionView_t2239851166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelectionView::set_selected(System.Boolean)
extern "C"  void CharacterSelectionView_set_selected_m3505561528 (CharacterSelectionView_t2239851166 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelectionView::RefreshView()
extern "C"  void CharacterSelectionView_RefreshView_m4071273875 (CharacterSelectionView_t2239851166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelectionView::UpdateView()
extern "C"  void CharacterSelectionView_UpdateView_m596098199 (CharacterSelectionView_t2239851166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelectionView::Clicked()
extern "C"  void CharacterSelectionView_Clicked_m4133973540 (CharacterSelectionView_t2239851166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelectionView::Awake()
extern "C"  void CharacterSelectionView_Awake_m3943182590 (CharacterSelectionView_t2239851166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
