﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3898244613 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t2637371637 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3184826381 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t489908132 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t754146990 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t362827733 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3437517264 ();
extern "C" void DelegatePInvokeWrapper_AsyncReadHandler_t1533281179 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t3362229488 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t1894833619 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1599878889 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t453000516 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1559754630 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t888270799 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t3737776727 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1824458113 ();
extern "C" void DelegatePInvokeWrapper_Action_t3226471752 ();
extern "C" void DelegatePInvokeWrapper_UnityPurchasingCallback_t2635187846 ();
extern "C" void DelegatePInvokeWrapper_AndroidJavaRunnable_t3501776228 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t1867914413 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2480912210 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t4025899511 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3486805455 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336 ();
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180 ();
extern "C" void DelegatePInvokeWrapper_UnityNativePurchasingCallback_t3230812225 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1946318473 ();
extern "C" void DelegatePInvokeWrapper_OnError_t1220768278 ();
extern "C" void DelegatePInvokeWrapper_OnInit_t1674205282 ();
extern "C" void DelegatePInvokeWrapper_OnSaved_t836722345 ();
extern "C" void DelegatePInvokeWrapper_OnAccelEvent_t1967739812 ();
extern "C" void DelegatePInvokeWrapper_OnButtonEvent_t358370788 ();
extern "C" void DelegatePInvokeWrapper_OnGyroEvent_t1804908545 ();
extern "C" void DelegatePInvokeWrapper_OnOrientationEvent_t602701282 ();
extern "C" void DelegatePInvokeWrapper_OnExceptionCallback_t1653610982 ();
extern "C" void DelegatePInvokeWrapper_OnVideoEventCallback_t3117232894 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[43] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t3898244613,
	DelegatePInvokeWrapper_Swapper_t2637371637,
	DelegatePInvokeWrapper_ReadDelegate_t3184826381,
	DelegatePInvokeWrapper_WriteDelegate_t489908132,
	DelegatePInvokeWrapper_CrossContextDelegate_t754146990,
	DelegatePInvokeWrapper_CallbackHandler_t362827733,
	DelegatePInvokeWrapper_ThreadStart_t3437517264,
	DelegatePInvokeWrapper_AsyncReadHandler_t1533281179,
	DelegatePInvokeWrapper_ReadMethod_t3362229488,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745,
	DelegatePInvokeWrapper_WriteMethod_t1894833619,
	DelegatePInvokeWrapper_ReadDelegate_t1599878889,
	DelegatePInvokeWrapper_WriteDelegate_t453000516,
	DelegatePInvokeWrapper_ReadDelegate_t1559754630,
	DelegatePInvokeWrapper_WriteDelegate_t888270799,
	DelegatePInvokeWrapper_SocketAsyncCall_t3737776727,
	DelegatePInvokeWrapper_CostDelegate_t1824458113,
	DelegatePInvokeWrapper_Action_t3226471752,
	DelegatePInvokeWrapper_UnityPurchasingCallback_t2635187846,
	DelegatePInvokeWrapper_AndroidJavaRunnable_t3501776228,
	DelegatePInvokeWrapper_LogCallback_t1867914413,
	DelegatePInvokeWrapper_PCMReaderCallback_t3007145346,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033,
	DelegatePInvokeWrapper_WillRenderCanvases_t3522132132,
	DelegatePInvokeWrapper_StateChanged_t2480912210,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815,
	DelegatePInvokeWrapper_UnityAction_t4025899511,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033,
	DelegatePInvokeWrapper_WindowFunction_t3486805455,
	DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336,
	DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180,
	DelegatePInvokeWrapper_UnityNativePurchasingCallback_t3230812225,
	DelegatePInvokeWrapper_OnValidateInput_t1946318473,
	DelegatePInvokeWrapper_OnError_t1220768278,
	DelegatePInvokeWrapper_OnInit_t1674205282,
	DelegatePInvokeWrapper_OnSaved_t836722345,
	DelegatePInvokeWrapper_OnAccelEvent_t1967739812,
	DelegatePInvokeWrapper_OnButtonEvent_t358370788,
	DelegatePInvokeWrapper_OnGyroEvent_t1804908545,
	DelegatePInvokeWrapper_OnOrientationEvent_t602701282,
	DelegatePInvokeWrapper_OnExceptionCallback_t1653610982,
	DelegatePInvokeWrapper_OnVideoEventCallback_t3117232894,
};
