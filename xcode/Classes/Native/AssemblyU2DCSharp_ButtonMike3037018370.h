﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HandleRecording
struct HandleRecording_t1220661029;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// Popup
struct Popup_t161311578;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonMike
struct  ButtonMike_t3037018370  : public MonoBehaviour_t1158329972
{
public:
	// HandleRecording ButtonMike::handleRecording
	HandleRecording_t1220661029 * ___handleRecording_2;
	// UnityEngine.AudioSource ButtonMike::audioSource
	AudioSource_t1135106623 * ___audioSource_3;
	// System.Boolean ButtonMike::isRecording
	bool ___isRecording_4;
	// System.Single ButtonMike::timeCountdonwn
	float ___timeCountdonwn_5;
	// System.Single ButtonMike::tempTimeCountdonwn
	float ___tempTimeCountdonwn_6;
	// Popup ButtonMike::popupRecording
	Popup_t161311578 * ___popupRecording_7;
	// UnityEngine.Sprite ButtonMike::sprDefault
	Sprite_t309593783 * ___sprDefault_8;
	// UnityEngine.Sprite ButtonMike::sprRecording
	Sprite_t309593783 * ___sprRecording_9;
	// UnityEngine.UI.Button ButtonMike::btnListen
	Button_t2872111280 * ___btnListen_10;
	// UnityEngine.UI.Button ButtonMike::btnNext
	Button_t2872111280 * ___btnNext_11;
	// UnityEngine.UI.Image ButtonMike::img
	Image_t2042527209 * ___img_12;
	// System.Boolean ButtonMike::isHolding
	bool ___isHolding_13;
	// System.Boolean ButtonMike::isRecorded
	bool ___isRecorded_14;

public:
	inline static int32_t get_offset_of_handleRecording_2() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___handleRecording_2)); }
	inline HandleRecording_t1220661029 * get_handleRecording_2() const { return ___handleRecording_2; }
	inline HandleRecording_t1220661029 ** get_address_of_handleRecording_2() { return &___handleRecording_2; }
	inline void set_handleRecording_2(HandleRecording_t1220661029 * value)
	{
		___handleRecording_2 = value;
		Il2CppCodeGenWriteBarrier(&___handleRecording_2, value);
	}

	inline static int32_t get_offset_of_audioSource_3() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___audioSource_3)); }
	inline AudioSource_t1135106623 * get_audioSource_3() const { return ___audioSource_3; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_3() { return &___audioSource_3; }
	inline void set_audioSource_3(AudioSource_t1135106623 * value)
	{
		___audioSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_3, value);
	}

	inline static int32_t get_offset_of_isRecording_4() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___isRecording_4)); }
	inline bool get_isRecording_4() const { return ___isRecording_4; }
	inline bool* get_address_of_isRecording_4() { return &___isRecording_4; }
	inline void set_isRecording_4(bool value)
	{
		___isRecording_4 = value;
	}

	inline static int32_t get_offset_of_timeCountdonwn_5() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___timeCountdonwn_5)); }
	inline float get_timeCountdonwn_5() const { return ___timeCountdonwn_5; }
	inline float* get_address_of_timeCountdonwn_5() { return &___timeCountdonwn_5; }
	inline void set_timeCountdonwn_5(float value)
	{
		___timeCountdonwn_5 = value;
	}

	inline static int32_t get_offset_of_tempTimeCountdonwn_6() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___tempTimeCountdonwn_6)); }
	inline float get_tempTimeCountdonwn_6() const { return ___tempTimeCountdonwn_6; }
	inline float* get_address_of_tempTimeCountdonwn_6() { return &___tempTimeCountdonwn_6; }
	inline void set_tempTimeCountdonwn_6(float value)
	{
		___tempTimeCountdonwn_6 = value;
	}

	inline static int32_t get_offset_of_popupRecording_7() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___popupRecording_7)); }
	inline Popup_t161311578 * get_popupRecording_7() const { return ___popupRecording_7; }
	inline Popup_t161311578 ** get_address_of_popupRecording_7() { return &___popupRecording_7; }
	inline void set_popupRecording_7(Popup_t161311578 * value)
	{
		___popupRecording_7 = value;
		Il2CppCodeGenWriteBarrier(&___popupRecording_7, value);
	}

	inline static int32_t get_offset_of_sprDefault_8() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___sprDefault_8)); }
	inline Sprite_t309593783 * get_sprDefault_8() const { return ___sprDefault_8; }
	inline Sprite_t309593783 ** get_address_of_sprDefault_8() { return &___sprDefault_8; }
	inline void set_sprDefault_8(Sprite_t309593783 * value)
	{
		___sprDefault_8 = value;
		Il2CppCodeGenWriteBarrier(&___sprDefault_8, value);
	}

	inline static int32_t get_offset_of_sprRecording_9() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___sprRecording_9)); }
	inline Sprite_t309593783 * get_sprRecording_9() const { return ___sprRecording_9; }
	inline Sprite_t309593783 ** get_address_of_sprRecording_9() { return &___sprRecording_9; }
	inline void set_sprRecording_9(Sprite_t309593783 * value)
	{
		___sprRecording_9 = value;
		Il2CppCodeGenWriteBarrier(&___sprRecording_9, value);
	}

	inline static int32_t get_offset_of_btnListen_10() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___btnListen_10)); }
	inline Button_t2872111280 * get_btnListen_10() const { return ___btnListen_10; }
	inline Button_t2872111280 ** get_address_of_btnListen_10() { return &___btnListen_10; }
	inline void set_btnListen_10(Button_t2872111280 * value)
	{
		___btnListen_10 = value;
		Il2CppCodeGenWriteBarrier(&___btnListen_10, value);
	}

	inline static int32_t get_offset_of_btnNext_11() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___btnNext_11)); }
	inline Button_t2872111280 * get_btnNext_11() const { return ___btnNext_11; }
	inline Button_t2872111280 ** get_address_of_btnNext_11() { return &___btnNext_11; }
	inline void set_btnNext_11(Button_t2872111280 * value)
	{
		___btnNext_11 = value;
		Il2CppCodeGenWriteBarrier(&___btnNext_11, value);
	}

	inline static int32_t get_offset_of_img_12() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___img_12)); }
	inline Image_t2042527209 * get_img_12() const { return ___img_12; }
	inline Image_t2042527209 ** get_address_of_img_12() { return &___img_12; }
	inline void set_img_12(Image_t2042527209 * value)
	{
		___img_12 = value;
		Il2CppCodeGenWriteBarrier(&___img_12, value);
	}

	inline static int32_t get_offset_of_isHolding_13() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___isHolding_13)); }
	inline bool get_isHolding_13() const { return ___isHolding_13; }
	inline bool* get_address_of_isHolding_13() { return &___isHolding_13; }
	inline void set_isHolding_13(bool value)
	{
		___isHolding_13 = value;
	}

	inline static int32_t get_offset_of_isRecorded_14() { return static_cast<int32_t>(offsetof(ButtonMike_t3037018370, ___isRecorded_14)); }
	inline bool get_isRecorded_14() const { return ___isRecorded_14; }
	inline bool* get_address_of_isRecorded_14() { return &___isRecorded_14; }
	inline void set_isRecorded_14(bool value)
	{
		___isRecorded_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
