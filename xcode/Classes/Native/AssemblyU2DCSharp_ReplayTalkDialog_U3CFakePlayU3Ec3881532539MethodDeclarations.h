﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayTalkDialog/<FakePlay>c__Iterator1
struct U3CFakePlayU3Ec__Iterator1_t3881532539;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ReplayTalkDialog/<FakePlay>c__Iterator1::.ctor()
extern "C"  void U3CFakePlayU3Ec__Iterator1__ctor_m3230726820 (U3CFakePlayU3Ec__Iterator1_t3881532539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayTalkDialog/<FakePlay>c__Iterator1::MoveNext()
extern "C"  bool U3CFakePlayU3Ec__Iterator1_MoveNext_m3448060872 (U3CFakePlayU3Ec__Iterator1_t3881532539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ReplayTalkDialog/<FakePlay>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFakePlayU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m561661462 (U3CFakePlayU3Ec__Iterator1_t3881532539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ReplayTalkDialog/<FakePlay>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFakePlayU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3694602030 (U3CFakePlayU3Ec__Iterator1_t3881532539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayTalkDialog/<FakePlay>c__Iterator1::Dispose()
extern "C"  void U3CFakePlayU3Ec__Iterator1_Dispose_m369411197 (U3CFakePlayU3Ec__Iterator1_t3881532539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayTalkDialog/<FakePlay>c__Iterator1::Reset()
extern "C"  void U3CFakePlayU3Ec__Iterator1_Reset_m3829440863 (U3CFakePlayU3Ec__Iterator1_t3881532539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
