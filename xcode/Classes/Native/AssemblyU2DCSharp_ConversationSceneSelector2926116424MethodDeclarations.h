﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSceneSelector
struct ConversationSceneSelector_t2926116424;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationSceneSelector::.ctor()
extern "C"  void ConversationSceneSelector__ctor_m846684929 (ConversationSceneSelector_t2926116424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ConversationSceneSelector::get_currentSelectedId()
extern "C"  int32_t ConversationSceneSelector_get_currentSelectedId_m1178297795 (ConversationSceneSelector_t2926116424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelector::set_currentSelectedId(System.Int32)
extern "C"  void ConversationSceneSelector_set_currentSelectedId_m2035074824 (ConversationSceneSelector_t2926116424 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelector::UpdateView()
extern "C"  void ConversationSceneSelector_UpdateView_m4013785649 (ConversationSceneSelector_t2926116424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelector::OnConversationSceneSelectionViewClicked(System.Int32)
extern "C"  void ConversationSceneSelector_OnConversationSceneSelectionViewClicked_m4199227804 (ConversationSceneSelector_t2926116424 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelector::Awake()
extern "C"  void ConversationSceneSelector_Awake_m1011107928 (ConversationSceneSelector_t2926116424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
