﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseConversationCommandLoader/<GoTo>c__AnonStorey0
struct U3CGoToU3Ec__AnonStorey0_t144537213;
// CommandSheet/Param
struct Param_t4123818474;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CommandSheet_Param4123818474.h"

// System.Void BaseConversationCommandLoader/<GoTo>c__AnonStorey0::.ctor()
extern "C"  void U3CGoToU3Ec__AnonStorey0__ctor_m570908408 (U3CGoToU3Ec__AnonStorey0_t144537213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BaseConversationCommandLoader/<GoTo>c__AnonStorey0::<>m__0(CommandSheet/Param)
extern "C"  bool U3CGoToU3Ec__AnonStorey0_U3CU3Em__0_m3998001915 (U3CGoToU3Ec__AnonStorey0_t144537213 * __this, Param_t4123818474 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
