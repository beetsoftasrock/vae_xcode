﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.Utility
struct Utility_t1353423966;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"

// System.Void AssetBundles.Utility::.ctor()
extern "C"  void Utility__ctor_m2355321134 (Utility_t1353423966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetBundles.Utility::GetPlatformName()
extern "C"  String_t* Utility_GetPlatformName_m2837361653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetBundles.Utility::GetPlatformForAssetBundles(UnityEngine.RuntimePlatform)
extern "C"  String_t* Utility_GetPlatformForAssetBundles_m345390698 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
