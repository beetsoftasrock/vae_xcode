﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence2DatasWrapper
struct Sentence2DatasWrapper_t2314527497;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// SentenceListeningSpellingQuestionData
struct SentenceListeningSpellingQuestionData_t3011907248;
// CommandSheet/Param
struct Param_t4123818474;
// System.Collections.Generic.List`1<SentenceListeningSpellingQuestionData>
struct List_1_t2381028380;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CommandSheet_Param4123818474.h"

// System.Void Sentence2DatasWrapper::.ctor(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void Sentence2DatasWrapper__ctor_m4090338248 (Sentence2DatasWrapper_t2314527497 * __this, List_1_t3492939606 * ___listData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CommandSheet/Param> Sentence2DatasWrapper::get_wrappedDatas()
extern "C"  List_1_t3492939606 * Sentence2DatasWrapper_get_wrappedDatas_m1514866860 (Sentence2DatasWrapper_t2314527497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2DatasWrapper::set_wrappedDatas(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void Sentence2DatasWrapper_set_wrappedDatas_m979495085 (Sentence2DatasWrapper_t2314527497 * __this, List_1_t3492939606 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SentenceListeningSpellingQuestionData Sentence2DatasWrapper::ConvertData(CommandSheet/Param)
extern "C"  SentenceListeningSpellingQuestionData_t3011907248 * Sentence2DatasWrapper_ConvertData_m3765457552 (Sentence2DatasWrapper_t2314527497 * __this, Param_t4123818474 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<SentenceListeningSpellingQuestionData> Sentence2DatasWrapper::GetConvertedDatas()
extern "C"  List_1_t2381028380 * Sentence2DatasWrapper_GetConvertedDatas_m2897738976 (Sentence2DatasWrapper_t2314527497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
