﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Listenning1DataLoader
struct Listenning1DataLoader_t2634637697;
// Listening1SceneController
struct Listening1SceneController_t4197317516;

#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterBasedListening1SceneSetup
struct  ChapterBasedListening1SceneSetup_t344182819  : public GlobalBasedSetupModule_t1655890765
{
public:
	// Listenning1DataLoader ChapterBasedListening1SceneSetup::dataLoader
	Listenning1DataLoader_t2634637697 * ___dataLoader_3;
	// Listening1SceneController ChapterBasedListening1SceneSetup::sceneController
	Listening1SceneController_t4197317516 * ___sceneController_4;

public:
	inline static int32_t get_offset_of_dataLoader_3() { return static_cast<int32_t>(offsetof(ChapterBasedListening1SceneSetup_t344182819, ___dataLoader_3)); }
	inline Listenning1DataLoader_t2634637697 * get_dataLoader_3() const { return ___dataLoader_3; }
	inline Listenning1DataLoader_t2634637697 ** get_address_of_dataLoader_3() { return &___dataLoader_3; }
	inline void set_dataLoader_3(Listenning1DataLoader_t2634637697 * value)
	{
		___dataLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_3, value);
	}

	inline static int32_t get_offset_of_sceneController_4() { return static_cast<int32_t>(offsetof(ChapterBasedListening1SceneSetup_t344182819, ___sceneController_4)); }
	inline Listening1SceneController_t4197317516 * get_sceneController_4() const { return ___sceneController_4; }
	inline Listening1SceneController_t4197317516 ** get_address_of_sceneController_4() { return &___sceneController_4; }
	inline void set_sceneController_4(Listening1SceneController_t4197317516 * value)
	{
		___sceneController_4 = value;
		Il2CppCodeGenWriteBarrier(&___sceneController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
