﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadSceneScript/<Awake>c__AnonStorey1
struct U3CAwakeU3Ec__AnonStorey1_t1264054739;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadSceneScript/<Awake>c__AnonStorey1::.ctor()
extern "C"  void U3CAwakeU3Ec__AnonStorey1__ctor_m1642323638 (U3CAwakeU3Ec__AnonStorey1_t1264054739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadSceneScript/<Awake>c__AnonStorey1::<>m__0()
extern "C"  void U3CAwakeU3Ec__AnonStorey1_U3CU3Em__0_m2233978345 (U3CAwakeU3Ec__AnonStorey1_t1264054739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
