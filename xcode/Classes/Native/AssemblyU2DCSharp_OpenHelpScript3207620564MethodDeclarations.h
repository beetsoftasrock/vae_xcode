﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelpScript
struct OpenHelpScript_t3207620564;
// ManageATutorial
struct ManageATutorial_t1512139496;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ManageATutorial1512139496.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void OpenHelpScript::.ctor()
extern "C"  void OpenHelpScript__ctor_m2436713389 (OpenHelpScript_t3207620564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpScript::Awake()
extern "C"  void OpenHelpScript_Awake_m26968256 (OpenHelpScript_t3207620564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpScript::Start()
extern "C"  void OpenHelpScript_Start_m448284277 (OpenHelpScript_t3207620564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ManageATutorial OpenHelpScript::get_ManageHelp()
extern "C"  ManageATutorial_t1512139496 * OpenHelpScript_get_ManageHelp_m915085891 (OpenHelpScript_t3207620564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpScript::set_ManageHelp(ManageATutorial)
extern "C"  void OpenHelpScript_set_ManageHelp_m736992992 (OpenHelpScript_t3207620564 * __this, ManageATutorial_t1512139496 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpScript::OnEnable()
extern "C"  void OpenHelpScript_OnEnable_m1840602581 (OpenHelpScript_t3207620564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpScript::ShowMe()
extern "C"  void OpenHelpScript_ShowMe_m1134961656 (OpenHelpScript_t3207620564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpScript::ResetObjectsBeFront()
extern "C"  void OpenHelpScript_ResetObjectsBeFront_m1295027612 (OpenHelpScript_t3207620564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OpenHelpScript::MoveHand(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  Il2CppObject * OpenHelpScript_MoveHand_m1991631905 (OpenHelpScript_t3207620564 * __this, Transform_t3275118058 * ___dockHand0, Transform_t3275118058 * ___iconHand1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
