﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrUnitySdkVersion
struct GvrUnitySdkVersion_t4210256426;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrUnitySdkVersion::.ctor()
extern "C"  void GvrUnitySdkVersion__ctor_m2593898401 (GvrUnitySdkVersion_t4210256426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrUnitySdkVersion::LogGvrUnitySdkVersion()
extern "C"  void GvrUnitySdkVersion_LogGvrUnitySdkVersion_m1284654103 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
