﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenOriSupport
struct ScreenOriSupport_t692417341;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenOriSupport::.ctor()
extern "C"  void ScreenOriSupport__ctor_m3959049386 (ScreenOriSupport_t692417341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenOriSupport::Awake()
extern "C"  void ScreenOriSupport_Awake_m2074074125 (ScreenOriSupport_t692417341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
