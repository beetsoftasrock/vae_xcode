﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence1Test
struct Sentence1Test_t1755393472;

#include "codegen/il2cpp-codegen.h"

// System.Void Sentence1Test::.ctor()
extern "C"  void Sentence1Test__ctor_m4042776189 (Sentence1Test_t1755393472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
