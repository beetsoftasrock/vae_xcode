﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisableWithTimeCountdown
struct DisableWithTimeCountdown_t622718188;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void DisableWithTimeCountdown::.ctor()
extern "C"  void DisableWithTimeCountdown__ctor_m3724804661 (DisableWithTimeCountdown_t622718188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeCountdown::OnEnable()
extern "C"  void DisableWithTimeCountdown_OnEnable_m86881197 (DisableWithTimeCountdown_t622718188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeCountdown::StartCountdown()
extern "C"  void DisableWithTimeCountdown_StartCountdown_m1156750608 (DisableWithTimeCountdown_t622718188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DisableWithTimeCountdown::PlayAudioAndDisable()
extern "C"  Il2CppObject * DisableWithTimeCountdown_PlayAudioAndDisable_m4136899164 (DisableWithTimeCountdown_t622718188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
