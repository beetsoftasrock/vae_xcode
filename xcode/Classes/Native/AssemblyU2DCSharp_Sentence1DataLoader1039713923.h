﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CommandSheet
struct CommandSheet_t2769384292;
// IDatasWrapper`1<SentenceStructureIdiomQuestionData>
struct IDatasWrapper_1_t1099524387;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sentence1DataLoader
struct  Sentence1DataLoader_t1039713923  : public MonoBehaviour_t1158329972
{
public:
	// CommandSheet Sentence1DataLoader::dataSheet
	CommandSheet_t2769384292 * ___dataSheet_2;
	// IDatasWrapper`1<SentenceStructureIdiomQuestionData> Sentence1DataLoader::<datasWrapper>k__BackingField
	Il2CppObject* ___U3CdatasWrapperU3Ek__BackingField_3;
	// System.String Sentence1DataLoader::_questionBundleText
	String_t* ____questionBundleText_4;

public:
	inline static int32_t get_offset_of_dataSheet_2() { return static_cast<int32_t>(offsetof(Sentence1DataLoader_t1039713923, ___dataSheet_2)); }
	inline CommandSheet_t2769384292 * get_dataSheet_2() const { return ___dataSheet_2; }
	inline CommandSheet_t2769384292 ** get_address_of_dataSheet_2() { return &___dataSheet_2; }
	inline void set_dataSheet_2(CommandSheet_t2769384292 * value)
	{
		___dataSheet_2 = value;
		Il2CppCodeGenWriteBarrier(&___dataSheet_2, value);
	}

	inline static int32_t get_offset_of_U3CdatasWrapperU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Sentence1DataLoader_t1039713923, ___U3CdatasWrapperU3Ek__BackingField_3)); }
	inline Il2CppObject* get_U3CdatasWrapperU3Ek__BackingField_3() const { return ___U3CdatasWrapperU3Ek__BackingField_3; }
	inline Il2CppObject** get_address_of_U3CdatasWrapperU3Ek__BackingField_3() { return &___U3CdatasWrapperU3Ek__BackingField_3; }
	inline void set_U3CdatasWrapperU3Ek__BackingField_3(Il2CppObject* value)
	{
		___U3CdatasWrapperU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdatasWrapperU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of__questionBundleText_4() { return static_cast<int32_t>(offsetof(Sentence1DataLoader_t1039713923, ____questionBundleText_4)); }
	inline String_t* get__questionBundleText_4() const { return ____questionBundleText_4; }
	inline String_t** get_address_of__questionBundleText_4() { return &____questionBundleText_4; }
	inline void set__questionBundleText_4(String_t* value)
	{
		____questionBundleText_4 = value;
		Il2CppCodeGenWriteBarrier(&____questionBundleText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
