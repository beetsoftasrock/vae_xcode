﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct List_1_t1457175523;
// System.Collections.Generic.IEnumerable`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct IEnumerable_1_t2380181436;
// Beetsoft.VAE.Purchaser/PurchaseSession[]
struct PurchaseSessionU5BU5D_t64753102;
// System.Collections.Generic.IEnumerator`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct IEnumerator_1_t3858545514;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct ICollection_1_t3040129696;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct ReadOnlyCollection_1_t2273840083;
// System.Predicate`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct Predicate_1_t531024506;
// System.Collections.Generic.IComparer`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct IComparer_1_t42517513;
// System.Comparison`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct Comparison_1_t3349793242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat991905197.h"

// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor()
extern "C"  void List_1__ctor_m3046606734_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1__ctor_m3046606734(__this, method) ((  void (*) (List_1_t1457175523 *, const MethodInfo*))List_1__ctor_m3046606734_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m325305740_gshared (List_1_t1457175523 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m325305740(__this, ___collection0, method) ((  void (*) (List_1_t1457175523 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m325305740_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3986910966_gshared (List_1_t1457175523 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3986910966(__this, ___capacity0, method) ((  void (*) (List_1_t1457175523 *, int32_t, const MethodInfo*))List_1__ctor_m3986910966_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m4209229082_gshared (List_1_t1457175523 * __this, PurchaseSessionU5BU5D_t64753102* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m4209229082(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1457175523 *, PurchaseSessionU5BU5D_t64753102*, int32_t, const MethodInfo*))List_1__ctor_m4209229082_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.cctor()
extern "C"  void List_1__cctor_m1237540542_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1237540542(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1237540542_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m262364923_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m262364923(__this, method) ((  Il2CppObject* (*) (List_1_t1457175523 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m262364923_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2292315431_gshared (List_1_t1457175523 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2292315431(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1457175523 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2292315431_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m455442944_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m455442944(__this, method) ((  Il2CppObject * (*) (List_1_t1457175523 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m455442944_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3162129551_gshared (List_1_t1457175523 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3162129551(__this, ___item0, method) ((  int32_t (*) (List_1_t1457175523 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3162129551_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1269983087_gshared (List_1_t1457175523 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1269983087(__this, ___item0, method) ((  bool (*) (List_1_t1457175523 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1269983087_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m522731877_gshared (List_1_t1457175523 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m522731877(__this, ___item0, method) ((  int32_t (*) (List_1_t1457175523 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m522731877_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m4026298226_gshared (List_1_t1457175523 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m4026298226(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1457175523 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m4026298226_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3175427108_gshared (List_1_t1457175523 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3175427108(__this, ___item0, method) ((  void (*) (List_1_t1457175523 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3175427108_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3897542268_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3897542268(__this, method) ((  bool (*) (List_1_t1457175523 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3897542268_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2963271671_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2963271671(__this, method) ((  bool (*) (List_1_t1457175523 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2963271671_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m111213923_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m111213923(__this, method) ((  Il2CppObject * (*) (List_1_t1457175523 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m111213923_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2458317206_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2458317206(__this, method) ((  bool (*) (List_1_t1457175523 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2458317206_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m151576099_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m151576099(__this, method) ((  bool (*) (List_1_t1457175523 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m151576099_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2307662320_gshared (List_1_t1457175523 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2307662320(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1457175523 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2307662320_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m352862849_gshared (List_1_t1457175523 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m352862849(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1457175523 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m352862849_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Add(T)
extern "C"  void List_1_Add_m1960722170_gshared (List_1_t1457175523 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define List_1_Add_m1960722170(__this, ___item0, method) ((  void (*) (List_1_t1457175523 *, PurchaseSession_t2088054391 , const MethodInfo*))List_1_Add_m1960722170_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1898021435_gshared (List_1_t1457175523 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1898021435(__this, ___newCount0, method) ((  void (*) (List_1_t1457175523 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1898021435_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m231211038_gshared (List_1_t1457175523 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m231211038(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1457175523 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m231211038_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m31863043_gshared (List_1_t1457175523 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m31863043(__this, ___collection0, method) ((  void (*) (List_1_t1457175523 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m31863043_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3595339587_gshared (List_1_t1457175523 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3595339587(__this, ___enumerable0, method) ((  void (*) (List_1_t1457175523 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3595339587_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3008797736_gshared (List_1_t1457175523 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3008797736(__this, ___collection0, method) ((  void (*) (List_1_t1457175523 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3008797736_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2273840083 * List_1_AsReadOnly_m2580955123_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2580955123(__this, method) ((  ReadOnlyCollection_1_t2273840083 * (*) (List_1_t1457175523 *, const MethodInfo*))List_1_AsReadOnly_m2580955123_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Clear()
extern "C"  void List_1_Clear_m759725690_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_Clear_m759725690(__this, method) ((  void (*) (List_1_t1457175523 *, const MethodInfo*))List_1_Clear_m759725690_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Contains(T)
extern "C"  bool List_1_Contains_m4195998680_gshared (List_1_t1457175523 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define List_1_Contains_m4195998680(__this, ___item0, method) ((  bool (*) (List_1_t1457175523 *, PurchaseSession_t2088054391 , const MethodInfo*))List_1_Contains_m4195998680_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m2037048259_gshared (List_1_t1457175523 * __this, PurchaseSessionU5BU5D_t64753102* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m2037048259(__this, ___array0, method) ((  void (*) (List_1_t1457175523 *, PurchaseSessionU5BU5D_t64753102*, const MethodInfo*))List_1_CopyTo_m2037048259_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3308460162_gshared (List_1_t1457175523 * __this, PurchaseSessionU5BU5D_t64753102* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3308460162(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1457175523 *, PurchaseSessionU5BU5D_t64753102*, int32_t, const MethodInfo*))List_1_CopyTo_m3308460162_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m538058244_gshared (List_1_t1457175523 * __this, Predicate_1_t531024506 * ___match0, const MethodInfo* method);
#define List_1_Exists_m538058244(__this, ___match0, method) ((  bool (*) (List_1_t1457175523 *, Predicate_1_t531024506 *, const MethodInfo*))List_1_Exists_m538058244_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Find(System.Predicate`1<T>)
extern "C"  PurchaseSession_t2088054391  List_1_Find_m1780699767_gshared (List_1_t1457175523 * __this, Predicate_1_t531024506 * ___match0, const MethodInfo* method);
#define List_1_Find_m1780699767(__this, ___match0, method) ((  PurchaseSession_t2088054391  (*) (List_1_t1457175523 *, Predicate_1_t531024506 *, const MethodInfo*))List_1_Find_m1780699767_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1730156263_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t531024506 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1730156263(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t531024506 *, const MethodInfo*))List_1_CheckMatch_m1730156263_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t1457175523 * List_1_FindAll_m613311263_gshared (List_1_t1457175523 * __this, Predicate_1_t531024506 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m613311263(__this, ___match0, method) ((  List_1_t1457175523 * (*) (List_1_t1457175523 *, Predicate_1_t531024506 *, const MethodInfo*))List_1_FindAll_m613311263_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t1457175523 * List_1_FindAllStackBits_m1122357841_gshared (List_1_t1457175523 * __this, Predicate_1_t531024506 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1122357841(__this, ___match0, method) ((  List_1_t1457175523 * (*) (List_1_t1457175523 *, Predicate_1_t531024506 *, const MethodInfo*))List_1_FindAllStackBits_m1122357841_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t1457175523 * List_1_FindAllList_m2824095825_gshared (List_1_t1457175523 * __this, Predicate_1_t531024506 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2824095825(__this, ___match0, method) ((  List_1_t1457175523 * (*) (List_1_t1457175523 *, Predicate_1_t531024506 *, const MethodInfo*))List_1_FindAllList_m2824095825_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m411785071_gshared (List_1_t1457175523 * __this, Predicate_1_t531024506 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m411785071(__this, ___match0, method) ((  int32_t (*) (List_1_t1457175523 *, Predicate_1_t531024506 *, const MethodInfo*))List_1_FindIndex_m411785071_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m59093882_gshared (List_1_t1457175523 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t531024506 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m59093882(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1457175523 *, int32_t, int32_t, Predicate_1_t531024506 *, const MethodInfo*))List_1_GetIndex_m59093882_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::GetEnumerator()
extern "C"  Enumerator_t991905197  List_1_GetEnumerator_m1419460891_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1419460891(__this, method) ((  Enumerator_t991905197  (*) (List_1_t1457175523 *, const MethodInfo*))List_1_GetEnumerator_m1419460891_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::GetRange(System.Int32,System.Int32)
extern "C"  List_1_t1457175523 * List_1_GetRange_m308865739_gshared (List_1_t1457175523 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_GetRange_m308865739(__this, ___index0, ___count1, method) ((  List_1_t1457175523 * (*) (List_1_t1457175523 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m308865739_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3422600788_gshared (List_1_t1457175523 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3422600788(__this, ___item0, method) ((  int32_t (*) (List_1_t1457175523 *, PurchaseSession_t2088054391 , const MethodInfo*))List_1_IndexOf_m3422600788_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1078231051_gshared (List_1_t1457175523 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1078231051(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1457175523 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1078231051_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3245701898_gshared (List_1_t1457175523 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3245701898(__this, ___index0, method) ((  void (*) (List_1_t1457175523 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3245701898_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2592093709_gshared (List_1_t1457175523 * __this, int32_t ___index0, PurchaseSession_t2088054391  ___item1, const MethodInfo* method);
#define List_1_Insert_m2592093709(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1457175523 *, int32_t, PurchaseSession_t2088054391 , const MethodInfo*))List_1_Insert_m2592093709_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m746525540_gshared (List_1_t1457175523 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m746525540(__this, ___collection0, method) ((  void (*) (List_1_t1457175523 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m746525540_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Remove(T)
extern "C"  bool List_1_Remove_m1701413599_gshared (List_1_t1457175523 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define List_1_Remove_m1701413599(__this, ___item0, method) ((  bool (*) (List_1_t1457175523 *, PurchaseSession_t2088054391 , const MethodInfo*))List_1_Remove_m1701413599_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m4237143911_gshared (List_1_t1457175523 * __this, Predicate_1_t531024506 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m4237143911(__this, ___match0, method) ((  int32_t (*) (List_1_t1457175523 *, Predicate_1_t531024506 *, const MethodInfo*))List_1_RemoveAll_m4237143911_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1548948401_gshared (List_1_t1457175523 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1548948401(__this, ___index0, method) ((  void (*) (List_1_t1457175523 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1548948401_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m320141334_gshared (List_1_t1457175523 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m320141334(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1457175523 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m320141334_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Reverse()
extern "C"  void List_1_Reverse_m1761526851_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_Reverse_m1761526851(__this, method) ((  void (*) (List_1_t1457175523 *, const MethodInfo*))List_1_Reverse_m1761526851_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Sort()
extern "C"  void List_1_Sort_m2116141677_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_Sort_m2116141677(__this, method) ((  void (*) (List_1_t1457175523 *, const MethodInfo*))List_1_Sort_m2116141677_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1012899065_gshared (List_1_t1457175523 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1012899065(__this, ___comparer0, method) ((  void (*) (List_1_t1457175523 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1012899065_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2439996550_gshared (List_1_t1457175523 * __this, Comparison_1_t3349793242 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2439996550(__this, ___comparison0, method) ((  void (*) (List_1_t1457175523 *, Comparison_1_t3349793242 *, const MethodInfo*))List_1_Sort_m2439996550_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::ToArray()
extern "C"  PurchaseSessionU5BU5D_t64753102* List_1_ToArray_m2017091648_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_ToArray_m2017091648(__this, method) ((  PurchaseSessionU5BU5D_t64753102* (*) (List_1_t1457175523 *, const MethodInfo*))List_1_ToArray_m2017091648_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3429816488_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3429816488(__this, method) ((  void (*) (List_1_t1457175523 *, const MethodInfo*))List_1_TrimExcess_m3429816488_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m408195622_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m408195622(__this, method) ((  int32_t (*) (List_1_t1457175523 *, const MethodInfo*))List_1_get_Capacity_m408195622_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m524271043_gshared (List_1_t1457175523 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m524271043(__this, ___value0, method) ((  void (*) (List_1_t1457175523 *, int32_t, const MethodInfo*))List_1_set_Capacity_m524271043_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Count()
extern "C"  int32_t List_1_get_Count_m3550982735_gshared (List_1_t1457175523 * __this, const MethodInfo* method);
#define List_1_get_Count_m3550982735(__this, method) ((  int32_t (*) (List_1_t1457175523 *, const MethodInfo*))List_1_get_Count_m3550982735_gshared)(__this, method)
// T System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Item(System.Int32)
extern "C"  PurchaseSession_t2088054391  List_1_get_Item_m2242976405_gshared (List_1_t1457175523 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2242976405(__this, ___index0, method) ((  PurchaseSession_t2088054391  (*) (List_1_t1457175523 *, int32_t, const MethodInfo*))List_1_get_Item_m2242976405_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3717858920_gshared (List_1_t1457175523 * __this, int32_t ___index0, PurchaseSession_t2088054391  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3717858920(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1457175523 *, int32_t, PurchaseSession_t2088054391 , const MethodInfo*))List_1_set_Item_m3717858920_gshared)(__this, ___index0, ___value1, method)
