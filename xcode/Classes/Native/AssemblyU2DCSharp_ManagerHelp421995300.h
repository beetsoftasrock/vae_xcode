﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;
// ManagerHelp
struct ManagerHelp_t421995300;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManagerHelp
struct  ManagerHelp_t421995300  : public Il2CppObject
{
public:
	// System.Boolean ManagerHelp::isOnHelp
	bool ___isOnHelp_0;
	// System.Boolean ManagerHelp::isLoadFromBundle
	bool ___isLoadFromBundle_1;
	// UnityEngine.Transform ManagerHelp::cloneHelp
	Transform_t3275118058 * ___cloneHelp_2;
	// System.Int32 ManagerHelp::indexHelp
	int32_t ___indexHelp_3;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> ManagerHelp::liCurrentSceneHelpSprite
	List_1_t3973682211 * ___liCurrentSceneHelpSprite_4;

public:
	inline static int32_t get_offset_of_isOnHelp_0() { return static_cast<int32_t>(offsetof(ManagerHelp_t421995300, ___isOnHelp_0)); }
	inline bool get_isOnHelp_0() const { return ___isOnHelp_0; }
	inline bool* get_address_of_isOnHelp_0() { return &___isOnHelp_0; }
	inline void set_isOnHelp_0(bool value)
	{
		___isOnHelp_0 = value;
	}

	inline static int32_t get_offset_of_isLoadFromBundle_1() { return static_cast<int32_t>(offsetof(ManagerHelp_t421995300, ___isLoadFromBundle_1)); }
	inline bool get_isLoadFromBundle_1() const { return ___isLoadFromBundle_1; }
	inline bool* get_address_of_isLoadFromBundle_1() { return &___isLoadFromBundle_1; }
	inline void set_isLoadFromBundle_1(bool value)
	{
		___isLoadFromBundle_1 = value;
	}

	inline static int32_t get_offset_of_cloneHelp_2() { return static_cast<int32_t>(offsetof(ManagerHelp_t421995300, ___cloneHelp_2)); }
	inline Transform_t3275118058 * get_cloneHelp_2() const { return ___cloneHelp_2; }
	inline Transform_t3275118058 ** get_address_of_cloneHelp_2() { return &___cloneHelp_2; }
	inline void set_cloneHelp_2(Transform_t3275118058 * value)
	{
		___cloneHelp_2 = value;
		Il2CppCodeGenWriteBarrier(&___cloneHelp_2, value);
	}

	inline static int32_t get_offset_of_indexHelp_3() { return static_cast<int32_t>(offsetof(ManagerHelp_t421995300, ___indexHelp_3)); }
	inline int32_t get_indexHelp_3() const { return ___indexHelp_3; }
	inline int32_t* get_address_of_indexHelp_3() { return &___indexHelp_3; }
	inline void set_indexHelp_3(int32_t value)
	{
		___indexHelp_3 = value;
	}

	inline static int32_t get_offset_of_liCurrentSceneHelpSprite_4() { return static_cast<int32_t>(offsetof(ManagerHelp_t421995300, ___liCurrentSceneHelpSprite_4)); }
	inline List_1_t3973682211 * get_liCurrentSceneHelpSprite_4() const { return ___liCurrentSceneHelpSprite_4; }
	inline List_1_t3973682211 ** get_address_of_liCurrentSceneHelpSprite_4() { return &___liCurrentSceneHelpSprite_4; }
	inline void set_liCurrentSceneHelpSprite_4(List_1_t3973682211 * value)
	{
		___liCurrentSceneHelpSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___liCurrentSceneHelpSprite_4, value);
	}
};

struct ManagerHelp_t421995300_StaticFields
{
public:
	// ManagerHelp ManagerHelp::instance
	ManagerHelp_t421995300 * ___instance_5;
	// UnityEngine.GameObject ManagerHelp::prefabHelp
	GameObject_t1756533147 * ___prefabHelp_6;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(ManagerHelp_t421995300_StaticFields, ___instance_5)); }
	inline ManagerHelp_t421995300 * get_instance_5() const { return ___instance_5; }
	inline ManagerHelp_t421995300 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(ManagerHelp_t421995300 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}

	inline static int32_t get_offset_of_prefabHelp_6() { return static_cast<int32_t>(offsetof(ManagerHelp_t421995300_StaticFields, ___prefabHelp_6)); }
	inline GameObject_t1756533147 * get_prefabHelp_6() const { return ___prefabHelp_6; }
	inline GameObject_t1756533147 ** get_address_of_prefabHelp_6() { return &___prefabHelp_6; }
	inline void set_prefabHelp_6(GameObject_t1756533147 * value)
	{
		___prefabHelp_6 = value;
		Il2CppCodeGenWriteBarrier(&___prefabHelp_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
