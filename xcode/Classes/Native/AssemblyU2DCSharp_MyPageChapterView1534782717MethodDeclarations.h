﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyPageChapterView
struct MyPageChapterView_t1534782717;

#include "codegen/il2cpp-codegen.h"

// System.Void MyPageChapterView::.ctor()
extern "C"  void MyPageChapterView__ctor_m1494101174 (MyPageChapterView_t1534782717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
