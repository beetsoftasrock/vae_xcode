﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonMode
struct ButtonMode_t2270855263;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ButtonMode::.ctor()
extern "C"  void ButtonMode__ctor_m2597220770 (ButtonMode_t2270855263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMode::SetIsVR(System.Boolean)
extern "C"  void ButtonMode_SetIsVR_m3681380181 (ButtonMode_t2270855263 * __this, bool ___isVR0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMode::SetIsChangingMode(System.Boolean)
extern "C"  void ButtonMode_SetIsChangingMode_m197042633 (ButtonMode_t2270855263 * __this, bool ___isChangingMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMode::ChangeScene(System.String)
extern "C"  void ButtonMode_ChangeScene_m1341151928 (ButtonMode_t2270855263 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMode::ChangeScene(System.Int32)
extern "C"  void ButtonMode_ChangeScene_m3842788523 (ButtonMode_t2270855263 * __this, int32_t ___sceneBuildIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMode::RotateToLandscapeLeft()
extern "C"  void ButtonMode_RotateToLandscapeLeft_m2901037340 (ButtonMode_t2270855263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMode::Portrait()
extern "C"  void ButtonMode_Portrait_m680786209 (ButtonMode_t2270855263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
