﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence2SceneController/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t1627489015;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Sentence2SceneController/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m1651253072 (U3CStartU3Ec__Iterator0_t1627489015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Sentence2SceneController/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1856825748 (U3CStartU3Ec__Iterator0_t1627489015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Sentence2SceneController/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1859197308 (U3CStartU3Ec__Iterator0_t1627489015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Sentence2SceneController/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1407242244 (U3CStartU3Ec__Iterator0_t1627489015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m1414857181 (U3CStartU3Ec__Iterator0_t1627489015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m3986605383 (U3CStartU3Ec__Iterator0_t1627489015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
