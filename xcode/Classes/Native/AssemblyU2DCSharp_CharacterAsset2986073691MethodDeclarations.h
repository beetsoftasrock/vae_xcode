﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterAsset
struct CharacterAsset_t2986073691;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterAsset::.ctor()
extern "C"  void CharacterAsset__ctor_m34261484 (CharacterAsset_t2986073691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
