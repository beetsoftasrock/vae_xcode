﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationUISettingFactory
struct ConversationUISettingFactory_t3134705897;
// BaseSetting
struct BaseSetting_t2575616157;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationUISettingFactory::.ctor()
extern "C"  void ConversationUISettingFactory__ctor_m894365866 (ConversationUISettingFactory_t3134705897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseSetting ConversationUISettingFactory::GetSetting(System.Int32)
extern "C"  BaseSetting_t2575616157 * ConversationUISettingFactory_GetSetting_m3269594353 (ConversationUISettingFactory_t3134705897 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
