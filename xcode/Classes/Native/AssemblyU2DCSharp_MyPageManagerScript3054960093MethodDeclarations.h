﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyPageManagerScript
struct MyPageManagerScript_t3054960093;
// System.Collections.Generic.List`1<ChapterMyPageScript>
struct List_1_t2962650743;
// System.String
struct String_t;
// LearningLogResponse
struct LearningLogResponse_t112900141;
// LearningLogData[]
struct LearningLogDataU5BU5D_t3295896437;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_LearningLogResponse112900141.h"

// System.Void MyPageManagerScript::.ctor()
extern "C"  void MyPageManagerScript__ctor_m2771703042 (MyPageManagerScript_t3054960093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageManagerScript::Awake()
extern "C"  void MyPageManagerScript_Awake_m2553505833 (MyPageManagerScript_t3054960093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageManagerScript::InitChapter()
extern "C"  void MyPageManagerScript_InitChapter_m1042782167 (MyPageManagerScript_t3054960093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageManagerScript::UpdateResultView(System.Collections.Generic.List`1<ChapterMyPageScript>)
extern "C"  void MyPageManagerScript_UpdateResultView_m3629777626 (MyPageManagerScript_t3054960093 * __this, List_1_t2962650743 * ___views0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ChapterMyPageScript> MyPageManagerScript::GenerateChapterResultView()
extern "C"  List_1_t2962650743 * MyPageManagerScript_GenerateChapterResultView_m2622769430 (MyPageManagerScript_t3054960093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageManagerScript::GetLearningData(System.String,System.DateTime,System.DateTime)
extern "C"  void MyPageManagerScript_GetLearningData_m748007798 (MyPageManagerScript_t3054960093 * __this, String_t* ___uuid0, DateTime_t693205669  ___startDate1, DateTime_t693205669  ___endDate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageManagerScript::HandleLearningDataResponse(LearningLogResponse)
extern "C"  void MyPageManagerScript_HandleLearningDataResponse_m1951122086 (MyPageManagerScript_t3054960093 * __this, LearningLogResponse_t112900141 * ___learningLogResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageManagerScript::UpdateLearningDatas(LearningLogData[])
extern "C"  void MyPageManagerScript_UpdateLearningDatas_m1053092184 (MyPageManagerScript_t3054960093 * __this, LearningLogDataU5BU5D_t3295896437* ___learningLogs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageManagerScript::<GetLearningData>m__0()
extern "C"  void MyPageManagerScript_U3CGetLearningDataU3Em__0_m898270197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
