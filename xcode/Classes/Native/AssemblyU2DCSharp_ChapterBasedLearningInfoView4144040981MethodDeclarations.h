﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedLearningInfoView
struct ChapterBasedLearningInfoView_t4144040981;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedLearningInfoView::.ctor()
extern "C"  void ChapterBasedLearningInfoView__ctor_m3823588778 (ChapterBasedLearningInfoView_t4144040981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedLearningInfoView::Start()
extern "C"  void ChapterBasedLearningInfoView_Start_m4032975394 (ChapterBasedLearningInfoView_t4144040981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
