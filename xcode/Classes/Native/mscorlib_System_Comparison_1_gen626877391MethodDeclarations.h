﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3951188146MethodDeclarations.h"

// System.Void System.Comparison`1<ConversationCommand>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3209006475(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t626877391 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2929820459_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<ConversationCommand>::Invoke(T,T)
#define Comparison_1_Invoke_m2003976689(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t626877391 *, ConversationCommand_t3660105836 *, ConversationCommand_t3660105836 *, const MethodInfo*))Comparison_1_Invoke_m2798106261_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<ConversationCommand>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m744469252(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t626877391 *, ConversationCommand_t3660105836 *, ConversationCommand_t3660105836 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1817828810_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<ConversationCommand>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m644288975(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t626877391 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m1056665895_gshared)(__this, ___result0, method)
