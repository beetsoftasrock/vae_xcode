﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.Purchaser/<RestorePurchases>c__AnonStorey1
struct U3CRestorePurchasesU3Ec__AnonStorey1_t1157070212;

#include "codegen/il2cpp-codegen.h"

// System.Void Beetsoft.VAE.Purchaser/<RestorePurchases>c__AnonStorey1::.ctor()
extern "C"  void U3CRestorePurchasesU3Ec__AnonStorey1__ctor_m1767195259 (U3CRestorePurchasesU3Ec__AnonStorey1_t1157070212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser/<RestorePurchases>c__AnonStorey1::<>m__0(System.Boolean)
extern "C"  void U3CRestorePurchasesU3Ec__AnonStorey1_U3CU3Em__0_m1019958923 (U3CRestorePurchasesU3Ec__AnonStorey1_t1157070212 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
