﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// LoadSceneScript
struct LoadSceneScript_t888325993;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadSceneScript/<Awake>c__AnonStorey1
struct  U3CAwakeU3Ec__AnonStorey1_t1264054739  : public Il2CppObject
{
public:
	// UnityEngine.UI.Button LoadSceneScript/<Awake>c__AnonStorey1::btn
	Button_t2872111280 * ___btn_0;
	// LoadSceneScript LoadSceneScript/<Awake>c__AnonStorey1::$this
	LoadSceneScript_t888325993 * ___U24this_1;

public:
	inline static int32_t get_offset_of_btn_0() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey1_t1264054739, ___btn_0)); }
	inline Button_t2872111280 * get_btn_0() const { return ___btn_0; }
	inline Button_t2872111280 ** get_address_of_btn_0() { return &___btn_0; }
	inline void set_btn_0(Button_t2872111280 * value)
	{
		___btn_0 = value;
		Il2CppCodeGenWriteBarrier(&___btn_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey1_t1264054739, ___U24this_1)); }
	inline LoadSceneScript_t888325993 * get_U24this_1() const { return ___U24this_1; }
	inline LoadSceneScript_t888325993 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LoadSceneScript_t888325993 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
