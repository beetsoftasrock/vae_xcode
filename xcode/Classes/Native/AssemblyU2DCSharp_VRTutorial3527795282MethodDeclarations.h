﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRTutorial
struct VRTutorial_t3527795282;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void VRTutorial::.ctor()
extern "C"  void VRTutorial__ctor_m3614168239 (VRTutorial_t3527795282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTutorial::Start()
extern "C"  void VRTutorial_Start_m764987827 (VRTutorial_t3527795282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VRTutorial::Delay()
extern "C"  Il2CppObject * VRTutorial_Delay_m778032646 (VRTutorial_t3527795282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRTutorial::GoToVRMainArea()
extern "C"  void VRTutorial_GoToVRMainArea_m75649650 (VRTutorial_t3527795282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
