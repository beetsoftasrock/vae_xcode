﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalConfig
struct  GlobalConfig_t3080413471  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 GlobalConfig::currentChapter
	int32_t ___currentChapter_2;

public:
	inline static int32_t get_offset_of_currentChapter_2() { return static_cast<int32_t>(offsetof(GlobalConfig_t3080413471, ___currentChapter_2)); }
	inline int32_t get_currentChapter_2() const { return ___currentChapter_2; }
	inline int32_t* get_address_of_currentChapter_2() { return &___currentChapter_2; }
	inline void set_currentChapter_2(int32_t value)
	{
		___currentChapter_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
