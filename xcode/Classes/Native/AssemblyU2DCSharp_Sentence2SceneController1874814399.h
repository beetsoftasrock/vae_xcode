﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// Sentence2DataLoader
struct Sentence2DataLoader_t844185122;
// System.Collections.Generic.List`1<SentenceListeningSpellingOnplayQuestion>
struct List_1_t1883666459;
// System.Collections.Generic.List`1<SentenceListeningSpellingQuestionData>
struct List_1_t2381028380;
// Sentence2QuestionView
struct Sentence2QuestionView_t1942347884;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// SentenceListeningSpellingOnplayQuestion
struct SentenceListeningSpellingOnplayQuestion_t2514545327;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sentence2SceneController
struct  Sentence2SceneController_t1874814399  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Sentence2SceneController::questionBundleText
	Text_t356221433 * ___questionBundleText_2;
	// Sentence2DataLoader Sentence2SceneController::dataLoader
	Sentence2DataLoader_t844185122 * ___dataLoader_3;
	// System.Collections.Generic.List`1<SentenceListeningSpellingOnplayQuestion> Sentence2SceneController::onplayQuestions
	List_1_t1883666459 * ___onplayQuestions_4;
	// System.Collections.Generic.List`1<SentenceListeningSpellingQuestionData> Sentence2SceneController::questionDatas
	List_1_t2381028380 * ___questionDatas_5;
	// Sentence2QuestionView Sentence2SceneController::questionView
	Sentence2QuestionView_t1942347884 * ___questionView_6;
	// UnityEngine.GameObject Sentence2SceneController::resultGo
	GameObject_t1756533147 * ___resultGo_7;
	// System.String Sentence2SceneController::soundPath
	String_t* ___soundPath_8;
	// SentenceListeningSpellingOnplayQuestion Sentence2SceneController::_currentQuestion
	SentenceListeningSpellingOnplayQuestion_t2514545327 * ____currentQuestion_9;

public:
	inline static int32_t get_offset_of_questionBundleText_2() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399, ___questionBundleText_2)); }
	inline Text_t356221433 * get_questionBundleText_2() const { return ___questionBundleText_2; }
	inline Text_t356221433 ** get_address_of_questionBundleText_2() { return &___questionBundleText_2; }
	inline void set_questionBundleText_2(Text_t356221433 * value)
	{
		___questionBundleText_2 = value;
		Il2CppCodeGenWriteBarrier(&___questionBundleText_2, value);
	}

	inline static int32_t get_offset_of_dataLoader_3() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399, ___dataLoader_3)); }
	inline Sentence2DataLoader_t844185122 * get_dataLoader_3() const { return ___dataLoader_3; }
	inline Sentence2DataLoader_t844185122 ** get_address_of_dataLoader_3() { return &___dataLoader_3; }
	inline void set_dataLoader_3(Sentence2DataLoader_t844185122 * value)
	{
		___dataLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_3, value);
	}

	inline static int32_t get_offset_of_onplayQuestions_4() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399, ___onplayQuestions_4)); }
	inline List_1_t1883666459 * get_onplayQuestions_4() const { return ___onplayQuestions_4; }
	inline List_1_t1883666459 ** get_address_of_onplayQuestions_4() { return &___onplayQuestions_4; }
	inline void set_onplayQuestions_4(List_1_t1883666459 * value)
	{
		___onplayQuestions_4 = value;
		Il2CppCodeGenWriteBarrier(&___onplayQuestions_4, value);
	}

	inline static int32_t get_offset_of_questionDatas_5() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399, ___questionDatas_5)); }
	inline List_1_t2381028380 * get_questionDatas_5() const { return ___questionDatas_5; }
	inline List_1_t2381028380 ** get_address_of_questionDatas_5() { return &___questionDatas_5; }
	inline void set_questionDatas_5(List_1_t2381028380 * value)
	{
		___questionDatas_5 = value;
		Il2CppCodeGenWriteBarrier(&___questionDatas_5, value);
	}

	inline static int32_t get_offset_of_questionView_6() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399, ___questionView_6)); }
	inline Sentence2QuestionView_t1942347884 * get_questionView_6() const { return ___questionView_6; }
	inline Sentence2QuestionView_t1942347884 ** get_address_of_questionView_6() { return &___questionView_6; }
	inline void set_questionView_6(Sentence2QuestionView_t1942347884 * value)
	{
		___questionView_6 = value;
		Il2CppCodeGenWriteBarrier(&___questionView_6, value);
	}

	inline static int32_t get_offset_of_resultGo_7() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399, ___resultGo_7)); }
	inline GameObject_t1756533147 * get_resultGo_7() const { return ___resultGo_7; }
	inline GameObject_t1756533147 ** get_address_of_resultGo_7() { return &___resultGo_7; }
	inline void set_resultGo_7(GameObject_t1756533147 * value)
	{
		___resultGo_7 = value;
		Il2CppCodeGenWriteBarrier(&___resultGo_7, value);
	}

	inline static int32_t get_offset_of_soundPath_8() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399, ___soundPath_8)); }
	inline String_t* get_soundPath_8() const { return ___soundPath_8; }
	inline String_t** get_address_of_soundPath_8() { return &___soundPath_8; }
	inline void set_soundPath_8(String_t* value)
	{
		___soundPath_8 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_8, value);
	}

	inline static int32_t get_offset_of__currentQuestion_9() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399, ____currentQuestion_9)); }
	inline SentenceListeningSpellingOnplayQuestion_t2514545327 * get__currentQuestion_9() const { return ____currentQuestion_9; }
	inline SentenceListeningSpellingOnplayQuestion_t2514545327 ** get_address_of__currentQuestion_9() { return &____currentQuestion_9; }
	inline void set__currentQuestion_9(SentenceListeningSpellingOnplayQuestion_t2514545327 * value)
	{
		____currentQuestion_9 = value;
		Il2CppCodeGenWriteBarrier(&____currentQuestion_9, value);
	}
};

struct Sentence2SceneController_t1874814399_StaticFields
{
public:
	// System.Func`1<System.Boolean> Sentence2SceneController::<>f__mg$cache0
	Func_1_t1485000104 * ___U3CU3Ef__mgU24cache0_10;
	// UnityEngine.Events.UnityAction Sentence2SceneController::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_11;
	// UnityEngine.Events.UnityAction Sentence2SceneController::<>f__am$cache1
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache1_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(Sentence2SceneController_t1874814399_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
