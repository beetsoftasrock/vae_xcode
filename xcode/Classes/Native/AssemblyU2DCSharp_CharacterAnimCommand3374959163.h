﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CharacterScript
struct CharacterScript_t1308706256;

#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterAnimCommand
struct  CharacterAnimCommand_t3374959163  : public ConversationCommand_t3660105836
{
public:
	// System.Int32 CharacterAnimCommand::_preCurrentPoint
	int32_t ____preCurrentPoint_0;
	// System.String CharacterAnimCommand::postureTypeName
	String_t* ___postureTypeName_1;
	// System.String CharacterAnimCommand::eyesTypeName
	String_t* ___eyesTypeName_2;
	// System.String CharacterAnimCommand::mouthTypeName
	String_t* ___mouthTypeName_3;
	// System.String CharacterAnimCommand::spawnPosition
	String_t* ___spawnPosition_4;
	// System.Single CharacterAnimCommand::wait
	float ___wait_5;
	// System.String CharacterAnimCommand::role
	String_t* ___role_6;
	// CharacterScript CharacterAnimCommand::charScript
	CharacterScript_t1308706256 * ___charScript_7;

public:
	inline static int32_t get_offset_of__preCurrentPoint_0() { return static_cast<int32_t>(offsetof(CharacterAnimCommand_t3374959163, ____preCurrentPoint_0)); }
	inline int32_t get__preCurrentPoint_0() const { return ____preCurrentPoint_0; }
	inline int32_t* get_address_of__preCurrentPoint_0() { return &____preCurrentPoint_0; }
	inline void set__preCurrentPoint_0(int32_t value)
	{
		____preCurrentPoint_0 = value;
	}

	inline static int32_t get_offset_of_postureTypeName_1() { return static_cast<int32_t>(offsetof(CharacterAnimCommand_t3374959163, ___postureTypeName_1)); }
	inline String_t* get_postureTypeName_1() const { return ___postureTypeName_1; }
	inline String_t** get_address_of_postureTypeName_1() { return &___postureTypeName_1; }
	inline void set_postureTypeName_1(String_t* value)
	{
		___postureTypeName_1 = value;
		Il2CppCodeGenWriteBarrier(&___postureTypeName_1, value);
	}

	inline static int32_t get_offset_of_eyesTypeName_2() { return static_cast<int32_t>(offsetof(CharacterAnimCommand_t3374959163, ___eyesTypeName_2)); }
	inline String_t* get_eyesTypeName_2() const { return ___eyesTypeName_2; }
	inline String_t** get_address_of_eyesTypeName_2() { return &___eyesTypeName_2; }
	inline void set_eyesTypeName_2(String_t* value)
	{
		___eyesTypeName_2 = value;
		Il2CppCodeGenWriteBarrier(&___eyesTypeName_2, value);
	}

	inline static int32_t get_offset_of_mouthTypeName_3() { return static_cast<int32_t>(offsetof(CharacterAnimCommand_t3374959163, ___mouthTypeName_3)); }
	inline String_t* get_mouthTypeName_3() const { return ___mouthTypeName_3; }
	inline String_t** get_address_of_mouthTypeName_3() { return &___mouthTypeName_3; }
	inline void set_mouthTypeName_3(String_t* value)
	{
		___mouthTypeName_3 = value;
		Il2CppCodeGenWriteBarrier(&___mouthTypeName_3, value);
	}

	inline static int32_t get_offset_of_spawnPosition_4() { return static_cast<int32_t>(offsetof(CharacterAnimCommand_t3374959163, ___spawnPosition_4)); }
	inline String_t* get_spawnPosition_4() const { return ___spawnPosition_4; }
	inline String_t** get_address_of_spawnPosition_4() { return &___spawnPosition_4; }
	inline void set_spawnPosition_4(String_t* value)
	{
		___spawnPosition_4 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPosition_4, value);
	}

	inline static int32_t get_offset_of_wait_5() { return static_cast<int32_t>(offsetof(CharacterAnimCommand_t3374959163, ___wait_5)); }
	inline float get_wait_5() const { return ___wait_5; }
	inline float* get_address_of_wait_5() { return &___wait_5; }
	inline void set_wait_5(float value)
	{
		___wait_5 = value;
	}

	inline static int32_t get_offset_of_role_6() { return static_cast<int32_t>(offsetof(CharacterAnimCommand_t3374959163, ___role_6)); }
	inline String_t* get_role_6() const { return ___role_6; }
	inline String_t** get_address_of_role_6() { return &___role_6; }
	inline void set_role_6(String_t* value)
	{
		___role_6 = value;
		Il2CppCodeGenWriteBarrier(&___role_6, value);
	}

	inline static int32_t get_offset_of_charScript_7() { return static_cast<int32_t>(offsetof(CharacterAnimCommand_t3374959163, ___charScript_7)); }
	inline CharacterScript_t1308706256 * get_charScript_7() const { return ___charScript_7; }
	inline CharacterScript_t1308706256 ** get_address_of_charScript_7() { return &___charScript_7; }
	inline void set_charScript_7(CharacterScript_t1308706256 * value)
	{
		___charScript_7 = value;
		Il2CppCodeGenWriteBarrier(&___charScript_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
