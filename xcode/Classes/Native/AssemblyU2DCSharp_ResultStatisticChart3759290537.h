﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CircleScript
struct CircleScript_t1225364279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultStatisticChart
struct  ResultStatisticChart_t3759290537  : public MonoBehaviour_t1158329972
{
public:
	// CircleScript ResultStatisticChart::circleScript
	CircleScript_t1225364279 * ___circleScript_2;

public:
	inline static int32_t get_offset_of_circleScript_2() { return static_cast<int32_t>(offsetof(ResultStatisticChart_t3759290537, ___circleScript_2)); }
	inline CircleScript_t1225364279 * get_circleScript_2() const { return ___circleScript_2; }
	inline CircleScript_t1225364279 ** get_address_of_circleScript_2() { return &___circleScript_2; }
	inline void set_circleScript_2(CircleScript_t1225364279 * value)
	{
		___circleScript_2 = value;
		Il2CppCodeGenWriteBarrier(&___circleScript_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
