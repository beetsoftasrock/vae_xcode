﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening1AnswerHelper
struct Listening1AnswerHelper_t2804121578;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening1AnswerHelper::.ctor()
extern "C"  void Listening1AnswerHelper__ctor_m3997270441 (Listening1AnswerHelper_t2804121578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1AnswerHelper::Clicked()
extern "C"  void Listening1AnswerHelper_Clicked_m620640988 (Listening1AnswerHelper_t2804121578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
