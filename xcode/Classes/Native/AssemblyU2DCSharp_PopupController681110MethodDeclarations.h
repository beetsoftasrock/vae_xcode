﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupController
struct PopupController_t681110;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PopupController::.ctor()
extern "C"  void PopupController__ctor_m299211241 (PopupController_t681110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupController::setData(System.Boolean,System.Int32,System.String,System.String)
extern "C"  void PopupController_setData_m3065128189 (PopupController_t681110 * __this, bool ___result0, int32_t ___type1, String_t* ___text2, String_t* ___subText3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupController::SetVocabuaryContent()
extern "C"  void PopupController_SetVocabuaryContent_m4103821692 (PopupController_t681110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupController::SetContentText(System.String,System.String)
extern "C"  void PopupController_SetContentText_m286945575 (PopupController_t681110 * __this, String_t* ___text0, String_t* ___subText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupController::OnNext()
extern "C"  void PopupController_OnNext_m1958662379 (PopupController_t681110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupController::Start()
extern "C"  void PopupController_Start_m102080241 (PopupController_t681110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupController::Update()
extern "C"  void PopupController_Update_m3775426548 (PopupController_t681110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
