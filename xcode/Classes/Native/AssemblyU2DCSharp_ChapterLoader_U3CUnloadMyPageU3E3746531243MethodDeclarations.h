﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterLoader/<UnloadMyPage>c__AnonStorey1
struct U3CUnloadMyPageU3Ec__AnonStorey1_t3746531243;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChapterLoader/<UnloadMyPage>c__AnonStorey1::.ctor()
extern "C"  void U3CUnloadMyPageU3Ec__AnonStorey1__ctor_m3398093258 (U3CUnloadMyPageU3Ec__AnonStorey1_t3746531243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterLoader/<UnloadMyPage>c__AnonStorey1::<>m__0(System.String)
extern "C"  bool U3CUnloadMyPageU3Ec__AnonStorey1_U3CU3Em__0_m3210853135 (U3CUnloadMyPageU3Ec__AnonStorey1_t3746531243 * __this, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
