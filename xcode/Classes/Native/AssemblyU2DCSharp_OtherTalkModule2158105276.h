﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationLogicController
struct ConversationLogicController_t3911886281;
// UnityEngine.UI.Button
struct Button_t2872111280;
// TalkDialogWithSoundButton
struct TalkDialogWithSoundButton_t1663397415;
// PlayOtherTalkDialog
struct PlayOtherTalkDialog_t78515058;
// ConversationTalkData
struct ConversationTalkData_t1570298305;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OtherTalkModule
struct  OtherTalkModule_t2158105276  : public MonoBehaviour_t1158329972
{
public:
	// ConversationLogicController OtherTalkModule::clc
	ConversationLogicController_t3911886281 * ___clc_2;
	// UnityEngine.UI.Button OtherTalkModule::recordButton
	Button_t2872111280 * ___recordButton_3;
	// UnityEngine.UI.Button OtherTalkModule::replayRecordedButton
	Button_t2872111280 * ___replayRecordedButton_4;
	// TalkDialogWithSoundButton OtherTalkModule::otherTalkDialog
	TalkDialogWithSoundButton_t1663397415 * ___otherTalkDialog_5;
	// PlayOtherTalkDialog OtherTalkModule::playOtherTalkDialog
	PlayOtherTalkDialog_t78515058 * ___playOtherTalkDialog_6;
	// ConversationTalkData OtherTalkModule::<currentOtherTalkData>k__BackingField
	ConversationTalkData_t1570298305 * ___U3CcurrentOtherTalkDataU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_clc_2() { return static_cast<int32_t>(offsetof(OtherTalkModule_t2158105276, ___clc_2)); }
	inline ConversationLogicController_t3911886281 * get_clc_2() const { return ___clc_2; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_2() { return &___clc_2; }
	inline void set_clc_2(ConversationLogicController_t3911886281 * value)
	{
		___clc_2 = value;
		Il2CppCodeGenWriteBarrier(&___clc_2, value);
	}

	inline static int32_t get_offset_of_recordButton_3() { return static_cast<int32_t>(offsetof(OtherTalkModule_t2158105276, ___recordButton_3)); }
	inline Button_t2872111280 * get_recordButton_3() const { return ___recordButton_3; }
	inline Button_t2872111280 ** get_address_of_recordButton_3() { return &___recordButton_3; }
	inline void set_recordButton_3(Button_t2872111280 * value)
	{
		___recordButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___recordButton_3, value);
	}

	inline static int32_t get_offset_of_replayRecordedButton_4() { return static_cast<int32_t>(offsetof(OtherTalkModule_t2158105276, ___replayRecordedButton_4)); }
	inline Button_t2872111280 * get_replayRecordedButton_4() const { return ___replayRecordedButton_4; }
	inline Button_t2872111280 ** get_address_of_replayRecordedButton_4() { return &___replayRecordedButton_4; }
	inline void set_replayRecordedButton_4(Button_t2872111280 * value)
	{
		___replayRecordedButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___replayRecordedButton_4, value);
	}

	inline static int32_t get_offset_of_otherTalkDialog_5() { return static_cast<int32_t>(offsetof(OtherTalkModule_t2158105276, ___otherTalkDialog_5)); }
	inline TalkDialogWithSoundButton_t1663397415 * get_otherTalkDialog_5() const { return ___otherTalkDialog_5; }
	inline TalkDialogWithSoundButton_t1663397415 ** get_address_of_otherTalkDialog_5() { return &___otherTalkDialog_5; }
	inline void set_otherTalkDialog_5(TalkDialogWithSoundButton_t1663397415 * value)
	{
		___otherTalkDialog_5 = value;
		Il2CppCodeGenWriteBarrier(&___otherTalkDialog_5, value);
	}

	inline static int32_t get_offset_of_playOtherTalkDialog_6() { return static_cast<int32_t>(offsetof(OtherTalkModule_t2158105276, ___playOtherTalkDialog_6)); }
	inline PlayOtherTalkDialog_t78515058 * get_playOtherTalkDialog_6() const { return ___playOtherTalkDialog_6; }
	inline PlayOtherTalkDialog_t78515058 ** get_address_of_playOtherTalkDialog_6() { return &___playOtherTalkDialog_6; }
	inline void set_playOtherTalkDialog_6(PlayOtherTalkDialog_t78515058 * value)
	{
		___playOtherTalkDialog_6 = value;
		Il2CppCodeGenWriteBarrier(&___playOtherTalkDialog_6, value);
	}

	inline static int32_t get_offset_of_U3CcurrentOtherTalkDataU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(OtherTalkModule_t2158105276, ___U3CcurrentOtherTalkDataU3Ek__BackingField_7)); }
	inline ConversationTalkData_t1570298305 * get_U3CcurrentOtherTalkDataU3Ek__BackingField_7() const { return ___U3CcurrentOtherTalkDataU3Ek__BackingField_7; }
	inline ConversationTalkData_t1570298305 ** get_address_of_U3CcurrentOtherTalkDataU3Ek__BackingField_7() { return &___U3CcurrentOtherTalkDataU3Ek__BackingField_7; }
	inline void set_U3CcurrentOtherTalkDataU3Ek__BackingField_7(ConversationTalkData_t1570298305 * value)
	{
		___U3CcurrentOtherTalkDataU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentOtherTalkDataU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
