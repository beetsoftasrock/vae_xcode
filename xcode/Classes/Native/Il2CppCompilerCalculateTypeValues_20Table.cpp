﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "winrt_U3CModuleU3E3783534214.h"
#include "winrt_UnityEngine_Purchasing_HttpManager2727579731.h"
#include "winrt_UnityEngine_Purchasing_Default_Factory1430638288.h"
#include "winrt_UnityEngine_Purchasing_Default_WinProductDes1075111405.h"
#include "Stores_U3CModuleU3E3783534214.h"
#include "Stores_UnityEngine_Purchasing_AndroidJavaStore2772549594.h"
#include "Stores_UnityEngine_Purchasing_SerializationExtensi3531056818.h"
#include "Stores_UnityEngine_Purchasing_JSONSerializer501879906.h"
#include "Stores_UnityEngine_Purchasing_JavaBridge44746847.h"
#include "Stores_UnityEngine_Purchasing_NativeJSONStore3685388740.h"
#include "Stores_UnityEngine_Purchasing_ScriptingUnityCallbac906080071.h"
#include "Stores_UnityEngine_Purchasing_AmazonAppStoreStoreE1518886395.h"
#include "Stores_UnityEngine_Purchasing_FakeAmazonExtensions2261777661.h"
#include "Stores_UnityEngine_Purchasing_GooglePlayAndroidJav4038061283.h"
#include "Stores_UnityEngine_Purchasing_FakeGooglePlayConfigu737012266.h"
#include "Stores_UnityEngine_Purchasing_FakeSamsungAppsExten1522853249.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsMode2214306743.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsStoreExte3441062041.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl1301617341.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3COn1019488239.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CMe2294210683.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleConfiguatio4052738437.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleExtensions4039399289.h"
#include "Stores_UnityEngine_Purchasing_FakeMicrosoftExtensi3351923809.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore36043095.h"
#include "Stores_UnityEngine_Purchasing_TizenStoreImpl274247241.h"
#include "Stores_UnityEngine_Purchasing_FakeTizenStoreConfig3055550456.h"
#include "Stores_UnityEngine_Purchasing_FacebookStoreImpl1362794587.h"
#include "Stores_UnityEngine_Purchasing_FacebookStoreImpl_U32084418580.h"
#include "Stores_UnityEngine_Purchasing_FakeMoolahConfigurat1681533527.h"
#include "Stores_UnityEngine_Purchasing_FakeMoolahExtensions1432169715.h"
#include "Stores_UnityEngine_Purchasing_CloudMoolahMode206291964.h"
#include "Stores_UnityEngine_Purchasing_LoginResultState2459016979.h"
#include "Stores_UnityEngine_Purchasing_FastRegisterError341731807.h"
#include "Stores_UnityEngine_Purchasing_ValidateReceiptState4359597.h"
#include "Stores_UnityEngine_Purchasing_RestoreTransactionID2487303652.h"
#include "Stores_UnityEngine_Purchasing_RequestPayOutState3537434082.h"
#include "Stores_UnityEngine_Purchasing_TradeSeqState1177537792.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl4206626141.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CV1969356947.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CP3194505917.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CR3192418403.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CS1718677452.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CL3027540100.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CFa706752957.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CR2861560799.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CV1464170479.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CP1516057908.h"
#include "Stores_UnityEngine_Purchasing_MoolahUtil1395207074.h"
#include "Stores_UnityEngine_Purchasing_PayMethod1558319955.h"
#include "Stores_UnityEngine_Purchasing_AndroidStore3203055206.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "Stores_UnityEngine_Purchasing_FakeStore3882981564.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_DialogType1733969544.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CRetriev2692708567.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CPurchas2000504963.h"
#include "Stores_UnityEngine_Purchasing_Price1853024949.h"
#include "Stores_UnityEngine_Purchasing_StoreID471452324.h"
#include "Stores_UnityEngine_Purchasing_GoogleLocale3144343871.h"
#include "Stores_UnityEngine_Purchasing_LocalizedProductDesc1525635964.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem977711995.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766.h"
#include "Stores_UnityEngine_Purchasing_RawStoreProvider3477922056.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2002[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2007[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2015[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (HttpManager_t2727579731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (Factory_t1430638288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (WinProductDescription_t1075111405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[9] = 
{
	WinProductDescription_t1075111405::get_offset_of_U3CplatformSpecificIDU3Ek__BackingField_0(),
	WinProductDescription_t1075111405::get_offset_of_U3CpriceU3Ek__BackingField_1(),
	WinProductDescription_t1075111405::get_offset_of_U3CtitleU3Ek__BackingField_2(),
	WinProductDescription_t1075111405::get_offset_of_U3CdescriptionU3Ek__BackingField_3(),
	WinProductDescription_t1075111405::get_offset_of_U3CISOCurrencyCodeU3Ek__BackingField_4(),
	WinProductDescription_t1075111405::get_offset_of_U3CpriceDecimalU3Ek__BackingField_5(),
	WinProductDescription_t1075111405::get_offset_of_U3CreceiptU3Ek__BackingField_6(),
	WinProductDescription_t1075111405::get_offset_of_U3CtransactionIDU3Ek__BackingField_7(),
	WinProductDescription_t1075111405::get_offset_of_U3CconsumableU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (AndroidJavaStore_t2772549594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[1] = 
{
	AndroidJavaStore_t2772549594::get_offset_of_m_Store_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (SerializationExtensions_t3531056818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (JSONSerializer_t501879906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (JavaBridge_t44746847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[1] = 
{
	JavaBridge_t44746847::get_offset_of_forwardTo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (NativeJSONStore_t3685388740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[2] = 
{
	NativeJSONStore_t3685388740::get_offset_of_unity_0(),
	NativeJSONStore_t3685388740::get_offset_of_store_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (ScriptingUnityCallback_t906080071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[2] = 
{
	ScriptingUnityCallback_t906080071::get_offset_of_forwardTo_0(),
	ScriptingUnityCallback_t906080071::get_offset_of_util_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (AmazonAppStoreStoreExtensions_t1518886395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[1] = 
{
	AmazonAppStoreStoreExtensions_t1518886395::get_offset_of_android_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (FakeAmazonExtensions_t2261777661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (GooglePlayAndroidJavaStore_t4038061283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (FakeGooglePlayConfiguration_t737012266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (FakeSamsungAppsExtensions_t1522853249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (SamsungAppsMode_t2214306743)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2043[4] = 
{
	SamsungAppsMode_t2214306743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (SamsungAppsStoreExtensions_t3441062041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[2] = 
{
	SamsungAppsStoreExtensions_t3441062041::get_offset_of_m_RestoreCallback_1(),
	SamsungAppsStoreExtensions_t3441062041::get_offset_of_m_Java_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (AppleStoreImpl_t1301617341), -1, sizeof(AppleStoreImpl_t1301617341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2045[9] = 
{
	AppleStoreImpl_t1301617341::get_offset_of_m_DeferredCallback_2(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptError_3(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptSuccess_4(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RestoreCallback_5(),
	AppleStoreImpl_t1301617341::get_offset_of_m_Native_6(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_util_7(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_instance_8(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (U3COnProductsRetrievedU3Ec__AnonStorey0_t1019488239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[1] = 
{
	U3COnProductsRetrievedU3Ec__AnonStorey0_t1019488239::get_offset_of_productDescription_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (U3CMessageCallbackU3Ec__AnonStorey1_t2294210683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[4] = 
{
	U3CMessageCallbackU3Ec__AnonStorey1_t2294210683::get_offset_of_subject_0(),
	U3CMessageCallbackU3Ec__AnonStorey1_t2294210683::get_offset_of_payload_1(),
	U3CMessageCallbackU3Ec__AnonStorey1_t2294210683::get_offset_of_receipt_2(),
	U3CMessageCallbackU3Ec__AnonStorey1_t2294210683::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (FakeAppleConfiguation_t4052738437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (FakeAppleExtensions_t4039399289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[1] = 
{
	FakeAppleExtensions_t4039399289::get_offset_of_m_FailRefresh_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (FakeMicrosoftExtensions_t3351923809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (WinRTStore_t36043095), -1, sizeof(WinRTStore_t36043095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2055[7] = 
{
	WinRTStore_t36043095::get_offset_of_win8_0(),
	WinRTStore_t36043095::get_offset_of_callback_1(),
	WinRTStore_t36043095::get_offset_of_util_2(),
	WinRTStore_t36043095::get_offset_of_logger_3(),
	WinRTStore_t36043095::get_offset_of_m_CanReceivePurchases_4(),
	WinRTStore_t36043095_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	WinRTStore_t36043095_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (TizenStoreImpl_t274247241), -1, sizeof(TizenStoreImpl_t274247241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2057[3] = 
{
	TizenStoreImpl_t274247241_StaticFields::get_offset_of_instance_2(),
	TizenStoreImpl_t274247241::get_offset_of_m_Native_3(),
	TizenStoreImpl_t274247241_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (FakeTizenStoreConfiguration_t3055550456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (FacebookStoreImpl_t1362794587), -1, sizeof(FacebookStoreImpl_t1362794587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2059[4] = 
{
	FacebookStoreImpl_t1362794587::get_offset_of_m_Native_2(),
	FacebookStoreImpl_t1362794587_StaticFields::get_offset_of_util_3(),
	FacebookStoreImpl_t1362794587_StaticFields::get_offset_of_instance_4(),
	FacebookStoreImpl_t1362794587_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (U3CMessageCallbackU3Ec__AnonStorey0_t2084418580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[4] = 
{
	U3CMessageCallbackU3Ec__AnonStorey0_t2084418580::get_offset_of_subject_0(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2084418580::get_offset_of_payload_1(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2084418580::get_offset_of_receipt_2(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2084418580::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (FakeMoolahConfiguration_t1681533527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[2] = 
{
	FakeMoolahConfiguration_t1681533527::get_offset_of_m_appKey_0(),
	FakeMoolahConfiguration_t1681533527::get_offset_of_m_hashKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (FakeMoolahExtensions_t1432169715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[2] = 
{
	FakeMoolahExtensions_t1432169715::get_offset_of_m_isLogined_0(),
	FakeMoolahExtensions_t1432169715::get_offset_of_m_register_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (CloudMoolahMode_t206291964)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2063[4] = 
{
	CloudMoolahMode_t206291964::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (LoginResultState_t2459016979)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2065[8] = 
{
	LoginResultState_t2459016979::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (FastRegisterError_t341731807)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2066[5] = 
{
	FastRegisterError_t341731807::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (ValidateReceiptState_t4359597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2067[4] = 
{
	ValidateReceiptState_t4359597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (RestoreTransactionIDState_t2487303652)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2068[5] = 
{
	RestoreTransactionIDState_t2487303652::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (RequestPayOutState_t3537434082)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2069[5] = 
{
	RequestPayOutState_t3537434082::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (TradeSeqState_t1177537792)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2070[7] = 
{
	TradeSeqState_t1177537792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (MoolahStoreImpl_t4206626141), -1, sizeof(MoolahStoreImpl_t4206626141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2072[25] = 
{
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_pollingPath_2(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_requestAuthCodePath_3(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_requestRestoreTransactionUrl_4(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_requestValidateReceiptUrl_5(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_requestProductValidateUrl_6(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_callback_7(),
	MoolahStoreImpl_t4206626141::get_offset_of_isNeedPolling_8(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_productDefinitions_9(),
	MoolahStoreImpl_t4206626141::get_offset_of_authCodeDic_10(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_md5StoreProductID_11(),
	MoolahStoreImpl_t4206626141::get_offset_of_isRequestAuthCodeing_12(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_appKey_13(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_hashKey_14(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_mode_15(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_IsTestMode_16(),
	0,
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_CM_PATH_18(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_REGISTER_OR_BINDING_PATH_19(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_LOGIN_PATH_20(),
	MoolahStoreImpl_t4206626141::get_offset_of_httpManager_21(),
	MoolahStoreImpl_t4206626141::get_offset_of_isLogining_22(),
	MoolahStoreImpl_t4206626141::get_offset_of_isRegister_23(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_LoginToken_24(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_CustomerID_25(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (U3CVaildateProductU3Ec__Iterator0_t1969356947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[10] = 
{
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_appkey_0(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_U3CsignU3E__0_1(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_U3CwfU3E__0_2(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_productInfo_3(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_U3CwU3E__0_4(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_result_5(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_U24this_6(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_U24current_7(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_U24disposing_8(),
	U3CVaildateProductU3Ec__Iterator0_t1969356947::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (U3CPurchaseU3Ec__AnonStorey6_t3194505917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[3] = 
{
	U3CPurchaseU3Ec__AnonStorey6_t3194505917::get_offset_of_purchaseSucceed_0(),
	U3CPurchaseU3Ec__AnonStorey6_t3194505917::get_offset_of_purchaseFailed_1(),
	U3CPurchaseU3Ec__AnonStorey6_t3194505917::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (U3CRequestAuthCodeU3Ec__Iterator1_t3192418403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[10] = 
{
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_wf_0(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_U3CwU3E__0_1(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_failed_2(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_productID_3(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_succeed_4(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_transactionId_5(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_U24this_6(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_U24current_7(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_U24disposing_8(),
	U3CRequestAuthCodeU3Ec__Iterator1_t3192418403::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (U3CStartPurchasePollingU3Ec__Iterator2_t1718677452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[14] = 
{
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U3CorderSuccessU3E__0_0(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_authGlobal_1(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_transactionId_2(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U3CsignstrU3E__0_3(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U3CsignU3E__0_4(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U3CparamU3E__0_5(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U3CurlU3E__0_6(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U3CpollingstrU3E__0_7(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_purchaseSucceed_8(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_purchaseFailed_9(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U24this_10(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U24current_11(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U24disposing_12(),
	U3CStartPurchasePollingU3Ec__Iterator2_t1718677452::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (U3CLoginU3Ec__AnonStorey7_t3027540100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[2] = 
{
	U3CLoginU3Ec__AnonStorey7_t3027540100::get_offset_of_LoginResult_0(),
	U3CLoginU3Ec__AnonStorey7_t3027540100::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (U3CFastRegisterU3Ec__AnonStorey8_t706752957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[3] = 
{
	U3CFastRegisterU3Ec__AnonStorey8_t706752957::get_offset_of_RegisterFailed_0(),
	U3CFastRegisterU3Ec__AnonStorey8_t706752957::get_offset_of_RegisterSucceed_1(),
	U3CFastRegisterU3Ec__AnonStorey8_t706752957::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[13] = 
{
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U3CcustomIDU3E__0_0(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_result_1(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U3CwfU3E__0_2(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U3CnowU3E__0_3(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U3CendDateU3E__0_4(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U3CupperWeekU3E__0_5(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U3CstartDateU3E__0_6(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U3CsignU3E__0_7(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U3CwU3E__0_8(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U24this_9(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U24current_10(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U24disposing_11(),
	U3CRestoreTransactionIDProcessU3Ec__Iterator3_t2861560799::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[11] = 
{
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_receipt_0(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_U3CtempJsonU3E__0_1(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_U3CwfU3E__0_2(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_U3CsignU3E__0_3(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_U3CwU3E__0_4(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_result_5(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_transactionId_6(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_U24this_7(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_U24current_8(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_U24disposing_9(),
	U3CValidateReceiptProcessU3Ec__Iterator4_t1464170479::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (U3CPayOutProcessU3Ec__Iterator5_t1516057908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[13] = 
{
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_transactionId_0(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U3CauthGlobalU3E__0_1(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_result_2(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U3CorderSuccessU3E__0_3(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U3CsignstrU3E__0_4(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U3CsignU3E__0_5(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U3CparamU3E__0_6(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U3CurlU3E__0_7(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U3CwU3E__0_8(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U24this_9(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U24current_10(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U24disposing_11(),
	U3CPayOutProcessU3Ec__Iterator5_t1516057908::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (MoolahUtil_t1395207074), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (PayMethod_t1558319955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (AndroidStore_t3203055206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2084[6] = 
{
	AndroidStore_t3203055206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (FakeStoreUIMode_t2321492887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2085[4] = 
{
	FakeStoreUIMode_t2321492887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (FakeStore_t3882981564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[5] = 
{
	FakeStore_t3882981564::get_offset_of_m_Biller_0(),
	FakeStore_t3882981564::get_offset_of_m_PurchasedProducts_1(),
	FakeStore_t3882981564::get_offset_of_purchaseCalled_2(),
	FakeStore_t3882981564::get_offset_of_U3CunavailableProductIdU3Ek__BackingField_3(),
	FakeStore_t3882981564::get_offset_of_UIMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (DialogType_t1733969544)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2087[3] = 
{
	DialogType_t1733969544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[2] = 
{
	U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567::get_offset_of_products_0(),
	U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (U3CPurchaseU3Ec__AnonStorey1_t2000504963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[2] = 
{
	U3CPurchaseU3Ec__AnonStorey1_t2000504963::get_offset_of_product_0(),
	U3CPurchaseU3Ec__AnonStorey1_t2000504963::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (Price_t1853024949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[3] = 
{
	Price_t1853024949::get_offset_of_value_0(),
	Price_t1853024949::get_offset_of_data_1(),
	Price_t1853024949::get_offset_of_num_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (StoreID_t471452324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[2] = 
{
	StoreID_t471452324::get_offset_of_store_0(),
	StoreID_t471452324::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (GoogleLocale_t3144343871)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[20] = 
{
	GoogleLocale_t3144343871::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (LocalizedProductDescription_t1525635964), -1, sizeof(LocalizedProductDescription_t1525635964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2096[4] = 
{
	LocalizedProductDescription_t1525635964::get_offset_of_googleLocale_0(),
	LocalizedProductDescription_t1525635964::get_offset_of_title_1(),
	LocalizedProductDescription_t1525635964::get_offset_of_description_2(),
	LocalizedProductDescription_t1525635964_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (ProductCatalogItem_t977711995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2097[7] = 
{
	ProductCatalogItem_t977711995::get_offset_of_id_0(),
	ProductCatalogItem_t977711995::get_offset_of_type_1(),
	ProductCatalogItem_t977711995::get_offset_of_storeIDs_2(),
	ProductCatalogItem_t977711995::get_offset_of_defaultDescription_3(),
	ProductCatalogItem_t977711995::get_offset_of_applePriceTier_4(),
	ProductCatalogItem_t977711995::get_offset_of_googlePrice_5(),
	ProductCatalogItem_t977711995::get_offset_of_descriptions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (ProductCatalog_t2667590766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[1] = 
{
	ProductCatalog_t2667590766::get_offset_of_products_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (RawStoreProvider_t3477922056), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
