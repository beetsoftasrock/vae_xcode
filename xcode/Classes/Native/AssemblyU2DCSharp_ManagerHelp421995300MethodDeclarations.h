﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManagerHelp
struct ManagerHelp_t421995300;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ManagerHelp::.ctor()
extern "C"  void ManagerHelp__ctor_m1853676249 (ManagerHelp_t421995300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ManagerHelp ManagerHelp::get_Instance()
extern "C"  ManagerHelp_t421995300 * ManagerHelp_get_Instance_m2341503794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ManagerHelp::get_PrefabHelp()
extern "C"  GameObject_t1756533147 * ManagerHelp_get_PrefabHelp_m1563840572 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerHelp::set_PrefabHelp(UnityEngine.GameObject)
extern "C"  void ManagerHelp_set_PrefabHelp_m310983659 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform ManagerHelp::CloneHelpPrefab(System.String)
extern "C"  Transform_t3275118058 * ManagerHelp_CloneHelpPrefab_m2832309379 (ManagerHelp_t421995300 * __this, String_t* ___bundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerHelp::OpenHelp(System.String,System.String)
extern "C"  void ManagerHelp_OpenHelp_m2434843834 (ManagerHelp_t421995300 * __this, String_t* ___prefixNameHelpFor0, String_t* ___bundleName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerHelp::NextHelp()
extern "C"  void ManagerHelp_NextHelp_m631110111 (ManagerHelp_t421995300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerHelp::SetSprite()
extern "C"  void ManagerHelp_SetSprite_m3731999634 (ManagerHelp_t421995300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerHelp::LoadSprite(System.String,System.String)
extern "C"  void ManagerHelp_LoadSprite_m1266953968 (ManagerHelp_t421995300 * __this, String_t* ___bundleName0, String_t* ___prefixName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
