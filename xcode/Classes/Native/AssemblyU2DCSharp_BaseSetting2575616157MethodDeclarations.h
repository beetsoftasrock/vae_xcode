﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseSetting
struct BaseSetting_t2575616157;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1021602117;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"

// System.Void BaseSetting::.ctor()
extern "C"  void BaseSetting__ctor_m4290264246 (BaseSetting_t2575616157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseSetting::AppendValue(System.String,UnityEngine.Object)
extern "C"  void BaseSetting_AppendValue_m1342025229 (BaseSetting_t2575616157 * __this, String_t* ___key0, Object_t1021602117 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
