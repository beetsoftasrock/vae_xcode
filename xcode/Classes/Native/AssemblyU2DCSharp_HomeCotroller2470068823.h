﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalConfig
struct GlobalConfig_t3080413471;
// VaeBuildSetting
struct VaeBuildSetting_t3353517300;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// IAP_UIScript
struct IAP_UIScript_t1776166942;
// System.Collections.Generic.List`1<CachedDragInfo>
struct List_1_t505826924;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_HomeCotroller_State4150594815.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeCotroller
struct  HomeCotroller_t2470068823  : public MonoBehaviour_t1158329972
{
public:
	// GlobalConfig HomeCotroller::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_2;
	// VaeBuildSetting HomeCotroller::buildSetting
	VaeBuildSetting_t3353517300 * ___buildSetting_3;
	// HomeCotroller/State HomeCotroller::doneState
	int32_t ___doneState_4;
	// UnityEngine.Transform HomeCotroller::prefabTutorial
	Transform_t3275118058 * ___prefabTutorial_5;
	// UnityEngine.UI.Text HomeCotroller::txtUserName
	Text_t356221433 * ___txtUserName_6;
	// System.Single HomeCotroller::speedMoving
	float ___speedMoving_7;
	// System.Boolean HomeCotroller::_isMoving
	bool ____isMoving_8;
	// UnityEngine.UI.Image HomeCotroller::imgOverlay
	Image_t2042527209 * ___imgOverlay_9;
	// UnityEngine.Vector2 HomeCotroller::_desPosOpen
	Vector2_t2243707579  ____desPosOpen_10;
	// UnityEngine.Vector2 HomeCotroller::_desPosClose
	Vector2_t2243707579  ____desPosClose_11;
	// UnityEngine.GameObject HomeCotroller::popupPolicy
	GameObject_t1756533147 * ___popupPolicy_12;
	// UnityEngine.GameObject HomeCotroller::MenuPanel
	GameObject_t1756533147 * ___MenuPanel_13;
	// UnityEngine.GameObject HomeCotroller::bg
	GameObject_t1756533147 * ___bg_14;
	// UnityEngine.UI.Text HomeCotroller::userIdTxt
	Text_t356221433 * ___userIdTxt_15;
	// UnityEngine.UI.Text HomeCotroller::text_Version
	Text_t356221433 * ___text_Version_16;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HomeCotroller::touchPositions
	List_1_t1612828712 * ___touchPositions_17;
	// IAP_UIScript HomeCotroller::iapUiScript
	IAP_UIScript_t1776166942 * ___iapUiScript_18;
	// UnityEngine.Vector2 HomeCotroller::_startMousePosition
	Vector2_t2243707579  ____startMousePosition_21;
	// System.Collections.Generic.List`1<CachedDragInfo> HomeCotroller::_dragDatas
	List_1_t505826924 * ____dragDatas_22;
	// UnityEngine.Coroutine HomeCotroller::_currentCoroutine
	Coroutine_t2299508840 * ____currentCoroutine_23;
	// UnityEngine.RectTransform HomeCotroller::target
	RectTransform_t3349966182 * ___target_24;
	// UnityEngine.Vector2 HomeCotroller::_startPosition
	Vector2_t2243707579  ____startPosition_25;

public:
	inline static int32_t get_offset_of_globalConfig_2() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___globalConfig_2)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_2() const { return ___globalConfig_2; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_2() { return &___globalConfig_2; }
	inline void set_globalConfig_2(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_2 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_2, value);
	}

	inline static int32_t get_offset_of_buildSetting_3() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___buildSetting_3)); }
	inline VaeBuildSetting_t3353517300 * get_buildSetting_3() const { return ___buildSetting_3; }
	inline VaeBuildSetting_t3353517300 ** get_address_of_buildSetting_3() { return &___buildSetting_3; }
	inline void set_buildSetting_3(VaeBuildSetting_t3353517300 * value)
	{
		___buildSetting_3 = value;
		Il2CppCodeGenWriteBarrier(&___buildSetting_3, value);
	}

	inline static int32_t get_offset_of_doneState_4() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___doneState_4)); }
	inline int32_t get_doneState_4() const { return ___doneState_4; }
	inline int32_t* get_address_of_doneState_4() { return &___doneState_4; }
	inline void set_doneState_4(int32_t value)
	{
		___doneState_4 = value;
	}

	inline static int32_t get_offset_of_prefabTutorial_5() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___prefabTutorial_5)); }
	inline Transform_t3275118058 * get_prefabTutorial_5() const { return ___prefabTutorial_5; }
	inline Transform_t3275118058 ** get_address_of_prefabTutorial_5() { return &___prefabTutorial_5; }
	inline void set_prefabTutorial_5(Transform_t3275118058 * value)
	{
		___prefabTutorial_5 = value;
		Il2CppCodeGenWriteBarrier(&___prefabTutorial_5, value);
	}

	inline static int32_t get_offset_of_txtUserName_6() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___txtUserName_6)); }
	inline Text_t356221433 * get_txtUserName_6() const { return ___txtUserName_6; }
	inline Text_t356221433 ** get_address_of_txtUserName_6() { return &___txtUserName_6; }
	inline void set_txtUserName_6(Text_t356221433 * value)
	{
		___txtUserName_6 = value;
		Il2CppCodeGenWriteBarrier(&___txtUserName_6, value);
	}

	inline static int32_t get_offset_of_speedMoving_7() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___speedMoving_7)); }
	inline float get_speedMoving_7() const { return ___speedMoving_7; }
	inline float* get_address_of_speedMoving_7() { return &___speedMoving_7; }
	inline void set_speedMoving_7(float value)
	{
		___speedMoving_7 = value;
	}

	inline static int32_t get_offset_of__isMoving_8() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ____isMoving_8)); }
	inline bool get__isMoving_8() const { return ____isMoving_8; }
	inline bool* get_address_of__isMoving_8() { return &____isMoving_8; }
	inline void set__isMoving_8(bool value)
	{
		____isMoving_8 = value;
	}

	inline static int32_t get_offset_of_imgOverlay_9() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___imgOverlay_9)); }
	inline Image_t2042527209 * get_imgOverlay_9() const { return ___imgOverlay_9; }
	inline Image_t2042527209 ** get_address_of_imgOverlay_9() { return &___imgOverlay_9; }
	inline void set_imgOverlay_9(Image_t2042527209 * value)
	{
		___imgOverlay_9 = value;
		Il2CppCodeGenWriteBarrier(&___imgOverlay_9, value);
	}

	inline static int32_t get_offset_of__desPosOpen_10() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ____desPosOpen_10)); }
	inline Vector2_t2243707579  get__desPosOpen_10() const { return ____desPosOpen_10; }
	inline Vector2_t2243707579 * get_address_of__desPosOpen_10() { return &____desPosOpen_10; }
	inline void set__desPosOpen_10(Vector2_t2243707579  value)
	{
		____desPosOpen_10 = value;
	}

	inline static int32_t get_offset_of__desPosClose_11() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ____desPosClose_11)); }
	inline Vector2_t2243707579  get__desPosClose_11() const { return ____desPosClose_11; }
	inline Vector2_t2243707579 * get_address_of__desPosClose_11() { return &____desPosClose_11; }
	inline void set__desPosClose_11(Vector2_t2243707579  value)
	{
		____desPosClose_11 = value;
	}

	inline static int32_t get_offset_of_popupPolicy_12() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___popupPolicy_12)); }
	inline GameObject_t1756533147 * get_popupPolicy_12() const { return ___popupPolicy_12; }
	inline GameObject_t1756533147 ** get_address_of_popupPolicy_12() { return &___popupPolicy_12; }
	inline void set_popupPolicy_12(GameObject_t1756533147 * value)
	{
		___popupPolicy_12 = value;
		Il2CppCodeGenWriteBarrier(&___popupPolicy_12, value);
	}

	inline static int32_t get_offset_of_MenuPanel_13() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___MenuPanel_13)); }
	inline GameObject_t1756533147 * get_MenuPanel_13() const { return ___MenuPanel_13; }
	inline GameObject_t1756533147 ** get_address_of_MenuPanel_13() { return &___MenuPanel_13; }
	inline void set_MenuPanel_13(GameObject_t1756533147 * value)
	{
		___MenuPanel_13 = value;
		Il2CppCodeGenWriteBarrier(&___MenuPanel_13, value);
	}

	inline static int32_t get_offset_of_bg_14() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___bg_14)); }
	inline GameObject_t1756533147 * get_bg_14() const { return ___bg_14; }
	inline GameObject_t1756533147 ** get_address_of_bg_14() { return &___bg_14; }
	inline void set_bg_14(GameObject_t1756533147 * value)
	{
		___bg_14 = value;
		Il2CppCodeGenWriteBarrier(&___bg_14, value);
	}

	inline static int32_t get_offset_of_userIdTxt_15() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___userIdTxt_15)); }
	inline Text_t356221433 * get_userIdTxt_15() const { return ___userIdTxt_15; }
	inline Text_t356221433 ** get_address_of_userIdTxt_15() { return &___userIdTxt_15; }
	inline void set_userIdTxt_15(Text_t356221433 * value)
	{
		___userIdTxt_15 = value;
		Il2CppCodeGenWriteBarrier(&___userIdTxt_15, value);
	}

	inline static int32_t get_offset_of_text_Version_16() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___text_Version_16)); }
	inline Text_t356221433 * get_text_Version_16() const { return ___text_Version_16; }
	inline Text_t356221433 ** get_address_of_text_Version_16() { return &___text_Version_16; }
	inline void set_text_Version_16(Text_t356221433 * value)
	{
		___text_Version_16 = value;
		Il2CppCodeGenWriteBarrier(&___text_Version_16, value);
	}

	inline static int32_t get_offset_of_touchPositions_17() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___touchPositions_17)); }
	inline List_1_t1612828712 * get_touchPositions_17() const { return ___touchPositions_17; }
	inline List_1_t1612828712 ** get_address_of_touchPositions_17() { return &___touchPositions_17; }
	inline void set_touchPositions_17(List_1_t1612828712 * value)
	{
		___touchPositions_17 = value;
		Il2CppCodeGenWriteBarrier(&___touchPositions_17, value);
	}

	inline static int32_t get_offset_of_iapUiScript_18() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___iapUiScript_18)); }
	inline IAP_UIScript_t1776166942 * get_iapUiScript_18() const { return ___iapUiScript_18; }
	inline IAP_UIScript_t1776166942 ** get_address_of_iapUiScript_18() { return &___iapUiScript_18; }
	inline void set_iapUiScript_18(IAP_UIScript_t1776166942 * value)
	{
		___iapUiScript_18 = value;
		Il2CppCodeGenWriteBarrier(&___iapUiScript_18, value);
	}

	inline static int32_t get_offset_of__startMousePosition_21() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ____startMousePosition_21)); }
	inline Vector2_t2243707579  get__startMousePosition_21() const { return ____startMousePosition_21; }
	inline Vector2_t2243707579 * get_address_of__startMousePosition_21() { return &____startMousePosition_21; }
	inline void set__startMousePosition_21(Vector2_t2243707579  value)
	{
		____startMousePosition_21 = value;
	}

	inline static int32_t get_offset_of__dragDatas_22() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ____dragDatas_22)); }
	inline List_1_t505826924 * get__dragDatas_22() const { return ____dragDatas_22; }
	inline List_1_t505826924 ** get_address_of__dragDatas_22() { return &____dragDatas_22; }
	inline void set__dragDatas_22(List_1_t505826924 * value)
	{
		____dragDatas_22 = value;
		Il2CppCodeGenWriteBarrier(&____dragDatas_22, value);
	}

	inline static int32_t get_offset_of__currentCoroutine_23() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ____currentCoroutine_23)); }
	inline Coroutine_t2299508840 * get__currentCoroutine_23() const { return ____currentCoroutine_23; }
	inline Coroutine_t2299508840 ** get_address_of__currentCoroutine_23() { return &____currentCoroutine_23; }
	inline void set__currentCoroutine_23(Coroutine_t2299508840 * value)
	{
		____currentCoroutine_23 = value;
		Il2CppCodeGenWriteBarrier(&____currentCoroutine_23, value);
	}

	inline static int32_t get_offset_of_target_24() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ___target_24)); }
	inline RectTransform_t3349966182 * get_target_24() const { return ___target_24; }
	inline RectTransform_t3349966182 ** get_address_of_target_24() { return &___target_24; }
	inline void set_target_24(RectTransform_t3349966182 * value)
	{
		___target_24 = value;
		Il2CppCodeGenWriteBarrier(&___target_24, value);
	}

	inline static int32_t get_offset_of__startPosition_25() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823, ____startPosition_25)); }
	inline Vector2_t2243707579  get__startPosition_25() const { return ____startPosition_25; }
	inline Vector2_t2243707579 * get_address_of__startPosition_25() { return &____startPosition_25; }
	inline void set__startPosition_25(Vector2_t2243707579  value)
	{
		____startPosition_25 = value;
	}
};

struct HomeCotroller_t2470068823_StaticFields
{
public:
	// System.Action HomeCotroller::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_26;
	// System.Action HomeCotroller::<>f__am$cache1
	Action_t3226471752 * ___U3CU3Ef__amU24cache1_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_26() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823_StaticFields, ___U3CU3Ef__amU24cache0_26)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_26() const { return ___U3CU3Ef__amU24cache0_26; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_26() { return &___U3CU3Ef__amU24cache0_26; }
	inline void set_U3CU3Ef__amU24cache0_26(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_27() { return static_cast<int32_t>(offsetof(HomeCotroller_t2470068823_StaticFields, ___U3CU3Ef__amU24cache1_27)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1_27() const { return ___U3CU3Ef__amU24cache1_27; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1_27() { return &___U3CU3Ef__amU24cache1_27; }
	inline void set_U3CU3Ef__amU24cache1_27(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
