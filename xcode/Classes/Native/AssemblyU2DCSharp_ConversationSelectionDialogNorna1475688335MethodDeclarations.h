﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionDialogNornal
struct ConversationSelectionDialogNornal_t1475688335;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationSelectionGroupData
struct ConversationSelectionGroupData_t3437723178;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationSelectionGroupData3437723178.h"
#include "AssemblyU2DCSharp_ConversationSelectionData4090008535.h"

// System.Void ConversationSelectionDialogNornal::.ctor()
extern "C"  void ConversationSelectionDialogNornal__ctor_m2941425352 (ConversationSelectionDialogNornal_t1475688335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConversationSelectionDialogNornal::get_showSubText()
extern "C"  bool ConversationSelectionDialogNornal_get_showSubText_m781584949 (ConversationSelectionDialogNornal_t1475688335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogNornal::set_showSubText(System.Boolean)
extern "C"  void ConversationSelectionDialogNornal_set_showSubText_m3832444428 (ConversationSelectionDialogNornal_t1475688335 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogNornal::UpdateSubtext()
extern "C"  void ConversationSelectionDialogNornal_UpdateSubtext_m2548499726 (ConversationSelectionDialogNornal_t1475688335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogNornal::Clear()
extern "C"  void ConversationSelectionDialogNornal_Clear_m621101699 (ConversationSelectionDialogNornal_t1475688335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ConversationSelectionDialogNornal::ShowSelection(ConversationSelectionGroupData)
extern "C"  Il2CppObject * ConversationSelectionDialogNornal_ShowSelection_m1635836093 (ConversationSelectionDialogNornal_t1475688335 * __this, ConversationSelectionGroupData_t3437723178 * ___gdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogNornal::ShowSelectionPanel(ConversationSelectionGroupData)
extern "C"  void ConversationSelectionDialogNornal_ShowSelectionPanel_m4111340717 (ConversationSelectionDialogNornal_t1475688335 * __this, ConversationSelectionGroupData_t3437723178 * ___gdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogNornal::SetSelected(ConversationSelectionData)
extern "C"  void ConversationSelectionDialogNornal_SetSelected_m2150973720 (ConversationSelectionDialogNornal_t1475688335 * __this, ConversationSelectionData_t4090008535 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationSelectionData ConversationSelectionDialogNornal::GetSelected()
extern "C"  ConversationSelectionData_t4090008535 * ConversationSelectionDialogNornal_GetSelected_m3744245911 (ConversationSelectionDialogNornal_t1475688335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
