﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExcerciseSceneSelector
struct ExcerciseSceneSelector_t1138496382;
// System.String
struct String_t;
// LearningLogResponse
struct LearningLogResponse_t112900141;
// LearningLogData[]
struct LearningLogDataU5BU5D_t3295896437;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_LearningLogResponse112900141.h"

// System.Void ExcerciseSceneSelector::.ctor()
extern "C"  void ExcerciseSceneSelector__ctor_m422581621 (ExcerciseSceneSelector_t1138496382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExcerciseSceneSelector::get_currentSelectedId()
extern "C"  int32_t ExcerciseSceneSelector_get_currentSelectedId_m4039760227 (ExcerciseSceneSelector_t1138496382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::set_currentSelectedId(System.Int32)
extern "C"  void ExcerciseSceneSelector_set_currentSelectedId_m1697604202 (ExcerciseSceneSelector_t1138496382 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::UpdateView()
extern "C"  void ExcerciseSceneSelector_UpdateView_m2422661101 (ExcerciseSceneSelector_t1138496382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::OnExcerciseSceneSelectionViewClicked(System.Int32)
extern "C"  void ExcerciseSceneSelector_OnExcerciseSceneSelectionViewClicked_m2066982238 (ExcerciseSceneSelector_t1138496382 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::Awake()
extern "C"  void ExcerciseSceneSelector_Awake_m4084115898 (ExcerciseSceneSelector_t1138496382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::Start()
extern "C"  void ExcerciseSceneSelector_Start_m2156177085 (ExcerciseSceneSelector_t1138496382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::UpdatePercent()
extern "C"  void ExcerciseSceneSelector_UpdatePercent_m1783630251 (ExcerciseSceneSelector_t1138496382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::GetLearningData(System.String,System.Int32,System.DateTime,System.DateTime)
extern "C"  void ExcerciseSceneSelector_GetLearningData_m4200857610 (ExcerciseSceneSelector_t1138496382 * __this, String_t* ___uuid0, int32_t ___chapter1, DateTime_t693205669  ___startDate2, DateTime_t693205669  ___endDate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::HandleLearningDataResponse(LearningLogResponse)
extern "C"  void ExcerciseSceneSelector_HandleLearningDataResponse_m2780919827 (ExcerciseSceneSelector_t1138496382 * __this, LearningLogResponse_t112900141 * ___learningLogResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::UpdateLearningDatas(LearningLogData[])
extern "C"  void ExcerciseSceneSelector_UpdateLearningDatas_m3347145757 (ExcerciseSceneSelector_t1138496382 * __this, LearningLogDataU5BU5D_t3295896437* ___learningLogs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelector::<GetLearningData>m__0()
extern "C"  void ExcerciseSceneSelector_U3CGetLearningDataU3Em__0_m3228042614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
