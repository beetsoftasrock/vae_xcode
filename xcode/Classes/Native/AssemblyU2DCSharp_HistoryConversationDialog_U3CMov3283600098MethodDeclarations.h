﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2
struct U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::.ctor()
extern "C"  void U3CMoveScrollViewToBottomU3Ec__Iterator2__ctor_m3458762811 (U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::MoveNext()
extern "C"  bool U3CMoveScrollViewToBottomU3Ec__Iterator2_MoveNext_m1827828805 (U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveScrollViewToBottomU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1122009105 (U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveScrollViewToBottomU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1106990793 (U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::Dispose()
extern "C"  void U3CMoveScrollViewToBottomU3Ec__Iterator2_Dispose_m4247178024 (U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::Reset()
extern "C"  void U3CMoveScrollViewToBottomU3Ec__Iterator2_Reset_m2667145138 (U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
