﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSelectionData
struct  ConversationSelectionData_t4090008535  : public Il2CppObject
{
public:
	// System.String ConversationSelectionData::destination
	String_t* ___destination_0;
	// System.String ConversationSelectionData::textEN
	String_t* ___textEN_1;
	// System.String ConversationSelectionData::textJP
	String_t* ___textJP_2;
	// System.String ConversationSelectionData::soundData
	String_t* ___soundData_3;

public:
	inline static int32_t get_offset_of_destination_0() { return static_cast<int32_t>(offsetof(ConversationSelectionData_t4090008535, ___destination_0)); }
	inline String_t* get_destination_0() const { return ___destination_0; }
	inline String_t** get_address_of_destination_0() { return &___destination_0; }
	inline void set_destination_0(String_t* value)
	{
		___destination_0 = value;
		Il2CppCodeGenWriteBarrier(&___destination_0, value);
	}

	inline static int32_t get_offset_of_textEN_1() { return static_cast<int32_t>(offsetof(ConversationSelectionData_t4090008535, ___textEN_1)); }
	inline String_t* get_textEN_1() const { return ___textEN_1; }
	inline String_t** get_address_of_textEN_1() { return &___textEN_1; }
	inline void set_textEN_1(String_t* value)
	{
		___textEN_1 = value;
		Il2CppCodeGenWriteBarrier(&___textEN_1, value);
	}

	inline static int32_t get_offset_of_textJP_2() { return static_cast<int32_t>(offsetof(ConversationSelectionData_t4090008535, ___textJP_2)); }
	inline String_t* get_textJP_2() const { return ___textJP_2; }
	inline String_t** get_address_of_textJP_2() { return &___textJP_2; }
	inline void set_textJP_2(String_t* value)
	{
		___textJP_2 = value;
		Il2CppCodeGenWriteBarrier(&___textJP_2, value);
	}

	inline static int32_t get_offset_of_soundData_3() { return static_cast<int32_t>(offsetof(ConversationSelectionData_t4090008535, ___soundData_3)); }
	inline String_t* get_soundData_3() const { return ___soundData_3; }
	inline String_t** get_address_of_soundData_3() { return &___soundData_3; }
	inline void set_soundData_3(String_t* value)
	{
		___soundData_3 = value;
		Il2CppCodeGenWriteBarrier(&___soundData_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
