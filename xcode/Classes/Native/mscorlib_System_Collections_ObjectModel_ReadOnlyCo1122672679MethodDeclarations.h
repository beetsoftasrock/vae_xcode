﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m904648597(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3671019970_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2149809863(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, SentenceStructureIdiomOnPlayQuestion_t936886987 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2989589458_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1450728579(__this, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m454937302_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m532775382(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, SentenceStructureIdiomOnPlayQuestion_t936886987 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4272763307_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1138996016(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1122672679 *, SentenceStructureIdiomOnPlayQuestion_t936886987 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3199809075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2105039530(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m962041751_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2104606726(__this, ___index0, method) ((  SentenceStructureIdiomOnPlayQuestion_t936886987 * (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m70085287_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2305609947(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, SentenceStructureIdiomOnPlayQuestion_t936886987 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1547026160_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3090411927(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4041967064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1884197048(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3664791405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2626830091(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m531171980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1733851028(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1122672679 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3780136817_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1054915062(__this, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3983677501_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2994725482(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1122672679 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1990607517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1411938790(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1122672679 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m606942423_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3848780083(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m691705570_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3505364403(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3182494192_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2675049757(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m572840272_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1776053052(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2871048729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3011457474(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m769863805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1395702587(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m942145650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1018188952(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1367736517_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2430437375(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3336878134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3440214262(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799572719_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::Contains(T)
#define ReadOnlyCollection_1_Contains_m29582937(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1122672679 *, SentenceStructureIdiomOnPlayQuestion_t936886987 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1227826160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1869903427(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1122672679 *, SentenceStructureIdiomOnPlayQuestionU5BU5D_t3172342186*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4257276542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m2632933792(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1627519329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m307317103(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1122672679 *, SentenceStructureIdiomOnPlayQuestion_t936886987 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1981423404_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::get_Count()
#define ReadOnlyCollection_1_get_Count_m4170366916(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1122672679 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomOnPlayQuestion>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m606831842(__this, ___index0, method) ((  SentenceStructureIdiomOnPlayQuestion_t936886987 * (*) (ReadOnlyCollection_1_t1122672679 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m191392387_gshared)(__this, ___index0, method)
