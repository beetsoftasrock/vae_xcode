﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionGroupData
struct ConversationSelectionGroupData_t3437723178;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationSelectionGroupData::.ctor()
extern "C"  void ConversationSelectionGroupData__ctor_m20614293 (ConversationSelectionGroupData_t3437723178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
