﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeDeviceResponse
struct ChangeDeviceResponse_t284910379;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangeDeviceResponse::.ctor()
extern "C"  void ChangeDeviceResponse__ctor_m3397656640 (ChangeDeviceResponse_t284910379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
