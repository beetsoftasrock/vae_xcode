﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectCommand/<Execute>c__Iterator1
struct U3CExecuteU3Ec__Iterator1_t564345066;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectCommand/<Execute>c__Iterator1::.ctor()
extern "C"  void U3CExecuteU3Ec__Iterator1__ctor_m180160341 (U3CExecuteU3Ec__Iterator1_t564345066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EffectCommand/<Execute>c__Iterator1::MoveNext()
extern "C"  bool U3CExecuteU3Ec__Iterator1_MoveNext_m1280612647 (U3CExecuteU3Ec__Iterator1_t564345066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EffectCommand/<Execute>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3282683035 (U3CExecuteU3Ec__Iterator1_t564345066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EffectCommand/<Execute>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1667872115 (U3CExecuteU3Ec__Iterator1_t564345066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCommand/<Execute>c__Iterator1::Dispose()
extern "C"  void U3CExecuteU3Ec__Iterator1_Dispose_m2441042300 (U3CExecuteU3Ec__Iterator1_t564345066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCommand/<Execute>c__Iterator1::Reset()
extern "C"  void U3CExecuteU3Ec__Iterator1_Reset_m2444515466 (U3CExecuteU3Ec__Iterator1_t564345066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
