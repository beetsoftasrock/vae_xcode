﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetProductID
struct  GetProductID_t3571953046  : public MonoBehaviour_t1158329972
{
public:
	// System.String GetProductID::productID
	String_t* ___productID_2;
	// System.String GetProductID::suffixesID
	String_t* ___suffixesID_3;
	// System.String GetProductID::prefixID
	String_t* ___prefixID_4;
	// UnityEngine.UI.Text GetProductID::txtDebug
	Text_t356221433 * ___txtDebug_6;

public:
	inline static int32_t get_offset_of_productID_2() { return static_cast<int32_t>(offsetof(GetProductID_t3571953046, ___productID_2)); }
	inline String_t* get_productID_2() const { return ___productID_2; }
	inline String_t** get_address_of_productID_2() { return &___productID_2; }
	inline void set_productID_2(String_t* value)
	{
		___productID_2 = value;
		Il2CppCodeGenWriteBarrier(&___productID_2, value);
	}

	inline static int32_t get_offset_of_suffixesID_3() { return static_cast<int32_t>(offsetof(GetProductID_t3571953046, ___suffixesID_3)); }
	inline String_t* get_suffixesID_3() const { return ___suffixesID_3; }
	inline String_t** get_address_of_suffixesID_3() { return &___suffixesID_3; }
	inline void set_suffixesID_3(String_t* value)
	{
		___suffixesID_3 = value;
		Il2CppCodeGenWriteBarrier(&___suffixesID_3, value);
	}

	inline static int32_t get_offset_of_prefixID_4() { return static_cast<int32_t>(offsetof(GetProductID_t3571953046, ___prefixID_4)); }
	inline String_t* get_prefixID_4() const { return ___prefixID_4; }
	inline String_t** get_address_of_prefixID_4() { return &___prefixID_4; }
	inline void set_prefixID_4(String_t* value)
	{
		___prefixID_4 = value;
		Il2CppCodeGenWriteBarrier(&___prefixID_4, value);
	}

	inline static int32_t get_offset_of_txtDebug_6() { return static_cast<int32_t>(offsetof(GetProductID_t3571953046, ___txtDebug_6)); }
	inline Text_t356221433 * get_txtDebug_6() const { return ___txtDebug_6; }
	inline Text_t356221433 ** get_address_of_txtDebug_6() { return &___txtDebug_6; }
	inline void set_txtDebug_6(Text_t356221433 * value)
	{
		___txtDebug_6 = value;
		Il2CppCodeGenWriteBarrier(&___txtDebug_6, value);
	}
};

struct GetProductID_t3571953046_StaticFields
{
public:
	// System.String GetProductID::<ProductID>k__BackingField
	String_t* ___U3CProductIDU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CProductIDU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetProductID_t3571953046_StaticFields, ___U3CProductIDU3Ek__BackingField_5)); }
	inline String_t* get_U3CProductIDU3Ek__BackingField_5() const { return ___U3CProductIDU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CProductIDU3Ek__BackingField_5() { return &___U3CProductIDU3Ek__BackingField_5; }
	inline void set_U3CProductIDU3Ek__BackingField_5(String_t* value)
	{
		___U3CProductIDU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProductIDU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
