﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectionButtonSelectedEvent
struct SelectionButtonSelectedEvent_t3088917173;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSelectionButtonView
struct  ConversationSelectionButtonView_t265815670  : public MonoBehaviour_t1158329972
{
public:
	// SelectionButtonSelectedEvent ConversationSelectionButtonView::onButtonSelected
	SelectionButtonSelectedEvent_t3088917173 * ___onButtonSelected_2;
	// ConversationSelectionData ConversationSelectionButtonView::_data
	ConversationSelectionData_t4090008535 * ____data_3;
	// UnityEngine.UI.Text ConversationSelectionButtonView::buttonIdText
	Text_t356221433 * ___buttonIdText_4;

public:
	inline static int32_t get_offset_of_onButtonSelected_2() { return static_cast<int32_t>(offsetof(ConversationSelectionButtonView_t265815670, ___onButtonSelected_2)); }
	inline SelectionButtonSelectedEvent_t3088917173 * get_onButtonSelected_2() const { return ___onButtonSelected_2; }
	inline SelectionButtonSelectedEvent_t3088917173 ** get_address_of_onButtonSelected_2() { return &___onButtonSelected_2; }
	inline void set_onButtonSelected_2(SelectionButtonSelectedEvent_t3088917173 * value)
	{
		___onButtonSelected_2 = value;
		Il2CppCodeGenWriteBarrier(&___onButtonSelected_2, value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(ConversationSelectionButtonView_t265815670, ____data_3)); }
	inline ConversationSelectionData_t4090008535 * get__data_3() const { return ____data_3; }
	inline ConversationSelectionData_t4090008535 ** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(ConversationSelectionData_t4090008535 * value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier(&____data_3, value);
	}

	inline static int32_t get_offset_of_buttonIdText_4() { return static_cast<int32_t>(offsetof(ConversationSelectionButtonView_t265815670, ___buttonIdText_4)); }
	inline Text_t356221433 * get_buttonIdText_4() const { return ___buttonIdText_4; }
	inline Text_t356221433 ** get_address_of_buttonIdText_4() { return &___buttonIdText_4; }
	inline void set_buttonIdText_4(Text_t356221433 * value)
	{
		___buttonIdText_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttonIdText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
