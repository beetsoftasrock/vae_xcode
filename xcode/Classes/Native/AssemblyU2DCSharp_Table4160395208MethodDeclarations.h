﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Table
struct Table_t4160395208;

#include "codegen/il2cpp-codegen.h"

// System.Void Table::.ctor()
extern "C"  void Table__ctor_m2653629653 (Table_t4160395208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
