﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyPageChapterController
struct MyPageChapterController_t2166380602;
// System.Single[]
struct SingleU5BU5D_t577127397;
// GraphValue[]
struct GraphValueU5BU5D_t3529246652;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MyPageChapterController::.ctor()
extern "C"  void MyPageChapterController__ctor_m496252785 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::Awake()
extern "C"  void MyPageChapterController_Awake_m2119133610 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::Start()
extern "C"  void MyPageChapterController_Start_m2453494081 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::TestValue()
extern "C"  void MyPageChapterController_TestValue_m2386617784 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::SetImageStatus()
extern "C"  void MyPageChapterController_SetImageStatus_m993592094 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::LoadValueFromData()
extern "C"  void MyPageChapterController_LoadValueFromData_m685198456 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MyPageChapterController::GetIndexMinPercent()
extern "C"  int32_t MyPageChapterController_GetIndexMinPercent_m2719727254 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::DrawCurrentGraph()
extern "C"  void MyPageChapterController_DrawCurrentGraph_m1345549828 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] MyPageChapterController::FinalValue(GraphValue[])
extern "C"  SingleU5BU5D_t577127397* MyPageChapterController_FinalValue_m2403429815 (MyPageChapterController_t2166380602 * __this, GraphValueU5BU5D_t3529246652* ___graphValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::DrawRadaGraph(System.Single[])
extern "C"  void MyPageChapterController_DrawRadaGraph_m1055995182 (MyPageChapterController_t2166380602 * __this, SingleU5BU5D_t577127397* ___verticeValues0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::DrawPrevGraph(System.Single[])
extern "C"  void MyPageChapterController_DrawPrevGraph_m2427376323 (MyPageChapterController_t2166380602 * __this, SingleU5BU5D_t577127397* ___verticeValues0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::SetRecommend()
extern "C"  void MyPageChapterController_SetRecommend_m1172718027 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MyPageChapterController::GetSceneNameFromGraphName(System.String)
extern "C"  String_t* MyPageChapterController_GetSceneNameFromGraphName_m1019032652 (MyPageChapterController_t2166380602 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::ShowTextLesson()
extern "C"  void MyPageChapterController_ShowTextLesson_m2769114771 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::DrawTableContent(GraphValue[])
extern "C"  void MyPageChapterController_DrawTableContent_m3162078279 (MyPageChapterController_t2166380602 * __this, GraphValueU5BU5D_t3529246652* ___graphValues0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::ShowTimeTotal()
extern "C"  void MyPageChapterController_ShowTimeTotal_m1299899333 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MyPageChapterController::timeLearnFromHours(System.Single)
extern "C"  String_t* MyPageChapterController_timeLearnFromHours_m2359548949 (MyPageChapterController_t2166380602 * __this, float ___timeOfHours0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageChapterController::<SetRecommend>m__0()
extern "C"  void MyPageChapterController_U3CSetRecommendU3Em__0_m2289758702 (MyPageChapterController_t2166380602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
