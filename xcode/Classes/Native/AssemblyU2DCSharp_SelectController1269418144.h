﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectButton
struct SelectButton_t132497280;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SelectController_SelectedState323245347.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectController
struct  SelectController_t1269418144  : public MonoBehaviour_t1158329972
{
public:
	// SelectController/SelectedState SelectController::<currentSelected>k__BackingField
	int32_t ___U3CcurrentSelectedU3Ek__BackingField_2;
	// SelectButton SelectController::left
	SelectButton_t132497280 * ___left_3;
	// SelectButton SelectController::right
	SelectButton_t132497280 * ___right_4;
	// UnityEngine.GameObject SelectController::leftSelectUI
	GameObject_t1756533147 * ___leftSelectUI_5;
	// UnityEngine.GameObject SelectController::rightSelectUI
	GameObject_t1756533147 * ___rightSelectUI_6;
	// System.Boolean SelectController::selectRight
	bool ___selectRight_7;

public:
	inline static int32_t get_offset_of_U3CcurrentSelectedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SelectController_t1269418144, ___U3CcurrentSelectedU3Ek__BackingField_2)); }
	inline int32_t get_U3CcurrentSelectedU3Ek__BackingField_2() const { return ___U3CcurrentSelectedU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CcurrentSelectedU3Ek__BackingField_2() { return &___U3CcurrentSelectedU3Ek__BackingField_2; }
	inline void set_U3CcurrentSelectedU3Ek__BackingField_2(int32_t value)
	{
		___U3CcurrentSelectedU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_left_3() { return static_cast<int32_t>(offsetof(SelectController_t1269418144, ___left_3)); }
	inline SelectButton_t132497280 * get_left_3() const { return ___left_3; }
	inline SelectButton_t132497280 ** get_address_of_left_3() { return &___left_3; }
	inline void set_left_3(SelectButton_t132497280 * value)
	{
		___left_3 = value;
		Il2CppCodeGenWriteBarrier(&___left_3, value);
	}

	inline static int32_t get_offset_of_right_4() { return static_cast<int32_t>(offsetof(SelectController_t1269418144, ___right_4)); }
	inline SelectButton_t132497280 * get_right_4() const { return ___right_4; }
	inline SelectButton_t132497280 ** get_address_of_right_4() { return &___right_4; }
	inline void set_right_4(SelectButton_t132497280 * value)
	{
		___right_4 = value;
		Il2CppCodeGenWriteBarrier(&___right_4, value);
	}

	inline static int32_t get_offset_of_leftSelectUI_5() { return static_cast<int32_t>(offsetof(SelectController_t1269418144, ___leftSelectUI_5)); }
	inline GameObject_t1756533147 * get_leftSelectUI_5() const { return ___leftSelectUI_5; }
	inline GameObject_t1756533147 ** get_address_of_leftSelectUI_5() { return &___leftSelectUI_5; }
	inline void set_leftSelectUI_5(GameObject_t1756533147 * value)
	{
		___leftSelectUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___leftSelectUI_5, value);
	}

	inline static int32_t get_offset_of_rightSelectUI_6() { return static_cast<int32_t>(offsetof(SelectController_t1269418144, ___rightSelectUI_6)); }
	inline GameObject_t1756533147 * get_rightSelectUI_6() const { return ___rightSelectUI_6; }
	inline GameObject_t1756533147 ** get_address_of_rightSelectUI_6() { return &___rightSelectUI_6; }
	inline void set_rightSelectUI_6(GameObject_t1756533147 * value)
	{
		___rightSelectUI_6 = value;
		Il2CppCodeGenWriteBarrier(&___rightSelectUI_6, value);
	}

	inline static int32_t get_offset_of_selectRight_7() { return static_cast<int32_t>(offsetof(SelectController_t1269418144, ___selectRight_7)); }
	inline bool get_selectRight_7() const { return ___selectRight_7; }
	inline bool* get_address_of_selectRight_7() { return &___selectRight_7; }
	inline void set_selectRight_7(bool value)
	{
		___selectRight_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
