﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonMike
struct ButtonMike_t3037018370;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void ButtonMike::.ctor()
extern "C"  void ButtonMike__ctor_m1325858971 (ButtonMike_t3037018370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMike::OnEnable()
extern "C"  void ButtonMike_OnEnable_m3555085167 (ButtonMike_t3037018370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMike::Start()
extern "C"  void ButtonMike_Start_m4253609359 (ButtonMike_t3037018370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMike::SetViewHolding(System.Boolean)
extern "C"  void ButtonMike_SetViewHolding_m1701113652 (ButtonMike_t3037018370 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMike::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ButtonMike_OnPointerDown_m3660698599 (ButtonMike_t3037018370 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMike::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ButtonMike_OnPointerUp_m1977570946 (ButtonMike_t3037018370 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonMike::Update()
extern "C"  void ButtonMike_Update_m3186878732 (ButtonMike_t3037018370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ButtonMike::IESavingVoice()
extern "C"  Il2CppObject * ButtonMike_IESavingVoice_m1548059891 (ButtonMike_t3037018370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
