﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelp01ChapterTopScript
struct OpenHelp01ChapterTopScript_t2491901699;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenHelp01ChapterTopScript::.ctor()
extern "C"  void OpenHelp01ChapterTopScript__ctor_m3457847422 (OpenHelp01ChapterTopScript_t2491901699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp01ChapterTopScript::OnEnable()
extern "C"  void OpenHelp01ChapterTopScript_OnEnable_m775749762 (OpenHelp01ChapterTopScript_t2491901699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
