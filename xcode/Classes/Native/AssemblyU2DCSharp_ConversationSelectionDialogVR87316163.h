﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_ConversationSelectionDialog2460570659.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSelectionDialogVR
struct  ConversationSelectionDialogVR_t87316163  : public ConversationSelectionDialog_t2460570659
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ConversationSelectionDialogVR::_selectionButtons
	List_1_t1125654279 * ____selectionButtons_2;
	// ConversationSelectionData ConversationSelectionDialogVR::_selectedData
	ConversationSelectionData_t4090008535 * ____selectedData_3;
	// UnityEngine.GameObject ConversationSelectionDialogVR::selectionButtonContainer
	GameObject_t1756533147 * ___selectionButtonContainer_4;
	// UnityEngine.GameObject ConversationSelectionDialogVR::selectionButtonPrefab
	GameObject_t1756533147 * ___selectionButtonPrefab_5;

public:
	inline static int32_t get_offset_of__selectionButtons_2() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogVR_t87316163, ____selectionButtons_2)); }
	inline List_1_t1125654279 * get__selectionButtons_2() const { return ____selectionButtons_2; }
	inline List_1_t1125654279 ** get_address_of__selectionButtons_2() { return &____selectionButtons_2; }
	inline void set__selectionButtons_2(List_1_t1125654279 * value)
	{
		____selectionButtons_2 = value;
		Il2CppCodeGenWriteBarrier(&____selectionButtons_2, value);
	}

	inline static int32_t get_offset_of__selectedData_3() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogVR_t87316163, ____selectedData_3)); }
	inline ConversationSelectionData_t4090008535 * get__selectedData_3() const { return ____selectedData_3; }
	inline ConversationSelectionData_t4090008535 ** get_address_of__selectedData_3() { return &____selectedData_3; }
	inline void set__selectedData_3(ConversationSelectionData_t4090008535 * value)
	{
		____selectedData_3 = value;
		Il2CppCodeGenWriteBarrier(&____selectedData_3, value);
	}

	inline static int32_t get_offset_of_selectionButtonContainer_4() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogVR_t87316163, ___selectionButtonContainer_4)); }
	inline GameObject_t1756533147 * get_selectionButtonContainer_4() const { return ___selectionButtonContainer_4; }
	inline GameObject_t1756533147 ** get_address_of_selectionButtonContainer_4() { return &___selectionButtonContainer_4; }
	inline void set_selectionButtonContainer_4(GameObject_t1756533147 * value)
	{
		___selectionButtonContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___selectionButtonContainer_4, value);
	}

	inline static int32_t get_offset_of_selectionButtonPrefab_5() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogVR_t87316163, ___selectionButtonPrefab_5)); }
	inline GameObject_t1756533147 * get_selectionButtonPrefab_5() const { return ___selectionButtonPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_selectionButtonPrefab_5() { return &___selectionButtonPrefab_5; }
	inline void set_selectionButtonPrefab_5(GameObject_t1756533147 * value)
	{
		___selectionButtonPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectionButtonPrefab_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
