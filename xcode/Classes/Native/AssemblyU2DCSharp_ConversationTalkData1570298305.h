﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// TalkCommand
struct TalkCommand_t2292920015;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationTalkData
struct  ConversationTalkData_t1570298305  : public Il2CppObject
{
public:
	// System.String ConversationTalkData::role
	String_t* ___role_0;
	// System.String ConversationTalkData::dataEn
	String_t* ___dataEn_1;
	// System.String ConversationTalkData::dataJp
	String_t* ___dataJp_2;
	// System.String ConversationTalkData::soundData
	String_t* ___soundData_3;
	// TalkCommand ConversationTalkData::linkedTalkCommand
	TalkCommand_t2292920015 * ___linkedTalkCommand_4;

public:
	inline static int32_t get_offset_of_role_0() { return static_cast<int32_t>(offsetof(ConversationTalkData_t1570298305, ___role_0)); }
	inline String_t* get_role_0() const { return ___role_0; }
	inline String_t** get_address_of_role_0() { return &___role_0; }
	inline void set_role_0(String_t* value)
	{
		___role_0 = value;
		Il2CppCodeGenWriteBarrier(&___role_0, value);
	}

	inline static int32_t get_offset_of_dataEn_1() { return static_cast<int32_t>(offsetof(ConversationTalkData_t1570298305, ___dataEn_1)); }
	inline String_t* get_dataEn_1() const { return ___dataEn_1; }
	inline String_t** get_address_of_dataEn_1() { return &___dataEn_1; }
	inline void set_dataEn_1(String_t* value)
	{
		___dataEn_1 = value;
		Il2CppCodeGenWriteBarrier(&___dataEn_1, value);
	}

	inline static int32_t get_offset_of_dataJp_2() { return static_cast<int32_t>(offsetof(ConversationTalkData_t1570298305, ___dataJp_2)); }
	inline String_t* get_dataJp_2() const { return ___dataJp_2; }
	inline String_t** get_address_of_dataJp_2() { return &___dataJp_2; }
	inline void set_dataJp_2(String_t* value)
	{
		___dataJp_2 = value;
		Il2CppCodeGenWriteBarrier(&___dataJp_2, value);
	}

	inline static int32_t get_offset_of_soundData_3() { return static_cast<int32_t>(offsetof(ConversationTalkData_t1570298305, ___soundData_3)); }
	inline String_t* get_soundData_3() const { return ___soundData_3; }
	inline String_t** get_address_of_soundData_3() { return &___soundData_3; }
	inline void set_soundData_3(String_t* value)
	{
		___soundData_3 = value;
		Il2CppCodeGenWriteBarrier(&___soundData_3, value);
	}

	inline static int32_t get_offset_of_linkedTalkCommand_4() { return static_cast<int32_t>(offsetof(ConversationTalkData_t1570298305, ___linkedTalkCommand_4)); }
	inline TalkCommand_t2292920015 * get_linkedTalkCommand_4() const { return ___linkedTalkCommand_4; }
	inline TalkCommand_t2292920015 ** get_address_of_linkedTalkCommand_4() { return &___linkedTalkCommand_4; }
	inline void set_linkedTalkCommand_4(TalkCommand_t2292920015 * value)
	{
		___linkedTalkCommand_4 = value;
		Il2CppCodeGenWriteBarrier(&___linkedTalkCommand_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
