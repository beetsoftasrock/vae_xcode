﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ApiConnector
struct ApiConnector_t2569785041;
// IApiRequest
struct IApiRequest_t1151250174;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiConnector
struct  ApiConnector_t2569785041  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<System.String> ApiConnector::_currentRequestUrls
	List_1_t1398341365 * ____currentRequestUrls_2;
	// UnityEngine.GameObject ApiConnector::errorPopup
	GameObject_t1756533147 * ___errorPopup_3;
	// IApiRequest ApiConnector::_currentRequest
	Il2CppObject * ____currentRequest_5;

public:
	inline static int32_t get_offset_of__currentRequestUrls_2() { return static_cast<int32_t>(offsetof(ApiConnector_t2569785041, ____currentRequestUrls_2)); }
	inline List_1_t1398341365 * get__currentRequestUrls_2() const { return ____currentRequestUrls_2; }
	inline List_1_t1398341365 ** get_address_of__currentRequestUrls_2() { return &____currentRequestUrls_2; }
	inline void set__currentRequestUrls_2(List_1_t1398341365 * value)
	{
		____currentRequestUrls_2 = value;
		Il2CppCodeGenWriteBarrier(&____currentRequestUrls_2, value);
	}

	inline static int32_t get_offset_of_errorPopup_3() { return static_cast<int32_t>(offsetof(ApiConnector_t2569785041, ___errorPopup_3)); }
	inline GameObject_t1756533147 * get_errorPopup_3() const { return ___errorPopup_3; }
	inline GameObject_t1756533147 ** get_address_of_errorPopup_3() { return &___errorPopup_3; }
	inline void set_errorPopup_3(GameObject_t1756533147 * value)
	{
		___errorPopup_3 = value;
		Il2CppCodeGenWriteBarrier(&___errorPopup_3, value);
	}

	inline static int32_t get_offset_of__currentRequest_5() { return static_cast<int32_t>(offsetof(ApiConnector_t2569785041, ____currentRequest_5)); }
	inline Il2CppObject * get__currentRequest_5() const { return ____currentRequest_5; }
	inline Il2CppObject ** get_address_of__currentRequest_5() { return &____currentRequest_5; }
	inline void set__currentRequest_5(Il2CppObject * value)
	{
		____currentRequest_5 = value;
		Il2CppCodeGenWriteBarrier(&____currentRequest_5, value);
	}
};

struct ApiConnector_t2569785041_StaticFields
{
public:
	// ApiConnector ApiConnector::_instance
	ApiConnector_t2569785041 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(ApiConnector_t2569785041_StaticFields, ____instance_4)); }
	inline ApiConnector_t2569785041 * get__instance_4() const { return ____instance_4; }
	inline ApiConnector_t2569785041 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(ApiConnector_t2569785041 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier(&____instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
