﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// Sentence1DataLoader
struct Sentence1DataLoader_t1039713923;
// System.Collections.Generic.List`1<SentenceStructureIdiomOnPlayQuestion>
struct List_1_t306008119;
// System.Collections.Generic.List`1<SentenceStructureIdiomQuestionData>
struct List_1_t2620853234;
// SentenceQuestionView
struct SentenceQuestionView_t1478559372;
// SentenceStructureIdiomOnPlayQuestion
struct SentenceStructureIdiomOnPlayQuestion_t936886987;
// System.String
struct String_t;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sentence1SceneController
struct  Sentence1SceneController_t2055584412  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Sentence1SceneController::questionBundleText
	Text_t356221433 * ___questionBundleText_2;
	// Sentence1DataLoader Sentence1SceneController::dataLoader
	Sentence1DataLoader_t1039713923 * ___dataLoader_3;
	// System.Collections.Generic.List`1<SentenceStructureIdiomOnPlayQuestion> Sentence1SceneController::onPlayQuestions
	List_1_t306008119 * ___onPlayQuestions_4;
	// System.Collections.Generic.List`1<SentenceStructureIdiomQuestionData> Sentence1SceneController::questionDatas
	List_1_t2620853234 * ___questionDatas_5;
	// SentenceQuestionView Sentence1SceneController::questionView
	SentenceQuestionView_t1478559372 * ___questionView_6;
	// SentenceStructureIdiomOnPlayQuestion Sentence1SceneController::_currentQuestion
	SentenceStructureIdiomOnPlayQuestion_t936886987 * ____currentQuestion_7;
	// System.String Sentence1SceneController::soundPath
	String_t* ___soundPath_8;
	// System.DateTime Sentence1SceneController::startTime
	DateTime_t693205669  ___startTime_9;

public:
	inline static int32_t get_offset_of_questionBundleText_2() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412, ___questionBundleText_2)); }
	inline Text_t356221433 * get_questionBundleText_2() const { return ___questionBundleText_2; }
	inline Text_t356221433 ** get_address_of_questionBundleText_2() { return &___questionBundleText_2; }
	inline void set_questionBundleText_2(Text_t356221433 * value)
	{
		___questionBundleText_2 = value;
		Il2CppCodeGenWriteBarrier(&___questionBundleText_2, value);
	}

	inline static int32_t get_offset_of_dataLoader_3() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412, ___dataLoader_3)); }
	inline Sentence1DataLoader_t1039713923 * get_dataLoader_3() const { return ___dataLoader_3; }
	inline Sentence1DataLoader_t1039713923 ** get_address_of_dataLoader_3() { return &___dataLoader_3; }
	inline void set_dataLoader_3(Sentence1DataLoader_t1039713923 * value)
	{
		___dataLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_3, value);
	}

	inline static int32_t get_offset_of_onPlayQuestions_4() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412, ___onPlayQuestions_4)); }
	inline List_1_t306008119 * get_onPlayQuestions_4() const { return ___onPlayQuestions_4; }
	inline List_1_t306008119 ** get_address_of_onPlayQuestions_4() { return &___onPlayQuestions_4; }
	inline void set_onPlayQuestions_4(List_1_t306008119 * value)
	{
		___onPlayQuestions_4 = value;
		Il2CppCodeGenWriteBarrier(&___onPlayQuestions_4, value);
	}

	inline static int32_t get_offset_of_questionDatas_5() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412, ___questionDatas_5)); }
	inline List_1_t2620853234 * get_questionDatas_5() const { return ___questionDatas_5; }
	inline List_1_t2620853234 ** get_address_of_questionDatas_5() { return &___questionDatas_5; }
	inline void set_questionDatas_5(List_1_t2620853234 * value)
	{
		___questionDatas_5 = value;
		Il2CppCodeGenWriteBarrier(&___questionDatas_5, value);
	}

	inline static int32_t get_offset_of_questionView_6() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412, ___questionView_6)); }
	inline SentenceQuestionView_t1478559372 * get_questionView_6() const { return ___questionView_6; }
	inline SentenceQuestionView_t1478559372 ** get_address_of_questionView_6() { return &___questionView_6; }
	inline void set_questionView_6(SentenceQuestionView_t1478559372 * value)
	{
		___questionView_6 = value;
		Il2CppCodeGenWriteBarrier(&___questionView_6, value);
	}

	inline static int32_t get_offset_of__currentQuestion_7() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412, ____currentQuestion_7)); }
	inline SentenceStructureIdiomOnPlayQuestion_t936886987 * get__currentQuestion_7() const { return ____currentQuestion_7; }
	inline SentenceStructureIdiomOnPlayQuestion_t936886987 ** get_address_of__currentQuestion_7() { return &____currentQuestion_7; }
	inline void set__currentQuestion_7(SentenceStructureIdiomOnPlayQuestion_t936886987 * value)
	{
		____currentQuestion_7 = value;
		Il2CppCodeGenWriteBarrier(&____currentQuestion_7, value);
	}

	inline static int32_t get_offset_of_soundPath_8() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412, ___soundPath_8)); }
	inline String_t* get_soundPath_8() const { return ___soundPath_8; }
	inline String_t** get_address_of_soundPath_8() { return &___soundPath_8; }
	inline void set_soundPath_8(String_t* value)
	{
		___soundPath_8 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_8, value);
	}

	inline static int32_t get_offset_of_startTime_9() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412, ___startTime_9)); }
	inline DateTime_t693205669  get_startTime_9() const { return ___startTime_9; }
	inline DateTime_t693205669 * get_address_of_startTime_9() { return &___startTime_9; }
	inline void set_startTime_9(DateTime_t693205669  value)
	{
		___startTime_9 = value;
	}
};

struct Sentence1SceneController_t2055584412_StaticFields
{
public:
	// System.Func`1<System.Boolean> Sentence1SceneController::<>f__mg$cache0
	Func_1_t1485000104 * ___U3CU3Ef__mgU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(Sentence1SceneController_t2055584412_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
