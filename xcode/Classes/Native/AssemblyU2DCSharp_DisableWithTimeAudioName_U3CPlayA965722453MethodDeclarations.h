﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisableWithTimeAudioName/<PlayAudioAndDisable>c__Iterator0
struct U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DisableWithTimeAudioName/<PlayAudioAndDisable>c__Iterator0::.ctor()
extern "C"  void U3CPlayAudioAndDisableU3Ec__Iterator0__ctor_m1122178144 (U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DisableWithTimeAudioName/<PlayAudioAndDisable>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayAudioAndDisableU3Ec__Iterator0_MoveNext_m3412296876 (U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DisableWithTimeAudioName/<PlayAudioAndDisable>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayAudioAndDisableU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m835500744 (U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DisableWithTimeAudioName/<PlayAudioAndDisable>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayAudioAndDisableU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2580859024 (U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeAudioName/<PlayAudioAndDisable>c__Iterator0::Dispose()
extern "C"  void U3CPlayAudioAndDisableU3Ec__Iterator0_Dispose_m996263991 (U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeAudioName/<PlayAudioAndDisable>c__Iterator0::Reset()
extern "C"  void U3CPlayAudioAndDisableU3Ec__Iterator0_Reset_m2193994577 (U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
