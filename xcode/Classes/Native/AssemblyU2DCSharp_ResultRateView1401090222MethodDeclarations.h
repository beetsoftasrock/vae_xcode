﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultRateView
struct ResultRateView_t1401090222;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultRateView::.ctor()
extern "C"  void ResultRateView__ctor_m1987238545 (ResultRateView_t1401090222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
