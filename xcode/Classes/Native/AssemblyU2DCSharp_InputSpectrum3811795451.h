﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InputSpectrum/Band[]
struct BandU5BU5D_t3389715132;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputSpectrum
struct  InputSpectrum_t3811795451  : public MonoBehaviour_t1158329972
{
public:
	// System.Single InputSpectrum::scale
	float ___scale_2;
	// InputSpectrum/Band[] InputSpectrum::bands
	BandU5BU5D_t3389715132* ___bands_3;

public:
	inline static int32_t get_offset_of_scale_2() { return static_cast<int32_t>(offsetof(InputSpectrum_t3811795451, ___scale_2)); }
	inline float get_scale_2() const { return ___scale_2; }
	inline float* get_address_of_scale_2() { return &___scale_2; }
	inline void set_scale_2(float value)
	{
		___scale_2 = value;
	}

	inline static int32_t get_offset_of_bands_3() { return static_cast<int32_t>(offsetof(InputSpectrum_t3811795451, ___bands_3)); }
	inline BandU5BU5D_t3389715132* get_bands_3() const { return ___bands_3; }
	inline BandU5BU5D_t3389715132** get_address_of_bands_3() { return &___bands_3; }
	inline void set_bands_3(BandU5BU5D_t3389715132* value)
	{
		___bands_3 = value;
		Il2CppCodeGenWriteBarrier(&___bands_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
