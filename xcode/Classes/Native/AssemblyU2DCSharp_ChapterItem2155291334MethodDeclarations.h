﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterItem
struct ChapterItem_t2155291334;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterItem::.ctor()
extern "C"  void ChapterItem__ctor_m967099375 (ChapterItem_t2155291334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterItem::Start()
extern "C"  void ChapterItem_Start_m2257486539 (ChapterItem_t2155291334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterItem::Update()
extern "C"  void ChapterItem_Update_m3489431988 (ChapterItem_t2155291334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterItem::UpdateSize()
extern "C"  void ChapterItem_UpdateSize_m2022615859 (ChapterItem_t2155291334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
