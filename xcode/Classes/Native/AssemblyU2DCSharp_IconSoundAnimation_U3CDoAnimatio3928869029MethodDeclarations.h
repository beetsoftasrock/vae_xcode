﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IconSoundAnimation/<DoAnimation>c__Iterator0
struct U3CDoAnimationU3Ec__Iterator0_t3928869029;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void IconSoundAnimation/<DoAnimation>c__Iterator0::.ctor()
extern "C"  void U3CDoAnimationU3Ec__Iterator0__ctor_m4123726850 (U3CDoAnimationU3Ec__Iterator0_t3928869029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IconSoundAnimation/<DoAnimation>c__Iterator0::MoveNext()
extern "C"  bool U3CDoAnimationU3Ec__Iterator0_MoveNext_m1438084698 (U3CDoAnimationU3Ec__Iterator0_t3928869029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IconSoundAnimation/<DoAnimation>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoAnimationU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2158417678 (U3CDoAnimationU3Ec__Iterator0_t3928869029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IconSoundAnimation/<DoAnimation>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoAnimationU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m280373910 (U3CDoAnimationU3Ec__Iterator0_t3928869029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconSoundAnimation/<DoAnimation>c__Iterator0::Dispose()
extern "C"  void U3CDoAnimationU3Ec__Iterator0_Dispose_m2233397327 (U3CDoAnimationU3Ec__Iterator0_t3928869029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IconSoundAnimation/<DoAnimation>c__Iterator0::Reset()
extern "C"  void U3CDoAnimationU3Ec__Iterator0_Reset_m3939534265 (U3CDoAnimationU3Ec__Iterator0_t3928869029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
