﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::.ctor()
#define List_1__ctor_m3038289549(__this, method) ((  void (*) (List_1_t1390196108 *, const MethodInfo*))List_1__ctor_m4167524594_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m915154321(__this, ___collection0, method) ((  void (*) (List_1_t1390196108 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::.ctor(System.Int32)
#define List_1__ctor_m4123989199(__this, ___capacity0, method) ((  void (*) (List_1_t1390196108 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::.ctor(T[],System.Int32)
#define List_1__ctor_m2709009605(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1390196108 *, VocabularyQuestionDataU5BU5D_t613651297*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::.cctor()
#define List_1__cctor_m3207687969(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3316818324(__this, method) ((  Il2CppObject* (*) (List_1_t1390196108 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m149864454(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1390196108 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m937302003(__this, method) ((  Il2CppObject * (*) (List_1_t1390196108 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2011765930(__this, ___item0, method) ((  int32_t (*) (List_1_t1390196108 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1441868596(__this, ___item0, method) ((  bool (*) (List_1_t1390196108 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m451519708(__this, ___item0, method) ((  int32_t (*) (List_1_t1390196108 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1345413187(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1390196108 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1723397955(__this, ___item0, method) ((  void (*) (List_1_t1390196108 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m345106295(__this, method) ((  bool (*) (List_1_t1390196108 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2509124754(__this, method) ((  bool (*) (List_1_t1390196108 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m250627740(__this, method) ((  Il2CppObject * (*) (List_1_t1390196108 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3865685443(__this, method) ((  bool (*) (List_1_t1390196108 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1123257982(__this, method) ((  bool (*) (List_1_t1390196108 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3652793503(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1390196108 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1192948084(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1390196108 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::Add(T)
#define List_1_Add_m3887375825(__this, ___item0, method) ((  void (*) (List_1_t1390196108 *, VocabularyQuestionData_t2021074976 *, const MethodInfo*))List_1_Add_m2984365430_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3572573356(__this, ___newCount0, method) ((  void (*) (List_1_t1390196108 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1773350799(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1390196108 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3935926668(__this, ___collection0, method) ((  void (*) (List_1_t1390196108 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m431402364(__this, ___enumerable0, method) ((  void (*) (List_1_t1390196108 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3456242127(__this, ___collection0, method) ((  void (*) (List_1_t1390196108 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<VocabularyQuestionData>::AsReadOnly()
#define List_1_AsReadOnly_m1715004822(__this, method) ((  ReadOnlyCollection_1_t2206860668 * (*) (List_1_t1390196108 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::Clear()
#define List_1_Clear_m2228749267(__this, method) ((  void (*) (List_1_t1390196108 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VocabularyQuestionData>::Contains(T)
#define List_1_Contains_m3514306985(__this, ___item0, method) ((  bool (*) (List_1_t1390196108 *, VocabularyQuestionData_t2021074976 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::CopyTo(T[])
#define List_1_CopyTo_m474784178(__this, ___array0, method) ((  void (*) (List_1_t1390196108 *, VocabularyQuestionDataU5BU5D_t613651297*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3825838267(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1390196108 *, VocabularyQuestionDataU5BU5D_t613651297*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.List`1<VocabularyQuestionData>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m4142203103(__this, ___match0, method) ((  bool (*) (List_1_t1390196108 *, Predicate_1_t464045091 *, const MethodInfo*))List_1_Exists_m1496178069_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<VocabularyQuestionData>::Find(System.Predicate`1<T>)
#define List_1_Find_m1027325441(__this, ___match0, method) ((  VocabularyQuestionData_t2021074976 * (*) (List_1_t1390196108 *, Predicate_1_t464045091 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2964558254(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t464045091 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VocabularyQuestionData>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m1814117842(__this, ___match0, method) ((  List_1_t1390196108 * (*) (List_1_t1390196108 *, Predicate_1_t464045091 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VocabularyQuestionData>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m3333954178(__this, ___match0, method) ((  List_1_t1390196108 * (*) (List_1_t1390196108 *, Predicate_1_t464045091 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VocabularyQuestionData>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m2173891698(__this, ___match0, method) ((  List_1_t1390196108 * (*) (List_1_t1390196108 *, Predicate_1_t464045091 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<VocabularyQuestionData>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1127236212(__this, ___match0, method) ((  int32_t (*) (List_1_t1390196108 *, Predicate_1_t464045091 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<VocabularyQuestionData>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3900367793(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1390196108 *, int32_t, int32_t, Predicate_1_t464045091 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<VocabularyQuestionData>::GetEnumerator()
#define List_1_GetEnumerator_m1312668498(__this, method) ((  Enumerator_t924925782  (*) (List_1_t1390196108 *, const MethodInfo*))List_1_GetEnumerator_m1854899495_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VocabularyQuestionData>::GetRange(System.Int32,System.Int32)
#define List_1_GetRange_m2508928094(__this, ___index0, ___count1, method) ((  List_1_t1390196108 * (*) (List_1_t1390196108 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1944954844_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<VocabularyQuestionData>::IndexOf(T)
#define List_1_IndexOf_m946520615(__this, ___item0, method) ((  int32_t (*) (List_1_t1390196108 *, VocabularyQuestionData_t2021074976 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2312390114(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1390196108 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1056390507(__this, ___index0, method) ((  void (*) (List_1_t1390196108 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::Insert(System.Int32,T)
#define List_1_Insert_m3331180688(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1390196108 *, int32_t, VocabularyQuestionData_t2021074976 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2673999297(__this, ___collection0, method) ((  void (*) (List_1_t1390196108 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<VocabularyQuestionData>::Remove(T)
#define List_1_Remove_m3734306918(__this, ___item0, method) ((  bool (*) (List_1_t1390196108 *, VocabularyQuestionData_t2021074976 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<VocabularyQuestionData>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m4007392602(__this, ___match0, method) ((  int32_t (*) (List_1_t1390196108 *, Predicate_1_t464045091 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2866516127(__this, ___index0, method) ((  void (*) (List_1_t1390196108 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1209792383(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1390196108 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::Reverse()
#define List_1_Reverse_m3898008484(__this, method) ((  void (*) (List_1_t1390196108 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::Sort()
#define List_1_Sort_m898148264(__this, method) ((  void (*) (List_1_t1390196108 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m778928308(__this, ___comparer0, method) ((  void (*) (List_1_t1390196108 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3413240857(__this, ___comparison0, method) ((  void (*) (List_1_t1390196108 *, Comparison_1_t3282813827 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<VocabularyQuestionData>::ToArray()
#define List_1_ToArray_m3566796221(__this, method) ((  VocabularyQuestionDataU5BU5D_t613651297* (*) (List_1_t1390196108 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::TrimExcess()
#define List_1_TrimExcess_m2640344095(__this, method) ((  void (*) (List_1_t1390196108 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VocabularyQuestionData>::get_Capacity()
#define List_1_get_Capacity_m2031712621(__this, method) ((  int32_t (*) (List_1_t1390196108 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2182752684(__this, ___value0, method) ((  void (*) (List_1_t1390196108 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<VocabularyQuestionData>::get_Count()
#define List_1_get_Count_m2808681525(__this, method) ((  int32_t (*) (List_1_t1390196108 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// T System.Collections.Generic.List`1<VocabularyQuestionData>::get_Item(System.Int32)
#define List_1_get_Item_m2609827824(__this, ___index0, method) ((  VocabularyQuestionData_t2021074976 * (*) (List_1_t1390196108 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VocabularyQuestionData>::set_Item(System.Int32,T)
#define List_1_set_Item_m4041920431(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1390196108 *, int32_t, VocabularyQuestionData_t2021074976 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
