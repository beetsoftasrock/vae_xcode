﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSceneSelectionView
struct ConversationSceneSelectionView_t407867144;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationSceneSelectionView::.ctor()
extern "C"  void ConversationSceneSelectionView__ctor_m3558663833 (ConversationSceneSelectionView_t407867144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConversationSceneSelectionView::get_selected()
extern "C"  bool ConversationSceneSelectionView_get_selected_m2375809969 (ConversationSceneSelectionView_t407867144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelectionView::set_selected(System.Boolean)
extern "C"  void ConversationSceneSelectionView_set_selected_m1313108958 (ConversationSceneSelectionView_t407867144 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelectionView::RefreshView()
extern "C"  void ConversationSceneSelectionView_RefreshView_m2804568669 (ConversationSceneSelectionView_t407867144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelectionView::UpdateView()
extern "C"  void ConversationSceneSelectionView_UpdateView_m1146308569 (ConversationSceneSelectionView_t407867144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelectionView::Clicked()
extern "C"  void ConversationSceneSelectionView_Clicked_m3281399490 (ConversationSceneSelectionView_t407867144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneSelectionView::Awake()
extern "C"  void ConversationSceneSelectionView_Awake_m4189796884 (ConversationSceneSelectionView_t407867144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
