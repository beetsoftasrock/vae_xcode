﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Analytics.DriveableProperty
struct DriveableProperty_t2641992127;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.RemoteSettings
struct  RemoteSettings_t3121907247  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Analytics.DriveableProperty UnityEngine.Analytics.RemoteSettings::m_DriveableProperty
	DriveableProperty_t2641992127 * ___m_DriveableProperty_2;

public:
	inline static int32_t get_offset_of_m_DriveableProperty_2() { return static_cast<int32_t>(offsetof(RemoteSettings_t3121907247, ___m_DriveableProperty_2)); }
	inline DriveableProperty_t2641992127 * get_m_DriveableProperty_2() const { return ___m_DriveableProperty_2; }
	inline DriveableProperty_t2641992127 ** get_address_of_m_DriveableProperty_2() { return &___m_DriveableProperty_2; }
	inline void set_m_DriveableProperty_2(DriveableProperty_t2641992127 * value)
	{
		___m_DriveableProperty_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_DriveableProperty_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
