﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Predicate`1<System.String>
struct Predicate_1_t472190348;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.Misc.LessionName
struct  LessionName_t3189920656  : public MonoBehaviour_t1158329972
{
public:
	// System.String[] Beetsoft.VAE.Misc.LessionName::sceneName
	StringU5BU5D_t1642385972* ___sceneName_2;
	// System.String[] Beetsoft.VAE.Misc.LessionName::lessionName
	StringU5BU5D_t1642385972* ___lessionName_3;

public:
	inline static int32_t get_offset_of_sceneName_2() { return static_cast<int32_t>(offsetof(LessionName_t3189920656, ___sceneName_2)); }
	inline StringU5BU5D_t1642385972* get_sceneName_2() const { return ___sceneName_2; }
	inline StringU5BU5D_t1642385972** get_address_of_sceneName_2() { return &___sceneName_2; }
	inline void set_sceneName_2(StringU5BU5D_t1642385972* value)
	{
		___sceneName_2 = value;
		Il2CppCodeGenWriteBarrier(&___sceneName_2, value);
	}

	inline static int32_t get_offset_of_lessionName_3() { return static_cast<int32_t>(offsetof(LessionName_t3189920656, ___lessionName_3)); }
	inline StringU5BU5D_t1642385972* get_lessionName_3() const { return ___lessionName_3; }
	inline StringU5BU5D_t1642385972** get_address_of_lessionName_3() { return &___lessionName_3; }
	inline void set_lessionName_3(StringU5BU5D_t1642385972* value)
	{
		___lessionName_3 = value;
		Il2CppCodeGenWriteBarrier(&___lessionName_3, value);
	}
};

struct LessionName_t3189920656_StaticFields
{
public:
	// System.Predicate`1<System.String> Beetsoft.VAE.Misc.LessionName::<>f__am$cache0
	Predicate_1_t472190348 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LessionName_t3189920656_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t472190348 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t472190348 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t472190348 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
