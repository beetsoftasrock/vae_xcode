﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalConfig
struct GlobalConfig_t3080413471;
// NewPracticeSceneController
struct NewPracticeSceneController_t1536734399;

#include "AssemblyU2DCSharp_ConversationDataSaver1206770280.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PracticeSceneDataSaver
struct  PracticeSceneDataSaver_t1376690392  : public ConversationDataSaver_t1206770280
{
public:
	// GlobalConfig PracticeSceneDataSaver::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_2;
	// NewPracticeSceneController PracticeSceneDataSaver::practiceSceneController
	NewPracticeSceneController_t1536734399 * ___practiceSceneController_3;

public:
	inline static int32_t get_offset_of_globalConfig_2() { return static_cast<int32_t>(offsetof(PracticeSceneDataSaver_t1376690392, ___globalConfig_2)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_2() const { return ___globalConfig_2; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_2() { return &___globalConfig_2; }
	inline void set_globalConfig_2(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_2 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_2, value);
	}

	inline static int32_t get_offset_of_practiceSceneController_3() { return static_cast<int32_t>(offsetof(PracticeSceneDataSaver_t1376690392, ___practiceSceneController_3)); }
	inline NewPracticeSceneController_t1536734399 * get_practiceSceneController_3() const { return ___practiceSceneController_3; }
	inline NewPracticeSceneController_t1536734399 ** get_address_of_practiceSceneController_3() { return &___practiceSceneController_3; }
	inline void set_practiceSceneController_3(NewPracticeSceneController_t1536734399 * value)
	{
		___practiceSceneController_3 = value;
		Il2CppCodeGenWriteBarrier(&___practiceSceneController_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
