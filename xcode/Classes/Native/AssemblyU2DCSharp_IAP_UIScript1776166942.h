﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IAP_Popup
struct IAP_Popup_t2375878299;
// UnityEngine.Transform
struct Transform_t3275118058;
// HomeCotroller
struct HomeCotroller_t2470068823;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAP_UIScript
struct  IAP_UIScript_t1776166942  : public MonoBehaviour_t1158329972
{
public:
	// IAP_Popup IAP_UIScript::popupIAP
	IAP_Popup_t2375878299 * ___popupIAP_2;
	// UnityEngine.Transform IAP_UIScript::contentScrollViewChapterHome
	Transform_t3275118058 * ___contentScrollViewChapterHome_3;
	// HomeCotroller IAP_UIScript::homeController
	HomeCotroller_t2470068823 * ___homeController_4;

public:
	inline static int32_t get_offset_of_popupIAP_2() { return static_cast<int32_t>(offsetof(IAP_UIScript_t1776166942, ___popupIAP_2)); }
	inline IAP_Popup_t2375878299 * get_popupIAP_2() const { return ___popupIAP_2; }
	inline IAP_Popup_t2375878299 ** get_address_of_popupIAP_2() { return &___popupIAP_2; }
	inline void set_popupIAP_2(IAP_Popup_t2375878299 * value)
	{
		___popupIAP_2 = value;
		Il2CppCodeGenWriteBarrier(&___popupIAP_2, value);
	}

	inline static int32_t get_offset_of_contentScrollViewChapterHome_3() { return static_cast<int32_t>(offsetof(IAP_UIScript_t1776166942, ___contentScrollViewChapterHome_3)); }
	inline Transform_t3275118058 * get_contentScrollViewChapterHome_3() const { return ___contentScrollViewChapterHome_3; }
	inline Transform_t3275118058 ** get_address_of_contentScrollViewChapterHome_3() { return &___contentScrollViewChapterHome_3; }
	inline void set_contentScrollViewChapterHome_3(Transform_t3275118058 * value)
	{
		___contentScrollViewChapterHome_3 = value;
		Il2CppCodeGenWriteBarrier(&___contentScrollViewChapterHome_3, value);
	}

	inline static int32_t get_offset_of_homeController_4() { return static_cast<int32_t>(offsetof(IAP_UIScript_t1776166942, ___homeController_4)); }
	inline HomeCotroller_t2470068823 * get_homeController_4() const { return ___homeController_4; }
	inline HomeCotroller_t2470068823 ** get_address_of_homeController_4() { return &___homeController_4; }
	inline void set_homeController_4(HomeCotroller_t2470068823 * value)
	{
		___homeController_4 = value;
		Il2CppCodeGenWriteBarrier(&___homeController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
