﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVRSample.AutoPlayVideo
struct AutoPlayVideo_t1314286476;

#include "codegen/il2cpp-codegen.h"

// System.Void GVRSample.AutoPlayVideo::.ctor()
extern "C"  void AutoPlayVideo__ctor_m3032452284 (AutoPlayVideo_t1314286476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVRSample.AutoPlayVideo::Start()
extern "C"  void AutoPlayVideo_Start_m2224703592 (AutoPlayVideo_t1314286476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVRSample.AutoPlayVideo::Update()
extern "C"  void AutoPlayVideo_Update_m1974203567 (AutoPlayVideo_t1314286476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
