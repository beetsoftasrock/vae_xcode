﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m784135531(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3671019970_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4124231769(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, AssetBundle_t2054978754 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2989589458_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3873167989(__this, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m454937302_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1352780538(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, AssetBundle_t2054978754 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4272763307_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3904877752(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2240764446 *, AssetBundle_t2054978754 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3199809075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2964095534(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m962041751_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m730267752(__this, ___index0, method) ((  AssetBundle_t2054978754 * (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m70085287_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1110197401(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, AssetBundle_t2054978754 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1547026160_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1778162109(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4041967064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3408139572(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3664791405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m664914729(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m531171980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3219656620(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2240764446 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3780136817_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2304873870(__this, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3983677501_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m584938586(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2240764446 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1990607517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m332855890(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2240764446 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m606942423_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1677028561(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m691705570_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1877136481(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3182494192_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m435126423(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m572840272_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m203789404(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2871048729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4037245316(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m769863805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1368042053(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m942145650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2536850328(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1367736517_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4235740621(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3336878134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4217106630(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799572719_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::Contains(T)
#define ReadOnlyCollection_1_Contains_m2525234203(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2240764446 *, AssetBundle_t2054978754 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1227826160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1029939461(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2240764446 *, AssetBundleU5BU5D_t2632285687*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4257276542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m910104656(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1627519329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1613578593(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2240764446 *, AssetBundle_t2054978754 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1981423404_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2106109116(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2240764446 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AssetBundle>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3520424212(__this, ___index0, method) ((  AssetBundle_t2054978754 * (*) (ReadOnlyCollection_1_t2240764446 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m191392387_gshared)(__this, ___index0, method)
