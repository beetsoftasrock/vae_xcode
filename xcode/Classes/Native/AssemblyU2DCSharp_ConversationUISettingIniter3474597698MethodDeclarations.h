﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationUISettingIniter
struct ConversationUISettingIniter_t3474597698;
// BaseSetting
struct BaseSetting_t2575616157;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationUISettingIniter::.ctor()
extern "C"  void ConversationUISettingIniter__ctor_m1637095721 (ConversationUISettingIniter_t3474597698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseSetting ConversationUISettingIniter::InitSetting()
extern "C"  BaseSetting_t2575616157 * ConversationUISettingIniter_InitSetting_m2433445161 (ConversationUISettingIniter_t3474597698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationUISettingIniter::Awake()
extern "C"  void ConversationUISettingIniter_Awake_m3235898290 (ConversationUISettingIniter_t3474597698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
