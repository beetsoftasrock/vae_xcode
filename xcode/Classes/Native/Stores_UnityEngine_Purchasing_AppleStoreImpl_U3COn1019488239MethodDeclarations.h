﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.AppleStoreImpl/<OnProductsRetrieved>c__AnonStorey0
struct U3COnProductsRetrievedU3Ec__AnonStorey0_t1019488239;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct AppleInAppPurchaseReceipt_t3271698749;

#include "codegen/il2cpp-codegen.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749.h"

// System.Void UnityEngine.Purchasing.AppleStoreImpl/<OnProductsRetrieved>c__AnonStorey0::.ctor()
extern "C"  void U3COnProductsRetrievedU3Ec__AnonStorey0__ctor_m3361059116 (U3COnProductsRetrievedU3Ec__AnonStorey0_t1019488239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.AppleStoreImpl/<OnProductsRetrieved>c__AnonStorey0::<>m__0(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern "C"  bool U3COnProductsRetrievedU3Ec__AnonStorey0_U3CU3Em__0_m1718410983 (U3COnProductsRetrievedU3Ec__AnonStorey0_t1019488239 * __this, AppleInAppPurchaseReceipt_t3271698749 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
