﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerTalkModule
struct PlayerTalkModule_t3356839145;
// OtherTalkModule
struct OtherTalkModule_t2158105276;
// BaseConversationCommandLoader
struct BaseConversationCommandLoader_t1091304408;
// ConversationLogicController
struct ConversationLogicController_t3911886281;
// System.Collections.Generic.List`1<ConversationCommand>
struct List_1_t3029226968;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvokerModule
struct  InvokerModule_t4256524346  : public MonoBehaviour_t1158329972
{
public:
	// PlayerTalkModule InvokerModule::playerTalkModule
	PlayerTalkModule_t3356839145 * ___playerTalkModule_2;
	// OtherTalkModule InvokerModule::otherTalkModule
	OtherTalkModule_t2158105276 * ___otherTalkModule_3;
	// BaseConversationCommandLoader InvokerModule::commandLoader
	BaseConversationCommandLoader_t1091304408 * ___commandLoader_4;
	// ConversationLogicController InvokerModule::clc
	ConversationLogicController_t3911886281 * ___clc_5;
	// System.Boolean InvokerModule::_isBlock
	bool ____isBlock_6;
	// System.Boolean InvokerModule::_isCompleted
	bool ____isCompleted_7;
	// System.Collections.Generic.List`1<ConversationCommand> InvokerModule::_cachedCommands
	List_1_t3029226968 * ____cachedCommands_8;
	// UnityEngine.GameObject InvokerModule::bottomButtons
	GameObject_t1756533147 * ___bottomButtons_9;

public:
	inline static int32_t get_offset_of_playerTalkModule_2() { return static_cast<int32_t>(offsetof(InvokerModule_t4256524346, ___playerTalkModule_2)); }
	inline PlayerTalkModule_t3356839145 * get_playerTalkModule_2() const { return ___playerTalkModule_2; }
	inline PlayerTalkModule_t3356839145 ** get_address_of_playerTalkModule_2() { return &___playerTalkModule_2; }
	inline void set_playerTalkModule_2(PlayerTalkModule_t3356839145 * value)
	{
		___playerTalkModule_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerTalkModule_2, value);
	}

	inline static int32_t get_offset_of_otherTalkModule_3() { return static_cast<int32_t>(offsetof(InvokerModule_t4256524346, ___otherTalkModule_3)); }
	inline OtherTalkModule_t2158105276 * get_otherTalkModule_3() const { return ___otherTalkModule_3; }
	inline OtherTalkModule_t2158105276 ** get_address_of_otherTalkModule_3() { return &___otherTalkModule_3; }
	inline void set_otherTalkModule_3(OtherTalkModule_t2158105276 * value)
	{
		___otherTalkModule_3 = value;
		Il2CppCodeGenWriteBarrier(&___otherTalkModule_3, value);
	}

	inline static int32_t get_offset_of_commandLoader_4() { return static_cast<int32_t>(offsetof(InvokerModule_t4256524346, ___commandLoader_4)); }
	inline BaseConversationCommandLoader_t1091304408 * get_commandLoader_4() const { return ___commandLoader_4; }
	inline BaseConversationCommandLoader_t1091304408 ** get_address_of_commandLoader_4() { return &___commandLoader_4; }
	inline void set_commandLoader_4(BaseConversationCommandLoader_t1091304408 * value)
	{
		___commandLoader_4 = value;
		Il2CppCodeGenWriteBarrier(&___commandLoader_4, value);
	}

	inline static int32_t get_offset_of_clc_5() { return static_cast<int32_t>(offsetof(InvokerModule_t4256524346, ___clc_5)); }
	inline ConversationLogicController_t3911886281 * get_clc_5() const { return ___clc_5; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_5() { return &___clc_5; }
	inline void set_clc_5(ConversationLogicController_t3911886281 * value)
	{
		___clc_5 = value;
		Il2CppCodeGenWriteBarrier(&___clc_5, value);
	}

	inline static int32_t get_offset_of__isBlock_6() { return static_cast<int32_t>(offsetof(InvokerModule_t4256524346, ____isBlock_6)); }
	inline bool get__isBlock_6() const { return ____isBlock_6; }
	inline bool* get_address_of__isBlock_6() { return &____isBlock_6; }
	inline void set__isBlock_6(bool value)
	{
		____isBlock_6 = value;
	}

	inline static int32_t get_offset_of__isCompleted_7() { return static_cast<int32_t>(offsetof(InvokerModule_t4256524346, ____isCompleted_7)); }
	inline bool get__isCompleted_7() const { return ____isCompleted_7; }
	inline bool* get_address_of__isCompleted_7() { return &____isCompleted_7; }
	inline void set__isCompleted_7(bool value)
	{
		____isCompleted_7 = value;
	}

	inline static int32_t get_offset_of__cachedCommands_8() { return static_cast<int32_t>(offsetof(InvokerModule_t4256524346, ____cachedCommands_8)); }
	inline List_1_t3029226968 * get__cachedCommands_8() const { return ____cachedCommands_8; }
	inline List_1_t3029226968 ** get_address_of__cachedCommands_8() { return &____cachedCommands_8; }
	inline void set__cachedCommands_8(List_1_t3029226968 * value)
	{
		____cachedCommands_8 = value;
		Il2CppCodeGenWriteBarrier(&____cachedCommands_8, value);
	}

	inline static int32_t get_offset_of_bottomButtons_9() { return static_cast<int32_t>(offsetof(InvokerModule_t4256524346, ___bottomButtons_9)); }
	inline GameObject_t1756533147 * get_bottomButtons_9() const { return ___bottomButtons_9; }
	inline GameObject_t1756533147 ** get_address_of_bottomButtons_9() { return &___bottomButtons_9; }
	inline void set_bottomButtons_9(GameObject_t1756533147 * value)
	{
		___bottomButtons_9 = value;
		Il2CppCodeGenWriteBarrier(&___bottomButtons_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
