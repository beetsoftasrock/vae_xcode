﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;

#include "AssemblyU2DCSharp_ConversationSelectionDialog2460570659.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSelectionDialogNornal
struct  ConversationSelectionDialogNornal_t1475688335  : public ConversationSelectionDialog_t2460570659
{
public:
	// System.Boolean ConversationSelectionDialogNornal::_showSubText
	bool ____showSubText_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ConversationSelectionDialogNornal::_selectionTexts
	List_1_t1125654279 * ____selectionTexts_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ConversationSelectionDialogNornal::_selectionButtons
	List_1_t1125654279 * ____selectionButtons_4;
	// UnityEngine.GameObject ConversationSelectionDialogNornal::selectionTextContainer
	GameObject_t1756533147 * ___selectionTextContainer_5;
	// UnityEngine.GameObject ConversationSelectionDialogNornal::selectionButtonContainer
	GameObject_t1756533147 * ___selectionButtonContainer_6;
	// UnityEngine.GameObject ConversationSelectionDialogNornal::selectionTextPrefab
	GameObject_t1756533147 * ___selectionTextPrefab_7;
	// UnityEngine.GameObject ConversationSelectionDialogNornal::selectionButtonPrefab
	GameObject_t1756533147 * ___selectionButtonPrefab_8;
	// ConversationSelectionData ConversationSelectionDialogNornal::_selectedData
	ConversationSelectionData_t4090008535 * ____selectedData_9;

public:
	inline static int32_t get_offset_of__showSubText_2() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogNornal_t1475688335, ____showSubText_2)); }
	inline bool get__showSubText_2() const { return ____showSubText_2; }
	inline bool* get_address_of__showSubText_2() { return &____showSubText_2; }
	inline void set__showSubText_2(bool value)
	{
		____showSubText_2 = value;
	}

	inline static int32_t get_offset_of__selectionTexts_3() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogNornal_t1475688335, ____selectionTexts_3)); }
	inline List_1_t1125654279 * get__selectionTexts_3() const { return ____selectionTexts_3; }
	inline List_1_t1125654279 ** get_address_of__selectionTexts_3() { return &____selectionTexts_3; }
	inline void set__selectionTexts_3(List_1_t1125654279 * value)
	{
		____selectionTexts_3 = value;
		Il2CppCodeGenWriteBarrier(&____selectionTexts_3, value);
	}

	inline static int32_t get_offset_of__selectionButtons_4() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogNornal_t1475688335, ____selectionButtons_4)); }
	inline List_1_t1125654279 * get__selectionButtons_4() const { return ____selectionButtons_4; }
	inline List_1_t1125654279 ** get_address_of__selectionButtons_4() { return &____selectionButtons_4; }
	inline void set__selectionButtons_4(List_1_t1125654279 * value)
	{
		____selectionButtons_4 = value;
		Il2CppCodeGenWriteBarrier(&____selectionButtons_4, value);
	}

	inline static int32_t get_offset_of_selectionTextContainer_5() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogNornal_t1475688335, ___selectionTextContainer_5)); }
	inline GameObject_t1756533147 * get_selectionTextContainer_5() const { return ___selectionTextContainer_5; }
	inline GameObject_t1756533147 ** get_address_of_selectionTextContainer_5() { return &___selectionTextContainer_5; }
	inline void set_selectionTextContainer_5(GameObject_t1756533147 * value)
	{
		___selectionTextContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectionTextContainer_5, value);
	}

	inline static int32_t get_offset_of_selectionButtonContainer_6() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogNornal_t1475688335, ___selectionButtonContainer_6)); }
	inline GameObject_t1756533147 * get_selectionButtonContainer_6() const { return ___selectionButtonContainer_6; }
	inline GameObject_t1756533147 ** get_address_of_selectionButtonContainer_6() { return &___selectionButtonContainer_6; }
	inline void set_selectionButtonContainer_6(GameObject_t1756533147 * value)
	{
		___selectionButtonContainer_6 = value;
		Il2CppCodeGenWriteBarrier(&___selectionButtonContainer_6, value);
	}

	inline static int32_t get_offset_of_selectionTextPrefab_7() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogNornal_t1475688335, ___selectionTextPrefab_7)); }
	inline GameObject_t1756533147 * get_selectionTextPrefab_7() const { return ___selectionTextPrefab_7; }
	inline GameObject_t1756533147 ** get_address_of_selectionTextPrefab_7() { return &___selectionTextPrefab_7; }
	inline void set_selectionTextPrefab_7(GameObject_t1756533147 * value)
	{
		___selectionTextPrefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___selectionTextPrefab_7, value);
	}

	inline static int32_t get_offset_of_selectionButtonPrefab_8() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogNornal_t1475688335, ___selectionButtonPrefab_8)); }
	inline GameObject_t1756533147 * get_selectionButtonPrefab_8() const { return ___selectionButtonPrefab_8; }
	inline GameObject_t1756533147 ** get_address_of_selectionButtonPrefab_8() { return &___selectionButtonPrefab_8; }
	inline void set_selectionButtonPrefab_8(GameObject_t1756533147 * value)
	{
		___selectionButtonPrefab_8 = value;
		Il2CppCodeGenWriteBarrier(&___selectionButtonPrefab_8, value);
	}

	inline static int32_t get_offset_of__selectedData_9() { return static_cast<int32_t>(offsetof(ConversationSelectionDialogNornal_t1475688335, ____selectedData_9)); }
	inline ConversationSelectionData_t4090008535 * get__selectedData_9() const { return ____selectedData_9; }
	inline ConversationSelectionData_t4090008535 ** get_address_of__selectedData_9() { return &____selectedData_9; }
	inline void set__selectedData_9(ConversationSelectionData_t4090008535 * value)
	{
		____selectedData_9 = value;
		Il2CppCodeGenWriteBarrier(&____selectedData_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
