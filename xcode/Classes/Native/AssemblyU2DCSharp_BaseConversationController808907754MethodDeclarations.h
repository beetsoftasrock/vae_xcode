﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseConversationController
struct BaseConversationController_t808907754;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseConversationController_Conve1349939935.h"

// System.Void BaseConversationController::.ctor()
extern "C"  void BaseConversationController__ctor_m640768595 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseConversationController/ConversationState BaseConversationController::get_state()
extern "C"  int32_t BaseConversationController_get_state_m1207507495 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::set_state(BaseConversationController/ConversationState)
extern "C"  void BaseConversationController_set_state_m3719489868 (BaseConversationController_t808907754 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::OpenCharacterSelect()
extern "C"  void BaseConversationController_OpenCharacterSelect_m1059838826 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::StartConversation()
extern "C"  void BaseConversationController_StartConversation_m2104433922 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::Replay()
extern "C"  void BaseConversationController_Replay_m2692166214 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::AutoPlay()
extern "C"  void BaseConversationController_AutoPlay_m30757084 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::ShowCompleteConversation()
extern "C"  void BaseConversationController_ShowCompleteConversation_m23333332 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::ShowResult()
extern "C"  void BaseConversationController_ShowResult_m884750733 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::Awake()
extern "C"  void BaseConversationController_Awake_m3053018230 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::StartEyeDetect()
extern "C"  void BaseConversationController_StartEyeDetect_m3815611677 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::StopEyeDetect()
extern "C"  void BaseConversationController_StopEyeDetect_m483963721 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::HandleTimeOutEvent()
extern "C"  void BaseConversationController_HandleTimeOutEvent_m453894714 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BaseConversationController::GetTotalTalkingTime()
extern "C"  float BaseConversationController_GetTotalTalkingTime_m1912395546 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BaseConversationController::GetDefaultTalkTime()
extern "C"  float BaseConversationController_GetDefaultTalkTime_m2466909887 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::<StartConversation>m__0()
extern "C"  void BaseConversationController_U3CStartConversationU3Em__0_m2965218741 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::<AutoPlay>m__1()
extern "C"  void BaseConversationController_U3CAutoPlayU3Em__1_m225155512 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::<ShowResult>m__2()
extern "C"  void BaseConversationController_U3CShowResultU3Em__2_m666390902 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationController::<Awake>m__3()
extern "C"  void BaseConversationController_U3CAwakeU3Em__3_m1338957440 (BaseConversationController_t808907754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
