﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LearningChapterLogData
struct  LearningChapterLogData_t1224380751  : public Il2CppObject
{
public:
	// System.Int32 LearningChapterLogData::category
	int32_t ___category_0;
	// System.Int32 LearningChapterLogData::percent
	int32_t ___percent_1;
	// System.Int32 LearningChapterLogData::upDown
	int32_t ___upDown_2;

public:
	inline static int32_t get_offset_of_category_0() { return static_cast<int32_t>(offsetof(LearningChapterLogData_t1224380751, ___category_0)); }
	inline int32_t get_category_0() const { return ___category_0; }
	inline int32_t* get_address_of_category_0() { return &___category_0; }
	inline void set_category_0(int32_t value)
	{
		___category_0 = value;
	}

	inline static int32_t get_offset_of_percent_1() { return static_cast<int32_t>(offsetof(LearningChapterLogData_t1224380751, ___percent_1)); }
	inline int32_t get_percent_1() const { return ___percent_1; }
	inline int32_t* get_address_of_percent_1() { return &___percent_1; }
	inline void set_percent_1(int32_t value)
	{
		___percent_1 = value;
	}

	inline static int32_t get_offset_of_upDown_2() { return static_cast<int32_t>(offsetof(LearningChapterLogData_t1224380751, ___upDown_2)); }
	inline int32_t get_upDown_2() const { return ___upDown_2; }
	inline int32_t* get_address_of_upDown_2() { return &___upDown_2; }
	inline void set_upDown_2(int32_t value)
	{
		___upDown_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
