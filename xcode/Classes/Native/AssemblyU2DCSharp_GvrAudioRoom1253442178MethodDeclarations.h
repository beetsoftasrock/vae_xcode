﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrAudioRoom
struct GvrAudioRoom_t1253442178;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrAudioRoom::.ctor()
extern "C"  void GvrAudioRoom__ctor_m1306426045 (GvrAudioRoom_t1253442178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrAudioRoom::OnEnable()
extern "C"  void GvrAudioRoom_OnEnable_m2514172269 (GvrAudioRoom_t1253442178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrAudioRoom::OnDisable()
extern "C"  void GvrAudioRoom_OnDisable_m3702867154 (GvrAudioRoom_t1253442178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrAudioRoom::Update()
extern "C"  void GvrAudioRoom_Update_m2293437988 (GvrAudioRoom_t1253442178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrAudioRoom::OnDrawGizmosSelected()
extern "C"  void GvrAudioRoom_OnDrawGizmosSelected_m3667597680 (GvrAudioRoom_t1253442178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
