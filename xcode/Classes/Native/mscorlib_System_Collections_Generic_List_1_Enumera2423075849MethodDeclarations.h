﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<VocabularyOnplayQuestion>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1470442710(__this, ___l0, method) ((  void (*) (Enumerator_t2423075849 *, List_1_t2888346175 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VocabularyOnplayQuestion>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3065493620(__this, method) ((  void (*) (Enumerator_t2423075849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<VocabularyOnplayQuestion>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3398516266(__this, method) ((  Il2CppObject * (*) (Enumerator_t2423075849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VocabularyOnplayQuestion>::Dispose()
#define Enumerator_Dispose_m1455847983(__this, method) ((  void (*) (Enumerator_t2423075849 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VocabularyOnplayQuestion>::VerifyState()
#define Enumerator_VerifyState_m1637711476(__this, method) ((  void (*) (Enumerator_t2423075849 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<VocabularyOnplayQuestion>::MoveNext()
#define Enumerator_MoveNext_m1315539420(__this, method) ((  bool (*) (Enumerator_t2423075849 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<VocabularyOnplayQuestion>::get_Current()
#define Enumerator_get_Current_m1440504995(__this, method) ((  VocabularyOnplayQuestion_t3519225043 * (*) (Enumerator_t2423075849 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
