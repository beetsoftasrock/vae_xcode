﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalConfig
struct GlobalConfig_t3080413471;

#include "AssemblyU2DCSharp_BaseChapterSettingIniter2856709253.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterTopSettingIniter
struct  ChapterTopSettingIniter_t1747874135  : public BaseChapterSettingIniter_t2856709253
{
public:
	// GlobalConfig ChapterTopSettingIniter::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_3;

public:
	inline static int32_t get_offset_of_globalConfig_3() { return static_cast<int32_t>(offsetof(ChapterTopSettingIniter_t1747874135, ___globalConfig_3)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_3() const { return ___globalConfig_3; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_3() { return &___globalConfig_3; }
	inline void set_globalConfig_3(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_3 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
