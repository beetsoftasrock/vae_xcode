﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_QuestionData3323763512.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SentenceStructureIdiomQuestionData
struct  SentenceStructureIdiomQuestionData_t3251732102  : public QuestionData_t3323763512
{
public:
	// System.String SentenceStructureIdiomQuestionData::<answerText0>k__BackingField
	String_t* ___U3CanswerText0U3Ek__BackingField_0;
	// System.String SentenceStructureIdiomQuestionData::<answerText1>k__BackingField
	String_t* ___U3CanswerText1U3Ek__BackingField_1;
	// System.String SentenceStructureIdiomQuestionData::<answerText2>k__BackingField
	String_t* ___U3CanswerText2U3Ek__BackingField_2;
	// System.String SentenceStructureIdiomQuestionData::<answerText3>k__BackingField
	String_t* ___U3CanswerText3U3Ek__BackingField_3;
	// System.Int32 SentenceStructureIdiomQuestionData::<correctAnswer>k__BackingField
	int32_t ___U3CcorrectAnswerU3Ek__BackingField_4;
	// System.String SentenceStructureIdiomQuestionData::<questionTextJP>k__BackingField
	String_t* ___U3CquestionTextJPU3Ek__BackingField_5;
	// System.String SentenceStructureIdiomQuestionData::<questionTextEN>k__BackingField
	String_t* ___U3CquestionTextENU3Ek__BackingField_6;
	// System.String SentenceStructureIdiomQuestionData::<soundData>k__BackingField
	String_t* ___U3CsoundDataU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CanswerText0U3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomQuestionData_t3251732102, ___U3CanswerText0U3Ek__BackingField_0)); }
	inline String_t* get_U3CanswerText0U3Ek__BackingField_0() const { return ___U3CanswerText0U3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CanswerText0U3Ek__BackingField_0() { return &___U3CanswerText0U3Ek__BackingField_0; }
	inline void set_U3CanswerText0U3Ek__BackingField_0(String_t* value)
	{
		___U3CanswerText0U3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanswerText0U3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CanswerText1U3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomQuestionData_t3251732102, ___U3CanswerText1U3Ek__BackingField_1)); }
	inline String_t* get_U3CanswerText1U3Ek__BackingField_1() const { return ___U3CanswerText1U3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CanswerText1U3Ek__BackingField_1() { return &___U3CanswerText1U3Ek__BackingField_1; }
	inline void set_U3CanswerText1U3Ek__BackingField_1(String_t* value)
	{
		___U3CanswerText1U3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanswerText1U3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CanswerText2U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomQuestionData_t3251732102, ___U3CanswerText2U3Ek__BackingField_2)); }
	inline String_t* get_U3CanswerText2U3Ek__BackingField_2() const { return ___U3CanswerText2U3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CanswerText2U3Ek__BackingField_2() { return &___U3CanswerText2U3Ek__BackingField_2; }
	inline void set_U3CanswerText2U3Ek__BackingField_2(String_t* value)
	{
		___U3CanswerText2U3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanswerText2U3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CanswerText3U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomQuestionData_t3251732102, ___U3CanswerText3U3Ek__BackingField_3)); }
	inline String_t* get_U3CanswerText3U3Ek__BackingField_3() const { return ___U3CanswerText3U3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CanswerText3U3Ek__BackingField_3() { return &___U3CanswerText3U3Ek__BackingField_3; }
	inline void set_U3CanswerText3U3Ek__BackingField_3(String_t* value)
	{
		___U3CanswerText3U3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CanswerText3U3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CcorrectAnswerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomQuestionData_t3251732102, ___U3CcorrectAnswerU3Ek__BackingField_4)); }
	inline int32_t get_U3CcorrectAnswerU3Ek__BackingField_4() const { return ___U3CcorrectAnswerU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CcorrectAnswerU3Ek__BackingField_4() { return &___U3CcorrectAnswerU3Ek__BackingField_4; }
	inline void set_U3CcorrectAnswerU3Ek__BackingField_4(int32_t value)
	{
		___U3CcorrectAnswerU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CquestionTextJPU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomQuestionData_t3251732102, ___U3CquestionTextJPU3Ek__BackingField_5)); }
	inline String_t* get_U3CquestionTextJPU3Ek__BackingField_5() const { return ___U3CquestionTextJPU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CquestionTextJPU3Ek__BackingField_5() { return &___U3CquestionTextJPU3Ek__BackingField_5; }
	inline void set_U3CquestionTextJPU3Ek__BackingField_5(String_t* value)
	{
		___U3CquestionTextJPU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionTextJPU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CquestionTextENU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomQuestionData_t3251732102, ___U3CquestionTextENU3Ek__BackingField_6)); }
	inline String_t* get_U3CquestionTextENU3Ek__BackingField_6() const { return ___U3CquestionTextENU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CquestionTextENU3Ek__BackingField_6() { return &___U3CquestionTextENU3Ek__BackingField_6; }
	inline void set_U3CquestionTextENU3Ek__BackingField_6(String_t* value)
	{
		___U3CquestionTextENU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionTextENU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CsoundDataU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomQuestionData_t3251732102, ___U3CsoundDataU3Ek__BackingField_7)); }
	inline String_t* get_U3CsoundDataU3Ek__BackingField_7() const { return ___U3CsoundDataU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CsoundDataU3Ek__BackingField_7() { return &___U3CsoundDataU3Ek__BackingField_7; }
	inline void set_U3CsoundDataU3Ek__BackingField_7(String_t* value)
	{
		___U3CsoundDataU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsoundDataU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
