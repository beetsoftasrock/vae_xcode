﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedVocabularySceneSetup
struct ChapterBasedVocabularySceneSetup_t4149890173;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedVocabularySceneSetup::.ctor()
extern "C"  void ChapterBasedVocabularySceneSetup__ctor_m3257606444 (ChapterBasedVocabularySceneSetup_t4149890173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedVocabularySceneSetup::Awake()
extern "C"  void ChapterBasedVocabularySceneSetup_Awake_m2623319945 (ChapterBasedVocabularySceneSetup_t4149890173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
