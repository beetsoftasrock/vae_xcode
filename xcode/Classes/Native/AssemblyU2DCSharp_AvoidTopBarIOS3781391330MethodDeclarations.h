﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AvoidTopBarIOS
struct AvoidTopBarIOS_t3781391330;

#include "codegen/il2cpp-codegen.h"

// System.Void AvoidTopBarIOS::.ctor()
extern "C"  void AvoidTopBarIOS__ctor_m2409099537 (AvoidTopBarIOS_t3781391330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvoidTopBarIOS::Start()
extern "C"  void AvoidTopBarIOS_Start_m2050793505 (AvoidTopBarIOS_t3781391330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
