﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EyeDetectTarget
struct EyeDetectTarget_t3038356393;

#include "codegen/il2cpp-codegen.h"

// System.Void EyeDetectTarget::.ctor()
extern "C"  void EyeDetectTarget__ctor_m2815669006 (EyeDetectTarget_t3038356393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EyeDetectTarget::Awake()
extern "C"  void EyeDetectTarget_Awake_m43255317 (EyeDetectTarget_t3038356393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
