﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TermsOfUse
struct TermsOfUse_t532492745;

#include "codegen/il2cpp-codegen.h"

// System.Void TermsOfUse::.ctor()
extern "C"  void TermsOfUse__ctor_m4215615372 (TermsOfUse_t532492745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TermsOfUse::Start()
extern "C"  void TermsOfUse_Start_m3959827380 (TermsOfUse_t532492745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TermsOfUse::OnDisable()
extern "C"  void TermsOfUse_OnDisable_m3535147977 (TermsOfUse_t532492745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TermsOfUse::Update()
extern "C"  void TermsOfUse_Update_m999626325 (TermsOfUse_t532492745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
