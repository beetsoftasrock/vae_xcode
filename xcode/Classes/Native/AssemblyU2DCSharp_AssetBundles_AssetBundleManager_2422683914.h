﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action
struct Action_t3226471752;
// AssetBundles.AssetBundleManager
struct AssetBundleManager_t364944953;
// System.Object
struct Il2CppObject;
// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8
struct U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063;
// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey9
struct U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1
struct  U3CloadAssetBundlesU3Ec__Iterator1_t2422683914  : public Il2CppObject
{
public:
	// System.Action`1<System.Single> AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::onProcess
	Action_1_t1878309314 * ___onProcess_0;
	// System.Action`1<System.String> AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::onError
	Action_1_t1831019615 * ___onError_1;
	// System.String[] AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::includeBundles
	StringU5BU5D_t1642385972* ___includeBundles_2;
	// System.Action AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::onSuccess
	Action_t3226471752 * ___onSuccess_3;
	// AssetBundles.AssetBundleManager AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::$this
	AssetBundleManager_t364944953 * ___U24this_4;
	// System.Object AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::$PC
	int32_t ___U24PC_7;
	// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8 AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::$locvar0
	U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 * ___U24locvar0_8;
	// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey9 AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::$locvar2
	U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062 * ___U24locvar2_9;

public:
	inline static int32_t get_offset_of_onProcess_0() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___onProcess_0)); }
	inline Action_1_t1878309314 * get_onProcess_0() const { return ___onProcess_0; }
	inline Action_1_t1878309314 ** get_address_of_onProcess_0() { return &___onProcess_0; }
	inline void set_onProcess_0(Action_1_t1878309314 * value)
	{
		___onProcess_0 = value;
		Il2CppCodeGenWriteBarrier(&___onProcess_0, value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___onError_1)); }
	inline Action_1_t1831019615 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1831019615 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1831019615 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier(&___onError_1, value);
	}

	inline static int32_t get_offset_of_includeBundles_2() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___includeBundles_2)); }
	inline StringU5BU5D_t1642385972* get_includeBundles_2() const { return ___includeBundles_2; }
	inline StringU5BU5D_t1642385972** get_address_of_includeBundles_2() { return &___includeBundles_2; }
	inline void set_includeBundles_2(StringU5BU5D_t1642385972* value)
	{
		___includeBundles_2 = value;
		Il2CppCodeGenWriteBarrier(&___includeBundles_2, value);
	}

	inline static int32_t get_offset_of_onSuccess_3() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___onSuccess_3)); }
	inline Action_t3226471752 * get_onSuccess_3() const { return ___onSuccess_3; }
	inline Action_t3226471752 ** get_address_of_onSuccess_3() { return &___onSuccess_3; }
	inline void set_onSuccess_3(Action_t3226471752 * value)
	{
		___onSuccess_3 = value;
		Il2CppCodeGenWriteBarrier(&___onSuccess_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___U24this_4)); }
	inline AssetBundleManager_t364944953 * get_U24this_4() const { return ___U24this_4; }
	inline AssetBundleManager_t364944953 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AssetBundleManager_t364944953 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___U24locvar0_8)); }
	inline U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 * get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 ** get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 * value)
	{
		___U24locvar0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_8, value);
	}

	inline static int32_t get_offset_of_U24locvar2_9() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914, ___U24locvar2_9)); }
	inline U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062 * get_U24locvar2_9() const { return ___U24locvar2_9; }
	inline U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062 ** get_address_of_U24locvar2_9() { return &___U24locvar2_9; }
	inline void set_U24locvar2_9(U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062 * value)
	{
		___U24locvar2_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar2_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
