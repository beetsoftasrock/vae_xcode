﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundHelperIOS
struct SoundHelperIOS_t3377713620;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundHelperIOS::.ctor()
extern "C"  void SoundHelperIOS__ctor_m132359411 (SoundHelperIOS_t3377713620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundHelperIOS::OnLevelWasLoaded(System.Int32)
extern "C"  void SoundHelperIOS_OnLevelWasLoaded_m1743375395 (SoundHelperIOS_t3377713620 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundHelperIOS::Awake()
extern "C"  void SoundHelperIOS_Awake_m3795196688 (SoundHelperIOS_t3377713620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
