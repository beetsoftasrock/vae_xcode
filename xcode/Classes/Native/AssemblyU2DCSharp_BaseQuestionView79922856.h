﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseQuestionView
struct  BaseQuestionView_t79922856  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text BaseQuestionView::progressText
	Text_t356221433 * ___progressText_2;

public:
	inline static int32_t get_offset_of_progressText_2() { return static_cast<int32_t>(offsetof(BaseQuestionView_t79922856, ___progressText_2)); }
	inline Text_t356221433 * get_progressText_2() const { return ___progressText_2; }
	inline Text_t356221433 ** get_address_of_progressText_2() { return &___progressText_2; }
	inline void set_progressText_2(Text_t356221433 * value)
	{
		___progressText_2 = value;
		Il2CppCodeGenWriteBarrier(&___progressText_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
