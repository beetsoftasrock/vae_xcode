﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoEyeDetect
struct DemoEyeDetect_t1677323547;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoEyeDetect::.ctor()
extern "C"  void DemoEyeDetect__ctor_m1157141466 (DemoEyeDetect_t1677323547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoEyeDetect::OnTimeOut()
extern "C"  void DemoEyeDetect_OnTimeOut_m2624847498 (DemoEyeDetect_t1677323547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
