﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationLogicController
struct ConversationLogicController_t3911886281;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickConfirmAfterConversationComplete
struct  ClickConfirmAfterConversationComplete_t3062069762  : public MonoBehaviour_t1158329972
{
public:
	// ConversationLogicController ClickConfirmAfterConversationComplete::clc
	ConversationLogicController_t3911886281 * ___clc_2;
	// UnityEngine.GameObject ClickConfirmAfterConversationComplete::endDialog
	GameObject_t1756533147 * ___endDialog_3;

public:
	inline static int32_t get_offset_of_clc_2() { return static_cast<int32_t>(offsetof(ClickConfirmAfterConversationComplete_t3062069762, ___clc_2)); }
	inline ConversationLogicController_t3911886281 * get_clc_2() const { return ___clc_2; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_2() { return &___clc_2; }
	inline void set_clc_2(ConversationLogicController_t3911886281 * value)
	{
		___clc_2 = value;
		Il2CppCodeGenWriteBarrier(&___clc_2, value);
	}

	inline static int32_t get_offset_of_endDialog_3() { return static_cast<int32_t>(offsetof(ClickConfirmAfterConversationComplete_t3062069762, ___endDialog_3)); }
	inline GameObject_t1756533147 * get_endDialog_3() const { return ___endDialog_3; }
	inline GameObject_t1756533147 ** get_address_of_endDialog_3() { return &___endDialog_3; }
	inline void set_endDialog_3(GameObject_t1756533147 * value)
	{
		___endDialog_3 = value;
		Il2CppCodeGenWriteBarrier(&___endDialog_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
