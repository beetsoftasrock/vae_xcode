﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.UI.Button
struct Button_t2872111280;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterLearningItemView
struct  ChapterLearningItemView_t1984184315  : public MonoBehaviour_t1158329972
{
public:
	// System.String ChapterLearningItemView::_name
	String_t* ____name_2;
	// System.Single ChapterLearningItemView::_percent
	float ____percent_3;
	// UnityEngine.UI.Text ChapterLearningItemView::titleText
	Text_t356221433 * ___titleText_4;
	// UnityEngine.UI.Text ChapterLearningItemView::percentText
	Text_t356221433 * ___percentText_5;
	// UnityEngine.UI.Image ChapterLearningItemView::fillSliderImage
	Image_t2042527209 * ___fillSliderImage_6;
	// UnityEngine.UI.Slider ChapterLearningItemView::slider
	Slider_t297367283 * ___slider_7;
	// UnityEngine.UI.Image ChapterLearningItemView::sliderBackground
	Image_t2042527209 * ___sliderBackground_8;
	// UnityEngine.UI.Button ChapterLearningItemView::studyButton
	Button_t2872111280 * ___studyButton_9;
	// UnityEngine.UI.Button ChapterLearningItemView::autoButton
	Button_t2872111280 * ___autoButton_10;
	// UnityEngine.UI.Image ChapterLearningItemView::autoButtonImage
	Image_t2042527209 * ___autoButtonImage_11;
	// UnityEngine.Color ChapterLearningItemView::_mainColor
	Color_t2020392075  ____mainColor_12;

public:
	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier(&____name_2, value);
	}

	inline static int32_t get_offset_of__percent_3() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ____percent_3)); }
	inline float get__percent_3() const { return ____percent_3; }
	inline float* get_address_of__percent_3() { return &____percent_3; }
	inline void set__percent_3(float value)
	{
		____percent_3 = value;
	}

	inline static int32_t get_offset_of_titleText_4() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ___titleText_4)); }
	inline Text_t356221433 * get_titleText_4() const { return ___titleText_4; }
	inline Text_t356221433 ** get_address_of_titleText_4() { return &___titleText_4; }
	inline void set_titleText_4(Text_t356221433 * value)
	{
		___titleText_4 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_4, value);
	}

	inline static int32_t get_offset_of_percentText_5() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ___percentText_5)); }
	inline Text_t356221433 * get_percentText_5() const { return ___percentText_5; }
	inline Text_t356221433 ** get_address_of_percentText_5() { return &___percentText_5; }
	inline void set_percentText_5(Text_t356221433 * value)
	{
		___percentText_5 = value;
		Il2CppCodeGenWriteBarrier(&___percentText_5, value);
	}

	inline static int32_t get_offset_of_fillSliderImage_6() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ___fillSliderImage_6)); }
	inline Image_t2042527209 * get_fillSliderImage_6() const { return ___fillSliderImage_6; }
	inline Image_t2042527209 ** get_address_of_fillSliderImage_6() { return &___fillSliderImage_6; }
	inline void set_fillSliderImage_6(Image_t2042527209 * value)
	{
		___fillSliderImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___fillSliderImage_6, value);
	}

	inline static int32_t get_offset_of_slider_7() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ___slider_7)); }
	inline Slider_t297367283 * get_slider_7() const { return ___slider_7; }
	inline Slider_t297367283 ** get_address_of_slider_7() { return &___slider_7; }
	inline void set_slider_7(Slider_t297367283 * value)
	{
		___slider_7 = value;
		Il2CppCodeGenWriteBarrier(&___slider_7, value);
	}

	inline static int32_t get_offset_of_sliderBackground_8() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ___sliderBackground_8)); }
	inline Image_t2042527209 * get_sliderBackground_8() const { return ___sliderBackground_8; }
	inline Image_t2042527209 ** get_address_of_sliderBackground_8() { return &___sliderBackground_8; }
	inline void set_sliderBackground_8(Image_t2042527209 * value)
	{
		___sliderBackground_8 = value;
		Il2CppCodeGenWriteBarrier(&___sliderBackground_8, value);
	}

	inline static int32_t get_offset_of_studyButton_9() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ___studyButton_9)); }
	inline Button_t2872111280 * get_studyButton_9() const { return ___studyButton_9; }
	inline Button_t2872111280 ** get_address_of_studyButton_9() { return &___studyButton_9; }
	inline void set_studyButton_9(Button_t2872111280 * value)
	{
		___studyButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___studyButton_9, value);
	}

	inline static int32_t get_offset_of_autoButton_10() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ___autoButton_10)); }
	inline Button_t2872111280 * get_autoButton_10() const { return ___autoButton_10; }
	inline Button_t2872111280 ** get_address_of_autoButton_10() { return &___autoButton_10; }
	inline void set_autoButton_10(Button_t2872111280 * value)
	{
		___autoButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___autoButton_10, value);
	}

	inline static int32_t get_offset_of_autoButtonImage_11() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ___autoButtonImage_11)); }
	inline Image_t2042527209 * get_autoButtonImage_11() const { return ___autoButtonImage_11; }
	inline Image_t2042527209 ** get_address_of_autoButtonImage_11() { return &___autoButtonImage_11; }
	inline void set_autoButtonImage_11(Image_t2042527209 * value)
	{
		___autoButtonImage_11 = value;
		Il2CppCodeGenWriteBarrier(&___autoButtonImage_11, value);
	}

	inline static int32_t get_offset_of__mainColor_12() { return static_cast<int32_t>(offsetof(ChapterLearningItemView_t1984184315, ____mainColor_12)); }
	inline Color_t2020392075  get__mainColor_12() const { return ____mainColor_12; }
	inline Color_t2020392075 * get_address_of__mainColor_12() { return &____mainColor_12; }
	inline void set__mainColor_12(Color_t2020392075  value)
	{
		____mainColor_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
