﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultStatisticView
struct ResultStatisticView_t1911483820;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultStatisticView::.ctor()
extern "C"  void ResultStatisticView__ctor_m550040937 (ResultStatisticView_t1911483820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticView::Awake()
extern "C"  void ResultStatisticView_Awake_m322108840 (ResultStatisticView_t1911483820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticView::<Awake>m__0()
extern "C"  void ResultStatisticView_U3CAwakeU3Em__0_m325743287 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
