﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZipSoundItem
struct  ZipSoundItem_t558808959  : public Il2CppObject
{
public:
	// System.Int32 ZipSoundItem::chapter
	int32_t ___chapter_0;
	// System.Int32 ZipSoundItem::category
	int32_t ___category_1;
	// System.Int32 ZipSoundItem::part
	int32_t ___part_2;
	// System.String ZipSoundItem::voice
	String_t* ___voice_3;

public:
	inline static int32_t get_offset_of_chapter_0() { return static_cast<int32_t>(offsetof(ZipSoundItem_t558808959, ___chapter_0)); }
	inline int32_t get_chapter_0() const { return ___chapter_0; }
	inline int32_t* get_address_of_chapter_0() { return &___chapter_0; }
	inline void set_chapter_0(int32_t value)
	{
		___chapter_0 = value;
	}

	inline static int32_t get_offset_of_category_1() { return static_cast<int32_t>(offsetof(ZipSoundItem_t558808959, ___category_1)); }
	inline int32_t get_category_1() const { return ___category_1; }
	inline int32_t* get_address_of_category_1() { return &___category_1; }
	inline void set_category_1(int32_t value)
	{
		___category_1 = value;
	}

	inline static int32_t get_offset_of_part_2() { return static_cast<int32_t>(offsetof(ZipSoundItem_t558808959, ___part_2)); }
	inline int32_t get_part_2() const { return ___part_2; }
	inline int32_t* get_address_of_part_2() { return &___part_2; }
	inline void set_part_2(int32_t value)
	{
		___part_2 = value;
	}

	inline static int32_t get_offset_of_voice_3() { return static_cast<int32_t>(offsetof(ZipSoundItem_t558808959, ___voice_3)); }
	inline String_t* get_voice_3() const { return ___voice_3; }
	inline String_t** get_address_of_voice_3() { return &___voice_3; }
	inline void set_voice_3(String_t* value)
	{
		___voice_3 = value;
		Il2CppCodeGenWriteBarrier(&___voice_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
