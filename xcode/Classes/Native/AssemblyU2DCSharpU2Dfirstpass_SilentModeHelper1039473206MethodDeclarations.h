﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SilentModeHelper
struct SilentModeHelper_t1039473206;

#include "codegen/il2cpp-codegen.h"

// System.Void SilentModeHelper::.ctor()
extern "C"  void SilentModeHelper__ctor_m961678781 (SilentModeHelper_t1039473206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SilentModeHelper::_activeSilentMode()
extern "C"  void SilentModeHelper__activeSilentMode_m535670354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SilentModeHelper::_deactiveSilentMode()
extern "C"  void SilentModeHelper__deactiveSilentMode_m582413417 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SilentModeHelper::isActiveSilent()
extern "C"  bool SilentModeHelper_isActiveSilent_m533583534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SilentModeHelper::ActiveSilentMode()
extern "C"  void SilentModeHelper_ActiveSilentMode_m2511133439 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SilentModeHelper::DeactiveSilentMode()
extern "C"  void SilentModeHelper_DeactiveSilentMode_m2234628724 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
