﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VocabularyDatasWrapper
struct VocabularyDatasWrapper_t3354957772;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// System.Collections.Generic.List`1<VocabularyQuestionData>
struct List_1_t1390196108;
// VocabularyQuestionData
struct VocabularyQuestionData_t2021074976;
// CommandSheet/Param
struct Param_t4123818474;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CommandSheet_Param4123818474.h"

// System.Void VocabularyDatasWrapper::.ctor(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void VocabularyDatasWrapper__ctor_m2531356103 (VocabularyDatasWrapper_t3354957772 * __this, List_1_t3492939606 * ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<VocabularyQuestionData> VocabularyDatasWrapper::GetConvertedDatas()
extern "C"  List_1_t1390196108 * VocabularyDatasWrapper_GetConvertedDatas_m3303901147 (VocabularyDatasWrapper_t3354957772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VocabularyQuestionData VocabularyDatasWrapper::ConvertData(CommandSheet/Param,System.Boolean)
extern "C"  VocabularyQuestionData_t2021074976 * VocabularyDatasWrapper_ConvertData_m4198705814 (VocabularyDatasWrapper_t3354957772 * __this, Param_t4123818474 * ___param0, bool ___IsRandomQuestionOder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
