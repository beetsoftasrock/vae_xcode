﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegisterSceneController
struct  RegisterSceneController_t27556205  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text RegisterSceneController::inputUserNameText
	Text_t356221433 * ___inputUserNameText_8;
	// UnityEngine.UI.Text RegisterSceneController::resultText
	Text_t356221433 * ___resultText_9;
	// UnityEngine.UI.Image RegisterSceneController::nicknameInputBg
	Image_t2042527209 * ___nicknameInputBg_10;
	// UnityEngine.Sprite RegisterSceneController::normalTextboxSprite
	Sprite_t309593783 * ___normalTextboxSprite_11;
	// UnityEngine.Sprite RegisterSceneController::highlightTextboxSprite
	Sprite_t309593783 * ___highlightTextboxSprite_12;
	// UnityEngine.UI.Button RegisterSceneController::summitButton
	Button_t2872111280 * ___summitButton_13;
	// System.String RegisterSceneController::nextSceneName
	String_t* ___nextSceneName_14;
	// System.String RegisterSceneController::username
	String_t* ___username_15;

public:
	inline static int32_t get_offset_of_inputUserNameText_8() { return static_cast<int32_t>(offsetof(RegisterSceneController_t27556205, ___inputUserNameText_8)); }
	inline Text_t356221433 * get_inputUserNameText_8() const { return ___inputUserNameText_8; }
	inline Text_t356221433 ** get_address_of_inputUserNameText_8() { return &___inputUserNameText_8; }
	inline void set_inputUserNameText_8(Text_t356221433 * value)
	{
		___inputUserNameText_8 = value;
		Il2CppCodeGenWriteBarrier(&___inputUserNameText_8, value);
	}

	inline static int32_t get_offset_of_resultText_9() { return static_cast<int32_t>(offsetof(RegisterSceneController_t27556205, ___resultText_9)); }
	inline Text_t356221433 * get_resultText_9() const { return ___resultText_9; }
	inline Text_t356221433 ** get_address_of_resultText_9() { return &___resultText_9; }
	inline void set_resultText_9(Text_t356221433 * value)
	{
		___resultText_9 = value;
		Il2CppCodeGenWriteBarrier(&___resultText_9, value);
	}

	inline static int32_t get_offset_of_nicknameInputBg_10() { return static_cast<int32_t>(offsetof(RegisterSceneController_t27556205, ___nicknameInputBg_10)); }
	inline Image_t2042527209 * get_nicknameInputBg_10() const { return ___nicknameInputBg_10; }
	inline Image_t2042527209 ** get_address_of_nicknameInputBg_10() { return &___nicknameInputBg_10; }
	inline void set_nicknameInputBg_10(Image_t2042527209 * value)
	{
		___nicknameInputBg_10 = value;
		Il2CppCodeGenWriteBarrier(&___nicknameInputBg_10, value);
	}

	inline static int32_t get_offset_of_normalTextboxSprite_11() { return static_cast<int32_t>(offsetof(RegisterSceneController_t27556205, ___normalTextboxSprite_11)); }
	inline Sprite_t309593783 * get_normalTextboxSprite_11() const { return ___normalTextboxSprite_11; }
	inline Sprite_t309593783 ** get_address_of_normalTextboxSprite_11() { return &___normalTextboxSprite_11; }
	inline void set_normalTextboxSprite_11(Sprite_t309593783 * value)
	{
		___normalTextboxSprite_11 = value;
		Il2CppCodeGenWriteBarrier(&___normalTextboxSprite_11, value);
	}

	inline static int32_t get_offset_of_highlightTextboxSprite_12() { return static_cast<int32_t>(offsetof(RegisterSceneController_t27556205, ___highlightTextboxSprite_12)); }
	inline Sprite_t309593783 * get_highlightTextboxSprite_12() const { return ___highlightTextboxSprite_12; }
	inline Sprite_t309593783 ** get_address_of_highlightTextboxSprite_12() { return &___highlightTextboxSprite_12; }
	inline void set_highlightTextboxSprite_12(Sprite_t309593783 * value)
	{
		___highlightTextboxSprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___highlightTextboxSprite_12, value);
	}

	inline static int32_t get_offset_of_summitButton_13() { return static_cast<int32_t>(offsetof(RegisterSceneController_t27556205, ___summitButton_13)); }
	inline Button_t2872111280 * get_summitButton_13() const { return ___summitButton_13; }
	inline Button_t2872111280 ** get_address_of_summitButton_13() { return &___summitButton_13; }
	inline void set_summitButton_13(Button_t2872111280 * value)
	{
		___summitButton_13 = value;
		Il2CppCodeGenWriteBarrier(&___summitButton_13, value);
	}

	inline static int32_t get_offset_of_nextSceneName_14() { return static_cast<int32_t>(offsetof(RegisterSceneController_t27556205, ___nextSceneName_14)); }
	inline String_t* get_nextSceneName_14() const { return ___nextSceneName_14; }
	inline String_t** get_address_of_nextSceneName_14() { return &___nextSceneName_14; }
	inline void set_nextSceneName_14(String_t* value)
	{
		___nextSceneName_14 = value;
		Il2CppCodeGenWriteBarrier(&___nextSceneName_14, value);
	}

	inline static int32_t get_offset_of_username_15() { return static_cast<int32_t>(offsetof(RegisterSceneController_t27556205, ___username_15)); }
	inline String_t* get_username_15() const { return ___username_15; }
	inline String_t** get_address_of_username_15() { return &___username_15; }
	inline void set_username_15(String_t* value)
	{
		___username_15 = value;
		Il2CppCodeGenWriteBarrier(&___username_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
