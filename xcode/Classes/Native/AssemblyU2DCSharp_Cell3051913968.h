﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LessionName1201611902.h"
#include "AssemblyU2DCSharp_SceneName904752595.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cell
struct  Cell_t3051913968  : public Il2CppObject
{
public:
	// LessionName Cell::lessionName
	int32_t ___lessionName_0;
	// SceneName Cell::sceneName
	int32_t ___sceneName_1;
	// UnityEngine.Transform Cell::cellTransform
	Transform_t3275118058 * ___cellTransform_2;
	// System.Single Cell::percent
	float ___percent_3;
	// System.Single Cell::percentPrev
	float ___percentPrev_4;

public:
	inline static int32_t get_offset_of_lessionName_0() { return static_cast<int32_t>(offsetof(Cell_t3051913968, ___lessionName_0)); }
	inline int32_t get_lessionName_0() const { return ___lessionName_0; }
	inline int32_t* get_address_of_lessionName_0() { return &___lessionName_0; }
	inline void set_lessionName_0(int32_t value)
	{
		___lessionName_0 = value;
	}

	inline static int32_t get_offset_of_sceneName_1() { return static_cast<int32_t>(offsetof(Cell_t3051913968, ___sceneName_1)); }
	inline int32_t get_sceneName_1() const { return ___sceneName_1; }
	inline int32_t* get_address_of_sceneName_1() { return &___sceneName_1; }
	inline void set_sceneName_1(int32_t value)
	{
		___sceneName_1 = value;
	}

	inline static int32_t get_offset_of_cellTransform_2() { return static_cast<int32_t>(offsetof(Cell_t3051913968, ___cellTransform_2)); }
	inline Transform_t3275118058 * get_cellTransform_2() const { return ___cellTransform_2; }
	inline Transform_t3275118058 ** get_address_of_cellTransform_2() { return &___cellTransform_2; }
	inline void set_cellTransform_2(Transform_t3275118058 * value)
	{
		___cellTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___cellTransform_2, value);
	}

	inline static int32_t get_offset_of_percent_3() { return static_cast<int32_t>(offsetof(Cell_t3051913968, ___percent_3)); }
	inline float get_percent_3() const { return ___percent_3; }
	inline float* get_address_of_percent_3() { return &___percent_3; }
	inline void set_percent_3(float value)
	{
		___percent_3 = value;
	}

	inline static int32_t get_offset_of_percentPrev_4() { return static_cast<int32_t>(offsetof(Cell_t3051913968, ___percentPrev_4)); }
	inline float get_percentPrev_4() const { return ___percentPrev_4; }
	inline float* get_address_of_percentPrev_4() { return &___percentPrev_4; }
	inline void set_percentPrev_4(float value)
	{
		___percentPrev_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
