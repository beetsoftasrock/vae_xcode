﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationVRSwitcher/<GoToNormalMode>c__AnonStorey0
struct U3CGoToNormalModeU3Ec__AnonStorey0_t3065078691;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationVRSwitcher/<GoToNormalMode>c__AnonStorey0::.ctor()
extern "C"  void U3CGoToNormalModeU3Ec__AnonStorey0__ctor_m767056180 (U3CGoToNormalModeU3Ec__AnonStorey0_t3065078691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher/<GoToNormalMode>c__AnonStorey0::<>m__0()
extern "C"  void U3CGoToNormalModeU3Ec__AnonStorey0_U3CU3Em__0_m722723921 (U3CGoToNormalModeU3Ec__AnonStorey0_t3065078691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
