﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterConfigData
struct ChapterConfigData_t999749205;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterConfigData::.ctor()
extern "C"  void ChapterConfigData__ctor_m2892189418 (ChapterConfigData_t999749205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
