﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0
struct U3CloadManifestFileU3Ec__Iterator0_t2549557299;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::.ctor()
extern "C"  void U3CloadManifestFileU3Ec__Iterator0__ctor_m1600620514 (U3CloadManifestFileU3Ec__Iterator0_t2549557299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::MoveNext()
extern "C"  bool U3CloadManifestFileU3Ec__Iterator0_MoveNext_m1405524514 (U3CloadManifestFileU3Ec__Iterator0_t2549557299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CloadManifestFileU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m265784322 (U3CloadManifestFileU3Ec__Iterator0_t2549557299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CloadManifestFileU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1458201226 (U3CloadManifestFileU3Ec__Iterator0_t2549557299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::Dispose()
extern "C"  void U3CloadManifestFileU3Ec__Iterator0_Dispose_m3533442529 (U3CloadManifestFileU3Ec__Iterator0_t2549557299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::Reset()
extern "C"  void U3CloadManifestFileU3Ec__Iterator0_Reset_m3282247179 (U3CloadManifestFileU3Ec__Iterator0_t2549557299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
