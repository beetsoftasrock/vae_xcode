﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThinkingEffectModule
struct ThinkingEffectModule_t4011786459;
// UnityEngine.Sprite
struct Sprite_t309593783;
// GlobalConfig
struct GlobalConfig_t3080413471;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ThinkingEffectModule::.ctor()
extern "C"  void ThinkingEffectModule__ctor_m2984008056 (ThinkingEffectModule_t4011786459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThinkingEffectModule::OnConversationReset()
extern "C"  void ThinkingEffectModule_OnConversationReset_m1778378415 (ThinkingEffectModule_t4011786459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThinkingEffectModule::OnConversationReplay()
extern "C"  void ThinkingEffectModule_OnConversationReplay_m2665969105 (ThinkingEffectModule_t4011786459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite ThinkingEffectModule::get_preThinkingSprite()
extern "C"  Sprite_t309593783 * ThinkingEffectModule_get_preThinkingSprite_m3339677016 (ThinkingEffectModule_t4011786459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThinkingEffectModule::set_preThinkingSprite(UnityEngine.Sprite)
extern "C"  void ThinkingEffectModule_set_preThinkingSprite_m3083528107 (ThinkingEffectModule_t4011786459 * __this, Sprite_t309593783 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalConfig ThinkingEffectModule::get_globalConfig()
extern "C"  GlobalConfig_t3080413471 * ThinkingEffectModule_get_globalConfig_m1938132910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThinkingEffectModule::ShowThinking(System.String)
extern "C"  void ThinkingEffectModule_ShowThinking_m911824263 (ThinkingEffectModule_t4011786459 * __this, String_t* ___imageName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThinkingEffectModule::SetThinking(UnityEngine.Sprite)
extern "C"  void ThinkingEffectModule_SetThinking_m1086101530 (ThinkingEffectModule_t4011786459 * __this, Sprite_t309593783 * ___sprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
