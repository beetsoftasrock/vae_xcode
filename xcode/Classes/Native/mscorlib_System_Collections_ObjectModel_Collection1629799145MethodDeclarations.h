﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct Collection_1_t1629799145;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Beetsoft.VAE.Purchaser/PurchaseSession[]
struct PurchaseSessionU5BU5D_t64753102;
// System.Collections.Generic.IEnumerator`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct IEnumerator_1_t3858545514;
// System.Collections.Generic.IList`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct IList_1_t2628994992;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"

// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor()
extern "C"  void Collection_1__ctor_m613143110_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1__ctor_m613143110(__this, method) ((  void (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1__ctor_m613143110_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2197648809_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2197648809(__this, method) ((  bool (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2197648809_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1700127982_gshared (Collection_1_t1629799145 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1700127982(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1629799145 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1700127982_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m4083303961_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m4083303961(__this, method) ((  Il2CppObject * (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m4083303961_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2395985626_gshared (Collection_1_t1629799145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2395985626(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1629799145 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2395985626_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3263934360_gshared (Collection_1_t1629799145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3263934360(__this, ___value0, method) ((  bool (*) (Collection_1_t1629799145 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3263934360_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m930795564_gshared (Collection_1_t1629799145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m930795564(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1629799145 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m930795564_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m408898397_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m408898397(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1629799145 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m408898397_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1880181893_gshared (Collection_1_t1629799145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1880181893(__this, ___value0, method) ((  void (*) (Collection_1_t1629799145 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1880181893_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3789743150_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3789743150(__this, method) ((  bool (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3789743150_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4119140696_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4119140696(__this, method) ((  Il2CppObject * (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4119140696_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m354594849_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m354594849(__this, method) ((  bool (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m354594849_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2258417162_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2258417162(__this, method) ((  bool (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2258417162_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1540807381_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1540807381(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1629799145 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1540807381_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m621723848_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m621723848(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1629799145 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m621723848_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Add(T)
extern "C"  void Collection_1_Add_m2535385841_gshared (Collection_1_t1629799145 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define Collection_1_Add_m2535385841(__this, ___item0, method) ((  void (*) (Collection_1_t1629799145 *, PurchaseSession_t2088054391 , const MethodInfo*))Collection_1_Add_m2535385841_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Clear()
extern "C"  void Collection_1_Clear_m2323852333_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2323852333(__this, method) ((  void (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_Clear_m2323852333_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1490176555_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1490176555(__this, method) ((  void (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_ClearItems_m1490176555_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Contains(T)
extern "C"  bool Collection_1_Contains_m840870919_gshared (Collection_1_t1629799145 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m840870919(__this, ___item0, method) ((  bool (*) (Collection_1_t1629799145 *, PurchaseSession_t2088054391 , const MethodInfo*))Collection_1_Contains_m840870919_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3682877945_gshared (Collection_1_t1629799145 * __this, PurchaseSessionU5BU5D_t64753102* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3682877945(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1629799145 *, PurchaseSessionU5BU5D_t64753102*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3682877945_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3021747662_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3021747662(__this, method) ((  Il2CppObject* (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_GetEnumerator_m3021747662_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2343140841_gshared (Collection_1_t1629799145 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2343140841(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1629799145 *, PurchaseSession_t2088054391 , const MethodInfo*))Collection_1_IndexOf_m2343140841_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4003077512_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, PurchaseSession_t2088054391  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4003077512(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1629799145 *, int32_t, PurchaseSession_t2088054391 , const MethodInfo*))Collection_1_Insert_m4003077512_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1804719879_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, PurchaseSession_t2088054391  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1804719879(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1629799145 *, int32_t, PurchaseSession_t2088054391 , const MethodInfo*))Collection_1_InsertItem_m1804719879_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Remove(T)
extern "C"  bool Collection_1_Remove_m4262160006_gshared (Collection_1_t1629799145 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m4262160006(__this, ___item0, method) ((  bool (*) (Collection_1_t1629799145 *, PurchaseSession_t2088054391 , const MethodInfo*))Collection_1_Remove_m4262160006_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m121059940_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m121059940(__this, ___index0, method) ((  void (*) (Collection_1_t1629799145 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m121059940_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2743138094_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2743138094(__this, ___index0, method) ((  void (*) (Collection_1_t1629799145 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2743138094_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1798056522_gshared (Collection_1_t1629799145 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1798056522(__this, method) ((  int32_t (*) (Collection_1_t1629799145 *, const MethodInfo*))Collection_1_get_Count_m1798056522_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Item(System.Int32)
extern "C"  PurchaseSession_t2088054391  Collection_1_get_Item_m1934605400_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1934605400(__this, ___index0, method) ((  PurchaseSession_t2088054391  (*) (Collection_1_t1629799145 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1934605400_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1891002257_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, PurchaseSession_t2088054391  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1891002257(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1629799145 *, int32_t, PurchaseSession_t2088054391 , const MethodInfo*))Collection_1_set_Item_m1891002257_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2054952520_gshared (Collection_1_t1629799145 * __this, int32_t ___index0, PurchaseSession_t2088054391  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2054952520(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1629799145 *, int32_t, PurchaseSession_t2088054391 , const MethodInfo*))Collection_1_SetItem_m2054952520_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m681496475_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m681496475(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m681496475_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::ConvertItem(System.Object)
extern "C"  PurchaseSession_t2088054391  Collection_1_ConvertItem_m279663543_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m279663543(__this /* static, unused */, ___item0, method) ((  PurchaseSession_t2088054391  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m279663543_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m377015815_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m377015815(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m377015815_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m307290199_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m307290199(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m307290199_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3001977484_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3001977484(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3001977484_gshared)(__this /* static, unused */, ___list0, method)
