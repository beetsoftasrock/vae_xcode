﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VocabularyController
struct VocabularyController_t843712768;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectItem
struct  SelectItem_t2844432199  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text SelectItem::text
	Text_t356221433 * ___text_2;
	// UnityEngine.GameObject SelectItem::offBG
	GameObject_t1756533147 * ___offBG_3;
	// UnityEngine.GameObject SelectItem::onBG
	GameObject_t1756533147 * ___onBG_4;
	// System.Boolean SelectItem::selected
	bool ___selected_5;
	// VocabularyController SelectItem::controller
	VocabularyController_t843712768 * ___controller_6;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(SelectItem_t2844432199, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_offBG_3() { return static_cast<int32_t>(offsetof(SelectItem_t2844432199, ___offBG_3)); }
	inline GameObject_t1756533147 * get_offBG_3() const { return ___offBG_3; }
	inline GameObject_t1756533147 ** get_address_of_offBG_3() { return &___offBG_3; }
	inline void set_offBG_3(GameObject_t1756533147 * value)
	{
		___offBG_3 = value;
		Il2CppCodeGenWriteBarrier(&___offBG_3, value);
	}

	inline static int32_t get_offset_of_onBG_4() { return static_cast<int32_t>(offsetof(SelectItem_t2844432199, ___onBG_4)); }
	inline GameObject_t1756533147 * get_onBG_4() const { return ___onBG_4; }
	inline GameObject_t1756533147 ** get_address_of_onBG_4() { return &___onBG_4; }
	inline void set_onBG_4(GameObject_t1756533147 * value)
	{
		___onBG_4 = value;
		Il2CppCodeGenWriteBarrier(&___onBG_4, value);
	}

	inline static int32_t get_offset_of_selected_5() { return static_cast<int32_t>(offsetof(SelectItem_t2844432199, ___selected_5)); }
	inline bool get_selected_5() const { return ___selected_5; }
	inline bool* get_address_of_selected_5() { return &___selected_5; }
	inline void set_selected_5(bool value)
	{
		___selected_5 = value;
	}

	inline static int32_t get_offset_of_controller_6() { return static_cast<int32_t>(offsetof(SelectItem_t2844432199, ___controller_6)); }
	inline VocabularyController_t843712768 * get_controller_6() const { return ___controller_6; }
	inline VocabularyController_t843712768 ** get_address_of_controller_6() { return &___controller_6; }
	inline void set_controller_6(VocabularyController_t843712768 * value)
	{
		___controller_6 = value;
		Il2CppCodeGenWriteBarrier(&___controller_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
