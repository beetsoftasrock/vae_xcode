﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OtherThinkingView
struct  OtherThinkingView_t677091083  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image OtherThinkingView::thinkingImg
	Image_t2042527209 * ___thinkingImg_2;

public:
	inline static int32_t get_offset_of_thinkingImg_2() { return static_cast<int32_t>(offsetof(OtherThinkingView_t677091083, ___thinkingImg_2)); }
	inline Image_t2042527209 * get_thinkingImg_2() const { return ___thinkingImg_2; }
	inline Image_t2042527209 ** get_address_of_thinkingImg_2() { return &___thinkingImg_2; }
	inline void set_thinkingImg_2(Image_t2042527209 * value)
	{
		___thinkingImg_2 = value;
		Il2CppCodeGenWriteBarrier(&___thinkingImg_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
