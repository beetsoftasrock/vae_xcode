﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearningLogResponse
struct LearningLogResponse_t112900141;

#include "codegen/il2cpp-codegen.h"

// System.Void LearningLogResponse::.ctor()
extern "C"  void LearningLogResponse__ctor_m1064801606 (LearningLogResponse_t112900141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
