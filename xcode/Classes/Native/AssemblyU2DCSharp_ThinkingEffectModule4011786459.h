﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationLogicController
struct ConversationLogicController_t3911886281;
// UnityEngine.Sprite
struct Sprite_t309593783;
// OtherThinkingView
struct OtherThinkingView_t677091083;
// GlobalConfig
struct GlobalConfig_t3080413471;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThinkingEffectModule
struct  ThinkingEffectModule_t4011786459  : public MonoBehaviour_t1158329972
{
public:
	// ConversationLogicController ThinkingEffectModule::clc
	ConversationLogicController_t3911886281 * ___clc_2;
	// UnityEngine.Sprite ThinkingEffectModule::<preThinkingSprite>k__BackingField
	Sprite_t309593783 * ___U3CpreThinkingSpriteU3Ek__BackingField_3;
	// OtherThinkingView ThinkingEffectModule::thinkingView
	OtherThinkingView_t677091083 * ___thinkingView_4;

public:
	inline static int32_t get_offset_of_clc_2() { return static_cast<int32_t>(offsetof(ThinkingEffectModule_t4011786459, ___clc_2)); }
	inline ConversationLogicController_t3911886281 * get_clc_2() const { return ___clc_2; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_2() { return &___clc_2; }
	inline void set_clc_2(ConversationLogicController_t3911886281 * value)
	{
		___clc_2 = value;
		Il2CppCodeGenWriteBarrier(&___clc_2, value);
	}

	inline static int32_t get_offset_of_U3CpreThinkingSpriteU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ThinkingEffectModule_t4011786459, ___U3CpreThinkingSpriteU3Ek__BackingField_3)); }
	inline Sprite_t309593783 * get_U3CpreThinkingSpriteU3Ek__BackingField_3() const { return ___U3CpreThinkingSpriteU3Ek__BackingField_3; }
	inline Sprite_t309593783 ** get_address_of_U3CpreThinkingSpriteU3Ek__BackingField_3() { return &___U3CpreThinkingSpriteU3Ek__BackingField_3; }
	inline void set_U3CpreThinkingSpriteU3Ek__BackingField_3(Sprite_t309593783 * value)
	{
		___U3CpreThinkingSpriteU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpreThinkingSpriteU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_thinkingView_4() { return static_cast<int32_t>(offsetof(ThinkingEffectModule_t4011786459, ___thinkingView_4)); }
	inline OtherThinkingView_t677091083 * get_thinkingView_4() const { return ___thinkingView_4; }
	inline OtherThinkingView_t677091083 ** get_address_of_thinkingView_4() { return &___thinkingView_4; }
	inline void set_thinkingView_4(OtherThinkingView_t677091083 * value)
	{
		___thinkingView_4 = value;
		Il2CppCodeGenWriteBarrier(&___thinkingView_4, value);
	}
};

struct ThinkingEffectModule_t4011786459_StaticFields
{
public:
	// GlobalConfig ThinkingEffectModule::_globalConfig
	GlobalConfig_t3080413471 * ____globalConfig_5;

public:
	inline static int32_t get_offset_of__globalConfig_5() { return static_cast<int32_t>(offsetof(ThinkingEffectModule_t4011786459_StaticFields, ____globalConfig_5)); }
	inline GlobalConfig_t3080413471 * get__globalConfig_5() const { return ____globalConfig_5; }
	inline GlobalConfig_t3080413471 ** get_address_of__globalConfig_5() { return &____globalConfig_5; }
	inline void set__globalConfig_5(GlobalConfig_t3080413471 * value)
	{
		____globalConfig_5 = value;
		Il2CppCodeGenWriteBarrier(&____globalConfig_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
