﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableWithTimeSound
struct  DisableWithTimeSound_t417185280  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DisableWithTimeSound::autoDestroy
	bool ___autoDestroy_2;
	// UnityEngine.AudioSource DisableWithTimeSound::audioSource
	AudioSource_t1135106623 * ___audioSource_3;
	// System.Boolean DisableWithTimeSound::autoPlayAudio
	bool ___autoPlayAudio_4;
	// System.Single DisableWithTimeSound::timeDelayFinish
	float ___timeDelayFinish_5;
	// UnityEngine.Events.UnityEvent DisableWithTimeSound::OnFinish
	UnityEvent_t408735097 * ___OnFinish_6;

public:
	inline static int32_t get_offset_of_autoDestroy_2() { return static_cast<int32_t>(offsetof(DisableWithTimeSound_t417185280, ___autoDestroy_2)); }
	inline bool get_autoDestroy_2() const { return ___autoDestroy_2; }
	inline bool* get_address_of_autoDestroy_2() { return &___autoDestroy_2; }
	inline void set_autoDestroy_2(bool value)
	{
		___autoDestroy_2 = value;
	}

	inline static int32_t get_offset_of_audioSource_3() { return static_cast<int32_t>(offsetof(DisableWithTimeSound_t417185280, ___audioSource_3)); }
	inline AudioSource_t1135106623 * get_audioSource_3() const { return ___audioSource_3; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_3() { return &___audioSource_3; }
	inline void set_audioSource_3(AudioSource_t1135106623 * value)
	{
		___audioSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_3, value);
	}

	inline static int32_t get_offset_of_autoPlayAudio_4() { return static_cast<int32_t>(offsetof(DisableWithTimeSound_t417185280, ___autoPlayAudio_4)); }
	inline bool get_autoPlayAudio_4() const { return ___autoPlayAudio_4; }
	inline bool* get_address_of_autoPlayAudio_4() { return &___autoPlayAudio_4; }
	inline void set_autoPlayAudio_4(bool value)
	{
		___autoPlayAudio_4 = value;
	}

	inline static int32_t get_offset_of_timeDelayFinish_5() { return static_cast<int32_t>(offsetof(DisableWithTimeSound_t417185280, ___timeDelayFinish_5)); }
	inline float get_timeDelayFinish_5() const { return ___timeDelayFinish_5; }
	inline float* get_address_of_timeDelayFinish_5() { return &___timeDelayFinish_5; }
	inline void set_timeDelayFinish_5(float value)
	{
		___timeDelayFinish_5 = value;
	}

	inline static int32_t get_offset_of_OnFinish_6() { return static_cast<int32_t>(offsetof(DisableWithTimeSound_t417185280, ___OnFinish_6)); }
	inline UnityEvent_t408735097 * get_OnFinish_6() const { return ___OnFinish_6; }
	inline UnityEvent_t408735097 ** get_address_of_OnFinish_6() { return &___OnFinish_6; }
	inline void set_OnFinish_6(UnityEvent_t408735097 * value)
	{
		___OnFinish_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnFinish_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
