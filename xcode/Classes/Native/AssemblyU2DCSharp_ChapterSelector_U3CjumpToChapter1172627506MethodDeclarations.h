﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterSelector/<jumpToChapter>c__Iterator2
struct U3CjumpToChapterU3Ec__Iterator2_t1172627506;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterSelector/<jumpToChapter>c__Iterator2::.ctor()
extern "C"  void U3CjumpToChapterU3Ec__Iterator2__ctor_m4016421921 (U3CjumpToChapterU3Ec__Iterator2_t1172627506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterSelector/<jumpToChapter>c__Iterator2::MoveNext()
extern "C"  bool U3CjumpToChapterU3Ec__Iterator2_MoveNext_m2108250543 (U3CjumpToChapterU3Ec__Iterator2_t1172627506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterSelector/<jumpToChapter>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CjumpToChapterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1911978723 (U3CjumpToChapterU3Ec__Iterator2_t1172627506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterSelector/<jumpToChapter>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CjumpToChapterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m779240571 (U3CjumpToChapterU3Ec__Iterator2_t1172627506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<jumpToChapter>c__Iterator2::Dispose()
extern "C"  void U3CjumpToChapterU3Ec__Iterator2_Dispose_m3259154340 (U3CjumpToChapterU3Ec__Iterator2_t1172627506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<jumpToChapter>c__Iterator2::Reset()
extern "C"  void U3CjumpToChapterU3Ec__Iterator2_Reset_m2153937718 (U3CjumpToChapterU3Ec__Iterator2_t1172627506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
