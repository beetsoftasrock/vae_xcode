﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.beetsoft.GUIElements.SelectableItem/<ShowFrameAnimation>c__Iterator0
struct U3CShowFrameAnimationU3Ec__Iterator0_t3617874698;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void com.beetsoft.GUIElements.SelectableItem/<ShowFrameAnimation>c__Iterator0::.ctor()
extern "C"  void U3CShowFrameAnimationU3Ec__Iterator0__ctor_m2738078901 (U3CShowFrameAnimationU3Ec__Iterator0_t3617874698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean com.beetsoft.GUIElements.SelectableItem/<ShowFrameAnimation>c__Iterator0::MoveNext()
extern "C"  bool U3CShowFrameAnimationU3Ec__Iterator0_MoveNext_m4273148667 (U3CShowFrameAnimationU3Ec__Iterator0_t3617874698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object com.beetsoft.GUIElements.SelectableItem/<ShowFrameAnimation>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowFrameAnimationU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3744803059 (U3CShowFrameAnimationU3Ec__Iterator0_t3617874698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object com.beetsoft.GUIElements.SelectableItem/<ShowFrameAnimation>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowFrameAnimationU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3156981291 (U3CShowFrameAnimationU3Ec__Iterator0_t3617874698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem/<ShowFrameAnimation>c__Iterator0::Dispose()
extern "C"  void U3CShowFrameAnimationU3Ec__Iterator0_Dispose_m19789420 (U3CShowFrameAnimationU3Ec__Iterator0_t3617874698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem/<ShowFrameAnimation>c__Iterator0::Reset()
extern "C"  void U3CShowFrameAnimationU3Ec__Iterator0_Reset_m1903660806 (U3CShowFrameAnimationU3Ec__Iterator0_t3617874698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
