﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAP_UIScript
struct IAP_UIScript_t1776166942;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IAP_UIScript::.ctor()
extern "C"  void IAP_UIScript__ctor_m4186823851 (IAP_UIScript_t1776166942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript::Start()
extern "C"  void IAP_UIScript_Start_m218966463 (IAP_UIScript_t1776166942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IAP_UIScript::WaitFromValidateIAP()
extern "C"  Il2CppObject * IAP_UIScript_WaitFromValidateIAP_m4019437686 (IAP_UIScript_t1776166942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript::OnClickButtonBuyFull()
extern "C"  void IAP_UIScript_OnClickButtonBuyFull_m763756735 (IAP_UIScript_t1776166942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript::OnClickButtonRestore()
extern "C"  void IAP_UIScript_OnClickButtonRestore_m1522307998 (IAP_UIScript_t1776166942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript::OnOffIAPPopup()
extern "C"  void IAP_UIScript_OnOffIAPPopup_m3063974743 (IAP_UIScript_t1776166942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript::<OnClickButtonBuyFull>m__0()
extern "C"  void IAP_UIScript_U3COnClickButtonBuyFullU3Em__0_m1447344456 (IAP_UIScript_t1776166942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript::<OnClickButtonBuyFull>m__1(System.String)
extern "C"  void IAP_UIScript_U3COnClickButtonBuyFullU3Em__1_m1457657351 (IAP_UIScript_t1776166942 * __this, String_t* ___err0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_UIScript::<OnClickButtonRestore>m__2(System.Boolean)
extern "C"  void IAP_UIScript_U3COnClickButtonRestoreU3Em__2_m1197022650 (IAP_UIScript_t1776166942 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
