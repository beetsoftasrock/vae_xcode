﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening1QuestionView/<ShowResult>c__AnonStorey0
struct  U3CShowResultU3Ec__AnonStorey0_t2212642809  : public Il2CppObject
{
public:
	// System.Action Listening1QuestionView/<ShowResult>c__AnonStorey0::popupClosedCallback
	Action_t3226471752 * ___popupClosedCallback_0;

public:
	inline static int32_t get_offset_of_popupClosedCallback_0() { return static_cast<int32_t>(offsetof(U3CShowResultU3Ec__AnonStorey0_t2212642809, ___popupClosedCallback_0)); }
	inline Action_t3226471752 * get_popupClosedCallback_0() const { return ___popupClosedCallback_0; }
	inline Action_t3226471752 ** get_address_of_popupClosedCallback_0() { return &___popupClosedCallback_0; }
	inline void set_popupClosedCallback_0(Action_t3226471752 * value)
	{
		___popupClosedCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___popupClosedCallback_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
