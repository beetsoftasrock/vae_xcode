﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefineCallCustomEvent
struct DefineCallCustomEvent_t3427966816;

#include "codegen/il2cpp-codegen.h"

// System.Void DefineCallCustomEvent::.ctor()
extern "C"  void DefineCallCustomEvent__ctor_m4265525151 (DefineCallCustomEvent_t3427966816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefineCallCustomEvent::Start()
extern "C"  void DefineCallCustomEvent_Start_m3932374779 (DefineCallCustomEvent_t3427966816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
