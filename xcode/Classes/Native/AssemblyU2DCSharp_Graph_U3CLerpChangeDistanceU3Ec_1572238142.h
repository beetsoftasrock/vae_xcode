﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Graph
struct Graph_t2735713688;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Graph/<LerpChangeDistance>c__Iterator0
struct  U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142  : public Il2CppObject
{
public:
	// System.Int32 Graph/<LerpChangeDistance>c__Iterator0::index
	int32_t ___index_0;
	// System.Single Graph/<LerpChangeDistance>c__Iterator0::targetValue
	float ___targetValue_1;
	// Graph Graph/<LerpChangeDistance>c__Iterator0::$this
	Graph_t2735713688 * ___U24this_2;
	// System.Object Graph/<LerpChangeDistance>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean Graph/<LerpChangeDistance>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Graph/<LerpChangeDistance>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_targetValue_1() { return static_cast<int32_t>(offsetof(U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142, ___targetValue_1)); }
	inline float get_targetValue_1() const { return ___targetValue_1; }
	inline float* get_address_of_targetValue_1() { return &___targetValue_1; }
	inline void set_targetValue_1(float value)
	{
		___targetValue_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142, ___U24this_2)); }
	inline Graph_t2735713688 * get_U24this_2() const { return ___U24this_2; }
	inline Graph_t2735713688 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Graph_t2735713688 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
