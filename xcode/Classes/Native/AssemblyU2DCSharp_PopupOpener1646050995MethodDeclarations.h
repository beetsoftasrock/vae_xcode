﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupOpener
struct PopupOpener_t1646050995;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void PopupOpener::.ctor()
extern "C"  void PopupOpener__ctor_m238656900 (PopupOpener_t1646050995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PopupOpener::get_PopupClone()
extern "C"  GameObject_t1756533147 * PopupOpener_get_PopupClone_m2569576437 (PopupOpener_t1646050995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupOpener::set_PopupClone(UnityEngine.GameObject)
extern "C"  void PopupOpener_set_PopupClone_m1791616898 (PopupOpener_t1646050995 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupOpener::Start()
extern "C"  void PopupOpener_Start_m406928696 (PopupOpener_t1646050995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupOpener::Open()
extern "C"  void PopupOpener_Open_m3325157526 (PopupOpener_t1646050995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
