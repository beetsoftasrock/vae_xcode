﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneController/<LoadScene>c__Iterator2
struct U3CLoadSceneU3Ec__Iterator2_t1985522208;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneController/<LoadScene>c__Iterator2::.ctor()
extern "C"  void U3CLoadSceneU3Ec__Iterator2__ctor_m17833747 (U3CLoadSceneU3Ec__Iterator2_t1985522208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneController/<LoadScene>c__Iterator2::MoveNext()
extern "C"  bool U3CLoadSceneU3Ec__Iterator2_MoveNext_m1212666209 (U3CLoadSceneU3Ec__Iterator2_t1985522208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SceneController/<LoadScene>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadSceneU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1026710065 (U3CLoadSceneU3Ec__Iterator2_t1985522208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SceneController/<LoadScene>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadSceneU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2141663193 (U3CLoadSceneU3Ec__Iterator2_t1985522208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneController/<LoadScene>c__Iterator2::Dispose()
extern "C"  void U3CLoadSceneU3Ec__Iterator2_Dispose_m2650413010 (U3CLoadSceneU3Ec__Iterator2_t1985522208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneController/<LoadScene>c__Iterator2::Reset()
extern "C"  void U3CLoadSceneU3Ec__Iterator2_Reset_m568652800 (U3CLoadSceneU3Ec__Iterator2_t1985522208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
