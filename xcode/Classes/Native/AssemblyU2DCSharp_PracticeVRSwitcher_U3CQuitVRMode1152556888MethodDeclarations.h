﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PracticeVRSwitcher/<QuitVRMode>c__AnonStorey2
struct U3CQuitVRModeU3Ec__AnonStorey2_t1152556888;

#include "codegen/il2cpp-codegen.h"

// System.Void PracticeVRSwitcher/<QuitVRMode>c__AnonStorey2::.ctor()
extern "C"  void U3CQuitVRModeU3Ec__AnonStorey2__ctor_m2850833383 (U3CQuitVRModeU3Ec__AnonStorey2_t1152556888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher/<QuitVRMode>c__AnonStorey2::<>m__0()
extern "C"  void U3CQuitVRModeU3Ec__AnonStorey2_U3CU3Em__0_m670183534 (U3CQuitVRModeU3Ec__AnonStorey2_t1152556888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
