﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestReporter
struct TestReporter_t864384665;

#include "codegen/il2cpp-codegen.h"

// System.Void TestReporter::.ctor()
extern "C"  void TestReporter__ctor_m3665318216 (TestReporter_t864384665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::Start()
extern "C"  void TestReporter_Start_m3698023704 (TestReporter_t864384665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::OnDestroy()
extern "C"  void TestReporter_OnDestroy_m1257369823 (TestReporter_t864384665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::threadLogTest()
extern "C"  void TestReporter_threadLogTest_m2185803742 (TestReporter_t864384665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::Update()
extern "C"  void TestReporter_Update_m2069306465 (TestReporter_t864384665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestReporter::OnGUI()
extern "C"  void TestReporter_OnGUI_m1842271924 (TestReporter_t864384665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
