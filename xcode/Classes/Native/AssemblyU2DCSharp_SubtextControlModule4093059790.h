﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubtextControlModule
struct  SubtextControlModule_t4093059790  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SubtextControlModule::_isShowing
	bool ____isShowing_2;

public:
	inline static int32_t get_offset_of__isShowing_2() { return static_cast<int32_t>(offsetof(SubtextControlModule_t4093059790, ____isShowing_2)); }
	inline bool get__isShowing_2() const { return ____isShowing_2; }
	inline bool* get_address_of__isShowing_2() { return &____isShowing_2; }
	inline void set__isShowing_2(bool value)
	{
		____isShowing_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
