﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence1DatasWrapper
struct Sentence1DatasWrapper_t174893388;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// System.Collections.Generic.List`1<SentenceStructureIdiomQuestionData>
struct List_1_t2620853234;
// SentenceStructureIdiomQuestionData
struct SentenceStructureIdiomQuestionData_t3251732102;
// CommandSheet/Param
struct Param_t4123818474;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CommandSheet_Param4123818474.h"

// System.Void Sentence1DatasWrapper::.ctor(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void Sentence1DatasWrapper__ctor_m2901992805 (Sentence1DatasWrapper_t174893388 * __this, List_1_t3492939606 * ___listData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<SentenceStructureIdiomQuestionData> Sentence1DatasWrapper::GetConvertedDatas()
extern "C"  List_1_t2620853234 * Sentence1DatasWrapper_GetConvertedDatas_m436154973 (Sentence1DatasWrapper_t174893388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CommandSheet/Param> Sentence1DatasWrapper::get_wrappedDatas()
extern "C"  List_1_t3492939606 * Sentence1DatasWrapper_get_wrappedDatas_m579725673 (Sentence1DatasWrapper_t174893388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1DatasWrapper::set_wrappedDatas(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void Sentence1DatasWrapper_set_wrappedDatas_m3823513964 (Sentence1DatasWrapper_t174893388 * __this, List_1_t3492939606 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SentenceStructureIdiomQuestionData Sentence1DatasWrapper::ConvertData(CommandSheet/Param,System.Boolean)
extern "C"  SentenceStructureIdiomQuestionData_t3251732102 * Sentence1DatasWrapper_ConvertData_m2384079278 (Sentence1DatasWrapper_t174893388 * __this, Param_t4123818474 * ___value0, bool ___IsRandomQuestionOder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
