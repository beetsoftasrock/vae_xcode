﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataStorage
struct DataStorage_t2091384907;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DataStorage::.ctor()
extern "C"  void DataStorage__ctor_m2229491700 (DataStorage_t2091384907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataStorage DataStorage::Instance()
extern "C"  DataStorage_t2091384907 * DataStorage_Instance_m4027826193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::Awake()
extern "C"  void DataStorage_Awake_m3776132011 (DataStorage_t2091384907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::SetFloatForCurrentChapter(System.String,System.Single)
extern "C"  void DataStorage_SetFloatForCurrentChapter_m3696155308 (DataStorage_t2091384907 * __this, String_t* ___key0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::SetIntForCurrentChapter(System.String,System.Int32)
extern "C"  void DataStorage_SetIntForCurrentChapter_m1045209755 (DataStorage_t2091384907 * __this, String_t* ___key0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::SetStringForCurrentChapter(System.String,System.String)
extern "C"  void DataStorage_SetStringForCurrentChapter_m3712421762 (DataStorage_t2091384907 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DataStorage::GetFloatForCurrentChapter(System.String)
extern "C"  float DataStorage_GetFloatForCurrentChapter_m2013785751 (DataStorage_t2091384907 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DataStorage::GetFloatForCurrentChapter(System.String,System.Single)
extern "C"  float DataStorage_GetFloatForCurrentChapter_m2776823370 (DataStorage_t2091384907 * __this, String_t* ___key0, float ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataStorage::GetIntForCurrentChapter(System.String)
extern "C"  int32_t DataStorage_GetIntForCurrentChapter_m1656313906 (DataStorage_t2091384907 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataStorage::GetIntForCurrentChapter(System.String,System.Int32)
extern "C"  int32_t DataStorage_GetIntForCurrentChapter_m434197617 (DataStorage_t2091384907 * __this, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DataStorage::GetStringForCurrentChapter(System.String)
extern "C"  String_t* DataStorage_GetStringForCurrentChapter_m290607633 (DataStorage_t2091384907 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DataStorage::GetStringForCurrentChapter(System.String,System.String)
extern "C"  String_t* DataStorage_GetStringForCurrentChapter_m126440583 (DataStorage_t2091384907 * __this, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DataStorage::HasKeyForCurrentChapter(System.String)
extern "C"  bool DataStorage_HasKeyForCurrentChapter_m595313096 (DataStorage_t2091384907 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::DeleteKeyForCurrentChapter(System.String)
extern "C"  void DataStorage_DeleteKeyForCurrentChapter_m423156973 (DataStorage_t2091384907 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::Save()
extern "C"  void DataStorage_Save_m1602404071 (DataStorage_t2091384907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::DeleteAll()
extern "C"  void DataStorage_DeleteAll_m3587396914 (DataStorage_t2091384907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::SetFloatChapter(System.Int32,System.String,System.Single)
extern "C"  void DataStorage_SetFloatChapter_m83585523 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::SetIntChapter(System.Int32,System.String,System.Int32)
extern "C"  void DataStorage_SetIntChapter_m367084654 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::SetStringChapter(System.Int32,System.String,System.String)
extern "C"  void DataStorage_SetStringChapter_m604168829 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DataStorage::GetFloatChapter(System.Int32,System.String)
extern "C"  float DataStorage_GetFloatChapter_m2970701744 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DataStorage::GetFloatChapter(System.Int32,System.String,System.Single)
extern "C"  float DataStorage_GetFloatChapter_m3033105185 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, float ___defaultValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataStorage::GetIntChapter(System.Int32,System.String)
extern "C"  int32_t DataStorage_GetIntChapter_m1038401955 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataStorage::GetIntChapter(System.Int32,System.String,System.Int32)
extern "C"  int32_t DataStorage_GetIntChapter_m3506694628 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, int32_t ___defaultValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DataStorage::GetStringChapter(System.Int32,System.String)
extern "C"  String_t* DataStorage_GetStringChapter_m3034701634 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DataStorage::GetStringChapter(System.Int32,System.String,System.String)
extern "C"  String_t* DataStorage_GetStringChapter_m1544084442 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, String_t* ___defaultValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DataStorage::HasKeyChapter(System.Int32,System.String)
extern "C"  bool DataStorage_HasKeyChapter_m3914393285 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataStorage::DeleteKeyChapter(System.Int32,System.String)
extern "C"  void DataStorage_DeleteKeyChapter_m3372996252 (DataStorage_t2091384907 * __this, int32_t ___chapter0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
