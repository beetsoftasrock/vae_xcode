﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LearningResultData
struct  LearningResultData_t1558878615  : public Il2CppObject
{
public:
	// System.Int32 LearningResultData::time
	int32_t ___time_0;
	// System.Int32 LearningResultData::achievement
	int32_t ___achievement_1;
	// System.Int32 LearningResultData::positive
	int32_t ___positive_2;
	// System.Int32 LearningResultData::mistake
	int32_t ___mistake_3;
	// System.Int32 LearningResultData::fit
	int32_t ___fit_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(LearningResultData_t1558878615, ___time_0)); }
	inline int32_t get_time_0() const { return ___time_0; }
	inline int32_t* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(int32_t value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_achievement_1() { return static_cast<int32_t>(offsetof(LearningResultData_t1558878615, ___achievement_1)); }
	inline int32_t get_achievement_1() const { return ___achievement_1; }
	inline int32_t* get_address_of_achievement_1() { return &___achievement_1; }
	inline void set_achievement_1(int32_t value)
	{
		___achievement_1 = value;
	}

	inline static int32_t get_offset_of_positive_2() { return static_cast<int32_t>(offsetof(LearningResultData_t1558878615, ___positive_2)); }
	inline int32_t get_positive_2() const { return ___positive_2; }
	inline int32_t* get_address_of_positive_2() { return &___positive_2; }
	inline void set_positive_2(int32_t value)
	{
		___positive_2 = value;
	}

	inline static int32_t get_offset_of_mistake_3() { return static_cast<int32_t>(offsetof(LearningResultData_t1558878615, ___mistake_3)); }
	inline int32_t get_mistake_3() const { return ___mistake_3; }
	inline int32_t* get_address_of_mistake_3() { return &___mistake_3; }
	inline void set_mistake_3(int32_t value)
	{
		___mistake_3 = value;
	}

	inline static int32_t get_offset_of_fit_4() { return static_cast<int32_t>(offsetof(LearningResultData_t1558878615, ___fit_4)); }
	inline int32_t get_fit_4() const { return ___fit_4; }
	inline int32_t* get_address_of_fit_4() { return &___fit_4; }
	inline void set_fit_4(int32_t value)
	{
		___fit_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
