﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationVRSwitcher/<QuitVRMode>c__AnonStorey2
struct U3CQuitVRModeU3Ec__AnonStorey2_t289205990;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationVRSwitcher/<QuitVRMode>c__AnonStorey2::.ctor()
extern "C"  void U3CQuitVRModeU3Ec__AnonStorey2__ctor_m3961881521 (U3CQuitVRModeU3Ec__AnonStorey2_t289205990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher/<QuitVRMode>c__AnonStorey2::<>m__0()
extern "C"  void U3CQuitVRModeU3Ec__AnonStorey2_U3CU3Em__0_m2194315328 (U3CQuitVRModeU3Ec__AnonStorey2_t289205990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
