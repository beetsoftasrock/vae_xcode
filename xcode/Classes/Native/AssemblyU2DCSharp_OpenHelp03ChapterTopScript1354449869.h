﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_OpenHelpScript3207620564.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenHelp03ChapterTopScript
struct  OpenHelp03ChapterTopScript_t1354449869  : public OpenHelpScript_t3207620564
{
public:
	// UnityEngine.Transform OpenHelp03ChapterTopScript::selectUIRight
	Transform_t3275118058 * ___selectUIRight_10;

public:
	inline static int32_t get_offset_of_selectUIRight_10() { return static_cast<int32_t>(offsetof(OpenHelp03ChapterTopScript_t1354449869, ___selectUIRight_10)); }
	inline Transform_t3275118058 * get_selectUIRight_10() const { return ___selectUIRight_10; }
	inline Transform_t3275118058 ** get_address_of_selectUIRight_10() { return &___selectUIRight_10; }
	inline void set_selectUIRight_10(Transform_t3275118058 * value)
	{
		___selectUIRight_10 = value;
		Il2CppCodeGenWriteBarrier(&___selectUIRight_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
