﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// HistoryConversationDialog
struct HistoryConversationDialog_t3676407505;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HistoryConversationDialog/<LerpItem>c__Iterator0
struct  U3CLerpItemU3Ec__Iterator0_t925026203  : public Il2CppObject
{
public:
	// UnityEngine.Transform HistoryConversationDialog/<LerpItem>c__Iterator0::item
	Transform_t3275118058 * ___item_0;
	// UnityEngine.Transform HistoryConversationDialog/<LerpItem>c__Iterator0::startLerpTransform
	Transform_t3275118058 * ___startLerpTransform_1;
	// UnityEngine.RectTransform HistoryConversationDialog/<LerpItem>c__Iterator0::<itemRect>__0
	RectTransform_t3349966182 * ___U3CitemRectU3E__0_2;
	// UnityEngine.Vector2 HistoryConversationDialog/<LerpItem>c__Iterator0::<oriRect>__1
	Vector2_t2243707579  ___U3CoriRectU3E__1_3;
	// System.Single HistoryConversationDialog/<LerpItem>c__Iterator0::<distance>__2
	float ___U3CdistanceU3E__2_4;
	// System.Single HistoryConversationDialog/<LerpItem>c__Iterator0::<passedTime>__3
	float ___U3CpassedTimeU3E__3_5;
	// UnityEngine.GameObject HistoryConversationDialog/<LerpItem>c__Iterator0::<trickTarget>__4
	GameObject_t1756533147 * ___U3CtrickTargetU3E__4_6;
	// UnityEngine.UI.Image HistoryConversationDialog/<LerpItem>c__Iterator0::<trickImg>__5
	Image_t2042527209 * ___U3CtrickImgU3E__5_7;
	// System.Single HistoryConversationDialog/<LerpItem>c__Iterator0::<currentDistance>__6
	float ___U3CcurrentDistanceU3E__6_8;
	// HistoryConversationDialog HistoryConversationDialog/<LerpItem>c__Iterator0::$this
	HistoryConversationDialog_t3676407505 * ___U24this_9;
	// System.Object HistoryConversationDialog/<LerpItem>c__Iterator0::$current
	Il2CppObject * ___U24current_10;
	// System.Boolean HistoryConversationDialog/<LerpItem>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 HistoryConversationDialog/<LerpItem>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___item_0)); }
	inline Transform_t3275118058 * get_item_0() const { return ___item_0; }
	inline Transform_t3275118058 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Transform_t3275118058 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier(&___item_0, value);
	}

	inline static int32_t get_offset_of_startLerpTransform_1() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___startLerpTransform_1)); }
	inline Transform_t3275118058 * get_startLerpTransform_1() const { return ___startLerpTransform_1; }
	inline Transform_t3275118058 ** get_address_of_startLerpTransform_1() { return &___startLerpTransform_1; }
	inline void set_startLerpTransform_1(Transform_t3275118058 * value)
	{
		___startLerpTransform_1 = value;
		Il2CppCodeGenWriteBarrier(&___startLerpTransform_1, value);
	}

	inline static int32_t get_offset_of_U3CitemRectU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U3CitemRectU3E__0_2)); }
	inline RectTransform_t3349966182 * get_U3CitemRectU3E__0_2() const { return ___U3CitemRectU3E__0_2; }
	inline RectTransform_t3349966182 ** get_address_of_U3CitemRectU3E__0_2() { return &___U3CitemRectU3E__0_2; }
	inline void set_U3CitemRectU3E__0_2(RectTransform_t3349966182 * value)
	{
		___U3CitemRectU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemRectU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CoriRectU3E__1_3() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U3CoriRectU3E__1_3)); }
	inline Vector2_t2243707579  get_U3CoriRectU3E__1_3() const { return ___U3CoriRectU3E__1_3; }
	inline Vector2_t2243707579 * get_address_of_U3CoriRectU3E__1_3() { return &___U3CoriRectU3E__1_3; }
	inline void set_U3CoriRectU3E__1_3(Vector2_t2243707579  value)
	{
		___U3CoriRectU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3E__2_4() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U3CdistanceU3E__2_4)); }
	inline float get_U3CdistanceU3E__2_4() const { return ___U3CdistanceU3E__2_4; }
	inline float* get_address_of_U3CdistanceU3E__2_4() { return &___U3CdistanceU3E__2_4; }
	inline void set_U3CdistanceU3E__2_4(float value)
	{
		___U3CdistanceU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CpassedTimeU3E__3_5() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U3CpassedTimeU3E__3_5)); }
	inline float get_U3CpassedTimeU3E__3_5() const { return ___U3CpassedTimeU3E__3_5; }
	inline float* get_address_of_U3CpassedTimeU3E__3_5() { return &___U3CpassedTimeU3E__3_5; }
	inline void set_U3CpassedTimeU3E__3_5(float value)
	{
		___U3CpassedTimeU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CtrickTargetU3E__4_6() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U3CtrickTargetU3E__4_6)); }
	inline GameObject_t1756533147 * get_U3CtrickTargetU3E__4_6() const { return ___U3CtrickTargetU3E__4_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CtrickTargetU3E__4_6() { return &___U3CtrickTargetU3E__4_6; }
	inline void set_U3CtrickTargetU3E__4_6(GameObject_t1756533147 * value)
	{
		___U3CtrickTargetU3E__4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtrickTargetU3E__4_6, value);
	}

	inline static int32_t get_offset_of_U3CtrickImgU3E__5_7() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U3CtrickImgU3E__5_7)); }
	inline Image_t2042527209 * get_U3CtrickImgU3E__5_7() const { return ___U3CtrickImgU3E__5_7; }
	inline Image_t2042527209 ** get_address_of_U3CtrickImgU3E__5_7() { return &___U3CtrickImgU3E__5_7; }
	inline void set_U3CtrickImgU3E__5_7(Image_t2042527209 * value)
	{
		___U3CtrickImgU3E__5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtrickImgU3E__5_7, value);
	}

	inline static int32_t get_offset_of_U3CcurrentDistanceU3E__6_8() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U3CcurrentDistanceU3E__6_8)); }
	inline float get_U3CcurrentDistanceU3E__6_8() const { return ___U3CcurrentDistanceU3E__6_8; }
	inline float* get_address_of_U3CcurrentDistanceU3E__6_8() { return &___U3CcurrentDistanceU3E__6_8; }
	inline void set_U3CcurrentDistanceU3E__6_8(float value)
	{
		___U3CcurrentDistanceU3E__6_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U24this_9)); }
	inline HistoryConversationDialog_t3676407505 * get_U24this_9() const { return ___U24this_9; }
	inline HistoryConversationDialog_t3676407505 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(HistoryConversationDialog_t3676407505 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_9, value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CLerpItemU3Ec__Iterator0_t925026203, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
