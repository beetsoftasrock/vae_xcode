﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Graph
struct Graph_t2735713688;
// Column
struct Column_t1930583302;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Column1930583302.h"

// System.Void Graph::.ctor()
extern "C"  void Graph__ctor_m273083539 (Graph_t2735713688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Graph::Awake()
extern "C"  void Graph_Awake_m1413724756 (Graph_t2735713688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Graph::ResortVertextName(Column,Column,System.Boolean)
extern "C"  void Graph_ResortVertextName_m2253446980 (Graph_t2735713688 * __this, Column_t1930583302 * ___left0, Column_t1930583302 * ___right1, bool ___isGraphDisplayNewValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Graph::LerpChangeDistance(System.Int32,System.Single)
extern "C"  Il2CppObject * Graph_LerpChangeDistance_m2821583031 (Graph_t2735713688 * __this, int32_t ___index0, float ___targetValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
