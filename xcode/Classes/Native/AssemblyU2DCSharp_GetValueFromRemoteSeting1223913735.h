﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GetValueFromRemoteSeting
struct GetValueFromRemoteSeting_t1223913735;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetValueFromRemoteSeting
struct  GetValueFromRemoteSeting_t1223913735  : public Il2CppObject
{
public:
	// System.String GetValueFromRemoteSeting::suffixesID
	String_t* ___suffixesID_0;
	// System.String GetValueFromRemoteSeting::prefixID
	String_t* ___prefixID_1;
	// System.String GetValueFromRemoteSeting::<ProductID>k__BackingField
	String_t* ___U3CProductIDU3Ek__BackingField_3;
	// System.String GetValueFromRemoteSeting::<Version>k__BackingField
	String_t* ___U3CVersionU3Ek__BackingField_4;
	// System.String GetValueFromRemoteSeting::<PriceProduct>k__BackingField
	String_t* ___U3CPriceProductU3Ek__BackingField_5;
	// System.String GetValueFromRemoteSeting::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_suffixesID_0() { return static_cast<int32_t>(offsetof(GetValueFromRemoteSeting_t1223913735, ___suffixesID_0)); }
	inline String_t* get_suffixesID_0() const { return ___suffixesID_0; }
	inline String_t** get_address_of_suffixesID_0() { return &___suffixesID_0; }
	inline void set_suffixesID_0(String_t* value)
	{
		___suffixesID_0 = value;
		Il2CppCodeGenWriteBarrier(&___suffixesID_0, value);
	}

	inline static int32_t get_offset_of_prefixID_1() { return static_cast<int32_t>(offsetof(GetValueFromRemoteSeting_t1223913735, ___prefixID_1)); }
	inline String_t* get_prefixID_1() const { return ___prefixID_1; }
	inline String_t** get_address_of_prefixID_1() { return &___prefixID_1; }
	inline void set_prefixID_1(String_t* value)
	{
		___prefixID_1 = value;
		Il2CppCodeGenWriteBarrier(&___prefixID_1, value);
	}

	inline static int32_t get_offset_of_U3CProductIDU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetValueFromRemoteSeting_t1223913735, ___U3CProductIDU3Ek__BackingField_3)); }
	inline String_t* get_U3CProductIDU3Ek__BackingField_3() const { return ___U3CProductIDU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CProductIDU3Ek__BackingField_3() { return &___U3CProductIDU3Ek__BackingField_3; }
	inline void set_U3CProductIDU3Ek__BackingField_3(String_t* value)
	{
		___U3CProductIDU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProductIDU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetValueFromRemoteSeting_t1223913735, ___U3CVersionU3Ek__BackingField_4)); }
	inline String_t* get_U3CVersionU3Ek__BackingField_4() const { return ___U3CVersionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CVersionU3Ek__BackingField_4() { return &___U3CVersionU3Ek__BackingField_4; }
	inline void set_U3CVersionU3Ek__BackingField_4(String_t* value)
	{
		___U3CVersionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVersionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CPriceProductU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetValueFromRemoteSeting_t1223913735, ___U3CPriceProductU3Ek__BackingField_5)); }
	inline String_t* get_U3CPriceProductU3Ek__BackingField_5() const { return ___U3CPriceProductU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CPriceProductU3Ek__BackingField_5() { return &___U3CPriceProductU3Ek__BackingField_5; }
	inline void set_U3CPriceProductU3Ek__BackingField_5(String_t* value)
	{
		___U3CPriceProductU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPriceProductU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetValueFromRemoteSeting_t1223913735, ___U3CUrlU3Ek__BackingField_6)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_6() const { return ___U3CUrlU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_6() { return &___U3CUrlU3Ek__BackingField_6; }
	inline void set_U3CUrlU3Ek__BackingField_6(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUrlU3Ek__BackingField_6, value);
	}
};

struct GetValueFromRemoteSeting_t1223913735_StaticFields
{
public:
	// GetValueFromRemoteSeting GetValueFromRemoteSeting::<Instance>k__BackingField
	GetValueFromRemoteSeting_t1223913735 * ___U3CInstanceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetValueFromRemoteSeting_t1223913735_StaticFields, ___U3CInstanceU3Ek__BackingField_2)); }
	inline GetValueFromRemoteSeting_t1223913735 * get_U3CInstanceU3Ek__BackingField_2() const { return ___U3CInstanceU3Ek__BackingField_2; }
	inline GetValueFromRemoteSeting_t1223913735 ** get_address_of_U3CInstanceU3Ek__BackingField_2() { return &___U3CInstanceU3Ek__BackingField_2; }
	inline void set_U3CInstanceU3Ek__BackingField_2(GetValueFromRemoteSeting_t1223913735 * value)
	{
		___U3CInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
