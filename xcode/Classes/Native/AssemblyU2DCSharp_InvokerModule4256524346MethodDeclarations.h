﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InvokerModule
struct InvokerModule_t4256524346;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationCommand
struct ConversationCommand_t3660105836;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"

// System.Void InvokerModule::.ctor()
extern "C"  void InvokerModule__ctor_m48589475 (InvokerModule_t4256524346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule::OnConversationReset()
extern "C"  void InvokerModule_OnConversationReset_m2807532864 (InvokerModule_t4256524346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule::OnConversationReplay()
extern "C"  void InvokerModule_OnConversationReplay_m2207195588 (InvokerModule_t4256524346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InvokerModule::GetReady()
extern "C"  bool InvokerModule_GetReady_m3377421956 (InvokerModule_t4256524346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InvokerModule::RunToNextPlayerCommand()
extern "C"  Il2CppObject * InvokerModule_RunToNextPlayerCommand_m984331020 (InvokerModule_t4256524346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationCommand InvokerModule::GetNextCommand()
extern "C"  ConversationCommand_t3660105836 * InvokerModule_GetNextCommand_m3136860600 (InvokerModule_t4256524346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InvokerModule::DoReplayAllCommand()
extern "C"  Il2CppObject * InvokerModule_DoReplayAllCommand_m1450806503 (InvokerModule_t4256524346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InvokerModule::DoAutoPlay(System.Action)
extern "C"  Il2CppObject * InvokerModule_DoAutoPlay_m2299263220 (InvokerModule_t4256524346 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule::RevertToCommand(ConversationCommand)
extern "C"  void InvokerModule_RevertToCommand_m76226743 (InvokerModule_t4256524346 * __this, ConversationCommand_t3660105836 * ___command0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
