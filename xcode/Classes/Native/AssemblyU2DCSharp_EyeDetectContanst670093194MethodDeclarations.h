﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EyeDetectContanst
struct EyeDetectContanst_t670093194;

#include "codegen/il2cpp-codegen.h"

// System.Void EyeDetectContanst::.ctor()
extern "C"  void EyeDetectContanst__ctor_m1052491883 (EyeDetectContanst_t670093194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
