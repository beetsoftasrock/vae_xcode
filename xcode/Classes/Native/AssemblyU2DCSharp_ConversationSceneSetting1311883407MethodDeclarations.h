﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSceneSetting
struct ConversationSceneSetting_t1311883407;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationSceneSetting::.ctor()
extern "C"  void ConversationSceneSetting__ctor_m2113849850 (ConversationSceneSetting_t1311883407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
