﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.Purchaser
struct Purchaser_t1674559779;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"

// System.Void Beetsoft.VAE.Purchaser::.ctor()
extern "C"  void Purchaser__ctor_m257265830 (Purchaser_t1674559779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::Initialize()
extern "C"  void Purchaser_Initialize_m384430006 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Beetsoft.VAE.Purchaser::get_ValidatedProducts()
extern "C"  List_1_t1398341365 * Purchaser_get_ValidatedProducts_m1564622146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.Purchaser::get_DoneValidated()
extern "C"  bool Purchaser_get_DoneValidated_m3375308303 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::set_DoneValidated(System.Boolean)
extern "C"  void Purchaser_set_DoneValidated_m3169023030 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.Purchaser::get_IsInitialized()
extern "C"  bool Purchaser_get_IsInitialized_m4129449187 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.Purchaser::Validate(System.String)
extern "C"  bool Purchaser_Validate_m248936054 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::Purchase(System.String,System.Action,System.Action`1<System.String>)
extern "C"  void Purchaser_Purchase_m3338697486 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, Action_t3226471752 * ___onSuccess1, Action_1_t1831019615 * ___onFailure2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::Restore(System.Action`1<System.Boolean>)
extern "C"  void Purchaser_Restore_m1971347461 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___onResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Product Beetsoft.VAE.Purchaser::Product(System.String)
extern "C"  Product_t1203687971 * Purchaser_Product_m644550348 (Purchaser_t1674559779 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Beetsoft.VAE.Purchaser::Start()
extern "C"  Il2CppObject * Purchaser_Start_m528049122 (Purchaser_t1674559779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::validAppleReceipt(System.String)
extern "C"  void Purchaser_validAppleReceipt_m2097693874 (Purchaser_t1674559779 * __this, String_t* ___appleReceipt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::BuyProductID(System.String,System.Action,System.Action`1<System.String>)
extern "C"  void Purchaser_BuyProductID_m3637745155 (Purchaser_t1674559779 * __this, String_t* ___productId0, Action_t3226471752 * ___onSuccess1, Action_1_t1831019615 * ___onFailure2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::RestorePurchases(System.Action`1<System.Boolean>)
extern "C"  void Purchaser_RestorePurchases_m475986393 (Purchaser_t1674559779 * __this, Action_1_t3627374100 * ___onResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern "C"  void Purchaser_OnInitialized_m1872435528 (Purchaser_t1674559779 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern "C"  void Purchaser_OnInitializeFailed_m1588648433 (Purchaser_t1674559779 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.PurchaseProcessingResult Beetsoft.VAE.Purchaser::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern "C"  int32_t Purchaser_ProcessPurchase_m1938435766 (Purchaser_t1674559779 * __this, PurchaseEventArgs_t547992434 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C"  void Purchaser_OnPurchaseFailed_m1935332597 (Purchaser_t1674559779 * __this, Product_t1203687971 * ___product0, int32_t ___failureReason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::.cctor()
extern "C"  void Purchaser__cctor_m4213394131 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::<OnInitialized>m__0(System.String)
extern "C"  void Purchaser_U3COnInitializedU3Em__0_m2709147726 (Purchaser_t1674559779 * __this, String_t* ___appReceipt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser::<OnInitialized>m__1()
extern "C"  void Purchaser_U3COnInitializedU3Em__1_m3148334367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
