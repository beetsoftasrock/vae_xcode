﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// HistoryConversationDialog
struct HistoryConversationDialog_t3676407505;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2
struct  U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098  : public Il2CppObject
{
public:
	// System.Single HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::<speedLerp>__0
	float ___U3CspeedLerpU3E__0_0;
	// UnityEngine.RectTransform HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::<rectTransform>__1
	RectTransform_t3349966182 * ___U3CrectTransformU3E__1_1;
	// UnityEngine.Vector2 HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::<targetAnchoredPosition>__2
	Vector2_t2243707579  ___U3CtargetAnchoredPositionU3E__2_2;
	// UnityEngine.Vector2 HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::<startAnchoredPosition>__3
	Vector2_t2243707579  ___U3CstartAnchoredPositionU3E__3_3;
	// System.Single HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::<timeCalculate>__4
	float ___U3CtimeCalculateU3E__4_4;
	// System.Single HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::<passedTime>__5
	float ___U3CpassedTimeU3E__5_5;
	// HistoryConversationDialog HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::$this
	HistoryConversationDialog_t3676407505 * ___U24this_6;
	// System.Object HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::$disposing
	bool ___U24disposing_8;
	// System.Int32 HistoryConversationDialog/<MoveScrollViewToBottom>c__Iterator2::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CspeedLerpU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U3CspeedLerpU3E__0_0)); }
	inline float get_U3CspeedLerpU3E__0_0() const { return ___U3CspeedLerpU3E__0_0; }
	inline float* get_address_of_U3CspeedLerpU3E__0_0() { return &___U3CspeedLerpU3E__0_0; }
	inline void set_U3CspeedLerpU3E__0_0(float value)
	{
		___U3CspeedLerpU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CrectTransformU3E__1_1() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U3CrectTransformU3E__1_1)); }
	inline RectTransform_t3349966182 * get_U3CrectTransformU3E__1_1() const { return ___U3CrectTransformU3E__1_1; }
	inline RectTransform_t3349966182 ** get_address_of_U3CrectTransformU3E__1_1() { return &___U3CrectTransformU3E__1_1; }
	inline void set_U3CrectTransformU3E__1_1(RectTransform_t3349966182 * value)
	{
		___U3CrectTransformU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrectTransformU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CtargetAnchoredPositionU3E__2_2() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U3CtargetAnchoredPositionU3E__2_2)); }
	inline Vector2_t2243707579  get_U3CtargetAnchoredPositionU3E__2_2() const { return ___U3CtargetAnchoredPositionU3E__2_2; }
	inline Vector2_t2243707579 * get_address_of_U3CtargetAnchoredPositionU3E__2_2() { return &___U3CtargetAnchoredPositionU3E__2_2; }
	inline void set_U3CtargetAnchoredPositionU3E__2_2(Vector2_t2243707579  value)
	{
		___U3CtargetAnchoredPositionU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CstartAnchoredPositionU3E__3_3() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U3CstartAnchoredPositionU3E__3_3)); }
	inline Vector2_t2243707579  get_U3CstartAnchoredPositionU3E__3_3() const { return ___U3CstartAnchoredPositionU3E__3_3; }
	inline Vector2_t2243707579 * get_address_of_U3CstartAnchoredPositionU3E__3_3() { return &___U3CstartAnchoredPositionU3E__3_3; }
	inline void set_U3CstartAnchoredPositionU3E__3_3(Vector2_t2243707579  value)
	{
		___U3CstartAnchoredPositionU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CtimeCalculateU3E__4_4() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U3CtimeCalculateU3E__4_4)); }
	inline float get_U3CtimeCalculateU3E__4_4() const { return ___U3CtimeCalculateU3E__4_4; }
	inline float* get_address_of_U3CtimeCalculateU3E__4_4() { return &___U3CtimeCalculateU3E__4_4; }
	inline void set_U3CtimeCalculateU3E__4_4(float value)
	{
		___U3CtimeCalculateU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CpassedTimeU3E__5_5() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U3CpassedTimeU3E__5_5)); }
	inline float get_U3CpassedTimeU3E__5_5() const { return ___U3CpassedTimeU3E__5_5; }
	inline float* get_address_of_U3CpassedTimeU3E__5_5() { return &___U3CpassedTimeU3E__5_5; }
	inline void set_U3CpassedTimeU3E__5_5(float value)
	{
		___U3CpassedTimeU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U24this_6)); }
	inline HistoryConversationDialog_t3676407505 * get_U24this_6() const { return ___U24this_6; }
	inline HistoryConversationDialog_t3676407505 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(HistoryConversationDialog_t3676407505 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CMoveScrollViewToBottomU3Ec__Iterator2_t3283600098, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
