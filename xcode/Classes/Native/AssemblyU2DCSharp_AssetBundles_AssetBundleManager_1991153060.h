﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1
struct U3CloadAssetBundlesU3Ec__Iterator1_t2422683914;
// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8
struct U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey7
struct  U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060  : public Il2CppObject
{
public:
	// System.String AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey7::namePrefix
	String_t* ___namePrefix_0;
	// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1 AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey7::<>f__ref$1
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * ___U3CU3Ef__refU241_1;
	// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8 AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey7::<>f__ref$8
	U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 * ___U3CU3Ef__refU248_2;

public:
	inline static int32_t get_offset_of_namePrefix_0() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060, ___namePrefix_0)); }
	inline String_t* get_namePrefix_0() const { return ___namePrefix_0; }
	inline String_t** get_address_of_namePrefix_0() { return &___namePrefix_0; }
	inline void set_namePrefix_0(String_t* value)
	{
		___namePrefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___namePrefix_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060, ___U3CU3Ef__refU241_1)); }
	inline U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU248_2() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060, ___U3CU3Ef__refU248_2)); }
	inline U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 * get_U3CU3Ef__refU248_2() const { return ___U3CU3Ef__refU248_2; }
	inline U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 ** get_address_of_U3CU3Ef__refU248_2() { return &___U3CU3Ef__refU248_2; }
	inline void set_U3CU3Ef__refU248_2(U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 * value)
	{
		___U3CU3Ef__refU248_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU248_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
