﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseQuestionView
struct BaseQuestionView_t79922856;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseQuestionView::.ctor()
extern "C"  void BaseQuestionView__ctor_m2678012341 (BaseQuestionView_t79922856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseQuestionView::ShowProgress(System.Int32,System.Int32)
extern "C"  void BaseQuestionView_ShowProgress_m1425628227 (BaseQuestionView_t79922856 * __this, int32_t ___leftProgress0, int32_t ___totalProgress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
