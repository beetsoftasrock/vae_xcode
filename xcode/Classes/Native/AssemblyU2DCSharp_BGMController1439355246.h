﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BGMController
struct BGMController_t1439355246;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BGMController
struct  BGMController_t1439355246  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource BGMController::audioSource
	AudioSource_t1135106623 * ___audioSource_3;
	// System.Boolean BGMController::_playBGM
	bool ____playBGM_4;

public:
	inline static int32_t get_offset_of_audioSource_3() { return static_cast<int32_t>(offsetof(BGMController_t1439355246, ___audioSource_3)); }
	inline AudioSource_t1135106623 * get_audioSource_3() const { return ___audioSource_3; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_3() { return &___audioSource_3; }
	inline void set_audioSource_3(AudioSource_t1135106623 * value)
	{
		___audioSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_3, value);
	}

	inline static int32_t get_offset_of__playBGM_4() { return static_cast<int32_t>(offsetof(BGMController_t1439355246, ____playBGM_4)); }
	inline bool get__playBGM_4() const { return ____playBGM_4; }
	inline bool* get_address_of__playBGM_4() { return &____playBGM_4; }
	inline void set__playBGM_4(bool value)
	{
		____playBGM_4 = value;
	}
};

struct BGMController_t1439355246_StaticFields
{
public:
	// BGMController BGMController::_instance
	BGMController_t1439355246 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(BGMController_t1439355246_StaticFields, ____instance_2)); }
	inline BGMController_t1439355246 * get__instance_2() const { return ____instance_2; }
	inline BGMController_t1439355246 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(BGMController_t1439355246 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
