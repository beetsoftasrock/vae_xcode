﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircleScript
struct  CircleScript_t1225364279  : public MonoBehaviour_t1158329972
{
public:
	// System.Single CircleScript::radius
	float ___radius_2;
	// System.Single CircleScript::offsetStartAngle
	float ___offsetStartAngle_3;
	// System.Single CircleScript::offsetRadius
	float ___offsetRadius_4;
	// System.Single CircleScript::offsetFillAmountHead
	float ___offsetFillAmountHead_5;
	// System.Single CircleScript::heightHeadTail
	float ___heightHeadTail_6;
	// System.Single CircleScript::withHeadTail
	float ___withHeadTail_7;
	// System.Single CircleScript::speed
	float ___speed_8;
	// UnityEngine.UI.Text CircleScript::txtValue
	Text_t356221433 * ___txtValue_9;
	// UnityEngine.GameObject CircleScript::head
	GameObject_t1756533147 * ___head_10;
	// UnityEngine.GameObject CircleScript::tail
	GameObject_t1756533147 * ___tail_11;
	// UnityEngine.GameObject CircleScript::center
	GameObject_t1756533147 * ___center_12;
	// UnityEngine.UI.Image CircleScript::imgCicle
	Image_t2042527209 * ___imgCicle_13;
	// System.Single CircleScript::oriFillAmountHead
	float ___oriFillAmountHead_14;
	// System.Single CircleScript::AcrDegreeHead
	float ___AcrDegreeHead_15;
	// System.Boolean CircleScript::isSetting
	bool ___isSetting_16;
	// System.Single CircleScript::<Value>k__BackingField
	float ___U3CValueU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}

	inline static int32_t get_offset_of_offsetStartAngle_3() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___offsetStartAngle_3)); }
	inline float get_offsetStartAngle_3() const { return ___offsetStartAngle_3; }
	inline float* get_address_of_offsetStartAngle_3() { return &___offsetStartAngle_3; }
	inline void set_offsetStartAngle_3(float value)
	{
		___offsetStartAngle_3 = value;
	}

	inline static int32_t get_offset_of_offsetRadius_4() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___offsetRadius_4)); }
	inline float get_offsetRadius_4() const { return ___offsetRadius_4; }
	inline float* get_address_of_offsetRadius_4() { return &___offsetRadius_4; }
	inline void set_offsetRadius_4(float value)
	{
		___offsetRadius_4 = value;
	}

	inline static int32_t get_offset_of_offsetFillAmountHead_5() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___offsetFillAmountHead_5)); }
	inline float get_offsetFillAmountHead_5() const { return ___offsetFillAmountHead_5; }
	inline float* get_address_of_offsetFillAmountHead_5() { return &___offsetFillAmountHead_5; }
	inline void set_offsetFillAmountHead_5(float value)
	{
		___offsetFillAmountHead_5 = value;
	}

	inline static int32_t get_offset_of_heightHeadTail_6() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___heightHeadTail_6)); }
	inline float get_heightHeadTail_6() const { return ___heightHeadTail_6; }
	inline float* get_address_of_heightHeadTail_6() { return &___heightHeadTail_6; }
	inline void set_heightHeadTail_6(float value)
	{
		___heightHeadTail_6 = value;
	}

	inline static int32_t get_offset_of_withHeadTail_7() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___withHeadTail_7)); }
	inline float get_withHeadTail_7() const { return ___withHeadTail_7; }
	inline float* get_address_of_withHeadTail_7() { return &___withHeadTail_7; }
	inline void set_withHeadTail_7(float value)
	{
		___withHeadTail_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_txtValue_9() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___txtValue_9)); }
	inline Text_t356221433 * get_txtValue_9() const { return ___txtValue_9; }
	inline Text_t356221433 ** get_address_of_txtValue_9() { return &___txtValue_9; }
	inline void set_txtValue_9(Text_t356221433 * value)
	{
		___txtValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___txtValue_9, value);
	}

	inline static int32_t get_offset_of_head_10() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___head_10)); }
	inline GameObject_t1756533147 * get_head_10() const { return ___head_10; }
	inline GameObject_t1756533147 ** get_address_of_head_10() { return &___head_10; }
	inline void set_head_10(GameObject_t1756533147 * value)
	{
		___head_10 = value;
		Il2CppCodeGenWriteBarrier(&___head_10, value);
	}

	inline static int32_t get_offset_of_tail_11() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___tail_11)); }
	inline GameObject_t1756533147 * get_tail_11() const { return ___tail_11; }
	inline GameObject_t1756533147 ** get_address_of_tail_11() { return &___tail_11; }
	inline void set_tail_11(GameObject_t1756533147 * value)
	{
		___tail_11 = value;
		Il2CppCodeGenWriteBarrier(&___tail_11, value);
	}

	inline static int32_t get_offset_of_center_12() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___center_12)); }
	inline GameObject_t1756533147 * get_center_12() const { return ___center_12; }
	inline GameObject_t1756533147 ** get_address_of_center_12() { return &___center_12; }
	inline void set_center_12(GameObject_t1756533147 * value)
	{
		___center_12 = value;
		Il2CppCodeGenWriteBarrier(&___center_12, value);
	}

	inline static int32_t get_offset_of_imgCicle_13() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___imgCicle_13)); }
	inline Image_t2042527209 * get_imgCicle_13() const { return ___imgCicle_13; }
	inline Image_t2042527209 ** get_address_of_imgCicle_13() { return &___imgCicle_13; }
	inline void set_imgCicle_13(Image_t2042527209 * value)
	{
		___imgCicle_13 = value;
		Il2CppCodeGenWriteBarrier(&___imgCicle_13, value);
	}

	inline static int32_t get_offset_of_oriFillAmountHead_14() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___oriFillAmountHead_14)); }
	inline float get_oriFillAmountHead_14() const { return ___oriFillAmountHead_14; }
	inline float* get_address_of_oriFillAmountHead_14() { return &___oriFillAmountHead_14; }
	inline void set_oriFillAmountHead_14(float value)
	{
		___oriFillAmountHead_14 = value;
	}

	inline static int32_t get_offset_of_AcrDegreeHead_15() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___AcrDegreeHead_15)); }
	inline float get_AcrDegreeHead_15() const { return ___AcrDegreeHead_15; }
	inline float* get_address_of_AcrDegreeHead_15() { return &___AcrDegreeHead_15; }
	inline void set_AcrDegreeHead_15(float value)
	{
		___AcrDegreeHead_15 = value;
	}

	inline static int32_t get_offset_of_isSetting_16() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___isSetting_16)); }
	inline bool get_isSetting_16() const { return ___isSetting_16; }
	inline bool* get_address_of_isSetting_16() { return &___isSetting_16; }
	inline void set_isSetting_16(bool value)
	{
		___isSetting_16 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(CircleScript_t1225364279, ___U3CValueU3Ek__BackingField_17)); }
	inline float get_U3CValueU3Ek__BackingField_17() const { return ___U3CValueU3Ek__BackingField_17; }
	inline float* get_address_of_U3CValueU3Ek__BackingField_17() { return &___U3CValueU3Ek__BackingField_17; }
	inline void set_U3CValueU3Ek__BackingField_17(float value)
	{
		___U3CValueU3Ek__BackingField_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
