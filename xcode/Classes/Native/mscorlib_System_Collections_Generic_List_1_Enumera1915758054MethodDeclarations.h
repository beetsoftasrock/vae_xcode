﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingQuestionData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1252350935(__this, ___l0, method) ((  void (*) (Enumerator_t1915758054 *, List_1_t2381028380 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingQuestionData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1541160851(__this, method) ((  void (*) (Enumerator_t1915758054 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingQuestionData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2697691935(__this, method) ((  Il2CppObject * (*) (Enumerator_t1915758054 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingQuestionData>::Dispose()
#define Enumerator_Dispose_m635092452(__this, method) ((  void (*) (Enumerator_t1915758054 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingQuestionData>::VerifyState()
#define Enumerator_VerifyState_m1753988905(__this, method) ((  void (*) (Enumerator_t1915758054 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingQuestionData>::MoveNext()
#define Enumerator_MoveNext_m3976573372(__this, method) ((  bool (*) (Enumerator_t1915758054 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingQuestionData>::get_Current()
#define Enumerator_get_Current_m3192695830(__this, method) ((  SentenceListeningSpellingQuestionData_t3011907248 * (*) (Enumerator_t1915758054 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
