﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopHelpCaching
struct ChapterTopHelpCaching_t800566100;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterTopHelpCaching::.ctor()
extern "C"  void ChapterTopHelpCaching__ctor_m671786301 (ChapterTopHelpCaching_t800566100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopHelpCaching::CachingBeforeOpenHelp()
extern "C"  void ChapterTopHelpCaching_CachingBeforeOpenHelp_m4147231042 (ChapterTopHelpCaching_t800566100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopHelpCaching::RestoreAfterCloseHelp()
extern "C"  void ChapterTopHelpCaching_RestoreAfterCloseHelp_m716734744 (ChapterTopHelpCaching_t800566100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
