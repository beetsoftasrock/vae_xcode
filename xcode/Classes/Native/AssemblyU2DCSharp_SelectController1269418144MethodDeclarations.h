﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectController
struct SelectController_t1269418144;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SelectController_SelectedState323245347.h"

// System.Void SelectController::.ctor()
extern "C"  void SelectController__ctor_m1080476415 (SelectController_t1269418144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SelectController/SelectedState SelectController::get_currentSelected()
extern "C"  int32_t SelectController_get_currentSelected_m1472086944 (SelectController_t1269418144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectController::set_currentSelected(SelectController/SelectedState)
extern "C"  void SelectController_set_currentSelected_m1324055139 (SelectController_t1269418144 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectController::OnSelectLeft()
extern "C"  void SelectController_OnSelectLeft_m4161037113 (SelectController_t1269418144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectController::OnSelectRight()
extern "C"  void SelectController_OnSelectRight_m3800179182 (SelectController_t1269418144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectController::SetSelectedState(SelectController/SelectedState)
extern "C"  void SelectController_SetSelectedState_m3651327502 (SelectController_t1269418144 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectController::Start()
extern "C"  void SelectController_Start_m1361793803 (SelectController_t1269418144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
