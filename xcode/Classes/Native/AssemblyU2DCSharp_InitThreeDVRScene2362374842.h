﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Asset
struct Asset_t2923928748;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitThreeDVRScene
struct  InitThreeDVRScene_t2362374842  : public MonoBehaviour_t1158329972
{
public:
	// Asset InitThreeDVRScene::asset
	Asset_t2923928748 * ___asset_2;

public:
	inline static int32_t get_offset_of_asset_2() { return static_cast<int32_t>(offsetof(InitThreeDVRScene_t2362374842, ___asset_2)); }
	inline Asset_t2923928748 * get_asset_2() const { return ___asset_2; }
	inline Asset_t2923928748 ** get_address_of_asset_2() { return &___asset_2; }
	inline void set_asset_2(Asset_t2923928748 * value)
	{
		___asset_2 = value;
		Il2CppCodeGenWriteBarrier(&___asset_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
