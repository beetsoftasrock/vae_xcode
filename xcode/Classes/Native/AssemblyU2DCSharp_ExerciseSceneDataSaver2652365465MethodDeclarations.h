﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExerciseSceneDataSaver
struct ExerciseSceneDataSaver_t2652365465;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"

// System.Void ExerciseSceneDataSaver::.ctor()
extern "C"  void ExerciseSceneDataSaver__ctor_m1952531378 (ExerciseSceneDataSaver_t2652365465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseSceneDataSaver::SaveRecord()
extern "C"  void ExerciseSceneDataSaver_SaveRecord_m734775200 (ExerciseSceneDataSaver_t2652365465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseSceneDataSaver::PushZip(System.String[],System.String)
extern "C"  void ExerciseSceneDataSaver_PushZip_m3876974107 (ExerciseSceneDataSaver_t2652365465 * __this, StringU5BU5D_t1642385972* ___soundFileNames0, String_t* ___zipFilePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseSceneDataSaver::SavePercent(System.Single,System.Single,System.Action,System.Action)
extern "C"  void ExerciseSceneDataSaver_SavePercent_m26806478 (ExerciseSceneDataSaver_t2652365465 * __this, float ___totalTimeRecorded0, float ___totalTimeSoundTemplate1, Action_t3226471752 * ___saveCompletedCallback2, Action_t3226471752 * ___saveErrorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
