﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonSound
struct  ButtonSound_t506092895  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button ButtonSound::_attachButton
	Button_t2872111280 * ____attachButton_2;

public:
	inline static int32_t get_offset_of__attachButton_2() { return static_cast<int32_t>(offsetof(ButtonSound_t506092895, ____attachButton_2)); }
	inline Button_t2872111280 * get__attachButton_2() const { return ____attachButton_2; }
	inline Button_t2872111280 ** get_address_of__attachButton_2() { return &____attachButton_2; }
	inline void set__attachButton_2(Button_t2872111280 * value)
	{
		____attachButton_2 = value;
		Il2CppCodeGenWriteBarrier(&____attachButton_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
