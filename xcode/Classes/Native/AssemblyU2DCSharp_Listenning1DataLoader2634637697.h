﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CommandSheet
struct CommandSheet_t2769384292;
// Listening1DatasWrapper
struct Listening1DatasWrapper_t1360134800;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listenning1DataLoader
struct  Listenning1DataLoader_t2634637697  : public MonoBehaviour_t1158329972
{
public:
	// CommandSheet Listenning1DataLoader::dataSheet
	CommandSheet_t2769384292 * ___dataSheet_2;
	// Listening1DatasWrapper Listenning1DataLoader::<datasWrapper>k__BackingField
	Listening1DatasWrapper_t1360134800 * ___U3CdatasWrapperU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_dataSheet_2() { return static_cast<int32_t>(offsetof(Listenning1DataLoader_t2634637697, ___dataSheet_2)); }
	inline CommandSheet_t2769384292 * get_dataSheet_2() const { return ___dataSheet_2; }
	inline CommandSheet_t2769384292 ** get_address_of_dataSheet_2() { return &___dataSheet_2; }
	inline void set_dataSheet_2(CommandSheet_t2769384292 * value)
	{
		___dataSheet_2 = value;
		Il2CppCodeGenWriteBarrier(&___dataSheet_2, value);
	}

	inline static int32_t get_offset_of_U3CdatasWrapperU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Listenning1DataLoader_t2634637697, ___U3CdatasWrapperU3Ek__BackingField_3)); }
	inline Listening1DatasWrapper_t1360134800 * get_U3CdatasWrapperU3Ek__BackingField_3() const { return ___U3CdatasWrapperU3Ek__BackingField_3; }
	inline Listening1DatasWrapper_t1360134800 ** get_address_of_U3CdatasWrapperU3Ek__BackingField_3() { return &___U3CdatasWrapperU3Ek__BackingField_3; }
	inline void set_U3CdatasWrapperU3Ek__BackingField_3(Listening1DatasWrapper_t1360134800 * value)
	{
		___U3CdatasWrapperU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdatasWrapperU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
