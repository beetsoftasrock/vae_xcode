﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSMicrophoneHelper
struct IOSMicrophoneHelper_t31973919;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSMicrophoneHelper::.ctor()
extern "C"  void IOSMicrophoneHelper__ctor_m173721080 (IOSMicrophoneHelper_t31973919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IOSMicrophoneHelper::Start()
extern "C"  Il2CppObject * IOSMicrophoneHelper_Start_m3446187404 (IOSMicrophoneHelper_t31973919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSMicrophoneHelper::RequestMicrophonePermission()
extern "C"  void IOSMicrophoneHelper_RequestMicrophonePermission_m2705723306 (IOSMicrophoneHelper_t31973919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
