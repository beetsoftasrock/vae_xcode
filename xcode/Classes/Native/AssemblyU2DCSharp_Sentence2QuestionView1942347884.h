﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// QuestionScenePopup
struct QuestionScenePopup_t792224618;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Animation
struct Animation_t2068071072;
// AnswerSound
struct AnswerSound_t4040477861;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;

#include "AssemblyU2DCSharp_BaseQuestionView79922856.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sentence2QuestionView
struct  Sentence2QuestionView_t1942347884  : public BaseQuestionView_t79922856
{
public:
	// QuestionScenePopup Sentence2QuestionView::popup
	QuestionScenePopup_t792224618 * ___popup_3;
	// UnityEngine.UI.InputField Sentence2QuestionView::inputField
	InputField_t1631627530 * ___inputField_4;
	// UnityEngine.UI.Text Sentence2QuestionView::_questionText
	Text_t356221433 * ____questionText_5;
	// UnityEngine.UI.Text Sentence2QuestionView::hiddenText
	Text_t356221433 * ___hiddenText_6;
	// UnityEngine.UI.Text Sentence2QuestionView::resultText
	Text_t356221433 * ___resultText_7;
	// UnityEngine.UI.Button Sentence2QuestionView::soundButton
	Button_t2872111280 * ___soundButton_8;
	// UnityEngine.UI.Button Sentence2QuestionView::summitButton
	Button_t2872111280 * ___summitButton_9;
	// UnityEngine.Animation Sentence2QuestionView::anim
	Animation_t2068071072 * ___anim_10;
	// AnswerSound Sentence2QuestionView::answerSound
	AnswerSound_t4040477861 * ___answerSound_11;
	// System.Collections.IEnumerator Sentence2QuestionView::_pointerEffectCoroutine
	Il2CppObject * ____pointerEffectCoroutine_12;
	// System.String Sentence2QuestionView::_mainText
	String_t* ____mainText_13;
	// System.String Sentence2QuestionView::_hightLight
	String_t* ____hightLight_14;
	// UnityEngine.AnimationClip Sentence2QuestionView::animResultOpen
	AnimationClip_t3510324950 * ___animResultOpen_15;
	// UnityEngine.AnimationClip Sentence2QuestionView::animResultClose
	AnimationClip_t3510324950 * ___animResultClose_16;

public:
	inline static int32_t get_offset_of_popup_3() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___popup_3)); }
	inline QuestionScenePopup_t792224618 * get_popup_3() const { return ___popup_3; }
	inline QuestionScenePopup_t792224618 ** get_address_of_popup_3() { return &___popup_3; }
	inline void set_popup_3(QuestionScenePopup_t792224618 * value)
	{
		___popup_3 = value;
		Il2CppCodeGenWriteBarrier(&___popup_3, value);
	}

	inline static int32_t get_offset_of_inputField_4() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___inputField_4)); }
	inline InputField_t1631627530 * get_inputField_4() const { return ___inputField_4; }
	inline InputField_t1631627530 ** get_address_of_inputField_4() { return &___inputField_4; }
	inline void set_inputField_4(InputField_t1631627530 * value)
	{
		___inputField_4 = value;
		Il2CppCodeGenWriteBarrier(&___inputField_4, value);
	}

	inline static int32_t get_offset_of__questionText_5() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ____questionText_5)); }
	inline Text_t356221433 * get__questionText_5() const { return ____questionText_5; }
	inline Text_t356221433 ** get_address_of__questionText_5() { return &____questionText_5; }
	inline void set__questionText_5(Text_t356221433 * value)
	{
		____questionText_5 = value;
		Il2CppCodeGenWriteBarrier(&____questionText_5, value);
	}

	inline static int32_t get_offset_of_hiddenText_6() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___hiddenText_6)); }
	inline Text_t356221433 * get_hiddenText_6() const { return ___hiddenText_6; }
	inline Text_t356221433 ** get_address_of_hiddenText_6() { return &___hiddenText_6; }
	inline void set_hiddenText_6(Text_t356221433 * value)
	{
		___hiddenText_6 = value;
		Il2CppCodeGenWriteBarrier(&___hiddenText_6, value);
	}

	inline static int32_t get_offset_of_resultText_7() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___resultText_7)); }
	inline Text_t356221433 * get_resultText_7() const { return ___resultText_7; }
	inline Text_t356221433 ** get_address_of_resultText_7() { return &___resultText_7; }
	inline void set_resultText_7(Text_t356221433 * value)
	{
		___resultText_7 = value;
		Il2CppCodeGenWriteBarrier(&___resultText_7, value);
	}

	inline static int32_t get_offset_of_soundButton_8() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___soundButton_8)); }
	inline Button_t2872111280 * get_soundButton_8() const { return ___soundButton_8; }
	inline Button_t2872111280 ** get_address_of_soundButton_8() { return &___soundButton_8; }
	inline void set_soundButton_8(Button_t2872111280 * value)
	{
		___soundButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___soundButton_8, value);
	}

	inline static int32_t get_offset_of_summitButton_9() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___summitButton_9)); }
	inline Button_t2872111280 * get_summitButton_9() const { return ___summitButton_9; }
	inline Button_t2872111280 ** get_address_of_summitButton_9() { return &___summitButton_9; }
	inline void set_summitButton_9(Button_t2872111280 * value)
	{
		___summitButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___summitButton_9, value);
	}

	inline static int32_t get_offset_of_anim_10() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___anim_10)); }
	inline Animation_t2068071072 * get_anim_10() const { return ___anim_10; }
	inline Animation_t2068071072 ** get_address_of_anim_10() { return &___anim_10; }
	inline void set_anim_10(Animation_t2068071072 * value)
	{
		___anim_10 = value;
		Il2CppCodeGenWriteBarrier(&___anim_10, value);
	}

	inline static int32_t get_offset_of_answerSound_11() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___answerSound_11)); }
	inline AnswerSound_t4040477861 * get_answerSound_11() const { return ___answerSound_11; }
	inline AnswerSound_t4040477861 ** get_address_of_answerSound_11() { return &___answerSound_11; }
	inline void set_answerSound_11(AnswerSound_t4040477861 * value)
	{
		___answerSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___answerSound_11, value);
	}

	inline static int32_t get_offset_of__pointerEffectCoroutine_12() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ____pointerEffectCoroutine_12)); }
	inline Il2CppObject * get__pointerEffectCoroutine_12() const { return ____pointerEffectCoroutine_12; }
	inline Il2CppObject ** get_address_of__pointerEffectCoroutine_12() { return &____pointerEffectCoroutine_12; }
	inline void set__pointerEffectCoroutine_12(Il2CppObject * value)
	{
		____pointerEffectCoroutine_12 = value;
		Il2CppCodeGenWriteBarrier(&____pointerEffectCoroutine_12, value);
	}

	inline static int32_t get_offset_of__mainText_13() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ____mainText_13)); }
	inline String_t* get__mainText_13() const { return ____mainText_13; }
	inline String_t** get_address_of__mainText_13() { return &____mainText_13; }
	inline void set__mainText_13(String_t* value)
	{
		____mainText_13 = value;
		Il2CppCodeGenWriteBarrier(&____mainText_13, value);
	}

	inline static int32_t get_offset_of__hightLight_14() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ____hightLight_14)); }
	inline String_t* get__hightLight_14() const { return ____hightLight_14; }
	inline String_t** get_address_of__hightLight_14() { return &____hightLight_14; }
	inline void set__hightLight_14(String_t* value)
	{
		____hightLight_14 = value;
		Il2CppCodeGenWriteBarrier(&____hightLight_14, value);
	}

	inline static int32_t get_offset_of_animResultOpen_15() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___animResultOpen_15)); }
	inline AnimationClip_t3510324950 * get_animResultOpen_15() const { return ___animResultOpen_15; }
	inline AnimationClip_t3510324950 ** get_address_of_animResultOpen_15() { return &___animResultOpen_15; }
	inline void set_animResultOpen_15(AnimationClip_t3510324950 * value)
	{
		___animResultOpen_15 = value;
		Il2CppCodeGenWriteBarrier(&___animResultOpen_15, value);
	}

	inline static int32_t get_offset_of_animResultClose_16() { return static_cast<int32_t>(offsetof(Sentence2QuestionView_t1942347884, ___animResultClose_16)); }
	inline AnimationClip_t3510324950 * get_animResultClose_16() const { return ___animResultClose_16; }
	inline AnimationClip_t3510324950 ** get_address_of_animResultClose_16() { return &___animResultClose_16; }
	inline void set_animResultClose_16(AnimationClip_t3510324950 * value)
	{
		___animResultClose_16 = value;
		Il2CppCodeGenWriteBarrier(&___animResultClose_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
