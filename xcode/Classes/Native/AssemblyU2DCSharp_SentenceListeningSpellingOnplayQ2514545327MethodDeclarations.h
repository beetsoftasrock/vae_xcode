﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SentenceListeningSpellingOnplayQuestion
struct SentenceListeningSpellingOnplayQuestion_t2514545327;
// SentenceListeningSpellingQuestionData
struct SentenceListeningSpellingQuestionData_t3011907248;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SentenceListeningSpellingQuestio3011907248.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void SentenceListeningSpellingOnplayQuestion::.ctor(SentenceListeningSpellingQuestionData)
extern "C"  void SentenceListeningSpellingOnplayQuestion__ctor_m761955048 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, SentenceListeningSpellingQuestionData_t3011907248 * ___questionData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SentenceListeningSpellingOnplayQuestion::CheckCorrect()
extern "C"  bool SentenceListeningSpellingOnplayQuestion_CheckCorrect_m1091493476 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SentenceListeningSpellingQuestionData SentenceListeningSpellingOnplayQuestion::get_questionData()
extern "C"  SentenceListeningSpellingQuestionData_t3011907248 * SentenceListeningSpellingOnplayQuestion_get_questionData_m2764697870 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::set_questionData(SentenceListeningSpellingQuestionData)
extern "C"  void SentenceListeningSpellingOnplayQuestion_set_questionData_m38961569 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, SentenceListeningSpellingQuestionData_t3011907248 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceListeningSpellingOnplayQuestion::get_currentAnswerText()
extern "C"  String_t* SentenceListeningSpellingOnplayQuestion_get_currentAnswerText_m1194751748 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::set_currentAnswerText(System.String)
extern "C"  void SentenceListeningSpellingOnplayQuestion_set_currentAnswerText_m838664781 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::SetView(UnityEngine.GameObject)
extern "C"  void SentenceListeningSpellingOnplayQuestion_SetView_m3155523181 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, GameObject_t1756533147 * ___viewGo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::RemoveView()
extern "C"  void SentenceListeningSpellingOnplayQuestion_RemoveView_m3739259441 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::ShowResult()
extern "C"  void SentenceListeningSpellingOnplayQuestion_ShowResult_m909480824 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SentenceListeningSpellingOnplayQuestion::GetScore()
extern "C"  float SentenceListeningSpellingOnplayQuestion_GetScore_m4171143140 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::InitView()
extern "C"  void SentenceListeningSpellingOnplayQuestion_InitView_m3430203409 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::SetEventHandler()
extern "C"  void SentenceListeningSpellingOnplayQuestion_SetEventHandler_m1471849134 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::UpdateQuestionText(System.String)
extern "C"  void SentenceListeningSpellingOnplayQuestion_UpdateQuestionText_m1890467176 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, String_t* ___fText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::HandleSummitAction()
extern "C"  void SentenceListeningSpellingOnplayQuestion_HandleSummitAction_m2424858145 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingOnplayQuestion::PlayQuestionSound()
extern "C"  void SentenceListeningSpellingOnplayQuestion_PlayQuestionSound_m505890855 (SentenceListeningSpellingOnplayQuestion_t2514545327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
