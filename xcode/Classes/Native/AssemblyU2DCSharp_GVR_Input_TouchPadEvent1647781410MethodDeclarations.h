﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.TouchPadEvent
struct TouchPadEvent_t1647781410;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.TouchPadEvent::.ctor()
extern "C"  void TouchPadEvent__ctor_m971226180 (TouchPadEvent_t1647781410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
