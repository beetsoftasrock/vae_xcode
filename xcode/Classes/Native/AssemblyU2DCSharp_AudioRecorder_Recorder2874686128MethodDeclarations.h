﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioRecorder.Recorder
struct Recorder_t2874686128;
// AudioRecorder.Recorder/OnInit
struct OnInit_t1674205282;
// AudioRecorder.Recorder/OnFinish
struct OnFinish_t2736449411;
// AudioRecorder.Recorder/OnError
struct OnError_t1220768278;
// AudioRecorder.Recorder/OnSaved
struct OnSaved_t836722345;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.IO.FileStream
struct FileStream_t1695958676;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_OnInit1674205282.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_OnFinish2736449411.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_OnError1220768278.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_OnSaved836722345.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "mscorlib_System_IO_FileStream1695958676.h"

// System.Void AudioRecorder.Recorder::.ctor()
extern "C"  void Recorder__ctor_m3528506547 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::add_onInit(AudioRecorder.Recorder/OnInit)
extern "C"  void Recorder_add_onInit_m2876192080 (Il2CppObject * __this /* static, unused */, OnInit_t1674205282 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::remove_onInit(AudioRecorder.Recorder/OnInit)
extern "C"  void Recorder_remove_onInit_m3198003925 (Il2CppObject * __this /* static, unused */, OnInit_t1674205282 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::add_onFinish(AudioRecorder.Recorder/OnFinish)
extern "C"  void Recorder_add_onFinish_m3632541520 (Il2CppObject * __this /* static, unused */, OnFinish_t2736449411 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::remove_onFinish(AudioRecorder.Recorder/OnFinish)
extern "C"  void Recorder_remove_onFinish_m3512099221 (Il2CppObject * __this /* static, unused */, OnFinish_t2736449411 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::add_onError(AudioRecorder.Recorder/OnError)
extern "C"  void Recorder_add_onError_m3456736288 (Il2CppObject * __this /* static, unused */, OnError_t1220768278 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::remove_onError(AudioRecorder.Recorder/OnError)
extern "C"  void Recorder_remove_onError_m1123835321 (Il2CppObject * __this /* static, unused */, OnError_t1220768278 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::add_onSaved(AudioRecorder.Recorder/OnSaved)
extern "C"  void Recorder_add_onSaved_m4025581986 (Il2CppObject * __this /* static, unused */, OnSaved_t836722345 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::remove_onSaved(AudioRecorder.Recorder/OnSaved)
extern "C"  void Recorder_remove_onSaved_m2021510901 (Il2CppObject * __this /* static, unused */, OnSaved_t836722345 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioRecorder.Recorder::get_hasRecorded()
extern "C"  bool Recorder_get_hasRecorded_m3453000918 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip AudioRecorder.Recorder::get_Clip()
extern "C"  AudioClip_t1932558630 * Recorder_get_Clip_m2400084054 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioRecorder.Recorder::get_IsRecording()
extern "C"  bool Recorder_get_IsRecording_m1309030433 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioRecorder.Recorder::get_IsReady()
extern "C"  bool Recorder_get_IsReady_m3305933831 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::AddHandler_onInit(AudioRecorder.Recorder/OnInit)
extern "C"  void Recorder_AddHandler_onInit_m3080951842 (Il2CppObject * __this /* static, unused */, OnInit_t1674205282 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::RemoveHandler_onInit(AudioRecorder.Recorder/OnInit)
extern "C"  void Recorder_RemoveHandler_onInit_m2327329513 (Il2CppObject * __this /* static, unused */, OnInit_t1674205282 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::AddHandler_onFinish(AudioRecorder.Recorder/OnFinish)
extern "C"  void Recorder_AddHandler_onFinish_m1606994274 (Il2CppObject * __this /* static, unused */, OnFinish_t2736449411 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::RemoveHandler_onFinish(AudioRecorder.Recorder/OnFinish)
extern "C"  void Recorder_RemoveHandler_onFinish_m1162836393 (Il2CppObject * __this /* static, unused */, OnFinish_t2736449411 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::AddHandler_onError(AudioRecorder.Recorder/OnError)
extern "C"  void Recorder_AddHandler_onError_m2576999474 (Il2CppObject * __this /* static, unused */, OnError_t1220768278 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::RemoveHandler_onError(AudioRecorder.Recorder/OnError)
extern "C"  void Recorder_RemoveHandler_onError_m3350387149 (Il2CppObject * __this /* static, unused */, OnError_t1220768278 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::AddHandler_onSaved(AudioRecorder.Recorder/OnSaved)
extern "C"  void Recorder_AddHandler_onSaved_m762018448 (Il2CppObject * __this /* static, unused */, OnSaved_t836722345 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::RemoveHandler_onSaved(AudioRecorder.Recorder/OnSaved)
extern "C"  void Recorder_RemoveHandler_onSaved_m3415908513 (Il2CppObject * __this /* static, unused */, OnSaved_t836722345 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::Init()
extern "C"  void Recorder_Init_m3685650413 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::PrepareForRecording()
extern "C"  void Recorder_PrepareForRecording_m2680997250 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::StartRecording(System.Boolean,System.Int32)
extern "C"  void Recorder_StartRecording_m600819564 (Recorder_t2874686128 * __this, bool ___isLoop0, int32_t ___lenInSec1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::StartRecording(System.Boolean,System.Int32,System.Int32)
extern "C"  void Recorder_StartRecording_m1379657793 (Recorder_t2874686128 * __this, bool ___isLoop0, int32_t ___lenInSec1, int32_t ___frequency2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::StopRecording()
extern "C"  void Recorder_StopRecording_m899945924 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::PlayRecorded(UnityEngine.AudioSource)
extern "C"  void Recorder_PlayRecorded_m3417418883 (Recorder_t2874686128 * __this, AudioSource_t1135106623 * ___src0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::Dispose()
extern "C"  void Recorder_Dispose_m3073919070 (Recorder_t2874686128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::PlayAudio(System.String,UnityEngine.AudioSource)
extern "C"  void Recorder_PlayAudio_m3922617851 (Recorder_t2874686128 * __this, String_t* ___filePath0, AudioSource_t1135106623 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::PlayAudio(UnityEngine.AudioClip,UnityEngine.AudioSource)
extern "C"  void Recorder_PlayAudio_m781169302 (Recorder_t2874686128 * __this, AudioClip_t1932558630 * ___clip0, AudioSource_t1135106623 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioRecorder.Recorder::Save(System.String,UnityEngine.AudioClip)
extern "C"  bool Recorder_Save_m3422789691 (Recorder_t2874686128 * __this, String_t* ___filePath0, AudioClip_t1932558630 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip AudioRecorder.Recorder::TrimSilence(UnityEngine.AudioClip,System.Single)
extern "C"  AudioClip_t1932558630 * Recorder_TrimSilence_m2446392940 (Recorder_t2874686128 * __this, AudioClip_t1932558630 * ___clip0, float ___min1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip AudioRecorder.Recorder::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32)
extern "C"  AudioClip_t1932558630 * Recorder_TrimSilence_m2677097346 (Recorder_t2874686128 * __this, List_1_t1445631064 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip AudioRecorder.Recorder::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32,System.Boolean)
extern "C"  AudioClip_t1932558630 * Recorder_TrimSilence_m2117374079 (Recorder_t2874686128 * __this, List_1_t1445631064 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, bool ___stream4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream AudioRecorder.Recorder::CreateEmpty(System.String)
extern "C"  FileStream_t1695958676 * Recorder_CreateEmpty_m1453121506 (Recorder_t2874686128 * __this, String_t* ___filepath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::ConvertAndWrite(System.IO.FileStream,UnityEngine.AudioClip)
extern "C"  void Recorder_ConvertAndWrite_m1954050746 (Recorder_t2874686128 * __this, FileStream_t1695958676 * ___fileStream0, AudioClip_t1932558630 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder::WriteHeader(System.IO.FileStream,UnityEngine.AudioClip)
extern "C"  void Recorder_WriteHeader_m2541979951 (Recorder_t2874686128 * __this, FileStream_t1695958676 * ___fileStream0, AudioClip_t1932558630 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
