﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// LearningResultData
struct LearningResultData_t1558878615;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LearningResultData1558878615.h"
#include "mscorlib_System_String2029220233.h"

// UnityEngine.WWWForm LearningResultDataExtension::GetWWWForm(LearningResultData,System.String)
extern "C"  WWWForm_t3950226929 * LearningResultDataExtension_GetWWWForm_m2732886196 (Il2CppObject * __this /* static, unused */, LearningResultData_t1558878615 * ___resultData0, String_t* ___uuid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
