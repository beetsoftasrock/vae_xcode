﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultSaver/<DoPushLearningResult>c__AnonStorey0
struct U3CDoPushLearningResultU3Ec__AnonStorey0_t3642887568;
// ResultSaveResponse
struct ResultSaveResponse_t3438979681;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResultSaveResponse3438979681.h"

// System.Void ResultSaver/<DoPushLearningResult>c__AnonStorey0::.ctor()
extern "C"  void U3CDoPushLearningResultU3Ec__AnonStorey0__ctor_m3618404207 (U3CDoPushLearningResultU3Ec__AnonStorey0_t3642887568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultSaver/<DoPushLearningResult>c__AnonStorey0::<>m__0(ResultSaveResponse)
extern "C"  void U3CDoPushLearningResultU3Ec__AnonStorey0_U3CU3Em__0_m486301931 (U3CDoPushLearningResultU3Ec__AnonStorey0_t3642887568 * __this, ResultSaveResponse_t3438979681 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultSaver/<DoPushLearningResult>c__AnonStorey0::<>m__1()
extern "C"  void U3CDoPushLearningResultU3Ec__AnonStorey0_U3CU3Em__1_m2026792957 (U3CDoPushLearningResultU3Ec__AnonStorey0_t3642887568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
