﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HUDFPS
struct  HUDFPS_t647744412  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rect HUDFPS::startRect
	Rect_t3681755626  ___startRect_2;
	// System.Boolean HUDFPS::updateColor
	bool ___updateColor_3;
	// System.Boolean HUDFPS::allowDrag
	bool ___allowDrag_4;
	// System.Single HUDFPS::frequency
	float ___frequency_5;
	// System.Int32 HUDFPS::nbDecimal
	int32_t ___nbDecimal_6;
	// System.Single HUDFPS::accum
	float ___accum_7;
	// System.Int32 HUDFPS::frames
	int32_t ___frames_8;
	// UnityEngine.Color HUDFPS::color
	Color_t2020392075  ___color_9;
	// System.String HUDFPS::sFPS
	String_t* ___sFPS_10;
	// UnityEngine.GUIStyle HUDFPS::style
	GUIStyle_t1799908754 * ___style_11;

public:
	inline static int32_t get_offset_of_startRect_2() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___startRect_2)); }
	inline Rect_t3681755626  get_startRect_2() const { return ___startRect_2; }
	inline Rect_t3681755626 * get_address_of_startRect_2() { return &___startRect_2; }
	inline void set_startRect_2(Rect_t3681755626  value)
	{
		___startRect_2 = value;
	}

	inline static int32_t get_offset_of_updateColor_3() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___updateColor_3)); }
	inline bool get_updateColor_3() const { return ___updateColor_3; }
	inline bool* get_address_of_updateColor_3() { return &___updateColor_3; }
	inline void set_updateColor_3(bool value)
	{
		___updateColor_3 = value;
	}

	inline static int32_t get_offset_of_allowDrag_4() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___allowDrag_4)); }
	inline bool get_allowDrag_4() const { return ___allowDrag_4; }
	inline bool* get_address_of_allowDrag_4() { return &___allowDrag_4; }
	inline void set_allowDrag_4(bool value)
	{
		___allowDrag_4 = value;
	}

	inline static int32_t get_offset_of_frequency_5() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___frequency_5)); }
	inline float get_frequency_5() const { return ___frequency_5; }
	inline float* get_address_of_frequency_5() { return &___frequency_5; }
	inline void set_frequency_5(float value)
	{
		___frequency_5 = value;
	}

	inline static int32_t get_offset_of_nbDecimal_6() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___nbDecimal_6)); }
	inline int32_t get_nbDecimal_6() const { return ___nbDecimal_6; }
	inline int32_t* get_address_of_nbDecimal_6() { return &___nbDecimal_6; }
	inline void set_nbDecimal_6(int32_t value)
	{
		___nbDecimal_6 = value;
	}

	inline static int32_t get_offset_of_accum_7() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___accum_7)); }
	inline float get_accum_7() const { return ___accum_7; }
	inline float* get_address_of_accum_7() { return &___accum_7; }
	inline void set_accum_7(float value)
	{
		___accum_7 = value;
	}

	inline static int32_t get_offset_of_frames_8() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___frames_8)); }
	inline int32_t get_frames_8() const { return ___frames_8; }
	inline int32_t* get_address_of_frames_8() { return &___frames_8; }
	inline void set_frames_8(int32_t value)
	{
		___frames_8 = value;
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___color_9)); }
	inline Color_t2020392075  get_color_9() const { return ___color_9; }
	inline Color_t2020392075 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_t2020392075  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_sFPS_10() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___sFPS_10)); }
	inline String_t* get_sFPS_10() const { return ___sFPS_10; }
	inline String_t** get_address_of_sFPS_10() { return &___sFPS_10; }
	inline void set_sFPS_10(String_t* value)
	{
		___sFPS_10 = value;
		Il2CppCodeGenWriteBarrier(&___sFPS_10, value);
	}

	inline static int32_t get_offset_of_style_11() { return static_cast<int32_t>(offsetof(HUDFPS_t647744412, ___style_11)); }
	inline GUIStyle_t1799908754 * get_style_11() const { return ___style_11; }
	inline GUIStyle_t1799908754 ** get_address_of_style_11() { return &___style_11; }
	inline void set_style_11(GUIStyle_t1799908754 * value)
	{
		___style_11 = value;
		Il2CppCodeGenWriteBarrier(&___style_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
