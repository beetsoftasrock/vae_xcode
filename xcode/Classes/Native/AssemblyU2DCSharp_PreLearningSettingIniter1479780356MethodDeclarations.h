﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PreLearningSettingIniter
struct PreLearningSettingIniter_t1479780356;
// BaseSetting
struct BaseSetting_t2575616157;

#include "codegen/il2cpp-codegen.h"

// System.Void PreLearningSettingIniter::.ctor()
extern "C"  void PreLearningSettingIniter__ctor_m1914703739 (PreLearningSettingIniter_t1479780356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseSetting PreLearningSettingIniter::InitSetting()
extern "C"  BaseSetting_t2575616157 * PreLearningSettingIniter_InitSetting_m3500002875 (PreLearningSettingIniter_t1479780356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
