﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<LoadScene>c__AnonStorey6
struct U3CLoadSceneU3Ec__AnonStorey6_t1280203046;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AssetBundles.AssetBundleManager/<LoadScene>c__AnonStorey6::.ctor()
extern "C"  void U3CLoadSceneU3Ec__AnonStorey6__ctor_m92355825 (U3CLoadSceneU3Ec__AnonStorey6_t1280203046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager/<LoadScene>c__AnonStorey6::<>m__0(System.String)
extern "C"  bool U3CLoadSceneU3Ec__AnonStorey6_U3CU3Em__0_m1565914640 (U3CLoadSceneU3Ec__AnonStorey6_t1280203046 * __this, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
