﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VaeBuildSetting
struct  VaeBuildSetting_t3353517300  : public ScriptableObject_t1975622470
{
public:
	// System.Boolean VaeBuildSetting::developMode
	bool ___developMode_4;
	// System.Boolean VaeBuildSetting::developBundle
	bool ___developBundle_5;
	// System.String VaeBuildSetting::branchHashCode
	String_t* ___branchHashCode_6;

public:
	inline static int32_t get_offset_of_developMode_4() { return static_cast<int32_t>(offsetof(VaeBuildSetting_t3353517300, ___developMode_4)); }
	inline bool get_developMode_4() const { return ___developMode_4; }
	inline bool* get_address_of_developMode_4() { return &___developMode_4; }
	inline void set_developMode_4(bool value)
	{
		___developMode_4 = value;
	}

	inline static int32_t get_offset_of_developBundle_5() { return static_cast<int32_t>(offsetof(VaeBuildSetting_t3353517300, ___developBundle_5)); }
	inline bool get_developBundle_5() const { return ___developBundle_5; }
	inline bool* get_address_of_developBundle_5() { return &___developBundle_5; }
	inline void set_developBundle_5(bool value)
	{
		___developBundle_5 = value;
	}

	inline static int32_t get_offset_of_branchHashCode_6() { return static_cast<int32_t>(offsetof(VaeBuildSetting_t3353517300, ___branchHashCode_6)); }
	inline String_t* get_branchHashCode_6() const { return ___branchHashCode_6; }
	inline String_t** get_address_of_branchHashCode_6() { return &___branchHashCode_6; }
	inline void set_branchHashCode_6(String_t* value)
	{
		___branchHashCode_6 = value;
		Il2CppCodeGenWriteBarrier(&___branchHashCode_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
