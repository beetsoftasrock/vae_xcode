﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;
// ChapterLoader
struct ChapterLoader_t1965880082;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterLoader/<loadAssetBundle>c__AnonStorey0
struct  U3CloadAssetBundleU3Ec__AnonStorey0_t99180152  : public Il2CppObject
{
public:
	// System.Action ChapterLoader/<loadAssetBundle>c__AnonStorey0::onSuccess
	Action_t3226471752 * ___onSuccess_0;
	// ChapterLoader ChapterLoader/<loadAssetBundle>c__AnonStorey0::$this
	ChapterLoader_t1965880082 * ___U24this_1;

public:
	inline static int32_t get_offset_of_onSuccess_0() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__AnonStorey0_t99180152, ___onSuccess_0)); }
	inline Action_t3226471752 * get_onSuccess_0() const { return ___onSuccess_0; }
	inline Action_t3226471752 ** get_address_of_onSuccess_0() { return &___onSuccess_0; }
	inline void set_onSuccess_0(Action_t3226471752 * value)
	{
		___onSuccess_0 = value;
		Il2CppCodeGenWriteBarrier(&___onSuccess_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__AnonStorey0_t99180152, ___U24this_1)); }
	inline ChapterLoader_t1965880082 * get_U24this_1() const { return ___U24this_1; }
	inline ChapterLoader_t1965880082 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ChapterLoader_t1965880082 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
