﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BaseConversationController
struct BaseConversationController_t808907754;
// ConversationLogicController
struct ConversationLogicController_t3911886281;
// PlayerTalkModule
struct PlayerTalkModule_t3356839145;
// VoiceRecordingDialog
struct VoiceRecordingDialog_t3950460413;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// SoundRecorder
struct SoundRecorder_t3431875919;
// ReplayTalkDialog
struct ReplayTalkDialog_t3732871695;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoiceRecordModule
struct  VoiceRecordModule_t679113955  : public MonoBehaviour_t1158329972
{
public:
	// BaseConversationController VoiceRecordModule::bcc
	BaseConversationController_t808907754 * ___bcc_2;
	// ConversationLogicController VoiceRecordModule::clc
	ConversationLogicController_t3911886281 * ___clc_3;
	// PlayerTalkModule VoiceRecordModule::playerTalkModule
	PlayerTalkModule_t3356839145 * ___playerTalkModule_4;
	// VoiceRecordingDialog VoiceRecordModule::voiceRecordingDialog
	VoiceRecordingDialog_t3950460413 * ___voiceRecordingDialog_5;
	// UnityEngine.UI.Button VoiceRecordModule::replayRecordedButton
	Button_t2872111280 * ___replayRecordedButton_6;
	// UnityEngine.GameObject VoiceRecordModule::bottomButtons
	GameObject_t1756533147 * ___bottomButtons_7;
	// SoundRecorder VoiceRecordModule::soundRecorder
	SoundRecorder_t3431875919 * ___soundRecorder_8;
	// ReplayTalkDialog VoiceRecordModule::replayTalkDialog
	ReplayTalkDialog_t3732871695 * ___replayTalkDialog_9;
	// System.Single VoiceRecordModule::startRecordTime
	float ___startRecordTime_11;
	// UnityEngine.Coroutine VoiceRecordModule::_replayCo
	Coroutine_t2299508840 * ____replayCo_12;

public:
	inline static int32_t get_offset_of_bcc_2() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___bcc_2)); }
	inline BaseConversationController_t808907754 * get_bcc_2() const { return ___bcc_2; }
	inline BaseConversationController_t808907754 ** get_address_of_bcc_2() { return &___bcc_2; }
	inline void set_bcc_2(BaseConversationController_t808907754 * value)
	{
		___bcc_2 = value;
		Il2CppCodeGenWriteBarrier(&___bcc_2, value);
	}

	inline static int32_t get_offset_of_clc_3() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___clc_3)); }
	inline ConversationLogicController_t3911886281 * get_clc_3() const { return ___clc_3; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_3() { return &___clc_3; }
	inline void set_clc_3(ConversationLogicController_t3911886281 * value)
	{
		___clc_3 = value;
		Il2CppCodeGenWriteBarrier(&___clc_3, value);
	}

	inline static int32_t get_offset_of_playerTalkModule_4() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___playerTalkModule_4)); }
	inline PlayerTalkModule_t3356839145 * get_playerTalkModule_4() const { return ___playerTalkModule_4; }
	inline PlayerTalkModule_t3356839145 ** get_address_of_playerTalkModule_4() { return &___playerTalkModule_4; }
	inline void set_playerTalkModule_4(PlayerTalkModule_t3356839145 * value)
	{
		___playerTalkModule_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerTalkModule_4, value);
	}

	inline static int32_t get_offset_of_voiceRecordingDialog_5() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___voiceRecordingDialog_5)); }
	inline VoiceRecordingDialog_t3950460413 * get_voiceRecordingDialog_5() const { return ___voiceRecordingDialog_5; }
	inline VoiceRecordingDialog_t3950460413 ** get_address_of_voiceRecordingDialog_5() { return &___voiceRecordingDialog_5; }
	inline void set_voiceRecordingDialog_5(VoiceRecordingDialog_t3950460413 * value)
	{
		___voiceRecordingDialog_5 = value;
		Il2CppCodeGenWriteBarrier(&___voiceRecordingDialog_5, value);
	}

	inline static int32_t get_offset_of_replayRecordedButton_6() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___replayRecordedButton_6)); }
	inline Button_t2872111280 * get_replayRecordedButton_6() const { return ___replayRecordedButton_6; }
	inline Button_t2872111280 ** get_address_of_replayRecordedButton_6() { return &___replayRecordedButton_6; }
	inline void set_replayRecordedButton_6(Button_t2872111280 * value)
	{
		___replayRecordedButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___replayRecordedButton_6, value);
	}

	inline static int32_t get_offset_of_bottomButtons_7() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___bottomButtons_7)); }
	inline GameObject_t1756533147 * get_bottomButtons_7() const { return ___bottomButtons_7; }
	inline GameObject_t1756533147 ** get_address_of_bottomButtons_7() { return &___bottomButtons_7; }
	inline void set_bottomButtons_7(GameObject_t1756533147 * value)
	{
		___bottomButtons_7 = value;
		Il2CppCodeGenWriteBarrier(&___bottomButtons_7, value);
	}

	inline static int32_t get_offset_of_soundRecorder_8() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___soundRecorder_8)); }
	inline SoundRecorder_t3431875919 * get_soundRecorder_8() const { return ___soundRecorder_8; }
	inline SoundRecorder_t3431875919 ** get_address_of_soundRecorder_8() { return &___soundRecorder_8; }
	inline void set_soundRecorder_8(SoundRecorder_t3431875919 * value)
	{
		___soundRecorder_8 = value;
		Il2CppCodeGenWriteBarrier(&___soundRecorder_8, value);
	}

	inline static int32_t get_offset_of_replayTalkDialog_9() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___replayTalkDialog_9)); }
	inline ReplayTalkDialog_t3732871695 * get_replayTalkDialog_9() const { return ___replayTalkDialog_9; }
	inline ReplayTalkDialog_t3732871695 ** get_address_of_replayTalkDialog_9() { return &___replayTalkDialog_9; }
	inline void set_replayTalkDialog_9(ReplayTalkDialog_t3732871695 * value)
	{
		___replayTalkDialog_9 = value;
		Il2CppCodeGenWriteBarrier(&___replayTalkDialog_9, value);
	}

	inline static int32_t get_offset_of_startRecordTime_11() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ___startRecordTime_11)); }
	inline float get_startRecordTime_11() const { return ___startRecordTime_11; }
	inline float* get_address_of_startRecordTime_11() { return &___startRecordTime_11; }
	inline void set_startRecordTime_11(float value)
	{
		___startRecordTime_11 = value;
	}

	inline static int32_t get_offset_of__replayCo_12() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955, ____replayCo_12)); }
	inline Coroutine_t2299508840 * get__replayCo_12() const { return ____replayCo_12; }
	inline Coroutine_t2299508840 ** get_address_of__replayCo_12() { return &____replayCo_12; }
	inline void set__replayCo_12(Coroutine_t2299508840 * value)
	{
		____replayCo_12 = value;
		Il2CppCodeGenWriteBarrier(&____replayCo_12, value);
	}
};

struct VoiceRecordModule_t679113955_StaticFields
{
public:
	// System.Int32 VoiceRecordModule::trackCountRecord
	int32_t ___trackCountRecord_10;

public:
	inline static int32_t get_offset_of_trackCountRecord_10() { return static_cast<int32_t>(offsetof(VoiceRecordModule_t679113955_StaticFields, ___trackCountRecord_10)); }
	inline int32_t get_trackCountRecord_10() const { return ___trackCountRecord_10; }
	inline int32_t* get_address_of_trackCountRecord_10() { return &___trackCountRecord_10; }
	inline void set_trackCountRecord_10(int32_t value)
	{
		___trackCountRecord_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
