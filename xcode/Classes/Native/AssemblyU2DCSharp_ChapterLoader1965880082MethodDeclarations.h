﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterLoader
struct ChapterLoader_t1965880082;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChapterLoader::.ctor()
extern "C"  void ChapterLoader__ctor_m773433955 (ChapterLoader_t1965880082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::loadAssetBundle(System.String[],System.Action)
extern "C"  void ChapterLoader_loadAssetBundle_m561759240 (ChapterLoader_t1965880082 * __this, StringU5BU5D_t1642385972* ___bundleName0, Action_t3226471752 * ___onSuccess1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::loadAssetBundle(System.String,System.Action)
extern "C"  void ChapterLoader_loadAssetBundle_m1367674340 (ChapterLoader_t1965880082 * __this, String_t* ___bundleName0, Action_t3226471752 * ___onSuccess1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ChapterLoader::getBundlesByChapter(System.Int32)
extern "C"  StringU5BU5D_t1642385972* ChapterLoader_getBundlesByChapter_m669914364 (Il2CppObject * __this /* static, unused */, int32_t ___chapterId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::UnloadChapterData(System.Int32)
extern "C"  void ChapterLoader_UnloadChapterData_m1822092340 (Il2CppObject * __this /* static, unused */, int32_t ___chapterId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::LoadChapterData(System.Int32,System.Action)
extern "C"  void ChapterLoader_LoadChapterData_m2116927332 (Il2CppObject * __this /* static, unused */, int32_t ___chapterId0, Action_t3226471752 * ___onSuccess1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> ChapterLoader::get_bundlesOfMyPage()
extern "C"  List_1_t1398341365 * ChapterLoader_get_bundlesOfMyPage_m604489380 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::GoToMyPage()
extern "C"  void ChapterLoader_GoToMyPage_m16059219 (ChapterLoader_t1965880082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::UnloadMyPage(System.Int32)
extern "C"  void ChapterLoader_UnloadMyPage_m1774883214 (Il2CppObject * __this /* static, unused */, int32_t ___ignoreChapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::GoToTermsOfUse()
extern "C"  void ChapterLoader_GoToTermsOfUse_m1449339755 (ChapterLoader_t1965880082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::<loadAssetBundle>m__0(System.String)
extern "C"  void ChapterLoader_U3CloadAssetBundleU3Em__0_m4064580134 (Il2CppObject * __this /* static, unused */, String_t* ___err0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterLoader::<UnloadChapterData>m__1(System.String)
extern "C"  bool ChapterLoader_U3CUnloadChapterDataU3Em__1_m3240543779 (Il2CppObject * __this /* static, unused */, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::<GoToMyPage>m__2()
extern "C"  void ChapterLoader_U3CGoToMyPageU3Em__2_m24146600 (ChapterLoader_t1965880082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader::<GoToTermsOfUse>m__3()
extern "C"  void ChapterLoader_U3CGoToTermsOfUseU3Em__3_m804059903 (ChapterLoader_t1965880082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
