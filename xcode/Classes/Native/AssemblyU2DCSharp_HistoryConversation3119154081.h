﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HistoryConversation
struct  HistoryConversation_t3119154081  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 HistoryConversation::indexItem
	int32_t ___indexItem_2;
	// UnityEngine.GameObject HistoryConversation::itemMe
	GameObject_t1756533147 * ___itemMe_3;
	// UnityEngine.GameObject HistoryConversation::itemYou
	GameObject_t1756533147 * ___itemYou_4;
	// UnityEngine.UI.Button HistoryConversation::btnUp
	Button_t2872111280 * ___btnUp_5;
	// UnityEngine.UI.Button HistoryConversation::btnDown
	Button_t2872111280 * ___btnDown_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HistoryConversation::lstItemDialog
	List_1_t1125654279 * ___lstItemDialog_7;
	// UnityEngine.GameObject HistoryConversation::dialogMe
	GameObject_t1756533147 * ___dialogMe_8;
	// UnityEngine.GameObject HistoryConversation::dialogYou
	GameObject_t1756533147 * ___dialogYou_9;

public:
	inline static int32_t get_offset_of_indexItem_2() { return static_cast<int32_t>(offsetof(HistoryConversation_t3119154081, ___indexItem_2)); }
	inline int32_t get_indexItem_2() const { return ___indexItem_2; }
	inline int32_t* get_address_of_indexItem_2() { return &___indexItem_2; }
	inline void set_indexItem_2(int32_t value)
	{
		___indexItem_2 = value;
	}

	inline static int32_t get_offset_of_itemMe_3() { return static_cast<int32_t>(offsetof(HistoryConversation_t3119154081, ___itemMe_3)); }
	inline GameObject_t1756533147 * get_itemMe_3() const { return ___itemMe_3; }
	inline GameObject_t1756533147 ** get_address_of_itemMe_3() { return &___itemMe_3; }
	inline void set_itemMe_3(GameObject_t1756533147 * value)
	{
		___itemMe_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemMe_3, value);
	}

	inline static int32_t get_offset_of_itemYou_4() { return static_cast<int32_t>(offsetof(HistoryConversation_t3119154081, ___itemYou_4)); }
	inline GameObject_t1756533147 * get_itemYou_4() const { return ___itemYou_4; }
	inline GameObject_t1756533147 ** get_address_of_itemYou_4() { return &___itemYou_4; }
	inline void set_itemYou_4(GameObject_t1756533147 * value)
	{
		___itemYou_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemYou_4, value);
	}

	inline static int32_t get_offset_of_btnUp_5() { return static_cast<int32_t>(offsetof(HistoryConversation_t3119154081, ___btnUp_5)); }
	inline Button_t2872111280 * get_btnUp_5() const { return ___btnUp_5; }
	inline Button_t2872111280 ** get_address_of_btnUp_5() { return &___btnUp_5; }
	inline void set_btnUp_5(Button_t2872111280 * value)
	{
		___btnUp_5 = value;
		Il2CppCodeGenWriteBarrier(&___btnUp_5, value);
	}

	inline static int32_t get_offset_of_btnDown_6() { return static_cast<int32_t>(offsetof(HistoryConversation_t3119154081, ___btnDown_6)); }
	inline Button_t2872111280 * get_btnDown_6() const { return ___btnDown_6; }
	inline Button_t2872111280 ** get_address_of_btnDown_6() { return &___btnDown_6; }
	inline void set_btnDown_6(Button_t2872111280 * value)
	{
		___btnDown_6 = value;
		Il2CppCodeGenWriteBarrier(&___btnDown_6, value);
	}

	inline static int32_t get_offset_of_lstItemDialog_7() { return static_cast<int32_t>(offsetof(HistoryConversation_t3119154081, ___lstItemDialog_7)); }
	inline List_1_t1125654279 * get_lstItemDialog_7() const { return ___lstItemDialog_7; }
	inline List_1_t1125654279 ** get_address_of_lstItemDialog_7() { return &___lstItemDialog_7; }
	inline void set_lstItemDialog_7(List_1_t1125654279 * value)
	{
		___lstItemDialog_7 = value;
		Il2CppCodeGenWriteBarrier(&___lstItemDialog_7, value);
	}

	inline static int32_t get_offset_of_dialogMe_8() { return static_cast<int32_t>(offsetof(HistoryConversation_t3119154081, ___dialogMe_8)); }
	inline GameObject_t1756533147 * get_dialogMe_8() const { return ___dialogMe_8; }
	inline GameObject_t1756533147 ** get_address_of_dialogMe_8() { return &___dialogMe_8; }
	inline void set_dialogMe_8(GameObject_t1756533147 * value)
	{
		___dialogMe_8 = value;
		Il2CppCodeGenWriteBarrier(&___dialogMe_8, value);
	}

	inline static int32_t get_offset_of_dialogYou_9() { return static_cast<int32_t>(offsetof(HistoryConversation_t3119154081, ___dialogYou_9)); }
	inline GameObject_t1756533147 * get_dialogYou_9() const { return ___dialogYou_9; }
	inline GameObject_t1756533147 ** get_address_of_dialogYou_9() { return &___dialogYou_9; }
	inline void set_dialogYou_9(GameObject_t1756533147 * value)
	{
		___dialogYou_9 = value;
		Il2CppCodeGenWriteBarrier(&___dialogYou_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
