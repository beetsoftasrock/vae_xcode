﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_BaseConversationCommandLoader1091304408.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExcerciseCommandLoader
struct  ExcerciseCommandLoader_t1886015525  : public BaseConversationCommandLoader_t1091304408
{
public:
	// System.Int32 ExcerciseCommandLoader::_currentLevel
	int32_t ____currentLevel_5;

public:
	inline static int32_t get_offset_of__currentLevel_5() { return static_cast<int32_t>(offsetof(ExcerciseCommandLoader_t1886015525, ____currentLevel_5)); }
	inline int32_t get__currentLevel_5() const { return ____currentLevel_5; }
	inline int32_t* get_address_of__currentLevel_5() { return &____currentLevel_5; }
	inline void set__currentLevel_5(int32_t value)
	{
		____currentLevel_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
