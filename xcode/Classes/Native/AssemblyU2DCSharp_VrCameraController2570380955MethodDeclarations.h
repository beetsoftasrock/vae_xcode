﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VrCameraController
struct VrCameraController_t2570380955;

#include "codegen/il2cpp-codegen.h"

// System.Void VrCameraController::.ctor()
extern "C"  void VrCameraController__ctor_m1600436806 (VrCameraController_t2570380955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
