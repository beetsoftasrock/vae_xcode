﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuDragHandler
struct MenuDragHandler_t1912038821;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void MenuDragHandler::.ctor()
extern "C"  void MenuDragHandler__ctor_m2147714994 (MenuDragHandler_t1912038821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuDragHandler::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MenuDragHandler_OnEndDrag_m3494516242 (MenuDragHandler_t1912038821 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuDragHandler::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MenuDragHandler_OnDrag_m1173098305 (MenuDragHandler_t1912038821 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuDragHandler::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void MenuDragHandler_OnBeginDrag_m2409072130 (MenuDragHandler_t1912038821 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
