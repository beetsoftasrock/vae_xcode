﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearningChapterLogData
struct LearningChapterLogData_t1224380751;

#include "codegen/il2cpp-codegen.h"

// System.Void LearningChapterLogData::.ctor()
extern "C"  void LearningChapterLogData__ctor_m2092444038 (LearningChapterLogData_t1224380751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
