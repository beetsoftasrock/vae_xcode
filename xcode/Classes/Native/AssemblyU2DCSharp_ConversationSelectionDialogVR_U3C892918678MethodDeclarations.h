﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionDialogVR/<ShowSelection>c__Iterator0
struct U3CShowSelectionU3Ec__Iterator0_t892918678;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::.ctor()
extern "C"  void U3CShowSelectionU3Ec__Iterator0__ctor_m3269138621 (U3CShowSelectionU3Ec__Iterator0_t892918678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::MoveNext()
extern "C"  bool U3CShowSelectionU3Ec__Iterator0_MoveNext_m67663875 (U3CShowSelectionU3Ec__Iterator0_t892918678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowSelectionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4260578855 (U3CShowSelectionU3Ec__Iterator0_t892918678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowSelectionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2248883711 (U3CShowSelectionU3Ec__Iterator0_t892918678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::Dispose()
extern "C"  void U3CShowSelectionU3Ec__Iterator0_Dispose_m3356375608 (U3CShowSelectionU3Ec__Iterator0_t892918678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::Reset()
extern "C"  void U3CShowSelectionU3Ec__Iterator0_Reset_m3833029562 (U3CShowSelectionU3Ec__Iterator0_t892918678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::<>m__0()
extern "C"  bool U3CShowSelectionU3Ec__Iterator0_U3CU3Em__0_m3533279650 (U3CShowSelectionU3Ec__Iterator0_t892918678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
