﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegionEffect
struct RegionEffect_t2803163609;

#include "codegen/il2cpp-codegen.h"

// System.Void RegionEffect::.ctor()
extern "C"  void RegionEffect__ctor_m112124424 (RegionEffect_t2803163609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegionEffect::On()
extern "C"  void RegionEffect_On_m3054776381 (RegionEffect_t2803163609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegionEffect::Off()
extern "C"  void RegionEffect_Off_m552752459 (RegionEffect_t2803163609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegionEffect::OnDisable()
extern "C"  void RegionEffect_OnDisable_m2365187005 (RegionEffect_t2803163609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
