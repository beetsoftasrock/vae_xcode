﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterMyPageScript/<waitFadeOut>c__Iterator0
struct U3CwaitFadeOutU3Ec__Iterator0_t1364814582;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterMyPageScript/<waitFadeOut>c__Iterator0::.ctor()
extern "C"  void U3CwaitFadeOutU3Ec__Iterator0__ctor_m3701149621 (U3CwaitFadeOutU3Ec__Iterator0_t1364814582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterMyPageScript/<waitFadeOut>c__Iterator0::MoveNext()
extern "C"  bool U3CwaitFadeOutU3Ec__Iterator0_MoveNext_m1653245611 (U3CwaitFadeOutU3Ec__Iterator0_t1364814582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterMyPageScript/<waitFadeOut>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitFadeOutU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3824712615 (U3CwaitFadeOutU3Ec__Iterator0_t1364814582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterMyPageScript/<waitFadeOut>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitFadeOutU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m216935887 (U3CwaitFadeOutU3Ec__Iterator0_t1364814582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript/<waitFadeOut>c__Iterator0::Dispose()
extern "C"  void U3CwaitFadeOutU3Ec__Iterator0_Dispose_m3457577984 (U3CwaitFadeOutU3Ec__Iterator0_t1364814582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript/<waitFadeOut>c__Iterator0::Reset()
extern "C"  void U3CwaitFadeOutU3Ec__Iterator0_Reset_m122019522 (U3CwaitFadeOutU3Ec__Iterator0_t1364814582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
