﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// LearningData
struct LearningData_t1664811342;
// LearningLogData
struct LearningLogData_t2500481692;
// LearningChapterLogData
struct LearningChapterLogData_t1224380751;
// AssetBundles.AssetBundleLoader
struct AssetBundleLoader_t639004779;
// PostureCharacter
struct PostureCharacter_t580023905;
// StateEmotionCharacter
struct StateEmotionCharacter_t3036603695;
// SelectButton
struct SelectButton_t132497280;
// ChapterTopSetting
struct ChapterTopSetting_t2509872352;
// Cell
struct Cell_t3051913968;
// LessonValue
struct LessonValue_t1076530945;
// MyPageChapterController
struct MyPageChapterController_t2166380602;
// Column
struct Column_t1930583302;
// ChapterMyPageScript
struct ChapterMyPageScript_t3593529611;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;
// CommandSheet/Param
struct Param_t4123818474;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// IConversationModule
struct IConversationModule_t2617440232;
// ConversationCommand
struct ConversationCommand_t3660105836;
// ISubtextItem
struct ISubtextItem_t4284961295;
// CharacterSelectionView
struct CharacterSelectionView_t2239851166;
// ConversationSceneSelectionView
struct ConversationSceneSelectionView_t407867144;
// ExcerciseSceneSelectionView
struct ExcerciseSceneSelectionView_t3932267486;
// ConversationHistoryItemView
struct ConversationHistoryItemView_t3846761105;
// ChapterItem
struct ChapterItem_t2155291334;
// CachedDragInfo
struct CachedDragInfo_t1136705792;
// Listening1QuestionData
struct Listening1QuestionData_t3381069024;
// TrueFalseAnswerSelection
struct TrueFalseAnswerSelection_t3406544577;
// com.beetsoft.GUIElements.SelectableItem
struct SelectableItem_t2941161729;
// Listening1OnplayQuestion
struct Listening1OnplayQuestion_t3534871625;
// Listening2QuestionData
struct Listening2QuestionData_t2443975119;
// Listening2SelectableItem
struct Listening2SelectableItem_t492261174;
// Listening2OnplayQuestion
struct Listening2OnplayQuestion_t3701806074;
// Dialog
struct Dialog_t1378192732;
// DisableWithTimeAudioName
struct DisableWithTimeAudioName_t4015415354;
// ZipSoundItem
struct ZipSoundItem_t558808959;
// SentenceStructureIdiomQuestionData
struct SentenceStructureIdiomQuestionData_t3251732102;
// SentenceStructureIdiomOnPlayQuestion
struct SentenceStructureIdiomOnPlayQuestion_t936886987;
// SentenceListeningSpellingQuestionData
struct SentenceListeningSpellingQuestionData_t3011907248;
// SentenceListeningSpellingOnplayQuestion
struct SentenceListeningSpellingOnplayQuestion_t2514545327;
// OpenHelpScript
struct OpenHelpScript_t3207620564;
// ManageATutorial
struct ManageATutorial_t1512139496;
// OpenHelpScript/TrackIndexObject
struct TrackIndexObject_t1096184091;
// SelectionItem
struct SelectionItem_t610427083;
// SelectItem
struct SelectItem_t2844432199;
// VocabularyQuestionData
struct VocabularyQuestionData_t2021074976;
// VocabularyOnplayQuestion
struct VocabularyOnplayQuestion_t3519225043;
// ScrubberEvents
struct ScrubberEvents_t2429506345;
// GvrEye
struct GvrEye_t3930157106;
// GvrAudioRoom
struct GvrAudioRoom_t1253442178;
// Gvr.Internal.EmulatorConfig
struct EmulatorConfig_t616150261;
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer
struct Pointer_t1211758263;
// GvrBasePointer
struct GvrBasePointer_t2150122635;
// Reporter/Sample
struct Sample_t3185432476;
// Reporter/Log
struct Log_t3604182180;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_LearningData1664811342.h"
#include "AssemblyU2DCSharp_LearningLogData2500481692.h"
#include "AssemblyU2DCSharp_LearningChapterLogData1224380751.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleLoader639004779.h"
#include "AssemblyU2DCSharp_InputSpectrum_Band1120454049.h"
#include "AssemblyU2DCSharp_PostureCharacter580023905.h"
#include "AssemblyU2DCSharp_StateEmotionCharacter3036603695.h"
#include "AssemblyU2DCSharp_SelectButton132497280.h"
#include "AssemblyU2DCSharp_ChapterTopSetting2509872352.h"
#include "AssemblyU2DCSharp_Cell3051913968.h"
#include "AssemblyU2DCSharp_LessonValue1076530945.h"
#include "AssemblyU2DCSharp_GraphValue2739901601.h"
#include "AssemblyU2DCSharp_MyPageChapterView_ViewTextUI4118675828.h"
#include "AssemblyU2DCSharp_MyPageChapterController2166380602.h"
#include "AssemblyU2DCSharp_Column1930583302.h"
#include "AssemblyU2DCSharp_ChapterMyPageScript3593529611.h"
#include "AssemblyU2DCSharp_ConversationSelectionData4090008535.h"
#include "AssemblyU2DCSharp_CommandSheet_Param4123818474.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"
#include "AssemblyU2DCSharp_CharacterSelectionView2239851166.h"
#include "AssemblyU2DCSharp_ConversationSceneSelectionView407867144.h"
#include "AssemblyU2DCSharp_ExcerciseSceneSelectionView3932267486.h"
#include "AssemblyU2DCSharp_ConversationHistoryItemView3846761105.h"
#include "AssemblyU2DCSharp_ChapterItem2155291334.h"
#include "AssemblyU2DCSharp_CachedDragInfo1136705792.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"
#include "AssemblyU2DCSharp_Listening1QuestionData3381069024.h"
#include "AssemblyU2DCSharp_TrueFalseAnswerSelection3406544577.h"
#include "AssemblyU2DCSharp_com_beetsoft_GUIElements_Selecta2941161729.h"
#include "AssemblyU2DCSharp_Listening1OnplayQuestion3534871625.h"
#include "AssemblyU2DCSharp_Listening2QuestionData2443975119.h"
#include "AssemblyU2DCSharp_Listening2SelectableItem492261174.h"
#include "AssemblyU2DCSharp_Listening2OnplayQuestion3701806074.h"
#include "AssemblyU2DCSharp_Dialog1378192732.h"
#include "AssemblyU2DCSharp_DisableWithTimeAudioName4015415354.h"
#include "AssemblyU2DCSharp_ZipSoundItem558808959.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomQuestionDa3251732102.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomOnPlayQuest936886987.h"
#include "AssemblyU2DCSharp_SentenceListeningSpellingQuestio3011907248.h"
#include "AssemblyU2DCSharp_SentenceListeningSpellingOnplayQ2514545327.h"
#include "AssemblyU2DCSharp_OpenHelpScript3207620564.h"
#include "AssemblyU2DCSharp_ManageATutorial1512139496.h"
#include "AssemblyU2DCSharp_OpenHelpScript_TrackIndexObject1096184091.h"
#include "AssemblyU2DCSharp_SelectionItem610427083.h"
#include "AssemblyU2DCSharp_SelectItem2844432199.h"
#include "AssemblyU2DCSharp_VocabularyQuestionData2021074976.h"
#include "AssemblyU2DCSharp_VocabularyOnplayQuestion3519225043.h"
#include "AssemblyU2DCSharp_ScrubberEvents2429506345.h"
#include "AssemblyU2DCSharp_GvrEye3930157106.h"
#include "AssemblyU2DCSharp_GvrAudioRoom1253442178.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig616150261.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_3000685002.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1211758263.h"
#include "AssemblyU2DCSharp_GvrBasePointer2150122635.h"
#include "AssemblyU2DCSharp_Reporter_Sample3185432476.h"
#include "AssemblyU2DCSharp_Reporter_Log3604182180.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Type1530480861.h"

#pragma once
// LearningData[]
struct LearningDataU5BU5D_t142178747  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LearningData_t1664811342 * m_Items[1];

public:
	inline LearningData_t1664811342 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LearningData_t1664811342 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LearningData_t1664811342 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LearningData_t1664811342 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LearningData_t1664811342 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LearningData_t1664811342 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LearningLogData[]
struct LearningLogDataU5BU5D_t3295896437  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LearningLogData_t2500481692 * m_Items[1];

public:
	inline LearningLogData_t2500481692 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LearningLogData_t2500481692 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LearningLogData_t2500481692 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LearningLogData_t2500481692 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LearningLogData_t2500481692 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LearningLogData_t2500481692 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LearningChapterLogData[]
struct LearningChapterLogDataU5BU5D_t2306100374  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LearningChapterLogData_t1224380751 * m_Items[1];

public:
	inline LearningChapterLogData_t1224380751 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LearningChapterLogData_t1224380751 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LearningChapterLogData_t1224380751 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LearningChapterLogData_t1224380751 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LearningChapterLogData_t1224380751 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LearningChapterLogData_t1224380751 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AssetBundles.AssetBundleLoader[]
struct AssetBundleLoaderU5BU5D_t1154736522  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AssetBundleLoader_t639004779 * m_Items[1];

public:
	inline AssetBundleLoader_t639004779 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AssetBundleLoader_t639004779 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AssetBundleLoader_t639004779 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline AssetBundleLoader_t639004779 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AssetBundleLoader_t639004779 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AssetBundleLoader_t639004779 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// InputSpectrum/Band[]
struct BandU5BU5D_t3389715132  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Band_t1120454049  m_Items[1];

public:
	inline Band_t1120454049  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Band_t1120454049 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Band_t1120454049  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Band_t1120454049  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Band_t1120454049 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Band_t1120454049  value)
	{
		m_Items[index] = value;
	}
};
// PostureCharacter[]
struct PostureCharacterU5BU5D_t2028906236  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PostureCharacter_t580023905 * m_Items[1];

public:
	inline PostureCharacter_t580023905 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PostureCharacter_t580023905 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PostureCharacter_t580023905 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PostureCharacter_t580023905 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PostureCharacter_t580023905 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PostureCharacter_t580023905 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// StateEmotionCharacter[]
struct StateEmotionCharacterU5BU5D_t385600822  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StateEmotionCharacter_t3036603695 * m_Items[1];

public:
	inline StateEmotionCharacter_t3036603695 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline StateEmotionCharacter_t3036603695 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, StateEmotionCharacter_t3036603695 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline StateEmotionCharacter_t3036603695 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline StateEmotionCharacter_t3036603695 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, StateEmotionCharacter_t3036603695 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SelectButton[]
struct SelectButtonU5BU5D_t666489473  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SelectButton_t132497280 * m_Items[1];

public:
	inline SelectButton_t132497280 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SelectButton_t132497280 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SelectButton_t132497280 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SelectButton_t132497280 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SelectButton_t132497280 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SelectButton_t132497280 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ChapterTopSetting[]
struct ChapterTopSettingU5BU5D_t3798922145  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ChapterTopSetting_t2509872352 * m_Items[1];

public:
	inline ChapterTopSetting_t2509872352 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ChapterTopSetting_t2509872352 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ChapterTopSetting_t2509872352 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ChapterTopSetting_t2509872352 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ChapterTopSetting_t2509872352 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ChapterTopSetting_t2509872352 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Cell[]
struct CellU5BU5D_t3743671121  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Cell_t3051913968 * m_Items[1];

public:
	inline Cell_t3051913968 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Cell_t3051913968 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Cell_t3051913968 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Cell_t3051913968 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Cell_t3051913968 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Cell_t3051913968 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LessonValue[]
struct LessonValueU5BU5D_t2514548700  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LessonValue_t1076530945 * m_Items[1];

public:
	inline LessonValue_t1076530945 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LessonValue_t1076530945 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LessonValue_t1076530945 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LessonValue_t1076530945 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LessonValue_t1076530945 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LessonValue_t1076530945 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GraphValue[]
struct GraphValueU5BU5D_t3529246652  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GraphValue_t2739901601  m_Items[1];

public:
	inline GraphValue_t2739901601  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GraphValue_t2739901601 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GraphValue_t2739901601  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline GraphValue_t2739901601  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GraphValue_t2739901601 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GraphValue_t2739901601  value)
	{
		m_Items[index] = value;
	}
};
// MyPageChapterView/ViewTextUI[]
struct ViewTextUIU5BU5D_t3674121789  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ViewTextUI_t4118675828  m_Items[1];

public:
	inline ViewTextUI_t4118675828  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ViewTextUI_t4118675828 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ViewTextUI_t4118675828  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ViewTextUI_t4118675828  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ViewTextUI_t4118675828 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ViewTextUI_t4118675828  value)
	{
		m_Items[index] = value;
	}
};
// MyPageChapterController[]
struct MyPageChapterControllerU5BU5D_t1864641951  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MyPageChapterController_t2166380602 * m_Items[1];

public:
	inline MyPageChapterController_t2166380602 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MyPageChapterController_t2166380602 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MyPageChapterController_t2166380602 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MyPageChapterController_t2166380602 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MyPageChapterController_t2166380602 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MyPageChapterController_t2166380602 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Column[]
struct ColumnU5BU5D_t2406440995  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Column_t1930583302 * m_Items[1];

public:
	inline Column_t1930583302 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Column_t1930583302 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Column_t1930583302 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Column_t1930583302 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Column_t1930583302 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Column_t1930583302 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ChapterMyPageScript[]
struct ChapterMyPageScriptU5BU5D_t4161688170  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ChapterMyPageScript_t3593529611 * m_Items[1];

public:
	inline ChapterMyPageScript_t3593529611 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ChapterMyPageScript_t3593529611 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ChapterMyPageScript_t3593529611 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ChapterMyPageScript_t3593529611 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ChapterMyPageScript_t3593529611 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ChapterMyPageScript_t3593529611 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ConversationSelectionData[]
struct ConversationSelectionDataU5BU5D_t1578796782  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConversationSelectionData_t4090008535 * m_Items[1];

public:
	inline ConversationSelectionData_t4090008535 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ConversationSelectionData_t4090008535 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ConversationSelectionData_t4090008535 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ConversationSelectionData_t4090008535 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ConversationSelectionData_t4090008535 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ConversationSelectionData_t4090008535 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CommandSheet/Param[]
struct ParamU5BU5D_t1368809263  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Param_t4123818474 * m_Items[1];

public:
	inline Param_t4123818474 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Param_t4123818474 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Param_t4123818474 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Param_t4123818474 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Param_t4123818474 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Param_t4123818474 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ConversationTalkData[]
struct ConversationTalkDataU5BU5D_t2382916124  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConversationTalkData_t1570298305 * m_Items[1];

public:
	inline ConversationTalkData_t1570298305 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ConversationTalkData_t1570298305 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ConversationTalkData_t1570298305 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ConversationTalkData_t1570298305 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ConversationTalkData_t1570298305 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ConversationTalkData_t1570298305 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IConversationModule[]
struct IConversationModuleU5BU5D_t3489325945  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ConversationCommand[]
struct ConversationCommandU5BU5D_t4180659301  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConversationCommand_t3660105836 * m_Items[1];

public:
	inline ConversationCommand_t3660105836 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ConversationCommand_t3660105836 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ConversationCommand_t3660105836 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ConversationCommand_t3660105836 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ConversationCommand_t3660105836 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ConversationCommand_t3660105836 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ISubtextItem[]
struct ISubtextItemU5BU5D_t2784407766  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CharacterSelectionView[]
struct CharacterSelectionViewU5BU5D_t2172268843  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CharacterSelectionView_t2239851166 * m_Items[1];

public:
	inline CharacterSelectionView_t2239851166 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CharacterSelectionView_t2239851166 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CharacterSelectionView_t2239851166 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CharacterSelectionView_t2239851166 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CharacterSelectionView_t2239851166 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CharacterSelectionView_t2239851166 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ConversationSceneSelectionView[]
struct ConversationSceneSelectionViewU5BU5D_t2992794585  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConversationSceneSelectionView_t407867144 * m_Items[1];

public:
	inline ConversationSceneSelectionView_t407867144 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ConversationSceneSelectionView_t407867144 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ConversationSceneSelectionView_t407867144 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ConversationSceneSelectionView_t407867144 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ConversationSceneSelectionView_t407867144 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ConversationSceneSelectionView_t407867144 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ExcerciseSceneSelectionView[]
struct ExcerciseSceneSelectionViewU5BU5D_t2584062699  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExcerciseSceneSelectionView_t3932267486 * m_Items[1];

public:
	inline ExcerciseSceneSelectionView_t3932267486 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ExcerciseSceneSelectionView_t3932267486 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ExcerciseSceneSelectionView_t3932267486 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ExcerciseSceneSelectionView_t3932267486 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ExcerciseSceneSelectionView_t3932267486 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ExcerciseSceneSelectionView_t3932267486 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ConversationHistoryItemView[]
struct ConversationHistoryItemViewU5BU5D_t929721356  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConversationHistoryItemView_t3846761105 * m_Items[1];

public:
	inline ConversationHistoryItemView_t3846761105 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ConversationHistoryItemView_t3846761105 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ConversationHistoryItemView_t3846761105 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ConversationHistoryItemView_t3846761105 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ConversationHistoryItemView_t3846761105 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ConversationHistoryItemView_t3846761105 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ChapterItem[]
struct ChapterItemU5BU5D_t3880258403  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ChapterItem_t2155291334 * m_Items[1];

public:
	inline ChapterItem_t2155291334 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ChapterItem_t2155291334 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ChapterItem_t2155291334 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ChapterItem_t2155291334 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ChapterItem_t2155291334 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ChapterItem_t2155291334 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CachedDragInfo[]
struct CachedDragInfoU5BU5D_t1778850561  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CachedDragInfo_t1136705792 * m_Items[1];

public:
	inline CachedDragInfo_t1136705792 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CachedDragInfo_t1136705792 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CachedDragInfo_t1136705792 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CachedDragInfo_t1136705792 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CachedDragInfo_t1136705792 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CachedDragInfo_t1136705792 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Beetsoft.VAE.Purchaser/PurchaseSession[]
struct PurchaseSessionU5BU5D_t64753102  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PurchaseSession_t2088054391  m_Items[1];

public:
	inline PurchaseSession_t2088054391  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PurchaseSession_t2088054391 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PurchaseSession_t2088054391  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PurchaseSession_t2088054391  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PurchaseSession_t2088054391 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PurchaseSession_t2088054391  value)
	{
		m_Items[index] = value;
	}
};
// Listening1QuestionData[]
struct Listening1QuestionDataU5BU5D_t441904033  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Listening1QuestionData_t3381069024 * m_Items[1];

public:
	inline Listening1QuestionData_t3381069024 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Listening1QuestionData_t3381069024 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Listening1QuestionData_t3381069024 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Listening1QuestionData_t3381069024 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Listening1QuestionData_t3381069024 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Listening1QuestionData_t3381069024 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TrueFalseAnswerSelection[]
struct TrueFalseAnswerSelectionU5BU5D_t2252099868  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TrueFalseAnswerSelection_t3406544577 * m_Items[1];

public:
	inline TrueFalseAnswerSelection_t3406544577 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TrueFalseAnswerSelection_t3406544577 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TrueFalseAnswerSelection_t3406544577 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline TrueFalseAnswerSelection_t3406544577 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TrueFalseAnswerSelection_t3406544577 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TrueFalseAnswerSelection_t3406544577 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// com.beetsoft.GUIElements.SelectableItem[]
struct SelectableItemU5BU5D_t1727180252  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SelectableItem_t2941161729 * m_Items[1];

public:
	inline SelectableItem_t2941161729 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SelectableItem_t2941161729 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SelectableItem_t2941161729 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SelectableItem_t2941161729 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SelectableItem_t2941161729 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SelectableItem_t2941161729 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Listening1OnplayQuestion[]
struct Listening1OnplayQuestionU5BU5D_t3872892020  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Listening1OnplayQuestion_t3534871625 * m_Items[1];

public:
	inline Listening1OnplayQuestion_t3534871625 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Listening1OnplayQuestion_t3534871625 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Listening1OnplayQuestion_t3534871625 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Listening1OnplayQuestion_t3534871625 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Listening1OnplayQuestion_t3534871625 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Listening1OnplayQuestion_t3534871625 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Listening2QuestionData[]
struct Listening2QuestionDataU5BU5D_t1588924950  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Listening2QuestionData_t2443975119 * m_Items[1];

public:
	inline Listening2QuestionData_t2443975119 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Listening2QuestionData_t2443975119 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Listening2QuestionData_t2443975119 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Listening2QuestionData_t2443975119 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Listening2QuestionData_t2443975119 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Listening2QuestionData_t2443975119 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Listening2SelectableItem[]
struct Listening2SelectableItemU5BU5D_t2626481971  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Listening2SelectableItem_t492261174 * m_Items[1];

public:
	inline Listening2SelectableItem_t492261174 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Listening2SelectableItem_t492261174 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Listening2SelectableItem_t492261174 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Listening2SelectableItem_t492261174 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Listening2SelectableItem_t492261174 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Listening2SelectableItem_t492261174 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Listening2OnplayQuestion[]
struct Listening2OnplayQuestionU5BU5D_t302009055  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Listening2OnplayQuestion_t3701806074 * m_Items[1];

public:
	inline Listening2OnplayQuestion_t3701806074 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Listening2OnplayQuestion_t3701806074 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Listening2OnplayQuestion_t3701806074 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Listening2OnplayQuestion_t3701806074 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Listening2OnplayQuestion_t3701806074 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Listening2OnplayQuestion_t3701806074 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Dialog[]
struct DialogU5BU5D_t627844533  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Dialog_t1378192732 * m_Items[1];

public:
	inline Dialog_t1378192732 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Dialog_t1378192732 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Dialog_t1378192732 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Dialog_t1378192732 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Dialog_t1378192732 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Dialog_t1378192732 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DisableWithTimeAudioName[]
struct DisableWithTimeAudioNameU5BU5D_t3094152607  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DisableWithTimeAudioName_t4015415354 * m_Items[1];

public:
	inline DisableWithTimeAudioName_t4015415354 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DisableWithTimeAudioName_t4015415354 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DisableWithTimeAudioName_t4015415354 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline DisableWithTimeAudioName_t4015415354 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DisableWithTimeAudioName_t4015415354 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DisableWithTimeAudioName_t4015415354 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZipSoundItem[]
struct ZipSoundItemU5BU5D_t965881766  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ZipSoundItem_t558808959 * m_Items[1];

public:
	inline ZipSoundItem_t558808959 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ZipSoundItem_t558808959 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ZipSoundItem_t558808959 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ZipSoundItem_t558808959 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ZipSoundItem_t558808959 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ZipSoundItem_t558808959 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SentenceStructureIdiomQuestionData[]
struct SentenceStructureIdiomQuestionDataU5BU5D_t2535721123  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SentenceStructureIdiomQuestionData_t3251732102 * m_Items[1];

public:
	inline SentenceStructureIdiomQuestionData_t3251732102 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SentenceStructureIdiomQuestionData_t3251732102 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SentenceStructureIdiomQuestionData_t3251732102 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SentenceStructureIdiomQuestionData_t3251732102 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SentenceStructureIdiomQuestionData_t3251732102 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SentenceStructureIdiomQuestionData_t3251732102 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SentenceStructureIdiomOnPlayQuestion[]
struct SentenceStructureIdiomOnPlayQuestionU5BU5D_t3172342186  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SentenceStructureIdiomOnPlayQuestion_t936886987 * m_Items[1];

public:
	inline SentenceStructureIdiomOnPlayQuestion_t936886987 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SentenceStructureIdiomOnPlayQuestion_t936886987 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SentenceStructureIdiomOnPlayQuestion_t936886987 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SentenceStructureIdiomOnPlayQuestion_t936886987 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SentenceStructureIdiomOnPlayQuestion_t936886987 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SentenceStructureIdiomOnPlayQuestion_t936886987 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SentenceListeningSpellingQuestionData[]
struct SentenceListeningSpellingQuestionDataU5BU5D_t14282897  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SentenceListeningSpellingQuestionData_t3011907248 * m_Items[1];

public:
	inline SentenceListeningSpellingQuestionData_t3011907248 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SentenceListeningSpellingQuestionData_t3011907248 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SentenceListeningSpellingQuestionData_t3011907248 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SentenceListeningSpellingQuestionData_t3011907248 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SentenceListeningSpellingQuestionData_t3011907248 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SentenceListeningSpellingQuestionData_t3011907248 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SentenceListeningSpellingOnplayQuestion[]
struct SentenceListeningSpellingOnplayQuestionU5BU5D_t3738272182  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SentenceListeningSpellingOnplayQuestion_t2514545327 * m_Items[1];

public:
	inline SentenceListeningSpellingOnplayQuestion_t2514545327 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SentenceListeningSpellingOnplayQuestion_t2514545327 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SentenceListeningSpellingOnplayQuestion_t2514545327 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SentenceListeningSpellingOnplayQuestion_t2514545327 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SentenceListeningSpellingOnplayQuestion_t2514545327 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SentenceListeningSpellingOnplayQuestion_t2514545327 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OpenHelpScript[]
struct OpenHelpScriptU5BU5D_t3307534941  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OpenHelpScript_t3207620564 * m_Items[1];

public:
	inline OpenHelpScript_t3207620564 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline OpenHelpScript_t3207620564 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, OpenHelpScript_t3207620564 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline OpenHelpScript_t3207620564 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline OpenHelpScript_t3207620564 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, OpenHelpScript_t3207620564 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ManageATutorial[]
struct ManageATutorialU5BU5D_t114115705  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ManageATutorial_t1512139496 * m_Items[1];

public:
	inline ManageATutorial_t1512139496 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ManageATutorial_t1512139496 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ManageATutorial_t1512139496 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ManageATutorial_t1512139496 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ManageATutorial_t1512139496 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ManageATutorial_t1512139496 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OpenHelpScript/TrackIndexObject[]
struct TrackIndexObjectU5BU5D_t14931994  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TrackIndexObject_t1096184091 * m_Items[1];

public:
	inline TrackIndexObject_t1096184091 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TrackIndexObject_t1096184091 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TrackIndexObject_t1096184091 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline TrackIndexObject_t1096184091 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TrackIndexObject_t1096184091 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TrackIndexObject_t1096184091 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SelectionItem[]
struct SelectionItemU5BU5D_t463946666  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SelectionItem_t610427083 * m_Items[1];

public:
	inline SelectionItem_t610427083 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SelectionItem_t610427083 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SelectionItem_t610427083 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SelectionItem_t610427083 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SelectionItem_t610427083 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SelectionItem_t610427083 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SelectItem[]
struct SelectItemU5BU5D_t1017141182  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SelectItem_t2844432199 * m_Items[1];

public:
	inline SelectItem_t2844432199 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SelectItem_t2844432199 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SelectItem_t2844432199 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SelectItem_t2844432199 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SelectItem_t2844432199 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SelectItem_t2844432199 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// VocabularyQuestionData[]
struct VocabularyQuestionDataU5BU5D_t613651297  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VocabularyQuestionData_t2021074976 * m_Items[1];

public:
	inline VocabularyQuestionData_t2021074976 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline VocabularyQuestionData_t2021074976 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, VocabularyQuestionData_t2021074976 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline VocabularyQuestionData_t2021074976 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline VocabularyQuestionData_t2021074976 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, VocabularyQuestionData_t2021074976 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// VocabularyOnplayQuestion[]
struct VocabularyOnplayQuestionU5BU5D_t607465090  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VocabularyOnplayQuestion_t3519225043 * m_Items[1];

public:
	inline VocabularyOnplayQuestion_t3519225043 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline VocabularyOnplayQuestion_t3519225043 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, VocabularyOnplayQuestion_t3519225043 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline VocabularyOnplayQuestion_t3519225043 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline VocabularyOnplayQuestion_t3519225043 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, VocabularyOnplayQuestion_t3519225043 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ScrubberEvents[]
struct ScrubberEventsU5BU5D_t3322287636  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ScrubberEvents_t2429506345 * m_Items[1];

public:
	inline ScrubberEvents_t2429506345 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ScrubberEvents_t2429506345 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ScrubberEvents_t2429506345 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ScrubberEvents_t2429506345 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ScrubberEvents_t2429506345 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ScrubberEvents_t2429506345 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GvrEye[]
struct GvrEyeU5BU5D_t3620642503  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GvrEye_t3930157106 * m_Items[1];

public:
	inline GvrEye_t3930157106 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GvrEye_t3930157106 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GvrEye_t3930157106 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GvrEye_t3930157106 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GvrEye_t3930157106 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GvrEye_t3930157106 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GvrAudioRoom[]
struct GvrAudioRoomU5BU5D_t3258870071  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GvrAudioRoom_t1253442178 * m_Items[1];

public:
	inline GvrAudioRoom_t1253442178 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GvrAudioRoom_t1253442178 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GvrAudioRoom_t1253442178 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GvrAudioRoom_t1253442178 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GvrAudioRoom_t1253442178 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GvrAudioRoom_t1253442178 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Gvr.Internal.EmulatorConfig[]
struct EmulatorConfigU5BU5D_t1923274648  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EmulatorConfig_t616150261 * m_Items[1];

public:
	inline EmulatorConfig_t616150261 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline EmulatorConfig_t616150261 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, EmulatorConfig_t616150261 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline EmulatorConfig_t616150261 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline EmulatorConfig_t616150261 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, EmulatorConfig_t616150261 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Gvr.Internal.EmulatorTouchEvent/Pointer[]
struct PointerU5BU5D_t1899449295  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Pointer_t3000685002  m_Items[1];

public:
	inline Pointer_t3000685002  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Pointer_t3000685002 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Pointer_t3000685002  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Pointer_t3000685002  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Pointer_t3000685002 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Pointer_t3000685002  value)
	{
		m_Items[index] = value;
	}
};
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer[]
struct PointerU5BU5D_t1785453710  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Pointer_t1211758263 * m_Items[1];

public:
	inline Pointer_t1211758263 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Pointer_t1211758263 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Pointer_t1211758263 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Pointer_t1211758263 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Pointer_t1211758263 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Pointer_t1211758263 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GvrBasePointer[]
struct GvrBasePointerU5BU5D_t1099995370  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GvrBasePointer_t2150122635 * m_Items[1];

public:
	inline GvrBasePointer_t2150122635 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GvrBasePointer_t2150122635 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GvrBasePointer_t2150122635 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GvrBasePointer_t2150122635 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GvrBasePointer_t2150122635 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GvrBasePointer_t2150122635 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Reporter/Sample[]
struct SampleU5BU5D_t1838164597  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Sample_t3185432476 * m_Items[1];

public:
	inline Sample_t3185432476 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sample_t3185432476 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sample_t3185432476 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sample_t3185432476 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sample_t3185432476 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sample_t3185432476 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Reporter/Log[]
struct LogU5BU5D_t1859948621  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Log_t3604182180 * m_Items[1];

public:
	inline Log_t3604182180 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Log_t3604182180 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Log_t3604182180 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Log_t3604182180 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Log_t3604182180 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Log_t3604182180 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// proto.PhoneEvent/Types/Type[]
struct TypeU5BU5D_t2649698064  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
