﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence2QuestionView/<FakeTextPointer>c__Iterator0
struct U3CFakeTextPointerU3Ec__Iterator0_t2012533373;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Sentence2QuestionView/<FakeTextPointer>c__Iterator0::.ctor()
extern "C"  void U3CFakeTextPointerU3Ec__Iterator0__ctor_m3043234838 (U3CFakeTextPointerU3Ec__Iterator0_t2012533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Sentence2QuestionView/<FakeTextPointer>c__Iterator0::MoveNext()
extern "C"  bool U3CFakeTextPointerU3Ec__Iterator0_MoveNext_m3755658078 (U3CFakeTextPointerU3Ec__Iterator0_t2012533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Sentence2QuestionView/<FakeTextPointer>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFakeTextPointerU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3615321528 (U3CFakeTextPointerU3Ec__Iterator0_t2012533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Sentence2QuestionView/<FakeTextPointer>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFakeTextPointerU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3556244240 (U3CFakeTextPointerU3Ec__Iterator0_t2012533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView/<FakeTextPointer>c__Iterator0::Dispose()
extern "C"  void U3CFakeTextPointerU3Ec__Iterator0_Dispose_m1346392559 (U3CFakeTextPointerU3Ec__Iterator0_t2012533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView/<FakeTextPointer>c__Iterator0::Reset()
extern "C"  void U3CFakeTextPointerU3Ec__Iterator0_Reset_m1355477153 (U3CFakeTextPointerU3Ec__Iterator0_t2012533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
