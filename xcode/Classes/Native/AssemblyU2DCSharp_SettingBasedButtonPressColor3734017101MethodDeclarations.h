﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedButtonPressColor
struct SettingBasedButtonPressColor_t3734017101;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedButtonPressColor::.ctor()
extern "C"  void SettingBasedButtonPressColor__ctor_m1252132540 (SettingBasedButtonPressColor_t3734017101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedButtonPressColor::Start()
extern "C"  void SettingBasedButtonPressColor_Start_m538550988 (SettingBasedButtonPressColor_t3734017101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
