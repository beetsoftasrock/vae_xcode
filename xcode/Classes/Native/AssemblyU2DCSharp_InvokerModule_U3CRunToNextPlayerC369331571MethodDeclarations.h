﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InvokerModule/<RunToNextPlayerCommand>c__Iterator0
struct U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void InvokerModule/<RunToNextPlayerCommand>c__Iterator0::.ctor()
extern "C"  void U3CRunToNextPlayerCommandU3Ec__Iterator0__ctor_m1627892888 (U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InvokerModule/<RunToNextPlayerCommand>c__Iterator0::MoveNext()
extern "C"  bool U3CRunToNextPlayerCommandU3Ec__Iterator0_MoveNext_m2070022744 (U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InvokerModule/<RunToNextPlayerCommand>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRunToNextPlayerCommandU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m687993904 (U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InvokerModule/<RunToNextPlayerCommand>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRunToNextPlayerCommandU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m911913272 (U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule/<RunToNextPlayerCommand>c__Iterator0::Dispose()
extern "C"  void U3CRunToNextPlayerCommandU3Ec__Iterator0_Dispose_m386134665 (U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InvokerModule/<RunToNextPlayerCommand>c__Iterator0::Reset()
extern "C"  void U3CRunToNextPlayerCommandU3Ec__Iterator0_Reset_m1808341455 (U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
