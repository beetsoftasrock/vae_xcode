﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectCommand
struct EffectCommand_t708799566;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationLogicController
struct ConversationLogicController_t3911886281;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ConversationLogicController3911886281.h"

// System.Void EffectCommand::.ctor(System.String,System.String)
extern "C"  void EffectCommand__ctor_m936215363 (EffectCommand_t708799566 * __this, String_t* ___role0, String_t* ___imageName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EffectCommand::AutoExecute(ConversationLogicController)
extern "C"  Il2CppObject * EffectCommand_AutoExecute_m3219946870 (EffectCommand_t708799566 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EffectCommand::GetRole()
extern "C"  String_t* EffectCommand_GetRole_m1795328412 (EffectCommand_t708799566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EffectCommand::Execute(ConversationLogicController)
extern "C"  Il2CppObject * EffectCommand_Execute_m3634834965 (EffectCommand_t708799566 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCommand::Revert(ConversationLogicController)
extern "C"  void EffectCommand_Revert_m1878053166 (EffectCommand_t708799566 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
