﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuestionScenePopup/<PlayAnimation>c__Iterator0
struct U3CPlayAnimationU3Ec__Iterator0_t1105760000;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void QuestionScenePopup/<PlayAnimation>c__Iterator0::.ctor()
extern "C"  void U3CPlayAnimationU3Ec__Iterator0__ctor_m3805582623 (U3CPlayAnimationU3Ec__Iterator0_t1105760000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean QuestionScenePopup/<PlayAnimation>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayAnimationU3Ec__Iterator0_MoveNext_m2795951465 (U3CPlayAnimationU3Ec__Iterator0_t1105760000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object QuestionScenePopup/<PlayAnimation>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayAnimationU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1851294565 (U3CPlayAnimationU3Ec__Iterator0_t1105760000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object QuestionScenePopup/<PlayAnimation>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayAnimationU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2869770589 (U3CPlayAnimationU3Ec__Iterator0_t1105760000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionScenePopup/<PlayAnimation>c__Iterator0::Dispose()
extern "C"  void U3CPlayAnimationU3Ec__Iterator0_Dispose_m1035464510 (U3CPlayAnimationU3Ec__Iterator0_t1105760000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionScenePopup/<PlayAnimation>c__Iterator0::Reset()
extern "C"  void U3CPlayAnimationU3Ec__Iterator0_Reset_m2976572568 (U3CPlayAnimationU3Ec__Iterator0_t1105760000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
