﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AssetBundles.AssetBundleManager
struct AssetBundleManager_t364944953;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// UnityEngine.AssetBundleManifest
struct AssetBundleManifest_t1328741589;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<UnityEngine.AssetBundle>
struct List_1_t1424099886;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager
struct  AssetBundleManager_t364944953  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AssetBundle AssetBundles.AssetBundleManager::bundleOfManifest
	AssetBundle_t2054978754 * ___bundleOfManifest_4;
	// UnityEngine.AssetBundleManifest AssetBundles.AssetBundleManager::assetManifest
	AssetBundleManifest_t1328741589 * ___assetManifest_5;
	// System.String AssetBundles.AssetBundleManager::assetBundleUrl
	String_t* ___assetBundleUrl_6;
	// System.Collections.Generic.List`1<System.String> AssetBundles.AssetBundleManager::bundleVariants
	List_1_t1398341365 * ___bundleVariants_7;
	// System.Collections.Generic.List`1<UnityEngine.AssetBundle> AssetBundles.AssetBundleManager::loadedBundles
	List_1_t1424099886 * ___loadedBundles_8;

public:
	inline static int32_t get_offset_of_bundleOfManifest_4() { return static_cast<int32_t>(offsetof(AssetBundleManager_t364944953, ___bundleOfManifest_4)); }
	inline AssetBundle_t2054978754 * get_bundleOfManifest_4() const { return ___bundleOfManifest_4; }
	inline AssetBundle_t2054978754 ** get_address_of_bundleOfManifest_4() { return &___bundleOfManifest_4; }
	inline void set_bundleOfManifest_4(AssetBundle_t2054978754 * value)
	{
		___bundleOfManifest_4 = value;
		Il2CppCodeGenWriteBarrier(&___bundleOfManifest_4, value);
	}

	inline static int32_t get_offset_of_assetManifest_5() { return static_cast<int32_t>(offsetof(AssetBundleManager_t364944953, ___assetManifest_5)); }
	inline AssetBundleManifest_t1328741589 * get_assetManifest_5() const { return ___assetManifest_5; }
	inline AssetBundleManifest_t1328741589 ** get_address_of_assetManifest_5() { return &___assetManifest_5; }
	inline void set_assetManifest_5(AssetBundleManifest_t1328741589 * value)
	{
		___assetManifest_5 = value;
		Il2CppCodeGenWriteBarrier(&___assetManifest_5, value);
	}

	inline static int32_t get_offset_of_assetBundleUrl_6() { return static_cast<int32_t>(offsetof(AssetBundleManager_t364944953, ___assetBundleUrl_6)); }
	inline String_t* get_assetBundleUrl_6() const { return ___assetBundleUrl_6; }
	inline String_t** get_address_of_assetBundleUrl_6() { return &___assetBundleUrl_6; }
	inline void set_assetBundleUrl_6(String_t* value)
	{
		___assetBundleUrl_6 = value;
		Il2CppCodeGenWriteBarrier(&___assetBundleUrl_6, value);
	}

	inline static int32_t get_offset_of_bundleVariants_7() { return static_cast<int32_t>(offsetof(AssetBundleManager_t364944953, ___bundleVariants_7)); }
	inline List_1_t1398341365 * get_bundleVariants_7() const { return ___bundleVariants_7; }
	inline List_1_t1398341365 ** get_address_of_bundleVariants_7() { return &___bundleVariants_7; }
	inline void set_bundleVariants_7(List_1_t1398341365 * value)
	{
		___bundleVariants_7 = value;
		Il2CppCodeGenWriteBarrier(&___bundleVariants_7, value);
	}

	inline static int32_t get_offset_of_loadedBundles_8() { return static_cast<int32_t>(offsetof(AssetBundleManager_t364944953, ___loadedBundles_8)); }
	inline List_1_t1424099886 * get_loadedBundles_8() const { return ___loadedBundles_8; }
	inline List_1_t1424099886 ** get_address_of_loadedBundles_8() { return &___loadedBundles_8; }
	inline void set_loadedBundles_8(List_1_t1424099886 * value)
	{
		___loadedBundles_8 = value;
		Il2CppCodeGenWriteBarrier(&___loadedBundles_8, value);
	}
};

struct AssetBundleManager_t364944953_StaticFields
{
public:
	// AssetBundles.AssetBundleManager AssetBundles.AssetBundleManager::instance
	AssetBundleManager_t364944953 * ___instance_2;
	// System.String[] AssetBundles.AssetBundleManager::<AllAssetBundles>k__BackingField
	StringU5BU5D_t1642385972* ___U3CAllAssetBundlesU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(AssetBundleManager_t364944953_StaticFields, ___instance_2)); }
	inline AssetBundleManager_t364944953 * get_instance_2() const { return ___instance_2; }
	inline AssetBundleManager_t364944953 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AssetBundleManager_t364944953 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_U3CAllAssetBundlesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AssetBundleManager_t364944953_StaticFields, ___U3CAllAssetBundlesU3Ek__BackingField_3)); }
	inline StringU5BU5D_t1642385972* get_U3CAllAssetBundlesU3Ek__BackingField_3() const { return ___U3CAllAssetBundlesU3Ek__BackingField_3; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CAllAssetBundlesU3Ek__BackingField_3() { return &___U3CAllAssetBundlesU3Ek__BackingField_3; }
	inline void set_U3CAllAssetBundlesU3Ek__BackingField_3(StringU5BU5D_t1642385972* value)
	{
		___U3CAllAssetBundlesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllAssetBundlesU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
