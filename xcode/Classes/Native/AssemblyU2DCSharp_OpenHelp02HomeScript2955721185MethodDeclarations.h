﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelp02HomeScript
struct OpenHelp02HomeScript_t2955721185;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenHelp02HomeScript::.ctor()
extern "C"  void OpenHelp02HomeScript__ctor_m1010550400 (OpenHelp02HomeScript_t2955721185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp02HomeScript::Awake()
extern "C"  void OpenHelp02HomeScript_Awake_m1639271485 (OpenHelp02HomeScript_t2955721185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp02HomeScript::Start()
extern "C"  void OpenHelp02HomeScript_Start_m2450670688 (OpenHelp02HomeScript_t2955721185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp02HomeScript::ShowMe()
extern "C"  void OpenHelp02HomeScript_ShowMe_m3328045067 (OpenHelp02HomeScript_t2955721185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp02HomeScript::OnEnable()
extern "C"  void OpenHelp02HomeScript_OnEnable_m2624749344 (OpenHelp02HomeScript_t2955721185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp02HomeScript::ResetObjectsBeFront()
extern "C"  void OpenHelp02HomeScript_ResetObjectsBeFront_m572378801 (OpenHelp02HomeScript_t2955721185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
