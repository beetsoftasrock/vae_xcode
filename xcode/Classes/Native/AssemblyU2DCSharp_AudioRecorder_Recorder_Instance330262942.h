﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InputSpectrum
struct InputSpectrum_t3811795451;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioRecorder.Recorder_Instance
struct  Recorder_Instance_t330262942  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 AudioRecorder.Recorder_Instance::<Frequency>k__BackingField
	int32_t ___U3CFrequencyU3Ek__BackingField_2;
	// InputSpectrum AudioRecorder.Recorder_Instance::inputSpectrum
	InputSpectrum_t3811795451 * ___inputSpectrum_3;
	// UnityEngine.AudioSource AudioRecorder.Recorder_Instance::audioSource
	AudioSource_t1135106623 * ___audioSource_4;
	// System.Single[] AudioRecorder.Recorder_Instance::samples
	SingleU5BU5D_t577127397* ___samples_5;

public:
	inline static int32_t get_offset_of_U3CFrequencyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Recorder_Instance_t330262942, ___U3CFrequencyU3Ek__BackingField_2)); }
	inline int32_t get_U3CFrequencyU3Ek__BackingField_2() const { return ___U3CFrequencyU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CFrequencyU3Ek__BackingField_2() { return &___U3CFrequencyU3Ek__BackingField_2; }
	inline void set_U3CFrequencyU3Ek__BackingField_2(int32_t value)
	{
		___U3CFrequencyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_inputSpectrum_3() { return static_cast<int32_t>(offsetof(Recorder_Instance_t330262942, ___inputSpectrum_3)); }
	inline InputSpectrum_t3811795451 * get_inputSpectrum_3() const { return ___inputSpectrum_3; }
	inline InputSpectrum_t3811795451 ** get_address_of_inputSpectrum_3() { return &___inputSpectrum_3; }
	inline void set_inputSpectrum_3(InputSpectrum_t3811795451 * value)
	{
		___inputSpectrum_3 = value;
		Il2CppCodeGenWriteBarrier(&___inputSpectrum_3, value);
	}

	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(Recorder_Instance_t330262942, ___audioSource_4)); }
	inline AudioSource_t1135106623 * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t1135106623 * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_4, value);
	}

	inline static int32_t get_offset_of_samples_5() { return static_cast<int32_t>(offsetof(Recorder_Instance_t330262942, ___samples_5)); }
	inline SingleU5BU5D_t577127397* get_samples_5() const { return ___samples_5; }
	inline SingleU5BU5D_t577127397** get_address_of_samples_5() { return &___samples_5; }
	inline void set_samples_5(SingleU5BU5D_t577127397* value)
	{
		___samples_5 = value;
		Il2CppCodeGenWriteBarrier(&___samples_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
