﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditUserNameScript
struct  EditUserNameScript_t1012132347  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform EditUserNameScript::popupEdit
	Transform_t3275118058 * ___popupEdit_8;
	// UnityEngine.UI.Text EditUserNameScript::txtUserName
	Text_t356221433 * ___txtUserName_9;
	// UnityEngine.UI.Text EditUserNameScript::txtID
	Text_t356221433 * ___txtID_10;
	// UnityEngine.UI.Text EditUserNameScript::txtResult
	Text_t356221433 * ___txtResult_11;
	// UnityEngine.UI.InputField EditUserNameScript::inputField
	InputField_t1631627530 * ___inputField_12;
	// System.String EditUserNameScript::username
	String_t* ___username_13;

public:
	inline static int32_t get_offset_of_popupEdit_8() { return static_cast<int32_t>(offsetof(EditUserNameScript_t1012132347, ___popupEdit_8)); }
	inline Transform_t3275118058 * get_popupEdit_8() const { return ___popupEdit_8; }
	inline Transform_t3275118058 ** get_address_of_popupEdit_8() { return &___popupEdit_8; }
	inline void set_popupEdit_8(Transform_t3275118058 * value)
	{
		___popupEdit_8 = value;
		Il2CppCodeGenWriteBarrier(&___popupEdit_8, value);
	}

	inline static int32_t get_offset_of_txtUserName_9() { return static_cast<int32_t>(offsetof(EditUserNameScript_t1012132347, ___txtUserName_9)); }
	inline Text_t356221433 * get_txtUserName_9() const { return ___txtUserName_9; }
	inline Text_t356221433 ** get_address_of_txtUserName_9() { return &___txtUserName_9; }
	inline void set_txtUserName_9(Text_t356221433 * value)
	{
		___txtUserName_9 = value;
		Il2CppCodeGenWriteBarrier(&___txtUserName_9, value);
	}

	inline static int32_t get_offset_of_txtID_10() { return static_cast<int32_t>(offsetof(EditUserNameScript_t1012132347, ___txtID_10)); }
	inline Text_t356221433 * get_txtID_10() const { return ___txtID_10; }
	inline Text_t356221433 ** get_address_of_txtID_10() { return &___txtID_10; }
	inline void set_txtID_10(Text_t356221433 * value)
	{
		___txtID_10 = value;
		Il2CppCodeGenWriteBarrier(&___txtID_10, value);
	}

	inline static int32_t get_offset_of_txtResult_11() { return static_cast<int32_t>(offsetof(EditUserNameScript_t1012132347, ___txtResult_11)); }
	inline Text_t356221433 * get_txtResult_11() const { return ___txtResult_11; }
	inline Text_t356221433 ** get_address_of_txtResult_11() { return &___txtResult_11; }
	inline void set_txtResult_11(Text_t356221433 * value)
	{
		___txtResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___txtResult_11, value);
	}

	inline static int32_t get_offset_of_inputField_12() { return static_cast<int32_t>(offsetof(EditUserNameScript_t1012132347, ___inputField_12)); }
	inline InputField_t1631627530 * get_inputField_12() const { return ___inputField_12; }
	inline InputField_t1631627530 ** get_address_of_inputField_12() { return &___inputField_12; }
	inline void set_inputField_12(InputField_t1631627530 * value)
	{
		___inputField_12 = value;
		Il2CppCodeGenWriteBarrier(&___inputField_12, value);
	}

	inline static int32_t get_offset_of_username_13() { return static_cast<int32_t>(offsetof(EditUserNameScript_t1012132347, ___username_13)); }
	inline String_t* get_username_13() const { return ___username_13; }
	inline String_t** get_address_of_username_13() { return &___username_13; }
	inline void set_username_13(String_t* value)
	{
		___username_13 = value;
		Il2CppCodeGenWriteBarrier(&___username_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
