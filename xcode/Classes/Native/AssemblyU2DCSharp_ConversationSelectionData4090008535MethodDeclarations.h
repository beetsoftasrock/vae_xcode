﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionData
struct ConversationSelectionData_t4090008535;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationSelectionData::.ctor()
extern "C"  void ConversationSelectionData__ctor_m1765771962 (ConversationSelectionData_t4090008535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
