﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioRecorder.Recorder_Instance/<playRecordedClip>c__Iterator0
struct U3CplayRecordedClipU3Ec__Iterator0_t3147911398;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AudioRecorder.Recorder_Instance/<playRecordedClip>c__Iterator0::.ctor()
extern "C"  void U3CplayRecordedClipU3Ec__Iterator0__ctor_m3441226691 (U3CplayRecordedClipU3Ec__Iterator0_t3147911398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioRecorder.Recorder_Instance/<playRecordedClip>c__Iterator0::MoveNext()
extern "C"  bool U3CplayRecordedClipU3Ec__Iterator0_MoveNext_m3338322345 (U3CplayRecordedClipU3Ec__Iterator0_t3147911398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AudioRecorder.Recorder_Instance/<playRecordedClip>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CplayRecordedClipU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3961065229 (U3CplayRecordedClipU3Ec__Iterator0_t3147911398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AudioRecorder.Recorder_Instance/<playRecordedClip>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CplayRecordedClipU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2982652421 (U3CplayRecordedClipU3Ec__Iterator0_t3147911398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder_Instance/<playRecordedClip>c__Iterator0::Dispose()
extern "C"  void U3CplayRecordedClipU3Ec__Iterator0_Dispose_m1005736676 (U3CplayRecordedClipU3Ec__Iterator0_t3147911398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder_Instance/<playRecordedClip>c__Iterator0::Reset()
extern "C"  void U3CplayRecordedClipU3Ec__Iterator0_Reset_m1752835290 (U3CplayRecordedClipU3Ec__Iterator0_t3147911398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
