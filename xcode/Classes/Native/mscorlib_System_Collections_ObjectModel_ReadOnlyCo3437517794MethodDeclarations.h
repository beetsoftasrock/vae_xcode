﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m1135180574(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3671019970_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1070733466(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, SentenceStructureIdiomQuestionData_t3251732102 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2989589458_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4267432646(__this, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m454937302_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3353831005(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, SentenceStructureIdiomQuestionData_t3251732102 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4272763307_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2424711149(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3437517794 *, SentenceStructureIdiomQuestionData_t3251732102 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3199809075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m855638145(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m962041751_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3822215365(__this, ___index0, method) ((  SentenceStructureIdiomQuestionData_t3251732102 * (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m70085287_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m933452120(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, SentenceStructureIdiomQuestionData_t3251732102 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1547026160_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2501247080(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4041967064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1333025427(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3664791405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3475274476(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m531171980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1887726571(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3437517794 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3780136817_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1937254379(__this, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3983677501_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2200454411(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3437517794 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1990607517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1010246905(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3437517794 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m606942423_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m550456246(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m691705570_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3210808376(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3182494192_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3379279396(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m572840272_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m570136003(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2871048729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2748753247(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m769863805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3943723394(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m942145650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2678948839(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1367736517_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2484652140(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3336878134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1304340629(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799572719_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1649872028(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3437517794 *, SentenceStructureIdiomQuestionData_t3251732102 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1227826160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1252504414(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3437517794 *, SentenceStructureIdiomQuestionDataU5BU5D_t2535721123*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4257276542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m2818287251(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1627519329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m2374836360(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3437517794 *, SentenceStructureIdiomQuestionData_t3251732102 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1981423404_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::get_Count()
#define ReadOnlyCollection_1_get_Count_m120100763(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3437517794 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SentenceStructureIdiomQuestionData>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m651693513(__this, ___index0, method) ((  SentenceStructureIdiomQuestionData_t3251732102 * (*) (ReadOnlyCollection_1_t3437517794 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m191392387_gshared)(__this, ___index0, method)
