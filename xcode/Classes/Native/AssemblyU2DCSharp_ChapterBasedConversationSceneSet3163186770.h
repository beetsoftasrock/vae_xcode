﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// BaseConversationCommandLoader
struct BaseConversationCommandLoader_t1091304408;
// BaseConversationController
struct BaseConversationController_t808907754;

#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterBasedConversationSceneSetup
struct  ChapterBasedConversationSceneSetup_t3163186770  : public GlobalBasedSetupModule_t1655890765
{
public:
	// UnityEngine.Sprite ChapterBasedConversationSceneSetup::defaultBackgroundSprite
	Sprite_t309593783 * ___defaultBackgroundSprite_3;
	// UnityEngine.UI.Image[] ChapterBasedConversationSceneSetup::backgroundImages
	ImageU5BU5D_t590162004* ___backgroundImages_4;
	// BaseConversationCommandLoader ChapterBasedConversationSceneSetup::commandLoader
	BaseConversationCommandLoader_t1091304408 * ___commandLoader_5;
	// BaseConversationController ChapterBasedConversationSceneSetup::conversationController
	BaseConversationController_t808907754 * ___conversationController_6;

public:
	inline static int32_t get_offset_of_defaultBackgroundSprite_3() { return static_cast<int32_t>(offsetof(ChapterBasedConversationSceneSetup_t3163186770, ___defaultBackgroundSprite_3)); }
	inline Sprite_t309593783 * get_defaultBackgroundSprite_3() const { return ___defaultBackgroundSprite_3; }
	inline Sprite_t309593783 ** get_address_of_defaultBackgroundSprite_3() { return &___defaultBackgroundSprite_3; }
	inline void set_defaultBackgroundSprite_3(Sprite_t309593783 * value)
	{
		___defaultBackgroundSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultBackgroundSprite_3, value);
	}

	inline static int32_t get_offset_of_backgroundImages_4() { return static_cast<int32_t>(offsetof(ChapterBasedConversationSceneSetup_t3163186770, ___backgroundImages_4)); }
	inline ImageU5BU5D_t590162004* get_backgroundImages_4() const { return ___backgroundImages_4; }
	inline ImageU5BU5D_t590162004** get_address_of_backgroundImages_4() { return &___backgroundImages_4; }
	inline void set_backgroundImages_4(ImageU5BU5D_t590162004* value)
	{
		___backgroundImages_4 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImages_4, value);
	}

	inline static int32_t get_offset_of_commandLoader_5() { return static_cast<int32_t>(offsetof(ChapterBasedConversationSceneSetup_t3163186770, ___commandLoader_5)); }
	inline BaseConversationCommandLoader_t1091304408 * get_commandLoader_5() const { return ___commandLoader_5; }
	inline BaseConversationCommandLoader_t1091304408 ** get_address_of_commandLoader_5() { return &___commandLoader_5; }
	inline void set_commandLoader_5(BaseConversationCommandLoader_t1091304408 * value)
	{
		___commandLoader_5 = value;
		Il2CppCodeGenWriteBarrier(&___commandLoader_5, value);
	}

	inline static int32_t get_offset_of_conversationController_6() { return static_cast<int32_t>(offsetof(ChapterBasedConversationSceneSetup_t3163186770, ___conversationController_6)); }
	inline BaseConversationController_t808907754 * get_conversationController_6() const { return ___conversationController_6; }
	inline BaseConversationController_t808907754 ** get_address_of_conversationController_6() { return &___conversationController_6; }
	inline void set_conversationController_6(BaseConversationController_t808907754 * value)
	{
		___conversationController_6 = value;
		Il2CppCodeGenWriteBarrier(&___conversationController_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
