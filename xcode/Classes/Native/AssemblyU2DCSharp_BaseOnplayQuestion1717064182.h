﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseOnplayQuestion
struct  BaseOnplayQuestion_t1717064182  : public Il2CppObject
{
public:
	// System.Action BaseOnplayQuestion::onQuestionCompleted
	Action_t3226471752 * ___onQuestionCompleted_0;
	// System.Boolean BaseOnplayQuestion::<isCompleted>k__BackingField
	bool ___U3CisCompletedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_onQuestionCompleted_0() { return static_cast<int32_t>(offsetof(BaseOnplayQuestion_t1717064182, ___onQuestionCompleted_0)); }
	inline Action_t3226471752 * get_onQuestionCompleted_0() const { return ___onQuestionCompleted_0; }
	inline Action_t3226471752 ** get_address_of_onQuestionCompleted_0() { return &___onQuestionCompleted_0; }
	inline void set_onQuestionCompleted_0(Action_t3226471752 * value)
	{
		___onQuestionCompleted_0 = value;
		Il2CppCodeGenWriteBarrier(&___onQuestionCompleted_0, value);
	}

	inline static int32_t get_offset_of_U3CisCompletedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BaseOnplayQuestion_t1717064182, ___U3CisCompletedU3Ek__BackingField_1)); }
	inline bool get_U3CisCompletedU3Ek__BackingField_1() const { return ___U3CisCompletedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CisCompletedU3Ek__BackingField_1() { return &___U3CisCompletedU3Ek__BackingField_1; }
	inline void set_U3CisCompletedU3Ek__BackingField_1(bool value)
	{
		___U3CisCompletedU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
