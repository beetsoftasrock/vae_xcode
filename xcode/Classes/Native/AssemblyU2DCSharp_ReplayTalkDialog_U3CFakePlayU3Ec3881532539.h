﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Action
struct Action_t3226471752;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// ReplayTalkDialog
struct ReplayTalkDialog_t3732871695;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayTalkDialog/<FakePlay>c__Iterator1
struct  U3CFakePlayU3Ec__Iterator1_t3881532539  : public Il2CppObject
{
public:
	// System.String ReplayTalkDialog/<FakePlay>c__Iterator1::pathName
	String_t* ___pathName_0;
	// UnityEngine.AudioClip ReplayTalkDialog/<FakePlay>c__Iterator1::<clip>__0
	AudioClip_t1932558630 * ___U3CclipU3E__0_1;
	// System.Action ReplayTalkDialog/<FakePlay>c__Iterator1::callback
	Action_t3226471752 * ___callback_2;
	// ConversationTalkData ReplayTalkDialog/<FakePlay>c__Iterator1::talkData
	ConversationTalkData_t1570298305 * ___talkData_3;
	// System.Single ReplayTalkDialog/<FakePlay>c__Iterator1::<karaokeTime>__1
	float ___U3CkaraokeTimeU3E__1_4;
	// ReplayTalkDialog ReplayTalkDialog/<FakePlay>c__Iterator1::$this
	ReplayTalkDialog_t3732871695 * ___U24this_5;
	// System.Object ReplayTalkDialog/<FakePlay>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean ReplayTalkDialog/<FakePlay>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 ReplayTalkDialog/<FakePlay>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_pathName_0() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___pathName_0)); }
	inline String_t* get_pathName_0() const { return ___pathName_0; }
	inline String_t** get_address_of_pathName_0() { return &___pathName_0; }
	inline void set_pathName_0(String_t* value)
	{
		___pathName_0 = value;
		Il2CppCodeGenWriteBarrier(&___pathName_0, value);
	}

	inline static int32_t get_offset_of_U3CclipU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___U3CclipU3E__0_1)); }
	inline AudioClip_t1932558630 * get_U3CclipU3E__0_1() const { return ___U3CclipU3E__0_1; }
	inline AudioClip_t1932558630 ** get_address_of_U3CclipU3E__0_1() { return &___U3CclipU3E__0_1; }
	inline void set_U3CclipU3E__0_1(AudioClip_t1932558630 * value)
	{
		___U3CclipU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CclipU3E__0_1, value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___callback_2)); }
	inline Action_t3226471752 * get_callback_2() const { return ___callback_2; }
	inline Action_t3226471752 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_t3226471752 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}

	inline static int32_t get_offset_of_talkData_3() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___talkData_3)); }
	inline ConversationTalkData_t1570298305 * get_talkData_3() const { return ___talkData_3; }
	inline ConversationTalkData_t1570298305 ** get_address_of_talkData_3() { return &___talkData_3; }
	inline void set_talkData_3(ConversationTalkData_t1570298305 * value)
	{
		___talkData_3 = value;
		Il2CppCodeGenWriteBarrier(&___talkData_3, value);
	}

	inline static int32_t get_offset_of_U3CkaraokeTimeU3E__1_4() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___U3CkaraokeTimeU3E__1_4)); }
	inline float get_U3CkaraokeTimeU3E__1_4() const { return ___U3CkaraokeTimeU3E__1_4; }
	inline float* get_address_of_U3CkaraokeTimeU3E__1_4() { return &___U3CkaraokeTimeU3E__1_4; }
	inline void set_U3CkaraokeTimeU3E__1_4(float value)
	{
		___U3CkaraokeTimeU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___U24this_5)); }
	inline ReplayTalkDialog_t3732871695 * get_U24this_5() const { return ___U24this_5; }
	inline ReplayTalkDialog_t3732871695 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(ReplayTalkDialog_t3732871695 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CFakePlayU3Ec__Iterator1_t3881532539, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
