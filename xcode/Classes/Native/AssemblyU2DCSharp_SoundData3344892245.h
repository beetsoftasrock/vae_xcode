﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundData
struct  SoundData_t3344892245  : public Il2CppObject
{
public:
	// System.Single SoundData::wait
	float ___wait_0;
	// System.String SoundData::commandName
	String_t* ___commandName_1;
	// System.String SoundData::soundName
	String_t* ___soundName_2;
	// System.String SoundData::volume
	String_t* ___volume_3;
	// System.String SoundData::isLoop
	String_t* ___isLoop_4;
	// System.String SoundData::position
	String_t* ___position_5;

public:
	inline static int32_t get_offset_of_wait_0() { return static_cast<int32_t>(offsetof(SoundData_t3344892245, ___wait_0)); }
	inline float get_wait_0() const { return ___wait_0; }
	inline float* get_address_of_wait_0() { return &___wait_0; }
	inline void set_wait_0(float value)
	{
		___wait_0 = value;
	}

	inline static int32_t get_offset_of_commandName_1() { return static_cast<int32_t>(offsetof(SoundData_t3344892245, ___commandName_1)); }
	inline String_t* get_commandName_1() const { return ___commandName_1; }
	inline String_t** get_address_of_commandName_1() { return &___commandName_1; }
	inline void set_commandName_1(String_t* value)
	{
		___commandName_1 = value;
		Il2CppCodeGenWriteBarrier(&___commandName_1, value);
	}

	inline static int32_t get_offset_of_soundName_2() { return static_cast<int32_t>(offsetof(SoundData_t3344892245, ___soundName_2)); }
	inline String_t* get_soundName_2() const { return ___soundName_2; }
	inline String_t** get_address_of_soundName_2() { return &___soundName_2; }
	inline void set_soundName_2(String_t* value)
	{
		___soundName_2 = value;
		Il2CppCodeGenWriteBarrier(&___soundName_2, value);
	}

	inline static int32_t get_offset_of_volume_3() { return static_cast<int32_t>(offsetof(SoundData_t3344892245, ___volume_3)); }
	inline String_t* get_volume_3() const { return ___volume_3; }
	inline String_t** get_address_of_volume_3() { return &___volume_3; }
	inline void set_volume_3(String_t* value)
	{
		___volume_3 = value;
		Il2CppCodeGenWriteBarrier(&___volume_3, value);
	}

	inline static int32_t get_offset_of_isLoop_4() { return static_cast<int32_t>(offsetof(SoundData_t3344892245, ___isLoop_4)); }
	inline String_t* get_isLoop_4() const { return ___isLoop_4; }
	inline String_t** get_address_of_isLoop_4() { return &___isLoop_4; }
	inline void set_isLoop_4(String_t* value)
	{
		___isLoop_4 = value;
		Il2CppCodeGenWriteBarrier(&___isLoop_4, value);
	}

	inline static int32_t get_offset_of_position_5() { return static_cast<int32_t>(offsetof(SoundData_t3344892245, ___position_5)); }
	inline String_t* get_position_5() const { return ___position_5; }
	inline String_t** get_address_of_position_5() { return &___position_5; }
	inline void set_position_5(String_t* value)
	{
		___position_5 = value;
		Il2CppCodeGenWriteBarrier(&___position_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
