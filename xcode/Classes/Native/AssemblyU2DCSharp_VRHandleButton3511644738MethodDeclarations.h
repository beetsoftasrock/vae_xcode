﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRHandleButton
struct VRHandleButton_t3511644738;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void VRHandleButton::.ctor()
extern "C"  void VRHandleButton__ctor_m1168741215 (VRHandleButton_t3511644738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRHandleButton::SetViewHolding(System.Boolean)
extern "C"  void VRHandleButton_SetViewHolding_m2870309764 (VRHandleButton_t3511644738 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRHandleButton::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRHandleButton_OnPointerEnter_m1616083909 (VRHandleButton_t3511644738 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRHandleButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VRHandleButton_OnPointerExit_m3454189881 (VRHandleButton_t3511644738 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRHandleButton::Update()
extern "C"  void VRHandleButton_Update_m3001495728 (VRHandleButton_t3511644738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
