﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeCotroller
struct HomeCotroller_t2470068823;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UserInfoResponse
struct UserInfoResponse_t693113528;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "AssemblyU2DCSharp_UserInfoResponse693113528.h"

// System.Void HomeCotroller::.ctor()
extern "C"  void HomeCotroller__ctor_m1304977786 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::UpdateCache(System.Single,UnityEngine.Vector2)
extern "C"  void HomeCotroller_UpdateCache_m1675399914 (HomeCotroller_t2470068823 * __this, float ___time0, Vector2_t2243707579  ___mousePosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::Start()
extern "C"  void HomeCotroller_Start_m2927605790 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::OnclickChapter(System.Int32)
extern "C"  void HomeCotroller_OnclickChapter_m1215660833 (HomeCotroller_t2470068823 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::Switch()
extern "C"  void HomeCotroller_Switch_m12098536 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::StopMoveCoroutine()
extern "C"  void HomeCotroller_StopMoveCoroutine_m1201021289 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::CloseMenu()
extern "C"  void HomeCotroller_CloseMenu_m2117278719 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::OpenMenu()
extern "C"  void HomeCotroller_OpenMenu_m3072474397 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HomeCotroller::MovingClose()
extern "C"  Il2CppObject * HomeCotroller_MovingClose_m302196884 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HomeCotroller::MovingOpen()
extern "C"  Il2CppObject * HomeCotroller_MovingOpen_m1007633430 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HomeCotroller::Moving(UnityEngine.Vector2)
extern "C"  Il2CppObject * HomeCotroller_Moving_m3522284998 (HomeCotroller_t2470068823 * __this, Vector2_t2243707579  ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::ChangeAlphaOvelay(System.Single)
extern "C"  void HomeCotroller_ChangeAlphaOvelay_m25774041 (HomeCotroller_t2470068823 * __this, float ___valueAlpha0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::LoadAccountInfo()
extern "C"  void HomeCotroller_LoadAccountInfo_m3483408443 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::OpenPolicy()
extern "C"  void HomeCotroller_OpenPolicy_m245261746 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::ClosePolicy()
extern "C"  void HomeCotroller_ClosePolicy_m531066344 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::SendMail()
extern "C"  void HomeCotroller_SendMail_m661325931 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::OpenWebPage(System.String)
extern "C"  void HomeCotroller_OpenWebPage_m412120731 (HomeCotroller_t2470068823 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::Awake()
extern "C"  void HomeCotroller_Awake_m2648629603 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::UpdateUsername()
extern "C"  void HomeCotroller_UpdateUsername_m1175770045 (HomeCotroller_t2470068823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void HomeCotroller_OnEndDrag_m2149876626 (HomeCotroller_t2470068823 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void HomeCotroller_OnDrag_m1013273199 (HomeCotroller_t2470068823 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void HomeCotroller_OnBeginDrag_m1290074854 (HomeCotroller_t2470068823 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::<OnclickChapter>m__0()
extern "C"  void HomeCotroller_U3COnclickChapterU3Em__0_m3489008679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::<UpdateUsername>m__1(UserInfoResponse)
extern "C"  void HomeCotroller_U3CUpdateUsernameU3Em__1_m3846676123 (HomeCotroller_t2470068823 * __this, UserInfoResponse_t693113528 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller::<UpdateUsername>m__2()
extern "C"  void HomeCotroller_U3CUpdateUsernameU3Em__2_m3382900644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
