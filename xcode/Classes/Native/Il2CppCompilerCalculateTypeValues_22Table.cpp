﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LearningData1664811342.h"
#include "AssemblyU2DCSharp_EditUsernameResponse1388559799.h"
#include "AssemblyU2DCSharp_ResetDataResponse2526516544.h"
#include "AssemblyU2DCSharp_PushZipResponse1905049400.h"
#include "AssemblyU2DCSharp_RegisterResponse410466074.h"
#include "AssemblyU2DCSharp_LearningChapterLogData1224380751.h"
#include "AssemblyU2DCSharp_LearningLogData2500481692.h"
#include "AssemblyU2DCSharp_LearningLogResponse112900141.h"
#include "AssemblyU2DCSharp_VAEApiController4241972696.h"
#include "AssemblyU2DCSharp_AnalyticsManager1593654123.h"
#include "AssemblyU2DCSharp_DefineCallCustomEvent3427966816.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager364944953.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_2853544650.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_2280417238.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_2686359034.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_1280203046.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_2549557299.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_2422683914.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_1991153063.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_1991153060.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_1991153062.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleManager_3037061210.h"
#include "AssemblyU2DCSharp_ChapterLoader1965880082.h"
#include "AssemblyU2DCSharp_ChapterLoader_U3CloadAssetBundleU399180152.h"
#include "AssemblyU2DCSharp_ChapterLoader_U3CUnloadMyPageU3E3746531243.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleLoader639004779.h"
#include "AssemblyU2DCSharp_AssetBundles_AssetBundleLoader_U3729294472.h"
#include "AssemblyU2DCSharp_AssetBundles_Utility1353423966.h"
#include "AssemblyU2DCSharp_VersionChecker1395544065.h"
#include "AssemblyU2DCSharp_VersionChecker_U3CcoroutineCheckU204427749.h"
#include "AssemblyU2DCSharp_VersionChecker_U3CchecksUpdateU32534716322.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_Instance330262942.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_Instance_3147911398.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder2874686128.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_OnInit1674205282.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_OnFinish2736449411.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_OnError1220768278.h"
#include "AssemblyU2DCSharp_AudioRecorder_Recorder_OnSaved836722345.h"
#include "AssemblyU2DCSharp_InputSpectrum3811795451.h"
#include "AssemblyU2DCSharp_InputSpectrum_Band1120454049.h"
#include "AssemblyU2DCSharp_Asset2923928748.h"
#include "AssemblyU2DCSharp_InitThreeDVRScene2362374842.h"
#include "AssemblyU2DCSharp_ChaperController3790003223.h"
#include "AssemblyU2DCSharp_ChapterConfigData999749205.h"
#include "AssemblyU2DCSharp_ChapterLearningItemView1984184315.h"
#include "AssemblyU2DCSharp_ChapterTopController2392392144.h"
#include "AssemblyU2DCSharp_ChapterTopController_U3CunloadBun676242841.h"
#include "AssemblyU2DCSharp_ChapterTopController_U3CDelayGet4178125851.h"
#include "AssemblyU2DCSharp_ChapterTopController_U3CSelectMod101845251.h"
#include "AssemblyU2DCSharp_ChapterView3775077312.h"
#include "AssemblyU2DCSharp_ChapterView_Part3646775580.h"
#include "AssemblyU2DCSharp_CharacterAsset2986073691.h"
#include "AssemblyU2DCSharp_CharacterScript1308706256.h"
#include "AssemblyU2DCSharp_CharacterScript_U3CChangeSpeedU31808002381.h"
#include "AssemblyU2DCSharp_CharacterScript_U3CWaitTalkingU32502173674.h"
#include "AssemblyU2DCSharp_CharacterScript_U3CDelaySomethin4205573215.h"
#include "AssemblyU2DCSharp_CharacterScript_U3CLerpCharacterT932858100.h"
#include "AssemblyU2DCSharp_StateEmotionCharacter3036603695.h"
#include "AssemblyU2DCSharp_ManagerCharacters162625631.h"
#include "AssemblyU2DCSharp_ManagerCharacters_U3CGetCharacte4143871164.h"
#include "AssemblyU2DCSharp_PostureCharacter580023905.h"
#include "AssemblyU2DCSharp_GlobalConfig3080413471.h"
#include "AssemblyU2DCSharp_ChapterBasedChapterTopSceneSetup675472893.h"
#include "AssemblyU2DCSharp_ChapterBasedConversationSceneSet3163186770.h"
#include "AssemblyU2DCSharp_ChapterBasedConversationSceneVRS2607282170.h"
#include "AssemblyU2DCSharp_ChapterBasedListening1SceneSetup344182819.h"
#include "AssemblyU2DCSharp_ChapterBasedListening2SceneSetup2782607380.h"
#include "AssemblyU2DCSharp_ChapterBasedSentence1SceneSetup2049935139.h"
#include "AssemblyU2DCSharp_ChapterBasedSentence2SceneSetup1868199748.h"
#include "AssemblyU2DCSharp_ChapterBasedVocabularySceneSetup4149890173.h"
#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"
#include "AssemblyU2DCSharp_BaseSetting2575616157.h"
#include "AssemblyU2DCSharp_ChapterTopConfig174003822.h"
#include "AssemblyU2DCSharp_ChapterTopSettings1968541953.h"
#include "AssemblyU2DCSharp_ChapterTopSetting2509872352.h"
#include "AssemblyU2DCSharp_ColorSettingAsset2075258157.h"
#include "AssemblyU2DCSharp_ConversationSceneSetting1311883407.h"
#include "AssemblyU2DCSharp_ConversationUISetting1271024345.h"
#include "AssemblyU2DCSharp_PrelearningSetting219472927.h"
#include "AssemblyU2DCSharp_BaseSettingBasedImageItem1370958008.h"
#include "AssemblyU2DCSharp_BaseSettingBasedItem2801114457.h"
#include "AssemblyU2DCSharp_ChapterBasedLearningInfoView4144040981.h"
#include "AssemblyU2DCSharp_ChapterBasedTabTitleText2163882332.h"
#include "AssemblyU2DCSharp_CharacterNameSelectionUISetting3672699392.h"
#include "AssemblyU2DCSharp_ConversationLevelSelectionUISett1205290263.h"
#include "AssemblyU2DCSharp_SettingBasedButtonRecord3548636674.h"
#include "AssemblyU2DCSharp_SettingBasedHistoryViewItem3178946919.h"
#include "AssemblyU2DCSharp_SettingBasedImageColor3812849895.h"
#include "AssemblyU2DCSharp_SettingBasedKaraokeTextColor3573755485.h"
#include "AssemblyU2DCSharp_SettingBasedLevelSelectionVR3153565435.h"
#include "AssemblyU2DCSharp_SettingBasedSoundIconAnimation662948807.h"
#include "AssemblyU2DCSharp_SettingBasedTextColor2837927353.h"
#include "AssemblyU2DCSharp_SettingBasedLevelSelectionItem3941678420.h"
#include "AssemblyU2DCSharp_SettingBasedListening2AnswerItem180429931.h"
#include "AssemblyU2DCSharp_SettingBasedButtonPressColor3734017101.h"
#include "AssemblyU2DCSharp_BaseSettingFactory22059301.h"
#include "AssemblyU2DCSharp_ChapterTopSettingFactory1128648570.h"
#include "AssemblyU2DCSharp_ConversationSceneSettingFactory1604207323.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (LearningData_t1664811342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[9] = 
{
	LearningData_t1664811342::get_offset_of_chapter_0(),
	LearningData_t1664811342::get_offset_of_category_1(),
	LearningData_t1664811342::get_offset_of_part_2(),
	LearningData_t1664811342::get_offset_of_time_3(),
	LearningData_t1664811342::get_offset_of_achievement_4(),
	LearningData_t1664811342::get_offset_of_positive_5(),
	LearningData_t1664811342::get_offset_of_mistake_6(),
	LearningData_t1664811342::get_offset_of_fit_7(),
	LearningData_t1664811342::get_offset_of_count_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (EditUsernameResponse_t1388559799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[1] = 
{
	EditUsernameResponse_t1388559799::get_offset_of_success_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (ResetDataResponse_t2526516544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[1] = 
{
	ResetDataResponse_t2526516544::get_offset_of_success_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (PushZipResponse_t1905049400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[2] = 
{
	PushZipResponse_t1905049400::get_offset_of_success_0(),
	PushZipResponse_t1905049400::get_offset_of_errorMsg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (RegisterResponse_t410466074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[3] = 
{
	RegisterResponse_t410466074::get_offset_of_success_0(),
	RegisterResponse_t410466074::get_offset_of_errorMsg_1(),
	RegisterResponse_t410466074::get_offset_of_uuid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (LearningChapterLogData_t1224380751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[3] = 
{
	LearningChapterLogData_t1224380751::get_offset_of_category_0(),
	LearningChapterLogData_t1224380751::get_offset_of_percent_1(),
	LearningChapterLogData_t1224380751::get_offset_of_upDown_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (LearningLogData_t2500481692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[3] = 
{
	LearningLogData_t2500481692::get_offset_of_chapter_0(),
	LearningLogData_t2500481692::get_offset_of_alltime_1(),
	LearningLogData_t2500481692::get_offset_of_logData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (LearningLogResponse_t112900141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[3] = 
{
	LearningLogResponse_t112900141::get_offset_of_success_0(),
	LearningLogResponse_t112900141::get_offset_of_errorMsg_1(),
	LearningLogResponse_t112900141::get_offset_of_learningLog_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (VAEApiController_t4241972696), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (AnalyticsManager_t1593654123), -1, sizeof(AnalyticsManager_t1593654123_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2209[2] = 
{
	AnalyticsManager_t1593654123_StaticFields::get_offset_of_instance_2(),
	AnalyticsManager_t1593654123_StaticFields::get_offset_of__globalConfig_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (DefineCallCustomEvent_t3427966816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (AssetBundleManager_t364944953), -1, sizeof(AssetBundleManager_t364944953_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2211[7] = 
{
	AssetBundleManager_t364944953_StaticFields::get_offset_of_instance_2(),
	AssetBundleManager_t364944953_StaticFields::get_offset_of_U3CAllAssetBundlesU3Ek__BackingField_3(),
	AssetBundleManager_t364944953::get_offset_of_bundleOfManifest_4(),
	AssetBundleManager_t364944953::get_offset_of_assetManifest_5(),
	AssetBundleManager_t364944953::get_offset_of_assetBundleUrl_6(),
	AssetBundleManager_t364944953::get_offset_of_bundleVariants_7(),
	AssetBundleManager_t364944953::get_offset_of_loadedBundles_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (U3CCheckBundleExistU3Ec__AnonStorey3_t2853544650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[1] = 
{
	U3CCheckBundleExistU3Ec__AnonStorey3_t2853544650::get_offset_of_bundleName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (U3CGetAssetBundleU3Ec__AnonStorey4_t2280417238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[1] = 
{
	U3CGetAssetBundleU3Ec__AnonStorey4_t2280417238::get_offset_of_bundleName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (U3CIsLoadedBundleU3Ec__AnonStorey5_t2686359034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[1] = 
{
	U3CIsLoadedBundleU3Ec__AnonStorey5_t2686359034::get_offset_of_bundleName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (U3CLoadSceneU3Ec__AnonStorey6_t1280203046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[1] = 
{
	U3CLoadSceneU3Ec__AnonStorey6_t1280203046::get_offset_of_sceneName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (U3CloadManifestFileU3Ec__Iterator0_t2549557299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[11] = 
{
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_U3CdownloadU3E__0_0(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_U3CmanifestPathU3E__1_1(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_U3CexistManifestU3E__2_2(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_U3CtU3E__3_3(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_onProcess_4(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_onError_5(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_onSuccess_6(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_U24this_7(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_U24current_8(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_U24disposing_9(),
	U3CloadManifestFileU3Ec__Iterator0_t2549557299::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (U3CloadAssetBundlesU3Ec__Iterator1_t2422683914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[10] = 
{
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_onProcess_0(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_onError_1(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_includeBundles_2(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_onSuccess_3(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_U24this_4(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_U24current_5(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_U24disposing_6(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_U24PC_7(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_U24locvar0_8(),
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914::get_offset_of_U24locvar2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[4] = 
{
	U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063::get_offset_of_bundleVariants_0(),
	U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063::get_offset_of_onProcess_1(),
	U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063::get_offset_of_allBundles_2(),
	U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063::get_offset_of_U3CU3Ef__refU241_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[3] = 
{
	U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060::get_offset_of_namePrefix_0(),
	U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060::get_offset_of_U3CU3Ef__refU241_1(),
	U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060::get_offset_of_U3CU3Ef__refU248_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[3] = 
{
	U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062::get_offset_of_i_0(),
	U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062::get_offset_of_U3CU3Ef__refU241_1(),
	U3CloadAssetBundlesU3Ec__AnonStorey9_t1991153062::get_offset_of_U3CU3Ef__refU248_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (U3CloadAssetBundleU3Ec__Iterator2_t3037061210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[12] = 
{
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_bundleName_0(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_U3ChasVariantU3E__0_1(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_U3CsplitedU3E__1_2(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_onProcess_3(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_onError_4(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_U3CstdNameU3E__2_5(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_onSuccess_6(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_U3CdownloadU3E__3_7(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_U24this_8(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_U24current_9(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_U24disposing_10(),
	U3CloadAssetBundleU3Ec__Iterator2_t3037061210::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (ChapterLoader_t1965880082), -1, sizeof(ChapterLoader_t1965880082_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2222[4] = 
{
	ChapterLoader_t1965880082::get_offset_of_OnLoadedTermsOfUse_2(),
	ChapterLoader_t1965880082::get_offset_of_OnLoadedMyPageData_3(),
	ChapterLoader_t1965880082_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ChapterLoader_t1965880082_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (U3CloadAssetBundleU3Ec__AnonStorey0_t99180152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[2] = 
{
	U3CloadAssetBundleU3Ec__AnonStorey0_t99180152::get_offset_of_onSuccess_0(),
	U3CloadAssetBundleU3Ec__AnonStorey0_t99180152::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (U3CUnloadMyPageU3Ec__AnonStorey1_t3746531243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[1] = 
{
	U3CUnloadMyPageU3Ec__AnonStorey1_t3746531243::get_offset_of_ignoreChapter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (AssetBundleLoader_t639004779), -1, sizeof(AssetBundleLoader_t639004779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2225[12] = 
{
	AssetBundleLoader_t639004779_StaticFields::get_offset_of_U3CAvailableChaptersU3Ek__BackingField_2(),
	AssetBundleLoader_t639004779_StaticFields::get_offset_of_U3CIsDoneFirstDownloadU3Ek__BackingField_3(),
	AssetBundleLoader_t639004779_StaticFields::get_offset_of__globalConfig_4(),
	AssetBundleLoader_t639004779::get_offset_of_text_Status_5(),
	AssetBundleLoader_t639004779::get_offset_of_text_OverallProg_6(),
	AssetBundleLoader_t639004779::get_offset_of_slider_OverallProg_7(),
	AssetBundleLoader_t639004779::get_offset_of_group_Loading_8(),
	AssetBundleLoader_t639004779::get_offset_of_group_Message_9(),
	AssetBundleLoader_t639004779::get_offset_of_group_Error_10(),
	AssetBundleLoader_t639004779::get_offset_of_behindOverlay_11(),
	AssetBundleLoader_t639004779::get_offset_of_needUpdateBundles_12(),
	AssetBundleLoader_t639004779::get_offset_of_onCompleted_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U3CforceShowU3Ec__Iterator0_t729294472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[6] = 
{
	U3CforceShowU3Ec__Iterator0_t729294472::get_offset_of_U3CspecificBundlesU3E__0_0(),
	U3CforceShowU3Ec__Iterator0_t729294472::get_offset_of_U24locvar0_1(),
	U3CforceShowU3Ec__Iterator0_t729294472::get_offset_of_U24this_2(),
	U3CforceShowU3Ec__Iterator0_t729294472::get_offset_of_U24current_3(),
	U3CforceShowU3Ec__Iterator0_t729294472::get_offset_of_U24disposing_4(),
	U3CforceShowU3Ec__Iterator0_t729294472::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (Utility_t1353423966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (VersionChecker_t1395544065), -1, sizeof(VersionChecker_t1395544065_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2228[1] = 
{
	VersionChecker_t1395544065_StaticFields::get_offset_of_U3CIsCheckDoneU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (U3CcoroutineCheckU3Ec__Iterator0_t204427749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[3] = 
{
	U3CcoroutineCheckU3Ec__Iterator0_t204427749::get_offset_of_U24current_0(),
	U3CcoroutineCheckU3Ec__Iterator0_t204427749::get_offset_of_U24disposing_1(),
	U3CcoroutineCheckU3Ec__Iterator0_t204427749::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (U3CchecksUpdateU3Ec__AnonStorey1_t2534716322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[1] = 
{
	U3CchecksUpdateU3Ec__AnonStorey1_t2534716322::get_offset_of_storeUrl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (Recorder_Instance_t330262942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[4] = 
{
	Recorder_Instance_t330262942::get_offset_of_U3CFrequencyU3Ek__BackingField_2(),
	Recorder_Instance_t330262942::get_offset_of_inputSpectrum_3(),
	Recorder_Instance_t330262942::get_offset_of_audioSource_4(),
	Recorder_Instance_t330262942::get_offset_of_samples_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (U3CplayRecordedClipU3Ec__Iterator0_t3147911398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[4] = 
{
	U3CplayRecordedClipU3Ec__Iterator0_t3147911398::get_offset_of_U24this_0(),
	U3CplayRecordedClipU3Ec__Iterator0_t3147911398::get_offset_of_U24current_1(),
	U3CplayRecordedClipU3Ec__Iterator0_t3147911398::get_offset_of_U24disposing_2(),
	U3CplayRecordedClipU3Ec__Iterator0_t3147911398::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (Recorder_t2874686128), -1, sizeof(Recorder_t2874686128_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2233[12] = 
{
	Recorder_t2874686128_StaticFields::get_offset_of__instance_0(),
	0,
	Recorder_t2874686128_StaticFields::get_offset_of_onInitInvoker_2(),
	Recorder_t2874686128_StaticFields::get_offset_of_onFinishInvoker_3(),
	Recorder_t2874686128_StaticFields::get_offset_of_onErrorInvoker_4(),
	Recorder_t2874686128_StaticFields::get_offset_of_onSavedInvoker_5(),
	Recorder_t2874686128::get_offset_of_minFreq_6(),
	Recorder_t2874686128::get_offset_of_maxFreq_7(),
	Recorder_t2874686128::get_offset_of_isRecorded_8(),
	Recorder_t2874686128::get_offset_of_clip_9(),
	Recorder_t2874686128::get_offset_of_isRecording_10(),
	Recorder_t2874686128::get_offset_of_isReady_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (OnInit_t1674205282), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (OnFinish_t2736449411), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (OnError_t1220768278), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (OnSaved_t836722345), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (InputSpectrum_t3811795451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[2] = 
{
	InputSpectrum_t3811795451::get_offset_of_scale_2(),
	InputSpectrum_t3811795451::get_offset_of_bands_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (Band_t1120454049)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[3] = 
{
	Band_t1120454049::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Band_t1120454049::get_offset_of_target_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Band_t1120454049::get_offset_of_bars_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (Asset_t2923928748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[2] = 
{
	Asset_t2923928748::get_offset_of_BundleName_0(),
	Asset_t2923928748::get_offset_of_AssetName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (InitThreeDVRScene_t2362374842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[1] = 
{
	InitThreeDVRScene_t2362374842::get_offset_of_asset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (ChaperController_t3790003223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[1] = 
{
	ChaperController_t3790003223::get_offset_of_chapterView_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (ChapterConfigData_t999749205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[6] = 
{
	ChapterConfigData_t999749205::get_offset_of_chapterId_0(),
	ChapterConfigData_t999749205::get_offset_of_chapterName_1(),
	ChapterConfigData_t999749205::get_offset_of_title_2(),
	ChapterConfigData_t999749205::get_offset_of_sub_3(),
	ChapterConfigData_t999749205::get_offset_of_detail_4(),
	ChapterConfigData_t999749205::get_offset_of_backgroundImage_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (ChapterLearningItemView_t1984184315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[11] = 
{
	ChapterLearningItemView_t1984184315::get_offset_of__name_2(),
	ChapterLearningItemView_t1984184315::get_offset_of__percent_3(),
	ChapterLearningItemView_t1984184315::get_offset_of_titleText_4(),
	ChapterLearningItemView_t1984184315::get_offset_of_percentText_5(),
	ChapterLearningItemView_t1984184315::get_offset_of_fillSliderImage_6(),
	ChapterLearningItemView_t1984184315::get_offset_of_slider_7(),
	ChapterLearningItemView_t1984184315::get_offset_of_sliderBackground_8(),
	ChapterLearningItemView_t1984184315::get_offset_of_studyButton_9(),
	ChapterLearningItemView_t1984184315::get_offset_of_autoButton_10(),
	ChapterLearningItemView_t1984184315::get_offset_of_autoButtonImage_11(),
	ChapterLearningItemView_t1984184315::get_offset_of__mainColor_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (ChapterTopController_t2392392144), -1, sizeof(ChapterTopController_t2392392144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2247[26] = 
{
	ChapterTopController_t2392392144::get_offset_of_itemNormal_2(),
	ChapterTopController_t2392392144::get_offset_of_itemAuto_3(),
	ChapterTopController_t2392392144::get_offset_of_leftContainer_4(),
	ChapterTopController_t2392392144::get_offset_of_rightContainer_5(),
	ChapterTopController_t2392392144::get_offset_of_chapterId_6(),
	ChapterTopController_t2392392144::get_offset_of_mainColor_7(),
	ChapterTopController_t2392392144::get_offset_of_buttonSprite_8(),
	ChapterTopController_t2392392144::get_offset_of_singleButtonSprite_9(),
	ChapterTopController_t2392392144::get_offset_of_autoButtonSprite_10(),
	ChapterTopController_t2392392144::get_offset_of_chapterNameText_11(),
	ChapterTopController_t2392392144::get_offset_of_vocabularyItem_12(),
	ChapterTopController_t2392392144::get_offset_of_sentenceItem_13(),
	ChapterTopController_t2392392144::get_offset_of_listeningItem_14(),
	ChapterTopController_t2392392144::get_offset_of_conversationItem_15(),
	ChapterTopController_t2392392144::get_offset_of_exerciseItem_16(),
	ChapterTopController_t2392392144::get_offset_of_practiceItem_17(),
	ChapterTopController_t2392392144::get_offset_of_titleText_18(),
	ChapterTopController_t2392392144::get_offset_of_subText_19(),
	ChapterTopController_t2392392144::get_offset_of_detailText_20(),
	ChapterTopController_t2392392144::get_offset_of_backgroundImage_21(),
	ChapterTopController_t2392392144_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
	ChapterTopController_t2392392144_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_23(),
	ChapterTopController_t2392392144_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_24(),
	ChapterTopController_t2392392144_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_25(),
	ChapterTopController_t2392392144_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_26(),
	ChapterTopController_t2392392144_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (U3CunloadBundleDataU3Ec__Iterator0_t676242841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[5] = 
{
	U3CunloadBundleDataU3Ec__Iterator0_t676242841::get_offset_of_U3CfadingObjU3E__0_0(),
	U3CunloadBundleDataU3Ec__Iterator0_t676242841::get_offset_of_U3CfadingImgU3E__1_1(),
	U3CunloadBundleDataU3Ec__Iterator0_t676242841::get_offset_of_U24current_2(),
	U3CunloadBundleDataU3Ec__Iterator0_t676242841::get_offset_of_U24disposing_3(),
	U3CunloadBundleDataU3Ec__Iterator0_t676242841::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[7] = 
{
	U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851::get_offset_of_popupOpener_0(),
	U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851::get_offset_of_sceneName_1(),
	U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851::get_offset_of_normalScene_2(),
	U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851::get_offset_of_U24this_3(),
	U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851::get_offset_of_U24current_4(),
	U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851::get_offset_of_U24disposing_5(),
	U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (U3CSelectModeU3Ec__AnonStorey2_t101845251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[3] = 
{
	U3CSelectModeU3Ec__AnonStorey2_t101845251::get_offset_of_sceneName_0(),
	U3CSelectModeU3Ec__AnonStorey2_t101845251::get_offset_of_normalScene_1(),
	U3CSelectModeU3Ec__AnonStorey2_t101845251::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (ChapterView_t3775077312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[6] = 
{
	ChapterView_t3775077312::get_offset_of_partVocab_2(),
	ChapterView_t3775077312::get_offset_of_partSentence_3(),
	ChapterView_t3775077312::get_offset_of_partListening_4(),
	ChapterView_t3775077312::get_offset_of_partConversation_5(),
	ChapterView_t3775077312::get_offset_of_partExercise_6(),
	ChapterView_t3775077312::get_offset_of_partPractice_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (Part_t3646775580)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[5] = 
{
	Part_t3646775580::get_offset_of_txtNameLesson_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Part_t3646775580::get_offset_of_txtPercent_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Part_t3646775580::get_offset_of_btnStudy_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Part_t3646775580::get_offset_of_btnAuto_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Part_t3646775580::get_offset_of_slider_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (CharacterAsset_t2986073691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[2] = 
{
	CharacterAsset_t2986073691::get_offset_of_nameCharacter_2(),
	CharacterAsset_t2986073691::get_offset_of_posturese_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (CharacterScript_t1308706256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[24] = 
{
	0,
	CharacterScript_t1308706256::get_offset_of_imgPostureType_3(),
	CharacterScript_t1308706256::get_offset_of_imgEyesType_4(),
	CharacterScript_t1308706256::get_offset_of_imgMouth_5(),
	CharacterScript_t1308706256::get_offset_of_resizeRatioWithHeight_6(),
	CharacterScript_t1308706256::get_offset_of_speedLerp_7(),
	CharacterScript_t1308706256::get_offset_of_strSpawnPostion_8(),
	CharacterScript_t1308706256::get_offset_of_spawPosition_9(),
	CharacterScript_t1308706256::get_offset_of_eyesSprite_10(),
	CharacterScript_t1308706256::get_offset_of_mouthSprite_11(),
	CharacterScript_t1308706256::get_offset_of_postureSprite_12(),
	CharacterScript_t1308706256::get_offset_of_mouthAnim_13(),
	CharacterScript_t1308706256::get_offset_of_eyesAnim_14(),
	CharacterScript_t1308706256::get_offset_of_currentEyesAnim_15(),
	CharacterScript_t1308706256::get_offset_of_animatorMouth_16(),
	CharacterScript_t1308706256::get_offset_of_wait_17(),
	CharacterScript_t1308706256::get_offset_of_isBlink_18(),
	CharacterScript_t1308706256::get_offset_of_isTalk_19(),
	CharacterScript_t1308706256::get_offset_of_pathURL_20(),
	CharacterScript_t1308706256::get_offset_of_alternativeTalking_21(),
	CharacterScript_t1308706256::get_offset_of_characterName_22(),
	CharacterScript_t1308706256::get_offset_of_eyesTypeName_23(),
	CharacterScript_t1308706256::get_offset_of_mouthTypeName_24(),
	CharacterScript_t1308706256::get_offset_of_postureTypeName_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (U3CChangeSpeedU3Ec__Iterator0_t1808002381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[5] = 
{
	U3CChangeSpeedU3Ec__Iterator0_t1808002381::get_offset_of_U3CtimeU3E__0_0(),
	U3CChangeSpeedU3Ec__Iterator0_t1808002381::get_offset_of_U24this_1(),
	U3CChangeSpeedU3Ec__Iterator0_t1808002381::get_offset_of_U24current_2(),
	U3CChangeSpeedU3Ec__Iterator0_t1808002381::get_offset_of_U24disposing_3(),
	U3CChangeSpeedU3Ec__Iterator0_t1808002381::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (U3CWaitTalkingU3Ec__Iterator1_t2502173674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[4] = 
{
	U3CWaitTalkingU3Ec__Iterator1_t2502173674::get_offset_of_U24this_0(),
	U3CWaitTalkingU3Ec__Iterator1_t2502173674::get_offset_of_U24current_1(),
	U3CWaitTalkingU3Ec__Iterator1_t2502173674::get_offset_of_U24disposing_2(),
	U3CWaitTalkingU3Ec__Iterator1_t2502173674::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (U3CDelaySomethingU3Ec__Iterator2_t4205573215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[5] = 
{
	U3CDelaySomethingU3Ec__Iterator2_t4205573215::get_offset_of_time_0(),
	U3CDelaySomethingU3Ec__Iterator2_t4205573215::get_offset_of_function_1(),
	U3CDelaySomethingU3Ec__Iterator2_t4205573215::get_offset_of_U24current_2(),
	U3CDelaySomethingU3Ec__Iterator2_t4205573215::get_offset_of_U24disposing_3(),
	U3CDelaySomethingU3Ec__Iterator2_t4205573215::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[5] = 
{
	U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100::get_offset_of_U3CrectU3E__0_0(),
	U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100::get_offset_of_U24this_1(),
	U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100::get_offset_of_U24current_2(),
	U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100::get_offset_of_U24disposing_3(),
	U3CLerpCharacterToSpawnPositionU3Ec__Iterator3_t932858100::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (StateEmotionCharacter_t3036603695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[2] = 
{
	StateEmotionCharacter_t3036603695::get_offset_of_emotionName_0(),
	StateEmotionCharacter_t3036603695::get_offset_of_parts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (ManagerCharacters_t162625631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[6] = 
{
	ManagerCharacters_t162625631::get_offset_of_currentCharTalking_0(),
	ManagerCharacters_t162625631::get_offset_of_currentCharNameTalk_1(),
	ManagerCharacters_t162625631::get_offset_of_pathCharacterResource_2(),
	ManagerCharacters_t162625631::get_offset_of_setCharacter_3(),
	ManagerCharacters_t162625631::get_offset_of_U3CPrefabCharacterU3Ek__BackingField_4(),
	ManagerCharacters_t162625631::get_offset_of_listCharacter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (U3CGetCharacterU3Ec__AnonStorey0_t4143871164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[1] = 
{
	U3CGetCharacterU3Ec__AnonStorey0_t4143871164::get_offset_of_characterName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (PostureCharacter_t580023905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[5] = 
{
	PostureCharacter_t580023905::get_offset_of_index_2(),
	PostureCharacter_t580023905::get_offset_of_postureSprite_3(),
	PostureCharacter_t580023905::get_offset_of_postureName_4(),
	PostureCharacter_t580023905::get_offset_of_remark_5(),
	PostureCharacter_t580023905::get_offset_of_emotions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (GlobalConfig_t3080413471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[1] = 
{
	GlobalConfig_t3080413471::get_offset_of_currentChapter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (ChapterBasedChapterTopSceneSetup_t675472893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[9] = 
{
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_controller_3(),
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_backgroundImages_4(),
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_chapterText_5(),
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_titleText_6(),
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_subTitleText_7(),
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_chapterInfoText_8(),
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_gageImages_9(),
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_tabTexts_10(),
	ChapterBasedChapterTopSceneSetup_t675472893::get_offset_of_selectButtons_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (ChapterBasedConversationSceneSetup_t3163186770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[4] = 
{
	ChapterBasedConversationSceneSetup_t3163186770::get_offset_of_defaultBackgroundSprite_3(),
	ChapterBasedConversationSceneSetup_t3163186770::get_offset_of_backgroundImages_4(),
	ChapterBasedConversationSceneSetup_t3163186770::get_offset_of_commandLoader_5(),
	ChapterBasedConversationSceneSetup_t3163186770::get_offset_of_conversationController_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (ChapterBasedConversationSceneVRSetup_t2607282170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[2] = 
{
	ChapterBasedConversationSceneVRSetup_t2607282170::get_offset_of_commandLoader_3(),
	ChapterBasedConversationSceneVRSetup_t2607282170::get_offset_of_conversationController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (ChapterBasedListening1SceneSetup_t344182819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[2] = 
{
	ChapterBasedListening1SceneSetup_t344182819::get_offset_of_dataLoader_3(),
	ChapterBasedListening1SceneSetup_t344182819::get_offset_of_sceneController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (ChapterBasedListening2SceneSetup_t2782607380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[2] = 
{
	ChapterBasedListening2SceneSetup_t2782607380::get_offset_of_dataLoader_3(),
	ChapterBasedListening2SceneSetup_t2782607380::get_offset_of_sceneController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (ChapterBasedSentence1SceneSetup_t2049935139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[2] = 
{
	ChapterBasedSentence1SceneSetup_t2049935139::get_offset_of_dataLoader_3(),
	ChapterBasedSentence1SceneSetup_t2049935139::get_offset_of_sceneController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (ChapterBasedSentence2SceneSetup_t1868199748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[2] = 
{
	ChapterBasedSentence2SceneSetup_t1868199748::get_offset_of_dataLoader_3(),
	ChapterBasedSentence2SceneSetup_t1868199748::get_offset_of_sceneController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (ChapterBasedVocabularySceneSetup_t4149890173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[2] = 
{
	ChapterBasedVocabularySceneSetup_t4149890173::get_offset_of_dataLoader_3(),
	ChapterBasedVocabularySceneSetup_t4149890173::get_offset_of_vocabularyController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (GlobalBasedSetupModule_t1655890765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[1] = 
{
	GlobalBasedSetupModule_t1655890765::get_offset_of_globalConfig_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (BaseSetting_t2575616157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[1] = 
{
	BaseSetting_t2575616157::get_offset_of__dictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (ChapterTopConfig_t174003822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[10] = 
{
	ChapterTopConfig_t174003822::get_offset_of_chapterText_2(),
	ChapterTopConfig_t174003822::get_offset_of_titleText_3(),
	ChapterTopConfig_t174003822::get_offset_of_subTitleText_4(),
	ChapterTopConfig_t174003822::get_offset_of_chapterInfoText_5(),
	ChapterTopConfig_t174003822::get_offset_of_background_6(),
	ChapterTopConfig_t174003822::get_offset_of_mainColor_7(),
	ChapterTopConfig_t174003822::get_offset_of_buttonSprite_8(),
	ChapterTopConfig_t174003822::get_offset_of_singleButtonSprite_9(),
	ChapterTopConfig_t174003822::get_offset_of_gageSprite_10(),
	ChapterTopConfig_t174003822::get_offset_of_autoButtonSprite_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (ChapterTopSettings_t1968541953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[1] = 
{
	ChapterTopSettings_t1968541953::get_offset_of_settings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (ChapterTopSetting_t2509872352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[11] = 
{
	ChapterTopSetting_t2509872352::get_offset_of_chapterText_0(),
	ChapterTopSetting_t2509872352::get_offset_of_titleText_1(),
	ChapterTopSetting_t2509872352::get_offset_of_subTitleText_2(),
	ChapterTopSetting_t2509872352::get_offset_of_chapterInfoText_3(),
	ChapterTopSetting_t2509872352::get_offset_of_background_4(),
	ChapterTopSetting_t2509872352::get_offset_of_mainColor_5(),
	ChapterTopSetting_t2509872352::get_offset_of_buttonSprite_6(),
	ChapterTopSetting_t2509872352::get_offset_of_singleButtonSprite_7(),
	ChapterTopSetting_t2509872352::get_offset_of_gageSprite_8(),
	ChapterTopSetting_t2509872352::get_offset_of_autoButtonSprite_9(),
	ChapterTopSetting_t2509872352::get_offset_of_tabBackgroundSprite_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (ColorSettingAsset_t2075258157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[2] = 
{
	ColorSettingAsset_t2075258157::get_offset_of_mainColor_2(),
	ColorSettingAsset_t2075258157::get_offset_of_transparentColor_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (ConversationSceneSetting_t1311883407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[4] = 
{
	ConversationSceneSetting_t1311883407::get_offset_of_backgroundSprite_0(),
	ConversationSceneSetting_t1311883407::get_offset_of_commandSheet_1(),
	ConversationSceneSetting_t1311883407::get_offset_of_audioResourcePath_2(),
	ConversationSceneSetting_t1311883407::get_offset_of_environmentPath_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (ConversationUISetting_t1271024345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (PrelearningSetting_t219472927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[2] = 
{
	PrelearningSetting_t219472927::get_offset_of_commandSheet_0(),
	PrelearningSetting_t219472927::get_offset_of_soundResourcePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (BaseSettingBasedImageItem_t1370958008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (BaseSettingBasedItem_t2801114457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[1] = 
{
	BaseSettingBasedItem_t2801114457::get_offset_of__settingIniter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (ChapterBasedLearningInfoView_t4144040981), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (ChapterBasedTabTitleText_t2163882332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (CharacterNameSelectionUISetting_t3672699392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (ConversationLevelSelectionUISetting_t1205290263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (SettingBasedButtonRecord_t3548636674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (SettingBasedHistoryViewItem_t3178946919), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (SettingBasedImageColor_t3812849895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (SettingBasedKaraokeTextColor_t3573755485), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (SettingBasedLevelSelectionVR_t3153565435), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (SettingBasedSoundIconAnimation_t662948807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (SettingBasedTextColor_t2837927353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (SettingBasedLevelSelectionItem_t3941678420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (SettingBasedListening2AnswerItem_t180429931), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (SettingBasedButtonPressColor_t3734017101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (BaseSettingFactory_t22059301), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (ChapterTopSettingFactory_t1128648570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (ConversationSceneSettingFactory_t1604207323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[3] = 
{
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
