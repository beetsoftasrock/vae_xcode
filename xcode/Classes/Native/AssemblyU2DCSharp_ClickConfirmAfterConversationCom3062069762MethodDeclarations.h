﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickConfirmAfterConversationComplete
struct ClickConfirmAfterConversationComplete_t3062069762;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationLogicController_Conv3360496668.h"

// System.Void ClickConfirmAfterConversationComplete::.ctor()
extern "C"  void ClickConfirmAfterConversationComplete__ctor_m2235064743 (ClickConfirmAfterConversationComplete_t3062069762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickConfirmAfterConversationComplete::WaitUserAction()
extern "C"  void ClickConfirmAfterConversationComplete_WaitUserAction_m287178811 (ClickConfirmAfterConversationComplete_t3062069762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickConfirmAfterConversationComplete::Confirm()
extern "C"  void ClickConfirmAfterConversationComplete_Confirm_m234814489 (ClickConfirmAfterConversationComplete_t3062069762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickConfirmAfterConversationComplete::HandleConversationLogicControllerStateChange(ConversationLogicController/ConversationState)
extern "C"  void ClickConfirmAfterConversationComplete_HandleConversationLogicControllerStateChange_m2064549381 (ClickConfirmAfterConversationComplete_t3062069762 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickConfirmAfterConversationComplete::Awake()
extern "C"  void ClickConfirmAfterConversationComplete_Awake_m2399522590 (ClickConfirmAfterConversationComplete_t3062069762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickConfirmAfterConversationComplete::OnEnable()
extern "C"  void ClickConfirmAfterConversationComplete_OnEnable_m1357710611 (ClickConfirmAfterConversationComplete_t3062069762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
