﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Object
struct Object_t1021602117;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey
struct  FieldWithRemoteSettingsKey_t2620356393  : public Il2CppObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::m_Target
	Object_t1021602117 * ___m_Target_0;
	// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::m_FieldPath
	String_t* ___m_FieldPath_1;
	// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::m_RSKeyName
	String_t* ___m_RSKeyName_2;
	// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::m_Type
	String_t* ___m_Type_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FieldWithRemoteSettingsKey_t2620356393, ___m_Target_0)); }
	inline Object_t1021602117 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t1021602117 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t1021602117 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Target_0, value);
	}

	inline static int32_t get_offset_of_m_FieldPath_1() { return static_cast<int32_t>(offsetof(FieldWithRemoteSettingsKey_t2620356393, ___m_FieldPath_1)); }
	inline String_t* get_m_FieldPath_1() const { return ___m_FieldPath_1; }
	inline String_t** get_address_of_m_FieldPath_1() { return &___m_FieldPath_1; }
	inline void set_m_FieldPath_1(String_t* value)
	{
		___m_FieldPath_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_FieldPath_1, value);
	}

	inline static int32_t get_offset_of_m_RSKeyName_2() { return static_cast<int32_t>(offsetof(FieldWithRemoteSettingsKey_t2620356393, ___m_RSKeyName_2)); }
	inline String_t* get_m_RSKeyName_2() const { return ___m_RSKeyName_2; }
	inline String_t** get_address_of_m_RSKeyName_2() { return &___m_RSKeyName_2; }
	inline void set_m_RSKeyName_2(String_t* value)
	{
		___m_RSKeyName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_RSKeyName_2, value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(FieldWithRemoteSettingsKey_t2620356393, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Type_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
