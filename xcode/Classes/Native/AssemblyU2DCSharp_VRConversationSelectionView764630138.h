﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ConversationSceneSelectionView407867144.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRConversationSelectionView
struct  VRConversationSelectionView_t764630138  : public ConversationSceneSelectionView_t407867144
{
public:
	// System.Boolean VRConversationSelectionView::_firstRun
	bool ____firstRun_13;
	// UnityEngine.Color VRConversationSelectionView::btnSelectedColor
	Color_t2020392075  ___btnSelectedColor_14;
	// UnityEngine.Color VRConversationSelectionView::btnUnselectedColor
	Color_t2020392075  ___btnUnselectedColor_15;

public:
	inline static int32_t get_offset_of__firstRun_13() { return static_cast<int32_t>(offsetof(VRConversationSelectionView_t764630138, ____firstRun_13)); }
	inline bool get__firstRun_13() const { return ____firstRun_13; }
	inline bool* get_address_of__firstRun_13() { return &____firstRun_13; }
	inline void set__firstRun_13(bool value)
	{
		____firstRun_13 = value;
	}

	inline static int32_t get_offset_of_btnSelectedColor_14() { return static_cast<int32_t>(offsetof(VRConversationSelectionView_t764630138, ___btnSelectedColor_14)); }
	inline Color_t2020392075  get_btnSelectedColor_14() const { return ___btnSelectedColor_14; }
	inline Color_t2020392075 * get_address_of_btnSelectedColor_14() { return &___btnSelectedColor_14; }
	inline void set_btnSelectedColor_14(Color_t2020392075  value)
	{
		___btnSelectedColor_14 = value;
	}

	inline static int32_t get_offset_of_btnUnselectedColor_15() { return static_cast<int32_t>(offsetof(VRConversationSelectionView_t764630138, ___btnUnselectedColor_15)); }
	inline Color_t2020392075  get_btnUnselectedColor_15() const { return ___btnUnselectedColor_15; }
	inline Color_t2020392075 * get_address_of_btnUnselectedColor_15() { return &___btnUnselectedColor_15; }
	inline void set_btnUnselectedColor_15(Color_t2020392075  value)
	{
		___btnUnselectedColor_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
