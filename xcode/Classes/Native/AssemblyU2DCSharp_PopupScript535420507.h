﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Canvas
struct Canvas_t209405766;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupScript
struct  PopupScript_t535420507  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PopupScript::destroyTime
	float ___destroyTime_2;
	// UnityEngine.GameObject PopupScript::overlay
	GameObject_t1756533147 * ___overlay_3;
	// UnityEngine.Canvas PopupScript::canvas
	Canvas_t209405766 * ___canvas_4;
	// UnityEngine.GameObject PopupScript::overlayClone
	GameObject_t1756533147 * ___overlayClone_5;
	// UnityEngine.GameObject PopupScript::popupClone
	GameObject_t1756533147 * ___popupClone_6;

public:
	inline static int32_t get_offset_of_destroyTime_2() { return static_cast<int32_t>(offsetof(PopupScript_t535420507, ___destroyTime_2)); }
	inline float get_destroyTime_2() const { return ___destroyTime_2; }
	inline float* get_address_of_destroyTime_2() { return &___destroyTime_2; }
	inline void set_destroyTime_2(float value)
	{
		___destroyTime_2 = value;
	}

	inline static int32_t get_offset_of_overlay_3() { return static_cast<int32_t>(offsetof(PopupScript_t535420507, ___overlay_3)); }
	inline GameObject_t1756533147 * get_overlay_3() const { return ___overlay_3; }
	inline GameObject_t1756533147 ** get_address_of_overlay_3() { return &___overlay_3; }
	inline void set_overlay_3(GameObject_t1756533147 * value)
	{
		___overlay_3 = value;
		Il2CppCodeGenWriteBarrier(&___overlay_3, value);
	}

	inline static int32_t get_offset_of_canvas_4() { return static_cast<int32_t>(offsetof(PopupScript_t535420507, ___canvas_4)); }
	inline Canvas_t209405766 * get_canvas_4() const { return ___canvas_4; }
	inline Canvas_t209405766 ** get_address_of_canvas_4() { return &___canvas_4; }
	inline void set_canvas_4(Canvas_t209405766 * value)
	{
		___canvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_4, value);
	}

	inline static int32_t get_offset_of_overlayClone_5() { return static_cast<int32_t>(offsetof(PopupScript_t535420507, ___overlayClone_5)); }
	inline GameObject_t1756533147 * get_overlayClone_5() const { return ___overlayClone_5; }
	inline GameObject_t1756533147 ** get_address_of_overlayClone_5() { return &___overlayClone_5; }
	inline void set_overlayClone_5(GameObject_t1756533147 * value)
	{
		___overlayClone_5 = value;
		Il2CppCodeGenWriteBarrier(&___overlayClone_5, value);
	}

	inline static int32_t get_offset_of_popupClone_6() { return static_cast<int32_t>(offsetof(PopupScript_t535420507, ___popupClone_6)); }
	inline GameObject_t1756533147 * get_popupClone_6() const { return ___popupClone_6; }
	inline GameObject_t1756533147 ** get_address_of_popupClone_6() { return &___popupClone_6; }
	inline void set_popupClone_6(GameObject_t1756533147 * value)
	{
		___popupClone_6 = value;
		Il2CppCodeGenWriteBarrier(&___popupClone_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
