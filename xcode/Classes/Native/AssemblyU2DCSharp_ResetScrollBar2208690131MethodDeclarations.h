﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResetScrollBar
struct ResetScrollBar_t2208690131;

#include "codegen/il2cpp-codegen.h"

// System.Void ResetScrollBar::.ctor()
extern "C"  void ResetScrollBar__ctor_m3227285198 (ResetScrollBar_t2208690131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetScrollBar::OnEnable()
extern "C"  void ResetScrollBar_OnEnable_m2637962450 (ResetScrollBar_t2208690131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
