﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SphereCollider
struct SphereCollider_t1662511355;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EyeDetectTarget
struct  EyeDetectTarget_t3038356393  : public MonoBehaviour_t1158329972
{
public:
	// System.Single EyeDetectTarget::size
	float ___size_2;
	// UnityEngine.SphereCollider EyeDetectTarget::_currentCollider
	SphereCollider_t1662511355 * ____currentCollider_3;

public:
	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(EyeDetectTarget_t3038356393, ___size_2)); }
	inline float get_size_2() const { return ___size_2; }
	inline float* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(float value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of__currentCollider_3() { return static_cast<int32_t>(offsetof(EyeDetectTarget_t3038356393, ____currentCollider_3)); }
	inline SphereCollider_t1662511355 * get__currentCollider_3() const { return ____currentCollider_3; }
	inline SphereCollider_t1662511355 ** get_address_of__currentCollider_3() { return &____currentCollider_3; }
	inline void set__currentCollider_3(SphereCollider_t1662511355 * value)
	{
		____currentCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&____currentCollider_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
