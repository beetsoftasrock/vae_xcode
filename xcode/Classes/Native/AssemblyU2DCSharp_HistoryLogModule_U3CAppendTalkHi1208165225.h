﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// HistoryLogModule
struct HistoryLogModule_t1965887714;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1
struct  U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225  : public Il2CppObject
{
public:
	// ConversationTalkData HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::data
	ConversationTalkData_t1570298305 * ___data_0;
	// System.Boolean HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::<isPlayerRole>__0
	bool ___U3CisPlayerRoleU3E__0_1;
	// System.String HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::<strEn>__1
	String_t* ___U3CstrEnU3E__1_2;
	// System.String HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::<strJp>__2
	String_t* ___U3CstrJpU3E__2_3;
	// UnityEngine.Transform HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::dialogTransform
	Transform_t3275118058 * ___dialogTransform_4;
	// HistoryLogModule HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::$this
	HistoryLogModule_t1965887714 * ___U24this_5;
	// System.Object HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___data_0)); }
	inline ConversationTalkData_t1570298305 * get_data_0() const { return ___data_0; }
	inline ConversationTalkData_t1570298305 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ConversationTalkData_t1570298305 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}

	inline static int32_t get_offset_of_U3CisPlayerRoleU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___U3CisPlayerRoleU3E__0_1)); }
	inline bool get_U3CisPlayerRoleU3E__0_1() const { return ___U3CisPlayerRoleU3E__0_1; }
	inline bool* get_address_of_U3CisPlayerRoleU3E__0_1() { return &___U3CisPlayerRoleU3E__0_1; }
	inline void set_U3CisPlayerRoleU3E__0_1(bool value)
	{
		___U3CisPlayerRoleU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CstrEnU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___U3CstrEnU3E__1_2)); }
	inline String_t* get_U3CstrEnU3E__1_2() const { return ___U3CstrEnU3E__1_2; }
	inline String_t** get_address_of_U3CstrEnU3E__1_2() { return &___U3CstrEnU3E__1_2; }
	inline void set_U3CstrEnU3E__1_2(String_t* value)
	{
		___U3CstrEnU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrEnU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CstrJpU3E__2_3() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___U3CstrJpU3E__2_3)); }
	inline String_t* get_U3CstrJpU3E__2_3() const { return ___U3CstrJpU3E__2_3; }
	inline String_t** get_address_of_U3CstrJpU3E__2_3() { return &___U3CstrJpU3E__2_3; }
	inline void set_U3CstrJpU3E__2_3(String_t* value)
	{
		___U3CstrJpU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrJpU3E__2_3, value);
	}

	inline static int32_t get_offset_of_dialogTransform_4() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___dialogTransform_4)); }
	inline Transform_t3275118058 * get_dialogTransform_4() const { return ___dialogTransform_4; }
	inline Transform_t3275118058 ** get_address_of_dialogTransform_4() { return &___dialogTransform_4; }
	inline void set_dialogTransform_4(Transform_t3275118058 * value)
	{
		___dialogTransform_4 = value;
		Il2CppCodeGenWriteBarrier(&___dialogTransform_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___U24this_5)); }
	inline HistoryLogModule_t1965887714 * get_U24this_5() const { return ___U24this_5; }
	inline HistoryLogModule_t1965887714 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(HistoryLogModule_t1965887714 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
