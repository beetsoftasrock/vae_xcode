﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.String
struct String_t;
// HistoryConversationDialog
struct HistoryConversationDialog_t3676407505;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HistoryConversationDialog/<AddConversationSentence>c__Iterator1
struct  U3CAddConversationSentenceU3Ec__Iterator1_t1274435845  : public Il2CppObject
{
public:
	// UnityEngine.GameObject HistoryConversationDialog/<AddConversationSentence>c__Iterator1::<item>__0
	GameObject_t1756533147 * ___U3CitemU3E__0_0;
	// UnityEngine.Transform HistoryConversationDialog/<AddConversationSentence>c__Iterator1::dialogTransform
	Transform_t3275118058 * ___dialogTransform_1;
	// System.Boolean HistoryConversationDialog/<AddConversationSentence>c__Iterator1::isPlayerRole
	bool ___isPlayerRole_2;
	// ConversationTalkData HistoryConversationDialog/<AddConversationSentence>c__Iterator1::data
	ConversationTalkData_t1570298305 * ___data_3;
	// System.String HistoryConversationDialog/<AddConversationSentence>c__Iterator1::textEn
	String_t* ___textEn_4;
	// System.String HistoryConversationDialog/<AddConversationSentence>c__Iterator1::textJp
	String_t* ___textJp_5;
	// HistoryConversationDialog HistoryConversationDialog/<AddConversationSentence>c__Iterator1::$this
	HistoryConversationDialog_t3676407505 * ___U24this_6;
	// System.Object HistoryConversationDialog/<AddConversationSentence>c__Iterator1::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean HistoryConversationDialog/<AddConversationSentence>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 HistoryConversationDialog/<AddConversationSentence>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CitemU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___U3CitemU3E__0_0)); }
	inline GameObject_t1756533147 * get_U3CitemU3E__0_0() const { return ___U3CitemU3E__0_0; }
	inline GameObject_t1756533147 ** get_address_of_U3CitemU3E__0_0() { return &___U3CitemU3E__0_0; }
	inline void set_U3CitemU3E__0_0(GameObject_t1756533147 * value)
	{
		___U3CitemU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemU3E__0_0, value);
	}

	inline static int32_t get_offset_of_dialogTransform_1() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___dialogTransform_1)); }
	inline Transform_t3275118058 * get_dialogTransform_1() const { return ___dialogTransform_1; }
	inline Transform_t3275118058 ** get_address_of_dialogTransform_1() { return &___dialogTransform_1; }
	inline void set_dialogTransform_1(Transform_t3275118058 * value)
	{
		___dialogTransform_1 = value;
		Il2CppCodeGenWriteBarrier(&___dialogTransform_1, value);
	}

	inline static int32_t get_offset_of_isPlayerRole_2() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___isPlayerRole_2)); }
	inline bool get_isPlayerRole_2() const { return ___isPlayerRole_2; }
	inline bool* get_address_of_isPlayerRole_2() { return &___isPlayerRole_2; }
	inline void set_isPlayerRole_2(bool value)
	{
		___isPlayerRole_2 = value;
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___data_3)); }
	inline ConversationTalkData_t1570298305 * get_data_3() const { return ___data_3; }
	inline ConversationTalkData_t1570298305 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(ConversationTalkData_t1570298305 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier(&___data_3, value);
	}

	inline static int32_t get_offset_of_textEn_4() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___textEn_4)); }
	inline String_t* get_textEn_4() const { return ___textEn_4; }
	inline String_t** get_address_of_textEn_4() { return &___textEn_4; }
	inline void set_textEn_4(String_t* value)
	{
		___textEn_4 = value;
		Il2CppCodeGenWriteBarrier(&___textEn_4, value);
	}

	inline static int32_t get_offset_of_textJp_5() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___textJp_5)); }
	inline String_t* get_textJp_5() const { return ___textJp_5; }
	inline String_t** get_address_of_textJp_5() { return &___textJp_5; }
	inline void set_textJp_5(String_t* value)
	{
		___textJp_5 = value;
		Il2CppCodeGenWriteBarrier(&___textJp_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___U24this_6)); }
	inline HistoryConversationDialog_t3676407505 * get_U24this_6() const { return ___U24this_6; }
	inline HistoryConversationDialog_t3676407505 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(HistoryConversationDialog_t3676407505 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CAddConversationSentenceU3Ec__Iterator1_t1274435845, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
