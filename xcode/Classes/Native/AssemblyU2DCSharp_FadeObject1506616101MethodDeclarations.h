﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FadeObject
struct FadeObject_t1506616101;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void FadeObject::.ctor()
extern "C"  void FadeObject__ctor_m469450928 (FadeObject_t1506616101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeObject::FadeOut()
extern "C"  void FadeObject_FadeOut_m3233398274 (FadeObject_t1506616101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeObject::FadeIn()
extern "C"  void FadeObject_FadeIn_m136161373 (FadeObject_t1506616101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeObject::OnLockScreen()
extern "C"  void FadeObject_OnLockScreen_m1420336044 (FadeObject_t1506616101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeObject::OnUnLockScreen()
extern "C"  void FadeObject_OnUnLockScreen_m3563933373 (FadeObject_t1506616101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FadeObject::IEFade(System.Single,System.Single)
extern "C"  Il2CppObject * FadeObject_IEFade_m1432190444 (FadeObject_t1506616101 * __this, float ___value0, float ___targetValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
