﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationDataSaver
struct ConversationDataSaver_t1206770280;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationDataSaver::.ctor()
extern "C"  void ConversationDataSaver__ctor_m308439565 (ConversationDataSaver_t1206770280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
