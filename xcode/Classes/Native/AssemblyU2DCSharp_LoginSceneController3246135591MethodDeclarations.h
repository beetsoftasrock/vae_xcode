﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginSceneController
struct LoginSceneController_t3246135591;
// System.String
struct String_t;
// ChangeDeviceResponse
struct ChangeDeviceResponse_t284910379;
// LearningLogResponse
struct LearningLogResponse_t112900141;
// LearningLogData[]
struct LearningLogDataU5BU5D_t3295896437;
// RegisterResponse
struct RegisterResponse_t410466074;
// System.Action`1<RegisterResponse>
struct Action_1_t212265456;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ChangeDeviceResponse284910379.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_LearningLogResponse112900141.h"
#include "AssemblyU2DCSharp_RegisterResponse410466074.h"
#include "System_Core_System_Action3226471752.h"

// System.Void LoginSceneController::.ctor()
extern "C"  void LoginSceneController__ctor_m874941646 (LoginSceneController_t3246135591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::Summit()
extern "C"  void LoginSceneController_Summit_m1240026473 (LoginSceneController_t3246135591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::LockSummit()
extern "C"  void LoginSceneController_LockSummit_m2471510648 (LoginSceneController_t3246135591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::UnlockSummit()
extern "C"  void LoginSceneController_UnlockSummit_m2147222253 (LoginSceneController_t3246135591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::OpenRegisterScene()
extern "C"  void LoginSceneController_OpenRegisterScene_m153945033 (LoginSceneController_t3246135591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::LoginChangeDevice(System.String,System.String)
extern "C"  void LoginSceneController_LoginChangeDevice_m292824007 (LoginSceneController_t3246135591 * __this, String_t* ___idText0, String_t* ___codeText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::HandleLoginResponse(ChangeDeviceResponse)
extern "C"  void LoginSceneController_HandleLoginResponse_m381154361 (LoginSceneController_t3246135591 * __this, ChangeDeviceResponse_t284910379 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::HandleConnectionError()
extern "C"  void LoginSceneController_HandleConnectionError_m2561567166 (LoginSceneController_t3246135591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::GetLearningData(System.String,System.DateTime,System.DateTime)
extern "C"  void LoginSceneController_GetLearningData_m2788203854 (LoginSceneController_t3246135591 * __this, String_t* ___uuid0, DateTime_t693205669  ___startDate1, DateTime_t693205669  ___endDate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::HandleLearningDataResponse(LearningLogResponse)
extern "C"  void LoginSceneController_HandleLearningDataResponse_m1622565986 (LoginSceneController_t3246135591 * __this, LearningLogResponse_t112900141 * ___learningLogResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::UpdateLearningDatas(LearningLogData[])
extern "C"  void LoginSceneController_UpdateLearningDatas_m3697371080 (LoginSceneController_t3246135591 * __this, LearningLogDataU5BU5D_t3295896437* ___learningLogs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::HandleRegisterReponse(RegisterResponse)
extern "C"  void LoginSceneController_HandleRegisterReponse_m1812644159 (LoginSceneController_t3246135591 * __this, RegisterResponse_t410466074 * ___rrd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::Register(System.String,System.Action`1<RegisterResponse>,System.Action)
extern "C"  void LoginSceneController_Register_m1181596156 (LoginSceneController_t3246135591 * __this, String_t* ___username0, Action_1_t212265456 * ___requestSuccessCallback1, Action_t3226471752 * ___requestErrorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::<Summit>m__0(RegisterResponse)
extern "C"  void LoginSceneController_U3CSummitU3Em__0_m1579634766 (LoginSceneController_t3246135591 * __this, RegisterResponse_t410466074 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSceneController::<Summit>m__1()
extern "C"  void LoginSceneController_U3CSummitU3Em__1_m3930768439 (LoginSceneController_t3246135591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
