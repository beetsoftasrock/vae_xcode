﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationLogicController
struct ConversationLogicController_t3911886281;
// TalkDialog
struct TalkDialog_t1412511810;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ReplayTalkDialog
struct ReplayTalkDialog_t3732871695;
// ConversationTalkData
struct ConversationTalkData_t1570298305;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerTalkModule
struct  PlayerTalkModule_t3356839145  : public MonoBehaviour_t1158329972
{
public:
	// ConversationLogicController PlayerTalkModule::clc
	ConversationLogicController_t3911886281 * ___clc_2;
	// TalkDialog PlayerTalkModule::playerTalkDialog
	TalkDialog_t1412511810 * ___playerTalkDialog_3;
	// UnityEngine.UI.Button PlayerTalkModule::recordButton
	Button_t2872111280 * ___recordButton_4;
	// UnityEngine.UI.Button PlayerTalkModule::replayRecordedButton
	Button_t2872111280 * ___replayRecordedButton_5;
	// UnityEngine.GameObject PlayerTalkModule::bottomButtons
	GameObject_t1756533147 * ___bottomButtons_6;
	// ReplayTalkDialog PlayerTalkModule::playSampleTalkDialog
	ReplayTalkDialog_t3732871695 * ___playSampleTalkDialog_7;
	// ReplayTalkDialog PlayerTalkModule::replayTalkDialog
	ReplayTalkDialog_t3732871695 * ___replayTalkDialog_8;
	// ConversationTalkData PlayerTalkModule::<currentPlayerTalkData>k__BackingField
	ConversationTalkData_t1570298305 * ___U3CcurrentPlayerTalkDataU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_clc_2() { return static_cast<int32_t>(offsetof(PlayerTalkModule_t3356839145, ___clc_2)); }
	inline ConversationLogicController_t3911886281 * get_clc_2() const { return ___clc_2; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_2() { return &___clc_2; }
	inline void set_clc_2(ConversationLogicController_t3911886281 * value)
	{
		___clc_2 = value;
		Il2CppCodeGenWriteBarrier(&___clc_2, value);
	}

	inline static int32_t get_offset_of_playerTalkDialog_3() { return static_cast<int32_t>(offsetof(PlayerTalkModule_t3356839145, ___playerTalkDialog_3)); }
	inline TalkDialog_t1412511810 * get_playerTalkDialog_3() const { return ___playerTalkDialog_3; }
	inline TalkDialog_t1412511810 ** get_address_of_playerTalkDialog_3() { return &___playerTalkDialog_3; }
	inline void set_playerTalkDialog_3(TalkDialog_t1412511810 * value)
	{
		___playerTalkDialog_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerTalkDialog_3, value);
	}

	inline static int32_t get_offset_of_recordButton_4() { return static_cast<int32_t>(offsetof(PlayerTalkModule_t3356839145, ___recordButton_4)); }
	inline Button_t2872111280 * get_recordButton_4() const { return ___recordButton_4; }
	inline Button_t2872111280 ** get_address_of_recordButton_4() { return &___recordButton_4; }
	inline void set_recordButton_4(Button_t2872111280 * value)
	{
		___recordButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___recordButton_4, value);
	}

	inline static int32_t get_offset_of_replayRecordedButton_5() { return static_cast<int32_t>(offsetof(PlayerTalkModule_t3356839145, ___replayRecordedButton_5)); }
	inline Button_t2872111280 * get_replayRecordedButton_5() const { return ___replayRecordedButton_5; }
	inline Button_t2872111280 ** get_address_of_replayRecordedButton_5() { return &___replayRecordedButton_5; }
	inline void set_replayRecordedButton_5(Button_t2872111280 * value)
	{
		___replayRecordedButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___replayRecordedButton_5, value);
	}

	inline static int32_t get_offset_of_bottomButtons_6() { return static_cast<int32_t>(offsetof(PlayerTalkModule_t3356839145, ___bottomButtons_6)); }
	inline GameObject_t1756533147 * get_bottomButtons_6() const { return ___bottomButtons_6; }
	inline GameObject_t1756533147 ** get_address_of_bottomButtons_6() { return &___bottomButtons_6; }
	inline void set_bottomButtons_6(GameObject_t1756533147 * value)
	{
		___bottomButtons_6 = value;
		Il2CppCodeGenWriteBarrier(&___bottomButtons_6, value);
	}

	inline static int32_t get_offset_of_playSampleTalkDialog_7() { return static_cast<int32_t>(offsetof(PlayerTalkModule_t3356839145, ___playSampleTalkDialog_7)); }
	inline ReplayTalkDialog_t3732871695 * get_playSampleTalkDialog_7() const { return ___playSampleTalkDialog_7; }
	inline ReplayTalkDialog_t3732871695 ** get_address_of_playSampleTalkDialog_7() { return &___playSampleTalkDialog_7; }
	inline void set_playSampleTalkDialog_7(ReplayTalkDialog_t3732871695 * value)
	{
		___playSampleTalkDialog_7 = value;
		Il2CppCodeGenWriteBarrier(&___playSampleTalkDialog_7, value);
	}

	inline static int32_t get_offset_of_replayTalkDialog_8() { return static_cast<int32_t>(offsetof(PlayerTalkModule_t3356839145, ___replayTalkDialog_8)); }
	inline ReplayTalkDialog_t3732871695 * get_replayTalkDialog_8() const { return ___replayTalkDialog_8; }
	inline ReplayTalkDialog_t3732871695 ** get_address_of_replayTalkDialog_8() { return &___replayTalkDialog_8; }
	inline void set_replayTalkDialog_8(ReplayTalkDialog_t3732871695 * value)
	{
		___replayTalkDialog_8 = value;
		Il2CppCodeGenWriteBarrier(&___replayTalkDialog_8, value);
	}

	inline static int32_t get_offset_of_U3CcurrentPlayerTalkDataU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PlayerTalkModule_t3356839145, ___U3CcurrentPlayerTalkDataU3Ek__BackingField_9)); }
	inline ConversationTalkData_t1570298305 * get_U3CcurrentPlayerTalkDataU3Ek__BackingField_9() const { return ___U3CcurrentPlayerTalkDataU3Ek__BackingField_9; }
	inline ConversationTalkData_t1570298305 ** get_address_of_U3CcurrentPlayerTalkDataU3Ek__BackingField_9() { return &___U3CcurrentPlayerTalkDataU3Ek__BackingField_9; }
	inline void set_U3CcurrentPlayerTalkDataU3Ek__BackingField_9(ConversationTalkData_t1570298305 * value)
	{
		___U3CcurrentPlayerTalkDataU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentPlayerTalkDataU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
