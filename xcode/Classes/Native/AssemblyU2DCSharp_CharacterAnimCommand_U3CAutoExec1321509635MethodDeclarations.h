﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterAnimCommand/<AutoExecute>c__Iterator0
struct U3CAutoExecuteU3Ec__Iterator0_t1321509635;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterAnimCommand/<AutoExecute>c__Iterator0::.ctor()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0__ctor_m1016370174 (U3CAutoExecuteU3Ec__Iterator0_t1321509635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterAnimCommand/<AutoExecute>c__Iterator0::MoveNext()
extern "C"  bool U3CAutoExecuteU3Ec__Iterator0_MoveNext_m2282596962 (U3CAutoExecuteU3Ec__Iterator0_t1321509635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterAnimCommand/<AutoExecute>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAutoExecuteU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3224593130 (U3CAutoExecuteU3Ec__Iterator0_t1321509635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterAnimCommand/<AutoExecute>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAutoExecuteU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1158690722 (U3CAutoExecuteU3Ec__Iterator0_t1321509635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterAnimCommand/<AutoExecute>c__Iterator0::Dispose()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0_Dispose_m279605545 (U3CAutoExecuteU3Ec__Iterator0_t1321509635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterAnimCommand/<AutoExecute>c__Iterator0::Reset()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0_Reset_m2981150975 (U3CAutoExecuteU3Ec__Iterator0_t1321509635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
