﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneController/<IEFadeWithTime>c__Iterator0
struct U3CIEFadeWithTimeU3Ec__Iterator0_t115250393;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneController/<IEFadeWithTime>c__Iterator0::.ctor()
extern "C"  void U3CIEFadeWithTimeU3Ec__Iterator0__ctor_m2891487814 (U3CIEFadeWithTimeU3Ec__Iterator0_t115250393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneController/<IEFadeWithTime>c__Iterator0::MoveNext()
extern "C"  bool U3CIEFadeWithTimeU3Ec__Iterator0_MoveNext_m853928166 (U3CIEFadeWithTimeU3Ec__Iterator0_t115250393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SceneController/<IEFadeWithTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIEFadeWithTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3224266722 (U3CIEFadeWithTimeU3Ec__Iterator0_t115250393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SceneController/<IEFadeWithTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIEFadeWithTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1337912042 (U3CIEFadeWithTimeU3Ec__Iterator0_t115250393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneController/<IEFadeWithTime>c__Iterator0::Dispose()
extern "C"  void U3CIEFadeWithTimeU3Ec__Iterator0_Dispose_m3161119003 (U3CIEFadeWithTimeU3Ec__Iterator0_t115250393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneController/<IEFadeWithTime>c__Iterator0::Reset()
extern "C"  void U3CIEFadeWithTimeU3Ec__Iterator0_Reset_m1448370037 (U3CIEFadeWithTimeU3Ec__Iterator0_t115250393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
