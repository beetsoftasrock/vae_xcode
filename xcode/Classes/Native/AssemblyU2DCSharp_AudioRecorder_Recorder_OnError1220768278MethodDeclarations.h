﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioRecorder.Recorder/OnError
struct OnError_t1220768278;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void AudioRecorder.Recorder/OnError::.ctor(System.Object,System.IntPtr)
extern "C"  void OnError__ctor_m1767230729 (OnError_t1220768278 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder/OnError::Invoke(System.String)
extern "C"  void OnError_Invoke_m2671052563 (OnError_t1220768278 * __this, String_t* ___err0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AudioRecorder.Recorder/OnError::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnError_BeginInvoke_m1829153016 (OnError_t1220768278 * __this, String_t* ___err0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder/OnError::EndInvoke(System.IAsyncResult)
extern "C"  void OnError_EndInvoke_m2995510659 (OnError_t1220768278 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
