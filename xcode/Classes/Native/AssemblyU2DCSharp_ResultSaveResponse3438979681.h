﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultSaveResponse
struct  ResultSaveResponse_t3438979681  : public Il2CppObject
{
public:
	// System.Boolean ResultSaveResponse::success
	bool ___success_0;
	// System.String ResultSaveResponse::errorMsg
	String_t* ___errorMsg_1;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(ResultSaveResponse_t3438979681, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_errorMsg_1() { return static_cast<int32_t>(offsetof(ResultSaveResponse_t3438979681, ___errorMsg_1)); }
	inline String_t* get_errorMsg_1() const { return ___errorMsg_1; }
	inline String_t** get_address_of_errorMsg_1() { return &___errorMsg_1; }
	inline void set_errorMsg_1(String_t* value)
	{
		___errorMsg_1 = value;
		Il2CppCodeGenWriteBarrier(&___errorMsg_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
