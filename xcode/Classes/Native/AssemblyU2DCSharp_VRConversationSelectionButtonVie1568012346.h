﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectionButtonSelectedEvent
struct SelectionButtonSelectedEvent_t3088917173;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_ConversationSelectionButtonView265815670.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRConversationSelectionButtonView
struct  VRConversationSelectionButtonView_t1568012346  : public ConversationSelectionButtonView_t265815670
{
public:
	// SelectionButtonSelectedEvent VRConversationSelectionButtonView::onButtonSelected
	SelectionButtonSelectedEvent_t3088917173 * ___onButtonSelected_5;
	// UnityEngine.UI.Text VRConversationSelectionButtonView::infoText
	Text_t356221433 * ___infoText_6;

public:
	inline static int32_t get_offset_of_onButtonSelected_5() { return static_cast<int32_t>(offsetof(VRConversationSelectionButtonView_t1568012346, ___onButtonSelected_5)); }
	inline SelectionButtonSelectedEvent_t3088917173 * get_onButtonSelected_5() const { return ___onButtonSelected_5; }
	inline SelectionButtonSelectedEvent_t3088917173 ** get_address_of_onButtonSelected_5() { return &___onButtonSelected_5; }
	inline void set_onButtonSelected_5(SelectionButtonSelectedEvent_t3088917173 * value)
	{
		___onButtonSelected_5 = value;
		Il2CppCodeGenWriteBarrier(&___onButtonSelected_5, value);
	}

	inline static int32_t get_offset_of_infoText_6() { return static_cast<int32_t>(offsetof(VRConversationSelectionButtonView_t1568012346, ___infoText_6)); }
	inline Text_t356221433 * get_infoText_6() const { return ___infoText_6; }
	inline Text_t356221433 ** get_address_of_infoText_6() { return &___infoText_6; }
	inline void set_infoText_6(Text_t356221433 * value)
	{
		___infoText_6 = value;
		Il2CppCodeGenWriteBarrier(&___infoText_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
