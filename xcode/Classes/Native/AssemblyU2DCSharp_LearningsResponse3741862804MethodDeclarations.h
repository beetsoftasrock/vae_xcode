﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearningsResponse
struct LearningsResponse_t3741862804;

#include "codegen/il2cpp-codegen.h"

// System.Void LearningsResponse::.ctor()
extern "C"  void LearningsResponse__ctor_m1827870475 (LearningsResponse_t3741862804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
