﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.ManagerInstance/<Stop>c__AnonStorey3
struct U3CStopU3Ec__AnonStorey3_t203410663;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"

// System.Void Beetsoft.VAE.ManagerInstance/<Stop>c__AnonStorey3::.ctor()
extern "C"  void U3CStopU3Ec__AnonStorey3__ctor_m2400885368 (U3CStopU3Ec__AnonStorey3_t203410663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.ManagerInstance/<Stop>c__AnonStorey3::<>m__0(UnityEngine.AudioSource)
extern "C"  bool U3CStopU3Ec__AnonStorey3_U3CU3Em__0_m3930023621 (U3CStopU3Ec__AnonStorey3_t203410663 * __this, AudioSource_t1135106623 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
