﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonAutoPlay
struct ButtonAutoPlay_t2268545083;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ButtonAutoPlay::.ctor()
extern "C"  void ButtonAutoPlay__ctor_m2778120088 (ButtonAutoPlay_t2268545083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAutoPlay::AutoPlayConversation()
extern "C"  void ButtonAutoPlay_AutoPlayConversation_m2912822372 (ButtonAutoPlay_t2268545083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAutoPlay::AutoPlayTemplateConversation(System.String)
extern "C"  void ButtonAutoPlay_AutoPlayTemplateConversation_m3057760308 (ButtonAutoPlay_t2268545083 * __this, String_t* ___scene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
