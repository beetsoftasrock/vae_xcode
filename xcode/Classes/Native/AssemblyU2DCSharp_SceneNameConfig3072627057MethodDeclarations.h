﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneNameConfig
struct SceneNameConfig_t3072627057;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneNameConfig::.ctor()
extern "C"  void SceneNameConfig__ctor_m2585061088 (SceneNameConfig_t3072627057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneNameConfig::.cctor()
extern "C"  void SceneNameConfig__cctor_m2642128555 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
