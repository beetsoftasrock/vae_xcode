﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0
struct U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::.ctor()
extern "C"  void U3CShowPlayerTalkDialogU3Ec__Iterator0__ctor_m2773063122 (U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::MoveNext()
extern "C"  bool U3CShowPlayerTalkDialogU3Ec__Iterator0_MoveNext_m206882534 (U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowPlayerTalkDialogU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3869346184 (U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowPlayerTalkDialogU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m421302000 (U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::Dispose()
extern "C"  void U3CShowPlayerTalkDialogU3Ec__Iterator0_Dispose_m2895078657 (U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::Reset()
extern "C"  void U3CShowPlayerTalkDialogU3Ec__Iterator0_Reset_m3488887647 (U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
