﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KaraokeTextEffect
struct KaraokeTextEffect_t1279919556;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "System_Core_System_Action3226471752.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void KaraokeTextEffect::.ctor()
extern "C"  void KaraokeTextEffect__ctor_m2232503333 (KaraokeTextEffect_t1279919556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KaraokeTextEffect::SetTextValue(System.String)
extern "C"  void KaraokeTextEffect_SetTextValue_m1498720419 (KaraokeTextEffect_t1279919556 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Text KaraokeTextEffect::get_mainText()
extern "C"  Text_t356221433 * KaraokeTextEffect_get_mainText_m2487885529 (KaraokeTextEffect_t1279919556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Text KaraokeTextEffect::get_effectText()
extern "C"  Text_t356221433 * KaraokeTextEffect_get_effectText_m4141963149 (KaraokeTextEffect_t1279919556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KaraokeTextEffect::set_effectText(UnityEngine.UI.Text)
extern "C"  void KaraokeTextEffect_set_effectText_m2874607020 (KaraokeTextEffect_t1279919556 * __this, Text_t356221433 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KaraokeTextEffect::Awake()
extern "C"  void KaraokeTextEffect_Awake_m2442724564 (KaraokeTextEffect_t1279919556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KaraokeTextEffect::CreateTextKaraoke()
extern "C"  void KaraokeTextEffect_CreateTextKaraoke_m1318349036 (KaraokeTextEffect_t1279919556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KaraokeTextEffect::StartKaraoke(UnityEngine.AudioSource)
extern "C"  void KaraokeTextEffect_StartKaraoke_m1383145677 (KaraokeTextEffect_t1279919556 * __this, AudioSource_t1135106623 * ___audioSource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KaraokeTextEffect::StartKaraoke(System.Single,System.Action)
extern "C"  void KaraokeTextEffect_StartKaraoke_m1265459677 (KaraokeTextEffect_t1279919556 * __this, float ___time0, Action_t3226471752 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator KaraokeTextEffect::PlayEffect(System.Single,System.Action)
extern "C"  Il2CppObject * KaraokeTextEffect_PlayEffect_m3560650492 (KaraokeTextEffect_t1279919556 * __this, float ___time0, Action_t3226471752 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String KaraokeTextEffect::ReplaceAt(System.String,System.Int32,System.String)
extern "C"  String_t* KaraokeTextEffect_ReplaceAt_m35250454 (KaraokeTextEffect_t1279919556 * __this, String_t* ___input0, int32_t ___index1, String_t* ___newChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String KaraokeTextEffect::ToRGBHex(UnityEngine.Color)
extern "C"  String_t* KaraokeTextEffect_ToRGBHex_m4027716587 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte KaraokeTextEffect::ToByte(System.Single)
extern "C"  uint8_t KaraokeTextEffect_ToByte_m1360153123 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
