﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.SoundManager/<LoadSoundBundle>c__AnonStorey0
struct  U3CLoadSoundBundleU3Ec__AnonStorey0_t1073847543  : public Il2CppObject
{
public:
	// System.String Beetsoft.VAE.SoundManager/<LoadSoundBundle>c__AnonStorey0::bundleName
	String_t* ___bundleName_0;

public:
	inline static int32_t get_offset_of_bundleName_0() { return static_cast<int32_t>(offsetof(U3CLoadSoundBundleU3Ec__AnonStorey0_t1073847543, ___bundleName_0)); }
	inline String_t* get_bundleName_0() const { return ___bundleName_0; }
	inline String_t** get_address_of_bundleName_0() { return &___bundleName_0; }
	inline void set_bundleName_0(String_t* value)
	{
		___bundleName_0 = value;
		Il2CppCodeGenWriteBarrier(&___bundleName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
