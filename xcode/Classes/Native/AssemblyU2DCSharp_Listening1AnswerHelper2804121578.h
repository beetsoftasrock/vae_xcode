﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// com.beetsoft.GUIElements.SelectableItem
struct SelectableItem_t2941161729;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening1AnswerHelper
struct  Listening1AnswerHelper_t2804121578  : public MonoBehaviour_t1158329972
{
public:
	// com.beetsoft.GUIElements.SelectableItem Listening1AnswerHelper::item
	SelectableItem_t2941161729 * ___item_2;

public:
	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(Listening1AnswerHelper_t2804121578, ___item_2)); }
	inline SelectableItem_t2941161729 * get_item_2() const { return ___item_2; }
	inline SelectableItem_t2941161729 ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(SelectableItem_t2941161729 * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier(&___item_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
