﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationSelectionGroupData
struct ConversationSelectionGroupData_t3437723178;
// ConversationSelectionDialogVR
struct ConversationSelectionDialogVR_t87316163;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSelectionDialogVR/<ShowSelection>c__Iterator0
struct  U3CShowSelectionU3Ec__Iterator0_t892918678  : public Il2CppObject
{
public:
	// ConversationSelectionGroupData ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::gdata
	ConversationSelectionGroupData_t3437723178 * ___gdata_0;
	// ConversationSelectionDialogVR ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::$this
	ConversationSelectionDialogVR_t87316163 * ___U24this_1;
	// System.Object ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ConversationSelectionDialogVR/<ShowSelection>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_gdata_0() { return static_cast<int32_t>(offsetof(U3CShowSelectionU3Ec__Iterator0_t892918678, ___gdata_0)); }
	inline ConversationSelectionGroupData_t3437723178 * get_gdata_0() const { return ___gdata_0; }
	inline ConversationSelectionGroupData_t3437723178 ** get_address_of_gdata_0() { return &___gdata_0; }
	inline void set_gdata_0(ConversationSelectionGroupData_t3437723178 * value)
	{
		___gdata_0 = value;
		Il2CppCodeGenWriteBarrier(&___gdata_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowSelectionU3Ec__Iterator0_t892918678, ___U24this_1)); }
	inline ConversationSelectionDialogVR_t87316163 * get_U24this_1() const { return ___U24this_1; }
	inline ConversationSelectionDialogVR_t87316163 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ConversationSelectionDialogVR_t87316163 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowSelectionU3Ec__Iterator0_t892918678, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowSelectionU3Ec__Iterator0_t892918678, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowSelectionU3Ec__Iterator0_t892918678, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
