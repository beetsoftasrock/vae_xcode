﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionScenePopup
struct  QuestionScenePopup_t792224618  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image QuestionScenePopup::correctImage
	Image_t2042527209 * ___correctImage_2;
	// UnityEngine.UI.Image QuestionScenePopup::notCorrectImage
	Image_t2042527209 * ___notCorrectImage_3;
	// UnityEngine.UI.Image QuestionScenePopup::notPerfectCorrectImage
	Image_t2042527209 * ___notPerfectCorrectImage_4;
	// UnityEngine.Events.UnityEvent QuestionScenePopup::onResultPopupClosed
	UnityEvent_t408735097 * ___onResultPopupClosed_5;
	// UnityEngine.GameObject QuestionScenePopup::resultPopup
	GameObject_t1756533147 * ___resultPopup_6;
	// UnityEngine.AnimationClip QuestionScenePopup::animResultOpen
	AnimationClip_t3510324950 * ___animResultOpen_7;
	// UnityEngine.AnimationClip QuestionScenePopup::animResultClose
	AnimationClip_t3510324950 * ___animResultClose_8;

public:
	inline static int32_t get_offset_of_correctImage_2() { return static_cast<int32_t>(offsetof(QuestionScenePopup_t792224618, ___correctImage_2)); }
	inline Image_t2042527209 * get_correctImage_2() const { return ___correctImage_2; }
	inline Image_t2042527209 ** get_address_of_correctImage_2() { return &___correctImage_2; }
	inline void set_correctImage_2(Image_t2042527209 * value)
	{
		___correctImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___correctImage_2, value);
	}

	inline static int32_t get_offset_of_notCorrectImage_3() { return static_cast<int32_t>(offsetof(QuestionScenePopup_t792224618, ___notCorrectImage_3)); }
	inline Image_t2042527209 * get_notCorrectImage_3() const { return ___notCorrectImage_3; }
	inline Image_t2042527209 ** get_address_of_notCorrectImage_3() { return &___notCorrectImage_3; }
	inline void set_notCorrectImage_3(Image_t2042527209 * value)
	{
		___notCorrectImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___notCorrectImage_3, value);
	}

	inline static int32_t get_offset_of_notPerfectCorrectImage_4() { return static_cast<int32_t>(offsetof(QuestionScenePopup_t792224618, ___notPerfectCorrectImage_4)); }
	inline Image_t2042527209 * get_notPerfectCorrectImage_4() const { return ___notPerfectCorrectImage_4; }
	inline Image_t2042527209 ** get_address_of_notPerfectCorrectImage_4() { return &___notPerfectCorrectImage_4; }
	inline void set_notPerfectCorrectImage_4(Image_t2042527209 * value)
	{
		___notPerfectCorrectImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___notPerfectCorrectImage_4, value);
	}

	inline static int32_t get_offset_of_onResultPopupClosed_5() { return static_cast<int32_t>(offsetof(QuestionScenePopup_t792224618, ___onResultPopupClosed_5)); }
	inline UnityEvent_t408735097 * get_onResultPopupClosed_5() const { return ___onResultPopupClosed_5; }
	inline UnityEvent_t408735097 ** get_address_of_onResultPopupClosed_5() { return &___onResultPopupClosed_5; }
	inline void set_onResultPopupClosed_5(UnityEvent_t408735097 * value)
	{
		___onResultPopupClosed_5 = value;
		Il2CppCodeGenWriteBarrier(&___onResultPopupClosed_5, value);
	}

	inline static int32_t get_offset_of_resultPopup_6() { return static_cast<int32_t>(offsetof(QuestionScenePopup_t792224618, ___resultPopup_6)); }
	inline GameObject_t1756533147 * get_resultPopup_6() const { return ___resultPopup_6; }
	inline GameObject_t1756533147 ** get_address_of_resultPopup_6() { return &___resultPopup_6; }
	inline void set_resultPopup_6(GameObject_t1756533147 * value)
	{
		___resultPopup_6 = value;
		Il2CppCodeGenWriteBarrier(&___resultPopup_6, value);
	}

	inline static int32_t get_offset_of_animResultOpen_7() { return static_cast<int32_t>(offsetof(QuestionScenePopup_t792224618, ___animResultOpen_7)); }
	inline AnimationClip_t3510324950 * get_animResultOpen_7() const { return ___animResultOpen_7; }
	inline AnimationClip_t3510324950 ** get_address_of_animResultOpen_7() { return &___animResultOpen_7; }
	inline void set_animResultOpen_7(AnimationClip_t3510324950 * value)
	{
		___animResultOpen_7 = value;
		Il2CppCodeGenWriteBarrier(&___animResultOpen_7, value);
	}

	inline static int32_t get_offset_of_animResultClose_8() { return static_cast<int32_t>(offsetof(QuestionScenePopup_t792224618, ___animResultClose_8)); }
	inline AnimationClip_t3510324950 * get_animResultClose_8() const { return ___animResultClose_8; }
	inline AnimationClip_t3510324950 ** get_address_of_animResultClose_8() { return &___animResultClose_8; }
	inline void set_animResultClose_8(AnimationClip_t3510324950 * value)
	{
		___animResultClose_8 = value;
		Il2CppCodeGenWriteBarrier(&___animResultClose_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
