﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoiceRecordingDialog/<Play>c__Iterator0
struct U3CPlayU3Ec__Iterator0_t2469087745;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VoiceRecordingDialog/<Play>c__Iterator0::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator0__ctor_m920957224 (U3CPlayU3Ec__Iterator0_t2469087745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoiceRecordingDialog/<Play>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayU3Ec__Iterator0_MoveNext_m372531244 (U3CPlayU3Ec__Iterator0_t2469087745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoiceRecordingDialog/<Play>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4160558642 (U3CPlayU3Ec__Iterator0_t2469087745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoiceRecordingDialog/<Play>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1789636122 (U3CPlayU3Ec__Iterator0_t2469087745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordingDialog/<Play>c__Iterator0::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator0_Dispose_m2301501515 (U3CPlayU3Ec__Iterator0_t2469087745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordingDialog/<Play>c__Iterator0::Reset()
extern "C"  void U3CPlayU3Ec__Iterator0_Reset_m632822829 (U3CPlayU3Ec__Iterator0_t2469087745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
