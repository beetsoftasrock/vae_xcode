﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ConversationSelectionData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4096971458(__this, ___l0, method) ((  void (*) (Enumerator_t2993859341 *, List_1_t3459129667 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ConversationSelectionData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3274047024(__this, method) ((  void (*) (Enumerator_t2993859341 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ConversationSelectionData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2332533148(__this, method) ((  Il2CppObject * (*) (Enumerator_t2993859341 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ConversationSelectionData>::Dispose()
#define Enumerator_Dispose_m616854575(__this, method) ((  void (*) (Enumerator_t2993859341 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ConversationSelectionData>::VerifyState()
#define Enumerator_VerifyState_m462443224(__this, method) ((  void (*) (Enumerator_t2993859341 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ConversationSelectionData>::MoveNext()
#define Enumerator_MoveNext_m838006912(__this, method) ((  bool (*) (Enumerator_t2993859341 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ConversationSelectionData>::get_Current()
#define Enumerator_get_Current_m3155188311(__this, method) ((  ConversationSelectionData_t4090008535 * (*) (Enumerator_t2993859341 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
