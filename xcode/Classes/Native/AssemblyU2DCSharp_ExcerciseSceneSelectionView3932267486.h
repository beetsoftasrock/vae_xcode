﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// OnExcerciseSceneSelectionViewClickedEvent
struct OnExcerciseSceneSelectionViewClickedEvent_t3939024814;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExcerciseSceneSelectionView
struct  ExcerciseSceneSelectionView_t3932267486  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ExcerciseSceneSelectionView::sceneId
	int32_t ___sceneId_2;
	// UnityEngine.Sprite ExcerciseSceneSelectionView::selectedSprite
	Sprite_t309593783 * ___selectedSprite_3;
	// UnityEngine.Sprite ExcerciseSceneSelectionView::normalSprite
	Sprite_t309593783 * ___normalSprite_4;
	// UnityEngine.UI.Text ExcerciseSceneSelectionView::levelName
	Text_t356221433 * ___levelName_5;
	// UnityEngine.UI.Text ExcerciseSceneSelectionView::levelCompleted
	Text_t356221433 * ___levelCompleted_6;
	// UnityEngine.UI.Image ExcerciseSceneSelectionView::levelFill
	Image_t2042527209 * ___levelFill_7;
	// System.Boolean ExcerciseSceneSelectionView::_selected
	bool ____selected_8;
	// System.Single ExcerciseSceneSelectionView::_completed
	float ____completed_9;
	// OnExcerciseSceneSelectionViewClickedEvent ExcerciseSceneSelectionView::onViewClicked
	OnExcerciseSceneSelectionViewClickedEvent_t3939024814 * ___onViewClicked_10;

public:
	inline static int32_t get_offset_of_sceneId_2() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ___sceneId_2)); }
	inline int32_t get_sceneId_2() const { return ___sceneId_2; }
	inline int32_t* get_address_of_sceneId_2() { return &___sceneId_2; }
	inline void set_sceneId_2(int32_t value)
	{
		___sceneId_2 = value;
	}

	inline static int32_t get_offset_of_selectedSprite_3() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ___selectedSprite_3)); }
	inline Sprite_t309593783 * get_selectedSprite_3() const { return ___selectedSprite_3; }
	inline Sprite_t309593783 ** get_address_of_selectedSprite_3() { return &___selectedSprite_3; }
	inline void set_selectedSprite_3(Sprite_t309593783 * value)
	{
		___selectedSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectedSprite_3, value);
	}

	inline static int32_t get_offset_of_normalSprite_4() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ___normalSprite_4)); }
	inline Sprite_t309593783 * get_normalSprite_4() const { return ___normalSprite_4; }
	inline Sprite_t309593783 ** get_address_of_normalSprite_4() { return &___normalSprite_4; }
	inline void set_normalSprite_4(Sprite_t309593783 * value)
	{
		___normalSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___normalSprite_4, value);
	}

	inline static int32_t get_offset_of_levelName_5() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ___levelName_5)); }
	inline Text_t356221433 * get_levelName_5() const { return ___levelName_5; }
	inline Text_t356221433 ** get_address_of_levelName_5() { return &___levelName_5; }
	inline void set_levelName_5(Text_t356221433 * value)
	{
		___levelName_5 = value;
		Il2CppCodeGenWriteBarrier(&___levelName_5, value);
	}

	inline static int32_t get_offset_of_levelCompleted_6() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ___levelCompleted_6)); }
	inline Text_t356221433 * get_levelCompleted_6() const { return ___levelCompleted_6; }
	inline Text_t356221433 ** get_address_of_levelCompleted_6() { return &___levelCompleted_6; }
	inline void set_levelCompleted_6(Text_t356221433 * value)
	{
		___levelCompleted_6 = value;
		Il2CppCodeGenWriteBarrier(&___levelCompleted_6, value);
	}

	inline static int32_t get_offset_of_levelFill_7() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ___levelFill_7)); }
	inline Image_t2042527209 * get_levelFill_7() const { return ___levelFill_7; }
	inline Image_t2042527209 ** get_address_of_levelFill_7() { return &___levelFill_7; }
	inline void set_levelFill_7(Image_t2042527209 * value)
	{
		___levelFill_7 = value;
		Il2CppCodeGenWriteBarrier(&___levelFill_7, value);
	}

	inline static int32_t get_offset_of__selected_8() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ____selected_8)); }
	inline bool get__selected_8() const { return ____selected_8; }
	inline bool* get_address_of__selected_8() { return &____selected_8; }
	inline void set__selected_8(bool value)
	{
		____selected_8 = value;
	}

	inline static int32_t get_offset_of__completed_9() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ____completed_9)); }
	inline float get__completed_9() const { return ____completed_9; }
	inline float* get_address_of__completed_9() { return &____completed_9; }
	inline void set__completed_9(float value)
	{
		____completed_9 = value;
	}

	inline static int32_t get_offset_of_onViewClicked_10() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelectionView_t3932267486, ___onViewClicked_10)); }
	inline OnExcerciseSceneSelectionViewClickedEvent_t3939024814 * get_onViewClicked_10() const { return ___onViewClicked_10; }
	inline OnExcerciseSceneSelectionViewClickedEvent_t3939024814 ** get_address_of_onViewClicked_10() { return &___onViewClicked_10; }
	inline void set_onViewClicked_10(OnExcerciseSceneSelectionViewClickedEvent_t3939024814 * value)
	{
		___onViewClicked_10 = value;
		Il2CppCodeGenWriteBarrier(&___onViewClicked_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
