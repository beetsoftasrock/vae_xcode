﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// OnConversationSceneSelectionViewClickedEvent
struct OnConversationSceneSelectionViewClickedEvent_t1625773638;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSceneSelectionView
struct  ConversationSceneSelectionView_t407867144  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ConversationSceneSelectionView::sceneId
	int32_t ___sceneId_2;
	// UnityEngine.UI.Text ConversationSceneSelectionView::levelText
	Text_t356221433 * ___levelText_3;
	// UnityEngine.UI.Text ConversationSceneSelectionView::modeText
	Text_t356221433 * ___modeText_4;
	// UnityEngine.UI.Text ConversationSceneSelectionView::subText
	Text_t356221433 * ___subText_5;
	// UnityEngine.UI.Image ConversationSceneSelectionView::backGroundImage
	Image_t2042527209 * ___backGroundImage_6;
	// UnityEngine.UI.Image ConversationSceneSelectionView::backgroundImageMode0
	Image_t2042527209 * ___backgroundImageMode0_7;
	// UnityEngine.UI.Image ConversationSceneSelectionView::backgroundImageMode1
	Image_t2042527209 * ___backgroundImageMode1_8;
	// UnityEngine.Sprite ConversationSceneSelectionView::backgroundSelected
	Sprite_t309593783 * ___backgroundSelected_9;
	// UnityEngine.Sprite ConversationSceneSelectionView::backgroundDefault
	Sprite_t309593783 * ___backgroundDefault_10;
	// System.Boolean ConversationSceneSelectionView::_selected
	bool ____selected_11;
	// OnConversationSceneSelectionViewClickedEvent ConversationSceneSelectionView::onViewClicked
	OnConversationSceneSelectionViewClickedEvent_t1625773638 * ___onViewClicked_12;

public:
	inline static int32_t get_offset_of_sceneId_2() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___sceneId_2)); }
	inline int32_t get_sceneId_2() const { return ___sceneId_2; }
	inline int32_t* get_address_of_sceneId_2() { return &___sceneId_2; }
	inline void set_sceneId_2(int32_t value)
	{
		___sceneId_2 = value;
	}

	inline static int32_t get_offset_of_levelText_3() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___levelText_3)); }
	inline Text_t356221433 * get_levelText_3() const { return ___levelText_3; }
	inline Text_t356221433 ** get_address_of_levelText_3() { return &___levelText_3; }
	inline void set_levelText_3(Text_t356221433 * value)
	{
		___levelText_3 = value;
		Il2CppCodeGenWriteBarrier(&___levelText_3, value);
	}

	inline static int32_t get_offset_of_modeText_4() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___modeText_4)); }
	inline Text_t356221433 * get_modeText_4() const { return ___modeText_4; }
	inline Text_t356221433 ** get_address_of_modeText_4() { return &___modeText_4; }
	inline void set_modeText_4(Text_t356221433 * value)
	{
		___modeText_4 = value;
		Il2CppCodeGenWriteBarrier(&___modeText_4, value);
	}

	inline static int32_t get_offset_of_subText_5() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___subText_5)); }
	inline Text_t356221433 * get_subText_5() const { return ___subText_5; }
	inline Text_t356221433 ** get_address_of_subText_5() { return &___subText_5; }
	inline void set_subText_5(Text_t356221433 * value)
	{
		___subText_5 = value;
		Il2CppCodeGenWriteBarrier(&___subText_5, value);
	}

	inline static int32_t get_offset_of_backGroundImage_6() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___backGroundImage_6)); }
	inline Image_t2042527209 * get_backGroundImage_6() const { return ___backGroundImage_6; }
	inline Image_t2042527209 ** get_address_of_backGroundImage_6() { return &___backGroundImage_6; }
	inline void set_backGroundImage_6(Image_t2042527209 * value)
	{
		___backGroundImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___backGroundImage_6, value);
	}

	inline static int32_t get_offset_of_backgroundImageMode0_7() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___backgroundImageMode0_7)); }
	inline Image_t2042527209 * get_backgroundImageMode0_7() const { return ___backgroundImageMode0_7; }
	inline Image_t2042527209 ** get_address_of_backgroundImageMode0_7() { return &___backgroundImageMode0_7; }
	inline void set_backgroundImageMode0_7(Image_t2042527209 * value)
	{
		___backgroundImageMode0_7 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImageMode0_7, value);
	}

	inline static int32_t get_offset_of_backgroundImageMode1_8() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___backgroundImageMode1_8)); }
	inline Image_t2042527209 * get_backgroundImageMode1_8() const { return ___backgroundImageMode1_8; }
	inline Image_t2042527209 ** get_address_of_backgroundImageMode1_8() { return &___backgroundImageMode1_8; }
	inline void set_backgroundImageMode1_8(Image_t2042527209 * value)
	{
		___backgroundImageMode1_8 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImageMode1_8, value);
	}

	inline static int32_t get_offset_of_backgroundSelected_9() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___backgroundSelected_9)); }
	inline Sprite_t309593783 * get_backgroundSelected_9() const { return ___backgroundSelected_9; }
	inline Sprite_t309593783 ** get_address_of_backgroundSelected_9() { return &___backgroundSelected_9; }
	inline void set_backgroundSelected_9(Sprite_t309593783 * value)
	{
		___backgroundSelected_9 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSelected_9, value);
	}

	inline static int32_t get_offset_of_backgroundDefault_10() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___backgroundDefault_10)); }
	inline Sprite_t309593783 * get_backgroundDefault_10() const { return ___backgroundDefault_10; }
	inline Sprite_t309593783 ** get_address_of_backgroundDefault_10() { return &___backgroundDefault_10; }
	inline void set_backgroundDefault_10(Sprite_t309593783 * value)
	{
		___backgroundDefault_10 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundDefault_10, value);
	}

	inline static int32_t get_offset_of__selected_11() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ____selected_11)); }
	inline bool get__selected_11() const { return ____selected_11; }
	inline bool* get_address_of__selected_11() { return &____selected_11; }
	inline void set__selected_11(bool value)
	{
		____selected_11 = value;
	}

	inline static int32_t get_offset_of_onViewClicked_12() { return static_cast<int32_t>(offsetof(ConversationSceneSelectionView_t407867144, ___onViewClicked_12)); }
	inline OnConversationSceneSelectionViewClickedEvent_t1625773638 * get_onViewClicked_12() const { return ___onViewClicked_12; }
	inline OnConversationSceneSelectionViewClickedEvent_t1625773638 ** get_address_of_onViewClicked_12() { return &___onViewClicked_12; }
	inline void set_onViewClicked_12(OnConversationSceneSelectionViewClickedEvent_t1625773638 * value)
	{
		___onViewClicked_12 = value;
		Il2CppCodeGenWriteBarrier(&___onViewClicked_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
