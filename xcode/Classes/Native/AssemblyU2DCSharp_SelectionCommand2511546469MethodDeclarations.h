﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectionCommand
struct SelectionCommand_t2511546469;
// System.String
struct String_t;
// ConversationSelectionGroupData
struct ConversationSelectionGroupData_t3437723178;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationLogicController
struct ConversationLogicController_t3911886281;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ConversationSelectionGroupData3437723178.h"
#include "AssemblyU2DCSharp_ConversationLogicController3911886281.h"

// System.Void SelectionCommand::.ctor(System.String,ConversationSelectionGroupData)
extern "C"  void SelectionCommand__ctor_m812899728 (SelectionCommand_t2511546469 * __this, String_t* ___role0, ConversationSelectionGroupData_t3437723178 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SelectionCommand::AutoExecute(ConversationLogicController)
extern "C"  Il2CppObject * SelectionCommand_AutoExecute_m4105476141 (SelectionCommand_t2511546469 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SelectionCommand::GetRole()
extern "C"  String_t* SelectionCommand_GetRole_m3987739359 (SelectionCommand_t2511546469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SelectionCommand::Execute(ConversationLogicController)
extern "C"  Il2CppObject * SelectionCommand_Execute_m1575489596 (SelectionCommand_t2511546469 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionCommand::Revert(ConversationLogicController)
extern "C"  void SelectionCommand_Revert_m2783863105 (SelectionCommand_t2511546469 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
