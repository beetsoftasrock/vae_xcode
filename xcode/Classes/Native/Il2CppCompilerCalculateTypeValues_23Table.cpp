﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationUISettingFactory3134705897.h"
#include "AssemblyU2DCSharp_PrelearningSceneFactory2112513415.h"
#include "AssemblyU2DCSharp_BaseChapterSettingIniter2856709253.h"
#include "AssemblyU2DCSharp_ChapterTopSettingIniter1747874135.h"
#include "AssemblyU2DCSharp_ConversationUISettingIniter3474597698.h"
#include "AssemblyU2DCSharp_PreLearningSettingIniter1479780356.h"
#include "AssemblyU2DCSharp_ConstantConfig1104991690.h"
#include "AssemblyU2DCSharp_DataStoreKeyConfig878099218.h"
#include "AssemblyU2DCSharp_SceneNameConfig3072627057.h"
#include "AssemblyU2DCSharp_VaeBuildSetting3353517300.h"
#include "AssemblyU2DCSharp_DrawGui872751393.h"
#include "AssemblyU2DCSharp_EditorLoadVRBackground1201325975.h"
#include "AssemblyU2DCSharp_ClickConfirmAfterConversationCom3062069762.h"
#include "AssemblyU2DCSharp_CharacterAnimCommand3374959163.h"
#include "AssemblyU2DCSharp_CharacterAnimCommand_U3CAutoExec1321509635.h"
#include "AssemblyU2DCSharp_CharacterAnimCommand_U3CExecuteU3319037821.h"
#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"
#include "AssemblyU2DCSharp_EffectCommand708799566.h"
#include "AssemblyU2DCSharp_EffectCommand_U3CAutoExecuteU3Ec_513472262.h"
#include "AssemblyU2DCSharp_EffectCommand_U3CExecuteU3Ec__Ite564345066.h"
#include "AssemblyU2DCSharp_GoToCommand2985371124.h"
#include "AssemblyU2DCSharp_GoToCommand_U3CAutoExecuteU3Ec__I849620780.h"
#include "AssemblyU2DCSharp_GoToCommand_U3CExecuteU3Ec__Iter1995833048.h"
#include "AssemblyU2DCSharp_GLQuart478788054.h"
#include "AssemblyU2DCSharp_Graph2735713688.h"
#include "AssemblyU2DCSharp_Graph_U3CLerpChangeDistanceU3Ec_1572238142.h"
#include "AssemblyU2DCSharp_GraphValue2739901601.h"
#include "AssemblyU2DCSharp_MyPageChapterController2166380602.h"
#include "AssemblyU2DCSharp_MyPageChapterView1534782717.h"
#include "AssemblyU2DCSharp_MyPageChapterView_ViewTextUI4118675828.h"
#include "AssemblyU2DCSharp_LessonValue1076530945.h"
#include "AssemblyU2DCSharp_MyPageController1695642829.h"
#include "AssemblyU2DCSharp_MyPageView3400191636.h"
#include "AssemblyU2DCSharp_Cell3051913968.h"
#include "AssemblyU2DCSharp_Column1930583302.h"
#include "AssemblyU2DCSharp_Table4160395208.h"
#include "AssemblyU2DCSharp_LessionName1201611902.h"
#include "AssemblyU2DCSharp_RunMode3692523130.h"
#include "AssemblyU2DCSharp_ChapterMyPageScript3593529611.h"
#include "AssemblyU2DCSharp_ChapterMyPageScript_U3CwaitFadeO1364814582.h"
#include "AssemblyU2DCSharp_ChapterResultSettingLoader4141859061.h"
#include "AssemblyU2DCSharp_MyPageManagerScript3054960093.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_UIPoly4199244354.h"
#include "AssemblyU2DCSharp_SelectionCommand2511546469.h"
#include "AssemblyU2DCSharp_SelectionCommand_U3CAutoExecuteU1017565241.h"
#include "AssemblyU2DCSharp_SelectionCommand_U3CExecuteU3Ec__191178719.h"
#include "AssemblyU2DCSharp_SoundCommand2356851872.h"
#include "AssemblyU2DCSharp_SoundCommand_U3CAutoExecuteU3Ec_3994390064.h"
#include "AssemblyU2DCSharp_SoundCommand_U3CExecuteU3Ec__Iter748120696.h"
#include "AssemblyU2DCSharp_StopEffectCommand3016203262.h"
#include "AssemblyU2DCSharp_StopEffectCommand_U3CExecuteU3Ec_386014617.h"
#include "AssemblyU2DCSharp_StopEffectCommand_U3CAutoExecute2553865167.h"
#include "AssemblyU2DCSharp_TalkCommand2292920015.h"
#include "AssemblyU2DCSharp_TalkCommand_U3CAutoExecuteU3Ec__3903119071.h"
#include "AssemblyU2DCSharp_TalkCommand_U3CExecuteU3Ec__Iter3431233089.h"
#include "AssemblyU2DCSharp_BaseConversationCommandLoader1091304408.h"
#include "AssemblyU2DCSharp_BaseConversationCommandLoader_U3C144537213.h"
#include "AssemblyU2DCSharp_CommandLoader2460237222.h"
#include "AssemblyU2DCSharp_ConversationCommandLoader41408221.h"
#include "AssemblyU2DCSharp_ConversationCommandLoader_U3CGet3009732323.h"
#include "AssemblyU2DCSharp_ExcerciseCommandLoader1886015525.h"
#include "AssemblyU2DCSharp_ExcerciseCommandLoader_U3CLoadLe2141008911.h"
#include "AssemblyU2DCSharp_PracticeCommandLoader3901273799.h"
#include "AssemblyU2DCSharp_BaseConversationController808907754.h"
#include "AssemblyU2DCSharp_BaseConversationController_Conve1349939935.h"
#include "AssemblyU2DCSharp_NewConversationSceneController3024536561.h"
#include "AssemblyU2DCSharp_NewExerciseSceneController2240697240.h"
#include "AssemblyU2DCSharp_NewPracticeSceneController1536734399.h"
#include "AssemblyU2DCSharp_ConversationLogicController3911886281.h"
#include "AssemblyU2DCSharp_ConversationLogicController_Conv3360496668.h"
#include "AssemblyU2DCSharp_ConversationLogicController_Conv2954138412.h"
#include "AssemblyU2DCSharp_ConversationView3413307054.h"
#include "AssemblyU2DCSharp_ConversationSelectionData4090008535.h"
#include "AssemblyU2DCSharp_ConversationSelectionGroupData3437723178.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "AssemblyU2DCSharp_ConversationDataSaver1206770280.h"
#include "AssemblyU2DCSharp_ConversationSceneDataSaver1575220710.h"
#include "AssemblyU2DCSharp_ExerciseSceneDataSaver2652365465.h"
#include "AssemblyU2DCSharp_PracticeSceneDataSaver1376690392.h"
#include "AssemblyU2DCSharp_HistoryLogModule1965887714.h"
#include "AssemblyU2DCSharp_HistoryLogModule_U3CAppendTalkDi2635638849.h"
#include "AssemblyU2DCSharp_HistoryLogModule_U3CAppendTalkHi1208165225.h"
#include "AssemblyU2DCSharp_HistoryLogModule_U3CMoveDialogTo1456012015.h"
#include "AssemblyU2DCSharp_InvokerModule4256524346.h"
#include "AssemblyU2DCSharp_InvokerModule_U3CRunToNextPlayerC369331571.h"
#include "AssemblyU2DCSharp_InvokerModule_U3CDoReplayAllComm2213755081.h"
#include "AssemblyU2DCSharp_InvokerModule_U3CDoAutoPlayU3Ec__874955144.h"
#include "AssemblyU2DCSharp_OtherTalkModule2158105276.h"
#include "AssemblyU2DCSharp_OtherTalkModule_U3CShowOtherTalkD245603399.h"
#include "AssemblyU2DCSharp_OtherTalkModule_U3CPlayOtherTalk2517786544.h"
#include "AssemblyU2DCSharp_PlayerSelectionModule1458090443.h"
#include "AssemblyU2DCSharp_PlayerSelectionModule_U3CShowPla4151862990.h"
#include "AssemblyU2DCSharp_PlayerTalkModule3356839145.h"
#include "AssemblyU2DCSharp_PlayerTalkModule_U3CShowPlayerTa3523564055.h"
#include "AssemblyU2DCSharp_PlayerTalkModule_U3CPlaySampleTa1643395947.h"
#include "AssemblyU2DCSharp_PlayerTalkModule_U3CPlayPlayerAu2853721934.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (ConversationUISettingFactory_t3134705897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[27] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (PrelearningSceneFactory_t2112513415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (BaseChapterSettingIniter_t2856709253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[1] = 
{
	BaseChapterSettingIniter_t2856709253::get_offset_of__setting_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (ChapterTopSettingIniter_t1747874135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[1] = 
{
	ChapterTopSettingIniter_t1747874135::get_offset_of_globalConfig_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (ConversationUISettingIniter_t3474597698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[1] = 
{
	ConversationUISettingIniter_t3474597698::get_offset_of_globalConfig_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (PreLearningSettingIniter_t1479780356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[1] = 
{
	PreLearningSettingIniter_t1479780356::get_offset_of_globalConfig_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (ConstantConfig_t1104991690), -1, sizeof(ConstantConfig_t1104991690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2306[17] = 
{
	ConstantConfig_t1104991690_StaticFields::get_offset_of__API_SERVER_LINK_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (DataStoreKeyConfig_t878099218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (SceneNameConfig_t3072627057), -1, sizeof(SceneNameConfig_t3072627057_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[21] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SceneNameConfig_t3072627057_StaticFields::get_offset_of_NEW_VR_SCENCE_CALL_19(),
	SceneNameConfig_t3072627057_StaticFields::get_offset_of_VR_CHAPTERTOP_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (VaeBuildSetting_t3353517300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[5] = 
{
	0,
	0,
	VaeBuildSetting_t3353517300::get_offset_of_developMode_4(),
	VaeBuildSetting_t3353517300::get_offset_of_developBundle_5(),
	VaeBuildSetting_t3353517300::get_offset_of_branchHashCode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (DrawGui_t872751393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (EditorLoadVRBackground_t1201325975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (ClickConfirmAfterConversationComplete_t3062069762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[2] = 
{
	ClickConfirmAfterConversationComplete_t3062069762::get_offset_of_clc_2(),
	ClickConfirmAfterConversationComplete_t3062069762::get_offset_of_endDialog_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (CharacterAnimCommand_t3374959163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[8] = 
{
	CharacterAnimCommand_t3374959163::get_offset_of__preCurrentPoint_0(),
	CharacterAnimCommand_t3374959163::get_offset_of_postureTypeName_1(),
	CharacterAnimCommand_t3374959163::get_offset_of_eyesTypeName_2(),
	CharacterAnimCommand_t3374959163::get_offset_of_mouthTypeName_3(),
	CharacterAnimCommand_t3374959163::get_offset_of_spawnPosition_4(),
	CharacterAnimCommand_t3374959163::get_offset_of_wait_5(),
	CharacterAnimCommand_t3374959163::get_offset_of_role_6(),
	CharacterAnimCommand_t3374959163::get_offset_of_charScript_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (U3CAutoExecuteU3Ec__Iterator0_t1321509635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[5] = 
{
	U3CAutoExecuteU3Ec__Iterator0_t1321509635::get_offset_of_context_0(),
	U3CAutoExecuteU3Ec__Iterator0_t1321509635::get_offset_of_U24this_1(),
	U3CAutoExecuteU3Ec__Iterator0_t1321509635::get_offset_of_U24current_2(),
	U3CAutoExecuteU3Ec__Iterator0_t1321509635::get_offset_of_U24disposing_3(),
	U3CAutoExecuteU3Ec__Iterator0_t1321509635::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (U3CExecuteU3Ec__Iterator1_t319037821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[5] = 
{
	U3CExecuteU3Ec__Iterator1_t319037821::get_offset_of_context_0(),
	U3CExecuteU3Ec__Iterator1_t319037821::get_offset_of_U24this_1(),
	U3CExecuteU3Ec__Iterator1_t319037821::get_offset_of_U24current_2(),
	U3CExecuteU3Ec__Iterator1_t319037821::get_offset_of_U24disposing_3(),
	U3CExecuteU3Ec__Iterator1_t319037821::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (ConversationCommand_t3660105836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (EffectCommand_t708799566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[4] = 
{
	EffectCommand_t708799566::get_offset_of__imageName_0(),
	EffectCommand_t708799566::get_offset_of__role_1(),
	EffectCommand_t708799566::get_offset_of__preThinkingSprite_2(),
	EffectCommand_t708799566::get_offset_of__preCurrentPoint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (U3CAutoExecuteU3Ec__Iterator0_t513472262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[5] = 
{
	U3CAutoExecuteU3Ec__Iterator0_t513472262::get_offset_of_context_0(),
	U3CAutoExecuteU3Ec__Iterator0_t513472262::get_offset_of_U24this_1(),
	U3CAutoExecuteU3Ec__Iterator0_t513472262::get_offset_of_U24current_2(),
	U3CAutoExecuteU3Ec__Iterator0_t513472262::get_offset_of_U24disposing_3(),
	U3CAutoExecuteU3Ec__Iterator0_t513472262::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (U3CExecuteU3Ec__Iterator1_t564345066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[5] = 
{
	U3CExecuteU3Ec__Iterator1_t564345066::get_offset_of_context_0(),
	U3CExecuteU3Ec__Iterator1_t564345066::get_offset_of_U24this_1(),
	U3CExecuteU3Ec__Iterator1_t564345066::get_offset_of_U24current_2(),
	U3CExecuteU3Ec__Iterator1_t564345066::get_offset_of_U24disposing_3(),
	U3CExecuteU3Ec__Iterator1_t564345066::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (GoToCommand_t2985371124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[2] = 
{
	GoToCommand_t2985371124::get_offset_of__destination_0(),
	GoToCommand_t2985371124::get_offset_of__preCurrentPoint_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (U3CAutoExecuteU3Ec__Iterator0_t849620780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[5] = 
{
	U3CAutoExecuteU3Ec__Iterator0_t849620780::get_offset_of_context_0(),
	U3CAutoExecuteU3Ec__Iterator0_t849620780::get_offset_of_U24this_1(),
	U3CAutoExecuteU3Ec__Iterator0_t849620780::get_offset_of_U24current_2(),
	U3CAutoExecuteU3Ec__Iterator0_t849620780::get_offset_of_U24disposing_3(),
	U3CAutoExecuteU3Ec__Iterator0_t849620780::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (U3CExecuteU3Ec__Iterator1_t1995833048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[5] = 
{
	U3CExecuteU3Ec__Iterator1_t1995833048::get_offset_of_context_0(),
	U3CExecuteU3Ec__Iterator1_t1995833048::get_offset_of_U24this_1(),
	U3CExecuteU3Ec__Iterator1_t1995833048::get_offset_of_U24current_2(),
	U3CExecuteU3Ec__Iterator1_t1995833048::get_offset_of_U24disposing_3(),
	U3CExecuteU3Ec__Iterator1_t1995833048::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (GLQuart_t478788054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[1] = 
{
	GLQuart_t478788054::get_offset_of_mat_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (Graph_t2735713688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[2] = 
{
	Graph_t2735713688::get_offset_of_linesRadius_35(),
	Graph_t2735713688::get_offset_of_vertexName_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[6] = 
{
	U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142::get_offset_of_index_0(),
	U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142::get_offset_of_targetValue_1(),
	U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142::get_offset_of_U24this_2(),
	U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142::get_offset_of_U24current_3(),
	U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142::get_offset_of_U24disposing_4(),
	U3CLerpChangeDistanceU3Ec__Iterator0_t1572238142::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (GraphValue_t2739901601)+ sizeof (Il2CppObject), sizeof(GraphValue_t2739901601_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2326[2] = 
{
	GraphValue_t2739901601::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GraphValue_t2739901601::get_offset_of_percent_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (MyPageChapterController_t2166380602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[9] = 
{
	MyPageChapterController_t2166380602::get_offset_of_ChapterId_2(),
	MyPageChapterController_t2166380602::get_offset_of_CharacterLoadDataName_3(),
	MyPageChapterController_t2166380602::get_offset_of_chapterView_4(),
	MyPageChapterController_t2166380602::get_offset_of_currentGraph_5(),
	MyPageChapterController_t2166380602::get_offset_of_prevGraph_6(),
	MyPageChapterController_t2166380602::get_offset_of_lessonValue_7(),
	MyPageChapterController_t2166380602::get_offset_of_graphValue_8(),
	MyPageChapterController_t2166380602::get_offset_of_graphValuePrev_9(),
	MyPageChapterController_t2166380602::get_offset_of_timeLearnTotal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (MyPageChapterView_t1534782717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[11] = 
{
	MyPageChapterView_t1534782717::get_offset_of_txtChapter_2(),
	MyPageChapterView_t1534782717::get_offset_of_txtTotalTimeComplete_3(),
	MyPageChapterView_t1534782717::get_offset_of_txtRecommend_4(),
	MyPageChapterView_t1534782717::get_offset_of_btnStudy_5(),
	MyPageChapterView_t1534782717::get_offset_of_colorIncrease_6(),
	MyPageChapterView_t1534782717::get_offset_of_colorDecrease_7(),
	MyPageChapterView_t1534782717::get_offset_of_colorUnchanged_8(),
	MyPageChapterView_t1534782717::get_offset_of_sprIncrease_9(),
	MyPageChapterView_t1534782717::get_offset_of_sprDecrease_10(),
	MyPageChapterView_t1534782717::get_offset_of_sprUnchanged_11(),
	MyPageChapterView_t1534782717::get_offset_of_tableContent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (ViewTextUI_t4118675828)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[4] = 
{
	ViewTextUI_t4118675828::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ViewTextUI_t4118675828::get_offset_of_txtNamePracticed_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ViewTextUI_t4118675828::get_offset_of_txtPercentPracticed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ViewTextUI_t4118675828::get_offset_of_imgStatus_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (LessonValue_t1076530945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[2] = 
{
	LessonValue_t1076530945::get_offset_of_name_0(),
	LessonValue_t1076530945::get_offset_of_nameMainScene_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (MyPageController_t1695642829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[5] = 
{
	MyPageController_t1695642829::get_offset_of_totalChapter_2(),
	MyPageController_t1695642829::get_offset_of_containChapter_3(),
	MyPageController_t1695642829::get_offset_of_myPageChapter_4(),
	MyPageController_t1695642829::get_offset_of_lessonValue_5(),
	MyPageController_t1695642829::get_offset_of_lstMyPageChapter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (MyPageView_t3400191636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (Cell_t3051913968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[5] = 
{
	Cell_t3051913968::get_offset_of_lessionName_0(),
	Cell_t3051913968::get_offset_of_sceneName_1(),
	Cell_t3051913968::get_offset_of_cellTransform_2(),
	Cell_t3051913968::get_offset_of_percent_3(),
	Cell_t3051913968::get_offset_of_percentPrev_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (Column_t1930583302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[2] = 
{
	Column_t1930583302::get_offset_of_columnCellsTransform_0(),
	Column_t1930583302::get_offset_of_dataCells_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (Table_t4160395208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[1] = 
{
	Table_t4160395208::get_offset_of_columns_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (LessionName_t1201611902)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2336[8] = 
{
	LessionName_t1201611902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (RunMode_t3692523130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2337[3] = 
{
	RunMode_t3692523130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (ChapterMyPageScript_t3593529611), -1, sizeof(ChapterMyPageScript_t3593529611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2338[16] = 
{
	ChapterMyPageScript_t3593529611::get_offset_of_runMode_2(),
	ChapterMyPageScript_t3593529611::get_offset_of_txtChapterName_3(),
	ChapterMyPageScript_t3593529611::get_offset_of_txtTime_4(),
	ChapterMyPageScript_t3593529611::get_offset_of_txtRecommend_5(),
	ChapterMyPageScript_t3593529611::get_offset_of_sprStatusIncrease_6(),
	ChapterMyPageScript_t3593529611::get_offset_of_sprStatusDecrease_7(),
	ChapterMyPageScript_t3593529611::get_offset_of_sprStatusNoChange_8(),
	ChapterMyPageScript_t3593529611::get_offset_of_btnStudy_9(),
	ChapterMyPageScript_t3593529611::get_offset_of_table_10(),
	ChapterMyPageScript_t3593529611::get_offset_of_graphOldValue_11(),
	ChapterMyPageScript_t3593529611::get_offset_of_graphNewValue_12(),
	ChapterMyPageScript_t3593529611::get_offset_of_chapter_13(),
	ChapterMyPageScript_t3593529611::get_offset_of_globalConfig_14(),
	ChapterMyPageScript_t3593529611::get_offset_of_totalTimeLeant_15(),
	ChapterMyPageScript_t3593529611::get_offset_of_minCell_16(),
	ChapterMyPageScript_t3593529611_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (U3CwaitFadeOutU3Ec__Iterator0_t1364814582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[6] = 
{
	U3CwaitFadeOutU3Ec__Iterator0_t1364814582::get_offset_of_U3CfadingObjU3E__0_0(),
	U3CwaitFadeOutU3Ec__Iterator0_t1364814582::get_offset_of_U3CfadingImgU3E__1_1(),
	U3CwaitFadeOutU3Ec__Iterator0_t1364814582::get_offset_of_onFinish_2(),
	U3CwaitFadeOutU3Ec__Iterator0_t1364814582::get_offset_of_U24current_3(),
	U3CwaitFadeOutU3Ec__Iterator0_t1364814582::get_offset_of_U24disposing_4(),
	U3CwaitFadeOutU3Ec__Iterator0_t1364814582::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (ChapterResultSettingLoader_t4141859061), -1, sizeof(ChapterResultSettingLoader_t4141859061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2340[5] = 
{
	ChapterResultSettingLoader_t4141859061_StaticFields::get_offset_of_CHAPTER_1_MAIN_COLOR_2(),
	ChapterResultSettingLoader_t4141859061_StaticFields::get_offset_of_CHAPTER_2_MAIN_COLOR_3(),
	ChapterResultSettingLoader_t4141859061::get_offset_of__chapter_4(),
	ChapterResultSettingLoader_t4141859061::get_offset_of_leftTitleText_5(),
	ChapterResultSettingLoader_t4141859061::get_offset_of_rightTitleText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (MyPageManagerScript_t3054960093), -1, sizeof(MyPageManagerScript_t3054960093_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2341[5] = 
{
	MyPageManagerScript_t3054960093::get_offset_of_chapterContainer_2(),
	MyPageManagerScript_t3054960093::get_offset_of_prefabChapter_3(),
	MyPageManagerScript_t3054960093::get_offset_of_globalConfig_4(),
	MyPageManagerScript_t3054960093::get_offset_of__resultViews_5(),
	MyPageManagerScript_t3054960093_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (UIPolygon_t4199244354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[7] = 
{
	UIPolygon_t4199244354::get_offset_of_m_Texture_28(),
	UIPolygon_t4199244354::get_offset_of_fill_29(),
	UIPolygon_t4199244354::get_offset_of_thickness_30(),
	UIPolygon_t4199244354::get_offset_of_sides_31(),
	UIPolygon_t4199244354::get_offset_of_rotation_32(),
	UIPolygon_t4199244354::get_offset_of_VerticesDistances_33(),
	UIPolygon_t4199244354::get_offset_of_size_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (SelectionCommand_t2511546469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[3] = 
{
	SelectionCommand_t2511546469::get_offset_of__role_0(),
	SelectionCommand_t2511546469::get_offset_of__data_1(),
	SelectionCommand_t2511546469::get_offset_of__preCurrentPoint_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (U3CAutoExecuteU3Ec__Iterator0_t1017565241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[5] = 
{
	U3CAutoExecuteU3Ec__Iterator0_t1017565241::get_offset_of_context_0(),
	U3CAutoExecuteU3Ec__Iterator0_t1017565241::get_offset_of_U24this_1(),
	U3CAutoExecuteU3Ec__Iterator0_t1017565241::get_offset_of_U24current_2(),
	U3CAutoExecuteU3Ec__Iterator0_t1017565241::get_offset_of_U24disposing_3(),
	U3CAutoExecuteU3Ec__Iterator0_t1017565241::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (U3CExecuteU3Ec__Iterator1_t191178719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[5] = 
{
	U3CExecuteU3Ec__Iterator1_t191178719::get_offset_of_context_0(),
	U3CExecuteU3Ec__Iterator1_t191178719::get_offset_of_U24this_1(),
	U3CExecuteU3Ec__Iterator1_t191178719::get_offset_of_U24current_2(),
	U3CExecuteU3Ec__Iterator1_t191178719::get_offset_of_U24disposing_3(),
	U3CExecuteU3Ec__Iterator1_t191178719::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (SoundCommand_t2356851872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[3] = 
{
	SoundCommand_t2356851872::get_offset_of__preCurrentPoint_0(),
	SoundCommand_t2356851872::get_offset_of_commandName_1(),
	SoundCommand_t2356851872::get_offset_of_soundData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (U3CAutoExecuteU3Ec__Iterator0_t3994390064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[5] = 
{
	U3CAutoExecuteU3Ec__Iterator0_t3994390064::get_offset_of_context_0(),
	U3CAutoExecuteU3Ec__Iterator0_t3994390064::get_offset_of_U24this_1(),
	U3CAutoExecuteU3Ec__Iterator0_t3994390064::get_offset_of_U24current_2(),
	U3CAutoExecuteU3Ec__Iterator0_t3994390064::get_offset_of_U24disposing_3(),
	U3CAutoExecuteU3Ec__Iterator0_t3994390064::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (U3CExecuteU3Ec__Iterator1_t748120696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[5] = 
{
	U3CExecuteU3Ec__Iterator1_t748120696::get_offset_of_context_0(),
	U3CExecuteU3Ec__Iterator1_t748120696::get_offset_of_U24this_1(),
	U3CExecuteU3Ec__Iterator1_t748120696::get_offset_of_U24current_2(),
	U3CExecuteU3Ec__Iterator1_t748120696::get_offset_of_U24disposing_3(),
	U3CExecuteU3Ec__Iterator1_t748120696::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (StopEffectCommand_t3016203262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[3] = 
{
	StopEffectCommand_t3016203262::get_offset_of__preCurrentPoint_0(),
	StopEffectCommand_t3016203262::get_offset_of__imageName_1(),
	StopEffectCommand_t3016203262::get_offset_of__role_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (U3CExecuteU3Ec__Iterator0_t386014617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[5] = 
{
	U3CExecuteU3Ec__Iterator0_t386014617::get_offset_of_context_0(),
	U3CExecuteU3Ec__Iterator0_t386014617::get_offset_of_U24this_1(),
	U3CExecuteU3Ec__Iterator0_t386014617::get_offset_of_U24current_2(),
	U3CExecuteU3Ec__Iterator0_t386014617::get_offset_of_U24disposing_3(),
	U3CExecuteU3Ec__Iterator0_t386014617::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (U3CAutoExecuteU3Ec__Iterator1_t2553865167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[5] = 
{
	U3CAutoExecuteU3Ec__Iterator1_t2553865167::get_offset_of_context_0(),
	U3CAutoExecuteU3Ec__Iterator1_t2553865167::get_offset_of_U24this_1(),
	U3CAutoExecuteU3Ec__Iterator1_t2553865167::get_offset_of_U24current_2(),
	U3CAutoExecuteU3Ec__Iterator1_t2553865167::get_offset_of_U24disposing_3(),
	U3CAutoExecuteU3Ec__Iterator1_t2553865167::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (TalkCommand_t2292920015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[3] = 
{
	TalkCommand_t2292920015::get_offset_of__data_0(),
	TalkCommand_t2292920015::get_offset_of__preTalkData_1(),
	TalkCommand_t2292920015::get_offset_of__preCurrentPoint_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (U3CAutoExecuteU3Ec__Iterator0_t3903119071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[5] = 
{
	U3CAutoExecuteU3Ec__Iterator0_t3903119071::get_offset_of_context_0(),
	U3CAutoExecuteU3Ec__Iterator0_t3903119071::get_offset_of_U24this_1(),
	U3CAutoExecuteU3Ec__Iterator0_t3903119071::get_offset_of_U24current_2(),
	U3CAutoExecuteU3Ec__Iterator0_t3903119071::get_offset_of_U24disposing_3(),
	U3CAutoExecuteU3Ec__Iterator0_t3903119071::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (U3CExecuteU3Ec__Iterator1_t3431233089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[5] = 
{
	U3CExecuteU3Ec__Iterator1_t3431233089::get_offset_of_context_0(),
	U3CExecuteU3Ec__Iterator1_t3431233089::get_offset_of_U24this_1(),
	U3CExecuteU3Ec__Iterator1_t3431233089::get_offset_of_U24current_2(),
	U3CExecuteU3Ec__Iterator1_t3431233089::get_offset_of_U24disposing_3(),
	U3CExecuteU3Ec__Iterator1_t3431233089::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (BaseConversationCommandLoader_t1091304408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[3] = 
{
	BaseConversationCommandLoader_t1091304408::get_offset_of_dataSheet_2(),
	BaseConversationCommandLoader_t1091304408::get_offset_of_levelSheet_3(),
	BaseConversationCommandLoader_t1091304408::get_offset_of_currentPoint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (U3CGoToU3Ec__AnonStorey0_t144537213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[1] = 
{
	U3CGoToU3Ec__AnonStorey0_t144537213::get_offset_of_destination_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (CommandLoader_t2460237222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (ConversationCommandLoader_t41408221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[1] = 
{
	ConversationCommandLoader_t41408221::get_offset_of__currentLevel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (U3CGetLevelDataU3Ec__AnonStorey0_t3009732323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[1] = 
{
	U3CGetLevelDataU3Ec__AnonStorey0_t3009732323::get_offset_of_level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (ExcerciseCommandLoader_t1886015525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[1] = 
{
	ExcerciseCommandLoader_t1886015525::get_offset_of__currentLevel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (U3CLoadLevelU3Ec__AnonStorey0_t2141008911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[1] = 
{
	U3CLoadLevelU3Ec__AnonStorey0_t2141008911::get_offset_of_level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (PracticeCommandLoader_t3901273799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (BaseConversationController_t808907754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[23] = 
{
	BaseConversationController_t808907754::get_offset_of_nameLesson_2(),
	BaseConversationController_t808907754::get_offset_of_level_3(),
	BaseConversationController_t808907754::get_offset_of_startTime_4(),
	BaseConversationController_t808907754::get_offset_of_characterRole0_5(),
	BaseConversationController_t808907754::get_offset_of_characterRole1_6(),
	BaseConversationController_t808907754::get_offset_of_characterName0_7(),
	BaseConversationController_t808907754::get_offset_of_characterName1_8(),
	BaseConversationController_t808907754::get_offset_of__state_9(),
	BaseConversationController_t808907754::get_offset_of_conversationView_10(),
	BaseConversationController_t808907754::get_offset_of_conversationLogicController_11(),
	BaseConversationController_t808907754::get_offset_of_commandLoader_12(),
	BaseConversationController_t808907754::get_offset_of_playerRole_13(),
	BaseConversationController_t808907754::get_offset_of_otherRole_14(),
	BaseConversationController_t808907754::get_offset_of_prefabCharacter_15(),
	BaseConversationController_t808907754::get_offset_of_buttonHelp_16(),
	BaseConversationController_t808907754::get_offset_of_soundSavePath_17(),
	BaseConversationController_t808907754::get_offset_of_soundResourcePath_18(),
	BaseConversationController_t808907754::get_offset_of_textureResourcePath_19(),
	BaseConversationController_t808907754::get_offset_of_rrc_20(),
	BaseConversationController_t808907754::get_offset_of_conversationDataSaver_21(),
	BaseConversationController_t808907754::get_offset_of__isReplay_22(),
	BaseConversationController_t808907754::get_offset_of_eyeRaycaster_23(),
	BaseConversationController_t808907754::get_offset_of__eyeContactScore_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (ConversationState_t1349939935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2364[5] = 
{
	ConversationState_t1349939935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (NewConversationSceneController_t3024536561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[2] = 
{
	NewConversationSceneController_t3024536561::get_offset_of_sceneSelector_25(),
	NewConversationSceneController_t3024536561::get_offset_of_characterSelector_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (NewExerciseSceneController_t2240697240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[2] = 
{
	NewExerciseSceneController_t2240697240::get_offset_of_sceneSelector_25(),
	NewExerciseSceneController_t2240697240::get_offset_of_characterSelector_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (NewPracticeSceneController_t1536734399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[1] = 
{
	NewPracticeSceneController_t1536734399::get_offset_of_characterSelector_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ConversationLogicController_t3911886281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[30] = 
{
	ConversationLogicController_t3911886281::get_offset_of_prefabCharacter_2(),
	ConversationLogicController_t3911886281::get_offset_of_characterManager_3(),
	ConversationLogicController_t3911886281::get_offset_of_isAutoPlay_4(),
	ConversationLogicController_t3911886281::get_offset_of_onConversationCompleted_5(),
	ConversationLogicController_t3911886281::get_offset_of_onConversationEnded_6(),
	ConversationLogicController_t3911886281::get_offset_of_soundSavePath_7(),
	ConversationLogicController_t3911886281::get_offset_of_soundResourcePath_8(),
	ConversationLogicController_t3911886281::get_offset_of_textureResourcePath_9(),
	ConversationLogicController_t3911886281::get_offset_of_playerRole_10(),
	ConversationLogicController_t3911886281::get_offset_of_otherRole_11(),
	ConversationLogicController_t3911886281::get_offset_of_commandLoader_12(),
	ConversationLogicController_t3911886281::get_offset_of_soundRecorder_13(),
	ConversationLogicController_t3911886281::get_offset_of_roles_14(),
	ConversationLogicController_t3911886281::get_offset_of_U3CstartPlayTimeU3Ek__BackingField_15(),
	ConversationLogicController_t3911886281::get_offset_of_U3CplayerTalkDatasU3Ek__BackingField_16(),
	ConversationLogicController_t3911886281::get_offset_of_isConversationEnd_17(),
	ConversationLogicController_t3911886281::get_offset_of__isBlock_18(),
	ConversationLogicController_t3911886281::get_offset_of_playingPlayerTalk_19(),
	ConversationLogicController_t3911886281::get_offset_of_nameLessionText_20(),
	ConversationLogicController_t3911886281::get_offset_of_onConversationStateChange_21(),
	ConversationLogicController_t3911886281::get_offset_of__state_22(),
	ConversationLogicController_t3911886281::get_offset_of_languageButton_23(),
	ConversationLogicController_t3911886281::get_offset_of_helpButton_24(),
	ConversationLogicController_t3911886281::get_offset_of_invokerModule_25(),
	ConversationLogicController_t3911886281::get_offset_of_thinkingEffectModule_26(),
	ConversationLogicController_t3911886281::get_offset_of_playerSelectionModule_27(),
	ConversationLogicController_t3911886281::get_offset_of_playerTalkModule_28(),
	ConversationLogicController_t3911886281::get_offset_of_otherTalkModule_29(),
	ConversationLogicController_t3911886281::get_offset_of_historyLogModule_30(),
	ConversationLogicController_t3911886281::get_offset_of_voiceRecordModule_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ConversationState_t3360496668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[3] = 
{
	ConversationState_t3360496668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ConversationStateChangeEvent_t2954138412), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ConversationView_t3413307054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[4] = 
{
	ConversationView_t3413307054::get_offset_of_levelSelectView_2(),
	ConversationView_t3413307054::get_offset_of_conversationView_3(),
	ConversationView_t3413307054::get_offset_of_completedView_4(),
	ConversationView_t3413307054::get_offset_of_resultView_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (ConversationSelectionData_t4090008535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[4] = 
{
	ConversationSelectionData_t4090008535::get_offset_of_destination_0(),
	ConversationSelectionData_t4090008535::get_offset_of_textEN_1(),
	ConversationSelectionData_t4090008535::get_offset_of_textJP_2(),
	ConversationSelectionData_t4090008535::get_offset_of_soundData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (ConversationSelectionGroupData_t3437723178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[2] = 
{
	ConversationSelectionGroupData_t3437723178::get_offset_of_role_0(),
	ConversationSelectionGroupData_t3437723178::get_offset_of_selectionDatas_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (ConversationTalkData_t1570298305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[5] = 
{
	ConversationTalkData_t1570298305::get_offset_of_role_0(),
	ConversationTalkData_t1570298305::get_offset_of_dataEn_1(),
	ConversationTalkData_t1570298305::get_offset_of_dataJp_2(),
	ConversationTalkData_t1570298305::get_offset_of_soundData_3(),
	ConversationTalkData_t1570298305::get_offset_of_linkedTalkCommand_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (ConversationDataSaver_t1206770280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (ConversationSceneDataSaver_t1575220710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[2] = 
{
	ConversationSceneDataSaver_t1575220710::get_offset_of_globalConfig_2(),
	ConversationSceneDataSaver_t1575220710::get_offset_of_newConversationSceneController_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (ExerciseSceneDataSaver_t2652365465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[2] = 
{
	ExerciseSceneDataSaver_t2652365465::get_offset_of_globalConfig_2(),
	ExerciseSceneDataSaver_t2652365465::get_offset_of_exerciseSceneController_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (PracticeSceneDataSaver_t1376690392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[2] = 
{
	PracticeSceneDataSaver_t1376690392::get_offset_of_globalConfig_2(),
	PracticeSceneDataSaver_t1376690392::get_offset_of_practiceSceneController_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (HistoryLogModule_t1965887714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[4] = 
{
	HistoryLogModule_t1965887714::get_offset_of_clc_2(),
	HistoryLogModule_t1965887714::get_offset_of_historyDialog_3(),
	HistoryLogModule_t1965887714::get_offset_of_playerTalkModule_4(),
	HistoryLogModule_t1965887714::get_offset_of_otherTalkModule_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[4] = 
{
	U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849::get_offset_of_U24this_0(),
	U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849::get_offset_of_U24current_1(),
	U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849::get_offset_of_U24disposing_2(),
	U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[9] = 
{
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_data_0(),
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_U3CisPlayerRoleU3E__0_1(),
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_U3CstrEnU3E__1_2(),
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_U3CstrJpU3E__2_3(),
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_dialogTransform_4(),
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_U24this_5(),
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_U24current_6(),
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_U24disposing_7(),
	U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[4] = 
{
	U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015::get_offset_of_U24this_0(),
	U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015::get_offset_of_U24current_1(),
	U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015::get_offset_of_U24disposing_2(),
	U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (InvokerModule_t4256524346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[8] = 
{
	InvokerModule_t4256524346::get_offset_of_playerTalkModule_2(),
	InvokerModule_t4256524346::get_offset_of_otherTalkModule_3(),
	InvokerModule_t4256524346::get_offset_of_commandLoader_4(),
	InvokerModule_t4256524346::get_offset_of_clc_5(),
	InvokerModule_t4256524346::get_offset_of__isBlock_6(),
	InvokerModule_t4256524346::get_offset_of__isCompleted_7(),
	InvokerModule_t4256524346::get_offset_of__cachedCommands_8(),
	InvokerModule_t4256524346::get_offset_of_bottomButtons_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[5] = 
{
	U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571::get_offset_of_U3CcommandU3E__0_0(),
	U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571::get_offset_of_U24this_1(),
	U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571::get_offset_of_U24current_2(),
	U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571::get_offset_of_U24disposing_3(),
	U3CRunToNextPlayerCommandU3Ec__Iterator0_t369331571::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[6] = 
{
	U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081::get_offset_of_U24locvar0_0(),
	U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081::get_offset_of_U3CcommandU3E__0_1(),
	U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081::get_offset_of_U24this_2(),
	U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081::get_offset_of_U24current_3(),
	U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081::get_offset_of_U24disposing_4(),
	U3CDoReplayAllCommandU3Ec__Iterator1_t2213755081::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (U3CDoAutoPlayU3Ec__Iterator2_t874955144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[6] = 
{
	U3CDoAutoPlayU3Ec__Iterator2_t874955144::get_offset_of_U3CnextCommandU3E__0_0(),
	U3CDoAutoPlayU3Ec__Iterator2_t874955144::get_offset_of_callback_1(),
	U3CDoAutoPlayU3Ec__Iterator2_t874955144::get_offset_of_U24this_2(),
	U3CDoAutoPlayU3Ec__Iterator2_t874955144::get_offset_of_U24current_3(),
	U3CDoAutoPlayU3Ec__Iterator2_t874955144::get_offset_of_U24disposing_4(),
	U3CDoAutoPlayU3Ec__Iterator2_t874955144::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (OtherTalkModule_t2158105276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[6] = 
{
	OtherTalkModule_t2158105276::get_offset_of_clc_2(),
	OtherTalkModule_t2158105276::get_offset_of_recordButton_3(),
	OtherTalkModule_t2158105276::get_offset_of_replayRecordedButton_4(),
	OtherTalkModule_t2158105276::get_offset_of_otherTalkDialog_5(),
	OtherTalkModule_t2158105276::get_offset_of_playOtherTalkDialog_6(),
	OtherTalkModule_t2158105276::get_offset_of_U3CcurrentOtherTalkDataU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (U3CShowOtherTalkDialogU3Ec__Iterator0_t245603399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[5] = 
{
	U3CShowOtherTalkDialogU3Ec__Iterator0_t245603399::get_offset_of_data_0(),
	U3CShowOtherTalkDialogU3Ec__Iterator0_t245603399::get_offset_of_U24this_1(),
	U3CShowOtherTalkDialogU3Ec__Iterator0_t245603399::get_offset_of_U24current_2(),
	U3CShowOtherTalkDialogU3Ec__Iterator0_t245603399::get_offset_of_U24disposing_3(),
	U3CShowOtherTalkDialogU3Ec__Iterator0_t245603399::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[4] = 
{
	U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544::get_offset_of_U24this_0(),
	U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544::get_offset_of_U24current_1(),
	U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544::get_offset_of_U24disposing_2(),
	U3CPlayOtherTalkEffectU3Ec__Iterator1_t2517786544::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (PlayerSelectionModule_t1458090443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[5] = 
{
	PlayerSelectionModule_t1458090443::get_offset_of_playerTalkModule_2(),
	PlayerSelectionModule_t1458090443::get_offset_of_otherTalkModule_3(),
	PlayerSelectionModule_t1458090443::get_offset_of_clc_4(),
	PlayerSelectionModule_t1458090443::get_offset_of_selectionDialog_5(),
	PlayerSelectionModule_t1458090443::get_offset_of_commandLoader_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[8] = 
{
	U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990::get_offset_of_U3CcachedShowPlayerTalkDialogU3E__0_0(),
	U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990::get_offset_of_U3CcachedShowOtherTalkDialogU3E__1_1(),
	U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990::get_offset_of_data_2(),
	U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990::get_offset_of_U3CselectedU3E__2_3(),
	U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990::get_offset_of_U24this_4(),
	U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990::get_offset_of_U24current_5(),
	U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990::get_offset_of_U24disposing_6(),
	U3CShowPlayerSelectionsU3Ec__Iterator0_t4151862990::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (PlayerTalkModule_t3356839145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[8] = 
{
	PlayerTalkModule_t3356839145::get_offset_of_clc_2(),
	PlayerTalkModule_t3356839145::get_offset_of_playerTalkDialog_3(),
	PlayerTalkModule_t3356839145::get_offset_of_recordButton_4(),
	PlayerTalkModule_t3356839145::get_offset_of_replayRecordedButton_5(),
	PlayerTalkModule_t3356839145::get_offset_of_bottomButtons_6(),
	PlayerTalkModule_t3356839145::get_offset_of_playSampleTalkDialog_7(),
	PlayerTalkModule_t3356839145::get_offset_of_replayTalkDialog_8(),
	PlayerTalkModule_t3356839145::get_offset_of_U3CcurrentPlayerTalkDataU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[5] = 
{
	U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055::get_offset_of_data_0(),
	U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055::get_offset_of_U24this_1(),
	U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055::get_offset_of_U24current_2(),
	U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055::get_offset_of_U24disposing_3(),
	U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947), -1, sizeof(U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2398[7] = 
{
	U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947::get_offset_of_U3CdialogU3E__0_0(),
	U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947::get_offset_of_data_1(),
	U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947::get_offset_of_U24this_2(),
	U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947::get_offset_of_U24current_3(),
	U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947::get_offset_of_U24disposing_4(),
	U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947::get_offset_of_U24PC_5(),
	U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[5] = 
{
	U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934::get_offset_of_data_0(),
	U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934::get_offset_of_U24this_1(),
	U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934::get_offset_of_U24current_2(),
	U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934::get_offset_of_U24disposing_3(),
	U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934::get_offset_of_U24PC_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
