﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_U3CRestor1157070212.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_U3CProces2428326241.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_U3COnPurc1880872493.h"
#include "AssemblyU2DCSharp_Listening1AnswerHelper2804121578.h"
#include "AssemblyU2DCSharp_Listening1DatasWrapper1360134800.h"
#include "AssemblyU2DCSharp_Listening1OnplayQuestion3534871625.h"
#include "AssemblyU2DCSharp_Listening1OnplayQuestion_U3CSetu2986955878.h"
#include "AssemblyU2DCSharp_Listening1Popup1745092138.h"
#include "AssemblyU2DCSharp_TrueFalseAnswerSelection3406544577.h"
#include "AssemblyU2DCSharp_Listening1QuestionData3381069024.h"
#include "AssemblyU2DCSharp_Listening1QuestionView2345876901.h"
#include "AssemblyU2DCSharp_Listening1QuestionView_U3CShowRe2212642809.h"
#include "AssemblyU2DCSharp_Listening1SceneController4197317516.h"
#include "AssemblyU2DCSharp_Listening1SceneController_U3CSta1868754606.h"
#include "AssemblyU2DCSharp_Listenning1DataLoader2634637697.h"
#include "AssemblyU2DCSharp_Listening2AnswerHelper4121473615.h"
#include "AssemblyU2DCSharp_Listening2DataLoader3991471186.h"
#include "AssemblyU2DCSharp_Listening2DatasWrapper1178512309.h"
#include "AssemblyU2DCSharp_Listening2OnplayQuestion3701806074.h"
#include "AssemblyU2DCSharp_Listening2OnplayQuestion_U3CSetu3933455307.h"
#include "AssemblyU2DCSharp_Listening2Popup2917639739.h"
#include "AssemblyU2DCSharp_Listening2QuestionData2443975119.h"
#include "AssemblyU2DCSharp_Listening2QuestionView3734381824.h"
#include "AssemblyU2DCSharp_Listening2QuestionView_U3CShowRe3957263220.h"
#include "AssemblyU2DCSharp_Listening2SceneController3779648327.h"
#include "AssemblyU2DCSharp_Listening2SceneController_U3CSta2772662239.h"
#include "AssemblyU2DCSharp_Listening2SelectableItem492261174.h"
#include "AssemblyU2DCSharp_Listening2SelectableItem_U3CLerpF379814438.h"
#include "AssemblyU2DCSharp_SceneName904752595.h"
#include "AssemblyU2DCSharp_LoadSceneScript888325993.h"
#include "AssemblyU2DCSharp_LoadSceneScript_U3CAwakeU3Ec__An1264054739.h"
#include "AssemblyU2DCSharp_LoadSceneScript_U3CDelayLoadVRU32009451244.h"
#include "AssemblyU2DCSharp_AutoChangeFont1379389610.h"
#include "AssemblyU2DCSharp_ButtonAutoPlay2268545083.h"
#include "AssemblyU2DCSharp_ButtonMike3037018370.h"
#include "AssemblyU2DCSharp_ButtonMike_U3CIESavingVoiceU3Ec_1871642022.h"
#include "AssemblyU2DCSharp_ButtonMode2270855263.h"
#include "AssemblyU2DCSharp_ConversationController3486580231.h"
#include "AssemblyU2DCSharp_ConversationController_U3CQuiteVR118348258.h"
#include "AssemblyU2DCSharp_DialogController3845653284.h"
#include "AssemblyU2DCSharp_Dialog1378192732.h"
#include "AssemblyU2DCSharp_DialogPanel1568014038.h"
#include "AssemblyU2DCSharp_DialogPanel_U3CPlayVoiceU3Ec__It3000682236.h"
#include "AssemblyU2DCSharp_DialogPanel_U3CPlayVoiceAndDisab3374691054.h"
#include "AssemblyU2DCSharp_DisableWithTimeAudioName4015415354.h"
#include "AssemblyU2DCSharp_DisableWithTimeAudioName_U3CPlayA965722453.h"
#include "AssemblyU2DCSharp_DisableWithTimeCountdown622718188.h"
#include "AssemblyU2DCSharp_DisableWithTimeCountdown_U3CPlay3247491393.h"
#include "AssemblyU2DCSharp_DisableWithTimeSound417185280.h"
#include "AssemblyU2DCSharp_DisableWithTimeSound_U3CPlayAudio934747567.h"
#include "AssemblyU2DCSharp_HandleRecording1220661029.h"
#include "AssemblyU2DCSharp_Popup161311578.h"
#include "AssemblyU2DCSharp_HistoryConversation3119154081.h"
#include "AssemblyU2DCSharp_MoveContent4217566478.h"
#include "AssemblyU2DCSharp_TextKaraoke2450194175.h"
#include "AssemblyU2DCSharp_TextKaraoke_U3CAutoTypingU3Ec__It785846093.h"
#include "AssemblyU2DCSharp_TextKaraoke_U3CAutoTypingU3Ec__I2351930034.h"
#include "AssemblyU2DCSharp_GetProductID3571953046.h"
#include "AssemblyU2DCSharp_GetValueFromRemoteSeting1223913735.h"
#include "AssemblyU2DCSharp_CircleScript1225364279.h"
#include "AssemblyU2DCSharp_LearningResultData1558878615.h"
#include "AssemblyU2DCSharp_LearningResultDataExtension2364822164.h"
#include "AssemblyU2DCSharp_ResultRateController2087696941.h"
#include "AssemblyU2DCSharp_ResultRateParam3783906052.h"
#include "AssemblyU2DCSharp_ResultRateSlider127081558.h"
#include "AssemblyU2DCSharp_ResultRateView1401090222.h"
#include "AssemblyU2DCSharp_CategoryId2845940439.h"
#include "AssemblyU2DCSharp_ResultSaveResponse3438979681.h"
#include "AssemblyU2DCSharp_ZipSoundItem558808959.h"
#include "AssemblyU2DCSharp_ResultSaver3290295394.h"
#include "AssemblyU2DCSharp_ResultSaver_U3CDoPushLearningRes3642887568.h"
#include "AssemblyU2DCSharp_ResultSaver_U3CPushSoundZipU3Ec_1694356377.h"
#include "AssemblyU2DCSharp_ResultStatisticChart3759290537.h"
#include "AssemblyU2DCSharp_ResultStatisticController2929782681.h"
#include "AssemblyU2DCSharp_ResultStatisticParam3727789074.h"
#include "AssemblyU2DCSharp_ResultStatisticView1911483820.h"
#include "AssemblyU2DCSharp_BaseOnplayQuestion1717064182.h"
#include "AssemblyU2DCSharp_QuestionScenePopup792224618.h"
#include "AssemblyU2DCSharp_QuestionScenePopup_U3CPlayAnimat1105760000.h"
#include "AssemblyU2DCSharp_com_beetsoft_GUIElements_Selecta2941161729.h"
#include "AssemblyU2DCSharp_com_beetsoft_GUIElements_Selecta3617874698.h"
#include "AssemblyU2DCSharp_BaseData450384115.h"
#include "AssemblyU2DCSharp_QuestionData3323763512.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomQuestionDa3251732102.h"
#include "AssemblyU2DCSharp_BaseQuestionView79922856.h"
#include "AssemblyU2DCSharp_Sentence1DataLoader1039713923.h"
#include "AssemblyU2DCSharp_Sentence1DatasWrapper174893388.h"
#include "AssemblyU2DCSharp_Sentence1SceneController2055584412.h"
#include "AssemblyU2DCSharp_Sentence1SceneController_U3CStar1440754326.h"
#include "AssemblyU2DCSharp_Sentence1Test1755393472.h"
#include "AssemblyU2DCSharp_SentenceQuestionView1478559372.h"
#include "AssemblyU2DCSharp_SentenceQuestionView_U3CPlayAnima905375778.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomOnPlayQuest936886987.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomOnPlayQues3958551864.h"
#include "AssemblyU2DCSharp_Sentence2DataLoader844185122.h"
#include "AssemblyU2DCSharp_Sentence2DatasWrapper2314527497.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (U3CRestorePurchasesU3Ec__AnonStorey1_t1157070212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[1] = 
{
	U3CRestorePurchasesU3Ec__AnonStorey1_t1157070212::get_offset_of_onResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (U3CProcessPurchaseU3Ec__AnonStorey2_t2428326241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[1] = 
{
	U3CProcessPurchaseU3Ec__AnonStorey2_t2428326241::get_offset_of_productDefinition_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (U3COnPurchaseFailedU3Ec__AnonStorey3_t1880872493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[1] = 
{
	U3COnPurchaseFailedU3Ec__AnonStorey3_t1880872493::get_offset_of_product_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (Listening1AnswerHelper_t2804121578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[1] = 
{
	Listening1AnswerHelper_t2804121578::get_offset_of_item_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (Listening1DatasWrapper_t1360134800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[2] = 
{
	Listening1DatasWrapper_t1360134800::get_offset_of__wrappedDatas_0(),
	Listening1DatasWrapper_t1360134800::get_offset_of__convertedDatas_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (Listening1OnplayQuestion_t3534871625), -1, sizeof(Listening1OnplayQuestion_t3534871625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2505[6] = 
{
	Listening1OnplayQuestion_t3534871625::get_offset_of_soundPath_2(),
	Listening1OnplayQuestion_t3534871625::get_offset_of__view_3(),
	Listening1OnplayQuestion_t3534871625::get_offset_of_U3CquestionDataU3Ek__BackingField_4(),
	Listening1OnplayQuestion_t3534871625::get_offset_of_U3CcurrentAnswerU3Ek__BackingField_5(),
	Listening1OnplayQuestion_t3534871625::get_offset_of_U3CcurrentIndexAnswerU3Ek__BackingField_6(),
	Listening1OnplayQuestion_t3534871625_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (U3CSetupEventHandlerU3Ec__AnonStorey0_t2986955878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[2] = 
{
	U3CSetupEventHandlerU3Ec__AnonStorey0_t2986955878::get_offset_of_index_0(),
	U3CSetupEventHandlerU3Ec__AnonStorey0_t2986955878::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (Listening1Popup_t1745092138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[3] = 
{
	Listening1Popup_t1745092138::get_offset_of_textPrefab_9(),
	Listening1Popup_t1745092138::get_offset_of_answerTextHolder_10(),
	Listening1Popup_t1745092138::get_offset_of__answerObjs_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (TrueFalseAnswerSelection_t3406544577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[2] = 
{
	TrueFalseAnswerSelection_t3406544577::get_offset_of_answerText_0(),
	TrueFalseAnswerSelection_t3406544577::get_offset_of_isCorrect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (Listening1QuestionData_t3381069024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[3] = 
{
	Listening1QuestionData_t3381069024::get_offset_of_U3CquestionTextU3Ek__BackingField_0(),
	Listening1QuestionData_t3381069024::get_offset_of_U3CquestionSoundU3Ek__BackingField_1(),
	Listening1QuestionData_t3381069024::get_offset_of_answers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (Listening1QuestionView_t2345876901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[9] = 
{
	Listening1QuestionView_t2345876901::get_offset_of_answerViewPrefab_3(),
	Listening1QuestionView_t2345876901::get_offset_of_answerContainer_4(),
	Listening1QuestionView_t2345876901::get_offset_of_questionText_5(),
	Listening1QuestionView_t2345876901::get_offset_of_questionSoundButton_6(),
	Listening1QuestionView_t2345876901::get_offset_of_summitButton_7(),
	Listening1QuestionView_t2345876901::get_offset_of_answers_8(),
	Listening1QuestionView_t2345876901::get_offset_of_answerSound_9(),
	Listening1QuestionView_t2345876901::get_offset_of_popup_10(),
	Listening1QuestionView_t2345876901::get_offset_of_tempList_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (U3CShowResultU3Ec__AnonStorey0_t2212642809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	U3CShowResultU3Ec__AnonStorey0_t2212642809::get_offset_of_popupClosedCallback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (Listening1SceneController_t4197317516), -1, sizeof(Listening1SceneController_t4197317516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2512[8] = 
{
	Listening1SceneController_t4197317516::get_offset_of_dataLoader_2(),
	Listening1SceneController_t4197317516::get_offset_of_questionDatas_3(),
	Listening1SceneController_t4197317516::get_offset_of_onPlayQuestions_4(),
	Listening1SceneController_t4197317516::get_offset_of_questionView_5(),
	Listening1SceneController_t4197317516::get_offset_of__currentQuestion_6(),
	Listening1SceneController_t4197317516::get_offset_of_soundPath_7(),
	Listening1SceneController_t4197317516::get_offset_of_startTime_8(),
	Listening1SceneController_t4197317516_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (U3CStartU3Ec__Iterator0_t1868754606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[4] = 
{
	U3CStartU3Ec__Iterator0_t1868754606::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1868754606::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1868754606::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1868754606::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (Listenning1DataLoader_t2634637697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[2] = 
{
	Listenning1DataLoader_t2634637697::get_offset_of_dataSheet_2(),
	Listenning1DataLoader_t2634637697::get_offset_of_U3CdatasWrapperU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (Listening2AnswerHelper_t4121473615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[1] = 
{
	Listening2AnswerHelper_t4121473615::get_offset_of_item_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (Listening2DataLoader_t3991471186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[2] = 
{
	Listening2DataLoader_t3991471186::get_offset_of_dataSheet_2(),
	Listening2DataLoader_t3991471186::get_offset_of_U3CdatasWrapperU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (Listening2DatasWrapper_t1178512309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[2] = 
{
	Listening2DatasWrapper_t1178512309::get_offset_of__wrappedDatas_0(),
	Listening2DatasWrapper_t1178512309::get_offset_of__convertedDatas_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (Listening2OnplayQuestion_t3701806074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[5] = 
{
	Listening2OnplayQuestion_t3701806074::get_offset_of_soundPath_2(),
	Listening2OnplayQuestion_t3701806074::get_offset_of__view_3(),
	Listening2OnplayQuestion_t3701806074::get_offset_of_U3CquestionDataU3Ek__BackingField_4(),
	Listening2OnplayQuestion_t3701806074::get_offset_of_U3CcurrentAnswerU3Ek__BackingField_5(),
	Listening2OnplayQuestion_t3701806074::get_offset_of_U3CcurrentIndexAnswerU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (U3CSetupEventHandlerU3Ec__AnonStorey0_t3933455307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[2] = 
{
	U3CSetupEventHandlerU3Ec__AnonStorey0_t3933455307::get_offset_of_index_0(),
	U3CSetupEventHandlerU3Ec__AnonStorey0_t3933455307::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (Listening2Popup_t2917639739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[1] = 
{
	Listening2Popup_t2917639739::get_offset_of__answerObjs_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (Listening2QuestionData_t2443975119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[3] = 
{
	Listening2QuestionData_t2443975119::get_offset_of_U3CquestionTextU3Ek__BackingField_0(),
	Listening2QuestionData_t2443975119::get_offset_of_U3CquestionSoundU3Ek__BackingField_1(),
	Listening2QuestionData_t2443975119::get_offset_of_answers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (Listening2QuestionView_t3734381824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[9] = 
{
	Listening2QuestionView_t3734381824::get_offset_of_answerViewPrefab_3(),
	Listening2QuestionView_t3734381824::get_offset_of_answerContainer_4(),
	Listening2QuestionView_t3734381824::get_offset_of_questionText_5(),
	Listening2QuestionView_t3734381824::get_offset_of_questionSoundButton_6(),
	Listening2QuestionView_t3734381824::get_offset_of_summitButton_7(),
	Listening2QuestionView_t3734381824::get_offset_of_answers_8(),
	Listening2QuestionView_t3734381824::get_offset_of_answerSound_9(),
	Listening2QuestionView_t3734381824::get_offset_of_popup_10(),
	Listening2QuestionView_t3734381824::get_offset_of_tempList_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (U3CShowResultU3Ec__AnonStorey0_t3957263220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[1] = 
{
	U3CShowResultU3Ec__AnonStorey0_t3957263220::get_offset_of_popupClosedCallback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (Listening2SceneController_t3779648327), -1, sizeof(Listening2SceneController_t3779648327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2524[10] = 
{
	Listening2SceneController_t3779648327::get_offset_of_dataLoader_2(),
	Listening2SceneController_t3779648327::get_offset_of_questionDatas_3(),
	Listening2SceneController_t3779648327::get_offset_of_onPlayQuestions_4(),
	Listening2SceneController_t3779648327::get_offset_of_questionView_5(),
	Listening2SceneController_t3779648327::get_offset_of_soundPath_6(),
	Listening2SceneController_t3779648327::get_offset_of__currentQuestion_7(),
	Listening2SceneController_t3779648327::get_offset_of_resultGo_8(),
	Listening2SceneController_t3779648327_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
	Listening2SceneController_t3779648327_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	Listening2SceneController_t3779648327_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (U3CStartU3Ec__Iterator0_t2772662239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[4] = 
{
	U3CStartU3Ec__Iterator0_t2772662239::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2772662239::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2772662239::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2772662239::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (Listening2SelectableItem_t492261174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[11] = 
{
	Listening2SelectableItem_t492261174::get_offset_of__trueBg_2(),
	Listening2SelectableItem_t492261174::get_offset_of__trueText_3(),
	Listening2SelectableItem_t492261174::get_offset_of__falseBg_4(),
	Listening2SelectableItem_t492261174::get_offset_of__falseText_5(),
	Listening2SelectableItem_t492261174::get_offset_of__onSprite_6(),
	Listening2SelectableItem_t492261174::get_offset_of__offSprite_7(),
	Listening2SelectableItem_t492261174::get_offset_of__text_8(),
	Listening2SelectableItem_t492261174::get_offset_of__selected_9(),
	Listening2SelectableItem_t492261174::get_offset_of__frameLeft_10(),
	Listening2SelectableItem_t492261174::get_offset_of__framRight_11(),
	Listening2SelectableItem_t492261174::get_offset_of_onSelectItemClicked_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (U3CLerpFrameU3Ec__Iterator0_t379814438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[4] = 
{
	U3CLerpFrameU3Ec__Iterator0_t379814438::get_offset_of__frame_0(),
	U3CLerpFrameU3Ec__Iterator0_t379814438::get_offset_of_U24current_1(),
	U3CLerpFrameU3Ec__Iterator0_t379814438::get_offset_of_U24disposing_2(),
	U3CLerpFrameU3Ec__Iterator0_t379814438::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (SceneName_t904752595)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2528[24] = 
{
	SceneName_t904752595::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (LoadSceneScript_t888325993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[4] = 
{
	LoadSceneScript_t888325993::get_offset_of_autoLoad_2(),
	LoadSceneScript_t888325993::get_offset_of_cutScene_3(),
	LoadSceneScript_t888325993::get_offset_of_additiveScene_4(),
	LoadSceneScript_t888325993::get_offset_of_baseScene_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (U3CAwakeU3Ec__AnonStorey1_t1264054739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[2] = 
{
	U3CAwakeU3Ec__AnonStorey1_t1264054739::get_offset_of_btn_0(),
	U3CAwakeU3Ec__AnonStorey1_t1264054739::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (U3CDelayLoadVRU3Ec__Iterator0_t2009451244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	U3CDelayLoadVRU3Ec__Iterator0_t2009451244::get_offset_of_U24this_0(),
	U3CDelayLoadVRU3Ec__Iterator0_t2009451244::get_offset_of_U24current_1(),
	U3CDelayLoadVRU3Ec__Iterator0_t2009451244::get_offset_of_U24disposing_2(),
	U3CDelayLoadVRU3Ec__Iterator0_t2009451244::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (AutoChangeFont_t1379389610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[2] = 
{
	AutoChangeFont_t1379389610::get_offset_of_normalFont_2(),
	AutoChangeFont_t1379389610::get_offset_of_boldFont_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (ButtonAutoPlay_t2268545083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (ButtonMike_t3037018370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[13] = 
{
	ButtonMike_t3037018370::get_offset_of_handleRecording_2(),
	ButtonMike_t3037018370::get_offset_of_audioSource_3(),
	ButtonMike_t3037018370::get_offset_of_isRecording_4(),
	ButtonMike_t3037018370::get_offset_of_timeCountdonwn_5(),
	ButtonMike_t3037018370::get_offset_of_tempTimeCountdonwn_6(),
	ButtonMike_t3037018370::get_offset_of_popupRecording_7(),
	ButtonMike_t3037018370::get_offset_of_sprDefault_8(),
	ButtonMike_t3037018370::get_offset_of_sprRecording_9(),
	ButtonMike_t3037018370::get_offset_of_btnListen_10(),
	ButtonMike_t3037018370::get_offset_of_btnNext_11(),
	ButtonMike_t3037018370::get_offset_of_img_12(),
	ButtonMike_t3037018370::get_offset_of_isHolding_13(),
	ButtonMike_t3037018370::get_offset_of_isRecorded_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (U3CIESavingVoiceU3Ec__Iterator0_t1871642022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[4] = 
{
	U3CIESavingVoiceU3Ec__Iterator0_t1871642022::get_offset_of_U24this_0(),
	U3CIESavingVoiceU3Ec__Iterator0_t1871642022::get_offset_of_U24current_1(),
	U3CIESavingVoiceU3Ec__Iterator0_t1871642022::get_offset_of_U24disposing_2(),
	U3CIESavingVoiceU3Ec__Iterator0_t1871642022::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (ButtonMode_t2270855263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (ConversationController_t3486580231), -1, sizeof(ConversationController_t3486580231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2537[9] = 
{
	ConversationController_t3486580231::get_offset_of_btnStartMenu_2(),
	ConversationController_t3486580231::get_offset_of_imgBg_3(),
	ConversationController_t3486580231::get_offset_of_imgCharacter_4(),
	ConversationController_t3486580231::get_offset_of_conversationSelector_5(),
	ConversationController_t3486580231::get_offset_of_characterSelector_6(),
	ConversationController_t3486580231::get_offset_of_dialogPrefab_7(),
	ConversationController_t3486580231_StaticFields::get_offset_of_btnModel_8(),
	ConversationController_t3486580231_StaticFields::get_offset_of_isStarted_9(),
	ConversationController_t3486580231_StaticFields::get_offset_of_isSelectedConversation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (U3CQuiteVRModeU3Ec__AnonStorey0_t118348258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[2] = 
{
	U3CQuiteVRModeU3Ec__AnonStorey0_t118348258::get_offset_of_popupQuit_0(),
	U3CQuiteVRModeU3Ec__AnonStorey0_t118348258::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (DialogController_t3845653284), -1, sizeof(DialogController_t3845653284_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2539[20] = 
{
	DialogController_t3845653284::get_offset_of_me_talk_dialog_2(),
	DialogController_t3845653284::get_offset_of_end_record_dialog_3(),
	DialogController_t3845653284::get_offset_of_you_talk_sound_dialog_4(),
	DialogController_t3845653284::get_offset_of_you_talk_mini_dialog_5(),
	DialogController_t3845653284::get_offset_of_historyConversation_6(),
	DialogController_t3845653284::get_offset_of_showSubYouDialog_7(),
	DialogController_t3845653284::get_offset_of_dialogs_8(),
	DialogController_t3845653284::get_offset_of_dialogUser_9(),
	DialogController_t3845653284::get_offset_of_dialogYou_10(),
	DialogController_t3845653284_StaticFields::get_offset_of_indexDialog_11(),
	DialogController_t3845653284_StaticFields::get_offset_of_indexYou_12(),
	DialogController_t3845653284_StaticFields::get_offset_of_indexUser_13(),
	DialogController_t3845653284::get_offset_of_currentDialog_14(),
	DialogController_t3845653284_StaticFields::get_offset_of_content_15(),
	DialogController_t3845653284_StaticFields::get_offset_of_subContent_16(),
	DialogController_t3845653284_StaticFields::get_offset_of_voice_17(),
	DialogController_t3845653284_StaticFields::get_offset_of_indexItemHistory_18(),
	DialogController_t3845653284_StaticFields::get_offset_of_startIndex_19(),
	DialogController_t3845653284_StaticFields::get_offset_of_isAutoPlay_20(),
	DialogController_t3845653284_StaticFields::get_offset_of_isAutoPlayTemplate_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (Dialog_t1378192732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[5] = 
{
	Dialog_t1378192732::get_offset_of_content_0(),
	Dialog_t1378192732::get_offset_of_subContent_1(),
	Dialog_t1378192732::get_offset_of_isUser_2(),
	Dialog_t1378192732::get_offset_of_voice_3(),
	Dialog_t1378192732::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (DialogPanel_t1568014038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[5] = 
{
	DialogPanel_t1568014038::get_offset_of_dialog_2(),
	DialogPanel_t1568014038::get_offset_of_txt_Eng_3(),
	DialogPanel_t1568014038::get_offset_of_txt_Jp_4(),
	DialogPanel_t1568014038::get_offset_of_Type_5(),
	DialogPanel_t1568014038::get_offset_of_audioSource_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (U3CPlayVoiceU3Ec__Iterator0_t3000682236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[6] = 
{
	U3CPlayVoiceU3Ec__Iterator0_t3000682236::get_offset_of_name_0(),
	U3CPlayVoiceU3Ec__Iterator0_t3000682236::get_offset_of_OnFinish_1(),
	U3CPlayVoiceU3Ec__Iterator0_t3000682236::get_offset_of_U24this_2(),
	U3CPlayVoiceU3Ec__Iterator0_t3000682236::get_offset_of_U24current_3(),
	U3CPlayVoiceU3Ec__Iterator0_t3000682236::get_offset_of_U24disposing_4(),
	U3CPlayVoiceU3Ec__Iterator0_t3000682236::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[6] = 
{
	U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054::get_offset_of_name_0(),
	U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054::get_offset_of_currentDialog_1(),
	U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054::get_offset_of_U24this_2(),
	U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054::get_offset_of_U24current_3(),
	U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054::get_offset_of_U24disposing_4(),
	U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (DisableWithTimeAudioName_t4015415354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[8] = 
{
	DisableWithTimeAudioName_t4015415354::get_offset_of_autoDestroy_2(),
	DisableWithTimeAudioName_t4015415354::get_offset_of_autoPlaySoundRecorded_3(),
	DisableWithTimeAudioName_t4015415354::get_offset_of_autoPlaySoundTemplate_4(),
	DisableWithTimeAudioName_t4015415354::get_offset_of_audioSource_5(),
	DisableWithTimeAudioName_t4015415354::get_offset_of_timeDelayStart_6(),
	DisableWithTimeAudioName_t4015415354::get_offset_of_timeDelayFinish_7(),
	DisableWithTimeAudioName_t4015415354::get_offset_of_OnFinish_8(),
	DisableWithTimeAudioName_t4015415354::get_offset_of_clipTemplate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[7] = 
{
	U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453::get_offset_of_U3CpathU3E__0_0(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453::get_offset_of_U3CwwwU3E__1_1(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453::get_offset_of_U3CclipU3E__2_2(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453::get_offset_of_U24this_3(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453::get_offset_of_U24current_4(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453::get_offset_of_U24disposing_5(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t965722453::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (DisableWithTimeCountdown_t622718188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[5] = 
{
	DisableWithTimeCountdown_t622718188::get_offset_of_autoDestroy_2(),
	DisableWithTimeCountdown_t622718188::get_offset_of_timeCountdown_3(),
	DisableWithTimeCountdown_t622718188::get_offset_of_timeDelayStart_4(),
	DisableWithTimeCountdown_t622718188::get_offset_of_timeDelayFinish_5(),
	DisableWithTimeCountdown_t622718188::get_offset_of_OnFinish_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (U3CPlayAudioAndDisableU3Ec__Iterator0_t3247491393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[4] = 
{
	U3CPlayAudioAndDisableU3Ec__Iterator0_t3247491393::get_offset_of_U24this_0(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t3247491393::get_offset_of_U24current_1(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t3247491393::get_offset_of_U24disposing_2(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t3247491393::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (DisableWithTimeSound_t417185280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[5] = 
{
	DisableWithTimeSound_t417185280::get_offset_of_autoDestroy_2(),
	DisableWithTimeSound_t417185280::get_offset_of_audioSource_3(),
	DisableWithTimeSound_t417185280::get_offset_of_autoPlayAudio_4(),
	DisableWithTimeSound_t417185280::get_offset_of_timeDelayFinish_5(),
	DisableWithTimeSound_t417185280::get_offset_of_OnFinish_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[4] = 
{
	U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567::get_offset_of_U24this_0(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567::get_offset_of_U24current_1(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567::get_offset_of_U24disposing_2(),
	U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (HandleRecording_t1220661029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[3] = 
{
	HandleRecording_t1220661029::get_offset_of_audioSource_2(),
	HandleRecording_t1220661029::get_offset_of_pathSaved_3(),
	HandleRecording_t1220661029::get_offset_of_recorder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (Popup_t161311578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[2] = 
{
	Popup_t161311578::get_offset_of_txtContent_2(),
	Popup_t161311578::get_offset_of_txtSubContent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (HistoryConversation_t3119154081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[8] = 
{
	HistoryConversation_t3119154081::get_offset_of_indexItem_2(),
	HistoryConversation_t3119154081::get_offset_of_itemMe_3(),
	HistoryConversation_t3119154081::get_offset_of_itemYou_4(),
	HistoryConversation_t3119154081::get_offset_of_btnUp_5(),
	HistoryConversation_t3119154081::get_offset_of_btnDown_6(),
	HistoryConversation_t3119154081::get_offset_of_lstItemDialog_7(),
	HistoryConversation_t3119154081::get_offset_of_dialogMe_8(),
	HistoryConversation_t3119154081::get_offset_of_dialogYou_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (MoveContent_t4217566478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[3] = 
{
	MoveContent_t4217566478::get_offset_of_speed_2(),
	MoveContent_t4217566478::get_offset_of_content_3(),
	MoveContent_t4217566478::get_offset_of_isHolding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (TextKaraoke_t2450194175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[7] = 
{
	TextKaraoke_t2450194175::get_offset_of_content_2(),
	TextKaraoke_t2450194175::get_offset_of_colorDefault_3(),
	TextKaraoke_t2450194175::get_offset_of_colorKaraoke_4(),
	TextKaraoke_t2450194175::get_offset_of_audioSource_5(),
	TextKaraoke_t2450194175::get_offset_of_txtMain_6(),
	TextKaraoke_t2450194175::get_offset_of_goKara_7(),
	TextKaraoke_t2450194175::get_offset_of_delayStart_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (U3CAutoTypingU3Ec__Iterator0_t785846093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[12] = 
{
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_voice_0(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U3CclipU3E__0_1(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U3CtimeSpaceU3E__1_2(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U3CtempU3E__2_3(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U3CiU3E__3_4(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U3CjU3E__4_5(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U3CpaintColorU3E__5_6(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U3CpassedTimeU3E__6_7(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U24this_8(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U24current_9(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U24disposing_10(),
	U3CAutoTypingU3Ec__Iterator0_t785846093::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (U3CAutoTypingU3Ec__Iterator1_t2351930034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[11] = 
{
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_audioSource_0(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U3CtimeSpaceU3E__0_1(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U3CtempU3E__1_2(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U3CiU3E__2_3(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U3CjU3E__3_4(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U3CpaintColorU3E__4_5(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U3CpassedTimeU3E__5_6(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U24this_7(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U24current_8(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U24disposing_9(),
	U3CAutoTypingU3Ec__Iterator1_t2351930034::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (GetProductID_t3571953046), -1, sizeof(GetProductID_t3571953046_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2557[5] = 
{
	GetProductID_t3571953046::get_offset_of_productID_2(),
	GetProductID_t3571953046::get_offset_of_suffixesID_3(),
	GetProductID_t3571953046::get_offset_of_prefixID_4(),
	GetProductID_t3571953046_StaticFields::get_offset_of_U3CProductIDU3Ek__BackingField_5(),
	GetProductID_t3571953046::get_offset_of_txtDebug_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (GetValueFromRemoteSeting_t1223913735), -1, sizeof(GetValueFromRemoteSeting_t1223913735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2558[7] = 
{
	GetValueFromRemoteSeting_t1223913735::get_offset_of_suffixesID_0(),
	GetValueFromRemoteSeting_t1223913735::get_offset_of_prefixID_1(),
	GetValueFromRemoteSeting_t1223913735_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_2(),
	GetValueFromRemoteSeting_t1223913735::get_offset_of_U3CProductIDU3Ek__BackingField_3(),
	GetValueFromRemoteSeting_t1223913735::get_offset_of_U3CVersionU3Ek__BackingField_4(),
	GetValueFromRemoteSeting_t1223913735::get_offset_of_U3CPriceProductU3Ek__BackingField_5(),
	GetValueFromRemoteSeting_t1223913735::get_offset_of_U3CUrlU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (CircleScript_t1225364279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[16] = 
{
	CircleScript_t1225364279::get_offset_of_radius_2(),
	CircleScript_t1225364279::get_offset_of_offsetStartAngle_3(),
	CircleScript_t1225364279::get_offset_of_offsetRadius_4(),
	CircleScript_t1225364279::get_offset_of_offsetFillAmountHead_5(),
	CircleScript_t1225364279::get_offset_of_heightHeadTail_6(),
	CircleScript_t1225364279::get_offset_of_withHeadTail_7(),
	CircleScript_t1225364279::get_offset_of_speed_8(),
	CircleScript_t1225364279::get_offset_of_txtValue_9(),
	CircleScript_t1225364279::get_offset_of_head_10(),
	CircleScript_t1225364279::get_offset_of_tail_11(),
	CircleScript_t1225364279::get_offset_of_center_12(),
	CircleScript_t1225364279::get_offset_of_imgCicle_13(),
	CircleScript_t1225364279::get_offset_of_oriFillAmountHead_14(),
	CircleScript_t1225364279::get_offset_of_AcrDegreeHead_15(),
	CircleScript_t1225364279::get_offset_of_isSetting_16(),
	CircleScript_t1225364279::get_offset_of_U3CValueU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (LearningResultData_t1558878615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[5] = 
{
	LearningResultData_t1558878615::get_offset_of_time_0(),
	LearningResultData_t1558878615::get_offset_of_achievement_1(),
	LearningResultData_t1558878615::get_offset_of_positive_2(),
	LearningResultData_t1558878615::get_offset_of_mistake_3(),
	LearningResultData_t1558878615::get_offset_of_fit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (LearningResultDataExtension_t2364822164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (ResultRateController_t2087696941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[4] = 
{
	ResultRateController_t2087696941::get_offset_of_view_2(),
	ResultRateController_t2087696941::get_offset_of_slider_3(),
	ResultRateController_t2087696941::get_offset_of_param_4(),
	ResultRateController_t2087696941::get_offset_of_nameLesson_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (ResultRateParam_t3783906052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[9] = 
{
	ResultRateParam_t3783906052::get_offset_of_nameResult_2(),
	ResultRateParam_t3783906052::get_offset_of_nameCharacter_3(),
	ResultRateParam_t3783906052::get_offset_of_frequencyLearnToday_4(),
	ResultRateParam_t3783906052::get_offset_of_frequencyLearnTotal_5(),
	ResultRateParam_t3783906052::get_offset_of_timeLearnToday_6(),
	ResultRateParam_t3783906052::get_offset_of_timeLearnTotal_7(),
	ResultRateParam_t3783906052::get_offset_of_valueSlider_8(),
	ResultRateParam_t3783906052::get_offset_of_totalTimeRecorded_9(),
	ResultRateParam_t3783906052::get_offset_of_totalTimeSoundTemplate_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (ResultRateSlider_t127081558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[6] = 
{
	ResultRateSlider_t127081558::get_offset_of_sliderTimeRate_2(),
	ResultRateSlider_t127081558::get_offset_of_percentText_3(),
	ResultRateSlider_t127081558::get_offset_of_fillCenter_4(),
	ResultRateSlider_t127081558::get_offset_of_percentTextLowContainer_5(),
	ResultRateSlider_t127081558::get_offset_of_sliderFIT_6(),
	ResultRateSlider_t127081558::get_offset_of_lenghtSliderTimeRate_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (ResultRateView_t1401090222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[12] = 
{
	ResultRateView_t1401090222::get_offset_of_txtNameResult_2(),
	ResultRateView_t1401090222::get_offset_of_txtNameCharacter_3(),
	ResultRateView_t1401090222::get_offset_of_txtFrequencyLearnToday_4(),
	ResultRateView_t1401090222::get_offset_of_txtFrequencyLearnTotal_5(),
	ResultRateView_t1401090222::get_offset_of_txtTimeLearnToday_6(),
	ResultRateView_t1401090222::get_offset_of_txtTimeLearnTotal_7(),
	ResultRateView_t1401090222::get_offset_of_txtComment_8(),
	ResultRateView_t1401090222::get_offset_of_btnReload_9(),
	ResultRateView_t1401090222::get_offset_of_btnAutoPlay_10(),
	ResultRateView_t1401090222::get_offset_of_btnFinish_11(),
	ResultRateView_t1401090222::get_offset_of_btnHelp_12(),
	ResultRateView_t1401090222::get_offset_of_btnBack_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (CategoryId_t2845940439)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2566[7] = 
{
	CategoryId_t2845940439::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (ResultSaveResponse_t3438979681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[2] = 
{
	ResultSaveResponse_t3438979681::get_offset_of_success_0(),
	ResultSaveResponse_t3438979681::get_offset_of_errorMsg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (ZipSoundItem_t558808959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[4] = 
{
	ZipSoundItem_t558808959::get_offset_of_chapter_0(),
	ZipSoundItem_t558808959::get_offset_of_category_1(),
	ZipSoundItem_t558808959::get_offset_of_part_2(),
	ZipSoundItem_t558808959::get_offset_of_voice_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (ResultSaver_t3290295394), -1, sizeof(ResultSaver_t3290295394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2569[1] = 
{
	ResultSaver_t3290295394_StaticFields::get_offset_of__instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (U3CDoPushLearningResultU3Ec__AnonStorey0_t3642887568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[2] = 
{
	U3CDoPushLearningResultU3Ec__AnonStorey0_t3642887568::get_offset_of_successCallback_0(),
	U3CDoPushLearningResultU3Ec__AnonStorey0_t3642887568::get_offset_of_errorCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (U3CPushSoundZipU3Ec__AnonStorey1_t1694356377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[2] = 
{
	U3CPushSoundZipU3Ec__AnonStorey1_t1694356377::get_offset_of_successCallback_0(),
	U3CPushSoundZipU3Ec__AnonStorey1_t1694356377::get_offset_of_errorCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (ResultStatisticChart_t3759290537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[1] = 
{
	ResultStatisticChart_t3759290537::get_offset_of_circleScript_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (ResultStatisticController_t2929782681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[4] = 
{
	ResultStatisticController_t2929782681::get_offset_of_view_2(),
	ResultStatisticController_t2929782681::get_offset_of_chart_3(),
	ResultStatisticController_t2929782681::get_offset_of_param_4(),
	ResultStatisticController_t2929782681::get_offset_of_nameLesson_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (ResultStatisticParam_t3727789074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[9] = 
{
	ResultStatisticParam_t3727789074::get_offset_of_nameResult_2(),
	ResultStatisticParam_t3727789074::get_offset_of_totalAnswerCorrect_3(),
	ResultStatisticParam_t3727789074::get_offset_of_totalAnswerWrong_4(),
	ResultStatisticParam_t3727789074::get_offset_of_totalAnwerCorrectListening_5(),
	ResultStatisticParam_t3727789074::get_offset_of_totalAnwerWrongListening_6(),
	ResultStatisticParam_t3727789074::get_offset_of_frequencyLearnToday_7(),
	ResultStatisticParam_t3727789074::get_offset_of_frequencyLearnTotal_8(),
	ResultStatisticParam_t3727789074::get_offset_of_timeLearnToday_9(),
	ResultStatisticParam_t3727789074::get_offset_of_timeLearnTotal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (ResultStatisticView_t1911483820), -1, sizeof(ResultStatisticView_t1911483820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2575[11] = 
{
	ResultStatisticView_t1911483820::get_offset_of_txtNameResult_2(),
	ResultStatisticView_t1911483820::get_offset_of_txtCorrectAnswerOverview_3(),
	ResultStatisticView_t1911483820::get_offset_of_txtFrequencyLearnToday_4(),
	ResultStatisticView_t1911483820::get_offset_of_txtFrequencyLearnTotal_5(),
	ResultStatisticView_t1911483820::get_offset_of_txtTimeLearnToday_6(),
	ResultStatisticView_t1911483820::get_offset_of_txtTimeLearnTotal_7(),
	ResultStatisticView_t1911483820::get_offset_of_btnReload_8(),
	ResultStatisticView_t1911483820::get_offset_of_btnFinish_9(),
	ResultStatisticView_t1911483820::get_offset_of_btnHelp_10(),
	ResultStatisticView_t1911483820::get_offset_of_btnBack_11(),
	ResultStatisticView_t1911483820_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (BaseOnplayQuestion_t1717064182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[2] = 
{
	BaseOnplayQuestion_t1717064182::get_offset_of_onQuestionCompleted_0(),
	BaseOnplayQuestion_t1717064182::get_offset_of_U3CisCompletedU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (QuestionScenePopup_t792224618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[7] = 
{
	QuestionScenePopup_t792224618::get_offset_of_correctImage_2(),
	QuestionScenePopup_t792224618::get_offset_of_notCorrectImage_3(),
	QuestionScenePopup_t792224618::get_offset_of_notPerfectCorrectImage_4(),
	QuestionScenePopup_t792224618::get_offset_of_onResultPopupClosed_5(),
	QuestionScenePopup_t792224618::get_offset_of_resultPopup_6(),
	QuestionScenePopup_t792224618::get_offset_of_animResultOpen_7(),
	QuestionScenePopup_t792224618::get_offset_of_animResultClose_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (U3CPlayAnimationU3Ec__Iterator0_t1105760000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[7] = 
{
	U3CPlayAnimationU3Ec__Iterator0_t1105760000::get_offset_of_anim_0(),
	U3CPlayAnimationU3Ec__Iterator0_t1105760000::get_offset_of_baseData_1(),
	U3CPlayAnimationU3Ec__Iterator0_t1105760000::get_offset_of_answers_2(),
	U3CPlayAnimationU3Ec__Iterator0_t1105760000::get_offset_of_U24this_3(),
	U3CPlayAnimationU3Ec__Iterator0_t1105760000::get_offset_of_U24current_4(),
	U3CPlayAnimationU3Ec__Iterator0_t1105760000::get_offset_of_U24disposing_5(),
	U3CPlayAnimationU3Ec__Iterator0_t1105760000::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (SelectableItem_t2941161729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[6] = 
{
	SelectableItem_t2941161729::get_offset_of__offBG_2(),
	SelectableItem_t2941161729::get_offset_of_onBG_3(),
	SelectableItem_t2941161729::get_offset_of__text_4(),
	SelectableItem_t2941161729::get_offset_of__frame_5(),
	SelectableItem_t2941161729::get_offset_of__selected_6(),
	SelectableItem_t2941161729::get_offset_of_onSelectItemClicked_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (U3CShowFrameAnimationU3Ec__Iterator0_t3617874698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[4] = 
{
	U3CShowFrameAnimationU3Ec__Iterator0_t3617874698::get_offset_of_U24this_0(),
	U3CShowFrameAnimationU3Ec__Iterator0_t3617874698::get_offset_of_U24current_1(),
	U3CShowFrameAnimationU3Ec__Iterator0_t3617874698::get_offset_of_U24disposing_2(),
	U3CShowFrameAnimationU3Ec__Iterator0_t3617874698::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (BaseData_t450384115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (QuestionData_t3323763512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (SentenceStructureIdiomQuestionData_t3251732102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[8] = 
{
	SentenceStructureIdiomQuestionData_t3251732102::get_offset_of_U3CanswerText0U3Ek__BackingField_0(),
	SentenceStructureIdiomQuestionData_t3251732102::get_offset_of_U3CanswerText1U3Ek__BackingField_1(),
	SentenceStructureIdiomQuestionData_t3251732102::get_offset_of_U3CanswerText2U3Ek__BackingField_2(),
	SentenceStructureIdiomQuestionData_t3251732102::get_offset_of_U3CanswerText3U3Ek__BackingField_3(),
	SentenceStructureIdiomQuestionData_t3251732102::get_offset_of_U3CcorrectAnswerU3Ek__BackingField_4(),
	SentenceStructureIdiomQuestionData_t3251732102::get_offset_of_U3CquestionTextJPU3Ek__BackingField_5(),
	SentenceStructureIdiomQuestionData_t3251732102::get_offset_of_U3CquestionTextENU3Ek__BackingField_6(),
	SentenceStructureIdiomQuestionData_t3251732102::get_offset_of_U3CsoundDataU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (BaseQuestionView_t79922856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[1] = 
{
	BaseQuestionView_t79922856::get_offset_of_progressText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Sentence1DataLoader_t1039713923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[3] = 
{
	Sentence1DataLoader_t1039713923::get_offset_of_dataSheet_2(),
	Sentence1DataLoader_t1039713923::get_offset_of_U3CdatasWrapperU3Ek__BackingField_3(),
	Sentence1DataLoader_t1039713923::get_offset_of__questionBundleText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (Sentence1DatasWrapper_t174893388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[1] = 
{
	Sentence1DatasWrapper_t174893388::get_offset_of_U3CwrappedDatasU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (Sentence1SceneController_t2055584412), -1, sizeof(Sentence1SceneController_t2055584412_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[9] = 
{
	Sentence1SceneController_t2055584412::get_offset_of_questionBundleText_2(),
	Sentence1SceneController_t2055584412::get_offset_of_dataLoader_3(),
	Sentence1SceneController_t2055584412::get_offset_of_onPlayQuestions_4(),
	Sentence1SceneController_t2055584412::get_offset_of_questionDatas_5(),
	Sentence1SceneController_t2055584412::get_offset_of_questionView_6(),
	Sentence1SceneController_t2055584412::get_offset_of__currentQuestion_7(),
	Sentence1SceneController_t2055584412::get_offset_of_soundPath_8(),
	Sentence1SceneController_t2055584412::get_offset_of_startTime_9(),
	Sentence1SceneController_t2055584412_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (U3CStartU3Ec__Iterator0_t1440754326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[4] = 
{
	U3CStartU3Ec__Iterator0_t1440754326::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1440754326::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1440754326::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1440754326::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (Sentence1Test_t1755393472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (SentenceQuestionView_t1478559372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[13] = 
{
	SentenceQuestionView_t1478559372::get_offset_of_answer0_3(),
	SentenceQuestionView_t1478559372::get_offset_of_answer1_4(),
	SentenceQuestionView_t1478559372::get_offset_of_answer2_5(),
	SentenceQuestionView_t1478559372::get_offset_of_answer3_6(),
	SentenceQuestionView_t1478559372::get_offset_of_questionTextJP_7(),
	SentenceQuestionView_t1478559372::get_offset_of_questionTextEN_8(),
	SentenceQuestionView_t1478559372::get_offset_of_summitButton_9(),
	SentenceQuestionView_t1478559372::get_offset_of_soundButton_10(),
	SentenceQuestionView_t1478559372::get_offset_of_popup_11(),
	SentenceQuestionView_t1478559372::get_offset_of_anim_12(),
	SentenceQuestionView_t1478559372::get_offset_of_answerSound_13(),
	SentenceQuestionView_t1478559372::get_offset_of_animResultOpen_14(),
	SentenceQuestionView_t1478559372::get_offset_of_animResultClose_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (U3CPlayAnimationU3Ec__Iterator0_t905375778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[4] = 
{
	U3CPlayAnimationU3Ec__Iterator0_t905375778::get_offset_of_U24this_0(),
	U3CPlayAnimationU3Ec__Iterator0_t905375778::get_offset_of_U24current_1(),
	U3CPlayAnimationU3Ec__Iterator0_t905375778::get_offset_of_U24disposing_2(),
	U3CPlayAnimationU3Ec__Iterator0_t905375778::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (SentenceStructureIdiomOnPlayQuestion_t936886987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[4] = 
{
	SentenceStructureIdiomOnPlayQuestion_t936886987::get_offset_of_soundPath_2(),
	SentenceStructureIdiomOnPlayQuestion_t936886987::get_offset_of__currentSelected_3(),
	SentenceStructureIdiomOnPlayQuestion_t936886987::get_offset_of__view_4(),
	SentenceStructureIdiomOnPlayQuestion_t936886987::get_offset_of_U3CquestionDataU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (SelectedState_t3958551864)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2597[6] = 
{
	SelectedState_t3958551864::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (Sentence2DataLoader_t844185122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[3] = 
{
	Sentence2DataLoader_t844185122::get_offset_of_dataSheet_2(),
	Sentence2DataLoader_t844185122::get_offset_of_U3CdatasWrapperU3Ek__BackingField_3(),
	Sentence2DataLoader_t844185122::get_offset_of__questionBundleText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (Sentence2DatasWrapper_t2314527497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[1] = 
{
	Sentence2DatasWrapper_t2314527497::get_offset_of_U3CwrappedDatasU3Ek__BackingField_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
