﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ConversationView
struct ConversationView_t3413307054;
// ConversationLogicController
struct ConversationLogicController_t3911886281;
// BaseConversationCommandLoader
struct BaseConversationCommandLoader_t1091304408;
// UnityEngine.Transform
struct Transform_t3275118058;
// ResultRateController
struct ResultRateController_t2087696941;
// ConversationDataSaver
struct ConversationDataSaver_t1206770280;
// EyeRaycaster
struct EyeRaycaster_t3983099531;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_BaseConversationController_Conve1349939935.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseConversationController
struct  BaseConversationController_t808907754  : public MonoBehaviour_t1158329972
{
public:
	// System.String BaseConversationController::nameLesson
	String_t* ___nameLesson_2;
	// System.Int32 BaseConversationController::level
	int32_t ___level_3;
	// System.DateTime BaseConversationController::startTime
	DateTime_t693205669  ___startTime_4;
	// System.String BaseConversationController::characterRole0
	String_t* ___characterRole0_5;
	// System.String BaseConversationController::characterRole1
	String_t* ___characterRole1_6;
	// System.String BaseConversationController::characterName0
	String_t* ___characterName0_7;
	// System.String BaseConversationController::characterName1
	String_t* ___characterName1_8;
	// BaseConversationController/ConversationState BaseConversationController::_state
	int32_t ____state_9;
	// ConversationView BaseConversationController::conversationView
	ConversationView_t3413307054 * ___conversationView_10;
	// ConversationLogicController BaseConversationController::conversationLogicController
	ConversationLogicController_t3911886281 * ___conversationLogicController_11;
	// BaseConversationCommandLoader BaseConversationController::commandLoader
	BaseConversationCommandLoader_t1091304408 * ___commandLoader_12;
	// System.String BaseConversationController::playerRole
	String_t* ___playerRole_13;
	// System.String BaseConversationController::otherRole
	String_t* ___otherRole_14;
	// UnityEngine.Transform BaseConversationController::prefabCharacter
	Transform_t3275118058 * ___prefabCharacter_15;
	// UnityEngine.Transform BaseConversationController::buttonHelp
	Transform_t3275118058 * ___buttonHelp_16;
	// System.String BaseConversationController::soundSavePath
	String_t* ___soundSavePath_17;
	// System.String BaseConversationController::soundResourcePath
	String_t* ___soundResourcePath_18;
	// System.String BaseConversationController::textureResourcePath
	String_t* ___textureResourcePath_19;
	// ResultRateController BaseConversationController::rrc
	ResultRateController_t2087696941 * ___rrc_20;
	// ConversationDataSaver BaseConversationController::conversationDataSaver
	ConversationDataSaver_t1206770280 * ___conversationDataSaver_21;
	// System.Boolean BaseConversationController::_isReplay
	bool ____isReplay_22;
	// EyeRaycaster BaseConversationController::eyeRaycaster
	EyeRaycaster_t3983099531 * ___eyeRaycaster_23;
	// System.Single BaseConversationController::_eyeContactScore
	float ____eyeContactScore_24;

public:
	inline static int32_t get_offset_of_nameLesson_2() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___nameLesson_2)); }
	inline String_t* get_nameLesson_2() const { return ___nameLesson_2; }
	inline String_t** get_address_of_nameLesson_2() { return &___nameLesson_2; }
	inline void set_nameLesson_2(String_t* value)
	{
		___nameLesson_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameLesson_2, value);
	}

	inline static int32_t get_offset_of_level_3() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___level_3)); }
	inline int32_t get_level_3() const { return ___level_3; }
	inline int32_t* get_address_of_level_3() { return &___level_3; }
	inline void set_level_3(int32_t value)
	{
		___level_3 = value;
	}

	inline static int32_t get_offset_of_startTime_4() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___startTime_4)); }
	inline DateTime_t693205669  get_startTime_4() const { return ___startTime_4; }
	inline DateTime_t693205669 * get_address_of_startTime_4() { return &___startTime_4; }
	inline void set_startTime_4(DateTime_t693205669  value)
	{
		___startTime_4 = value;
	}

	inline static int32_t get_offset_of_characterRole0_5() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___characterRole0_5)); }
	inline String_t* get_characterRole0_5() const { return ___characterRole0_5; }
	inline String_t** get_address_of_characterRole0_5() { return &___characterRole0_5; }
	inline void set_characterRole0_5(String_t* value)
	{
		___characterRole0_5 = value;
		Il2CppCodeGenWriteBarrier(&___characterRole0_5, value);
	}

	inline static int32_t get_offset_of_characterRole1_6() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___characterRole1_6)); }
	inline String_t* get_characterRole1_6() const { return ___characterRole1_6; }
	inline String_t** get_address_of_characterRole1_6() { return &___characterRole1_6; }
	inline void set_characterRole1_6(String_t* value)
	{
		___characterRole1_6 = value;
		Il2CppCodeGenWriteBarrier(&___characterRole1_6, value);
	}

	inline static int32_t get_offset_of_characterName0_7() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___characterName0_7)); }
	inline String_t* get_characterName0_7() const { return ___characterName0_7; }
	inline String_t** get_address_of_characterName0_7() { return &___characterName0_7; }
	inline void set_characterName0_7(String_t* value)
	{
		___characterName0_7 = value;
		Il2CppCodeGenWriteBarrier(&___characterName0_7, value);
	}

	inline static int32_t get_offset_of_characterName1_8() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___characterName1_8)); }
	inline String_t* get_characterName1_8() const { return ___characterName1_8; }
	inline String_t** get_address_of_characterName1_8() { return &___characterName1_8; }
	inline void set_characterName1_8(String_t* value)
	{
		___characterName1_8 = value;
		Il2CppCodeGenWriteBarrier(&___characterName1_8, value);
	}

	inline static int32_t get_offset_of__state_9() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ____state_9)); }
	inline int32_t get__state_9() const { return ____state_9; }
	inline int32_t* get_address_of__state_9() { return &____state_9; }
	inline void set__state_9(int32_t value)
	{
		____state_9 = value;
	}

	inline static int32_t get_offset_of_conversationView_10() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___conversationView_10)); }
	inline ConversationView_t3413307054 * get_conversationView_10() const { return ___conversationView_10; }
	inline ConversationView_t3413307054 ** get_address_of_conversationView_10() { return &___conversationView_10; }
	inline void set_conversationView_10(ConversationView_t3413307054 * value)
	{
		___conversationView_10 = value;
		Il2CppCodeGenWriteBarrier(&___conversationView_10, value);
	}

	inline static int32_t get_offset_of_conversationLogicController_11() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___conversationLogicController_11)); }
	inline ConversationLogicController_t3911886281 * get_conversationLogicController_11() const { return ___conversationLogicController_11; }
	inline ConversationLogicController_t3911886281 ** get_address_of_conversationLogicController_11() { return &___conversationLogicController_11; }
	inline void set_conversationLogicController_11(ConversationLogicController_t3911886281 * value)
	{
		___conversationLogicController_11 = value;
		Il2CppCodeGenWriteBarrier(&___conversationLogicController_11, value);
	}

	inline static int32_t get_offset_of_commandLoader_12() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___commandLoader_12)); }
	inline BaseConversationCommandLoader_t1091304408 * get_commandLoader_12() const { return ___commandLoader_12; }
	inline BaseConversationCommandLoader_t1091304408 ** get_address_of_commandLoader_12() { return &___commandLoader_12; }
	inline void set_commandLoader_12(BaseConversationCommandLoader_t1091304408 * value)
	{
		___commandLoader_12 = value;
		Il2CppCodeGenWriteBarrier(&___commandLoader_12, value);
	}

	inline static int32_t get_offset_of_playerRole_13() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___playerRole_13)); }
	inline String_t* get_playerRole_13() const { return ___playerRole_13; }
	inline String_t** get_address_of_playerRole_13() { return &___playerRole_13; }
	inline void set_playerRole_13(String_t* value)
	{
		___playerRole_13 = value;
		Il2CppCodeGenWriteBarrier(&___playerRole_13, value);
	}

	inline static int32_t get_offset_of_otherRole_14() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___otherRole_14)); }
	inline String_t* get_otherRole_14() const { return ___otherRole_14; }
	inline String_t** get_address_of_otherRole_14() { return &___otherRole_14; }
	inline void set_otherRole_14(String_t* value)
	{
		___otherRole_14 = value;
		Il2CppCodeGenWriteBarrier(&___otherRole_14, value);
	}

	inline static int32_t get_offset_of_prefabCharacter_15() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___prefabCharacter_15)); }
	inline Transform_t3275118058 * get_prefabCharacter_15() const { return ___prefabCharacter_15; }
	inline Transform_t3275118058 ** get_address_of_prefabCharacter_15() { return &___prefabCharacter_15; }
	inline void set_prefabCharacter_15(Transform_t3275118058 * value)
	{
		___prefabCharacter_15 = value;
		Il2CppCodeGenWriteBarrier(&___prefabCharacter_15, value);
	}

	inline static int32_t get_offset_of_buttonHelp_16() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___buttonHelp_16)); }
	inline Transform_t3275118058 * get_buttonHelp_16() const { return ___buttonHelp_16; }
	inline Transform_t3275118058 ** get_address_of_buttonHelp_16() { return &___buttonHelp_16; }
	inline void set_buttonHelp_16(Transform_t3275118058 * value)
	{
		___buttonHelp_16 = value;
		Il2CppCodeGenWriteBarrier(&___buttonHelp_16, value);
	}

	inline static int32_t get_offset_of_soundSavePath_17() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___soundSavePath_17)); }
	inline String_t* get_soundSavePath_17() const { return ___soundSavePath_17; }
	inline String_t** get_address_of_soundSavePath_17() { return &___soundSavePath_17; }
	inline void set_soundSavePath_17(String_t* value)
	{
		___soundSavePath_17 = value;
		Il2CppCodeGenWriteBarrier(&___soundSavePath_17, value);
	}

	inline static int32_t get_offset_of_soundResourcePath_18() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___soundResourcePath_18)); }
	inline String_t* get_soundResourcePath_18() const { return ___soundResourcePath_18; }
	inline String_t** get_address_of_soundResourcePath_18() { return &___soundResourcePath_18; }
	inline void set_soundResourcePath_18(String_t* value)
	{
		___soundResourcePath_18 = value;
		Il2CppCodeGenWriteBarrier(&___soundResourcePath_18, value);
	}

	inline static int32_t get_offset_of_textureResourcePath_19() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___textureResourcePath_19)); }
	inline String_t* get_textureResourcePath_19() const { return ___textureResourcePath_19; }
	inline String_t** get_address_of_textureResourcePath_19() { return &___textureResourcePath_19; }
	inline void set_textureResourcePath_19(String_t* value)
	{
		___textureResourcePath_19 = value;
		Il2CppCodeGenWriteBarrier(&___textureResourcePath_19, value);
	}

	inline static int32_t get_offset_of_rrc_20() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___rrc_20)); }
	inline ResultRateController_t2087696941 * get_rrc_20() const { return ___rrc_20; }
	inline ResultRateController_t2087696941 ** get_address_of_rrc_20() { return &___rrc_20; }
	inline void set_rrc_20(ResultRateController_t2087696941 * value)
	{
		___rrc_20 = value;
		Il2CppCodeGenWriteBarrier(&___rrc_20, value);
	}

	inline static int32_t get_offset_of_conversationDataSaver_21() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___conversationDataSaver_21)); }
	inline ConversationDataSaver_t1206770280 * get_conversationDataSaver_21() const { return ___conversationDataSaver_21; }
	inline ConversationDataSaver_t1206770280 ** get_address_of_conversationDataSaver_21() { return &___conversationDataSaver_21; }
	inline void set_conversationDataSaver_21(ConversationDataSaver_t1206770280 * value)
	{
		___conversationDataSaver_21 = value;
		Il2CppCodeGenWriteBarrier(&___conversationDataSaver_21, value);
	}

	inline static int32_t get_offset_of__isReplay_22() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ____isReplay_22)); }
	inline bool get__isReplay_22() const { return ____isReplay_22; }
	inline bool* get_address_of__isReplay_22() { return &____isReplay_22; }
	inline void set__isReplay_22(bool value)
	{
		____isReplay_22 = value;
	}

	inline static int32_t get_offset_of_eyeRaycaster_23() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ___eyeRaycaster_23)); }
	inline EyeRaycaster_t3983099531 * get_eyeRaycaster_23() const { return ___eyeRaycaster_23; }
	inline EyeRaycaster_t3983099531 ** get_address_of_eyeRaycaster_23() { return &___eyeRaycaster_23; }
	inline void set_eyeRaycaster_23(EyeRaycaster_t3983099531 * value)
	{
		___eyeRaycaster_23 = value;
		Il2CppCodeGenWriteBarrier(&___eyeRaycaster_23, value);
	}

	inline static int32_t get_offset_of__eyeContactScore_24() { return static_cast<int32_t>(offsetof(BaseConversationController_t808907754, ____eyeContactScore_24)); }
	inline float get__eyeContactScore_24() const { return ____eyeContactScore_24; }
	inline float* get_address_of__eyeContactScore_24() { return &____eyeContactScore_24; }
	inline void set__eyeContactScore_24(float value)
	{
		____eyeContactScore_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
