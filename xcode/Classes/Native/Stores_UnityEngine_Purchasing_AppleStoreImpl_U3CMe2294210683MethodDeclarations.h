﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.AppleStoreImpl/<MessageCallback>c__AnonStorey1
struct U3CMessageCallbackU3Ec__AnonStorey1_t2294210683;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.AppleStoreImpl/<MessageCallback>c__AnonStorey1::.ctor()
extern "C"  void U3CMessageCallbackU3Ec__AnonStorey1__ctor_m1907423830 (U3CMessageCallbackU3Ec__AnonStorey1_t2294210683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.AppleStoreImpl/<MessageCallback>c__AnonStorey1::<>m__0()
extern "C"  void U3CMessageCallbackU3Ec__AnonStorey1_U3CU3Em__0_m249534733 (U3CMessageCallbackU3Ec__AnonStorey1_t2294210683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
