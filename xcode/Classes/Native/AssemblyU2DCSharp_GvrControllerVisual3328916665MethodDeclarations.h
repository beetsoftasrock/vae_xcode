﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrControllerVisual
struct GvrControllerVisual_t3328916665;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrControllerVisual::.ctor()
extern "C"  void GvrControllerVisual__ctor_m4119471196 (GvrControllerVisual_t3328916665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
