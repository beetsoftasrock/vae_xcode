﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reporter/Log
struct Log_t3604182180;

#include "codegen/il2cpp-codegen.h"

// System.Void Reporter/Log::.ctor()
extern "C"  void Log__ctor_m742278161 (Log_t3604182180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Reporter/Log Reporter/Log::CreateCopy()
extern "C"  Log_t3604182180 * Log_CreateCopy_m956153745 (Log_t3604182180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Reporter/Log::GetMemoryUsage()
extern "C"  float Log_GetMemoryUsage_m1330022541 (Log_t3604182180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
