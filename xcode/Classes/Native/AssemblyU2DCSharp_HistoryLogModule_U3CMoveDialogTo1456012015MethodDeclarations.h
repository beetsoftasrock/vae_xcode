﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryLogModule/<MoveDialogToStartPosition>c__Iterator2
struct U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HistoryLogModule/<MoveDialogToStartPosition>c__Iterator2::.ctor()
extern "C"  void U3CMoveDialogToStartPositionU3Ec__Iterator2__ctor_m1990761830 (U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HistoryLogModule/<MoveDialogToStartPosition>c__Iterator2::MoveNext()
extern "C"  bool U3CMoveDialogToStartPositionU3Ec__Iterator2_MoveNext_m2809215630 (U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryLogModule/<MoveDialogToStartPosition>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveDialogToStartPositionU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m877764254 (U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryLogModule/<MoveDialogToStartPosition>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveDialogToStartPositionU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m254497766 (U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryLogModule/<MoveDialogToStartPosition>c__Iterator2::Dispose()
extern "C"  void U3CMoveDialogToStartPositionU3Ec__Iterator2_Dispose_m1473234237 (U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryLogModule/<MoveDialogToStartPosition>c__Iterator2::Reset()
extern "C"  void U3CMoveDialogToStartPositionU3Ec__Iterator2_Reset_m1154479207 (U3CMoveDialogToStartPositionU3Ec__Iterator2_t1456012015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
