﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SceneName904752595.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadSceneScript
struct  LoadSceneScript_t888325993  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean LoadSceneScript::autoLoad
	bool ___autoLoad_2;
	// SceneName LoadSceneScript::cutScene
	int32_t ___cutScene_3;
	// SceneName LoadSceneScript::additiveScene
	int32_t ___additiveScene_4;
	// SceneName LoadSceneScript::baseScene
	int32_t ___baseScene_5;

public:
	inline static int32_t get_offset_of_autoLoad_2() { return static_cast<int32_t>(offsetof(LoadSceneScript_t888325993, ___autoLoad_2)); }
	inline bool get_autoLoad_2() const { return ___autoLoad_2; }
	inline bool* get_address_of_autoLoad_2() { return &___autoLoad_2; }
	inline void set_autoLoad_2(bool value)
	{
		___autoLoad_2 = value;
	}

	inline static int32_t get_offset_of_cutScene_3() { return static_cast<int32_t>(offsetof(LoadSceneScript_t888325993, ___cutScene_3)); }
	inline int32_t get_cutScene_3() const { return ___cutScene_3; }
	inline int32_t* get_address_of_cutScene_3() { return &___cutScene_3; }
	inline void set_cutScene_3(int32_t value)
	{
		___cutScene_3 = value;
	}

	inline static int32_t get_offset_of_additiveScene_4() { return static_cast<int32_t>(offsetof(LoadSceneScript_t888325993, ___additiveScene_4)); }
	inline int32_t get_additiveScene_4() const { return ___additiveScene_4; }
	inline int32_t* get_address_of_additiveScene_4() { return &___additiveScene_4; }
	inline void set_additiveScene_4(int32_t value)
	{
		___additiveScene_4 = value;
	}

	inline static int32_t get_offset_of_baseScene_5() { return static_cast<int32_t>(offsetof(LoadSceneScript_t888325993, ___baseScene_5)); }
	inline int32_t get_baseScene_5() const { return ___baseScene_5; }
	inline int32_t* get_address_of_baseScene_5() { return &___baseScene_5; }
	inline void set_baseScene_5(int32_t value)
	{
		___baseScene_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
