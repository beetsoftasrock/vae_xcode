﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2SelectableItem
struct Listening2SelectableItem_t492261174;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"

// System.Void Listening2SelectableItem::.ctor()
extern "C"  void Listening2SelectableItem__ctor_m1591097547 (Listening2SelectableItem_t492261174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening2SelectableItem::get_selected()
extern "C"  bool Listening2SelectableItem_get_selected_m2416264459 (Listening2SelectableItem_t492261174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem::set_selected(System.Boolean)
extern "C"  void Listening2SelectableItem_set_selected_m540322864 (Listening2SelectableItem_t492261174 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem::SetItemText(System.String)
extern "C"  void Listening2SelectableItem_SetItemText_m2658618879 (Listening2SelectableItem_t492261174 * __this, String_t* ___itemText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem::UpdateSelectedView()
extern "C"  void Listening2SelectableItem_UpdateSelectedView_m2123547776 (Listening2SelectableItem_t492261174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem::RefreshView()
extern "C"  void Listening2SelectableItem_RefreshView_m2540671435 (Listening2SelectableItem_t492261174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem::OnItemClick()
extern "C"  void Listening2SelectableItem_OnItemClick_m3045059605 (Listening2SelectableItem_t492261174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem::SetSelected()
extern "C"  void Listening2SelectableItem_SetSelected_m2385905140 (Listening2SelectableItem_t492261174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem::SetNoSelected()
extern "C"  void Listening2SelectableItem_SetNoSelected_m2434737313 (Listening2SelectableItem_t492261174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem::DisplayFrame(System.Boolean)
extern "C"  void Listening2SelectableItem_DisplayFrame_m2140110047 (Listening2SelectableItem_t492261174 * __this, bool ___isLeftOrRight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Listening2SelectableItem::LerpFrame(System.Boolean,UnityEngine.UI.Image)
extern "C"  Il2CppObject * Listening2SelectableItem_LerpFrame_m2274103210 (Listening2SelectableItem_t492261174 * __this, bool ___isLeftOrRight0, Image_t2042527209 * ____frame1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
