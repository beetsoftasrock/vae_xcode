﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstantConfig
struct  ConstantConfig_t1104991690  : public Il2CppObject
{
public:

public:
};

struct ConstantConfig_t1104991690_StaticFields
{
public:
	// System.String ConstantConfig::_API_SERVER_LINK
	String_t* ____API_SERVER_LINK_0;

public:
	inline static int32_t get_offset_of__API_SERVER_LINK_0() { return static_cast<int32_t>(offsetof(ConstantConfig_t1104991690_StaticFields, ____API_SERVER_LINK_0)); }
	inline String_t* get__API_SERVER_LINK_0() const { return ____API_SERVER_LINK_0; }
	inline String_t** get_address_of__API_SERVER_LINK_0() { return &____API_SERVER_LINK_0; }
	inline void set__API_SERVER_LINK_0(String_t* value)
	{
		____API_SERVER_LINK_0 = value;
		Il2CppCodeGenWriteBarrier(&____API_SERVER_LINK_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
