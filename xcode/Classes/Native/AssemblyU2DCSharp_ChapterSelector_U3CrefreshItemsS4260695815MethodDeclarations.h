﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterSelector/<refreshItemsState>c__Iterator1
struct U3CrefreshItemsStateU3Ec__Iterator1_t4260695815;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterSelector/<refreshItemsState>c__Iterator1::.ctor()
extern "C"  void U3CrefreshItemsStateU3Ec__Iterator1__ctor_m1944415106 (U3CrefreshItemsStateU3Ec__Iterator1_t4260695815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterSelector/<refreshItemsState>c__Iterator1::MoveNext()
extern "C"  bool U3CrefreshItemsStateU3Ec__Iterator1_MoveNext_m1417130582 (U3CrefreshItemsStateU3Ec__Iterator1_t4260695815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterSelector/<refreshItemsState>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CrefreshItemsStateU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1774169304 (U3CrefreshItemsStateU3Ec__Iterator1_t4260695815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterSelector/<refreshItemsState>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CrefreshItemsStateU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m639151856 (U3CrefreshItemsStateU3Ec__Iterator1_t4260695815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<refreshItemsState>c__Iterator1::Dispose()
extern "C"  void U3CrefreshItemsStateU3Ec__Iterator1_Dispose_m497823793 (U3CrefreshItemsStateU3Ec__Iterator1_t4260695815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<refreshItemsState>c__Iterator1::Reset()
extern "C"  void U3CrefreshItemsStateU3Ec__Iterator1_Reset_m2636170319 (U3CrefreshItemsStateU3Ec__Iterator1_t4260695815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
