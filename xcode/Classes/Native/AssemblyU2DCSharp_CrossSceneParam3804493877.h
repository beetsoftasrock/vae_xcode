﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CrossSceneParam
struct  CrossSceneParam_t3804493877  : public Il2CppObject
{
public:

public:
};

struct CrossSceneParam_t3804493877_StaticFields
{
public:
	// System.Boolean CrossSceneParam::autoPlayExercise
	bool ___autoPlayExercise_0;
	// System.Boolean CrossSceneParam::autoPlayConversation
	bool ___autoPlayConversation_1;

public:
	inline static int32_t get_offset_of_autoPlayExercise_0() { return static_cast<int32_t>(offsetof(CrossSceneParam_t3804493877_StaticFields, ___autoPlayExercise_0)); }
	inline bool get_autoPlayExercise_0() const { return ___autoPlayExercise_0; }
	inline bool* get_address_of_autoPlayExercise_0() { return &___autoPlayExercise_0; }
	inline void set_autoPlayExercise_0(bool value)
	{
		___autoPlayExercise_0 = value;
	}

	inline static int32_t get_offset_of_autoPlayConversation_1() { return static_cast<int32_t>(offsetof(CrossSceneParam_t3804493877_StaticFields, ___autoPlayConversation_1)); }
	inline bool get_autoPlayConversation_1() const { return ___autoPlayConversation_1; }
	inline bool* get_address_of_autoPlayConversation_1() { return &___autoPlayConversation_1; }
	inline void set_autoPlayConversation_1(bool value)
	{
		___autoPlayConversation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
