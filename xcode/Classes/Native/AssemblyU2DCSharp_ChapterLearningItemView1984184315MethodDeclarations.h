﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterLearningItemView
struct ChapterLearningItemView_t1984184315;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void ChapterLearningItemView::.ctor()
extern "C"  void ChapterLearningItemView__ctor_m274199928 (ChapterLearningItemView_t1984184315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChapterLearningItemView::get_name()
extern "C"  String_t* ChapterLearningItemView_get_name_m1986475389 (ChapterLearningItemView_t1984184315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLearningItemView::set_name(System.String)
extern "C"  void ChapterLearningItemView_set_name_m3592531450 (ChapterLearningItemView_t1984184315 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ChapterLearningItemView::get_percent()
extern "C"  float ChapterLearningItemView_get_percent_m4142565488 (ChapterLearningItemView_t1984184315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLearningItemView::set_percent(System.Single)
extern "C"  void ChapterLearningItemView_set_percent_m4277749133 (ChapterLearningItemView_t1984184315 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color ChapterLearningItemView::get_mainColor()
extern "C"  Color_t2020392075  ChapterLearningItemView_get_mainColor_m375022080 (ChapterLearningItemView_t1984184315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLearningItemView::set_mainColor(UnityEngine.Color)
extern "C"  void ChapterLearningItemView_set_mainColor_m1890665223 (ChapterLearningItemView_t1984184315 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLearningItemView::UpdateMainColor()
extern "C"  void ChapterLearningItemView_UpdateMainColor_m3637684417 (ChapterLearningItemView_t1984184315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
