﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VocabularyQuestionView
struct VocabularyQuestionView_t1631345783;
// VocabularyQuestionData
struct VocabularyQuestionData_t2021074976;

#include "AssemblyU2DCSharp_BaseOnplayQuestion1717064182.h"
#include "AssemblyU2DCSharp_VocabularyOnplayQuestion_Selecte3971704390.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VocabularyOnplayQuestion
struct  VocabularyOnplayQuestion_t3519225043  : public BaseOnplayQuestion_t1717064182
{
public:
	// System.String VocabularyOnplayQuestion::soundPath
	String_t* ___soundPath_2;
	// VocabularyOnplayQuestion/SelectedState VocabularyOnplayQuestion::_currentSelected
	int32_t ____currentSelected_3;
	// VocabularyQuestionView VocabularyOnplayQuestion::_view
	VocabularyQuestionView_t1631345783 * ____view_4;
	// VocabularyQuestionData VocabularyOnplayQuestion::questionData
	VocabularyQuestionData_t2021074976 * ___questionData_5;

public:
	inline static int32_t get_offset_of_soundPath_2() { return static_cast<int32_t>(offsetof(VocabularyOnplayQuestion_t3519225043, ___soundPath_2)); }
	inline String_t* get_soundPath_2() const { return ___soundPath_2; }
	inline String_t** get_address_of_soundPath_2() { return &___soundPath_2; }
	inline void set_soundPath_2(String_t* value)
	{
		___soundPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_2, value);
	}

	inline static int32_t get_offset_of__currentSelected_3() { return static_cast<int32_t>(offsetof(VocabularyOnplayQuestion_t3519225043, ____currentSelected_3)); }
	inline int32_t get__currentSelected_3() const { return ____currentSelected_3; }
	inline int32_t* get_address_of__currentSelected_3() { return &____currentSelected_3; }
	inline void set__currentSelected_3(int32_t value)
	{
		____currentSelected_3 = value;
	}

	inline static int32_t get_offset_of__view_4() { return static_cast<int32_t>(offsetof(VocabularyOnplayQuestion_t3519225043, ____view_4)); }
	inline VocabularyQuestionView_t1631345783 * get__view_4() const { return ____view_4; }
	inline VocabularyQuestionView_t1631345783 ** get_address_of__view_4() { return &____view_4; }
	inline void set__view_4(VocabularyQuestionView_t1631345783 * value)
	{
		____view_4 = value;
		Il2CppCodeGenWriteBarrier(&____view_4, value);
	}

	inline static int32_t get_offset_of_questionData_5() { return static_cast<int32_t>(offsetof(VocabularyOnplayQuestion_t3519225043, ___questionData_5)); }
	inline VocabularyQuestionData_t2021074976 * get_questionData_5() const { return ___questionData_5; }
	inline VocabularyQuestionData_t2021074976 ** get_address_of_questionData_5() { return &___questionData_5; }
	inline void set_questionData_5(VocabularyQuestionData_t2021074976 * value)
	{
		___questionData_5 = value;
		Il2CppCodeGenWriteBarrier(&___questionData_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
