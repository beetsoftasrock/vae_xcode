﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpPopupIniter
struct  HelpPopupIniter_t3764049838  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject HelpPopupIniter::helpPopupPrefab
	GameObject_t1756533147 * ___helpPopupPrefab_5;
	// System.String HelpPopupIniter::folderName
	String_t* ___folderName_6;
	// System.String HelpPopupIniter::prefix
	String_t* ___prefix_7;

public:
	inline static int32_t get_offset_of_helpPopupPrefab_5() { return static_cast<int32_t>(offsetof(HelpPopupIniter_t3764049838, ___helpPopupPrefab_5)); }
	inline GameObject_t1756533147 * get_helpPopupPrefab_5() const { return ___helpPopupPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_helpPopupPrefab_5() { return &___helpPopupPrefab_5; }
	inline void set_helpPopupPrefab_5(GameObject_t1756533147 * value)
	{
		___helpPopupPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___helpPopupPrefab_5, value);
	}

	inline static int32_t get_offset_of_folderName_6() { return static_cast<int32_t>(offsetof(HelpPopupIniter_t3764049838, ___folderName_6)); }
	inline String_t* get_folderName_6() const { return ___folderName_6; }
	inline String_t** get_address_of_folderName_6() { return &___folderName_6; }
	inline void set_folderName_6(String_t* value)
	{
		___folderName_6 = value;
		Il2CppCodeGenWriteBarrier(&___folderName_6, value);
	}

	inline static int32_t get_offset_of_prefix_7() { return static_cast<int32_t>(offsetof(HelpPopupIniter_t3764049838, ___prefix_7)); }
	inline String_t* get_prefix_7() const { return ___prefix_7; }
	inline String_t** get_address_of_prefix_7() { return &___prefix_7; }
	inline void set_prefix_7(String_t* value)
	{
		___prefix_7 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
