﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReporterMessageReceiver
struct ReporterMessageReceiver_t1067427633;
// Reporter/Log
struct Log_t3604182180;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reporter_Log3604182180.h"

// System.Void ReporterMessageReceiver::.ctor()
extern "C"  void ReporterMessageReceiver__ctor_m2815523308 (ReporterMessageReceiver_t1067427633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::Start()
extern "C"  void ReporterMessageReceiver_Start_m936930572 (ReporterMessageReceiver_t1067427633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::OnPreStart()
extern "C"  void ReporterMessageReceiver_OnPreStart_m1129337412 (ReporterMessageReceiver_t1067427633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::OnHideReporter()
extern "C"  void ReporterMessageReceiver_OnHideReporter_m389170554 (ReporterMessageReceiver_t1067427633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::OnShowReporter()
extern "C"  void ReporterMessageReceiver_OnShowReporter_m1734353559 (ReporterMessageReceiver_t1067427633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterMessageReceiver::OnLog(Reporter/Log)
extern "C"  void ReporterMessageReceiver_OnLog_m3553099171 (ReporterMessageReceiver_t1067427633 * __this, Log_t3604182180 * ___log0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
