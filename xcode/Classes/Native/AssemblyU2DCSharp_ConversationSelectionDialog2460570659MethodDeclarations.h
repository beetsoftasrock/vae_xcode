﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionDialog
struct ConversationSelectionDialog_t2460570659;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationSelectionDialog::.ctor()
extern "C"  void ConversationSelectionDialog__ctor_m3686863482 (ConversationSelectionDialog_t2460570659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
