﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FadeObject/<IEFade>c__Iterator0
struct U3CIEFadeU3Ec__Iterator0_t2603370079;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FadeObject/<IEFade>c__Iterator0::.ctor()
extern "C"  void U3CIEFadeU3Ec__Iterator0__ctor_m3508793234 (U3CIEFadeU3Ec__Iterator0_t2603370079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FadeObject/<IEFade>c__Iterator0::MoveNext()
extern "C"  bool U3CIEFadeU3Ec__Iterator0_MoveNext_m1399295430 (U3CIEFadeU3Ec__Iterator0_t2603370079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FadeObject/<IEFade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIEFadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2095968592 (U3CIEFadeU3Ec__Iterator0_t2603370079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FadeObject/<IEFade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIEFadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2605007096 (U3CIEFadeU3Ec__Iterator0_t2603370079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeObject/<IEFade>c__Iterator0::Dispose()
extern "C"  void U3CIEFadeU3Ec__Iterator0_Dispose_m1888235825 (U3CIEFadeU3Ec__Iterator0_t2603370079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeObject/<IEFade>c__Iterator0::Reset()
extern "C"  void U3CIEFadeU3Ec__Iterator0_Reset_m4059620735 (U3CIEFadeU3Ec__Iterator0_t2603370079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
