﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterScript/<WaitTalking>c__Iterator1
struct U3CWaitTalkingU3Ec__Iterator1_t2502173674;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterScript/<WaitTalking>c__Iterator1::.ctor()
extern "C"  void U3CWaitTalkingU3Ec__Iterator1__ctor_m1483861985 (U3CWaitTalkingU3Ec__Iterator1_t2502173674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterScript/<WaitTalking>c__Iterator1::MoveNext()
extern "C"  bool U3CWaitTalkingU3Ec__Iterator1_MoveNext_m2886830607 (U3CWaitTalkingU3Ec__Iterator1_t2502173674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterScript/<WaitTalking>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitTalkingU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2562231227 (U3CWaitTalkingU3Ec__Iterator1_t2502173674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterScript/<WaitTalking>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitTalkingU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2057106915 (U3CWaitTalkingU3Ec__Iterator1_t2502173674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript/<WaitTalking>c__Iterator1::Dispose()
extern "C"  void U3CWaitTalkingU3Ec__Iterator1_Dispose_m1098195428 (U3CWaitTalkingU3Ec__Iterator1_t2502173674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript/<WaitTalking>c__Iterator1::Reset()
extern "C"  void U3CWaitTalkingU3Ec__Iterator1_Reset_m2071822998 (U3CWaitTalkingU3Ec__Iterator1_t2502173674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
