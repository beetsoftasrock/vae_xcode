﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationLogicController
struct ConversationLogicController_t3911886281;
// ManageATutorial
struct ManageATutorial_t1512139496;
// ResetData
struct ResetData_t4114871043;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackShowHelpAtFistTimeScript
struct  TrackShowHelpAtFistTimeScript_t2844513732  : public MonoBehaviour_t1158329972
{
public:
	// ConversationLogicController TrackShowHelpAtFistTimeScript::clc
	ConversationLogicController_t3911886281 * ___clc_2;
	// ManageATutorial TrackShowHelpAtFistTimeScript::manageATutorial
	ManageATutorial_t1512139496 * ___manageATutorial_3;
	// ResetData TrackShowHelpAtFistTimeScript::resetData
	ResetData_t4114871043 * ___resetData_4;

public:
	inline static int32_t get_offset_of_clc_2() { return static_cast<int32_t>(offsetof(TrackShowHelpAtFistTimeScript_t2844513732, ___clc_2)); }
	inline ConversationLogicController_t3911886281 * get_clc_2() const { return ___clc_2; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_2() { return &___clc_2; }
	inline void set_clc_2(ConversationLogicController_t3911886281 * value)
	{
		___clc_2 = value;
		Il2CppCodeGenWriteBarrier(&___clc_2, value);
	}

	inline static int32_t get_offset_of_manageATutorial_3() { return static_cast<int32_t>(offsetof(TrackShowHelpAtFistTimeScript_t2844513732, ___manageATutorial_3)); }
	inline ManageATutorial_t1512139496 * get_manageATutorial_3() const { return ___manageATutorial_3; }
	inline ManageATutorial_t1512139496 ** get_address_of_manageATutorial_3() { return &___manageATutorial_3; }
	inline void set_manageATutorial_3(ManageATutorial_t1512139496 * value)
	{
		___manageATutorial_3 = value;
		Il2CppCodeGenWriteBarrier(&___manageATutorial_3, value);
	}

	inline static int32_t get_offset_of_resetData_4() { return static_cast<int32_t>(offsetof(TrackShowHelpAtFistTimeScript_t2844513732, ___resetData_4)); }
	inline ResetData_t4114871043 * get_resetData_4() const { return ___resetData_4; }
	inline ResetData_t4114871043 ** get_address_of_resetData_4() { return &___resetData_4; }
	inline void set_resetData_4(ResetData_t4114871043 * value)
	{
		___resetData_4 = value;
		Il2CppCodeGenWriteBarrier(&___resetData_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
