﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TalkCommand
struct TalkCommand_t2292920015;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationLogicController
struct ConversationLogicController_t3911886281;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "AssemblyU2DCSharp_ConversationLogicController3911886281.h"

// System.Void TalkCommand::.ctor(ConversationTalkData)
extern "C"  void TalkCommand__ctor_m2112705593 (TalkCommand_t2292920015 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TalkCommand::AutoExecute(ConversationLogicController)
extern "C"  Il2CppObject * TalkCommand_AutoExecute_m472323747 (TalkCommand_t2292920015 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TalkCommand::Execute(ConversationLogicController)
extern "C"  Il2CppObject * TalkCommand_Execute_m252186136 (TalkCommand_t2292920015 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TalkCommand::GetRole()
extern "C"  String_t* TalkCommand_GetRole_m3136129029 (TalkCommand_t2292920015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkCommand::Revert(ConversationLogicController)
extern "C"  void TalkCommand_Revert_m1861670119 (TalkCommand_t2292920015 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
