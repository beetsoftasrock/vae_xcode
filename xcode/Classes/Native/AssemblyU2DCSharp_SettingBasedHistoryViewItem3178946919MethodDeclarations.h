﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedHistoryViewItem
struct SettingBasedHistoryViewItem_t3178946919;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedHistoryViewItem::.ctor()
extern "C"  void SettingBasedHistoryViewItem__ctor_m110539554 (SettingBasedHistoryViewItem_t3178946919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedHistoryViewItem::Start()
extern "C"  void SettingBasedHistoryViewItem_Start_m648742510 (SettingBasedHistoryViewItem_t3178946919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
