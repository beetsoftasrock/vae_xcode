﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// ManagerCharacters
struct ManagerCharacters_t162625631;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.String
struct String_t;
// CommandLoader
struct CommandLoader_t2460237222;
// SoundRecorder
struct SoundRecorder_t3431875919;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<ConversationTalkData>
struct List_1_t939419437;
// UnityEngine.UI.Text
struct Text_t356221433;
// ConversationLogicController/ConversationStateChangeEvent
struct ConversationStateChangeEvent_t2954138412;
// UnityEngine.UI.Button
struct Button_t2872111280;
// InvokerModule
struct InvokerModule_t4256524346;
// ThinkingEffectModule
struct ThinkingEffectModule_t4011786459;
// PlayerSelectionModule
struct PlayerSelectionModule_t1458090443;
// PlayerTalkModule
struct PlayerTalkModule_t3356839145;
// OtherTalkModule
struct OtherTalkModule_t2158105276;
// HistoryLogModule
struct HistoryLogModule_t1965887714;
// VoiceRecordModule
struct VoiceRecordModule_t679113955;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ConversationLogicController_Conv3360496668.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationLogicController
struct  ConversationLogicController_t3911886281  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform ConversationLogicController::prefabCharacter
	Transform_t3275118058 * ___prefabCharacter_2;
	// ManagerCharacters ConversationLogicController::characterManager
	ManagerCharacters_t162625631 * ___characterManager_3;
	// System.Boolean ConversationLogicController::isAutoPlay
	bool ___isAutoPlay_4;
	// UnityEngine.Events.UnityEvent ConversationLogicController::onConversationCompleted
	UnityEvent_t408735097 * ___onConversationCompleted_5;
	// UnityEngine.Events.UnityEvent ConversationLogicController::onConversationEnded
	UnityEvent_t408735097 * ___onConversationEnded_6;
	// System.String ConversationLogicController::soundSavePath
	String_t* ___soundSavePath_7;
	// System.String ConversationLogicController::soundResourcePath
	String_t* ___soundResourcePath_8;
	// System.String ConversationLogicController::textureResourcePath
	String_t* ___textureResourcePath_9;
	// System.String ConversationLogicController::playerRole
	String_t* ___playerRole_10;
	// System.String ConversationLogicController::otherRole
	String_t* ___otherRole_11;
	// CommandLoader ConversationLogicController::commandLoader
	CommandLoader_t2460237222 * ___commandLoader_12;
	// SoundRecorder ConversationLogicController::soundRecorder
	SoundRecorder_t3431875919 * ___soundRecorder_13;
	// System.String[] ConversationLogicController::roles
	StringU5BU5D_t1642385972* ___roles_14;
	// System.Single ConversationLogicController::<startPlayTime>k__BackingField
	float ___U3CstartPlayTimeU3Ek__BackingField_15;
	// System.Collections.Generic.List`1<ConversationTalkData> ConversationLogicController::<playerTalkDatas>k__BackingField
	List_1_t939419437 * ___U3CplayerTalkDatasU3Ek__BackingField_16;
	// System.Boolean ConversationLogicController::isConversationEnd
	bool ___isConversationEnd_17;
	// System.Boolean ConversationLogicController::_isBlock
	bool ____isBlock_18;
	// System.Boolean ConversationLogicController::playingPlayerTalk
	bool ___playingPlayerTalk_19;
	// UnityEngine.UI.Text ConversationLogicController::nameLessionText
	Text_t356221433 * ___nameLessionText_20;
	// ConversationLogicController/ConversationStateChangeEvent ConversationLogicController::onConversationStateChange
	ConversationStateChangeEvent_t2954138412 * ___onConversationStateChange_21;
	// ConversationLogicController/ConversationState ConversationLogicController::_state
	int32_t ____state_22;
	// UnityEngine.UI.Button ConversationLogicController::languageButton
	Button_t2872111280 * ___languageButton_23;
	// UnityEngine.UI.Button ConversationLogicController::helpButton
	Button_t2872111280 * ___helpButton_24;
	// InvokerModule ConversationLogicController::invokerModule
	InvokerModule_t4256524346 * ___invokerModule_25;
	// ThinkingEffectModule ConversationLogicController::thinkingEffectModule
	ThinkingEffectModule_t4011786459 * ___thinkingEffectModule_26;
	// PlayerSelectionModule ConversationLogicController::playerSelectionModule
	PlayerSelectionModule_t1458090443 * ___playerSelectionModule_27;
	// PlayerTalkModule ConversationLogicController::playerTalkModule
	PlayerTalkModule_t3356839145 * ___playerTalkModule_28;
	// OtherTalkModule ConversationLogicController::otherTalkModule
	OtherTalkModule_t2158105276 * ___otherTalkModule_29;
	// HistoryLogModule ConversationLogicController::historyLogModule
	HistoryLogModule_t1965887714 * ___historyLogModule_30;
	// VoiceRecordModule ConversationLogicController::voiceRecordModule
	VoiceRecordModule_t679113955 * ___voiceRecordModule_31;

public:
	inline static int32_t get_offset_of_prefabCharacter_2() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___prefabCharacter_2)); }
	inline Transform_t3275118058 * get_prefabCharacter_2() const { return ___prefabCharacter_2; }
	inline Transform_t3275118058 ** get_address_of_prefabCharacter_2() { return &___prefabCharacter_2; }
	inline void set_prefabCharacter_2(Transform_t3275118058 * value)
	{
		___prefabCharacter_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefabCharacter_2, value);
	}

	inline static int32_t get_offset_of_characterManager_3() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___characterManager_3)); }
	inline ManagerCharacters_t162625631 * get_characterManager_3() const { return ___characterManager_3; }
	inline ManagerCharacters_t162625631 ** get_address_of_characterManager_3() { return &___characterManager_3; }
	inline void set_characterManager_3(ManagerCharacters_t162625631 * value)
	{
		___characterManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___characterManager_3, value);
	}

	inline static int32_t get_offset_of_isAutoPlay_4() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___isAutoPlay_4)); }
	inline bool get_isAutoPlay_4() const { return ___isAutoPlay_4; }
	inline bool* get_address_of_isAutoPlay_4() { return &___isAutoPlay_4; }
	inline void set_isAutoPlay_4(bool value)
	{
		___isAutoPlay_4 = value;
	}

	inline static int32_t get_offset_of_onConversationCompleted_5() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___onConversationCompleted_5)); }
	inline UnityEvent_t408735097 * get_onConversationCompleted_5() const { return ___onConversationCompleted_5; }
	inline UnityEvent_t408735097 ** get_address_of_onConversationCompleted_5() { return &___onConversationCompleted_5; }
	inline void set_onConversationCompleted_5(UnityEvent_t408735097 * value)
	{
		___onConversationCompleted_5 = value;
		Il2CppCodeGenWriteBarrier(&___onConversationCompleted_5, value);
	}

	inline static int32_t get_offset_of_onConversationEnded_6() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___onConversationEnded_6)); }
	inline UnityEvent_t408735097 * get_onConversationEnded_6() const { return ___onConversationEnded_6; }
	inline UnityEvent_t408735097 ** get_address_of_onConversationEnded_6() { return &___onConversationEnded_6; }
	inline void set_onConversationEnded_6(UnityEvent_t408735097 * value)
	{
		___onConversationEnded_6 = value;
		Il2CppCodeGenWriteBarrier(&___onConversationEnded_6, value);
	}

	inline static int32_t get_offset_of_soundSavePath_7() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___soundSavePath_7)); }
	inline String_t* get_soundSavePath_7() const { return ___soundSavePath_7; }
	inline String_t** get_address_of_soundSavePath_7() { return &___soundSavePath_7; }
	inline void set_soundSavePath_7(String_t* value)
	{
		___soundSavePath_7 = value;
		Il2CppCodeGenWriteBarrier(&___soundSavePath_7, value);
	}

	inline static int32_t get_offset_of_soundResourcePath_8() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___soundResourcePath_8)); }
	inline String_t* get_soundResourcePath_8() const { return ___soundResourcePath_8; }
	inline String_t** get_address_of_soundResourcePath_8() { return &___soundResourcePath_8; }
	inline void set_soundResourcePath_8(String_t* value)
	{
		___soundResourcePath_8 = value;
		Il2CppCodeGenWriteBarrier(&___soundResourcePath_8, value);
	}

	inline static int32_t get_offset_of_textureResourcePath_9() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___textureResourcePath_9)); }
	inline String_t* get_textureResourcePath_9() const { return ___textureResourcePath_9; }
	inline String_t** get_address_of_textureResourcePath_9() { return &___textureResourcePath_9; }
	inline void set_textureResourcePath_9(String_t* value)
	{
		___textureResourcePath_9 = value;
		Il2CppCodeGenWriteBarrier(&___textureResourcePath_9, value);
	}

	inline static int32_t get_offset_of_playerRole_10() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___playerRole_10)); }
	inline String_t* get_playerRole_10() const { return ___playerRole_10; }
	inline String_t** get_address_of_playerRole_10() { return &___playerRole_10; }
	inline void set_playerRole_10(String_t* value)
	{
		___playerRole_10 = value;
		Il2CppCodeGenWriteBarrier(&___playerRole_10, value);
	}

	inline static int32_t get_offset_of_otherRole_11() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___otherRole_11)); }
	inline String_t* get_otherRole_11() const { return ___otherRole_11; }
	inline String_t** get_address_of_otherRole_11() { return &___otherRole_11; }
	inline void set_otherRole_11(String_t* value)
	{
		___otherRole_11 = value;
		Il2CppCodeGenWriteBarrier(&___otherRole_11, value);
	}

	inline static int32_t get_offset_of_commandLoader_12() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___commandLoader_12)); }
	inline CommandLoader_t2460237222 * get_commandLoader_12() const { return ___commandLoader_12; }
	inline CommandLoader_t2460237222 ** get_address_of_commandLoader_12() { return &___commandLoader_12; }
	inline void set_commandLoader_12(CommandLoader_t2460237222 * value)
	{
		___commandLoader_12 = value;
		Il2CppCodeGenWriteBarrier(&___commandLoader_12, value);
	}

	inline static int32_t get_offset_of_soundRecorder_13() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___soundRecorder_13)); }
	inline SoundRecorder_t3431875919 * get_soundRecorder_13() const { return ___soundRecorder_13; }
	inline SoundRecorder_t3431875919 ** get_address_of_soundRecorder_13() { return &___soundRecorder_13; }
	inline void set_soundRecorder_13(SoundRecorder_t3431875919 * value)
	{
		___soundRecorder_13 = value;
		Il2CppCodeGenWriteBarrier(&___soundRecorder_13, value);
	}

	inline static int32_t get_offset_of_roles_14() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___roles_14)); }
	inline StringU5BU5D_t1642385972* get_roles_14() const { return ___roles_14; }
	inline StringU5BU5D_t1642385972** get_address_of_roles_14() { return &___roles_14; }
	inline void set_roles_14(StringU5BU5D_t1642385972* value)
	{
		___roles_14 = value;
		Il2CppCodeGenWriteBarrier(&___roles_14, value);
	}

	inline static int32_t get_offset_of_U3CstartPlayTimeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___U3CstartPlayTimeU3Ek__BackingField_15)); }
	inline float get_U3CstartPlayTimeU3Ek__BackingField_15() const { return ___U3CstartPlayTimeU3Ek__BackingField_15; }
	inline float* get_address_of_U3CstartPlayTimeU3Ek__BackingField_15() { return &___U3CstartPlayTimeU3Ek__BackingField_15; }
	inline void set_U3CstartPlayTimeU3Ek__BackingField_15(float value)
	{
		___U3CstartPlayTimeU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CplayerTalkDatasU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___U3CplayerTalkDatasU3Ek__BackingField_16)); }
	inline List_1_t939419437 * get_U3CplayerTalkDatasU3Ek__BackingField_16() const { return ___U3CplayerTalkDatasU3Ek__BackingField_16; }
	inline List_1_t939419437 ** get_address_of_U3CplayerTalkDatasU3Ek__BackingField_16() { return &___U3CplayerTalkDatasU3Ek__BackingField_16; }
	inline void set_U3CplayerTalkDatasU3Ek__BackingField_16(List_1_t939419437 * value)
	{
		___U3CplayerTalkDatasU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CplayerTalkDatasU3Ek__BackingField_16, value);
	}

	inline static int32_t get_offset_of_isConversationEnd_17() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___isConversationEnd_17)); }
	inline bool get_isConversationEnd_17() const { return ___isConversationEnd_17; }
	inline bool* get_address_of_isConversationEnd_17() { return &___isConversationEnd_17; }
	inline void set_isConversationEnd_17(bool value)
	{
		___isConversationEnd_17 = value;
	}

	inline static int32_t get_offset_of__isBlock_18() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ____isBlock_18)); }
	inline bool get__isBlock_18() const { return ____isBlock_18; }
	inline bool* get_address_of__isBlock_18() { return &____isBlock_18; }
	inline void set__isBlock_18(bool value)
	{
		____isBlock_18 = value;
	}

	inline static int32_t get_offset_of_playingPlayerTalk_19() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___playingPlayerTalk_19)); }
	inline bool get_playingPlayerTalk_19() const { return ___playingPlayerTalk_19; }
	inline bool* get_address_of_playingPlayerTalk_19() { return &___playingPlayerTalk_19; }
	inline void set_playingPlayerTalk_19(bool value)
	{
		___playingPlayerTalk_19 = value;
	}

	inline static int32_t get_offset_of_nameLessionText_20() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___nameLessionText_20)); }
	inline Text_t356221433 * get_nameLessionText_20() const { return ___nameLessionText_20; }
	inline Text_t356221433 ** get_address_of_nameLessionText_20() { return &___nameLessionText_20; }
	inline void set_nameLessionText_20(Text_t356221433 * value)
	{
		___nameLessionText_20 = value;
		Il2CppCodeGenWriteBarrier(&___nameLessionText_20, value);
	}

	inline static int32_t get_offset_of_onConversationStateChange_21() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___onConversationStateChange_21)); }
	inline ConversationStateChangeEvent_t2954138412 * get_onConversationStateChange_21() const { return ___onConversationStateChange_21; }
	inline ConversationStateChangeEvent_t2954138412 ** get_address_of_onConversationStateChange_21() { return &___onConversationStateChange_21; }
	inline void set_onConversationStateChange_21(ConversationStateChangeEvent_t2954138412 * value)
	{
		___onConversationStateChange_21 = value;
		Il2CppCodeGenWriteBarrier(&___onConversationStateChange_21, value);
	}

	inline static int32_t get_offset_of__state_22() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ____state_22)); }
	inline int32_t get__state_22() const { return ____state_22; }
	inline int32_t* get_address_of__state_22() { return &____state_22; }
	inline void set__state_22(int32_t value)
	{
		____state_22 = value;
	}

	inline static int32_t get_offset_of_languageButton_23() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___languageButton_23)); }
	inline Button_t2872111280 * get_languageButton_23() const { return ___languageButton_23; }
	inline Button_t2872111280 ** get_address_of_languageButton_23() { return &___languageButton_23; }
	inline void set_languageButton_23(Button_t2872111280 * value)
	{
		___languageButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___languageButton_23, value);
	}

	inline static int32_t get_offset_of_helpButton_24() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___helpButton_24)); }
	inline Button_t2872111280 * get_helpButton_24() const { return ___helpButton_24; }
	inline Button_t2872111280 ** get_address_of_helpButton_24() { return &___helpButton_24; }
	inline void set_helpButton_24(Button_t2872111280 * value)
	{
		___helpButton_24 = value;
		Il2CppCodeGenWriteBarrier(&___helpButton_24, value);
	}

	inline static int32_t get_offset_of_invokerModule_25() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___invokerModule_25)); }
	inline InvokerModule_t4256524346 * get_invokerModule_25() const { return ___invokerModule_25; }
	inline InvokerModule_t4256524346 ** get_address_of_invokerModule_25() { return &___invokerModule_25; }
	inline void set_invokerModule_25(InvokerModule_t4256524346 * value)
	{
		___invokerModule_25 = value;
		Il2CppCodeGenWriteBarrier(&___invokerModule_25, value);
	}

	inline static int32_t get_offset_of_thinkingEffectModule_26() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___thinkingEffectModule_26)); }
	inline ThinkingEffectModule_t4011786459 * get_thinkingEffectModule_26() const { return ___thinkingEffectModule_26; }
	inline ThinkingEffectModule_t4011786459 ** get_address_of_thinkingEffectModule_26() { return &___thinkingEffectModule_26; }
	inline void set_thinkingEffectModule_26(ThinkingEffectModule_t4011786459 * value)
	{
		___thinkingEffectModule_26 = value;
		Il2CppCodeGenWriteBarrier(&___thinkingEffectModule_26, value);
	}

	inline static int32_t get_offset_of_playerSelectionModule_27() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___playerSelectionModule_27)); }
	inline PlayerSelectionModule_t1458090443 * get_playerSelectionModule_27() const { return ___playerSelectionModule_27; }
	inline PlayerSelectionModule_t1458090443 ** get_address_of_playerSelectionModule_27() { return &___playerSelectionModule_27; }
	inline void set_playerSelectionModule_27(PlayerSelectionModule_t1458090443 * value)
	{
		___playerSelectionModule_27 = value;
		Il2CppCodeGenWriteBarrier(&___playerSelectionModule_27, value);
	}

	inline static int32_t get_offset_of_playerTalkModule_28() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___playerTalkModule_28)); }
	inline PlayerTalkModule_t3356839145 * get_playerTalkModule_28() const { return ___playerTalkModule_28; }
	inline PlayerTalkModule_t3356839145 ** get_address_of_playerTalkModule_28() { return &___playerTalkModule_28; }
	inline void set_playerTalkModule_28(PlayerTalkModule_t3356839145 * value)
	{
		___playerTalkModule_28 = value;
		Il2CppCodeGenWriteBarrier(&___playerTalkModule_28, value);
	}

	inline static int32_t get_offset_of_otherTalkModule_29() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___otherTalkModule_29)); }
	inline OtherTalkModule_t2158105276 * get_otherTalkModule_29() const { return ___otherTalkModule_29; }
	inline OtherTalkModule_t2158105276 ** get_address_of_otherTalkModule_29() { return &___otherTalkModule_29; }
	inline void set_otherTalkModule_29(OtherTalkModule_t2158105276 * value)
	{
		___otherTalkModule_29 = value;
		Il2CppCodeGenWriteBarrier(&___otherTalkModule_29, value);
	}

	inline static int32_t get_offset_of_historyLogModule_30() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___historyLogModule_30)); }
	inline HistoryLogModule_t1965887714 * get_historyLogModule_30() const { return ___historyLogModule_30; }
	inline HistoryLogModule_t1965887714 ** get_address_of_historyLogModule_30() { return &___historyLogModule_30; }
	inline void set_historyLogModule_30(HistoryLogModule_t1965887714 * value)
	{
		___historyLogModule_30 = value;
		Il2CppCodeGenWriteBarrier(&___historyLogModule_30, value);
	}

	inline static int32_t get_offset_of_voiceRecordModule_31() { return static_cast<int32_t>(offsetof(ConversationLogicController_t3911886281, ___voiceRecordModule_31)); }
	inline VoiceRecordModule_t679113955 * get_voiceRecordModule_31() const { return ___voiceRecordModule_31; }
	inline VoiceRecordModule_t679113955 ** get_address_of_voiceRecordModule_31() { return &___voiceRecordModule_31; }
	inline void set_voiceRecordModule_31(VoiceRecordModule_t679113955 * value)
	{
		___voiceRecordModule_31 = value;
		Il2CppCodeGenWriteBarrier(&___voiceRecordModule_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
