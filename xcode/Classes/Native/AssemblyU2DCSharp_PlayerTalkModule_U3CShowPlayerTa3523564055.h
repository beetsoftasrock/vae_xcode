﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationTalkData
struct ConversationTalkData_t1570298305;
// PlayerTalkModule
struct PlayerTalkModule_t3356839145;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0
struct  U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055  : public Il2CppObject
{
public:
	// ConversationTalkData PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::data
	ConversationTalkData_t1570298305 * ___data_0;
	// PlayerTalkModule PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::$this
	PlayerTalkModule_t3356839145 * ___U24this_1;
	// System.Object PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 PlayerTalkModule/<ShowPlayerTalkDialog>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055, ___data_0)); }
	inline ConversationTalkData_t1570298305 * get_data_0() const { return ___data_0; }
	inline ConversationTalkData_t1570298305 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ConversationTalkData_t1570298305 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055, ___U24this_1)); }
	inline PlayerTalkModule_t3356839145 * get_U24this_1() const { return ___U24this_1; }
	inline PlayerTalkModule_t3356839145 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PlayerTalkModule_t3356839145 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowPlayerTalkDialogU3Ec__Iterator0_t3523564055, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
