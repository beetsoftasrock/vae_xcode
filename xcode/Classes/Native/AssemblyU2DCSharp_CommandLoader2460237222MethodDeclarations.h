﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CommandLoader
struct CommandLoader_t2460237222;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CommandLoader::.ctor()
extern "C"  void CommandLoader__ctor_m1395900505 (CommandLoader_t2460237222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommandLoader::GoTo(System.String)
extern "C"  void CommandLoader_GoTo_m1022694104 (CommandLoader_t2460237222 * __this, String_t* ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
