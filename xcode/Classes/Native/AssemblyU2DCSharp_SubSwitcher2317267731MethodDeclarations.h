﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubSwitcher
struct SubSwitcher_t2317267731;

#include "codegen/il2cpp-codegen.h"

// System.Void SubSwitcher::.ctor()
extern "C"  void SubSwitcher__ctor_m1222412780 (SubSwitcher_t2317267731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubSwitcher::Awake()
extern "C"  void SubSwitcher_Awake_m3583923827 (SubSwitcher_t2317267731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubSwitcher::Switch()
extern "C"  void SubSwitcher_Switch_m1423726166 (SubSwitcher_t2317267731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
