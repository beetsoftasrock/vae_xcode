﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManageTutorialsInPractice
struct ManageTutorialsInPractice_t1214376460;

#include "codegen/il2cpp-codegen.h"

// System.Void ManageTutorialsInPractice::.ctor()
extern "C"  void ManageTutorialsInPractice__ctor_m2835592809 (ManageTutorialsInPractice_t1214376460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageTutorialsInPractice::Awake()
extern "C"  void ManageTutorialsInPractice_Awake_m314302936 (ManageTutorialsInPractice_t1214376460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageTutorialsInPractice::OpenHelp()
extern "C"  void ManageTutorialsInPractice_OpenHelp_m1927409056 (ManageTutorialsInPractice_t1214376460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
