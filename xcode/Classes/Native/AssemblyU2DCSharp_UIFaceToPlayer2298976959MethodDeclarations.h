﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIFaceToPlayer
struct UIFaceToPlayer_t2298976959;

#include "codegen/il2cpp-codegen.h"

// System.Void UIFaceToPlayer::.ctor()
extern "C"  void UIFaceToPlayer__ctor_m1746706050 (UIFaceToPlayer_t2298976959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFaceToPlayer::Awake()
extern "C"  void UIFaceToPlayer_Awake_m2378622959 (UIFaceToPlayer_t2298976959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFaceToPlayer::FaceToPlayer()
extern "C"  void UIFaceToPlayer_FaceToPlayer_m177076931 (UIFaceToPlayer_t2298976959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
