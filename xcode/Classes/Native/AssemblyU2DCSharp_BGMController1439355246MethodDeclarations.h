﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BGMController
struct BGMController_t1439355246;

#include "codegen/il2cpp-codegen.h"

// System.Void BGMController::.ctor()
extern "C"  void BGMController__ctor_m3993434535 (BGMController_t1439355246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BGMController BGMController::get_instance()
extern "C"  BGMController_t1439355246 * BGMController_get_instance_m4276966546 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGMController::Awake()
extern "C"  void BGMController_Awake_m4061579358 (BGMController_t1439355246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BGMController::get_playBGM()
extern "C"  bool BGMController_get_playBGM_m1964766912 (BGMController_t1439355246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGMController::set_playBGM(System.Boolean)
extern "C"  void BGMController_set_playBGM_m485483469 (BGMController_t1439355246 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGMController::PlayBGM()
extern "C"  void BGMController_PlayBGM_m3899721919 (BGMController_t1439355246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGMController::StopBGM()
extern "C"  void BGMController_StopBGM_m1938255759 (BGMController_t1439355246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
