﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t2667590766;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t1298400415;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductCatalogItem>
struct IEnumerator_1_t2748203118;
// Beetsoft.VAE.Purchaser
struct Purchaser_t1674559779;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.Purchaser/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t4187929873  : public Il2CppObject
{
public:
	// System.String Beetsoft.VAE.Purchaser/<Start>c__Iterator0::<appIdentifier>__0
	String_t* ___U3CappIdentifierU3E__0_0;
	// UnityEngine.Purchasing.ProductCatalog Beetsoft.VAE.Purchaser/<Start>c__Iterator0::<productCatalog>__1
	ProductCatalog_t2667590766 * ___U3CproductCatalogU3E__1_1;
	// UnityEngine.Purchasing.ConfigurationBuilder Beetsoft.VAE.Purchaser/<Start>c__Iterator0::<builder>__2
	ConfigurationBuilder_t1298400415 * ___U3CbuilderU3E__2_2;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductCatalogItem> Beetsoft.VAE.Purchaser/<Start>c__Iterator0::$locvar0
	Il2CppObject* ___U24locvar0_3;
	// Beetsoft.VAE.Purchaser Beetsoft.VAE.Purchaser/<Start>c__Iterator0::$this
	Purchaser_t1674559779 * ___U24this_4;
	// System.Object Beetsoft.VAE.Purchaser/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean Beetsoft.VAE.Purchaser/<Start>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Beetsoft.VAE.Purchaser/<Start>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CappIdentifierU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4187929873, ___U3CappIdentifierU3E__0_0)); }
	inline String_t* get_U3CappIdentifierU3E__0_0() const { return ___U3CappIdentifierU3E__0_0; }
	inline String_t** get_address_of_U3CappIdentifierU3E__0_0() { return &___U3CappIdentifierU3E__0_0; }
	inline void set_U3CappIdentifierU3E__0_0(String_t* value)
	{
		___U3CappIdentifierU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CappIdentifierU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CproductCatalogU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4187929873, ___U3CproductCatalogU3E__1_1)); }
	inline ProductCatalog_t2667590766 * get_U3CproductCatalogU3E__1_1() const { return ___U3CproductCatalogU3E__1_1; }
	inline ProductCatalog_t2667590766 ** get_address_of_U3CproductCatalogU3E__1_1() { return &___U3CproductCatalogU3E__1_1; }
	inline void set_U3CproductCatalogU3E__1_1(ProductCatalog_t2667590766 * value)
	{
		___U3CproductCatalogU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CproductCatalogU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CbuilderU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4187929873, ___U3CbuilderU3E__2_2)); }
	inline ConfigurationBuilder_t1298400415 * get_U3CbuilderU3E__2_2() const { return ___U3CbuilderU3E__2_2; }
	inline ConfigurationBuilder_t1298400415 ** get_address_of_U3CbuilderU3E__2_2() { return &___U3CbuilderU3E__2_2; }
	inline void set_U3CbuilderU3E__2_2(ConfigurationBuilder_t1298400415 * value)
	{
		___U3CbuilderU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbuilderU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4187929873, ___U24locvar0_3)); }
	inline Il2CppObject* get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline Il2CppObject** get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(Il2CppObject* value)
	{
		___U24locvar0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4187929873, ___U24this_4)); }
	inline Purchaser_t1674559779 * get_U24this_4() const { return ___U24this_4; }
	inline Purchaser_t1674559779 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Purchaser_t1674559779 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4187929873, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4187929873, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4187929873, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
