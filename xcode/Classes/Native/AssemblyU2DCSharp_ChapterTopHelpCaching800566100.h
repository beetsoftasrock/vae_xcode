﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectController
struct SelectController_t1269418144;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SelectController_SelectedState323245347.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterTopHelpCaching
struct  ChapterTopHelpCaching_t800566100  : public MonoBehaviour_t1158329972
{
public:
	// SelectController ChapterTopHelpCaching::selectController
	SelectController_t1269418144 * ___selectController_2;
	// SelectController/SelectedState ChapterTopHelpCaching::_preSelectedState
	int32_t ____preSelectedState_3;

public:
	inline static int32_t get_offset_of_selectController_2() { return static_cast<int32_t>(offsetof(ChapterTopHelpCaching_t800566100, ___selectController_2)); }
	inline SelectController_t1269418144 * get_selectController_2() const { return ___selectController_2; }
	inline SelectController_t1269418144 ** get_address_of_selectController_2() { return &___selectController_2; }
	inline void set_selectController_2(SelectController_t1269418144 * value)
	{
		___selectController_2 = value;
		Il2CppCodeGenWriteBarrier(&___selectController_2, value);
	}

	inline static int32_t get_offset_of__preSelectedState_3() { return static_cast<int32_t>(offsetof(ChapterTopHelpCaching_t800566100, ____preSelectedState_3)); }
	inline int32_t get__preSelectedState_3() const { return ____preSelectedState_3; }
	inline int32_t* get_address_of__preSelectedState_3() { return &____preSelectedState_3; }
	inline void set__preSelectedState_3(int32_t value)
	{
		____preSelectedState_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
