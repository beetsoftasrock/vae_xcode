﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseData
struct BaseData_t450384115;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseData::.ctor()
extern "C"  void BaseData__ctor_m1759393944 (BaseData_t450384115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
