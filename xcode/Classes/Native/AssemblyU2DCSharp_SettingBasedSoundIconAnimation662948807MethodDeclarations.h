﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedSoundIconAnimation
struct SettingBasedSoundIconAnimation_t662948807;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedSoundIconAnimation::.ctor()
extern "C"  void SettingBasedSoundIconAnimation__ctor_m3908493302 (SettingBasedSoundIconAnimation_t662948807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedSoundIconAnimation::Start()
extern "C"  void SettingBasedSoundIconAnimation_Start_m1435728370 (SettingBasedSoundIconAnimation_t662948807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
