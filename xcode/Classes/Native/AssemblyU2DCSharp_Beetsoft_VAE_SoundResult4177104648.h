﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.SoundResult
struct  SoundResult_t4177104648  : public Il2CppObject
{
public:
	// System.Action Beetsoft.VAE.SoundResult::OnPlay
	Action_t3226471752 * ___OnPlay_0;
	// System.Action Beetsoft.VAE.SoundResult::OnEnd
	Action_t3226471752 * ___OnEnd_1;
	// UnityEngine.AudioClip Beetsoft.VAE.SoundResult::Clip
	AudioClip_t1932558630 * ___Clip_2;

public:
	inline static int32_t get_offset_of_OnPlay_0() { return static_cast<int32_t>(offsetof(SoundResult_t4177104648, ___OnPlay_0)); }
	inline Action_t3226471752 * get_OnPlay_0() const { return ___OnPlay_0; }
	inline Action_t3226471752 ** get_address_of_OnPlay_0() { return &___OnPlay_0; }
	inline void set_OnPlay_0(Action_t3226471752 * value)
	{
		___OnPlay_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnPlay_0, value);
	}

	inline static int32_t get_offset_of_OnEnd_1() { return static_cast<int32_t>(offsetof(SoundResult_t4177104648, ___OnEnd_1)); }
	inline Action_t3226471752 * get_OnEnd_1() const { return ___OnEnd_1; }
	inline Action_t3226471752 ** get_address_of_OnEnd_1() { return &___OnEnd_1; }
	inline void set_OnEnd_1(Action_t3226471752 * value)
	{
		___OnEnd_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnEnd_1, value);
	}

	inline static int32_t get_offset_of_Clip_2() { return static_cast<int32_t>(offsetof(SoundResult_t4177104648, ___Clip_2)); }
	inline AudioClip_t1932558630 * get_Clip_2() const { return ___Clip_2; }
	inline AudioClip_t1932558630 ** get_address_of_Clip_2() { return &___Clip_2; }
	inline void set_Clip_2(AudioClip_t1932558630 * value)
	{
		___Clip_2 = value;
		Il2CppCodeGenWriteBarrier(&___Clip_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
