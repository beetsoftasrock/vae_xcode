﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InitThreeDVRScene
struct InitThreeDVRScene_t2362374842;
// Asset
struct Asset_t2923928748;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Asset2923928748.h"

// System.Void InitThreeDVRScene::.ctor()
extern "C"  void InitThreeDVRScene__ctor_m2974599453 (InitThreeDVRScene_t2362374842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitThreeDVRScene::Start()
extern "C"  void InitThreeDVRScene_Start_m2248862165 (InitThreeDVRScene_t2362374842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitThreeDVRScene::LoadBundlePrefab(Asset)
extern "C"  void InitThreeDVRScene_LoadBundlePrefab_m4128245593 (InitThreeDVRScene_t2362374842 * __this, Asset_t2923928748 * ___asset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
