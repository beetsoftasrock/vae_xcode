﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// Cell[]
struct CellU5BU5D_t3743671121;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Column
struct  Column_t1930583302  : public Il2CppObject
{
public:
	// UnityEngine.Transform Column::columnCellsTransform
	Transform_t3275118058 * ___columnCellsTransform_0;
	// Cell[] Column::dataCells
	CellU5BU5D_t3743671121* ___dataCells_1;

public:
	inline static int32_t get_offset_of_columnCellsTransform_0() { return static_cast<int32_t>(offsetof(Column_t1930583302, ___columnCellsTransform_0)); }
	inline Transform_t3275118058 * get_columnCellsTransform_0() const { return ___columnCellsTransform_0; }
	inline Transform_t3275118058 ** get_address_of_columnCellsTransform_0() { return &___columnCellsTransform_0; }
	inline void set_columnCellsTransform_0(Transform_t3275118058 * value)
	{
		___columnCellsTransform_0 = value;
		Il2CppCodeGenWriteBarrier(&___columnCellsTransform_0, value);
	}

	inline static int32_t get_offset_of_dataCells_1() { return static_cast<int32_t>(offsetof(Column_t1930583302, ___dataCells_1)); }
	inline CellU5BU5D_t3743671121* get_dataCells_1() const { return ___dataCells_1; }
	inline CellU5BU5D_t3743671121** get_address_of_dataCells_1() { return &___dataCells_1; }
	inline void set_dataCells_1(CellU5BU5D_t3743671121* value)
	{
		___dataCells_1 = value;
		Il2CppCodeGenWriteBarrier(&___dataCells_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
