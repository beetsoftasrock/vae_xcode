﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataStoreKeyConfig
struct DataStoreKeyConfig_t878099218;

#include "codegen/il2cpp-codegen.h"

// System.Void DataStoreKeyConfig::.ctor()
extern "C"  void DataStoreKeyConfig__ctor_m2391592117 (DataStoreKeyConfig_t878099218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
