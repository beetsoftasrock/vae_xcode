﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSceneDataSaver
struct ConversationSceneDataSaver_t1575220710;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"

// System.Void ConversationSceneDataSaver::.ctor()
extern "C"  void ConversationSceneDataSaver__ctor_m364799375 (ConversationSceneDataSaver_t1575220710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneDataSaver::SaveRecord()
extern "C"  void ConversationSceneDataSaver_SaveRecord_m67194195 (ConversationSceneDataSaver_t1575220710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneDataSaver::PushZip(System.String[],System.String)
extern "C"  void ConversationSceneDataSaver_PushZip_m4040755844 (ConversationSceneDataSaver_t1575220710 * __this, StringU5BU5D_t1642385972* ___soundFileNames0, String_t* ___zipFilePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSceneDataSaver::SavePercent(System.Single,System.Single,System.Action,System.Action)
extern "C"  void ConversationSceneDataSaver_SavePercent_m1502552757 (ConversationSceneDataSaver_t1575220710 * __this, float ___totalTimeRecorded0, float ___totalTimeSoundTemplate1, Action_t3226471752 * ___saveCompletedCallback2, Action_t3226471752 * ___saveErrorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
