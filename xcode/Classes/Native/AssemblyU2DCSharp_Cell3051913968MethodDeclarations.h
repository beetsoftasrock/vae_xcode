﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell
struct Cell_t3051913968;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Cell3051913968.h"

// System.Void Cell::.ctor()
extern "C"  void Cell__ctor_m556620985 (Cell_t3051913968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Cell::System.IComparable<Cell>.CompareTo(Cell)
extern "C"  int32_t Cell_System_IComparableU3CCellU3E_CompareTo_m16104305 (Cell_t3051913968 * __this, Cell_t3051913968 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
