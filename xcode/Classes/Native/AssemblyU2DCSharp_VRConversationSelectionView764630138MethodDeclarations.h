﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRConversationSelectionView
struct VRConversationSelectionView_t764630138;

#include "codegen/il2cpp-codegen.h"

// System.Void VRConversationSelectionView::.ctor()
extern "C"  void VRConversationSelectionView__ctor_m869922519 (VRConversationSelectionView_t764630138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRConversationSelectionView::UpdateView()
extern "C"  void VRConversationSelectionView_UpdateView_m383999579 (VRConversationSelectionView_t764630138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
