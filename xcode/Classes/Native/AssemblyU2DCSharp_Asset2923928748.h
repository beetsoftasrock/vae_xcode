﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Asset
struct  Asset_t2923928748  : public Il2CppObject
{
public:
	// System.String Asset::BundleName
	String_t* ___BundleName_0;
	// System.String Asset::AssetName
	String_t* ___AssetName_1;

public:
	inline static int32_t get_offset_of_BundleName_0() { return static_cast<int32_t>(offsetof(Asset_t2923928748, ___BundleName_0)); }
	inline String_t* get_BundleName_0() const { return ___BundleName_0; }
	inline String_t** get_address_of_BundleName_0() { return &___BundleName_0; }
	inline void set_BundleName_0(String_t* value)
	{
		___BundleName_0 = value;
		Il2CppCodeGenWriteBarrier(&___BundleName_0, value);
	}

	inline static int32_t get_offset_of_AssetName_1() { return static_cast<int32_t>(offsetof(Asset_t2923928748, ___AssetName_1)); }
	inline String_t* get_AssetName_1() const { return ___AssetName_1; }
	inline String_t** get_address_of_AssetName_1() { return &___AssetName_1; }
	inline void set_AssetName_1(String_t* value)
	{
		___AssetName_1 = value;
		Il2CppCodeGenWriteBarrier(&___AssetName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
