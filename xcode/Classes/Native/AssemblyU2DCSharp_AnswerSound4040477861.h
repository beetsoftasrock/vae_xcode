﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnswerSound
struct  AnswerSound_t4040477861  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip AnswerSound::correctClip
	AudioClip_t1932558630 * ___correctClip_2;
	// UnityEngine.AudioClip AnswerSound::incorrectClip
	AudioClip_t1932558630 * ___incorrectClip_3;
	// UnityEngine.AudioSource AnswerSound::audioSource
	AudioSource_t1135106623 * ___audioSource_4;

public:
	inline static int32_t get_offset_of_correctClip_2() { return static_cast<int32_t>(offsetof(AnswerSound_t4040477861, ___correctClip_2)); }
	inline AudioClip_t1932558630 * get_correctClip_2() const { return ___correctClip_2; }
	inline AudioClip_t1932558630 ** get_address_of_correctClip_2() { return &___correctClip_2; }
	inline void set_correctClip_2(AudioClip_t1932558630 * value)
	{
		___correctClip_2 = value;
		Il2CppCodeGenWriteBarrier(&___correctClip_2, value);
	}

	inline static int32_t get_offset_of_incorrectClip_3() { return static_cast<int32_t>(offsetof(AnswerSound_t4040477861, ___incorrectClip_3)); }
	inline AudioClip_t1932558630 * get_incorrectClip_3() const { return ___incorrectClip_3; }
	inline AudioClip_t1932558630 ** get_address_of_incorrectClip_3() { return &___incorrectClip_3; }
	inline void set_incorrectClip_3(AudioClip_t1932558630 * value)
	{
		___incorrectClip_3 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectClip_3, value);
	}

	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(AnswerSound_t4040477861, ___audioSource_4)); }
	inline AudioSource_t1135106623 * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t1135106623 * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
