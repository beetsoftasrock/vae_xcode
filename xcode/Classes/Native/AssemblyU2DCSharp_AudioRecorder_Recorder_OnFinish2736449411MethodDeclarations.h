﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioRecorder.Recorder/OnFinish
struct OnFinish_t2736449411;
// System.Object
struct Il2CppObject;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void AudioRecorder.Recorder/OnFinish::.ctor(System.Object,System.IntPtr)
extern "C"  void OnFinish__ctor_m1837934310 (OnFinish_t2736449411 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder/OnFinish::Invoke(UnityEngine.AudioClip)
extern "C"  void OnFinish_Invoke_m2243504333 (OnFinish_t2736449411 * __this, AudioClip_t1932558630 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AudioRecorder.Recorder/OnFinish::BeginInvoke(UnityEngine.AudioClip,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnFinish_BeginInvoke_m14885726 (OnFinish_t2736449411 * __this, AudioClip_t1932558630 * ___clip0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder/OnFinish::EndInvoke(System.IAsyncResult)
extern "C"  void OnFinish_EndInvoke_m2805342724 (OnFinish_t2736449411 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
