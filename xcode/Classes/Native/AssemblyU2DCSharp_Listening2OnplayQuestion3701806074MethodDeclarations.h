﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2OnplayQuestion
struct Listening2OnplayQuestion_t3701806074;
// Listening2QuestionData
struct Listening2QuestionData_t2443975119;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3194695850;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Listening2QuestionData2443975119.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void Listening2OnplayQuestion::.ctor(Listening2QuestionData)
extern "C"  void Listening2OnplayQuestion__ctor_m708516946 (Listening2OnplayQuestion_t3701806074 * __this, Listening2QuestionData_t2443975119 * ___questionData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening2OnplayQuestion::CheckCorrect()
extern "C"  bool Listening2OnplayQuestion_CheckCorrect_m2656749051 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Listening2OnplayQuestion::GetSumScoreCorrect()
extern "C"  int32_t Listening2OnplayQuestion_GetSumScoreCorrect_m1140434388 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Listening2OnplayQuestion::CheckCorrectNew()
extern "C"  String_t* Listening2OnplayQuestion_CheckCorrectNew_m1154375694 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening2OnplayQuestion::GetTotalScore(System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern "C"  bool Listening2OnplayQuestion_GetTotalScore_m3845114231 (Listening2OnplayQuestion_t3701806074 * __this, int32_t* ___numberQuestion0, int32_t* ___numberWrong1, int32_t* ___numberRight2, int32_t* ___totalScore3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::SetView(UnityEngine.GameObject)
extern "C"  void Listening2OnplayQuestion_SetView_m1015451478 (Listening2OnplayQuestion_t3701806074 * __this, GameObject_t1756533147 * ___viewGo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::SetupEventHandler()
extern "C"  void Listening2OnplayQuestion_SetupEventHandler_m3887119776 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::HandleAnswerChangedEvent(System.Int32)
extern "C"  void Listening2OnplayQuestion_HandleAnswerChangedEvent_m1049166056 (Listening2OnplayQuestion_t3701806074 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::RemoveView()
extern "C"  void Listening2OnplayQuestion_RemoveView_m1180495888 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::ShowResult()
extern "C"  void Listening2OnplayQuestion_ShowResult_m3411047357 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Listening2OnplayQuestion::GetScore()
extern "C"  float Listening2OnplayQuestion_GetScore_m3768011431 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Listening2QuestionData Listening2OnplayQuestion::get_questionData()
extern "C"  Listening2QuestionData_t2443975119 * Listening2OnplayQuestion_get_questionData_m4104098948 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::set_questionData(Listening2QuestionData)
extern "C"  void Listening2OnplayQuestion_set_questionData_m228339881 (Listening2OnplayQuestion_t3701806074 * __this, Listening2QuestionData_t2443975119 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Boolean> Listening2OnplayQuestion::get_currentAnswer()
extern "C"  List_1_t3194695850 * Listening2OnplayQuestion_get_currentAnswer_m2666994609 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::set_currentAnswer(System.Collections.Generic.List`1<System.Boolean>)
extern "C"  void Listening2OnplayQuestion_set_currentAnswer_m2968222618 (Listening2OnplayQuestion_t3701806074 * __this, List_1_t3194695850 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> Listening2OnplayQuestion::get_currentIndexAnswer()
extern "C"  List_1_t1440998580 * Listening2OnplayQuestion_get_currentIndexAnswer_m1226152413 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::set_currentIndexAnswer(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void Listening2OnplayQuestion_set_currentIndexAnswer_m3415239858 (Listening2OnplayQuestion_t3701806074 * __this, List_1_t1440998580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::PlayQuestionSound()
extern "C"  void Listening2OnplayQuestion_PlayQuestionSound_m3287754976 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::HandleSummitAction()
extern "C"  void Listening2OnplayQuestion_HandleSummitAction_m1759290002 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2OnplayQuestion::<ShowResult>m__0()
extern "C"  void Listening2OnplayQuestion_U3CShowResultU3Em__0_m4266143292 (Listening2OnplayQuestion_t3701806074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
