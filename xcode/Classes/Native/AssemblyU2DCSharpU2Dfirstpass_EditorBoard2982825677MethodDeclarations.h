﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditorBoard
struct EditorBoard_t2982825677;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EditorBoard::.ctor()
extern "C"  void EditorBoard__ctor_m1092619784 (EditorBoard_t2982825677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorBoard::SetText(System.String)
extern "C"  void EditorBoard_SetText_m162367667 (EditorBoard_t2982825677 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EditorBoard::GetText()
extern "C"  String_t* EditorBoard_GetText_m1275062826 (EditorBoard_t2982825677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
