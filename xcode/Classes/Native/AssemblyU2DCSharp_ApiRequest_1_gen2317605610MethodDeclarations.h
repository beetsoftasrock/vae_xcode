﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApiRequest`1<System.Object>
struct ApiRequest_1_t2317605610;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "System_Core_System_Action3226471752.h"

// System.Void ApiRequest`1<System.Object>::.ctor(System.String,UnityEngine.WWWForm,System.Action`1<T>,System.Action)
extern "C"  void ApiRequest_1__ctor_m3300473338_gshared (ApiRequest_1_t2317605610 * __this, String_t* ___url0, WWWForm_t3950226929 * ___requestForm1, Action_1_t2491248677 * ___requestSuccess2, Action_t3226471752 * ___requestError3, const MethodInfo* method);
#define ApiRequest_1__ctor_m3300473338(__this, ___url0, ___requestForm1, ___requestSuccess2, ___requestError3, method) ((  void (*) (ApiRequest_1_t2317605610 *, String_t*, WWWForm_t3950226929 *, Action_1_t2491248677 *, Action_t3226471752 *, const MethodInfo*))ApiRequest_1__ctor_m3300473338_gshared)(__this, ___url0, ___requestForm1, ___requestSuccess2, ___requestError3, method)
// System.Collections.IEnumerator ApiRequest`1<System.Object>::DoRequest()
extern "C"  Il2CppObject * ApiRequest_1_DoRequest_m1521263467_gshared (ApiRequest_1_t2317605610 * __this, const MethodInfo* method);
#define ApiRequest_1_DoRequest_m1521263467(__this, method) ((  Il2CppObject * (*) (ApiRequest_1_t2317605610 *, const MethodInfo*))ApiRequest_1_DoRequest_m1521263467_gshared)(__this, method)
