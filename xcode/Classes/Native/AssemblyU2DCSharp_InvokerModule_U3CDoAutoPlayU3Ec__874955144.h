﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationCommand
struct ConversationCommand_t3660105836;
// System.Action
struct Action_t3226471752;
// InvokerModule
struct InvokerModule_t4256524346;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InvokerModule/<DoAutoPlay>c__Iterator2
struct  U3CDoAutoPlayU3Ec__Iterator2_t874955144  : public Il2CppObject
{
public:
	// ConversationCommand InvokerModule/<DoAutoPlay>c__Iterator2::<nextCommand>__0
	ConversationCommand_t3660105836 * ___U3CnextCommandU3E__0_0;
	// System.Action InvokerModule/<DoAutoPlay>c__Iterator2::callback
	Action_t3226471752 * ___callback_1;
	// InvokerModule InvokerModule/<DoAutoPlay>c__Iterator2::$this
	InvokerModule_t4256524346 * ___U24this_2;
	// System.Object InvokerModule/<DoAutoPlay>c__Iterator2::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean InvokerModule/<DoAutoPlay>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 InvokerModule/<DoAutoPlay>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CnextCommandU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoAutoPlayU3Ec__Iterator2_t874955144, ___U3CnextCommandU3E__0_0)); }
	inline ConversationCommand_t3660105836 * get_U3CnextCommandU3E__0_0() const { return ___U3CnextCommandU3E__0_0; }
	inline ConversationCommand_t3660105836 ** get_address_of_U3CnextCommandU3E__0_0() { return &___U3CnextCommandU3E__0_0; }
	inline void set_U3CnextCommandU3E__0_0(ConversationCommand_t3660105836 * value)
	{
		___U3CnextCommandU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnextCommandU3E__0_0, value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CDoAutoPlayU3Ec__Iterator2_t874955144, ___callback_1)); }
	inline Action_t3226471752 * get_callback_1() const { return ___callback_1; }
	inline Action_t3226471752 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_t3226471752 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier(&___callback_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDoAutoPlayU3Ec__Iterator2_t874955144, ___U24this_2)); }
	inline InvokerModule_t4256524346 * get_U24this_2() const { return ___U24this_2; }
	inline InvokerModule_t4256524346 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(InvokerModule_t4256524346 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDoAutoPlayU3Ec__Iterator2_t874955144, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDoAutoPlayU3Ec__Iterator2_t874955144, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDoAutoPlayU3Ec__Iterator2_t874955144, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
