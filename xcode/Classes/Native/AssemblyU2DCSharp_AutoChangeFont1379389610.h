﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Font
struct Font_t4239498691;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoChangeFont
struct  AutoChangeFont_t1379389610  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Font AutoChangeFont::normalFont
	Font_t4239498691 * ___normalFont_2;
	// UnityEngine.Font AutoChangeFont::boldFont
	Font_t4239498691 * ___boldFont_3;

public:
	inline static int32_t get_offset_of_normalFont_2() { return static_cast<int32_t>(offsetof(AutoChangeFont_t1379389610, ___normalFont_2)); }
	inline Font_t4239498691 * get_normalFont_2() const { return ___normalFont_2; }
	inline Font_t4239498691 ** get_address_of_normalFont_2() { return &___normalFont_2; }
	inline void set_normalFont_2(Font_t4239498691 * value)
	{
		___normalFont_2 = value;
		Il2CppCodeGenWriteBarrier(&___normalFont_2, value);
	}

	inline static int32_t get_offset_of_boldFont_3() { return static_cast<int32_t>(offsetof(AutoChangeFont_t1379389610, ___boldFont_3)); }
	inline Font_t4239498691 * get_boldFont_3() const { return ___boldFont_3; }
	inline Font_t4239498691 ** get_address_of_boldFont_3() { return &___boldFont_3; }
	inline void set_boldFont_3(Font_t4239498691 * value)
	{
		___boldFont_3 = value;
		Il2CppCodeGenWriteBarrier(&___boldFont_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
