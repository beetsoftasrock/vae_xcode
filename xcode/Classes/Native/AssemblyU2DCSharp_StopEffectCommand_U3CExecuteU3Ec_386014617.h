﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationLogicController
struct ConversationLogicController_t3911886281;
// StopEffectCommand
struct StopEffectCommand_t3016203262;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StopEffectCommand/<Execute>c__Iterator0
struct  U3CExecuteU3Ec__Iterator0_t386014617  : public Il2CppObject
{
public:
	// ConversationLogicController StopEffectCommand/<Execute>c__Iterator0::context
	ConversationLogicController_t3911886281 * ___context_0;
	// StopEffectCommand StopEffectCommand/<Execute>c__Iterator0::$this
	StopEffectCommand_t3016203262 * ___U24this_1;
	// System.Object StopEffectCommand/<Execute>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean StopEffectCommand/<Execute>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 StopEffectCommand/<Execute>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(U3CExecuteU3Ec__Iterator0_t386014617, ___context_0)); }
	inline ConversationLogicController_t3911886281 * get_context_0() const { return ___context_0; }
	inline ConversationLogicController_t3911886281 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(ConversationLogicController_t3911886281 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier(&___context_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CExecuteU3Ec__Iterator0_t386014617, ___U24this_1)); }
	inline StopEffectCommand_t3016203262 * get_U24this_1() const { return ___U24this_1; }
	inline StopEffectCommand_t3016203262 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(StopEffectCommand_t3016203262 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CExecuteU3Ec__Iterator0_t386014617, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CExecuteU3Ec__Iterator0_t386014617, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CExecuteU3Ec__Iterator0_t386014617, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
