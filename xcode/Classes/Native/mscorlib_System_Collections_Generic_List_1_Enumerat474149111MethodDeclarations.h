﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ConversationTalkData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2409561424(__this, ___l0, method) ((  void (*) (Enumerator_t474149111 *, List_1_t939419437 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ConversationTalkData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2399048450(__this, method) ((  void (*) (Enumerator_t474149111 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ConversationTalkData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m261301652(__this, method) ((  Il2CppObject * (*) (Enumerator_t474149111 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ConversationTalkData>::Dispose()
#define Enumerator_Dispose_m723834297(__this, method) ((  void (*) (Enumerator_t474149111 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ConversationTalkData>::VerifyState()
#define Enumerator_VerifyState_m1354833806(__this, method) ((  void (*) (Enumerator_t474149111 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ConversationTalkData>::MoveNext()
#define Enumerator_MoveNext_m2015711879(__this, method) ((  bool (*) (Enumerator_t474149111 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ConversationTalkData>::get_Current()
#define Enumerator_get_Current_m1608075363(__this, method) ((  ConversationTalkData_t1570298305 * (*) (Enumerator_t474149111 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
