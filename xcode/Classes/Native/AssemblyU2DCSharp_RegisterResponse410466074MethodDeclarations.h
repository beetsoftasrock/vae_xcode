﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegisterResponse
struct RegisterResponse_t410466074;

#include "codegen/il2cpp-codegen.h"

// System.Void RegisterResponse::.ctor()
extern "C"  void RegisterResponse__ctor_m3845728593 (RegisterResponse_t410466074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
