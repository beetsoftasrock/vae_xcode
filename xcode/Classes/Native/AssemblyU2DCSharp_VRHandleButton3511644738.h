﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRHandleButton
struct  VRHandleButton_t3511644738  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityEvent VRHandleButton::onPointerEnter
	UnityEvent_t408735097 * ___onPointerEnter_2;
	// UnityEngine.Events.UnityEvent VRHandleButton::onPointerExit
	UnityEvent_t408735097 * ___onPointerExit_3;
	// System.Boolean VRHandleButton::isHolding
	bool ___isHolding_4;

public:
	inline static int32_t get_offset_of_onPointerEnter_2() { return static_cast<int32_t>(offsetof(VRHandleButton_t3511644738, ___onPointerEnter_2)); }
	inline UnityEvent_t408735097 * get_onPointerEnter_2() const { return ___onPointerEnter_2; }
	inline UnityEvent_t408735097 ** get_address_of_onPointerEnter_2() { return &___onPointerEnter_2; }
	inline void set_onPointerEnter_2(UnityEvent_t408735097 * value)
	{
		___onPointerEnter_2 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerEnter_2, value);
	}

	inline static int32_t get_offset_of_onPointerExit_3() { return static_cast<int32_t>(offsetof(VRHandleButton_t3511644738, ___onPointerExit_3)); }
	inline UnityEvent_t408735097 * get_onPointerExit_3() const { return ___onPointerExit_3; }
	inline UnityEvent_t408735097 ** get_address_of_onPointerExit_3() { return &___onPointerExit_3; }
	inline void set_onPointerExit_3(UnityEvent_t408735097 * value)
	{
		___onPointerExit_3 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerExit_3, value);
	}

	inline static int32_t get_offset_of_isHolding_4() { return static_cast<int32_t>(offsetof(VRHandleButton_t3511644738, ___isHolding_4)); }
	inline bool get_isHolding_4() const { return ___isHolding_4; }
	inline bool* get_address_of_isHolding_4() { return &___isHolding_4; }
	inline void set_isHolding_4(bool value)
	{
		___isHolding_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
