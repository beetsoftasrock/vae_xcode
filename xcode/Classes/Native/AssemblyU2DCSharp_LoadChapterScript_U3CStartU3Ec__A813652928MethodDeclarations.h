﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadChapterScript/<Start>c__AnonStorey0
struct U3CStartU3Ec__AnonStorey0_t813652928;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadChapterScript/<Start>c__AnonStorey0::.ctor()
extern "C"  void U3CStartU3Ec__AnonStorey0__ctor_m1247142205 (U3CStartU3Ec__AnonStorey0_t813652928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadChapterScript/<Start>c__AnonStorey0::<>m__0()
extern "C"  void U3CStartU3Ec__AnonStorey0_U3CU3Em__0_m24736578 (U3CStartU3Ec__AnonStorey0_t813652928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
