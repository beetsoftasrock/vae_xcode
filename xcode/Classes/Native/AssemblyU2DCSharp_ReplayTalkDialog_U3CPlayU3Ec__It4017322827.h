﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// ReplayTalkDialog
struct ReplayTalkDialog_t3732871695;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayTalkDialog/<Play>c__Iterator0
struct  U3CPlayU3Ec__Iterator0_t4017322827  : public Il2CppObject
{
public:
	// UnityEngine.AudioClip ReplayTalkDialog/<Play>c__Iterator0::clip
	AudioClip_t1932558630 * ___clip_0;
	// System.Action ReplayTalkDialog/<Play>c__Iterator0::callback
	Action_t3226471752 * ___callback_1;
	// System.String ReplayTalkDialog/<Play>c__Iterator0::resourcePathName
	String_t* ___resourcePathName_2;
	// UnityEngine.AudioClip ReplayTalkDialog/<Play>c__Iterator0::<resourceClip>__0
	AudioClip_t1932558630 * ___U3CresourceClipU3E__0_3;
	// ConversationTalkData ReplayTalkDialog/<Play>c__Iterator0::talkData
	ConversationTalkData_t1570298305 * ___talkData_4;
	// System.Single ReplayTalkDialog/<Play>c__Iterator0::<karaokeTime>__1
	float ___U3CkaraokeTimeU3E__1_5;
	// UnityEngine.Coroutine ReplayTalkDialog/<Play>c__Iterator0::<co0>__2
	Coroutine_t2299508840 * ___U3Cco0U3E__2_6;
	// UnityEngine.Coroutine ReplayTalkDialog/<Play>c__Iterator0::<co1>__3
	Coroutine_t2299508840 * ___U3Cco1U3E__3_7;
	// ReplayTalkDialog ReplayTalkDialog/<Play>c__Iterator0::$this
	ReplayTalkDialog_t3732871695 * ___U24this_8;
	// System.Object ReplayTalkDialog/<Play>c__Iterator0::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean ReplayTalkDialog/<Play>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 ReplayTalkDialog/<Play>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_clip_0() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___clip_0)); }
	inline AudioClip_t1932558630 * get_clip_0() const { return ___clip_0; }
	inline AudioClip_t1932558630 ** get_address_of_clip_0() { return &___clip_0; }
	inline void set_clip_0(AudioClip_t1932558630 * value)
	{
		___clip_0 = value;
		Il2CppCodeGenWriteBarrier(&___clip_0, value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___callback_1)); }
	inline Action_t3226471752 * get_callback_1() const { return ___callback_1; }
	inline Action_t3226471752 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_t3226471752 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier(&___callback_1, value);
	}

	inline static int32_t get_offset_of_resourcePathName_2() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___resourcePathName_2)); }
	inline String_t* get_resourcePathName_2() const { return ___resourcePathName_2; }
	inline String_t** get_address_of_resourcePathName_2() { return &___resourcePathName_2; }
	inline void set_resourcePathName_2(String_t* value)
	{
		___resourcePathName_2 = value;
		Il2CppCodeGenWriteBarrier(&___resourcePathName_2, value);
	}

	inline static int32_t get_offset_of_U3CresourceClipU3E__0_3() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___U3CresourceClipU3E__0_3)); }
	inline AudioClip_t1932558630 * get_U3CresourceClipU3E__0_3() const { return ___U3CresourceClipU3E__0_3; }
	inline AudioClip_t1932558630 ** get_address_of_U3CresourceClipU3E__0_3() { return &___U3CresourceClipU3E__0_3; }
	inline void set_U3CresourceClipU3E__0_3(AudioClip_t1932558630 * value)
	{
		___U3CresourceClipU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresourceClipU3E__0_3, value);
	}

	inline static int32_t get_offset_of_talkData_4() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___talkData_4)); }
	inline ConversationTalkData_t1570298305 * get_talkData_4() const { return ___talkData_4; }
	inline ConversationTalkData_t1570298305 ** get_address_of_talkData_4() { return &___talkData_4; }
	inline void set_talkData_4(ConversationTalkData_t1570298305 * value)
	{
		___talkData_4 = value;
		Il2CppCodeGenWriteBarrier(&___talkData_4, value);
	}

	inline static int32_t get_offset_of_U3CkaraokeTimeU3E__1_5() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___U3CkaraokeTimeU3E__1_5)); }
	inline float get_U3CkaraokeTimeU3E__1_5() const { return ___U3CkaraokeTimeU3E__1_5; }
	inline float* get_address_of_U3CkaraokeTimeU3E__1_5() { return &___U3CkaraokeTimeU3E__1_5; }
	inline void set_U3CkaraokeTimeU3E__1_5(float value)
	{
		___U3CkaraokeTimeU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3Cco0U3E__2_6() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___U3Cco0U3E__2_6)); }
	inline Coroutine_t2299508840 * get_U3Cco0U3E__2_6() const { return ___U3Cco0U3E__2_6; }
	inline Coroutine_t2299508840 ** get_address_of_U3Cco0U3E__2_6() { return &___U3Cco0U3E__2_6; }
	inline void set_U3Cco0U3E__2_6(Coroutine_t2299508840 * value)
	{
		___U3Cco0U3E__2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cco0U3E__2_6, value);
	}

	inline static int32_t get_offset_of_U3Cco1U3E__3_7() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___U3Cco1U3E__3_7)); }
	inline Coroutine_t2299508840 * get_U3Cco1U3E__3_7() const { return ___U3Cco1U3E__3_7; }
	inline Coroutine_t2299508840 ** get_address_of_U3Cco1U3E__3_7() { return &___U3Cco1U3E__3_7; }
	inline void set_U3Cco1U3E__3_7(Coroutine_t2299508840 * value)
	{
		___U3Cco1U3E__3_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cco1U3E__3_7, value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___U24this_8)); }
	inline ReplayTalkDialog_t3732871695 * get_U24this_8() const { return ___U24this_8; }
	inline ReplayTalkDialog_t3732871695 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(ReplayTalkDialog_t3732871695 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_8, value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__Iterator0_t4017322827, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
