﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// ConversationTalkData
struct ConversationTalkData_t1570298305;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkDialog
struct  TalkDialog_t1412511810  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TalkDialog::_showSubText
	bool ____showSubText_2;
	// UnityEngine.UI.Text TalkDialog::textEN
	Text_t356221433 * ___textEN_3;
	// UnityEngine.UI.Text TalkDialog::textJP
	Text_t356221433 * ___textJP_4;
	// ConversationTalkData TalkDialog::<data>k__BackingField
	ConversationTalkData_t1570298305 * ___U3CdataU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of__showSubText_2() { return static_cast<int32_t>(offsetof(TalkDialog_t1412511810, ____showSubText_2)); }
	inline bool get__showSubText_2() const { return ____showSubText_2; }
	inline bool* get_address_of__showSubText_2() { return &____showSubText_2; }
	inline void set__showSubText_2(bool value)
	{
		____showSubText_2 = value;
	}

	inline static int32_t get_offset_of_textEN_3() { return static_cast<int32_t>(offsetof(TalkDialog_t1412511810, ___textEN_3)); }
	inline Text_t356221433 * get_textEN_3() const { return ___textEN_3; }
	inline Text_t356221433 ** get_address_of_textEN_3() { return &___textEN_3; }
	inline void set_textEN_3(Text_t356221433 * value)
	{
		___textEN_3 = value;
		Il2CppCodeGenWriteBarrier(&___textEN_3, value);
	}

	inline static int32_t get_offset_of_textJP_4() { return static_cast<int32_t>(offsetof(TalkDialog_t1412511810, ___textJP_4)); }
	inline Text_t356221433 * get_textJP_4() const { return ___textJP_4; }
	inline Text_t356221433 ** get_address_of_textJP_4() { return &___textJP_4; }
	inline void set_textJP_4(Text_t356221433 * value)
	{
		___textJP_4 = value;
		Il2CppCodeGenWriteBarrier(&___textJP_4, value);
	}

	inline static int32_t get_offset_of_U3CdataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TalkDialog_t1412511810, ___U3CdataU3Ek__BackingField_5)); }
	inline ConversationTalkData_t1570298305 * get_U3CdataU3Ek__BackingField_5() const { return ___U3CdataU3Ek__BackingField_5; }
	inline ConversationTalkData_t1570298305 ** get_address_of_U3CdataU3Ek__BackingField_5() { return &___U3CdataU3Ek__BackingField_5; }
	inline void set_U3CdataU3Ek__BackingField_5(ConversationTalkData_t1570298305 * value)
	{
		___U3CdataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdataU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
