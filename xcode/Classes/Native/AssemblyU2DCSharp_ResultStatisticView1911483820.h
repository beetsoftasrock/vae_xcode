﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultStatisticView
struct  ResultStatisticView_t1911483820  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ResultStatisticView::txtNameResult
	Text_t356221433 * ___txtNameResult_2;
	// UnityEngine.UI.Text ResultStatisticView::txtCorrectAnswerOverview
	Text_t356221433 * ___txtCorrectAnswerOverview_3;
	// UnityEngine.UI.Text ResultStatisticView::txtFrequencyLearnToday
	Text_t356221433 * ___txtFrequencyLearnToday_4;
	// UnityEngine.UI.Text ResultStatisticView::txtFrequencyLearnTotal
	Text_t356221433 * ___txtFrequencyLearnTotal_5;
	// UnityEngine.UI.Text ResultStatisticView::txtTimeLearnToday
	Text_t356221433 * ___txtTimeLearnToday_6;
	// UnityEngine.UI.Text ResultStatisticView::txtTimeLearnTotal
	Text_t356221433 * ___txtTimeLearnTotal_7;
	// UnityEngine.UI.Button ResultStatisticView::btnReload
	Button_t2872111280 * ___btnReload_8;
	// UnityEngine.UI.Button ResultStatisticView::btnFinish
	Button_t2872111280 * ___btnFinish_9;
	// UnityEngine.UI.Button ResultStatisticView::btnHelp
	Button_t2872111280 * ___btnHelp_10;
	// UnityEngine.UI.Button ResultStatisticView::btnBack
	Button_t2872111280 * ___btnBack_11;

public:
	inline static int32_t get_offset_of_txtNameResult_2() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___txtNameResult_2)); }
	inline Text_t356221433 * get_txtNameResult_2() const { return ___txtNameResult_2; }
	inline Text_t356221433 ** get_address_of_txtNameResult_2() { return &___txtNameResult_2; }
	inline void set_txtNameResult_2(Text_t356221433 * value)
	{
		___txtNameResult_2 = value;
		Il2CppCodeGenWriteBarrier(&___txtNameResult_2, value);
	}

	inline static int32_t get_offset_of_txtCorrectAnswerOverview_3() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___txtCorrectAnswerOverview_3)); }
	inline Text_t356221433 * get_txtCorrectAnswerOverview_3() const { return ___txtCorrectAnswerOverview_3; }
	inline Text_t356221433 ** get_address_of_txtCorrectAnswerOverview_3() { return &___txtCorrectAnswerOverview_3; }
	inline void set_txtCorrectAnswerOverview_3(Text_t356221433 * value)
	{
		___txtCorrectAnswerOverview_3 = value;
		Il2CppCodeGenWriteBarrier(&___txtCorrectAnswerOverview_3, value);
	}

	inline static int32_t get_offset_of_txtFrequencyLearnToday_4() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___txtFrequencyLearnToday_4)); }
	inline Text_t356221433 * get_txtFrequencyLearnToday_4() const { return ___txtFrequencyLearnToday_4; }
	inline Text_t356221433 ** get_address_of_txtFrequencyLearnToday_4() { return &___txtFrequencyLearnToday_4; }
	inline void set_txtFrequencyLearnToday_4(Text_t356221433 * value)
	{
		___txtFrequencyLearnToday_4 = value;
		Il2CppCodeGenWriteBarrier(&___txtFrequencyLearnToday_4, value);
	}

	inline static int32_t get_offset_of_txtFrequencyLearnTotal_5() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___txtFrequencyLearnTotal_5)); }
	inline Text_t356221433 * get_txtFrequencyLearnTotal_5() const { return ___txtFrequencyLearnTotal_5; }
	inline Text_t356221433 ** get_address_of_txtFrequencyLearnTotal_5() { return &___txtFrequencyLearnTotal_5; }
	inline void set_txtFrequencyLearnTotal_5(Text_t356221433 * value)
	{
		___txtFrequencyLearnTotal_5 = value;
		Il2CppCodeGenWriteBarrier(&___txtFrequencyLearnTotal_5, value);
	}

	inline static int32_t get_offset_of_txtTimeLearnToday_6() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___txtTimeLearnToday_6)); }
	inline Text_t356221433 * get_txtTimeLearnToday_6() const { return ___txtTimeLearnToday_6; }
	inline Text_t356221433 ** get_address_of_txtTimeLearnToday_6() { return &___txtTimeLearnToday_6; }
	inline void set_txtTimeLearnToday_6(Text_t356221433 * value)
	{
		___txtTimeLearnToday_6 = value;
		Il2CppCodeGenWriteBarrier(&___txtTimeLearnToday_6, value);
	}

	inline static int32_t get_offset_of_txtTimeLearnTotal_7() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___txtTimeLearnTotal_7)); }
	inline Text_t356221433 * get_txtTimeLearnTotal_7() const { return ___txtTimeLearnTotal_7; }
	inline Text_t356221433 ** get_address_of_txtTimeLearnTotal_7() { return &___txtTimeLearnTotal_7; }
	inline void set_txtTimeLearnTotal_7(Text_t356221433 * value)
	{
		___txtTimeLearnTotal_7 = value;
		Il2CppCodeGenWriteBarrier(&___txtTimeLearnTotal_7, value);
	}

	inline static int32_t get_offset_of_btnReload_8() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___btnReload_8)); }
	inline Button_t2872111280 * get_btnReload_8() const { return ___btnReload_8; }
	inline Button_t2872111280 ** get_address_of_btnReload_8() { return &___btnReload_8; }
	inline void set_btnReload_8(Button_t2872111280 * value)
	{
		___btnReload_8 = value;
		Il2CppCodeGenWriteBarrier(&___btnReload_8, value);
	}

	inline static int32_t get_offset_of_btnFinish_9() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___btnFinish_9)); }
	inline Button_t2872111280 * get_btnFinish_9() const { return ___btnFinish_9; }
	inline Button_t2872111280 ** get_address_of_btnFinish_9() { return &___btnFinish_9; }
	inline void set_btnFinish_9(Button_t2872111280 * value)
	{
		___btnFinish_9 = value;
		Il2CppCodeGenWriteBarrier(&___btnFinish_9, value);
	}

	inline static int32_t get_offset_of_btnHelp_10() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___btnHelp_10)); }
	inline Button_t2872111280 * get_btnHelp_10() const { return ___btnHelp_10; }
	inline Button_t2872111280 ** get_address_of_btnHelp_10() { return &___btnHelp_10; }
	inline void set_btnHelp_10(Button_t2872111280 * value)
	{
		___btnHelp_10 = value;
		Il2CppCodeGenWriteBarrier(&___btnHelp_10, value);
	}

	inline static int32_t get_offset_of_btnBack_11() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820, ___btnBack_11)); }
	inline Button_t2872111280 * get_btnBack_11() const { return ___btnBack_11; }
	inline Button_t2872111280 ** get_address_of_btnBack_11() { return &___btnBack_11; }
	inline void set_btnBack_11(Button_t2872111280 * value)
	{
		___btnBack_11 = value;
		Il2CppCodeGenWriteBarrier(&___btnBack_11, value);
	}
};

struct ResultStatisticView_t1911483820_StaticFields
{
public:
	// UnityEngine.Events.UnityAction ResultStatisticView::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(ResultStatisticView_t1911483820_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
