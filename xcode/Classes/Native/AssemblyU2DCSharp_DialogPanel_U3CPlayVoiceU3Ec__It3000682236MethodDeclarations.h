﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DialogPanel/<PlayVoice>c__Iterator0
struct U3CPlayVoiceU3Ec__Iterator0_t3000682236;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DialogPanel/<PlayVoice>c__Iterator0::.ctor()
extern "C"  void U3CPlayVoiceU3Ec__Iterator0__ctor_m1239237263 (U3CPlayVoiceU3Ec__Iterator0_t3000682236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DialogPanel/<PlayVoice>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayVoiceU3Ec__Iterator0_MoveNext_m2831306197 (U3CPlayVoiceU3Ec__Iterator0_t3000682236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DialogPanel/<PlayVoice>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayVoiceU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1948413165 (U3CPlayVoiceU3Ec__Iterator0_t3000682236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DialogPanel/<PlayVoice>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayVoiceU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m559942213 (U3CPlayVoiceU3Ec__Iterator0_t3000682236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel/<PlayVoice>c__Iterator0::Dispose()
extern "C"  void U3CPlayVoiceU3Ec__Iterator0_Dispose_m3481533270 (U3CPlayVoiceU3Ec__Iterator0_t3000682236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogPanel/<PlayVoice>c__Iterator0::Reset()
extern "C"  void U3CPlayVoiceU3Ec__Iterator0_Reset_m1765977828 (U3CPlayVoiceU3Ec__Iterator0_t3000682236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
