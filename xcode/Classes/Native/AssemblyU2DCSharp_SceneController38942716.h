﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FadeObject
struct FadeObject_t1506616101;
// SceneController
struct SceneController_t38942716;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneController
struct  SceneController_t38942716  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SceneController::isLoading
	bool ___isLoading_2;
	// FadeObject SceneController::FadeObject
	FadeObject_t1506616101 * ___FadeObject_3;

public:
	inline static int32_t get_offset_of_isLoading_2() { return static_cast<int32_t>(offsetof(SceneController_t38942716, ___isLoading_2)); }
	inline bool get_isLoading_2() const { return ___isLoading_2; }
	inline bool* get_address_of_isLoading_2() { return &___isLoading_2; }
	inline void set_isLoading_2(bool value)
	{
		___isLoading_2 = value;
	}

	inline static int32_t get_offset_of_FadeObject_3() { return static_cast<int32_t>(offsetof(SceneController_t38942716, ___FadeObject_3)); }
	inline FadeObject_t1506616101 * get_FadeObject_3() const { return ___FadeObject_3; }
	inline FadeObject_t1506616101 ** get_address_of_FadeObject_3() { return &___FadeObject_3; }
	inline void set_FadeObject_3(FadeObject_t1506616101 * value)
	{
		___FadeObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___FadeObject_3, value);
	}
};

struct SceneController_t38942716_StaticFields
{
public:
	// SceneController SceneController::instance
	SceneController_t38942716 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(SceneController_t38942716_StaticFields, ___instance_4)); }
	inline SceneController_t38942716 * get_instance_4() const { return ___instance_4; }
	inline SceneController_t38942716 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(SceneController_t38942716 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
