﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object,Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>
struct Transform_1_t205558989;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Exte3093161221.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object,Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3685213815_gshared (Transform_1_t205558989 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m3685213815(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t205558989 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3685213815_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object,Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::Invoke(TKey,TValue)
extern "C"  ExtensionIntPair_t3093161221  Transform_1_Invoke_m2075099811_gshared (Transform_1_t205558989 * __this, ExtensionIntPair_t3093161221  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2075099811(__this, ___key0, ___value1, method) ((  ExtensionIntPair_t3093161221  (*) (Transform_1_t205558989 *, ExtensionIntPair_t3093161221 , Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2075099811_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object,Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m205844546_gshared (Transform_1_t205558989 * __this, ExtensionIntPair_t3093161221  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m205844546(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t205558989 *, ExtensionIntPair_t3093161221 , Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m205844546_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,System.Object,Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair>::EndInvoke(System.IAsyncResult)
extern "C"  ExtensionIntPair_t3093161221  Transform_1_EndInvoke_m838145761_gshared (Transform_1_t205558989 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m838145761(__this, ___result0, method) ((  ExtensionIntPair_t3093161221  (*) (Transform_1_t205558989 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m838145761_gshared)(__this, ___result0, method)
