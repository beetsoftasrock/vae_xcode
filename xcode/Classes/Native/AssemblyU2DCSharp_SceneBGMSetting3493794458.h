﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BGMController
struct BGMController_t1439355246;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneBGMSetting
struct  SceneBGMSetting_t3493794458  : public MonoBehaviour_t1158329972
{
public:
	// BGMController SceneBGMSetting::bgmController
	BGMController_t1439355246 * ___bgmController_2;

public:
	inline static int32_t get_offset_of_bgmController_2() { return static_cast<int32_t>(offsetof(SceneBGMSetting_t3493794458, ___bgmController_2)); }
	inline BGMController_t1439355246 * get_bgmController_2() const { return ___bgmController_2; }
	inline BGMController_t1439355246 ** get_address_of_bgmController_2() { return &___bgmController_2; }
	inline void set_bgmController_2(BGMController_t1439355246 * value)
	{
		___bgmController_2 = value;
		Il2CppCodeGenWriteBarrier(&___bgmController_2, value);
	}
};

struct SceneBGMSetting_t3493794458_StaticFields
{
public:
	// System.String[] SceneBGMSetting::playBGMScenes
	StringU5BU5D_t1642385972* ___playBGMScenes_3;

public:
	inline static int32_t get_offset_of_playBGMScenes_3() { return static_cast<int32_t>(offsetof(SceneBGMSetting_t3493794458_StaticFields, ___playBGMScenes_3)); }
	inline StringU5BU5D_t1642385972* get_playBGMScenes_3() const { return ___playBGMScenes_3; }
	inline StringU5BU5D_t1642385972** get_address_of_playBGMScenes_3() { return &___playBGMScenes_3; }
	inline void set_playBGMScenes_3(StringU5BU5D_t1642385972* value)
	{
		___playBGMScenes_3 = value;
		Il2CppCodeGenWriteBarrier(&___playBGMScenes_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
