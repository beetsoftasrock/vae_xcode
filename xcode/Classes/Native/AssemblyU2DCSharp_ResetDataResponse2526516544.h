﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResetDataResponse
struct  ResetDataResponse_t2526516544  : public Il2CppObject
{
public:
	// System.Boolean ResetDataResponse::success
	bool ___success_0;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(ResetDataResponse_t2526516544, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
