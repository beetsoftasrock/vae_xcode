﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoToCommand
struct  GoToCommand_t2985371124  : public ConversationCommand_t3660105836
{
public:
	// System.String GoToCommand::_destination
	String_t* ____destination_0;
	// System.Int32 GoToCommand::_preCurrentPoint
	int32_t ____preCurrentPoint_1;

public:
	inline static int32_t get_offset_of__destination_0() { return static_cast<int32_t>(offsetof(GoToCommand_t2985371124, ____destination_0)); }
	inline String_t* get__destination_0() const { return ____destination_0; }
	inline String_t** get_address_of__destination_0() { return &____destination_0; }
	inline void set__destination_0(String_t* value)
	{
		____destination_0 = value;
		Il2CppCodeGenWriteBarrier(&____destination_0, value);
	}

	inline static int32_t get_offset_of__preCurrentPoint_1() { return static_cast<int32_t>(offsetof(GoToCommand_t2985371124, ____preCurrentPoint_1)); }
	inline int32_t get__preCurrentPoint_1() const { return ____preCurrentPoint_1; }
	inline int32_t* get_address_of__preCurrentPoint_1() { return &____preCurrentPoint_1; }
	inline void set__preCurrentPoint_1(int32_t value)
	{
		____preCurrentPoint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
