﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VocabularyOnplayQuestion
struct VocabularyOnplayQuestion_t3519225043;
// VocabularyQuestionData
struct VocabularyQuestionData_t2021074976;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VocabularyQuestionData2021074976.h"
#include "AssemblyU2DCSharp_VocabularyOnplayQuestion_Selecte3971704390.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VocabularyOnplayQuestion::.ctor(VocabularyQuestionData)
extern "C"  void VocabularyOnplayQuestion__ctor_m1904838930 (VocabularyOnplayQuestion_t3519225043 * __this, VocabularyQuestionData_t2021074976 * ___ques0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VocabularyOnplayQuestion::CheckCorrect()
extern "C"  bool VocabularyOnplayQuestion_CheckCorrect_m4105310264 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VocabularyOnplayQuestion/SelectedState VocabularyOnplayQuestion::get_currentSelected()
extern "C"  int32_t VocabularyOnplayQuestion_get_currentSelected_m646498646 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::set_currentSelected(VocabularyOnplayQuestion/SelectedState)
extern "C"  void VocabularyOnplayQuestion_set_currentSelected_m1116331715 (VocabularyOnplayQuestion_t3519225043 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::SetView(UnityEngine.GameObject)
extern "C"  void VocabularyOnplayQuestion_SetView_m869706861 (VocabularyOnplayQuestion_t3519225043 * __this, GameObject_t1756533147 * ___viewGo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::RemoveView()
extern "C"  void VocabularyOnplayQuestion_RemoveView_m3945840105 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::ShowResult()
extern "C"  void VocabularyOnplayQuestion_ShowResult_m901909396 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VocabularyOnplayQuestion::GetScore()
extern "C"  float VocabularyOnplayQuestion_GetScore_m3607958992 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::InitView()
extern "C"  void VocabularyOnplayQuestion_InitView_m1770140217 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::SetEventHandler()
extern "C"  void VocabularyOnplayQuestion_SetEventHandler_m2971734090 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::HandleSummitAction()
extern "C"  void VocabularyOnplayQuestion_HandleSummitAction_m89988561 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::PlayQuestionSound()
extern "C"  void VocabularyOnplayQuestion_PlayQuestionSound_m302349927 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::<SetEventHandler>m__0()
extern "C"  void VocabularyOnplayQuestion_U3CSetEventHandlerU3Em__0_m800678311 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::<SetEventHandler>m__1()
extern "C"  void VocabularyOnplayQuestion_U3CSetEventHandlerU3Em__1_m800678216 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::<SetEventHandler>m__2()
extern "C"  void VocabularyOnplayQuestion_U3CSetEventHandlerU3Em__2_m800678245 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyOnplayQuestion::<SetEventHandler>m__3()
extern "C"  void VocabularyOnplayQuestion_U3CSetEventHandlerU3Em__3_m800678150 (VocabularyOnplayQuestion_t3519225043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
