﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// LearningData[]
struct LearningDataU5BU5D_t142178747;
// UnityEngine.UI.Button
struct Button_t2872111280;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginSceneController
struct  LoginSceneController_t3246135591  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text LoginSceneController::resultText
	Text_t356221433 * ___resultText_3;
	// UnityEngine.UI.Text LoginSceneController::codeText
	Text_t356221433 * ___codeText_4;
	// System.String LoginSceneController::_id
	String_t* ____id_5;
	// System.String LoginSceneController::_code
	String_t* ____code_6;
	// System.String LoginSceneController::_userName
	String_t* ____userName_7;
	// LearningData[] LoginSceneController::_learningDatas
	LearningDataU5BU5D_t142178747* ____learningDatas_8;
	// UnityEngine.UI.Button LoginSceneController::summitButton
	Button_t2872111280 * ___summitButton_9;

public:
	inline static int32_t get_offset_of_resultText_3() { return static_cast<int32_t>(offsetof(LoginSceneController_t3246135591, ___resultText_3)); }
	inline Text_t356221433 * get_resultText_3() const { return ___resultText_3; }
	inline Text_t356221433 ** get_address_of_resultText_3() { return &___resultText_3; }
	inline void set_resultText_3(Text_t356221433 * value)
	{
		___resultText_3 = value;
		Il2CppCodeGenWriteBarrier(&___resultText_3, value);
	}

	inline static int32_t get_offset_of_codeText_4() { return static_cast<int32_t>(offsetof(LoginSceneController_t3246135591, ___codeText_4)); }
	inline Text_t356221433 * get_codeText_4() const { return ___codeText_4; }
	inline Text_t356221433 ** get_address_of_codeText_4() { return &___codeText_4; }
	inline void set_codeText_4(Text_t356221433 * value)
	{
		___codeText_4 = value;
		Il2CppCodeGenWriteBarrier(&___codeText_4, value);
	}

	inline static int32_t get_offset_of__id_5() { return static_cast<int32_t>(offsetof(LoginSceneController_t3246135591, ____id_5)); }
	inline String_t* get__id_5() const { return ____id_5; }
	inline String_t** get_address_of__id_5() { return &____id_5; }
	inline void set__id_5(String_t* value)
	{
		____id_5 = value;
		Il2CppCodeGenWriteBarrier(&____id_5, value);
	}

	inline static int32_t get_offset_of__code_6() { return static_cast<int32_t>(offsetof(LoginSceneController_t3246135591, ____code_6)); }
	inline String_t* get__code_6() const { return ____code_6; }
	inline String_t** get_address_of__code_6() { return &____code_6; }
	inline void set__code_6(String_t* value)
	{
		____code_6 = value;
		Il2CppCodeGenWriteBarrier(&____code_6, value);
	}

	inline static int32_t get_offset_of__userName_7() { return static_cast<int32_t>(offsetof(LoginSceneController_t3246135591, ____userName_7)); }
	inline String_t* get__userName_7() const { return ____userName_7; }
	inline String_t** get_address_of__userName_7() { return &____userName_7; }
	inline void set__userName_7(String_t* value)
	{
		____userName_7 = value;
		Il2CppCodeGenWriteBarrier(&____userName_7, value);
	}

	inline static int32_t get_offset_of__learningDatas_8() { return static_cast<int32_t>(offsetof(LoginSceneController_t3246135591, ____learningDatas_8)); }
	inline LearningDataU5BU5D_t142178747* get__learningDatas_8() const { return ____learningDatas_8; }
	inline LearningDataU5BU5D_t142178747** get_address_of__learningDatas_8() { return &____learningDatas_8; }
	inline void set__learningDatas_8(LearningDataU5BU5D_t142178747* value)
	{
		____learningDatas_8 = value;
		Il2CppCodeGenWriteBarrier(&____learningDatas_8, value);
	}

	inline static int32_t get_offset_of_summitButton_9() { return static_cast<int32_t>(offsetof(LoginSceneController_t3246135591, ___summitButton_9)); }
	inline Button_t2872111280 * get_summitButton_9() const { return ___summitButton_9; }
	inline Button_t2872111280 ** get_address_of_summitButton_9() { return &___summitButton_9; }
	inline void set_summitButton_9(Button_t2872111280 * value)
	{
		___summitButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___summitButton_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
