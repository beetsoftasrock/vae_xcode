﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectionController
struct SelectionController_t1038360966;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectionController::.ctor()
extern "C"  void SelectionController__ctor_m1365596759 (SelectionController_t1038360966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionController::Set(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void SelectionController_Set_m2756532151 (SelectionController_t1038360966 * __this, List_1_t3492939606 * ___selections0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionController::OnSelectButton(System.Int32)
extern "C"  void SelectionController_OnSelectButton_m117228157 (SelectionController_t1038360966 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionController::Start()
extern "C"  void SelectionController_Start_m4259328139 (SelectionController_t1038360966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionController::Update()
extern "C"  void SelectionController_Update_m4294949812 (SelectionController_t1038360966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
