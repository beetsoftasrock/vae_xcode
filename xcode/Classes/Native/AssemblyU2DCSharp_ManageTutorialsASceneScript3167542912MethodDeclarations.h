﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManageTutorialsASceneScript
struct ManageTutorialsASceneScript_t3167542912;

#include "codegen/il2cpp-codegen.h"

// System.Void ManageTutorialsASceneScript::.ctor()
extern "C"  void ManageTutorialsASceneScript__ctor_m1156988177 (ManageTutorialsASceneScript_t3167542912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
