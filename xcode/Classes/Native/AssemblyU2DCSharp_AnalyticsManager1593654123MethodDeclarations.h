﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyticsManager
struct AnalyticsManager_t1593654123;
// GlobalConfig
struct GlobalConfig_t3080413471;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AnalyticsManager::.ctor()
extern "C"  void AnalyticsManager__ctor_m342599602 (AnalyticsManager_t1593654123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnalyticsManager AnalyticsManager::get_Instance()
extern "C"  AnalyticsManager_t1593654123 * AnalyticsManager_get_Instance_m491779676 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalConfig AnalyticsManager::get_globalConfig()
extern "C"  GlobalConfig_t3080413471 * AnalyticsManager_get_globalConfig_m691307180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsManager::EventTimeLearning(System.String,System.String,System.String,System.String)
extern "C"  void AnalyticsManager_EventTimeLearning_m2429424067 (AnalyticsManager_t1593654123 * __this, String_t* ___eventName0, String_t* ___nameLession1, String_t* ___action2, String_t* ___time3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsManager::EventRecordAchivement(System.String,System.String,System.String,System.String)
extern "C"  void AnalyticsManager_EventRecordAchivement_m1986627561 (AnalyticsManager_t1593654123 * __this, String_t* ___eventName0, String_t* ___nameLession1, String_t* ___action2, String_t* ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsManager::EventCountEffort_Without_Part(System.String,System.String,System.String,System.Single)
extern "C"  void AnalyticsManager_EventCountEffort_Without_Part_m844695405 (AnalyticsManager_t1593654123 * __this, String_t* ___eventName0, String_t* ___nameLession1, String_t* ___action2, float ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsManager::EventCountEffort_With_Part(System.String,System.String,System.String,System.String,System.Single)
extern "C"  void AnalyticsManager_EventCountEffort_With_Part_m205686329 (AnalyticsManager_t1593654123 * __this, String_t* ___eventName0, String_t* ___nameLession1, String_t* ___part2, String_t* ___action3, float ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsManager::CountSoundAudioRecorder_Without_Part(System.String,System.String,System.String,System.Single)
extern "C"  void AnalyticsManager_CountSoundAudioRecorder_Without_Part_m809958784 (AnalyticsManager_t1593654123 * __this, String_t* ___eventName0, String_t* ___lessionName1, String_t* ___action2, float ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsManager::CountSoundAudioRecorder_With_Part(System.String,System.String,System.String,System.String,System.Single)
extern "C"  void AnalyticsManager_CountSoundAudioRecorder_With_Part_m88535948 (AnalyticsManager_t1593654123 * __this, String_t* ___eventName0, String_t* ___lessionName1, String_t* ___part2, String_t* ___action3, float ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsManager::EventCountCommon(System.String,System.String,System.Single)
extern "C"  void AnalyticsManager_EventCountCommon_m1111049877 (AnalyticsManager_t1593654123 * __this, String_t* ___eventName0, String_t* ___action1, float ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsManager::EventRepeatRecordAudio(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnalyticsManager_EventRepeatRecordAudio_m825371678 (AnalyticsManager_t1593654123 * __this, String_t* ___eventName0, String_t* ___lession1, String_t* ___part2, String_t* ___soundAudioName3, String_t* ___japaneseText4, String_t* ___enText5, String_t* ___action6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
