﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TalkDialogWithSoundButton
struct TalkDialogWithSoundButton_t1663397415;

#include "codegen/il2cpp-codegen.h"

// System.Void TalkDialogWithSoundButton::.ctor()
extern "C"  void TalkDialogWithSoundButton__ctor_m2880485456 (TalkDialogWithSoundButton_t1663397415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
