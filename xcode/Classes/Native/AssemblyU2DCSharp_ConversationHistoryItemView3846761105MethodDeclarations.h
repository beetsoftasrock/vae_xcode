﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationHistoryItemView
struct ConversationHistoryItemView_t3846761105;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"

// System.Void ConversationHistoryItemView::.ctor()
extern "C"  void ConversationHistoryItemView__ctor_m99191630 (ConversationHistoryItemView_t3846761105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationHistoryItemView::OnConversationReset()
extern "C"  void ConversationHistoryItemView_OnConversationReset_m825800621 (ConversationHistoryItemView_t3846761105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationHistoryItemView::OnConversationReplay()
extern "C"  void ConversationHistoryItemView_OnConversationReplay_m3763684267 (ConversationHistoryItemView_t3846761105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationHistoryItemView::SetupView(System.Boolean,ConversationTalkData,System.String,System.String)
extern "C"  void ConversationHistoryItemView_SetupView_m3384220136 (ConversationHistoryItemView_t3846761105 * __this, bool ___isPlayerRole0, ConversationTalkData_t1570298305 * ___data1, String_t* ___strEn2, String_t* ___strJp3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationHistoryItemView::SetBackgroundSprite(UnityEngine.Sprite)
extern "C"  void ConversationHistoryItemView_SetBackgroundSprite_m3615977259 (ConversationHistoryItemView_t3846761105 * __this, Sprite_t309593783 * ___sprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationHistoryItemView::RefreshView()
extern "C"  void ConversationHistoryItemView_RefreshView_m3894925664 (ConversationHistoryItemView_t3846761105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationHistoryItemView::OnClicked()
extern "C"  void ConversationHistoryItemView_OnClicked_m4063903072 (ConversationHistoryItemView_t3846761105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
