﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectCommand/<AutoExecute>c__Iterator0
struct U3CAutoExecuteU3Ec__Iterator0_t513472262;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectCommand/<AutoExecute>c__Iterator0::.ctor()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0__ctor_m1072416855 (U3CAutoExecuteU3Ec__Iterator0_t513472262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EffectCommand/<AutoExecute>c__Iterator0::MoveNext()
extern "C"  bool U3CAutoExecuteU3Ec__Iterator0_MoveNext_m2096672585 (U3CAutoExecuteU3Ec__Iterator0_t513472262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EffectCommand/<AutoExecute>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAutoExecuteU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3823075777 (U3CAutoExecuteU3Ec__Iterator0_t513472262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EffectCommand/<AutoExecute>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAutoExecuteU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3850395865 (U3CAutoExecuteU3Ec__Iterator0_t513472262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCommand/<AutoExecute>c__Iterator0::Dispose()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0_Dispose_m274901608 (U3CAutoExecuteU3Ec__Iterator0_t513472262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCommand/<AutoExecute>c__Iterator0::Reset()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0_Reset_m1661396010 (U3CAutoExecuteU3Ec__Iterator0_t513472262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
