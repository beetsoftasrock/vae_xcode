﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ReplayTalkDialog
struct ReplayTalkDialog_t3732871695;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// PlayerTalkModule
struct PlayerTalkModule_t3356839145;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1
struct  U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947  : public Il2CppObject
{
public:
	// ReplayTalkDialog PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::<dialog>__0
	ReplayTalkDialog_t3732871695 * ___U3CdialogU3E__0_0;
	// ConversationTalkData PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::data
	ConversationTalkData_t1570298305 * ___data_1;
	// PlayerTalkModule PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::$this
	PlayerTalkModule_t3356839145 * ___U24this_2;
	// System.Object PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CdialogU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947, ___U3CdialogU3E__0_0)); }
	inline ReplayTalkDialog_t3732871695 * get_U3CdialogU3E__0_0() const { return ___U3CdialogU3E__0_0; }
	inline ReplayTalkDialog_t3732871695 ** get_address_of_U3CdialogU3E__0_0() { return &___U3CdialogU3E__0_0; }
	inline void set_U3CdialogU3E__0_0(ReplayTalkDialog_t3732871695 * value)
	{
		___U3CdialogU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdialogU3E__0_0, value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947, ___data_1)); }
	inline ConversationTalkData_t1570298305 * get_data_1() const { return ___data_1; }
	inline ConversationTalkData_t1570298305 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ConversationTalkData_t1570298305 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947, ___U24this_2)); }
	inline PlayerTalkModule_t3356839145 * get_U24this_2() const { return ___U24this_2; }
	inline PlayerTalkModule_t3356839145 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PlayerTalkModule_t3356839145 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

struct U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947_StaticFields
{
public:
	// System.Action PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
