﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MultiKeyDictionary`3<System.Object,System.Object,System.Object>
struct MultiKeyDictionary_3_t2642076887;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void MultiKeyDictionary`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void MultiKeyDictionary_3__ctor_m2884643606_gshared (MultiKeyDictionary_3_t2642076887 * __this, const MethodInfo* method);
#define MultiKeyDictionary_3__ctor_m2884643606(__this, method) ((  void (*) (MultiKeyDictionary_3_t2642076887 *, const MethodInfo*))MultiKeyDictionary_3__ctor_m2884643606_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2<T2,T3> MultiKeyDictionary`3<System.Object,System.Object,System.Object>::get_Item(T1)
extern "C"  Dictionary_2_t2281509423 * MultiKeyDictionary_3_get_Item_m186052900_gshared (MultiKeyDictionary_3_t2642076887 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define MultiKeyDictionary_3_get_Item_m186052900(__this, ___key0, method) ((  Dictionary_2_t2281509423 * (*) (MultiKeyDictionary_3_t2642076887 *, Il2CppObject *, const MethodInfo*))MultiKeyDictionary_3_get_Item_m186052900_gshared)(__this, ___key0, method)
// System.Boolean MultiKeyDictionary`3<System.Object,System.Object,System.Object>::ContainsKey(T1,T2)
extern "C"  bool MultiKeyDictionary_3_ContainsKey_m2647488417_gshared (MultiKeyDictionary_3_t2642076887 * __this, Il2CppObject * ___key10, Il2CppObject * ___key21, const MethodInfo* method);
#define MultiKeyDictionary_3_ContainsKey_m2647488417(__this, ___key10, ___key21, method) ((  bool (*) (MultiKeyDictionary_3_t2642076887 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))MultiKeyDictionary_3_ContainsKey_m2647488417_gshared)(__this, ___key10, ___key21, method)
