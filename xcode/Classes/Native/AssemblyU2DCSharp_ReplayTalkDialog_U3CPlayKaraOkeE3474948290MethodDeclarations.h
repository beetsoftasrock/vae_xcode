﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2
struct U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::.ctor()
extern "C"  void U3CPlayKaraOkeEffectU3Ec__Iterator2__ctor_m263283227 (U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::MoveNext()
extern "C"  bool U3CPlayKaraOkeEffectU3Ec__Iterator2_MoveNext_m3506697061 (U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayKaraOkeEffectU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m303191057 (U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayKaraOkeEffectU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2415474585 (U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::Dispose()
extern "C"  void U3CPlayKaraOkeEffectU3Ec__Iterator2_Dispose_m315060712 (U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::Reset()
extern "C"  void U3CPlayKaraOkeEffectU3Ec__Iterator2_Reset_m77725234 (U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
