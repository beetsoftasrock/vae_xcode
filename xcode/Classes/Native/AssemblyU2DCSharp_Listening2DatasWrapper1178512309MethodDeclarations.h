﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2DatasWrapper
struct Listening2DatasWrapper_t1178512309;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// System.Collections.Generic.List`1<Listening2QuestionData>
struct List_1_t1813096251;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening2DatasWrapper::.ctor(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void Listening2DatasWrapper__ctor_m1119702896 (Listening2DatasWrapper_t1178512309 * __this, List_1_t3492939606 * ___wrappedDatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Listening2QuestionData> Listening2DatasWrapper::GetConvertedDatas()
extern "C"  List_1_t1813096251 * Listening2DatasWrapper_GetConvertedDatas_m528137755 (Listening2DatasWrapper_t1178512309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
