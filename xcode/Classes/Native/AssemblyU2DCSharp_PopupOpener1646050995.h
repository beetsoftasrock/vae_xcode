﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Canvas
struct Canvas_t209405766;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupOpener
struct  PopupOpener_t1646050995  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PopupOpener::prefabPopup
	GameObject_t1756533147 * ___prefabPopup_2;
	// UnityEngine.Canvas PopupOpener::canvas
	Canvas_t209405766 * ___canvas_3;
	// UnityEngine.GameObject PopupOpener::<PopupClone>k__BackingField
	GameObject_t1756533147 * ___U3CPopupCloneU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_prefabPopup_2() { return static_cast<int32_t>(offsetof(PopupOpener_t1646050995, ___prefabPopup_2)); }
	inline GameObject_t1756533147 * get_prefabPopup_2() const { return ___prefabPopup_2; }
	inline GameObject_t1756533147 ** get_address_of_prefabPopup_2() { return &___prefabPopup_2; }
	inline void set_prefabPopup_2(GameObject_t1756533147 * value)
	{
		___prefabPopup_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefabPopup_2, value);
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(PopupOpener_t1646050995, ___canvas_3)); }
	inline Canvas_t209405766 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t209405766 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t209405766 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_3, value);
	}

	inline static int32_t get_offset_of_U3CPopupCloneU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PopupOpener_t1646050995, ___U3CPopupCloneU3Ek__BackingField_4)); }
	inline GameObject_t1756533147 * get_U3CPopupCloneU3Ek__BackingField_4() const { return ___U3CPopupCloneU3Ek__BackingField_4; }
	inline GameObject_t1756533147 ** get_address_of_U3CPopupCloneU3Ek__BackingField_4() { return &___U3CPopupCloneU3Ek__BackingField_4; }
	inline void set_U3CPopupCloneU3Ek__BackingField_4(GameObject_t1756533147 * value)
	{
		___U3CPopupCloneU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPopupCloneU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
