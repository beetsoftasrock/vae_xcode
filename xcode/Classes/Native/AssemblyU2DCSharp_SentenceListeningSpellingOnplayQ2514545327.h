﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// SentenceListeningSpellingQuestionData
struct SentenceListeningSpellingQuestionData_t3011907248;
// Sentence2QuestionView
struct Sentence2QuestionView_t1942347884;

#include "AssemblyU2DCSharp_BaseOnplayQuestion1717064182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SentenceListeningSpellingOnplayQuestion
struct  SentenceListeningSpellingOnplayQuestion_t2514545327  : public BaseOnplayQuestion_t1717064182
{
public:
	// System.String SentenceListeningSpellingOnplayQuestion::soundPath
	String_t* ___soundPath_2;
	// SentenceListeningSpellingQuestionData SentenceListeningSpellingOnplayQuestion::<questionData>k__BackingField
	SentenceListeningSpellingQuestionData_t3011907248 * ___U3CquestionDataU3Ek__BackingField_3;
	// Sentence2QuestionView SentenceListeningSpellingOnplayQuestion::_view
	Sentence2QuestionView_t1942347884 * ____view_4;
	// System.String SentenceListeningSpellingOnplayQuestion::<currentAnswerText>k__BackingField
	String_t* ___U3CcurrentAnswerTextU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_soundPath_2() { return static_cast<int32_t>(offsetof(SentenceListeningSpellingOnplayQuestion_t2514545327, ___soundPath_2)); }
	inline String_t* get_soundPath_2() const { return ___soundPath_2; }
	inline String_t** get_address_of_soundPath_2() { return &___soundPath_2; }
	inline void set_soundPath_2(String_t* value)
	{
		___soundPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_2, value);
	}

	inline static int32_t get_offset_of_U3CquestionDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SentenceListeningSpellingOnplayQuestion_t2514545327, ___U3CquestionDataU3Ek__BackingField_3)); }
	inline SentenceListeningSpellingQuestionData_t3011907248 * get_U3CquestionDataU3Ek__BackingField_3() const { return ___U3CquestionDataU3Ek__BackingField_3; }
	inline SentenceListeningSpellingQuestionData_t3011907248 ** get_address_of_U3CquestionDataU3Ek__BackingField_3() { return &___U3CquestionDataU3Ek__BackingField_3; }
	inline void set_U3CquestionDataU3Ek__BackingField_3(SentenceListeningSpellingQuestionData_t3011907248 * value)
	{
		___U3CquestionDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionDataU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of__view_4() { return static_cast<int32_t>(offsetof(SentenceListeningSpellingOnplayQuestion_t2514545327, ____view_4)); }
	inline Sentence2QuestionView_t1942347884 * get__view_4() const { return ____view_4; }
	inline Sentence2QuestionView_t1942347884 ** get_address_of__view_4() { return &____view_4; }
	inline void set__view_4(Sentence2QuestionView_t1942347884 * value)
	{
		____view_4 = value;
		Il2CppCodeGenWriteBarrier(&____view_4, value);
	}

	inline static int32_t get_offset_of_U3CcurrentAnswerTextU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SentenceListeningSpellingOnplayQuestion_t2514545327, ___U3CcurrentAnswerTextU3Ek__BackingField_5)); }
	inline String_t* get_U3CcurrentAnswerTextU3Ek__BackingField_5() const { return ___U3CcurrentAnswerTextU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CcurrentAnswerTextU3Ek__BackingField_5() { return &___U3CcurrentAnswerTextU3Ek__BackingField_5; }
	inline void set_U3CcurrentAnswerTextU3Ek__BackingField_5(String_t* value)
	{
		___U3CcurrentAnswerTextU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentAnswerTextU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
