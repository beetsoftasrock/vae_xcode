﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManageTutorialsInHome
struct ManageTutorialsInHome_t2855635566;

#include "codegen/il2cpp-codegen.h"

// System.Void ManageTutorialsInHome::.ctor()
extern "C"  void ManageTutorialsInHome__ctor_m2920141413 (ManageTutorialsInHome_t2855635566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
