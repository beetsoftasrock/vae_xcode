﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening2SelectableItem
struct  Listening2SelectableItem_t492261174  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image Listening2SelectableItem::_trueBg
	Image_t2042527209 * ____trueBg_2;
	// UnityEngine.UI.Text Listening2SelectableItem::_trueText
	Text_t356221433 * ____trueText_3;
	// UnityEngine.UI.Image Listening2SelectableItem::_falseBg
	Image_t2042527209 * ____falseBg_4;
	// UnityEngine.UI.Text Listening2SelectableItem::_falseText
	Text_t356221433 * ____falseText_5;
	// UnityEngine.Sprite Listening2SelectableItem::_onSprite
	Sprite_t309593783 * ____onSprite_6;
	// UnityEngine.Sprite Listening2SelectableItem::_offSprite
	Sprite_t309593783 * ____offSprite_7;
	// UnityEngine.UI.Text Listening2SelectableItem::_text
	Text_t356221433 * ____text_8;
	// System.Boolean Listening2SelectableItem::_selected
	bool ____selected_9;
	// UnityEngine.UI.Image Listening2SelectableItem::_frameLeft
	Image_t2042527209 * ____frameLeft_10;
	// UnityEngine.UI.Image Listening2SelectableItem::_framRight
	Image_t2042527209 * ____framRight_11;
	// UnityEngine.Events.UnityEvent Listening2SelectableItem::onSelectItemClicked
	UnityEvent_t408735097 * ___onSelectItemClicked_12;

public:
	inline static int32_t get_offset_of__trueBg_2() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____trueBg_2)); }
	inline Image_t2042527209 * get__trueBg_2() const { return ____trueBg_2; }
	inline Image_t2042527209 ** get_address_of__trueBg_2() { return &____trueBg_2; }
	inline void set__trueBg_2(Image_t2042527209 * value)
	{
		____trueBg_2 = value;
		Il2CppCodeGenWriteBarrier(&____trueBg_2, value);
	}

	inline static int32_t get_offset_of__trueText_3() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____trueText_3)); }
	inline Text_t356221433 * get__trueText_3() const { return ____trueText_3; }
	inline Text_t356221433 ** get_address_of__trueText_3() { return &____trueText_3; }
	inline void set__trueText_3(Text_t356221433 * value)
	{
		____trueText_3 = value;
		Il2CppCodeGenWriteBarrier(&____trueText_3, value);
	}

	inline static int32_t get_offset_of__falseBg_4() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____falseBg_4)); }
	inline Image_t2042527209 * get__falseBg_4() const { return ____falseBg_4; }
	inline Image_t2042527209 ** get_address_of__falseBg_4() { return &____falseBg_4; }
	inline void set__falseBg_4(Image_t2042527209 * value)
	{
		____falseBg_4 = value;
		Il2CppCodeGenWriteBarrier(&____falseBg_4, value);
	}

	inline static int32_t get_offset_of__falseText_5() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____falseText_5)); }
	inline Text_t356221433 * get__falseText_5() const { return ____falseText_5; }
	inline Text_t356221433 ** get_address_of__falseText_5() { return &____falseText_5; }
	inline void set__falseText_5(Text_t356221433 * value)
	{
		____falseText_5 = value;
		Il2CppCodeGenWriteBarrier(&____falseText_5, value);
	}

	inline static int32_t get_offset_of__onSprite_6() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____onSprite_6)); }
	inline Sprite_t309593783 * get__onSprite_6() const { return ____onSprite_6; }
	inline Sprite_t309593783 ** get_address_of__onSprite_6() { return &____onSprite_6; }
	inline void set__onSprite_6(Sprite_t309593783 * value)
	{
		____onSprite_6 = value;
		Il2CppCodeGenWriteBarrier(&____onSprite_6, value);
	}

	inline static int32_t get_offset_of__offSprite_7() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____offSprite_7)); }
	inline Sprite_t309593783 * get__offSprite_7() const { return ____offSprite_7; }
	inline Sprite_t309593783 ** get_address_of__offSprite_7() { return &____offSprite_7; }
	inline void set__offSprite_7(Sprite_t309593783 * value)
	{
		____offSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&____offSprite_7, value);
	}

	inline static int32_t get_offset_of__text_8() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____text_8)); }
	inline Text_t356221433 * get__text_8() const { return ____text_8; }
	inline Text_t356221433 ** get_address_of__text_8() { return &____text_8; }
	inline void set__text_8(Text_t356221433 * value)
	{
		____text_8 = value;
		Il2CppCodeGenWriteBarrier(&____text_8, value);
	}

	inline static int32_t get_offset_of__selected_9() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____selected_9)); }
	inline bool get__selected_9() const { return ____selected_9; }
	inline bool* get_address_of__selected_9() { return &____selected_9; }
	inline void set__selected_9(bool value)
	{
		____selected_9 = value;
	}

	inline static int32_t get_offset_of__frameLeft_10() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____frameLeft_10)); }
	inline Image_t2042527209 * get__frameLeft_10() const { return ____frameLeft_10; }
	inline Image_t2042527209 ** get_address_of__frameLeft_10() { return &____frameLeft_10; }
	inline void set__frameLeft_10(Image_t2042527209 * value)
	{
		____frameLeft_10 = value;
		Il2CppCodeGenWriteBarrier(&____frameLeft_10, value);
	}

	inline static int32_t get_offset_of__framRight_11() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ____framRight_11)); }
	inline Image_t2042527209 * get__framRight_11() const { return ____framRight_11; }
	inline Image_t2042527209 ** get_address_of__framRight_11() { return &____framRight_11; }
	inline void set__framRight_11(Image_t2042527209 * value)
	{
		____framRight_11 = value;
		Il2CppCodeGenWriteBarrier(&____framRight_11, value);
	}

	inline static int32_t get_offset_of_onSelectItemClicked_12() { return static_cast<int32_t>(offsetof(Listening2SelectableItem_t492261174, ___onSelectItemClicked_12)); }
	inline UnityEvent_t408735097 * get_onSelectItemClicked_12() const { return ___onSelectItemClicked_12; }
	inline UnityEvent_t408735097 ** get_address_of_onSelectItemClicked_12() { return &___onSelectItemClicked_12; }
	inline void set_onSelectItemClicked_12(UnityEvent_t408735097 * value)
	{
		___onSelectItemClicked_12 = value;
		Il2CppCodeGenWriteBarrier(&___onSelectItemClicked_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
