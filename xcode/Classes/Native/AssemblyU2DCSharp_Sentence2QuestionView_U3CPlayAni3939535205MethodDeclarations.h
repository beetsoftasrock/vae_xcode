﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence2QuestionView/<PlayAnimation>c__Iterator1
struct U3CPlayAnimationU3Ec__Iterator1_t3939535205;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Sentence2QuestionView/<PlayAnimation>c__Iterator1::.ctor()
extern "C"  void U3CPlayAnimationU3Ec__Iterator1__ctor_m1749646692 (U3CPlayAnimationU3Ec__Iterator1_t3939535205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Sentence2QuestionView/<PlayAnimation>c__Iterator1::MoveNext()
extern "C"  bool U3CPlayAnimationU3Ec__Iterator1_MoveNext_m1970811328 (U3CPlayAnimationU3Ec__Iterator1_t3939535205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Sentence2QuestionView/<PlayAnimation>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayAnimationU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2377743190 (U3CPlayAnimationU3Ec__Iterator1_t3939535205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Sentence2QuestionView/<PlayAnimation>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayAnimationU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1181355246 (U3CPlayAnimationU3Ec__Iterator1_t3939535205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView/<PlayAnimation>c__Iterator1::Dispose()
extern "C"  void U3CPlayAnimationU3Ec__Iterator1_Dispose_m1469747279 (U3CPlayAnimationU3Ec__Iterator1_t3939535205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView/<PlayAnimation>c__Iterator1::Reset()
extern "C"  void U3CPlayAnimationU3Ec__Iterator1_Reset_m4182130545 (U3CPlayAnimationU3Ec__Iterator1_t3939535205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
