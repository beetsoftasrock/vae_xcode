﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DialogPanel
struct DialogPanel_t1568014038;
// HistoryConversation
struct HistoryConversation_t3119154081;
// System.Collections.Generic.List`1<Dialog>
struct List_1_t747313864;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogController
struct  DialogController_t3845653284  : public MonoBehaviour_t1158329972
{
public:
	// DialogPanel DialogController::me_talk_dialog
	DialogPanel_t1568014038 * ___me_talk_dialog_2;
	// DialogPanel DialogController::end_record_dialog
	DialogPanel_t1568014038 * ___end_record_dialog_3;
	// DialogPanel DialogController::you_talk_sound_dialog
	DialogPanel_t1568014038 * ___you_talk_sound_dialog_4;
	// DialogPanel DialogController::you_talk_mini_dialog
	DialogPanel_t1568014038 * ___you_talk_mini_dialog_5;
	// HistoryConversation DialogController::historyConversation
	HistoryConversation_t3119154081 * ___historyConversation_6;
	// System.Boolean DialogController::showSubYouDialog
	bool ___showSubYouDialog_7;
	// System.Collections.Generic.List`1<Dialog> DialogController::dialogs
	List_1_t747313864 * ___dialogs_8;
	// System.Collections.Generic.List`1<Dialog> DialogController::dialogUser
	List_1_t747313864 * ___dialogUser_9;
	// System.Collections.Generic.List`1<Dialog> DialogController::dialogYou
	List_1_t747313864 * ___dialogYou_10;
	// DialogPanel DialogController::currentDialog
	DialogPanel_t1568014038 * ___currentDialog_14;

public:
	inline static int32_t get_offset_of_me_talk_dialog_2() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___me_talk_dialog_2)); }
	inline DialogPanel_t1568014038 * get_me_talk_dialog_2() const { return ___me_talk_dialog_2; }
	inline DialogPanel_t1568014038 ** get_address_of_me_talk_dialog_2() { return &___me_talk_dialog_2; }
	inline void set_me_talk_dialog_2(DialogPanel_t1568014038 * value)
	{
		___me_talk_dialog_2 = value;
		Il2CppCodeGenWriteBarrier(&___me_talk_dialog_2, value);
	}

	inline static int32_t get_offset_of_end_record_dialog_3() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___end_record_dialog_3)); }
	inline DialogPanel_t1568014038 * get_end_record_dialog_3() const { return ___end_record_dialog_3; }
	inline DialogPanel_t1568014038 ** get_address_of_end_record_dialog_3() { return &___end_record_dialog_3; }
	inline void set_end_record_dialog_3(DialogPanel_t1568014038 * value)
	{
		___end_record_dialog_3 = value;
		Il2CppCodeGenWriteBarrier(&___end_record_dialog_3, value);
	}

	inline static int32_t get_offset_of_you_talk_sound_dialog_4() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___you_talk_sound_dialog_4)); }
	inline DialogPanel_t1568014038 * get_you_talk_sound_dialog_4() const { return ___you_talk_sound_dialog_4; }
	inline DialogPanel_t1568014038 ** get_address_of_you_talk_sound_dialog_4() { return &___you_talk_sound_dialog_4; }
	inline void set_you_talk_sound_dialog_4(DialogPanel_t1568014038 * value)
	{
		___you_talk_sound_dialog_4 = value;
		Il2CppCodeGenWriteBarrier(&___you_talk_sound_dialog_4, value);
	}

	inline static int32_t get_offset_of_you_talk_mini_dialog_5() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___you_talk_mini_dialog_5)); }
	inline DialogPanel_t1568014038 * get_you_talk_mini_dialog_5() const { return ___you_talk_mini_dialog_5; }
	inline DialogPanel_t1568014038 ** get_address_of_you_talk_mini_dialog_5() { return &___you_talk_mini_dialog_5; }
	inline void set_you_talk_mini_dialog_5(DialogPanel_t1568014038 * value)
	{
		___you_talk_mini_dialog_5 = value;
		Il2CppCodeGenWriteBarrier(&___you_talk_mini_dialog_5, value);
	}

	inline static int32_t get_offset_of_historyConversation_6() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___historyConversation_6)); }
	inline HistoryConversation_t3119154081 * get_historyConversation_6() const { return ___historyConversation_6; }
	inline HistoryConversation_t3119154081 ** get_address_of_historyConversation_6() { return &___historyConversation_6; }
	inline void set_historyConversation_6(HistoryConversation_t3119154081 * value)
	{
		___historyConversation_6 = value;
		Il2CppCodeGenWriteBarrier(&___historyConversation_6, value);
	}

	inline static int32_t get_offset_of_showSubYouDialog_7() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___showSubYouDialog_7)); }
	inline bool get_showSubYouDialog_7() const { return ___showSubYouDialog_7; }
	inline bool* get_address_of_showSubYouDialog_7() { return &___showSubYouDialog_7; }
	inline void set_showSubYouDialog_7(bool value)
	{
		___showSubYouDialog_7 = value;
	}

	inline static int32_t get_offset_of_dialogs_8() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___dialogs_8)); }
	inline List_1_t747313864 * get_dialogs_8() const { return ___dialogs_8; }
	inline List_1_t747313864 ** get_address_of_dialogs_8() { return &___dialogs_8; }
	inline void set_dialogs_8(List_1_t747313864 * value)
	{
		___dialogs_8 = value;
		Il2CppCodeGenWriteBarrier(&___dialogs_8, value);
	}

	inline static int32_t get_offset_of_dialogUser_9() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___dialogUser_9)); }
	inline List_1_t747313864 * get_dialogUser_9() const { return ___dialogUser_9; }
	inline List_1_t747313864 ** get_address_of_dialogUser_9() { return &___dialogUser_9; }
	inline void set_dialogUser_9(List_1_t747313864 * value)
	{
		___dialogUser_9 = value;
		Il2CppCodeGenWriteBarrier(&___dialogUser_9, value);
	}

	inline static int32_t get_offset_of_dialogYou_10() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___dialogYou_10)); }
	inline List_1_t747313864 * get_dialogYou_10() const { return ___dialogYou_10; }
	inline List_1_t747313864 ** get_address_of_dialogYou_10() { return &___dialogYou_10; }
	inline void set_dialogYou_10(List_1_t747313864 * value)
	{
		___dialogYou_10 = value;
		Il2CppCodeGenWriteBarrier(&___dialogYou_10, value);
	}

	inline static int32_t get_offset_of_currentDialog_14() { return static_cast<int32_t>(offsetof(DialogController_t3845653284, ___currentDialog_14)); }
	inline DialogPanel_t1568014038 * get_currentDialog_14() const { return ___currentDialog_14; }
	inline DialogPanel_t1568014038 ** get_address_of_currentDialog_14() { return &___currentDialog_14; }
	inline void set_currentDialog_14(DialogPanel_t1568014038 * value)
	{
		___currentDialog_14 = value;
		Il2CppCodeGenWriteBarrier(&___currentDialog_14, value);
	}
};

struct DialogController_t3845653284_StaticFields
{
public:
	// System.Int32 DialogController::indexDialog
	int32_t ___indexDialog_11;
	// System.Int32 DialogController::indexYou
	int32_t ___indexYou_12;
	// System.Int32 DialogController::indexUser
	int32_t ___indexUser_13;
	// System.String DialogController::content
	String_t* ___content_15;
	// System.String DialogController::subContent
	String_t* ___subContent_16;
	// System.String DialogController::voice
	String_t* ___voice_17;
	// System.Int32 DialogController::indexItemHistory
	int32_t ___indexItemHistory_18;
	// System.Int32 DialogController::startIndex
	int32_t ___startIndex_19;
	// System.Boolean DialogController::isAutoPlay
	bool ___isAutoPlay_20;
	// System.Boolean DialogController::isAutoPlayTemplate
	bool ___isAutoPlayTemplate_21;

public:
	inline static int32_t get_offset_of_indexDialog_11() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___indexDialog_11)); }
	inline int32_t get_indexDialog_11() const { return ___indexDialog_11; }
	inline int32_t* get_address_of_indexDialog_11() { return &___indexDialog_11; }
	inline void set_indexDialog_11(int32_t value)
	{
		___indexDialog_11 = value;
	}

	inline static int32_t get_offset_of_indexYou_12() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___indexYou_12)); }
	inline int32_t get_indexYou_12() const { return ___indexYou_12; }
	inline int32_t* get_address_of_indexYou_12() { return &___indexYou_12; }
	inline void set_indexYou_12(int32_t value)
	{
		___indexYou_12 = value;
	}

	inline static int32_t get_offset_of_indexUser_13() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___indexUser_13)); }
	inline int32_t get_indexUser_13() const { return ___indexUser_13; }
	inline int32_t* get_address_of_indexUser_13() { return &___indexUser_13; }
	inline void set_indexUser_13(int32_t value)
	{
		___indexUser_13 = value;
	}

	inline static int32_t get_offset_of_content_15() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___content_15)); }
	inline String_t* get_content_15() const { return ___content_15; }
	inline String_t** get_address_of_content_15() { return &___content_15; }
	inline void set_content_15(String_t* value)
	{
		___content_15 = value;
		Il2CppCodeGenWriteBarrier(&___content_15, value);
	}

	inline static int32_t get_offset_of_subContent_16() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___subContent_16)); }
	inline String_t* get_subContent_16() const { return ___subContent_16; }
	inline String_t** get_address_of_subContent_16() { return &___subContent_16; }
	inline void set_subContent_16(String_t* value)
	{
		___subContent_16 = value;
		Il2CppCodeGenWriteBarrier(&___subContent_16, value);
	}

	inline static int32_t get_offset_of_voice_17() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___voice_17)); }
	inline String_t* get_voice_17() const { return ___voice_17; }
	inline String_t** get_address_of_voice_17() { return &___voice_17; }
	inline void set_voice_17(String_t* value)
	{
		___voice_17 = value;
		Il2CppCodeGenWriteBarrier(&___voice_17, value);
	}

	inline static int32_t get_offset_of_indexItemHistory_18() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___indexItemHistory_18)); }
	inline int32_t get_indexItemHistory_18() const { return ___indexItemHistory_18; }
	inline int32_t* get_address_of_indexItemHistory_18() { return &___indexItemHistory_18; }
	inline void set_indexItemHistory_18(int32_t value)
	{
		___indexItemHistory_18 = value;
	}

	inline static int32_t get_offset_of_startIndex_19() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___startIndex_19)); }
	inline int32_t get_startIndex_19() const { return ___startIndex_19; }
	inline int32_t* get_address_of_startIndex_19() { return &___startIndex_19; }
	inline void set_startIndex_19(int32_t value)
	{
		___startIndex_19 = value;
	}

	inline static int32_t get_offset_of_isAutoPlay_20() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___isAutoPlay_20)); }
	inline bool get_isAutoPlay_20() const { return ___isAutoPlay_20; }
	inline bool* get_address_of_isAutoPlay_20() { return &___isAutoPlay_20; }
	inline void set_isAutoPlay_20(bool value)
	{
		___isAutoPlay_20 = value;
	}

	inline static int32_t get_offset_of_isAutoPlayTemplate_21() { return static_cast<int32_t>(offsetof(DialogController_t3845653284_StaticFields, ___isAutoPlayTemplate_21)); }
	inline bool get_isAutoPlayTemplate_21() const { return ___isAutoPlayTemplate_21; }
	inline bool* get_address_of_isAutoPlayTemplate_21() { return &___isAutoPlayTemplate_21; }
	inline void set_isAutoPlayTemplate_21(bool value)
	{
		___isAutoPlayTemplate_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
