﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2SceneController/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t2772662239;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening2SceneController/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m1019705560 (U3CStartU3Ec__Iterator0_t2772662239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening2SceneController/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m161222372 (U3CStartU3Ec__Iterator0_t2772662239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Listening2SceneController/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3656663482 (U3CStartU3Ec__Iterator0_t2772662239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Listening2SceneController/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2107748130 (U3CStartU3Ec__Iterator0_t2772662239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m516836921 (U3CStartU3Ec__Iterator0_t2772662239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1923114251 (U3CStartU3Ec__Iterator0_t2772662239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
