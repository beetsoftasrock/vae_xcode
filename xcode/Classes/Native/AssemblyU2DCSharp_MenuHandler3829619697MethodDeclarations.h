﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuHandler
struct MenuHandler_t3829619697;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuHandler::.ctor()
extern "C"  void MenuHandler__ctor_m1337626732 (MenuHandler_t3829619697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuHandler::HideMenu()
extern "C"  void MenuHandler_HideMenu_m2928093211 (MenuHandler_t3829619697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuHandler::ShowMenu()
extern "C"  void MenuHandler_ShowMenu_m2774339068 (MenuHandler_t3829619697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MenuHandler::DoAppear()
extern "C"  Il2CppObject * MenuHandler_DoAppear_m838354990 (MenuHandler_t3829619697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MenuHandler::DoFade()
extern "C"  Il2CppObject * MenuHandler_DoFade_m143639787 (MenuHandler_t3829619697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
