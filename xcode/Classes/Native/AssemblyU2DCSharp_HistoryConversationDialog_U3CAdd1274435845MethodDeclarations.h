﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryConversationDialog/<AddConversationSentence>c__Iterator1
struct U3CAddConversationSentenceU3Ec__Iterator1_t1274435845;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HistoryConversationDialog/<AddConversationSentence>c__Iterator1::.ctor()
extern "C"  void U3CAddConversationSentenceU3Ec__Iterator1__ctor_m3497587704 (U3CAddConversationSentenceU3Ec__Iterator1_t1274435845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HistoryConversationDialog/<AddConversationSentence>c__Iterator1::MoveNext()
extern "C"  bool U3CAddConversationSentenceU3Ec__Iterator1_MoveNext_m810244280 (U3CAddConversationSentenceU3Ec__Iterator1_t1274435845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryConversationDialog/<AddConversationSentence>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAddConversationSentenceU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2614127062 (U3CAddConversationSentenceU3Ec__Iterator1_t1274435845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryConversationDialog/<AddConversationSentence>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAddConversationSentenceU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2539953534 (U3CAddConversationSentenceU3Ec__Iterator1_t1274435845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog/<AddConversationSentence>c__Iterator1::Dispose()
extern "C"  void U3CAddConversationSentenceU3Ec__Iterator1_Dispose_m963496951 (U3CAddConversationSentenceU3Ec__Iterator1_t1274435845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog/<AddConversationSentence>c__Iterator1::Reset()
extern "C"  void U3CAddConversationSentenceU3Ec__Iterator1_Reset_m2475822981 (U3CAddConversationSentenceU3Ec__Iterator1_t1274435845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
