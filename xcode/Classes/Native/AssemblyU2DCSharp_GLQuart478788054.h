﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLQuart
struct  GLQuart_t478788054  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material GLQuart::mat
	Material_t193706927 * ___mat_2;

public:
	inline static int32_t get_offset_of_mat_2() { return static_cast<int32_t>(offsetof(GLQuart_t478788054, ___mat_2)); }
	inline Material_t193706927 * get_mat_2() const { return ___mat_2; }
	inline Material_t193706927 ** get_address_of_mat_2() { return &___mat_2; }
	inline void set_mat_2(Material_t193706927 * value)
	{
		___mat_2 = value;
		Il2CppCodeGenWriteBarrier(&___mat_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
