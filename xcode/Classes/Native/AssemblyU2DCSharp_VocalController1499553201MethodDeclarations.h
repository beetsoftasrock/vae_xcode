﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VocalController
struct VocalController_t1499553201;
// VocabularyOnplayQuestion
struct VocabularyOnplayQuestion_t3519225043;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VocabularyOnplayQuestion3519225043.h"

// System.Void VocalController::.ctor()
extern "C"  void VocalController__ctor_m2347159724 (VocalController_t1499553201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VocabularyOnplayQuestion VocalController::get_currentQuestion()
extern "C"  VocabularyOnplayQuestion_t3519225043 * VocalController_get_currentQuestion_m519594104 (VocalController_t1499553201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController::set_currentQuestion(VocabularyOnplayQuestion)
extern "C"  void VocalController_set_currentQuestion_m3414809267 (VocalController_t1499553201 * __this, VocabularyOnplayQuestion_t3519225043 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VocalController::Start()
extern "C"  Il2CppObject * VocalController_Start_m606645016 (VocalController_t1499553201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController::StartPlay()
extern "C"  void VocalController_StartPlay_m3789342302 (VocalController_t1499553201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController::InitData()
extern "C"  void VocalController_InitData_m134891448 (VocalController_t1499553201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController::NextQuestion()
extern "C"  void VocalController_NextQuestion_m3813240449 (VocalController_t1499553201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController::ShowResult()
extern "C"  void VocalController_ShowResult_m1902975730 (VocalController_t1499553201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController::BackToChapterTop()
extern "C"  void VocalController_BackToChapterTop_m747475280 (VocalController_t1499553201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController::<ShowResult>m__0()
extern "C"  void VocalController_U3CShowResultU3Em__0_m2640412333 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController::<ShowResult>m__1()
extern "C"  void VocalController_U3CShowResultU3Em__1_m2640412428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
