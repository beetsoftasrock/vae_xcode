﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApiConnector/<DoRequestApi>c__Iterator0
struct U3CDoRequestApiU3Ec__Iterator0_t2897658463;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ApiConnector/<DoRequestApi>c__Iterator0::.ctor()
extern "C"  void U3CDoRequestApiU3Ec__Iterator0__ctor_m3625977034 (U3CDoRequestApiU3Ec__Iterator0_t2897658463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ApiConnector/<DoRequestApi>c__Iterator0::MoveNext()
extern "C"  bool U3CDoRequestApiU3Ec__Iterator0_MoveNext_m3866574 (U3CDoRequestApiU3Ec__Iterator0_t2897658463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ApiConnector/<DoRequestApi>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoRequestApiU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m321657104 (U3CDoRequestApiU3Ec__Iterator0_t2897658463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ApiConnector/<DoRequestApi>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoRequestApiU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2631242024 (U3CDoRequestApiU3Ec__Iterator0_t2897658463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApiConnector/<DoRequestApi>c__Iterator0::Dispose()
extern "C"  void U3CDoRequestApiU3Ec__Iterator0_Dispose_m1916458585 (U3CDoRequestApiU3Ec__Iterator0_t2897658463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApiConnector/<DoRequestApi>c__Iterator0::Reset()
extern "C"  void U3CDoRequestApiU3Ec__Iterator0_Reset_m2484279175 (U3CDoRequestApiU3Ec__Iterator0_t2897658463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
