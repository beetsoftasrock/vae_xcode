﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneBGMSetting
struct SceneBGMSetting_t3493794458;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneBGMSetting::.ctor()
extern "C"  void SceneBGMSetting__ctor_m2629013723 (SceneBGMSetting_t3493794458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneBGMSetting::OnLevelWasLoaded(System.Int32)
extern "C"  void SceneBGMSetting_OnLevelWasLoaded_m3598222587 (SceneBGMSetting_t3493794458 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneBGMSetting::.cctor()
extern "C"  void SceneBGMSetting__cctor_m718289136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
