﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.String
struct String_t;
// ApiConnector
struct ApiConnector_t2569785041;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiConnector/<RequestApi>c__AnonStorey1`1<System.Object>
struct  U3CRequestApiU3Ec__AnonStorey1_1_t4122123012  : public Il2CppObject
{
public:
	// System.Action`1<T> ApiConnector/<RequestApi>c__AnonStorey1`1::requestSuccessCallback
	Action_1_t2491248677 * ___requestSuccessCallback_0;
	// System.String ApiConnector/<RequestApi>c__AnonStorey1`1::url
	String_t* ___url_1;
	// ApiConnector ApiConnector/<RequestApi>c__AnonStorey1`1::$this
	ApiConnector_t2569785041 * ___U24this_2;

public:
	inline static int32_t get_offset_of_requestSuccessCallback_0() { return static_cast<int32_t>(offsetof(U3CRequestApiU3Ec__AnonStorey1_1_t4122123012, ___requestSuccessCallback_0)); }
	inline Action_1_t2491248677 * get_requestSuccessCallback_0() const { return ___requestSuccessCallback_0; }
	inline Action_1_t2491248677 ** get_address_of_requestSuccessCallback_0() { return &___requestSuccessCallback_0; }
	inline void set_requestSuccessCallback_0(Action_1_t2491248677 * value)
	{
		___requestSuccessCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___requestSuccessCallback_0, value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(U3CRequestApiU3Ec__AnonStorey1_1_t4122123012, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier(&___url_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRequestApiU3Ec__AnonStorey1_1_t4122123012, ___U24this_2)); }
	inline ApiConnector_t2569785041 * get_U24this_2() const { return ___U24this_2; }
	inline ApiConnector_t2569785041 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ApiConnector_t2569785041 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
