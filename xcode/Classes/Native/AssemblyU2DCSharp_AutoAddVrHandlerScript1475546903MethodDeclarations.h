﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoAddVrHandlerScript
struct AutoAddVrHandlerScript_t1475546903;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoAddVrHandlerScript::.ctor()
extern "C"  void AutoAddVrHandlerScript__ctor_m818372264 (AutoAddVrHandlerScript_t1475546903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoAddVrHandlerScript::AddVRHandlerScriptToAllButton()
extern "C"  void AutoAddVrHandlerScript_AddVRHandlerScriptToAllButton_m1344711814 (AutoAddVrHandlerScript_t1475546903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
