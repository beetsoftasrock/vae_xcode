﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.AppleReceiptParser
struct AppleReceiptParser_t732159403;
// UnityEngine.Purchasing.Security.AppleReceipt
struct AppleReceipt_t3991411794;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Purchasing.Security.PKCS7
struct PKCS7_t1974940522;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct AppleInAppPurchaseReceipt_t3271698749;

#include "codegen/il2cpp-codegen.h"
#include "Security_UnityEngine_Purchasing_Security_PKCS71974940522.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void UnityEngine.Purchasing.Security.AppleReceiptParser::.ctor()
extern "C"  void AppleReceiptParser__ctor_m323628125 (AppleReceiptParser_t732159403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleReceiptParser::Parse(System.Byte[])
extern "C"  AppleReceipt_t3991411794 * AppleReceiptParser_Parse_m2211912591 (AppleReceiptParser_t732159403 * __this, ByteU5BU5D_t3397334013* ___receiptData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleReceiptParser::Parse(System.Byte[],UnityEngine.Purchasing.Security.PKCS7&)
extern "C"  AppleReceipt_t3991411794 * AppleReceiptParser_Parse_m575758582 (AppleReceiptParser_t732159403 * __this, ByteU5BU5D_t3397334013* ___receiptData0, PKCS7_t1974940522 ** ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleReceiptParser::ParseReceipt(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  AppleReceipt_t3991411794 * AppleReceiptParser_ParseReceipt_m297215418 (AppleReceiptParser_t732159403 * __this, Asn1Node_t1770761751 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt UnityEngine.Purchasing.Security.AppleReceiptParser::ParseInAppReceipt(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  AppleInAppPurchaseReceipt_t3271698749 * AppleReceiptParser_ParseInAppReceipt_m4100374355 (AppleReceiptParser_t732159403 * __this, Asn1Node_t1770761751 * ___inApp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.AppleReceiptParser::TryParseDateTimeNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  DateTime_t693205669  AppleReceiptParser_TryParseDateTimeNode_m4194547979 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
