﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Sentence2QuestionView1942347884.h"
#include "AssemblyU2DCSharp_Sentence2QuestionView_U3CFakeTex2012533373.h"
#include "AssemblyU2DCSharp_Sentence2QuestionView_U3CPlayAni3939535205.h"
#include "AssemblyU2DCSharp_Sentence2SceneController1874814399.h"
#include "AssemblyU2DCSharp_Sentence2SceneController_U3CStar1627489015.h"
#include "AssemblyU2DCSharp_SentenceListeningSpellingOnplayQ2514545327.h"
#include "AssemblyU2DCSharp_SentenceListeningSpellingQuestio3011907248.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_SoundManager4159196980.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_SoundManager_U3CLoa1073847543.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_SoundResult4177104648.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_ManagerInstance2189493790.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_ManagerInstance_U3C1896239954.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_ManagerInstance_U3CS203410663.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_ManagerInstance_U3C3547164181.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_ManagerInstance_U3C1386859499.h"
#include "AssemblyU2DCSharp_com_beetsoft_utility_old_FakeSou3415940987.h"
#include "AssemblyU2DCSharp_com_beetsoft_utility_old_SoundMan947862142.h"
#include "AssemblyU2DCSharp_com_beetsoft_utility_old_SoundMan695211022.h"
#include "AssemblyU2DCSharp_com_beetsoft_utility_old_SoundMan341311129.h"
#include "AssemblyU2DCSharp_com_beetsoft_utility_SoundManage1001218626.h"
#include "AssemblyU2DCSharp_com_beetsoft_utility_SoundManager552765882.h"
#include "AssemblyU2DCSharp_LoadChapterScript2140695782.h"
#include "AssemblyU2DCSharp_LoadChapterScript_U3CStartU3Ec__A813652928.h"
#include "AssemblyU2DCSharp_CallHelpInSceneScript2709929151.h"
#include "AssemblyU2DCSharp_ChapterTopHelpCaching800566100.h"
#include "AssemblyU2DCSharp_HelpPopupBroadCastEvent2836716150.h"
#include "AssemblyU2DCSharp_HelpPopup3079166541.h"
#include "AssemblyU2DCSharp_HelpPopupIniter3764049838.h"
#include "AssemblyU2DCSharp_ManageATutorial1512139496.h"
#include "AssemblyU2DCSharp_ManageTutorialsASceneScript3167542912.h"
#include "AssemblyU2DCSharp_ManageTutorialsInHome2855635566.h"
#include "AssemblyU2DCSharp_ManageTutorialsInPractice1214376460.h"
#include "AssemblyU2DCSharp_ManagerHelp421995300.h"
#include "AssemblyU2DCSharp_ManagerHelp_U3CLoadSpriteU3Ec__A1128402625.h"
#include "AssemblyU2DCSharp_OpenHelp01ChapterTopScript2491901699.h"
#include "AssemblyU2DCSharp_OpenHelp02HomeScript2955721185.h"
#include "AssemblyU2DCSharp_OpenHelp03ChapterTopScript1354449869.h"
#include "AssemblyU2DCSharp_OpenHelp03StudyNormalScript2716318723.h"
#include "AssemblyU2DCSharp_OpenHelp03StudyNormalScript_U3CD2233182363.h"
#include "AssemblyU2DCSharp_OpenHelpListening2Script1079893715.h"
#include "AssemblyU2DCSharp_OpenHelpMyPageScript1712211883.h"
#include "AssemblyU2DCSharp_OpenHelpScript3207620564.h"
#include "AssemblyU2DCSharp_OpenHelpScript_TrackIndexObject1096184091.h"
#include "AssemblyU2DCSharp_OpenHelpScript_U3CMoveHandU3Ec__Ite3351712.h"
#include "AssemblyU2DCSharp_PageHelpScript1232253699.h"
#include "AssemblyU2DCSharp_TrackShowHelpAtFistTimeScript2844513732.h"
#include "AssemblyU2DCSharp_ButtonSound506092895.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Misc_LessionName3189920656.h"
#include "AssemblyU2DCSharp_ResetScrollBar2208690131.h"
#include "AssemblyU2DCSharp_TermsOfUse532492745.h"
#include "AssemblyU2DCSharp_PopupController681110.h"
#include "AssemblyU2DCSharp_PopupController_Type163767367.h"
#include "AssemblyU2DCSharp_PopupHelperVR3784706002.h"
#include "AssemblyU2DCSharp_SelectButton132497280.h"
#include "AssemblyU2DCSharp_SelectController1269418144.h"
#include "AssemblyU2DCSharp_SelectController_SelectedState323245347.h"
#include "AssemblyU2DCSharp_SelectItem2844432199.h"
#include "AssemblyU2DCSharp_SelectionController1038360966.h"
#include "AssemblyU2DCSharp_SelectionItem610427083.h"
#include "AssemblyU2DCSharp_AvoidTopBarIOS3781391330.h"
#include "AssemblyU2DCSharp_BGMController1439355246.h"
#include "AssemblyU2DCSharp_SoundData3344892245.h"
#include "AssemblyU2DCSharp_IOSMicrophoneHelper31973919.h"
#include "AssemblyU2DCSharp_IOSMicrophoneHelper_U3CStartU3Ec3706913335.h"
#include "AssemblyU2DCSharp_SceneBGMSetting3493794458.h"
#include "AssemblyU2DCSharp_SoundHelperIOS3377713620.h"
#include "AssemblyU2DCSharp_TrackingHelpFirstPlayTime2653171715.h"
#include "AssemblyU2DCSharp_DataName4229495749.h"
#include "AssemblyU2DCSharp_DataStorage2091384907.h"
#include "AssemblyU2DCSharp_FadeObject1506616101.h"
#include "AssemblyU2DCSharp_FadeObject_U3CIEFadeU3Ec__Iterat2603370079.h"
#include "AssemblyU2DCSharp_PopupOpener1646050995.h"
#include "AssemblyU2DCSharp_PopupScript535420507.h"
#include "AssemblyU2DCSharp_PopupScript_U3CRunPopupDestroyU3E318910796.h"
#include "AssemblyU2DCSharp_SceneAndScreenHelper3943722385.h"
#include "AssemblyU2DCSharp_SceneController38942716.h"
#include "AssemblyU2DCSharp_SceneController_U3CIEFadeWithTime115250393.h"
#include "AssemblyU2DCSharp_SceneController_U3CLoadSceneU3Ec1985522205.h"
#include "AssemblyU2DCSharp_SceneController_U3CLoadSceneU3Ec1985522208.h"
#include "AssemblyU2DCSharp_ScrollSnapRect671131207.h"
#include "AssemblyU2DCSharp_UIController2029583246.h"
#include "AssemblyU2DCSharp_VAEController2229688832.h"
#include "AssemblyU2DCSharp_VAEController_U3CShowResultU3Ec_2697595828.h"
#include "AssemblyU2DCSharp_AutoAddVrHandlerScript1475546903.h"
#include "AssemblyU2DCSharp_VRCharactorSelectorView1986333981.h"
#include "AssemblyU2DCSharp_VRConversationSelectionView764630138.h"
#include "AssemblyU2DCSharp_VRTutorial3527795282.h"
#include "AssemblyU2DCSharp_VRTutorial_U3CDelayU3Ec__Iterato2881116251.h"
#include "AssemblyU2DCSharp_VRHandleButton3511644738.h"
#include "AssemblyU2DCSharp_VrCameraController2570380955.h"
#include "AssemblyU2DCSharp_VrReticle1471479728.h"
#include "AssemblyU2DCSharp_AnswerSound4040477861.h"
#include "AssemblyU2DCSharp_VocabularyController843712768.h"
#include "AssemblyU2DCSharp_VocabularyDataLoader2031208153.h"
#include "AssemblyU2DCSharp_VocabularyDatasWrapper3354957772.h"
#include "AssemblyU2DCSharp_VocabularyOnplayQuestion3519225043.h"
#include "AssemblyU2DCSharp_VocabularyOnplayQuestion_Selecte3971704390.h"
#include "AssemblyU2DCSharp_VocabularyQuestionData2021074976.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (Sentence2QuestionView_t1942347884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[14] = 
{
	Sentence2QuestionView_t1942347884::get_offset_of_popup_3(),
	Sentence2QuestionView_t1942347884::get_offset_of_inputField_4(),
	Sentence2QuestionView_t1942347884::get_offset_of__questionText_5(),
	Sentence2QuestionView_t1942347884::get_offset_of_hiddenText_6(),
	Sentence2QuestionView_t1942347884::get_offset_of_resultText_7(),
	Sentence2QuestionView_t1942347884::get_offset_of_soundButton_8(),
	Sentence2QuestionView_t1942347884::get_offset_of_summitButton_9(),
	Sentence2QuestionView_t1942347884::get_offset_of_anim_10(),
	Sentence2QuestionView_t1942347884::get_offset_of_answerSound_11(),
	Sentence2QuestionView_t1942347884::get_offset_of__pointerEffectCoroutine_12(),
	Sentence2QuestionView_t1942347884::get_offset_of__mainText_13(),
	Sentence2QuestionView_t1942347884::get_offset_of__hightLight_14(),
	Sentence2QuestionView_t1942347884::get_offset_of_animResultOpen_15(),
	Sentence2QuestionView_t1942347884::get_offset_of_animResultClose_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (U3CFakeTextPointerU3Ec__Iterator0_t2012533373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[6] = 
{
	U3CFakeTextPointerU3Ec__Iterator0_t2012533373::get_offset_of_U3CregU3E__0_0(),
	U3CFakeTextPointerU3Ec__Iterator0_t2012533373::get_offset_of_U3CswitcherU3E__1_1(),
	U3CFakeTextPointerU3Ec__Iterator0_t2012533373::get_offset_of_U24this_2(),
	U3CFakeTextPointerU3Ec__Iterator0_t2012533373::get_offset_of_U24current_3(),
	U3CFakeTextPointerU3Ec__Iterator0_t2012533373::get_offset_of_U24disposing_4(),
	U3CFakeTextPointerU3Ec__Iterator0_t2012533373::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (U3CPlayAnimationU3Ec__Iterator1_t3939535205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[4] = 
{
	U3CPlayAnimationU3Ec__Iterator1_t3939535205::get_offset_of_U24this_0(),
	U3CPlayAnimationU3Ec__Iterator1_t3939535205::get_offset_of_U24current_1(),
	U3CPlayAnimationU3Ec__Iterator1_t3939535205::get_offset_of_U24disposing_2(),
	U3CPlayAnimationU3Ec__Iterator1_t3939535205::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (Sentence2SceneController_t1874814399), -1, sizeof(Sentence2SceneController_t1874814399_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2603[11] = 
{
	Sentence2SceneController_t1874814399::get_offset_of_questionBundleText_2(),
	Sentence2SceneController_t1874814399::get_offset_of_dataLoader_3(),
	Sentence2SceneController_t1874814399::get_offset_of_onplayQuestions_4(),
	Sentence2SceneController_t1874814399::get_offset_of_questionDatas_5(),
	Sentence2SceneController_t1874814399::get_offset_of_questionView_6(),
	Sentence2SceneController_t1874814399::get_offset_of_resultGo_7(),
	Sentence2SceneController_t1874814399::get_offset_of_soundPath_8(),
	Sentence2SceneController_t1874814399::get_offset_of__currentQuestion_9(),
	Sentence2SceneController_t1874814399_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
	Sentence2SceneController_t1874814399_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
	Sentence2SceneController_t1874814399_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (U3CStartU3Ec__Iterator0_t1627489015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[4] = 
{
	U3CStartU3Ec__Iterator0_t1627489015::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1627489015::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1627489015::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1627489015::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (SentenceListeningSpellingOnplayQuestion_t2514545327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[4] = 
{
	SentenceListeningSpellingOnplayQuestion_t2514545327::get_offset_of_soundPath_2(),
	SentenceListeningSpellingOnplayQuestion_t2514545327::get_offset_of_U3CquestionDataU3Ek__BackingField_3(),
	SentenceListeningSpellingOnplayQuestion_t2514545327::get_offset_of__view_4(),
	SentenceListeningSpellingOnplayQuestion_t2514545327::get_offset_of_U3CcurrentAnswerTextU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (SentenceListeningSpellingQuestionData_t3011907248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[3] = 
{
	SentenceListeningSpellingQuestionData_t3011907248::get_offset_of_U3CquestionTextU3Ek__BackingField_0(),
	SentenceListeningSpellingQuestionData_t3011907248::get_offset_of_U3CsoundDataU3Ek__BackingField_1(),
	SentenceListeningSpellingQuestionData_t3011907248::get_offset_of_U3CcorrectAnswerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (SoundManager_t4159196980), -1, sizeof(SoundManager_t4159196980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2607[5] = 
{
	SoundManager_t4159196980_StaticFields::get_offset_of_instance_0(),
	SoundManager_t4159196980_StaticFields::get_offset_of_loadedClips_1(),
	SoundManager_t4159196980_StaticFields::get_offset_of_curSinglePlaying_2(),
	SoundManager_t4159196980_StaticFields::get_offset_of_curBgMusicPlaying_3(),
	SoundManager_t4159196980_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (U3CLoadSoundBundleU3Ec__AnonStorey0_t1073847543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[1] = 
{
	U3CLoadSoundBundleU3Ec__AnonStorey0_t1073847543::get_offset_of_bundleName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (SoundResult_t4177104648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[3] = 
{
	SoundResult_t4177104648::get_offset_of_OnPlay_0(),
	SoundResult_t4177104648::get_offset_of_OnEnd_1(),
	SoundResult_t4177104648::get_offset_of_Clip_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (ManagerInstance_t2189493790), -1, sizeof(ManagerInstance_t2189493790_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2610[2] = 
{
	ManagerInstance_t2189493790::get_offset_of_pooledObjects_2(),
	ManagerInstance_t2189493790_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (U3CIsPlayingU3Ec__AnonStorey2_t1896239954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[1] = 
{
	U3CIsPlayingU3Ec__AnonStorey2_t1896239954::get_offset_of_audioClip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (U3CStopU3Ec__AnonStorey3_t203410663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2612[1] = 
{
	U3CStopU3Ec__AnonStorey3_t203410663::get_offset_of_audioClip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (U3CtrackingPlayU3Ec__Iterator0_t3547164181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[5] = 
{
	U3CtrackingPlayU3Ec__Iterator0_t3547164181::get_offset_of_targetSource_0(),
	U3CtrackingPlayU3Ec__Iterator0_t3547164181::get_offset_of_soundResult_1(),
	U3CtrackingPlayU3Ec__Iterator0_t3547164181::get_offset_of_U24current_2(),
	U3CtrackingPlayU3Ec__Iterator0_t3547164181::get_offset_of_U24disposing_3(),
	U3CtrackingPlayU3Ec__Iterator0_t3547164181::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (U3CstopDelayedU3Ec__Iterator1_t1386859499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[6] = 
{
	U3CstopDelayedU3Ec__Iterator1_t1386859499::get_offset_of_delay_0(),
	U3CstopDelayedU3Ec__Iterator1_t1386859499::get_offset_of_audioClip_1(),
	U3CstopDelayedU3Ec__Iterator1_t1386859499::get_offset_of_U24this_2(),
	U3CstopDelayedU3Ec__Iterator1_t1386859499::get_offset_of_U24current_3(),
	U3CstopDelayedU3Ec__Iterator1_t1386859499::get_offset_of_U24disposing_4(),
	U3CstopDelayedU3Ec__Iterator1_t1386859499::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (FakeSoundManager_t3415940987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (SoundManagerFactory_t947862142), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (SoundManager_t695211022), -1, sizeof(SoundManager_t695211022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2618[4] = 
{
	SoundManager_t695211022::get_offset_of_audioClips_2(),
	SoundManager_t695211022::get_offset_of_audioSources_3(),
	SoundManager_t695211022::get_offset_of_dicSoundCommand_4(),
	SoundManager_t695211022_StaticFields::get_offset_of__instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[8] = 
{
	U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129::get_offset_of_namePath_0(),
	U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129::get_offset_of_U3CpathU3E__0_1(),
	U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129::get_offset_of_U3CwwwU3E__1_2(),
	U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129::get_offset_of_U3CclipU3E__2_3(),
	U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129::get_offset_of_U24this_4(),
	U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129::get_offset_of_U24current_5(),
	U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129::get_offset_of_U24disposing_6(),
	U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (SoundManager_t1001218626), -1, sizeof(SoundManager_t1001218626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2621[2] = 
{
	SoundManager_t1001218626::get_offset_of_globalConfig_2(),
	SoundManager_t1001218626_StaticFields::get_offset_of__instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (BundleSoundName_t552765882)+ sizeof (Il2CppObject), sizeof(BundleSoundName_t552765882_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2622[2] = 
{
	BundleSoundName_t552765882::get_offset_of_bundleName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BundleSoundName_t552765882::get_offset_of_soundName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (LoadChapterScript_t2140695782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[2] = 
{
	LoadChapterScript_t2140695782::get_offset_of_selectVR_2(),
	LoadChapterScript_t2140695782::get_offset_of_selectChapter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (U3CStartU3Ec__AnonStorey0_t813652928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[2] = 
{
	U3CStartU3Ec__AnonStorey0_t813652928::get_offset_of_i_0(),
	U3CStartU3Ec__AnonStorey0_t813652928::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (CallHelpInSceneScript_t2709929151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[2] = 
{
	CallHelpInSceneScript_t2709929151::get_offset_of_prefixNameHelpFor_2(),
	CallHelpInSceneScript_t2709929151::get_offset_of_bundleName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (ChapterTopHelpCaching_t800566100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[2] = 
{
	ChapterTopHelpCaching_t800566100::get_offset_of_selectController_2(),
	ChapterTopHelpCaching_t800566100::get_offset_of__preSelectedState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (HelpPopupBroadCastEvent_t2836716150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (HelpPopup_t3079166541), -1, sizeof(HelpPopup_t3079166541_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2628[5] = 
{
	HelpPopup_t3079166541_StaticFields::get_offset_of_openEventBroadCast_2(),
	HelpPopup_t3079166541::get_offset_of_identify_3(),
	HelpPopup_t3079166541::get_offset_of_currentSprite_4(),
	HelpPopup_t3079166541::get_offset_of_sprites_5(),
	HelpPopup_t3079166541::get_offset_of_mainImage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (HelpPopupIniter_t3764049838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[6] = 
{
	0,
	0,
	0,
	HelpPopupIniter_t3764049838::get_offset_of_helpPopupPrefab_5(),
	HelpPopupIniter_t3764049838::get_offset_of_folderName_6(),
	HelpPopupIniter_t3764049838::get_offset_of_prefix_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (ManageATutorial_t1512139496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[14] = 
{
	ManageATutorial_t1512139496::get_offset_of_onOpenHelpEvent_2(),
	ManageATutorial_t1512139496::get_offset_of_onCloseHelpEvent_3(),
	ManageATutorial_t1512139496::get_offset_of_listOpenHelpScript_4(),
	ManageATutorial_t1512139496::get_offset_of_iconHandContainer_5(),
	ManageATutorial_t1512139496::get_offset_of_trackObjectToOpenHelp_6(),
	ManageATutorial_t1512139496::get_offset_of_keyHelpTutorialName_7(),
	ManageATutorial_t1512139496::get_offset_of_speedMoveIconHand_8(),
	ManageATutorial_t1512139496::get_offset_of_pageHelpScript_9(),
	ManageATutorial_t1512139496::get_offset_of_isResetIconHand_10(),
	ManageATutorial_t1512139496::get_offset_of_currentHelpScript_11(),
	ManageATutorial_t1512139496::get_offset_of_oriPositionIconHand_12(),
	ManageATutorial_t1512139496::get_offset_of_oriEulerIconHand_13(),
	ManageATutorial_t1512139496::get_offset_of_index_14(),
	ManageATutorial_t1512139496::get_offset_of_isCheckTrack_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (ManageTutorialsASceneScript_t3167542912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (ManageTutorialsInHome_t2855635566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (ManageTutorialsInPractice_t1214376460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[4] = 
{
	ManageTutorialsInPractice_t1214376460::get_offset_of_characterSelector_2(),
	ManageTutorialsInPractice_t1214376460::get_offset_of_liManageATutorial_3(),
	ManageTutorialsInPractice_t1214376460::get_offset_of_btnHelp_4(),
	ManageTutorialsInPractice_t1214376460::get_offset_of_selectionDialog_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (ManagerHelp_t421995300), -1, sizeof(ManagerHelp_t421995300_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2634[7] = 
{
	ManagerHelp_t421995300::get_offset_of_isOnHelp_0(),
	ManagerHelp_t421995300::get_offset_of_isLoadFromBundle_1(),
	ManagerHelp_t421995300::get_offset_of_cloneHelp_2(),
	ManagerHelp_t421995300::get_offset_of_indexHelp_3(),
	ManagerHelp_t421995300::get_offset_of_liCurrentSceneHelpSprite_4(),
	ManagerHelp_t421995300_StaticFields::get_offset_of_instance_5(),
	ManagerHelp_t421995300_StaticFields::get_offset_of_prefabHelp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (U3CLoadSpriteU3Ec__AnonStorey0_t1128402625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[1] = 
{
	U3CLoadSpriteU3Ec__AnonStorey0_t1128402625::get_offset_of_prefixName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (OpenHelp01ChapterTopScript_t2491901699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[1] = 
{
	OpenHelp01ChapterTopScript_t2491901699::get_offset_of_selectorControler_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (OpenHelp02HomeScript_t2955721185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[1] = 
{
	OpenHelp02HomeScript_t2955721185::get_offset_of_chapterSelector_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (OpenHelp03ChapterTopScript_t1354449869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[1] = 
{
	OpenHelp03ChapterTopScript_t1354449869::get_offset_of_selectUIRight_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (OpenHelp03StudyNormalScript_t2716318723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (U3CDelayInitOnclickU3Ec__Iterator0_t2233182363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[5] = 
{
	U3CDelayInitOnclickU3Ec__Iterator0_t2233182363::get_offset_of_U24locvar0_0(),
	U3CDelayInitOnclickU3Ec__Iterator0_t2233182363::get_offset_of_U24this_1(),
	U3CDelayInitOnclickU3Ec__Iterator0_t2233182363::get_offset_of_U24current_2(),
	U3CDelayInitOnclickU3Ec__Iterator0_t2233182363::get_offset_of_U24disposing_3(),
	U3CDelayInitOnclickU3Ec__Iterator0_t2233182363::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (OpenHelpListening2Script_t1079893715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[1] = 
{
	OpenHelpListening2Script_t1079893715::get_offset_of_answerGrid_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (OpenHelpMyPageScript_t1712211883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[1] = 
{
	OpenHelpMyPageScript_t1712211883::get_offset_of_Chapter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (OpenHelpScript_t3207620564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[8] = 
{
	OpenHelpScript_t3207620564::get_offset_of_dockHands_2(),
	OpenHelpScript_t3207620564::get_offset_of_listObjectsHighlightFront_3(),
	OpenHelpScript_t3207620564::get_offset_of_listObjectHighlightBehind_4(),
	OpenHelpScript_t3207620564::get_offset_of_nextButtonOverlay_5(),
	OpenHelpScript_t3207620564::get_offset_of_parentObjectsHighlightBeFront_6(),
	OpenHelpScript_t3207620564::get_offset_of_liTrackOriginIndexObjects_7(),
	OpenHelpScript_t3207620564::get_offset_of_isShowCustomButton_8(),
	OpenHelpScript_t3207620564::get_offset_of_U3CManageHelpU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (TrackIndexObject_t1096184091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[2] = 
{
	TrackIndexObject_t1096184091::get_offset_of_index_0(),
	TrackIndexObject_t1096184091::get_offset_of_parent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (U3CMoveHandU3Ec__Iterator0_t3351712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[6] = 
{
	U3CMoveHandU3Ec__Iterator0_t3351712::get_offset_of_iconHand_0(),
	U3CMoveHandU3Ec__Iterator0_t3351712::get_offset_of_dockHand_1(),
	U3CMoveHandU3Ec__Iterator0_t3351712::get_offset_of_U24this_2(),
	U3CMoveHandU3Ec__Iterator0_t3351712::get_offset_of_U24current_3(),
	U3CMoveHandU3Ec__Iterator0_t3351712::get_offset_of_U24disposing_4(),
	U3CMoveHandU3Ec__Iterator0_t3351712::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (PageHelpScript_t1232253699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[2] = 
{
	PageHelpScript_t1232253699::get_offset_of_activeHelp_2(),
	PageHelpScript_t1232253699::get_offset_of_inactiveHelp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (TrackShowHelpAtFistTimeScript_t2844513732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[3] = 
{
	TrackShowHelpAtFistTimeScript_t2844513732::get_offset_of_clc_2(),
	TrackShowHelpAtFistTimeScript_t2844513732::get_offset_of_manageATutorial_3(),
	TrackShowHelpAtFistTimeScript_t2844513732::get_offset_of_resetData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (ButtonSound_t506092895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[2] = 
{
	ButtonSound_t506092895::get_offset_of__attachButton_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (LessionName_t3189920656), -1, sizeof(LessionName_t3189920656_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2649[3] = 
{
	LessionName_t3189920656::get_offset_of_sceneName_2(),
	LessionName_t3189920656::get_offset_of_lessionName_3(),
	LessionName_t3189920656_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (ResetScrollBar_t2208690131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[1] = 
{
	ResetScrollBar_t2208690131::get_offset_of_bar_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (TermsOfUse_t532492745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[3] = 
{
	TermsOfUse_t532492745::get_offset_of_parentRect_2(),
	TermsOfUse_t532492745::get_offset_of_rect_3(),
	TermsOfUse_t532492745::get_offset_of_additionSpacing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (PopupController_t681110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[6] = 
{
	PopupController_t681110::get_offset_of_trueImage_2(),
	PopupController_t681110::get_offset_of_falseImage_3(),
	PopupController_t681110::get_offset_of_Title_4(),
	PopupController_t681110::get_offset_of_OnNextPress_5(),
	PopupController_t681110::get_offset_of_Content_6(),
	PopupController_t681110::get_offset_of_contentUI_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (Type_t163767367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (PopupHelperVR_t3784706002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[2] = 
{
	PopupHelperVR_t3784706002::get_offset_of_cachedTransforms_2(),
	PopupHelperVR_t3784706002::get_offset_of_cachedScales_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (SelectButton_t132497280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[8] = 
{
	SelectButton_t132497280::get_offset_of_text_2(),
	SelectButton_t132497280::get_offset_of__colorTextDisable_3(),
	SelectButton_t132497280::get_offset_of__colorTextEnable_4(),
	SelectButton_t132497280::get_offset_of_image_5(),
	SelectButton_t132497280::get_offset_of__colorImageDisable_6(),
	SelectButton_t132497280::get_offset_of__colorImageEnable_7(),
	SelectButton_t132497280::get_offset_of__mainColor_8(),
	SelectButton_t132497280::get_offset_of__selected_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (SelectController_t1269418144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[6] = 
{
	SelectController_t1269418144::get_offset_of_U3CcurrentSelectedU3Ek__BackingField_2(),
	SelectController_t1269418144::get_offset_of_left_3(),
	SelectController_t1269418144::get_offset_of_right_4(),
	SelectController_t1269418144::get_offset_of_leftSelectUI_5(),
	SelectController_t1269418144::get_offset_of_rightSelectUI_6(),
	SelectController_t1269418144::get_offset_of_selectRight_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (SelectedState_t323245347)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2657[3] = 
{
	SelectedState_t323245347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (SelectItem_t2844432199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[5] = 
{
	SelectItem_t2844432199::get_offset_of_text_2(),
	SelectItem_t2844432199::get_offset_of_offBG_3(),
	SelectItem_t2844432199::get_offset_of_onBG_4(),
	SelectItem_t2844432199::get_offset_of_selected_5(),
	SelectItem_t2844432199::get_offset_of_controller_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (SelectionController_t1038360966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[2] = 
{
	SelectionController_t1038360966::get_offset_of_selectionitems_2(),
	SelectionController_t1038360966::get_offset_of_selectBranchs_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (SelectionItem_t610427083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[2] = 
{
	SelectionItem_t610427083::get_offset_of_text_2(),
	SelectionItem_t610427083::get_offset_of_subText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (AvoidTopBarIOS_t3781391330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[1] = 
{
	AvoidTopBarIOS_t3781391330::get_offset_of_offetY_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (BGMController_t1439355246), -1, sizeof(BGMController_t1439355246_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2662[3] = 
{
	BGMController_t1439355246_StaticFields::get_offset_of__instance_2(),
	BGMController_t1439355246::get_offset_of_audioSource_3(),
	BGMController_t1439355246::get_offset_of__playBGM_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (SoundData_t3344892245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[6] = 
{
	SoundData_t3344892245::get_offset_of_wait_0(),
	SoundData_t3344892245::get_offset_of_commandName_1(),
	SoundData_t3344892245::get_offset_of_soundName_2(),
	SoundData_t3344892245::get_offset_of_volume_3(),
	SoundData_t3344892245::get_offset_of_isLoop_4(),
	SoundData_t3344892245::get_offset_of_position_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (IOSMicrophoneHelper_t31973919), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (U3CStartU3Ec__Iterator0_t3706913335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[4] = 
{
	U3CStartU3Ec__Iterator0_t3706913335::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3706913335::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3706913335::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3706913335::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (SceneBGMSetting_t3493794458), -1, sizeof(SceneBGMSetting_t3493794458_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2666[2] = 
{
	SceneBGMSetting_t3493794458::get_offset_of_bgmController_2(),
	SceneBGMSetting_t3493794458_StaticFields::get_offset_of_playBGMScenes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (SoundHelperIOS_t3377713620), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (TrackingHelpFirstPlayTime_t2653171715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[2] = 
{
	TrackingHelpFirstPlayTime_t2653171715::get_offset_of__helpControl_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (DataName_t4229495749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (DataStorage_t2091384907), -1, sizeof(DataStorage_t2091384907_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2670[2] = 
{
	DataStorage_t2091384907::get_offset_of_globalConfig_2(),
	DataStorage_t2091384907_StaticFields::get_offset_of__instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (FadeObject_t1506616101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[5] = 
{
	FadeObject_t1506616101::get_offset_of_img_2(),
	0,
	FadeObject_t1506616101::get_offset_of_OnStartFade_4(),
	FadeObject_t1506616101::get_offset_of_OnEndFade_5(),
	FadeObject_t1506616101::get_offset_of_coroutineFade_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (U3CIEFadeU3Ec__Iterator0_t2603370079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[7] = 
{
	U3CIEFadeU3Ec__Iterator0_t2603370079::get_offset_of_U3CcurrentTimeU3E__0_0(),
	U3CIEFadeU3Ec__Iterator0_t2603370079::get_offset_of_value_1(),
	U3CIEFadeU3Ec__Iterator0_t2603370079::get_offset_of_targetValue_2(),
	U3CIEFadeU3Ec__Iterator0_t2603370079::get_offset_of_U24this_3(),
	U3CIEFadeU3Ec__Iterator0_t2603370079::get_offset_of_U24current_4(),
	U3CIEFadeU3Ec__Iterator0_t2603370079::get_offset_of_U24disposing_5(),
	U3CIEFadeU3Ec__Iterator0_t2603370079::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (PopupOpener_t1646050995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[3] = 
{
	PopupOpener_t1646050995::get_offset_of_prefabPopup_2(),
	PopupOpener_t1646050995::get_offset_of_canvas_3(),
	PopupOpener_t1646050995::get_offset_of_U3CPopupCloneU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (PopupScript_t535420507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[5] = 
{
	PopupScript_t535420507::get_offset_of_destroyTime_2(),
	PopupScript_t535420507::get_offset_of_overlay_3(),
	PopupScript_t535420507::get_offset_of_canvas_4(),
	PopupScript_t535420507::get_offset_of_overlayClone_5(),
	PopupScript_t535420507::get_offset_of_popupClone_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (U3CRunPopupDestroyU3Ec__Iterator0_t318910796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[4] = 
{
	U3CRunPopupDestroyU3Ec__Iterator0_t318910796::get_offset_of_U24this_0(),
	U3CRunPopupDestroyU3Ec__Iterator0_t318910796::get_offset_of_U24current_1(),
	U3CRunPopupDestroyU3Ec__Iterator0_t318910796::get_offset_of_U24disposing_2(),
	U3CRunPopupDestroyU3Ec__Iterator0_t318910796::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (SceneAndScreenHelper_t3943722385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (SceneController_t38942716), -1, sizeof(SceneController_t38942716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2677[3] = 
{
	SceneController_t38942716::get_offset_of_isLoading_2(),
	SceneController_t38942716::get_offset_of_FadeObject_3(),
	SceneController_t38942716_StaticFields::get_offset_of_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (U3CIEFadeWithTimeU3Ec__Iterator0_t115250393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[5] = 
{
	U3CIEFadeWithTimeU3Ec__Iterator0_t115250393::get_offset_of_time_0(),
	U3CIEFadeWithTimeU3Ec__Iterator0_t115250393::get_offset_of_U24this_1(),
	U3CIEFadeWithTimeU3Ec__Iterator0_t115250393::get_offset_of_U24current_2(),
	U3CIEFadeWithTimeU3Ec__Iterator0_t115250393::get_offset_of_U24disposing_3(),
	U3CIEFadeWithTimeU3Ec__Iterator0_t115250393::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (U3CLoadSceneU3Ec__Iterator1_t1985522205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[6] = 
{
	U3CLoadSceneU3Ec__Iterator1_t1985522205::get_offset_of_SceneName_0(),
	U3CLoadSceneU3Ec__Iterator1_t1985522205::get_offset_of_U3CasyncU3E__0_1(),
	U3CLoadSceneU3Ec__Iterator1_t1985522205::get_offset_of_U24this_2(),
	U3CLoadSceneU3Ec__Iterator1_t1985522205::get_offset_of_U24current_3(),
	U3CLoadSceneU3Ec__Iterator1_t1985522205::get_offset_of_U24disposing_4(),
	U3CLoadSceneU3Ec__Iterator1_t1985522205::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (U3CLoadSceneU3Ec__Iterator2_t1985522208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[6] = 
{
	U3CLoadSceneU3Ec__Iterator2_t1985522208::get_offset_of_indexScene_0(),
	U3CLoadSceneU3Ec__Iterator2_t1985522208::get_offset_of_U3CasyncU3E__0_1(),
	U3CLoadSceneU3Ec__Iterator2_t1985522208::get_offset_of_U24this_2(),
	U3CLoadSceneU3Ec__Iterator2_t1985522208::get_offset_of_U24current_3(),
	U3CLoadSceneU3Ec__Iterator2_t1985522208::get_offset_of_U24disposing_4(),
	U3CLoadSceneU3Ec__Iterator2_t1985522208::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (ScrollSnapRect_t671131207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[29] = 
{
	ScrollSnapRect_t671131207::get_offset_of_startingPage_2(),
	ScrollSnapRect_t671131207::get_offset_of_fastSwipeThresholdTime_3(),
	ScrollSnapRect_t671131207::get_offset_of_fastSwipeThresholdDistance_4(),
	ScrollSnapRect_t671131207::get_offset_of_decelerationRate_5(),
	ScrollSnapRect_t671131207::get_offset_of_prevButton_6(),
	ScrollSnapRect_t671131207::get_offset_of_nextButton_7(),
	ScrollSnapRect_t671131207::get_offset_of_unselectedPage_8(),
	ScrollSnapRect_t671131207::get_offset_of_selectedPage_9(),
	ScrollSnapRect_t671131207::get_offset_of_pageSelectionIcons_10(),
	ScrollSnapRect_t671131207::get_offset_of__fastSwipeThresholdMaxLimit_11(),
	ScrollSnapRect_t671131207::get_offset_of__scrollRectComponent_12(),
	ScrollSnapRect_t671131207::get_offset_of__scrollRectRect_13(),
	ScrollSnapRect_t671131207::get_offset_of__container_14(),
	ScrollSnapRect_t671131207::get_offset_of__horizontal_15(),
	ScrollSnapRect_t671131207::get_offset_of__pageCount_16(),
	ScrollSnapRect_t671131207::get_offset_of__currentPage_17(),
	ScrollSnapRect_t671131207::get_offset_of__lerp_18(),
	ScrollSnapRect_t671131207::get_offset_of__lerpTo_19(),
	ScrollSnapRect_t671131207::get_offset_of__pagePositions_20(),
	ScrollSnapRect_t671131207::get_offset_of__pageTransform_21(),
	ScrollSnapRect_t671131207::get_offset_of__dragging_22(),
	ScrollSnapRect_t671131207::get_offset_of__timeStamp_23(),
	ScrollSnapRect_t671131207::get_offset_of__startPosition_24(),
	ScrollSnapRect_t671131207::get_offset_of__showPageSelection_25(),
	ScrollSnapRect_t671131207::get_offset_of__previousPageSelectionIndex_26(),
	ScrollSnapRect_t671131207::get_offset_of__pageSelectionImages_27(),
	ScrollSnapRect_t671131207::get_offset_of_scaleCurrentPage_28(),
	ScrollSnapRect_t671131207::get_offset_of_scaleOtherPage_29(),
	ScrollSnapRect_t671131207::get_offset_of_speedSmoothScale_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (UIController_t2029583246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (VAEController_t2229688832), -1, sizeof(VAEController_t2229688832_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2683[14] = 
{
	VAEController_t2229688832_StaticFields::get_offset_of_isVR_2(),
	VAEController_t2229688832_StaticFields::get_offset_of_isChangingMode_3(),
	VAEController_t2229688832_StaticFields::get_offset_of_typeChar_4(),
	VAEController_t2229688832_StaticFields::get_offset_of_nameChar_5(),
	VAEController_t2229688832_StaticFields::get_offset_of_VRchar_6(),
	VAEController_t2229688832_StaticFields::get_offset_of_PracticeComand_7(),
	VAEController_t2229688832_StaticFields::get_offset_of_currentIndex_8(),
	VAEController_t2229688832_StaticFields::get_offset_of_count_9(),
	VAEController_t2229688832_StaticFields::get_offset_of_uiController_10(),
	VAEController_t2229688832_StaticFields::get_offset_of_startTime_11(),
	VAEController_t2229688832_StaticFields::get_offset_of_endTime_12(),
	VAEController_t2229688832_StaticFields::get_offset_of_totalTimeRecorded_13(),
	VAEController_t2229688832_StaticFields::get_offset_of_totalTimeSoundTemplate_14(),
	VAEController_t2229688832_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (U3CShowResultU3Ec__AnonStorey0_t2697595828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[1] = 
{
	U3CShowResultU3Ec__AnonStorey0_t2697595828::get_offset_of_nameLesson_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (AutoAddVrHandlerScript_t1475546903), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (VRCharactorSelectorView_t1986333981), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (VRConversationSelectionView_t764630138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[3] = 
{
	VRConversationSelectionView_t764630138::get_offset_of__firstRun_13(),
	VRConversationSelectionView_t764630138::get_offset_of_btnSelectedColor_14(),
	VRConversationSelectionView_t764630138::get_offset_of_btnUnselectedColor_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (VRTutorial_t3527795282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[3] = 
{
	VRTutorial_t3527795282::get_offset_of_popupContructorPlugDevice_2(),
	VRTutorial_t3527795282::get_offset_of_popupBeforeIntoVR_3(),
	VRTutorial_t3527795282::get_offset_of_vRMainArea_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (U3CDelayU3Ec__Iterator0_t2881116251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[4] = 
{
	U3CDelayU3Ec__Iterator0_t2881116251::get_offset_of_U24this_0(),
	U3CDelayU3Ec__Iterator0_t2881116251::get_offset_of_U24current_1(),
	U3CDelayU3Ec__Iterator0_t2881116251::get_offset_of_U24disposing_2(),
	U3CDelayU3Ec__Iterator0_t2881116251::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (VRHandleButton_t3511644738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[3] = 
{
	VRHandleButton_t3511644738::get_offset_of_onPointerEnter_2(),
	VRHandleButton_t3511644738::get_offset_of_onPointerExit_3(),
	VRHandleButton_t3511644738::get_offset_of_isHolding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (VrCameraController_t2570380955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[11] = 
{
	VrCameraController_t2570380955::get_offset_of_txt1_2(),
	VrCameraController_t2570380955::get_offset_of_txt2_3(),
	VrCameraController_t2570380955::get_offset_of_txtMainCamera_4(),
	VrCameraController_t2570380955::get_offset_of_txtDebug_5(),
	VrCameraController_t2570380955::get_offset_of_camera_6(),
	VrCameraController_t2570380955::get_offset_of_limitLeft_7(),
	VrCameraController_t2570380955::get_offset_of_limitRight_8(),
	VrCameraController_t2570380955::get_offset_of_limitUp_9(),
	VrCameraController_t2570380955::get_offset_of_limitDown_10(),
	VrCameraController_t2570380955::get_offset_of_head_11(),
	VrCameraController_t2570380955::get_offset_of_headStart_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (VrReticle_t1471479728), -1, sizeof(VrReticle_t1471479728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2692[14] = 
{
	VrReticle_t1471479728::get_offset_of_timeHold_2(),
	VrReticle_t1471479728_StaticFields::get_offset_of_timeCountdown_3(),
	VrReticle_t1471479728_StaticFields::get_offset_of_tempTimeHold_4(),
	VrReticle_t1471479728_StaticFields::get_offset_of_imgFill_5(),
	VrReticle_t1471479728::get_offset_of_adjustment_6(),
	VrReticle_t1471479728_StaticFields::get_offset_of_isGrow_7(),
	VrReticle_t1471479728::get_offset_of_timeScale_8(),
	VrReticle_t1471479728::get_offset_of_newScale_9(),
	VrReticle_t1471479728::get_offset_of_newSmallScale_10(),
	VrReticle_t1471479728_StaticFields::get_offset_of_originalScale_11(),
	VrReticle_t1471479728_StaticFields::get_offset_of_currentlScale_12(),
	VrReticle_t1471479728_StaticFields::get_offset_of_isZoomIn_13(),
	VrReticle_t1471479728_StaticFields::get_offset_of_currentObject_14(),
	VrReticle_t1471479728_StaticFields::get_offset_of_vrHandleButton_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (AnswerSound_t4040477861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[3] = 
{
	AnswerSound_t4040477861::get_offset_of_correctClip_2(),
	AnswerSound_t4040477861::get_offset_of_incorrectClip_3(),
	AnswerSound_t4040477861::get_offset_of_audioSource_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (VocabularyController_t843712768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[14] = 
{
	VocabularyController_t843712768::get_offset_of_selectItems_2(),
	VocabularyController_t843712768::get_offset_of_vocabularys_3(),
	VocabularyController_t843712768::get_offset_of_vocabComand_4(),
	VocabularyController_t843712768::get_offset_of_index_5(),
	VocabularyController_t843712768::get_offset_of_count_6(),
	VocabularyController_t843712768::get_offset_of_quesion_7(),
	VocabularyController_t843712768::get_offset_of_currentItem_8(),
	VocabularyController_t843712768::get_offset_of_currentAnswer_9(),
	VocabularyController_t843712768::get_offset_of_currentText_10(),
	VocabularyController_t843712768::get_offset_of_currentSubText_11(),
	VocabularyController_t843712768::get_offset_of_vocabVoicePath_12(),
	VocabularyController_t843712768::get_offset_of_sound_13(),
	VocabularyController_t843712768::get_offset_of_resultPopup_14(),
	VocabularyController_t843712768::get_offset_of_OnFinish_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (VocabularyDataLoader_t2031208153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[3] = 
{
	VocabularyDataLoader_t2031208153::get_offset_of_dataSheet_2(),
	VocabularyDataLoader_t2031208153::get_offset_of_U3CdatasWrapperU3Ek__BackingField_3(),
	VocabularyDataLoader_t2031208153::get_offset_of__questionBundleText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (VocabularyDatasWrapper_t3354957772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[1] = 
{
	VocabularyDatasWrapper_t3354957772::get_offset_of_wrappedDatas_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (VocabularyOnplayQuestion_t3519225043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[4] = 
{
	VocabularyOnplayQuestion_t3519225043::get_offset_of_soundPath_2(),
	VocabularyOnplayQuestion_t3519225043::get_offset_of__currentSelected_3(),
	VocabularyOnplayQuestion_t3519225043::get_offset_of__view_4(),
	VocabularyOnplayQuestion_t3519225043::get_offset_of_questionData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (SelectedState_t3971704390)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2698[6] = 
{
	SelectedState_t3971704390::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (VocabularyQuestionData_t2021074976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[7] = 
{
	VocabularyQuestionData_t2021074976::get_offset_of_U3CquestionTextU3Ek__BackingField_0(),
	VocabularyQuestionData_t2021074976::get_offset_of_U3CquestionSoundU3Ek__BackingField_1(),
	VocabularyQuestionData_t2021074976::get_offset_of_U3CanswerText0U3Ek__BackingField_2(),
	VocabularyQuestionData_t2021074976::get_offset_of_U3CanswerText1U3Ek__BackingField_3(),
	VocabularyQuestionData_t2021074976::get_offset_of_U3CanswerText2U3Ek__BackingField_4(),
	VocabularyQuestionData_t2021074976::get_offset_of_U3CanswerText3U3Ek__BackingField_5(),
	VocabularyQuestionData_t2021074976::get_offset_of_U3CcorrectAnswerU3Ek__BackingField_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
