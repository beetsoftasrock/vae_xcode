﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CallHelpInSceneScript
struct CallHelpInSceneScript_t2709929151;

#include "codegen/il2cpp-codegen.h"

// System.Void CallHelpInSceneScript::.ctor()
extern "C"  void CallHelpInSceneScript__ctor_m349144682 (CallHelpInSceneScript_t2709929151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallHelpInSceneScript::Start()
extern "C"  void CallHelpInSceneScript_Start_m1648150534 (CallHelpInSceneScript_t2709929151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallHelpInSceneScript::OpenHelp()
extern "C"  void CallHelpInSceneScript_OpenHelp_m2222173215 (CallHelpInSceneScript_t2709929151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
