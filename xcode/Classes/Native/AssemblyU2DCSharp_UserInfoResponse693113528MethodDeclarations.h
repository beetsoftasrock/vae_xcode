﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserInfoResponse
struct UserInfoResponse_t693113528;

#include "codegen/il2cpp-codegen.h"

// System.Void UserInfoResponse::.ctor()
extern "C"  void UserInfoResponse__ctor_m2074212263 (UserInfoResponse_t693113528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
