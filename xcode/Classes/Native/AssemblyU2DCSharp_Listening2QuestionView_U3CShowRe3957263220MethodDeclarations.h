﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2QuestionView/<ShowResult>c__AnonStorey0
struct U3CShowResultU3Ec__AnonStorey0_t3957263220;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening2QuestionView/<ShowResult>c__AnonStorey0::.ctor()
extern "C"  void U3CShowResultU3Ec__AnonStorey0__ctor_m160601195 (U3CShowResultU3Ec__AnonStorey0_t3957263220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2QuestionView/<ShowResult>c__AnonStorey0::<>m__0()
extern "C"  void U3CShowResultU3Ec__AnonStorey0_U3CU3Em__0_m4075214314 (U3CShowResultU3Ec__AnonStorey0_t3957263220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
