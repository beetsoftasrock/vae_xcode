﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalConfig
struct GlobalConfig_t3080413471;
// NewConversationSceneController
struct NewConversationSceneController_t3024536561;

#include "AssemblyU2DCSharp_ConversationDataSaver1206770280.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSceneDataSaver
struct  ConversationSceneDataSaver_t1575220710  : public ConversationDataSaver_t1206770280
{
public:
	// GlobalConfig ConversationSceneDataSaver::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_2;
	// NewConversationSceneController ConversationSceneDataSaver::newConversationSceneController
	NewConversationSceneController_t3024536561 * ___newConversationSceneController_3;

public:
	inline static int32_t get_offset_of_globalConfig_2() { return static_cast<int32_t>(offsetof(ConversationSceneDataSaver_t1575220710, ___globalConfig_2)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_2() const { return ___globalConfig_2; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_2() { return &___globalConfig_2; }
	inline void set_globalConfig_2(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_2 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_2, value);
	}

	inline static int32_t get_offset_of_newConversationSceneController_3() { return static_cast<int32_t>(offsetof(ConversationSceneDataSaver_t1575220710, ___newConversationSceneController_3)); }
	inline NewConversationSceneController_t3024536561 * get_newConversationSceneController_3() const { return ___newConversationSceneController_3; }
	inline NewConversationSceneController_t3024536561 ** get_address_of_newConversationSceneController_3() { return &___newConversationSceneController_3; }
	inline void set_newConversationSceneController_3(NewConversationSceneController_t3024536561 * value)
	{
		___newConversationSceneController_3 = value;
		Il2CppCodeGenWriteBarrier(&___newConversationSceneController_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
