﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t3033456180;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.RemoteSettings::add_Updated(UnityEngine.RemoteSettings/UpdatedEventHandler)
extern "C"  void RemoteSettings_add_Updated_m337456019 (Il2CppObject * __this /* static, unused */, UpdatedEventHandler_t3033456180 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RemoteSettings::remove_Updated(UnityEngine.RemoteSettings/UpdatedEventHandler)
extern "C"  void RemoteSettings_remove_Updated_m908524342 (Il2CppObject * __this /* static, unused */, UpdatedEventHandler_t3033456180 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RemoteSettings::CallOnUpdate()
extern "C"  void RemoteSettings_CallOnUpdate_m1624968574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RemoteSettings::GetInt(System.String,System.Int32)
extern "C"  int32_t RemoteSettings_GetInt_m494005198 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RemoteSettings::GetInt(System.String)
extern "C"  int32_t RemoteSettings_GetInt_m2058337813 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RemoteSettings::GetFloat(System.String,System.Single)
extern "C"  float RemoteSettings_GetFloat_m2568129227 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RemoteSettings::GetFloat(System.String)
extern "C"  float RemoteSettings_GetFloat_m3325352824 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.RemoteSettings::GetString(System.String,System.String)
extern "C"  String_t* RemoteSettings_GetString_m2124143696 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.RemoteSettings::GetString(System.String)
extern "C"  String_t* RemoteSettings_GetString_m1879132784 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RemoteSettings::GetBool(System.String,System.Boolean)
extern "C"  bool RemoteSettings_GetBool_m2305197237 (Il2CppObject * __this /* static, unused */, String_t* ___key0, bool ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RemoteSettings::GetBool(System.String)
extern "C"  bool RemoteSettings_GetBool_m3999689170 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RemoteSettings::HasKey(System.String)
extern "C"  bool RemoteSettings_HasKey_m4137791587 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
