﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GLQuart
struct GLQuart_t478788054;

#include "codegen/il2cpp-codegen.h"

// System.Void GLQuart::.ctor()
extern "C"  void GLQuart__ctor_m3485017467 (GLQuart_t478788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GLQuart::OnRenderObject()
extern "C"  void GLQuart_OnRenderObject_m887539729 (GLQuart_t478788054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
