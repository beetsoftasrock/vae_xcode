﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableWithTimeAudioName
struct  DisableWithTimeAudioName_t4015415354  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DisableWithTimeAudioName::autoDestroy
	bool ___autoDestroy_2;
	// System.Boolean DisableWithTimeAudioName::autoPlaySoundRecorded
	bool ___autoPlaySoundRecorded_3;
	// System.Boolean DisableWithTimeAudioName::autoPlaySoundTemplate
	bool ___autoPlaySoundTemplate_4;
	// UnityEngine.AudioSource DisableWithTimeAudioName::audioSource
	AudioSource_t1135106623 * ___audioSource_5;
	// System.Single DisableWithTimeAudioName::timeDelayStart
	float ___timeDelayStart_6;
	// System.Single DisableWithTimeAudioName::timeDelayFinish
	float ___timeDelayFinish_7;
	// UnityEngine.Events.UnityEvent DisableWithTimeAudioName::OnFinish
	UnityEvent_t408735097 * ___OnFinish_8;
	// UnityEngine.AudioClip DisableWithTimeAudioName::clipTemplate
	AudioClip_t1932558630 * ___clipTemplate_9;

public:
	inline static int32_t get_offset_of_autoDestroy_2() { return static_cast<int32_t>(offsetof(DisableWithTimeAudioName_t4015415354, ___autoDestroy_2)); }
	inline bool get_autoDestroy_2() const { return ___autoDestroy_2; }
	inline bool* get_address_of_autoDestroy_2() { return &___autoDestroy_2; }
	inline void set_autoDestroy_2(bool value)
	{
		___autoDestroy_2 = value;
	}

	inline static int32_t get_offset_of_autoPlaySoundRecorded_3() { return static_cast<int32_t>(offsetof(DisableWithTimeAudioName_t4015415354, ___autoPlaySoundRecorded_3)); }
	inline bool get_autoPlaySoundRecorded_3() const { return ___autoPlaySoundRecorded_3; }
	inline bool* get_address_of_autoPlaySoundRecorded_3() { return &___autoPlaySoundRecorded_3; }
	inline void set_autoPlaySoundRecorded_3(bool value)
	{
		___autoPlaySoundRecorded_3 = value;
	}

	inline static int32_t get_offset_of_autoPlaySoundTemplate_4() { return static_cast<int32_t>(offsetof(DisableWithTimeAudioName_t4015415354, ___autoPlaySoundTemplate_4)); }
	inline bool get_autoPlaySoundTemplate_4() const { return ___autoPlaySoundTemplate_4; }
	inline bool* get_address_of_autoPlaySoundTemplate_4() { return &___autoPlaySoundTemplate_4; }
	inline void set_autoPlaySoundTemplate_4(bool value)
	{
		___autoPlaySoundTemplate_4 = value;
	}

	inline static int32_t get_offset_of_audioSource_5() { return static_cast<int32_t>(offsetof(DisableWithTimeAudioName_t4015415354, ___audioSource_5)); }
	inline AudioSource_t1135106623 * get_audioSource_5() const { return ___audioSource_5; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_5() { return &___audioSource_5; }
	inline void set_audioSource_5(AudioSource_t1135106623 * value)
	{
		___audioSource_5 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_5, value);
	}

	inline static int32_t get_offset_of_timeDelayStart_6() { return static_cast<int32_t>(offsetof(DisableWithTimeAudioName_t4015415354, ___timeDelayStart_6)); }
	inline float get_timeDelayStart_6() const { return ___timeDelayStart_6; }
	inline float* get_address_of_timeDelayStart_6() { return &___timeDelayStart_6; }
	inline void set_timeDelayStart_6(float value)
	{
		___timeDelayStart_6 = value;
	}

	inline static int32_t get_offset_of_timeDelayFinish_7() { return static_cast<int32_t>(offsetof(DisableWithTimeAudioName_t4015415354, ___timeDelayFinish_7)); }
	inline float get_timeDelayFinish_7() const { return ___timeDelayFinish_7; }
	inline float* get_address_of_timeDelayFinish_7() { return &___timeDelayFinish_7; }
	inline void set_timeDelayFinish_7(float value)
	{
		___timeDelayFinish_7 = value;
	}

	inline static int32_t get_offset_of_OnFinish_8() { return static_cast<int32_t>(offsetof(DisableWithTimeAudioName_t4015415354, ___OnFinish_8)); }
	inline UnityEvent_t408735097 * get_OnFinish_8() const { return ___OnFinish_8; }
	inline UnityEvent_t408735097 ** get_address_of_OnFinish_8() { return &___OnFinish_8; }
	inline void set_OnFinish_8(UnityEvent_t408735097 * value)
	{
		___OnFinish_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnFinish_8, value);
	}

	inline static int32_t get_offset_of_clipTemplate_9() { return static_cast<int32_t>(offsetof(DisableWithTimeAudioName_t4015415354, ___clipTemplate_9)); }
	inline AudioClip_t1932558630 * get_clipTemplate_9() const { return ___clipTemplate_9; }
	inline AudioClip_t1932558630 ** get_address_of_clipTemplate_9() { return &___clipTemplate_9; }
	inline void set_clipTemplate_9(AudioClip_t1932558630 * value)
	{
		___clipTemplate_9 = value;
		Il2CppCodeGenWriteBarrier(&___clipTemplate_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
