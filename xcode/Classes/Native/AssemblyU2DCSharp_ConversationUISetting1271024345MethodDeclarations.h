﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationUISetting
struct ConversationUISetting_t1271024345;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationUISetting::.ctor()
extern "C"  void ConversationUISetting__ctor_m3979801330 (ConversationUISetting_t1271024345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
