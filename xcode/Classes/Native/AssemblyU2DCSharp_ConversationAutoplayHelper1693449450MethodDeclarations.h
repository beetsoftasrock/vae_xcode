﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationAutoplayHelper
struct ConversationAutoplayHelper_t1693449450;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationAutoplayHelper::.ctor()
extern "C"  void ConversationAutoplayHelper__ctor_m3727125783 (ConversationAutoplayHelper_t1693449450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationAutoplayHelper::Awake()
extern "C"  void ConversationAutoplayHelper_Awake_m66411066 (ConversationAutoplayHelper_t1693449450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
