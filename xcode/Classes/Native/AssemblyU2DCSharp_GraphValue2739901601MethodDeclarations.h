﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GraphValue
struct GraphValue_t2739901601;
struct GraphValue_t2739901601_marshaled_pinvoke;
struct GraphValue_t2739901601_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct GraphValue_t2739901601;
struct GraphValue_t2739901601_marshaled_pinvoke;

extern "C" void GraphValue_t2739901601_marshal_pinvoke(const GraphValue_t2739901601& unmarshaled, GraphValue_t2739901601_marshaled_pinvoke& marshaled);
extern "C" void GraphValue_t2739901601_marshal_pinvoke_back(const GraphValue_t2739901601_marshaled_pinvoke& marshaled, GraphValue_t2739901601& unmarshaled);
extern "C" void GraphValue_t2739901601_marshal_pinvoke_cleanup(GraphValue_t2739901601_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GraphValue_t2739901601;
struct GraphValue_t2739901601_marshaled_com;

extern "C" void GraphValue_t2739901601_marshal_com(const GraphValue_t2739901601& unmarshaled, GraphValue_t2739901601_marshaled_com& marshaled);
extern "C" void GraphValue_t2739901601_marshal_com_back(const GraphValue_t2739901601_marshaled_com& marshaled, GraphValue_t2739901601& unmarshaled);
extern "C" void GraphValue_t2739901601_marshal_com_cleanup(GraphValue_t2739901601_marshaled_com& marshaled);
