﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2QuestionView
struct Listening2QuestionView_t3734381824;
// Listening2QuestionData
struct Listening2QuestionData_t2443975119;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Listening2QuestionData2443975119.h"
#include "System_Core_System_Action3226471752.h"

// System.Void Listening2QuestionView::.ctor()
extern "C"  void Listening2QuestionView__ctor_m1152652083 (Listening2QuestionView_t3734381824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2QuestionView::ClearView()
extern "C"  void Listening2QuestionView_ClearView_m2279261195 (Listening2QuestionView_t3734381824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2QuestionView::ClearQuestionSelections()
extern "C"  void Listening2QuestionView_ClearQuestionSelections_m2239638591 (Listening2QuestionView_t3734381824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening2QuestionView::IsLockedInteractive()
extern "C"  bool Listening2QuestionView_IsLockedInteractive_m914810747 (Listening2QuestionView_t3734381824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2QuestionView::InitView(Listening2QuestionData)
extern "C"  void Listening2QuestionView_InitView_m3885738609 (Listening2QuestionView_t3734381824 * __this, Listening2QuestionData_t2443975119 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2QuestionView::ShowResult(System.Boolean,Listening2QuestionData,System.Action)
extern "C"  void Listening2QuestionView_ShowResult_m2356658022 (Listening2QuestionView_t3734381824 * __this, bool ___isCorrect0, Listening2QuestionData_t2443975119 * ___data1, Action_t3226471752 * ___popupClosedCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2QuestionView::LockInteractive()
extern "C"  void Listening2QuestionView_LockInteractive_m2789664438 (Listening2QuestionView_t3734381824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
