﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.InvalidPublicKeyException
struct InvalidPublicKeyException_t3305053858;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.Security.InvalidPublicKeyException::.ctor(System.String)
extern "C"  void InvalidPublicKeyException__ctor_m3317821116 (InvalidPublicKeyException_t3305053858 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
