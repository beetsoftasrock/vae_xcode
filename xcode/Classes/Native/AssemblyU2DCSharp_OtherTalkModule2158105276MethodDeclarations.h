﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OtherTalkModule
struct OtherTalkModule_t2158105276;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void OtherTalkModule::.ctor()
extern "C"  void OtherTalkModule__ctor_m682404067 (OtherTalkModule_t2158105276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherTalkModule::OnConversationReset()
extern "C"  void OtherTalkModule_OnConversationReset_m744519470 (OtherTalkModule_t2158105276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherTalkModule::OnConversationReplay()
extern "C"  void OtherTalkModule_OnConversationReplay_m1375546966 (OtherTalkModule_t2158105276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationTalkData OtherTalkModule::get_currentOtherTalkData()
extern "C"  ConversationTalkData_t1570298305 * OtherTalkModule_get_currentOtherTalkData_m2937855825 (OtherTalkModule_t2158105276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherTalkModule::set_currentOtherTalkData(ConversationTalkData)
extern "C"  void OtherTalkModule_set_currentOtherTalkData_m2340677322 (OtherTalkModule_t2158105276 * __this, ConversationTalkData_t1570298305 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OtherTalkModule::ShowOtherTalkDialog(ConversationTalkData,UnityEngine.Transform)
extern "C"  Il2CppObject * OtherTalkModule_ShowOtherTalkDialog_m1505294148 (OtherTalkModule_t2158105276 * __this, ConversationTalkData_t1570298305 * ___data0, Transform_t3275118058 * ___dialogTransform1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OtherTalkModule::PlayOtherTalkEffect()
extern "C"  Il2CppObject * OtherTalkModule_PlayOtherTalkEffect_m1709879140 (OtherTalkModule_t2158105276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherTalkModule::ReplayOtherTalk()
extern "C"  void OtherTalkModule_ReplayOtherTalk_m334834762 (OtherTalkModule_t2158105276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OtherTalkModule::SetOtherTalkData(ConversationTalkData)
extern "C"  void OtherTalkModule_SetOtherTalkData_m3853551732 (OtherTalkModule_t2158105276 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
