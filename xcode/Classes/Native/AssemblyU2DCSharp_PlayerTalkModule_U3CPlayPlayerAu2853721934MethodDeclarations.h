﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerTalkModule/<PlayPlayerAutoTalkEffect>c__Iterator2
struct U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerTalkModule/<PlayPlayerAutoTalkEffect>c__Iterator2::.ctor()
extern "C"  void U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2__ctor_m1545514779 (U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerTalkModule/<PlayPlayerAutoTalkEffect>c__Iterator2::MoveNext()
extern "C"  bool U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_MoveNext_m4238780369 (U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerTalkModule/<PlayPlayerAutoTalkEffect>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3691832649 (U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerTalkModule/<PlayPlayerAutoTalkEffect>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2774748145 (U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule/<PlayPlayerAutoTalkEffect>c__Iterator2::Dispose()
extern "C"  void U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_Dispose_m3586663288 (U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule/<PlayPlayerAutoTalkEffect>c__Iterator2::Reset()
extern "C"  void U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_Reset_m2208541238 (U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule/<PlayPlayerAutoTalkEffect>c__Iterator2::<>m__0()
extern "C"  void U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_U3CU3Em__0_m195131444 (U3CPlayPlayerAutoTalkEffectU3Ec__Iterator2_t2853721934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
