﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CrossSceneParam
struct CrossSceneParam_t3804493877;

#include "codegen/il2cpp-codegen.h"

// System.Void CrossSceneParam::.ctor()
extern "C"  void CrossSceneParam__ctor_m1494027188 (CrossSceneParam_t3804493877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CrossSceneParam::.cctor()
extern "C"  void CrossSceneParam__cctor_m97089967 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
