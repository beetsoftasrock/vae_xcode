﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AvoidTopBarIOS
struct  AvoidTopBarIOS_t3781391330  : public MonoBehaviour_t1158329972
{
public:
	// System.Single AvoidTopBarIOS::offetY
	float ___offetY_2;

public:
	inline static int32_t get_offset_of_offetY_2() { return static_cast<int32_t>(offsetof(AvoidTopBarIOS_t3781391330, ___offetY_2)); }
	inline float get_offetY_2() const { return ___offetY_2; }
	inline float* get_address_of_offetY_2() { return &___offetY_2; }
	inline void set_offetY_2(float value)
	{
		___offetY_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
