﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.SoundManager
struct SoundManager_t4159196980;
// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// Beetsoft.VAE.SoundResult
struct SoundResult_t4177104648;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Beetsoft.VAE.SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m4053211129 (SoundManager_t4159196980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::OnRuntimeMethodLoad()
extern "C"  void SoundManager_OnRuntimeMethodLoad_m2440710221 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Beetsoft.VAE.SoundManager::GetAllClipsName()
extern "C"  String_t* SoundManager_GetAllClipsName_m1518810365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::LoadFromResources(System.String)
extern "C"  void SoundManager_LoadFromResources_m1714133446 (Il2CppObject * __this /* static, unused */, String_t* ___pathName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::UnloadFromResources(System.String)
extern "C"  void SoundManager_UnloadFromResources_m1105117157 (Il2CppObject * __this /* static, unused */, String_t* ___pathName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::LoadSoundBundle(System.String)
extern "C"  void SoundManager_LoadSoundBundle_m3662681308 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::UnloadSoundBundle(System.String)
extern "C"  void SoundManager_UnloadSoundBundle_m1246139117 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.SoundManager::ClipIsLoaded(System.String)
extern "C"  bool SoundManager_ClipIsLoaded_m3253524274 (Il2CppObject * __this /* static, unused */, String_t* ___pathName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.SoundManager::ClipIsLoaded(System.String,System.String)
extern "C"  bool SoundManager_ClipIsLoaded_m3633702878 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip Beetsoft.VAE.SoundManager::GetClipOrLoad(System.String)
extern "C"  AudioClip_t1932558630 * SoundManager_GetClipOrLoad_m3374854978 (Il2CppObject * __this /* static, unused */, String_t* ___pathName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip Beetsoft.VAE.SoundManager::GetClipOrLoad(System.String,System.String)
extern "C"  AudioClip_t1932558630 * SoundManager_GetClipOrLoad_m3285379950 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::UnloadClip(System.String)
extern "C"  void SoundManager_UnloadClip_m3009414142 (Il2CppObject * __this /* static, unused */, String_t* ___pathName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::UnloadClip(System.String,System.String)
extern "C"  void SoundManager_UnloadClip_m497014942 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.SoundManager::IsPlaying(System.String)
extern "C"  bool SoundManager_IsPlaying_m2217926197 (Il2CppObject * __this /* static, unused */, String_t* ___pathName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.SoundManager::IsPlaying(System.String,System.String)
extern "C"  bool SoundManager_IsPlaying_m3861175583 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Beetsoft.VAE.SoundResult Beetsoft.VAE.SoundManager::Play(UnityEngine.AudioClip,System.Boolean,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  SoundResult_t4177104648 * SoundManager_Play_m257096847 (Il2CppObject * __this /* static, unused */, AudioClip_t1932558630 * ___clip0, bool ___loop1, Vector3_t2243707580  ___position2, float ___delay3, float ___volume4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Beetsoft.VAE.SoundResult Beetsoft.VAE.SoundManager::Play(System.String,System.Boolean,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  SoundResult_t4177104648 * SoundManager_Play_m497797230 (Il2CppObject * __this /* static, unused */, String_t* ___pathName0, bool ___loop1, Vector3_t2243707580  ___position2, float ___delay3, float ___volume4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Beetsoft.VAE.SoundResult Beetsoft.VAE.SoundManager::Play(System.String,System.String,System.Boolean,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  SoundResult_t4177104648 * SoundManager_Play_m322968422 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, bool ___loop2, Vector3_t2243707580  ___position3, float ___delay4, float ___volume5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::Stop(UnityEngine.AudioClip,System.Single)
extern "C"  void SoundManager_Stop_m1485466551 (Il2CppObject * __this /* static, unused */, AudioClip_t1932558630 * ___audioClip0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::Stop(System.String,System.Single)
extern "C"  void SoundManager_Stop_m550139250 (Il2CppObject * __this /* static, unused */, String_t* ___pathName0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::Stop(System.String,System.String,System.Single)
extern "C"  void SoundManager_Stop_m2440078194 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.SoundManager::PlaySingle(System.String,System.String)
extern "C"  bool SoundManager_PlaySingle_m2419212419 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.SoundManager::PlayBGM(System.String,System.String,System.Boolean,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  bool SoundManager_PlayBGM_m3625615571 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, bool ___loop2, Vector3_t2243707580  ___position3, float ___delay4, float ___volume5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.SoundManager::PlayBGM(System.String,System.String)
extern "C"  bool SoundManager_PlayBGM_m3623928825 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___soundName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::StopBGM(System.Single)
extern "C"  void SoundManager_StopBGM_m2874833136 (Il2CppObject * __this /* static, unused */, float ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::StopAll()
extern "C"  void SoundManager_StopAll_m544549754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::.cctor()
extern "C"  void SoundManager__cctor_m1585505044 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager::<PlaySingle>m__0()
extern "C"  void SoundManager_U3CPlaySingleU3Em__0_m902048482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
