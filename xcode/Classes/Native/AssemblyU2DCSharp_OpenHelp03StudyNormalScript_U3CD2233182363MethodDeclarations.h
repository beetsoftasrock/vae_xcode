﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelp03StudyNormalScript/<DelayInitOnclick>c__Iterator0
struct U3CDelayInitOnclickU3Ec__Iterator0_t2233182363;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenHelp03StudyNormalScript/<DelayInitOnclick>c__Iterator0::.ctor()
extern "C"  void U3CDelayInitOnclickU3Ec__Iterator0__ctor_m2500132268 (U3CDelayInitOnclickU3Ec__Iterator0_t2233182363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OpenHelp03StudyNormalScript/<DelayInitOnclick>c__Iterator0::MoveNext()
extern "C"  bool U3CDelayInitOnclickU3Ec__Iterator0_MoveNext_m2655619912 (U3CDelayInitOnclickU3Ec__Iterator0_t2233182363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OpenHelp03StudyNormalScript/<DelayInitOnclick>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayInitOnclickU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3809938432 (U3CDelayInitOnclickU3Ec__Iterator0_t2233182363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OpenHelp03StudyNormalScript/<DelayInitOnclick>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayInitOnclickU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m616526776 (U3CDelayInitOnclickU3Ec__Iterator0_t2233182363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp03StudyNormalScript/<DelayInitOnclick>c__Iterator0::Dispose()
extern "C"  void U3CDelayInitOnclickU3Ec__Iterator0_Dispose_m272007969 (U3CDelayInitOnclickU3Ec__Iterator0_t2233182363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelp03StudyNormalScript/<DelayInitOnclick>c__Iterator0::Reset()
extern "C"  void U3CDelayInitOnclickU3Ec__Iterator0_Reset_m1707153675 (U3CDelayInitOnclickU3Ec__Iterator0_t2233182363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
