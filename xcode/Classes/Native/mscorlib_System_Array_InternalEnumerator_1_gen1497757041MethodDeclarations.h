﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<AssetBundles.AssetBundleLoader>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m579639175(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1497757041 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AssetBundles.AssetBundleLoader>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m905551039(__this, method) ((  void (*) (InternalEnumerator_1_t1497757041 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AssetBundles.AssetBundleLoader>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2109870987(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1497757041 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AssetBundles.AssetBundleLoader>::Dispose()
#define InternalEnumerator_1_Dispose_m118658368(__this, method) ((  void (*) (InternalEnumerator_1_t1497757041 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AssetBundles.AssetBundleLoader>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1571995899(__this, method) ((  bool (*) (InternalEnumerator_1_t1497757041 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AssetBundles.AssetBundleLoader>::get_Current()
#define InternalEnumerator_1_get_Current_m3511572614(__this, method) ((  AssetBundleLoader_t639004779 * (*) (InternalEnumerator_1_t1497757041 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
