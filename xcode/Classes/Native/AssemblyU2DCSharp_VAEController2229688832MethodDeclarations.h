﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VAEController
struct VAEController_t2229688832;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// CommandSheet/Param
struct Param_t4123818474;
// ResultRateController
struct ResultRateController_t2087696941;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_CommandSheet_Param4123818474.h"
#include "AssemblyU2DCSharp_ResultRateController2087696941.h"

// System.Void VAEController::.ctor()
extern "C"  void VAEController__ctor_m2869582993 (VAEController_t2229688832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::SetCharacterName(UnityEngine.UI.Text)
extern "C"  void VAEController_SetCharacterName_m2980836541 (VAEController_t2229688832 * __this, Text_t356221433 * ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::Awake()
extern "C"  void VAEController_Awake_m1346131584 (VAEController_t2229688832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::VaePlay()
extern "C"  void VAEController_VaePlay_m4164309279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::SetCharacter(System.String,System.String,System.String)
extern "C"  void VAEController_SetCharacter_m1364619290 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___type1, String_t* ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::Selection(System.String,System.Int32)
extern "C"  void VAEController_Selection_m1637705444 (Il2CppObject * __this /* static, unused */, String_t* ___uiType0, int32_t ___selectCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::Play(CommandSheet/Param)
extern "C"  void VAEController_Play_m757454757 (Il2CppObject * __this /* static, unused */, Param_t4123818474 * ___command0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::End()
extern "C"  void VAEController_End_m1039156738 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::CompressZip()
extern "C"  void VAEController_CompressZip_m539506292 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::ShowResult(ResultRateController,System.String)
extern "C"  void VAEController_ShowResult_m2755011370 (Il2CppObject * __this /* static, unused */, ResultRateController_t2087696941 * ___rrc0, String_t* ___nameLesson1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::GoTo(System.String)
extern "C"  void VAEController_GoTo_m1042915058 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::Start()
extern "C"  void VAEController_Start_m3312155889 (VAEController_t2229688832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::.cctor()
extern "C"  void VAEController__cctor_m2686774722 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController::<ShowResult>m__0()
extern "C"  void VAEController_U3CShowResultU3Em__0_m2305697418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
