﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour2151245329.h"
#include "UnityEngine_UnityEngine_SystemClock104337557.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType875112366.h"
#include "UnityEngine_UnityEngine_TrackedReference1045890189.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "Apple_U3CModuleU3E3783534214.h"
#include "Apple_UnityEngine_Purchasing_iOSStoreBindings2633471826.h"
#include "Apple_UnityEngine_Purchasing_OSXStoreBindings116576999.h"
#include "FacebookStore_U3CModuleU3E3783534214.h"
#include "FacebookStore_UnityEngine_Purchasing_FacebookStore1025727499.h"
#include "Security_U3CModuleU3E3783534214.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Parser914015216.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Util2059476207.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Oid113668572.h"
#include "Security_LipingShare_LCLib_Asn1Processor_RelativeOi880150712.h"
#include "Security_UnityEngine_Purchasing_Security_Distingui1881593989.h"
#include "Security_UnityEngine_Purchasing_Security_X509Cert481809278.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidX51630759105.h"
#include "Security_UnityEngine_Purchasing_Security_PKCS71974940522.h"
#include "Security_UnityEngine_Purchasing_Security_SignerInf4122348804.h"
#include "Security_UnityEngine_Purchasing_Security_IAPSecuri3038093501.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidSig488933488.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidPK4123278833.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidTi3933748955.h"
#include "Security_UnityEngine_Purchasing_Security_Unsupport2780725255.h"
#include "Security_UnityEngine_Purchasing_Security_RSAKey446464277.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidRS1674954323.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePla4061171767.h"
#include "Security_UnityEngine_Purchasing_Security_AppleVali3837389912.h"
#include "Security_UnityEngine_Purchasing_Security_AppleRecei732159403.h"
#include "Security_UnityEngine_Purchasing_Security_AppleRece3991411794.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato2878230988.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato1106656056.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidPu3305053858.h"
#include "Security_UnityEngine_Purchasing_Security_CrossPlatf305796431.h"
#include "Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Security_U3CPrivateImplementationDetailsU3E_U24Arr1568637719.h"
#include "Tizen_U3CModuleU3E3783534214.h"
#include "Tizen_UnityEngine_Purchasing_UnityNativePurchasing3230812225.h"
#include "Tizen_UnityEngine_Purchasing_TizenStoreBindings1951392817.h"
#include "UnityEngine_Purchasing_U3CModuleU3E3783534214.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Anal3513180421.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Async423752048.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Asyn2099263868.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Asyn3541402849.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Clou3988464631.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Clou2402319400.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (SharedBetweenAnimatorsAttribute_t1565472209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (StateMachineBehaviour_t2151245329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (SystemClock_t104337557), -1, sizeof(SystemClock_t104337557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1702[1] = 
{
	SystemClock_t104337557_StaticFields::get_offset_of_s_Epoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (TouchScreenKeyboardType_t875112366)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1703[10] = 
{
	TouchScreenKeyboardType_t875112366::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (TrackedReference_t1045890189), sizeof(TrackedReference_t1045890189_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[1] = 
{
	TrackedReference_t1045890189::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (PersistentListenerMode_t857969000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1705[8] = 
{
	PersistentListenerMode_t857969000::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (ArgumentCache_t4810721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[6] = 
{
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t4810721::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t4810721::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t4810721::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t4810721::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (BaseInvokableCall_t2229564840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (InvokableCall_t2183506063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[1] = 
{
	InvokableCall_t2183506063::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (UnityEventCallState_t3420894182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1714[4] = 
{
	UnityEventCallState_t3420894182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (PersistentCall_t3793436469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[5] = 
{
	PersistentCall_t3793436469::get_offset_of_m_Target_0(),
	PersistentCall_t3793436469::get_offset_of_m_MethodName_1(),
	PersistentCall_t3793436469::get_offset_of_m_Mode_2(),
	PersistentCall_t3793436469::get_offset_of_m_Arguments_3(),
	PersistentCall_t3793436469::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (PersistentCallGroup_t339478082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[1] = 
{
	PersistentCallGroup_t339478082::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (InvokableCallList_t2295673753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[4] = 
{
	InvokableCallList_t2295673753::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2295673753::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2295673753::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2295673753::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (UnityEventBase_t828812576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[4] = 
{
	UnityEventBase_t828812576::get_offset_of_m_Calls_0(),
	UnityEventBase_t828812576::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t828812576::get_offset_of_m_TypeName_2(),
	UnityEventBase_t828812576::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (UnityAction_t4025899511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (UnityString_t276356480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (Vector2_t2243707579)+ sizeof (Il2CppObject), sizeof(Vector2_t2243707579 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1730[3] = 
{
	Vector2_t2243707579::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2243707579::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (Vector4_t2243707581)+ sizeof (Il2CppObject), sizeof(Vector4_t2243707581 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1731[5] = 
{
	0,
	Vector4_t2243707581::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (AnimationPlayableUtilities_t1808633952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1734[4] = 
{
	FrameData_t1120735295::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (PreserveAttribute_t4182602970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1744[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (iOSStoreBindings_t2633471826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (OSXStoreBindings_t116576999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (FacebookStoreBindings_t1025727499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (Asn1Node_t1770761751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[18] = 
{
	Asn1Node_t1770761751::get_offset_of_tag_0(),
	Asn1Node_t1770761751::get_offset_of_dataOffset_1(),
	Asn1Node_t1770761751::get_offset_of_dataLength_2(),
	Asn1Node_t1770761751::get_offset_of_lengthFieldBytes_3(),
	Asn1Node_t1770761751::get_offset_of_data_4(),
	Asn1Node_t1770761751::get_offset_of_childNodeList_5(),
	Asn1Node_t1770761751::get_offset_of_unusedBits_6(),
	Asn1Node_t1770761751::get_offset_of_deepness_7(),
	Asn1Node_t1770761751::get_offset_of_path_8(),
	0,
	Asn1Node_t1770761751::get_offset_of_parentNode_10(),
	Asn1Node_t1770761751::get_offset_of_requireRecalculatePar_11(),
	Asn1Node_t1770761751::get_offset_of_isIndefiniteLength_12(),
	Asn1Node_t1770761751::get_offset_of_parseEncapsulatedData_13(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (Asn1Parser_t914015216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[2] = 
{
	Asn1Parser_t914015216::get_offset_of_rawData_0(),
	Asn1Parser_t914015216::get_offset_of_rootNode_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (Asn1Util_t2059476207), -1, sizeof(Asn1Util_t2059476207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1759[1] = 
{
	Asn1Util_t2059476207_StaticFields::get_offset_of_hexDigits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (Oid_t113668572), -1, sizeof(Oid_t113668572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1760[1] = 
{
	Oid_t113668572_StaticFields::get_offset_of_oidDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (RelativeOid_t880150712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (DistinguishedName_t1881593989), -1, sizeof(DistinguishedName_t1881593989_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1762[8] = 
{
	DistinguishedName_t1881593989::get_offset_of_U3CCountryU3Ek__BackingField_0(),
	DistinguishedName_t1881593989::get_offset_of_U3COrganizationU3Ek__BackingField_1(),
	DistinguishedName_t1881593989::get_offset_of_U3COrganizationalUnitU3Ek__BackingField_2(),
	DistinguishedName_t1881593989::get_offset_of_U3CDnqU3Ek__BackingField_3(),
	DistinguishedName_t1881593989::get_offset_of_U3CStateU3Ek__BackingField_4(),
	DistinguishedName_t1881593989::get_offset_of_U3CCommonNameU3Ek__BackingField_5(),
	DistinguishedName_t1881593989::get_offset_of_U3CSerialNumberU3Ek__BackingField_6(),
	DistinguishedName_t1881593989_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (X509Cert_t481809278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[10] = 
{
	X509Cert_t481809278::get_offset_of_U3CSerialNumberU3Ek__BackingField_0(),
	X509Cert_t481809278::get_offset_of_U3CValidAfterU3Ek__BackingField_1(),
	X509Cert_t481809278::get_offset_of_U3CValidBeforeU3Ek__BackingField_2(),
	X509Cert_t481809278::get_offset_of_U3CPubKeyU3Ek__BackingField_3(),
	X509Cert_t481809278::get_offset_of_U3CSelfSignedU3Ek__BackingField_4(),
	X509Cert_t481809278::get_offset_of_U3CSubjectU3Ek__BackingField_5(),
	X509Cert_t481809278::get_offset_of_U3CIssuerU3Ek__BackingField_6(),
	X509Cert_t481809278::get_offset_of_TbsCertificate_7(),
	X509Cert_t481809278::get_offset_of_U3CSignatureU3Ek__BackingField_8(),
	X509Cert_t481809278::get_offset_of_rawTBSCertificate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (InvalidX509Data_t1630759105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (PKCS7_t1974940522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[5] = 
{
	PKCS7_t1974940522::get_offset_of_root_0(),
	PKCS7_t1974940522::get_offset_of_U3CdataU3Ek__BackingField_1(),
	PKCS7_t1974940522::get_offset_of_U3CsinfosU3Ek__BackingField_2(),
	PKCS7_t1974940522::get_offset_of_U3CcertChainU3Ek__BackingField_3(),
	PKCS7_t1974940522::get_offset_of_validStructure_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (SignerInfo_t4122348804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[3] = 
{
	SignerInfo_t4122348804::get_offset_of_U3CVersionU3Ek__BackingField_0(),
	SignerInfo_t4122348804::get_offset_of_U3CIssuerSerialNumberU3Ek__BackingField_1(),
	SignerInfo_t4122348804::get_offset_of_U3CEncryptedDigestU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (IAPSecurityException_t3038093501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (InvalidSignatureException_t488933488), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (InvalidPKCS7Data_t4123278833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (InvalidTimeFormat_t3933748955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (UnsupportedSignerInfoVersion_t2780725255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (RSAKey_t446464277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[1] = 
{
	RSAKey_t446464277::get_offset_of_U3CrsaU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (InvalidRSAData_t1674954323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (GooglePlayValidator_t4061171767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[1] = 
{
	GooglePlayValidator_t4061171767::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (AppleValidator_t3837389912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[2] = 
{
	AppleValidator_t3837389912::get_offset_of_cert_0(),
	AppleValidator_t3837389912::get_offset_of_parser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (AppleReceiptParser_t732159403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (AppleReceipt_t3991411794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[7] = 
{
	AppleReceipt_t3991411794::get_offset_of_U3CbundleIDU3Ek__BackingField_0(),
	AppleReceipt_t3991411794::get_offset_of_U3CappVersionU3Ek__BackingField_1(),
	AppleReceipt_t3991411794::get_offset_of_U3CopaqueU3Ek__BackingField_2(),
	AppleReceipt_t3991411794::get_offset_of_U3ChashU3Ek__BackingField_3(),
	AppleReceipt_t3991411794::get_offset_of_U3CoriginalApplicationVersionU3Ek__BackingField_4(),
	AppleReceipt_t3991411794::get_offset_of_U3CreceiptCreationDateU3Ek__BackingField_5(),
	AppleReceipt_t3991411794::get_offset_of_inAppPurchaseReceipts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (AppleInAppPurchaseReceipt_t3271698749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[8] = 
{
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CquantityU3Ek__BackingField_0(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CproductIDU3Ek__BackingField_1(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CtransactionIDU3Ek__BackingField_2(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CpurchaseDateU3Ek__BackingField_4(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CoriginalPurchaseDateU3Ek__BackingField_5(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CcancellationDateU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (Obfuscator_t2878230988), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (U3CDeObfuscateU3Ec__AnonStorey0_t1106656056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[1] = 
{
	U3CDeObfuscateU3Ec__AnonStorey0_t1106656056::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (InvalidPublicKeyException_t3305053858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (CrossPlatformValidator_t305796431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[4] = 
{
	CrossPlatformValidator_t305796431::get_offset_of_google_0(),
	CrossPlatformValidator_t305796431::get_offset_of_apple_1(),
	CrossPlatformValidator_t305796431::get_offset_of_googleBundleId_2(),
	CrossPlatformValidator_t305796431::get_offset_of_appleBundleId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1784[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (U24ArrayTypeU3D32_t1568637719)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637719 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (UnityNativePurchasingCallback_t3230812225), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (TizenStoreBindings_t1951392817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (AnalyticsReporter_t3513180421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[1] = 
{
	AnalyticsReporter_t3513180421::get_offset_of_m_Analytics_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (AsyncUtil_t423752048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (U3CDoInvokeU3Ec__Iterator0_t2099263868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[5] = 
{
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_delayInSeconds_0(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_a_1(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24current_2(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24disposing_3(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (U3CProcessU3Ec__Iterator1_t3541402849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[6] = 
{
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_request_0(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_errorHandler_1(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_responseHandler_2(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24current_3(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24disposing_4(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (CloudCatalogManager_t3988464631), -1, sizeof(CloudCatalogManager_t3988464631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1795[7] = 
{
	CloudCatalogManager_t3988464631::get_offset_of_m_AsyncUtil_0(),
	CloudCatalogManager_t3988464631::get_offset_of_m_CacheFileName_1(),
	CloudCatalogManager_t3988464631::get_offset_of_m_Logger_2(),
	CloudCatalogManager_t3988464631::get_offset_of_m_CatalogURL_3(),
	CloudCatalogManager_t3988464631::get_offset_of_m_StoreName_4(),
	CloudCatalogManager_t3988464631_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	CloudCatalogManager_t3988464631_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (U3CFetchProductsU3Ec__AnonStorey0_t2402319400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[3] = 
{
	U3CFetchProductsU3Ec__AnonStorey0_t2402319400::get_offset_of_callback_0(),
	U3CFetchProductsU3Ec__AnonStorey0_t2402319400::get_offset_of_delayInSeconds_1(),
	U3CFetchProductsU3Ec__AnonStorey0_t2402319400::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (IDs_t3808979560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[1] = 
{
	IDs_t3808979560::get_offset_of_m_Dic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (ConfigurationBuilder_t1298400415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[3] = 
{
	ConfigurationBuilder_t1298400415::get_offset_of_m_Factory_0(),
	ConfigurationBuilder_t1298400415::get_offset_of_m_Products_1(),
	ConfigurationBuilder_t1298400415::get_offset_of_U3CuseCloudCatalogU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
