﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupScript/<RunPopupDestroy>c__Iterator0
struct U3CRunPopupDestroyU3Ec__Iterator0_t318910796;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PopupScript/<RunPopupDestroy>c__Iterator0::.ctor()
extern "C"  void U3CRunPopupDestroyU3Ec__Iterator0__ctor_m1547110983 (U3CRunPopupDestroyU3Ec__Iterator0_t318910796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PopupScript/<RunPopupDestroy>c__Iterator0::MoveNext()
extern "C"  bool U3CRunPopupDestroyU3Ec__Iterator0_MoveNext_m2919406941 (U3CRunPopupDestroyU3Ec__Iterator0_t318910796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PopupScript/<RunPopupDestroy>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRunPopupDestroyU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1629678621 (U3CRunPopupDestroyU3Ec__Iterator0_t318910796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PopupScript/<RunPopupDestroy>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRunPopupDestroyU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1168803077 (U3CRunPopupDestroyU3Ec__Iterator0_t318910796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScript/<RunPopupDestroy>c__Iterator0::Dispose()
extern "C"  void U3CRunPopupDestroyU3Ec__Iterator0_Dispose_m4197776782 (U3CRunPopupDestroyU3Ec__Iterator0_t318910796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScript/<RunPopupDestroy>c__Iterator0::Reset()
extern "C"  void U3CRunPopupDestroyU3Ec__Iterator0_Reset_m4003676556 (U3CRunPopupDestroyU3Ec__Iterator0_t318910796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
