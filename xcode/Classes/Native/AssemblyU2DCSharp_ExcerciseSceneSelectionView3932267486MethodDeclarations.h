﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExcerciseSceneSelectionView
struct ExcerciseSceneSelectionView_t3932267486;

#include "codegen/il2cpp-codegen.h"

// System.Void ExcerciseSceneSelectionView::.ctor()
extern "C"  void ExcerciseSceneSelectionView__ctor_m3954777557 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExcerciseSceneSelectionView::get_selected()
extern "C"  bool ExcerciseSceneSelectionView_get_selected_m1359717 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelectionView::set_selected(System.Boolean)
extern "C"  void ExcerciseSceneSelectionView_set_selected_m3541957468 (ExcerciseSceneSelectionView_t3932267486 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExcerciseSceneSelectionView::get_completed()
extern "C"  float ExcerciseSceneSelectionView_get_completed_m3133312147 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelectionView::set_completed(System.Single)
extern "C"  void ExcerciseSceneSelectionView_set_completed_m3908314272 (ExcerciseSceneSelectionView_t3932267486 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelectionView::RefreshView()
extern "C"  void ExcerciseSceneSelectionView_RefreshView_m2233785977 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelectionView::UpdateView()
extern "C"  void ExcerciseSceneSelectionView_UpdateView_m996469245 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelectionView::Clicked()
extern "C"  void ExcerciseSceneSelectionView_Clicked_m2609554296 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelectionView::Awake()
extern "C"  void ExcerciseSceneSelectionView_Awake_m188556494 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelectionView::OnEnable()
extern "C"  void ExcerciseSceneSelectionView_OnEnable_m708317733 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExcerciseSceneSelectionView::UpdateData()
extern "C"  void ExcerciseSceneSelectionView_UpdateData_m3176917424 (ExcerciseSceneSelectionView_t3932267486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
