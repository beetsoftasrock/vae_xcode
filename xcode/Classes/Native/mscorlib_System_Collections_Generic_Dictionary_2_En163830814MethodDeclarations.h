﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4087912246(__this, ___dictionary0, method) ((  void (*) (Enumerator_t163830814 *, Dictionary_2_t3138773408 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3862459447(__this, method) ((  Il2CppObject * (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4522891(__this, method) ((  void (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3791826696(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2391803593(__this, method) ((  Il2CppObject * (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m74562993(__this, method) ((  Il2CppObject * (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::MoveNext()
#define Enumerator_MoveNext_m2108326463(__this, method) ((  bool (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_Current()
#define Enumerator_get_Current_m3144027431(__this, method) ((  KeyValuePair_2_t896118630  (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1750326614(__this, method) ((  String_t* (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3319495766(__this, method) ((  Dictionary_2_t1223994146 * (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::Reset()
#define Enumerator_Reset_m1068500852(__this, method) ((  void (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::VerifyState()
#define Enumerator_VerifyState_m640063645(__this, method) ((  void (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m346314563(__this, method) ((  void (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::Dispose()
#define Enumerator_Dispose_m987111850(__this, method) ((  void (*) (Enumerator_t163830814 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
