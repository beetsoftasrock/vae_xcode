﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedSentence1SceneSetup
struct ChapterBasedSentence1SceneSetup_t2049935139;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedSentence1SceneSetup::.ctor()
extern "C"  void ChapterBasedSentence1SceneSetup__ctor_m2711095260 (ChapterBasedSentence1SceneSetup_t2049935139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedSentence1SceneSetup::Awake()
extern "C"  void ChapterBasedSentence1SceneSetup_Awake_m2779213683 (ChapterBasedSentence1SceneSetup_t2049935139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
