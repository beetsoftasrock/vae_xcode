﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryConversationDialog/<LerpItem>c__Iterator0
struct U3CLerpItemU3Ec__Iterator0_t925026203;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HistoryConversationDialog/<LerpItem>c__Iterator0::.ctor()
extern "C"  void U3CLerpItemU3Ec__Iterator0__ctor_m1060166530 (U3CLerpItemU3Ec__Iterator0_t925026203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HistoryConversationDialog/<LerpItem>c__Iterator0::MoveNext()
extern "C"  bool U3CLerpItemU3Ec__Iterator0_MoveNext_m3368053666 (U3CLerpItemU3Ec__Iterator0_t925026203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryConversationDialog/<LerpItem>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLerpItemU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2607303850 (U3CLerpItemU3Ec__Iterator0_t925026203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryConversationDialog/<LerpItem>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLerpItemU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m738680994 (U3CLerpItemU3Ec__Iterator0_t925026203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog/<LerpItem>c__Iterator0::Dispose()
extern "C"  void U3CLerpItemU3Ec__Iterator0_Dispose_m894713233 (U3CLerpItemU3Ec__Iterator0_t925026203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog/<LerpItem>c__Iterator0::Reset()
extern "C"  void U3CLerpItemU3Ec__Iterator0_Reset_m877553163 (U3CLerpItemU3Ec__Iterator0_t925026203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
