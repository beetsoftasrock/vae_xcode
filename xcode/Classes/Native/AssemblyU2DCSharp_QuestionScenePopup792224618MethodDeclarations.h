﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuestionScenePopup
struct QuestionScenePopup_t792224618;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Animation
struct Animation_t2068071072;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t390723249;
// BaseData
struct BaseData_t450384115;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "AssemblyU2DCSharp_BaseData450384115.h"

// System.Void QuestionScenePopup::.ctor()
extern "C"  void QuestionScenePopup__ctor_m2840277665 (QuestionScenePopup_t792224618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean QuestionScenePopup::IsShowing()
extern "C"  bool QuestionScenePopup_IsShowing_m1273769064 (QuestionScenePopup_t792224618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionScenePopup::ShowResultPopup()
extern "C"  void QuestionScenePopup_ShowResultPopup_m4074748145 (QuestionScenePopup_t792224618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionScenePopup::CloseResultPopup()
extern "C"  void QuestionScenePopup_CloseResultPopup_m39481832 (QuestionScenePopup_t792224618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator QuestionScenePopup::PlayAnimation(UnityEngine.Animation,System.Collections.Generic.List`1<UnityEngine.Object>,BaseData)
extern "C"  Il2CppObject * QuestionScenePopup_PlayAnimation_m2675865585 (QuestionScenePopup_t792224618 * __this, Animation_t2068071072 * ___anim0, List_1_t390723249 * ___answers1, BaseData_t450384115 * ___baseData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
