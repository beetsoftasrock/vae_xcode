﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExcerciseCommandLoader/<LoadLevel>c__AnonStorey0
struct  U3CLoadLevelU3Ec__AnonStorey0_t2141008911  : public Il2CppObject
{
public:
	// System.Int32 ExcerciseCommandLoader/<LoadLevel>c__AnonStorey0::level
	int32_t ___level_0;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(U3CLoadLevelU3Ec__AnonStorey0_t2141008911, ___level_0)); }
	inline int32_t get_level_0() const { return ___level_0; }
	inline int32_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(int32_t value)
	{
		___level_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
