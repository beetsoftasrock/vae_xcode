﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2
struct U3CloadAssetBundleU3Ec__Iterator2_t3037061210;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::.ctor()
extern "C"  void U3CloadAssetBundleU3Ec__Iterator2__ctor_m446472625 (U3CloadAssetBundleU3Ec__Iterator2_t3037061210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::MoveNext()
extern "C"  bool U3CloadAssetBundleU3Ec__Iterator2_MoveNext_m3528540607 (U3CloadAssetBundleU3Ec__Iterator2_t3037061210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CloadAssetBundleU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m107649003 (U3CloadAssetBundleU3Ec__Iterator2_t3037061210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CloadAssetBundleU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m4293722627 (U3CloadAssetBundleU3Ec__Iterator2_t3037061210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::Dispose()
extern "C"  void U3CloadAssetBundleU3Ec__Iterator2_Dispose_m3002324212 (U3CloadAssetBundleU3Ec__Iterator2_t3037061210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager/<loadAssetBundle>c__Iterator2::Reset()
extern "C"  void U3CloadAssetBundleU3Ec__Iterator2_Reset_m134268422 (U3CloadAssetBundleU3Ec__Iterator2_t3037061210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
