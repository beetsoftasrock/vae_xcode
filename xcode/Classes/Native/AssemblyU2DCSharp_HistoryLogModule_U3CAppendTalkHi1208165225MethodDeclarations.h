﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1
struct U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::.ctor()
extern "C"  void U3CAppendTalkHistoryDialogU3Ec__Iterator1__ctor_m253162068 (U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::MoveNext()
extern "C"  bool U3CAppendTalkHistoryDialogU3Ec__Iterator1_MoveNext_m2834741256 (U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAppendTalkHistoryDialogU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2202845820 (U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAppendTalkHistoryDialogU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m652004532 (U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::Dispose()
extern "C"  void U3CAppendTalkHistoryDialogU3Ec__Iterator1_Dispose_m47138947 (U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryLogModule/<AppendTalkHistoryDialog>c__Iterator1::Reset()
extern "C"  void U3CAppendTalkHistoryDialogU3Ec__Iterator1_Reset_m69470525 (U3CAppendTalkHistoryDialogU3Ec__Iterator1_t1208165225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
