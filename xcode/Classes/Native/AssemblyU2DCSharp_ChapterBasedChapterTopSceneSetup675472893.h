﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChapterTopController
struct ChapterTopController_t2392392144;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// SelectButton[]
struct SelectButtonU5BU5D_t666489473;

#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterBasedChapterTopSceneSetup
struct  ChapterBasedChapterTopSceneSetup_t675472893  : public GlobalBasedSetupModule_t1655890765
{
public:
	// ChapterTopController ChapterBasedChapterTopSceneSetup::controller
	ChapterTopController_t2392392144 * ___controller_3;
	// UnityEngine.UI.Image[] ChapterBasedChapterTopSceneSetup::backgroundImages
	ImageU5BU5D_t590162004* ___backgroundImages_4;
	// UnityEngine.UI.Text ChapterBasedChapterTopSceneSetup::chapterText
	Text_t356221433 * ___chapterText_5;
	// UnityEngine.UI.Text ChapterBasedChapterTopSceneSetup::titleText
	Text_t356221433 * ___titleText_6;
	// UnityEngine.UI.Text ChapterBasedChapterTopSceneSetup::subTitleText
	Text_t356221433 * ___subTitleText_7;
	// UnityEngine.UI.Text ChapterBasedChapterTopSceneSetup::chapterInfoText
	Text_t356221433 * ___chapterInfoText_8;
	// UnityEngine.UI.Image[] ChapterBasedChapterTopSceneSetup::gageImages
	ImageU5BU5D_t590162004* ___gageImages_9;
	// UnityEngine.UI.Text[] ChapterBasedChapterTopSceneSetup::tabTexts
	TextU5BU5D_t4216439300* ___tabTexts_10;
	// SelectButton[] ChapterBasedChapterTopSceneSetup::selectButtons
	SelectButtonU5BU5D_t666489473* ___selectButtons_11;

public:
	inline static int32_t get_offset_of_controller_3() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___controller_3)); }
	inline ChapterTopController_t2392392144 * get_controller_3() const { return ___controller_3; }
	inline ChapterTopController_t2392392144 ** get_address_of_controller_3() { return &___controller_3; }
	inline void set_controller_3(ChapterTopController_t2392392144 * value)
	{
		___controller_3 = value;
		Il2CppCodeGenWriteBarrier(&___controller_3, value);
	}

	inline static int32_t get_offset_of_backgroundImages_4() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___backgroundImages_4)); }
	inline ImageU5BU5D_t590162004* get_backgroundImages_4() const { return ___backgroundImages_4; }
	inline ImageU5BU5D_t590162004** get_address_of_backgroundImages_4() { return &___backgroundImages_4; }
	inline void set_backgroundImages_4(ImageU5BU5D_t590162004* value)
	{
		___backgroundImages_4 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImages_4, value);
	}

	inline static int32_t get_offset_of_chapterText_5() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___chapterText_5)); }
	inline Text_t356221433 * get_chapterText_5() const { return ___chapterText_5; }
	inline Text_t356221433 ** get_address_of_chapterText_5() { return &___chapterText_5; }
	inline void set_chapterText_5(Text_t356221433 * value)
	{
		___chapterText_5 = value;
		Il2CppCodeGenWriteBarrier(&___chapterText_5, value);
	}

	inline static int32_t get_offset_of_titleText_6() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___titleText_6)); }
	inline Text_t356221433 * get_titleText_6() const { return ___titleText_6; }
	inline Text_t356221433 ** get_address_of_titleText_6() { return &___titleText_6; }
	inline void set_titleText_6(Text_t356221433 * value)
	{
		___titleText_6 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_6, value);
	}

	inline static int32_t get_offset_of_subTitleText_7() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___subTitleText_7)); }
	inline Text_t356221433 * get_subTitleText_7() const { return ___subTitleText_7; }
	inline Text_t356221433 ** get_address_of_subTitleText_7() { return &___subTitleText_7; }
	inline void set_subTitleText_7(Text_t356221433 * value)
	{
		___subTitleText_7 = value;
		Il2CppCodeGenWriteBarrier(&___subTitleText_7, value);
	}

	inline static int32_t get_offset_of_chapterInfoText_8() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___chapterInfoText_8)); }
	inline Text_t356221433 * get_chapterInfoText_8() const { return ___chapterInfoText_8; }
	inline Text_t356221433 ** get_address_of_chapterInfoText_8() { return &___chapterInfoText_8; }
	inline void set_chapterInfoText_8(Text_t356221433 * value)
	{
		___chapterInfoText_8 = value;
		Il2CppCodeGenWriteBarrier(&___chapterInfoText_8, value);
	}

	inline static int32_t get_offset_of_gageImages_9() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___gageImages_9)); }
	inline ImageU5BU5D_t590162004* get_gageImages_9() const { return ___gageImages_9; }
	inline ImageU5BU5D_t590162004** get_address_of_gageImages_9() { return &___gageImages_9; }
	inline void set_gageImages_9(ImageU5BU5D_t590162004* value)
	{
		___gageImages_9 = value;
		Il2CppCodeGenWriteBarrier(&___gageImages_9, value);
	}

	inline static int32_t get_offset_of_tabTexts_10() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___tabTexts_10)); }
	inline TextU5BU5D_t4216439300* get_tabTexts_10() const { return ___tabTexts_10; }
	inline TextU5BU5D_t4216439300** get_address_of_tabTexts_10() { return &___tabTexts_10; }
	inline void set_tabTexts_10(TextU5BU5D_t4216439300* value)
	{
		___tabTexts_10 = value;
		Il2CppCodeGenWriteBarrier(&___tabTexts_10, value);
	}

	inline static int32_t get_offset_of_selectButtons_11() { return static_cast<int32_t>(offsetof(ChapterBasedChapterTopSceneSetup_t675472893, ___selectButtons_11)); }
	inline SelectButtonU5BU5D_t666489473* get_selectButtons_11() const { return ___selectButtons_11; }
	inline SelectButtonU5BU5D_t666489473** get_address_of_selectButtons_11() { return &___selectButtons_11; }
	inline void set_selectButtons_11(SelectButtonU5BU5D_t666489473* value)
	{
		___selectButtons_11 = value;
		Il2CppCodeGenWriteBarrier(&___selectButtons_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
