﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterTopConfig
struct  ChapterTopConfig_t174003822  : public ScriptableObject_t1975622470
{
public:
	// System.String ChapterTopConfig::chapterText
	String_t* ___chapterText_2;
	// System.String ChapterTopConfig::titleText
	String_t* ___titleText_3;
	// System.String ChapterTopConfig::subTitleText
	String_t* ___subTitleText_4;
	// System.String ChapterTopConfig::chapterInfoText
	String_t* ___chapterInfoText_5;
	// UnityEngine.Sprite ChapterTopConfig::background
	Sprite_t309593783 * ___background_6;
	// UnityEngine.Color ChapterTopConfig::mainColor
	Color_t2020392075  ___mainColor_7;
	// UnityEngine.Sprite ChapterTopConfig::buttonSprite
	Sprite_t309593783 * ___buttonSprite_8;
	// UnityEngine.Sprite ChapterTopConfig::singleButtonSprite
	Sprite_t309593783 * ___singleButtonSprite_9;
	// UnityEngine.Sprite ChapterTopConfig::gageSprite
	Sprite_t309593783 * ___gageSprite_10;
	// UnityEngine.Sprite ChapterTopConfig::autoButtonSprite
	Sprite_t309593783 * ___autoButtonSprite_11;

public:
	inline static int32_t get_offset_of_chapterText_2() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___chapterText_2)); }
	inline String_t* get_chapterText_2() const { return ___chapterText_2; }
	inline String_t** get_address_of_chapterText_2() { return &___chapterText_2; }
	inline void set_chapterText_2(String_t* value)
	{
		___chapterText_2 = value;
		Il2CppCodeGenWriteBarrier(&___chapterText_2, value);
	}

	inline static int32_t get_offset_of_titleText_3() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___titleText_3)); }
	inline String_t* get_titleText_3() const { return ___titleText_3; }
	inline String_t** get_address_of_titleText_3() { return &___titleText_3; }
	inline void set_titleText_3(String_t* value)
	{
		___titleText_3 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_3, value);
	}

	inline static int32_t get_offset_of_subTitleText_4() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___subTitleText_4)); }
	inline String_t* get_subTitleText_4() const { return ___subTitleText_4; }
	inline String_t** get_address_of_subTitleText_4() { return &___subTitleText_4; }
	inline void set_subTitleText_4(String_t* value)
	{
		___subTitleText_4 = value;
		Il2CppCodeGenWriteBarrier(&___subTitleText_4, value);
	}

	inline static int32_t get_offset_of_chapterInfoText_5() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___chapterInfoText_5)); }
	inline String_t* get_chapterInfoText_5() const { return ___chapterInfoText_5; }
	inline String_t** get_address_of_chapterInfoText_5() { return &___chapterInfoText_5; }
	inline void set_chapterInfoText_5(String_t* value)
	{
		___chapterInfoText_5 = value;
		Il2CppCodeGenWriteBarrier(&___chapterInfoText_5, value);
	}

	inline static int32_t get_offset_of_background_6() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___background_6)); }
	inline Sprite_t309593783 * get_background_6() const { return ___background_6; }
	inline Sprite_t309593783 ** get_address_of_background_6() { return &___background_6; }
	inline void set_background_6(Sprite_t309593783 * value)
	{
		___background_6 = value;
		Il2CppCodeGenWriteBarrier(&___background_6, value);
	}

	inline static int32_t get_offset_of_mainColor_7() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___mainColor_7)); }
	inline Color_t2020392075  get_mainColor_7() const { return ___mainColor_7; }
	inline Color_t2020392075 * get_address_of_mainColor_7() { return &___mainColor_7; }
	inline void set_mainColor_7(Color_t2020392075  value)
	{
		___mainColor_7 = value;
	}

	inline static int32_t get_offset_of_buttonSprite_8() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___buttonSprite_8)); }
	inline Sprite_t309593783 * get_buttonSprite_8() const { return ___buttonSprite_8; }
	inline Sprite_t309593783 ** get_address_of_buttonSprite_8() { return &___buttonSprite_8; }
	inline void set_buttonSprite_8(Sprite_t309593783 * value)
	{
		___buttonSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_8, value);
	}

	inline static int32_t get_offset_of_singleButtonSprite_9() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___singleButtonSprite_9)); }
	inline Sprite_t309593783 * get_singleButtonSprite_9() const { return ___singleButtonSprite_9; }
	inline Sprite_t309593783 ** get_address_of_singleButtonSprite_9() { return &___singleButtonSprite_9; }
	inline void set_singleButtonSprite_9(Sprite_t309593783 * value)
	{
		___singleButtonSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___singleButtonSprite_9, value);
	}

	inline static int32_t get_offset_of_gageSprite_10() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___gageSprite_10)); }
	inline Sprite_t309593783 * get_gageSprite_10() const { return ___gageSprite_10; }
	inline Sprite_t309593783 ** get_address_of_gageSprite_10() { return &___gageSprite_10; }
	inline void set_gageSprite_10(Sprite_t309593783 * value)
	{
		___gageSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___gageSprite_10, value);
	}

	inline static int32_t get_offset_of_autoButtonSprite_11() { return static_cast<int32_t>(offsetof(ChapterTopConfig_t174003822, ___autoButtonSprite_11)); }
	inline Sprite_t309593783 * get_autoButtonSprite_11() const { return ___autoButtonSprite_11; }
	inline Sprite_t309593783 ** get_address_of_autoButtonSprite_11() { return &___autoButtonSprite_11; }
	inline void set_autoButtonSprite_11(Sprite_t309593783 * value)
	{
		___autoButtonSprite_11 = value;
		Il2CppCodeGenWriteBarrier(&___autoButtonSprite_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
