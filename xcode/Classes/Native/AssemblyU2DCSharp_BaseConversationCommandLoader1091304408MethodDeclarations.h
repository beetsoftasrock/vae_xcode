﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseConversationCommandLoader
struct BaseConversationCommandLoader_t1091304408;
// ConversationCommand
struct ConversationCommand_t3660105836;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BaseConversationCommandLoader::.ctor()
extern "C"  void BaseConversationCommandLoader__ctor_m209899557 (BaseConversationCommandLoader_t1091304408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationCommandLoader::MoveCurrentPoint(System.Int32)
extern "C"  void BaseConversationCommandLoader_MoveCurrentPoint_m176550358 (BaseConversationCommandLoader_t1091304408 * __this, int32_t ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BaseConversationCommandLoader::GetCurrentPoint()
extern "C"  int32_t BaseConversationCommandLoader_GetCurrentPoint_m161700028 (BaseConversationCommandLoader_t1091304408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationCommand BaseConversationCommandLoader::LoadNextCommand()
extern "C"  ConversationCommand_t3660105836 * BaseConversationCommandLoader_LoadNextCommand_m859578540 (BaseConversationCommandLoader_t1091304408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BaseConversationCommandLoader::HaveNextCommand()
extern "C"  bool BaseConversationCommandLoader_HaveNextCommand_m3906398383 (BaseConversationCommandLoader_t1091304408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationCommandLoader::Reset()
extern "C"  void BaseConversationCommandLoader_Reset_m2514168376 (BaseConversationCommandLoader_t1091304408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseConversationCommandLoader::GoTo(System.String)
extern "C"  void BaseConversationCommandLoader_GoTo_m2943472342 (BaseConversationCommandLoader_t1091304408 * __this, String_t* ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
