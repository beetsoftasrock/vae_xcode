﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedKaraokeTextColor
struct SettingBasedKaraokeTextColor_t3573755485;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedKaraokeTextColor::.ctor()
extern "C"  void SettingBasedKaraokeTextColor__ctor_m603532662 (SettingBasedKaraokeTextColor_t3573755485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedKaraokeTextColor::Start()
extern "C"  void SettingBasedKaraokeTextColor_Start_m1729920574 (SettingBasedKaraokeTextColor_t3573755485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
