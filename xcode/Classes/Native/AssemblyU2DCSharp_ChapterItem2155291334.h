﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterItem
struct  ChapterItem_t2155291334  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ChapterItem::sizeEffectRange
	float ___sizeEffectRange_2;
	// UnityEngine.RectTransform ChapterItem::container
	RectTransform_t3349966182 * ___container_5;
	// UnityEngine.Transform ChapterItem::mapLocation
	Transform_t3275118058 * ___mapLocation_6;
	// UnityEngine.GameObject ChapterItem::icon_Lock
	GameObject_t1756533147 * ___icon_Lock_7;
	// UnityEngine.GameObject ChapterItem::icon_New
	GameObject_t1756533147 * ___icon_New_8;
	// UnityEngine.UI.Image ChapterItem::image
	Image_t2042527209 * ___image_9;

public:
	inline static int32_t get_offset_of_sizeEffectRange_2() { return static_cast<int32_t>(offsetof(ChapterItem_t2155291334, ___sizeEffectRange_2)); }
	inline float get_sizeEffectRange_2() const { return ___sizeEffectRange_2; }
	inline float* get_address_of_sizeEffectRange_2() { return &___sizeEffectRange_2; }
	inline void set_sizeEffectRange_2(float value)
	{
		___sizeEffectRange_2 = value;
	}

	inline static int32_t get_offset_of_container_5() { return static_cast<int32_t>(offsetof(ChapterItem_t2155291334, ___container_5)); }
	inline RectTransform_t3349966182 * get_container_5() const { return ___container_5; }
	inline RectTransform_t3349966182 ** get_address_of_container_5() { return &___container_5; }
	inline void set_container_5(RectTransform_t3349966182 * value)
	{
		___container_5 = value;
		Il2CppCodeGenWriteBarrier(&___container_5, value);
	}

	inline static int32_t get_offset_of_mapLocation_6() { return static_cast<int32_t>(offsetof(ChapterItem_t2155291334, ___mapLocation_6)); }
	inline Transform_t3275118058 * get_mapLocation_6() const { return ___mapLocation_6; }
	inline Transform_t3275118058 ** get_address_of_mapLocation_6() { return &___mapLocation_6; }
	inline void set_mapLocation_6(Transform_t3275118058 * value)
	{
		___mapLocation_6 = value;
		Il2CppCodeGenWriteBarrier(&___mapLocation_6, value);
	}

	inline static int32_t get_offset_of_icon_Lock_7() { return static_cast<int32_t>(offsetof(ChapterItem_t2155291334, ___icon_Lock_7)); }
	inline GameObject_t1756533147 * get_icon_Lock_7() const { return ___icon_Lock_7; }
	inline GameObject_t1756533147 ** get_address_of_icon_Lock_7() { return &___icon_Lock_7; }
	inline void set_icon_Lock_7(GameObject_t1756533147 * value)
	{
		___icon_Lock_7 = value;
		Il2CppCodeGenWriteBarrier(&___icon_Lock_7, value);
	}

	inline static int32_t get_offset_of_icon_New_8() { return static_cast<int32_t>(offsetof(ChapterItem_t2155291334, ___icon_New_8)); }
	inline GameObject_t1756533147 * get_icon_New_8() const { return ___icon_New_8; }
	inline GameObject_t1756533147 ** get_address_of_icon_New_8() { return &___icon_New_8; }
	inline void set_icon_New_8(GameObject_t1756533147 * value)
	{
		___icon_New_8 = value;
		Il2CppCodeGenWriteBarrier(&___icon_New_8, value);
	}

	inline static int32_t get_offset_of_image_9() { return static_cast<int32_t>(offsetof(ChapterItem_t2155291334, ___image_9)); }
	inline Image_t2042527209 * get_image_9() const { return ___image_9; }
	inline Image_t2042527209 ** get_address_of_image_9() { return &___image_9; }
	inline void set_image_9(Image_t2042527209 * value)
	{
		___image_9 = value;
		Il2CppCodeGenWriteBarrier(&___image_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
