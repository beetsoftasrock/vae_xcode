﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionChecker/<checksUpdate>c__AnonStorey1
struct U3CchecksUpdateU3Ec__AnonStorey1_t2534716322;

#include "codegen/il2cpp-codegen.h"

// System.Void VersionChecker/<checksUpdate>c__AnonStorey1::.ctor()
extern "C"  void U3CchecksUpdateU3Ec__AnonStorey1__ctor_m3669723507 (U3CchecksUpdateU3Ec__AnonStorey1_t2534716322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionChecker/<checksUpdate>c__AnonStorey1::<>m__0()
extern "C"  void U3CchecksUpdateU3Ec__AnonStorey1_U3CU3Em__0_m4214507968 (U3CchecksUpdateU3Ec__AnonStorey1_t2534716322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
