﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToCommand/<Execute>c__Iterator1
struct U3CExecuteU3Ec__Iterator1_t1995833048;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GoToCommand/<Execute>c__Iterator1::.ctor()
extern "C"  void U3CExecuteU3Ec__Iterator1__ctor_m4273054233 (U3CExecuteU3Ec__Iterator1_t1995833048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoToCommand/<Execute>c__Iterator1::MoveNext()
extern "C"  bool U3CExecuteU3Ec__Iterator1_MoveNext_m639715439 (U3CExecuteU3Ec__Iterator1_t1995833048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoToCommand/<Execute>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4085011251 (U3CExecuteU3Ec__Iterator1_t1995833048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoToCommand/<Execute>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2301404235 (U3CExecuteU3Ec__Iterator1_t1995833048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToCommand/<Execute>c__Iterator1::Dispose()
extern "C"  void U3CExecuteU3Ec__Iterator1_Dispose_m3606778090 (U3CExecuteU3Ec__Iterator1_t1995833048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToCommand/<Execute>c__Iterator1::Reset()
extern "C"  void U3CExecuteU3Ec__Iterator1_Reset_m4290872444 (U3CExecuteU3Ec__Iterator1_t1995833048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
