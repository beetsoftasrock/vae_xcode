﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedTextColor
struct SettingBasedTextColor_t2837927353;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedTextColor::.ctor()
extern "C"  void SettingBasedTextColor__ctor_m3367448082 (SettingBasedTextColor_t2837927353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedTextColor::Start()
extern "C"  void SettingBasedTextColor_Start_m2701524010 (SettingBasedTextColor_t2837927353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
