﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VocabularyQuestionView1631345783.h"
#include "AssemblyU2DCSharp_VocabularyQuestionView_U3CPlayAn3262807437.h"
#include "AssemblyU2DCSharp_VocalController1499553201.h"
#include "AssemblyU2DCSharp_VocalController_U3CStartU3Ec__Ite956566065.h"
#include "AssemblyU2DCSharp_ControllerDemoManager4136074806.h"
#include "AssemblyU2DCSharp_Teleport282063519.h"
#include "AssemblyU2DCSharp_DemoInputManager2776755480.h"
#include "AssemblyU2DCSharp_DemoSceneManager779426248.h"
#include "AssemblyU2DCSharp_JumpToPage3783692930.h"
#include "AssemblyU2DCSharp_ChildrenPageProvider958136491.h"
#include "AssemblyU2DCSharp_PrefabPageProvider3978016920.h"
#include "AssemblyU2DCSharp_PagedScrollBar4073376237.h"
#include "AssemblyU2DCSharp_PagedScrollRect3048021378.h"
#include "AssemblyU2DCSharp_BaseScrollEffect2855282033.h"
#include "AssemblyU2DCSharp_BaseTile3549052087.h"
#include "AssemblyU2DCSharp_FadeScrollEffect2128935010.h"
#include "AssemblyU2DCSharp_FloatTile645192174.h"
#include "AssemblyU2DCSharp_MaskedTile4024183987.h"
#include "AssemblyU2DCSharp_ScaleScrollEffect3430758866.h"
#include "AssemblyU2DCSharp_TileScrollEffect2151509712.h"
#include "AssemblyU2DCSharp_TiledPage4183784445.h"
#include "AssemblyU2DCSharp_TranslateScrollEffect636656150.h"
#include "AssemblyU2DCSharp_Tab4262919697.h"
#include "AssemblyU2DCSharp_TabGroup2567651434.h"
#include "AssemblyU2DCSharp_UIFadeTransition1996000123.h"
#include "AssemblyU2DCSharp_GVR_Input_AppButtonInput1324551885.h"
#include "AssemblyU2DCSharp_GVRSample_AutoPlayVideo1314286476.h"
#include "AssemblyU2DCSharp_GVR_Input_Vector3Event2806921088.h"
#include "AssemblyU2DCSharp_GVR_Input_Vector2Event2806928513.h"
#include "AssemblyU2DCSharp_GVR_Input_FloatEvent2213495270.h"
#include "AssemblyU2DCSharp_GVR_Input_BoolEvent555382268.h"
#include "AssemblyU2DCSharp_GVR_Input_ButtonEvent3014361476.h"
#include "AssemblyU2DCSharp_GVR_Input_TouchPadEvent1647781410.h"
#include "AssemblyU2DCSharp_GVR_Input_TransformEvent206501054.h"
#include "AssemblyU2DCSharp_GVR_Input_GameObjectEvent3653055841.h"
#include "AssemblyU2DCSharp_MenuHandler3829619697.h"
#include "AssemblyU2DCSharp_MenuHandler_U3CDoAppearU3Ec__Ite3667605745.h"
#include "AssemblyU2DCSharp_MenuHandler_U3CDoFadeU3Ec__Itera3187801445.h"
#include "AssemblyU2DCSharp_GVR_Events_PositionSwapper2793617445.h"
#include "AssemblyU2DCSharp_ScrubberEvents2429506345.h"
#include "AssemblyU2DCSharp_SwitchVideos3516593024.h"
#include "AssemblyU2DCSharp_SwitchVideos_U3CSetActiveDelayedU531920552.h"
#include "AssemblyU2DCSharp_GVR_Events_ToggleAction2865238344.h"
#include "AssemblyU2DCSharp_VideoControlsManager3010523296.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoAppear2331568912.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoFadeU34193299950.h"
#include "AssemblyU2DCSharp_VideoPlayerReference1150574547.h"
#include "AssemblyU2DCSharp_GvrHead3923315805.h"
#include "AssemblyU2DCSharp_GvrHead_HeadUpdatedDelegate1289521902.h"
#include "AssemblyU2DCSharp_GvrCameraUtils3683962711.h"
#include "AssemblyU2DCSharp_GvrEye3930157106.h"
#include "AssemblyU2DCSharp_GvrPostRender3118402863.h"
#include "AssemblyU2DCSharp_GvrPreRender2074710158.h"
#include "AssemblyU2DCSharp_GvrProfile2070273202.h"
#include "AssemblyU2DCSharp_GvrProfile_Screen839756045.h"
#include "AssemblyU2DCSharp_GvrProfile_Lenses2112994543.h"
#include "AssemblyU2DCSharp_GvrProfile_MaxFOV1743211906.h"
#include "AssemblyU2DCSharp_GvrProfile_Distortion550060296.h"
#include "AssemblyU2DCSharp_GvrProfile_Viewer1642017539.h"
#include "AssemblyU2DCSharp_GvrProfile_ScreenSizes2720173411.h"
#include "AssemblyU2DCSharp_GvrProfile_ViewerTypes2891453798.h"
#include "AssemblyU2DCSharp_StereoController3144380552.h"
#include "AssemblyU2DCSharp_StereoController_U3CEndOfFrameU33626315335.h"
#include "AssemblyU2DCSharp_StereoRenderEffect958489249.h"
#include "AssemblyU2DCSharp_Gvr_Internal_BaseVRDevice4004462063.h"
#include "AssemblyU2DCSharp_Gvr_Internal_GvrDevice635821333.h"
#include "AssemblyU2DCSharp_Gvr_Internal_iOSDevice1373308423.h"
#include "AssemblyU2DCSharp_GvrAudio2627885619.h"
#include "AssemblyU2DCSharp_GvrAudio_Quality2125366261.h"
#include "AssemblyU2DCSharp_GvrAudio_SpatializerData1929858338.h"
#include "AssemblyU2DCSharp_GvrAudio_SpatializerType3348390394.h"
#include "AssemblyU2DCSharp_GvrAudio_RoomProperties2834448096.h"
#include "AssemblyU2DCSharp_GvrAudioListener1521766837.h"
#include "AssemblyU2DCSharp_GvrAudioRoom1253442178.h"
#include "AssemblyU2DCSharp_GvrAudioRoom_SurfaceMaterial3590751945.h"
#include "AssemblyU2DCSharp_GvrAudioSoundfield1301118448.h"
#include "AssemblyU2DCSharp_GvrAudioSource2307460312.h"
#include "AssemblyU2DCSharp_GvrArmModel1664224602.h"
#include "AssemblyU2DCSharp_GvrArmModelOffsets2241056642.h"
#include "AssemblyU2DCSharp_GvrController1602869021.h"
#include "AssemblyU2DCSharp_GvrControllerVisual3328916665.h"
#include "AssemblyU2DCSharp_GvrControllerVisualManager1857939020.h"
#include "AssemblyU2DCSharp_GvrPointerManager2205699129.h"
#include "AssemblyU2DCSharp_GvrRecenterOnlyController2745329441.h"
#include "AssemblyU2DCSharp_GvrTooltip801170144.h"
#include "AssemblyU2DCSharp_Gvr_Internal_AndroidNativeContro1389606029.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorClientSocke2001911543.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig616150261.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig_Mode1624619217.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorGyroEvent1858389926.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorAccelEvent621139879.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent1122923020.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_A936529327.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_3000685002.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorOrientation4153005117.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorButtonEvent156276569.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorButtonEvent4043921137.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager3364249716.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (VocabularyQuestionView_t1631345783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[12] = 
{
	VocabularyQuestionView_t1631345783::get_offset_of_answer0_3(),
	VocabularyQuestionView_t1631345783::get_offset_of_answer1_4(),
	VocabularyQuestionView_t1631345783::get_offset_of_answer2_5(),
	VocabularyQuestionView_t1631345783::get_offset_of_answer3_6(),
	VocabularyQuestionView_t1631345783::get_offset_of_questionText_7(),
	VocabularyQuestionView_t1631345783::get_offset_of_anim_8(),
	VocabularyQuestionView_t1631345783::get_offset_of_questionSoundButton_9(),
	VocabularyQuestionView_t1631345783::get_offset_of_summitButton_10(),
	VocabularyQuestionView_t1631345783::get_offset_of_answerSound_11(),
	VocabularyQuestionView_t1631345783::get_offset_of_popup_12(),
	VocabularyQuestionView_t1631345783::get_offset_of_animResultOpen_13(),
	VocabularyQuestionView_t1631345783::get_offset_of_animResultClose_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (U3CPlayAnimationU3Ec__Iterator0_t3262807437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[4] = 
{
	U3CPlayAnimationU3Ec__Iterator0_t3262807437::get_offset_of_U24this_0(),
	U3CPlayAnimationU3Ec__Iterator0_t3262807437::get_offset_of_U24current_1(),
	U3CPlayAnimationU3Ec__Iterator0_t3262807437::get_offset_of_U24disposing_2(),
	U3CPlayAnimationU3Ec__Iterator0_t3262807437::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (VocalController_t1499553201), -1, sizeof(VocalController_t1499553201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2702[14] = 
{
	VocalController_t1499553201::get_offset_of_questionBundleText_2(),
	VocalController_t1499553201::get_offset_of_answerSound_3(),
	VocalController_t1499553201::get_offset_of_dataLoader_4(),
	VocalController_t1499553201::get_offset_of_onPlayQuestions_5(),
	VocalController_t1499553201::get_offset_of_questionDatas_6(),
	VocalController_t1499553201::get_offset_of_questionView_7(),
	VocalController_t1499553201::get_offset_of_resultGo_8(),
	VocalController_t1499553201::get_offset_of_completedQuestion_9(),
	VocalController_t1499553201::get_offset_of_soundPath_10(),
	VocalController_t1499553201::get_offset_of__currentQuestion_11(),
	VocalController_t1499553201::get_offset_of_startTime_12(),
	VocalController_t1499553201_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
	VocalController_t1499553201_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
	VocalController_t1499553201_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (U3CStartU3Ec__Iterator0_t956566065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[4] = 
{
	U3CStartU3Ec__Iterator0_t956566065::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t956566065::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t956566065::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t956566065::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (ControllerDemoManager_t4136074806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[9] = 
{
	ControllerDemoManager_t4136074806::get_offset_of_controllerPivot_2(),
	ControllerDemoManager_t4136074806::get_offset_of_messageCanvas_3(),
	ControllerDemoManager_t4136074806::get_offset_of_messageText_4(),
	ControllerDemoManager_t4136074806::get_offset_of_cubeInactiveMaterial_5(),
	ControllerDemoManager_t4136074806::get_offset_of_cubeHoverMaterial_6(),
	ControllerDemoManager_t4136074806::get_offset_of_cubeActiveMaterial_7(),
	ControllerDemoManager_t4136074806::get_offset_of_controllerCursorRenderer_8(),
	ControllerDemoManager_t4136074806::get_offset_of_selectedObject_9(),
	ControllerDemoManager_t4136074806::get_offset_of_dragging_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (Teleport_t282063519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[3] = 
{
	Teleport_t282063519::get_offset_of_startingPosition_2(),
	Teleport_t282063519::get_offset_of_inactiveMaterial_3(),
	Teleport_t282063519::get_offset_of_gazedAtMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (DemoInputManager_t2776755480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (DemoSceneManager_t779426248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (JumpToPage_t3783692930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (ChildrenPageProvider_t958136491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[2] = 
{
	ChildrenPageProvider_t958136491::get_offset_of_pages_2(),
	ChildrenPageProvider_t958136491::get_offset_of_spacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (PrefabPageProvider_t3978016920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[2] = 
{
	PrefabPageProvider_t3978016920::get_offset_of_prefabs_2(),
	PrefabPageProvider_t3978016920::get_offset_of_spacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (PagedScrollBar_t4073376237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (PagedScrollRect_t3048021378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (BaseScrollEffect_t2855282033), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (BaseTile_t3549052087), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (FadeScrollEffect_t2128935010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (FloatTile_t645192174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (MaskedTile_t4024183987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (ScaleScrollEffect_t3430758866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (TileScrollEffect_t2151509712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (TiledPage_t4183784445), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (TranslateScrollEffect_t636656150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (Tab_t4262919697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (TabGroup_t2567651434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (UIFadeTransition_t1996000123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (AppButtonInput_t1324551885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (AutoPlayVideo_t1314286476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[5] = 
{
	AutoPlayVideo_t1314286476::get_offset_of_done_2(),
	AutoPlayVideo_t1314286476::get_offset_of_t_3(),
	AutoPlayVideo_t1314286476::get_offset_of_player_4(),
	AutoPlayVideo_t1314286476::get_offset_of_delay_5(),
	AutoPlayVideo_t1314286476::get_offset_of_loop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (Vector3Event_t2806921088), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (Vector2Event_t2806928513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (FloatEvent_t2213495270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (BoolEvent_t555382268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (ButtonEvent_t3014361476), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (TouchPadEvent_t1647781410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (TransformEvent_t206501054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (GameObjectEvent_t3653055841), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (MenuHandler_t3829619697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[1] = 
{
	MenuHandler_t3829619697::get_offset_of_menuObjects_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (U3CDoAppearU3Ec__Iterator0_t3667605745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[5] = 
{
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_U3CcgU3E__0_0(),
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_U24this_1(),
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_U24current_2(),
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_U24disposing_3(),
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (U3CDoFadeU3Ec__Iterator1_t3187801445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[5] = 
{
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_U3CcgU3E__0_0(),
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_U24this_1(),
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_U24current_2(),
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_U24disposing_3(),
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (PositionSwapper_t2793617445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[2] = 
{
	PositionSwapper_t2793617445::get_offset_of_currentIndex_2(),
	PositionSwapper_t2793617445::get_offset_of_Positions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (ScrubberEvents_t2429506345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[5] = 
{
	ScrubberEvents_t2429506345::get_offset_of_newPositionHandle_2(),
	ScrubberEvents_t2429506345::get_offset_of_corners_3(),
	ScrubberEvents_t2429506345::get_offset_of_slider_4(),
	ScrubberEvents_t2429506345::get_offset_of_mgr_5(),
	ScrubberEvents_t2429506345::get_offset_of_inp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (SwitchVideos_t3516593024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[5] = 
{
	SwitchVideos_t3516593024::get_offset_of_localVideoSample_2(),
	SwitchVideos_t3516593024::get_offset_of_dashVideoSample_3(),
	SwitchVideos_t3516593024::get_offset_of_panoVideoSample_4(),
	SwitchVideos_t3516593024::get_offset_of_videoSamples_5(),
	SwitchVideos_t3516593024::get_offset_of_missingLibText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (U3CSetActiveDelayedU3Ec__Iterator0_t531920552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[5] = 
{
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_go_0(),
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_state_1(),
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_U24current_2(),
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_U24disposing_3(),
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (ToggleAction_t2865238344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[7] = 
{
	ToggleAction_t2865238344::get_offset_of_lastUsage_2(),
	ToggleAction_t2865238344::get_offset_of_on_3(),
	ToggleAction_t2865238344::get_offset_of_OnToggleOn_4(),
	ToggleAction_t2865238344::get_offset_of_OnToggleOff_5(),
	ToggleAction_t2865238344::get_offset_of_InitialState_6(),
	ToggleAction_t2865238344::get_offset_of_RaiseEventForInitialState_7(),
	ToggleAction_t2865238344::get_offset_of_Cooldown_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (VideoControlsManager_t3010523296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[11] = 
{
	VideoControlsManager_t3010523296::get_offset_of_pauseSprite_2(),
	VideoControlsManager_t3010523296::get_offset_of_playSprite_3(),
	VideoControlsManager_t3010523296::get_offset_of_videoScrubber_4(),
	VideoControlsManager_t3010523296::get_offset_of_volumeSlider_5(),
	VideoControlsManager_t3010523296::get_offset_of_volumeWidget_6(),
	VideoControlsManager_t3010523296::get_offset_of_settingsPanel_7(),
	VideoControlsManager_t3010523296::get_offset_of_bufferedBackground_8(),
	VideoControlsManager_t3010523296::get_offset_of_basePosition_9(),
	VideoControlsManager_t3010523296::get_offset_of_videoPosition_10(),
	VideoControlsManager_t3010523296::get_offset_of_videoDuration_11(),
	VideoControlsManager_t3010523296::get_offset_of_U3CPlayerU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (U3CDoAppearU3Ec__Iterator0_t2331568912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[5] = 
{
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_U3CcgU3E__0_0(),
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_U24this_1(),
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_U24current_2(),
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_U24disposing_3(),
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (U3CDoFadeU3Ec__Iterator1_t4193299950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[5] = 
{
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_U3CcgU3E__0_0(),
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_U24this_1(),
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_U24current_2(),
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_U24disposing_3(),
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (VideoPlayerReference_t1150574547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[1] = 
{
	VideoPlayerReference_t1150574547::get_offset_of_player_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (GvrHead_t3923315805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[6] = 
{
	GvrHead_t3923315805::get_offset_of_trackRotation_2(),
	GvrHead_t3923315805::get_offset_of_trackPosition_3(),
	GvrHead_t3923315805::get_offset_of_target_4(),
	GvrHead_t3923315805::get_offset_of_updateEarly_5(),
	GvrHead_t3923315805::get_offset_of_OnHeadUpdated_6(),
	GvrHead_t3923315805::get_offset_of_updated_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (HeadUpdatedDelegate_t1289521902), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (GvrCameraUtils_t3683962711), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (GvrEye_t3930157106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[8] = 
{
	GvrEye_t3930157106::get_offset_of_eye_2(),
	GvrEye_t3930157106::get_offset_of_toggleCullingMask_3(),
	GvrEye_t3930157106::get_offset_of_controller_4(),
	GvrEye_t3930157106::get_offset_of_stereoEffect_5(),
	GvrEye_t3930157106::get_offset_of_monoCamera_6(),
	GvrEye_t3930157106::get_offset_of_realProj_7(),
	GvrEye_t3930157106::get_offset_of_interpPosition_8(),
	GvrEye_t3930157106::get_offset_of_U3CcamU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (GvrPostRender_t3118402863), -1, sizeof(GvrPostRender_t3118402863_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2753[22] = 
{
	GvrPostRender_t3118402863::get_offset_of_U3CcamU3Ek__BackingField_2(),
	0,
	0,
	0,
	GvrPostRender_t3118402863::get_offset_of_distortionMesh_6(),
	GvrPostRender_t3118402863::get_offset_of_meshMaterial_7(),
	GvrPostRender_t3118402863::get_offset_of_uiMaterial_8(),
	GvrPostRender_t3118402863::get_offset_of_centerWidthPx_9(),
	GvrPostRender_t3118402863::get_offset_of_buttonWidthPx_10(),
	GvrPostRender_t3118402863::get_offset_of_xScale_11(),
	GvrPostRender_t3118402863::get_offset_of_yScale_12(),
	GvrPostRender_t3118402863::get_offset_of_xfm_13(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrPostRender_t3118402863_StaticFields::get_offset_of_Angles_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (GvrPreRender_t2074710158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[1] = 
{
	GvrPreRender_t2074710158::get_offset_of_U3CcamU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (GvrProfile_t2070273202), -1, sizeof(GvrProfile_t2070273202_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2755[15] = 
{
	GvrProfile_t2070273202::get_offset_of_screen_0(),
	GvrProfile_t2070273202::get_offset_of_viewer_1(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_Nexus5_2(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_Nexus6_3(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_GalaxyS6_4(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_GalaxyNote4_5(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_LGG3_6(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_iPhone4_7(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_iPhone5_8(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_iPhone6_9(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_iPhone6p_10(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_CardboardJun2014_11(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_CardboardMay2015_12(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_GoggleTechC1Glass_13(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_Default_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (Screen_t839756045)+ sizeof (Il2CppObject), sizeof(Screen_t839756045 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2756[3] = 
{
	Screen_t839756045::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Screen_t839756045::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Screen_t839756045::get_offset_of_border_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (Lenses_t2112994543)+ sizeof (Il2CppObject), sizeof(Lenses_t2112994543 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2757[7] = 
{
	Lenses_t2112994543::get_offset_of_separation_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t2112994543::get_offset_of_offset_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t2112994543::get_offset_of_screenDistance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t2112994543::get_offset_of_alignment_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (MaxFOV_t1743211906)+ sizeof (Il2CppObject), sizeof(MaxFOV_t1743211906 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2758[4] = 
{
	MaxFOV_t1743211906::get_offset_of_outer_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t1743211906::get_offset_of_inner_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t1743211906::get_offset_of_upper_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t1743211906::get_offset_of_lower_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (Distortion_t550060296)+ sizeof (Il2CppObject), sizeof(Distortion_t550060296_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2759[1] = 
{
	Distortion_t550060296::get_offset_of_coef_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (Viewer_t1642017539)+ sizeof (Il2CppObject), sizeof(Viewer_t1642017539_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2760[4] = 
{
	Viewer_t1642017539::get_offset_of_lenses_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t1642017539::get_offset_of_maxFOV_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t1642017539::get_offset_of_distortion_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t1642017539::get_offset_of_inverse_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (ScreenSizes_t2720173411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2761[10] = 
{
	ScreenSizes_t2720173411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (ViewerTypes_t2891453798)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2762[4] = 
{
	ViewerTypes_t2891453798::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (StereoController_t3144380552), -1, sizeof(StereoController_t3144380552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2763[17] = 
{
	StereoController_t3144380552::get_offset_of_directRender_2(),
	StereoController_t3144380552::get_offset_of_keepStereoUpdated_3(),
	StereoController_t3144380552::get_offset_of_stereoMultiplier_4(),
	StereoController_t3144380552::get_offset_of_matchMonoFOV_5(),
	StereoController_t3144380552::get_offset_of_matchByZoom_6(),
	StereoController_t3144380552::get_offset_of_centerOfInterest_7(),
	StereoController_t3144380552::get_offset_of_radiusOfInterest_8(),
	StereoController_t3144380552::get_offset_of_checkStereoComfort_9(),
	StereoController_t3144380552::get_offset_of_stereoAdjustSmoothing_10(),
	StereoController_t3144380552::get_offset_of_screenParallax_11(),
	StereoController_t3144380552::get_offset_of_stereoPaddingX_12(),
	StereoController_t3144380552::get_offset_of_stereoPaddingY_13(),
	StereoController_t3144380552::get_offset_of_renderedStereo_14(),
	StereoController_t3144380552::get_offset_of_eyes_15(),
	StereoController_t3144380552::get_offset_of_head_16(),
	StereoController_t3144380552::get_offset_of_U3CcamU3Ek__BackingField_17(),
	StereoController_t3144380552_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t3626315335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[4] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t3626315335::get_offset_of_U24this_0(),
	U3CEndOfFrameU3Ec__Iterator0_t3626315335::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator0_t3626315335::get_offset_of_U24disposing_2(),
	U3CEndOfFrameU3Ec__Iterator0_t3626315335::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (StereoRenderEffect_t958489249), -1, sizeof(StereoRenderEffect_t958489249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2765[3] = 
{
	StereoRenderEffect_t958489249::get_offset_of_material_2(),
	StereoRenderEffect_t958489249::get_offset_of_cam_3(),
	StereoRenderEffect_t958489249_StaticFields::get_offset_of_fullRect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (BaseVRDevice_t4004462063), -1, sizeof(BaseVRDevice_t4004462063_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2766[19] = 
{
	BaseVRDevice_t4004462063_StaticFields::get_offset_of_device_0(),
	BaseVRDevice_t4004462063::get_offset_of_U3CProfileU3Ek__BackingField_1(),
	BaseVRDevice_t4004462063::get_offset_of_headPose_2(),
	BaseVRDevice_t4004462063::get_offset_of_leftEyePose_3(),
	BaseVRDevice_t4004462063::get_offset_of_rightEyePose_4(),
	BaseVRDevice_t4004462063::get_offset_of_leftEyeDistortedProjection_5(),
	BaseVRDevice_t4004462063::get_offset_of_rightEyeDistortedProjection_6(),
	BaseVRDevice_t4004462063::get_offset_of_leftEyeUndistortedProjection_7(),
	BaseVRDevice_t4004462063::get_offset_of_rightEyeUndistortedProjection_8(),
	BaseVRDevice_t4004462063::get_offset_of_leftEyeDistortedViewport_9(),
	BaseVRDevice_t4004462063::get_offset_of_rightEyeDistortedViewport_10(),
	BaseVRDevice_t4004462063::get_offset_of_leftEyeUndistortedViewport_11(),
	BaseVRDevice_t4004462063::get_offset_of_rightEyeUndistortedViewport_12(),
	BaseVRDevice_t4004462063::get_offset_of_recommendedTextureSize_13(),
	BaseVRDevice_t4004462063::get_offset_of_leftEyeOrientation_14(),
	BaseVRDevice_t4004462063::get_offset_of_rightEyeOrientation_15(),
	BaseVRDevice_t4004462063::get_offset_of_tilted_16(),
	BaseVRDevice_t4004462063::get_offset_of_profileChanged_17(),
	BaseVRDevice_t4004462063::get_offset_of_backButtonPressed_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (GvrDevice_t635821333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[13] = 
{
	0,
	0,
	0,
	0,
	GvrDevice_t635821333::get_offset_of_headData_23(),
	GvrDevice_t635821333::get_offset_of_viewData_24(),
	GvrDevice_t635821333::get_offset_of_profileData_25(),
	GvrDevice_t635821333::get_offset_of_headView_26(),
	GvrDevice_t635821333::get_offset_of_leftEyeView_27(),
	GvrDevice_t635821333::get_offset_of_rightEyeView_28(),
	GvrDevice_t635821333::get_offset_of_debugDisableNativeProjections_29(),
	GvrDevice_t635821333::get_offset_of_debugDisableNativeUILayer_30(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (iOSDevice_t1373308423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[1] = 
{
	iOSDevice_t1373308423::get_offset_of_isOpenGL_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (GvrAudio_t2627885619), -1, sizeof(GvrAudio_t2627885619_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2769[23] = 
{
	GvrAudio_t2627885619_StaticFields::get_offset_of_sampleRate_0(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_numChannels_1(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_framesPerBuffer_2(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_listenerDirectivityColor_3(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_sourceDirectivityColor_4(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrAudio_t2627885619_StaticFields::get_offset_of_bounds_16(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_enabledRooms_17(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_initialized_18(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_listenerTransform_19(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_occlusionMaskValue_20(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_pose_21(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (Quality_t2125366261)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2770[4] = 
{
	Quality_t2125366261::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (SpatializerData_t1929858338)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2771[9] = 
{
	SpatializerData_t1929858338::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (SpatializerType_t3348390394)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2772[3] = 
{
	SpatializerType_t3348390394::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (RoomProperties_t2834448096)+ sizeof (Il2CppObject), sizeof(RoomProperties_t2834448096 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2773[20] = 
{
	RoomProperties_t2834448096::get_offset_of_positionX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_positionY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_positionZ_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_rotationX_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_rotationY_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_rotationZ_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_rotationW_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_dimensionsX_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_dimensionsY_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_dimensionsZ_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialLeft_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialRight_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialBottom_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialTop_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialFront_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialBack_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_reflectionScalar_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_reverbGain_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_reverbTime_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_reverbBrightness_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (GvrAudioListener_t1521766837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[3] = 
{
	GvrAudioListener_t1521766837::get_offset_of_globalGainDb_2(),
	GvrAudioListener_t1521766837::get_offset_of_occlusionMask_3(),
	GvrAudioListener_t1521766837::get_offset_of_quality_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (GvrAudioRoom_t1253442178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[11] = 
{
	GvrAudioRoom_t1253442178::get_offset_of_leftWall_2(),
	GvrAudioRoom_t1253442178::get_offset_of_rightWall_3(),
	GvrAudioRoom_t1253442178::get_offset_of_floor_4(),
	GvrAudioRoom_t1253442178::get_offset_of_ceiling_5(),
	GvrAudioRoom_t1253442178::get_offset_of_backWall_6(),
	GvrAudioRoom_t1253442178::get_offset_of_frontWall_7(),
	GvrAudioRoom_t1253442178::get_offset_of_reflectivity_8(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbGainDb_9(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbBrightness_10(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbTime_11(),
	GvrAudioRoom_t1253442178::get_offset_of_size_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (SurfaceMaterial_t3590751945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2776[24] = 
{
	SurfaceMaterial_t3590751945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (GvrAudioSoundfield_t1301118448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[13] = 
{
	GvrAudioSoundfield_t1301118448::get_offset_of_bypassRoomEffects_2(),
	GvrAudioSoundfield_t1301118448::get_offset_of_gainDb_3(),
	GvrAudioSoundfield_t1301118448::get_offset_of_playOnAwake_4(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldClip0102_5(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldClip0304_6(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldLoop_7(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldMute_8(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldPitch_9(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldPriority_10(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldVolume_11(),
	GvrAudioSoundfield_t1301118448::get_offset_of_id_12(),
	GvrAudioSoundfield_t1301118448::get_offset_of_audioSources_13(),
	GvrAudioSoundfield_t1301118448::get_offset_of_isPaused_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (GvrAudioSource_t2307460312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[25] = 
{
	GvrAudioSource_t2307460312::get_offset_of_bypassRoomEffects_2(),
	GvrAudioSource_t2307460312::get_offset_of_directivityAlpha_3(),
	GvrAudioSource_t2307460312::get_offset_of_directivitySharpness_4(),
	GvrAudioSource_t2307460312::get_offset_of_listenerDirectivityAlpha_5(),
	GvrAudioSource_t2307460312::get_offset_of_listenerDirectivitySharpness_6(),
	GvrAudioSource_t2307460312::get_offset_of_gainDb_7(),
	GvrAudioSource_t2307460312::get_offset_of_occlusionEnabled_8(),
	GvrAudioSource_t2307460312::get_offset_of_playOnAwake_9(),
	GvrAudioSource_t2307460312::get_offset_of_sourceClip_10(),
	GvrAudioSource_t2307460312::get_offset_of_sourceLoop_11(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMute_12(),
	GvrAudioSource_t2307460312::get_offset_of_sourcePitch_13(),
	GvrAudioSource_t2307460312::get_offset_of_sourcePriority_14(),
	GvrAudioSource_t2307460312::get_offset_of_sourceDopplerLevel_15(),
	GvrAudioSource_t2307460312::get_offset_of_sourceSpread_16(),
	GvrAudioSource_t2307460312::get_offset_of_sourceVolume_17(),
	GvrAudioSource_t2307460312::get_offset_of_sourceRolloffMode_18(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMaxDistance_19(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMinDistance_20(),
	GvrAudioSource_t2307460312::get_offset_of_hrtfEnabled_21(),
	GvrAudioSource_t2307460312::get_offset_of_audioSource_22(),
	GvrAudioSource_t2307460312::get_offset_of_id_23(),
	GvrAudioSource_t2307460312::get_offset_of_currentOcclusion_24(),
	GvrAudioSource_t2307460312::get_offset_of_nextOcclusionUpdate_25(),
	GvrAudioSource_t2307460312::get_offset_of_isPaused_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (GvrArmModel_t1664224602), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (GvrArmModelOffsets_t2241056642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (GvrController_t1602869021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (GvrControllerVisual_t3328916665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (GvrControllerVisualManager_t1857939020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (GvrPointerManager_t2205699129), -1, sizeof(GvrPointerManager_t2205699129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2784[2] = 
{
	GvrPointerManager_t2205699129_StaticFields::get_offset_of_instance_2(),
	GvrPointerManager_t2205699129::get_offset_of_pointer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (GvrRecenterOnlyController_t2745329441), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (GvrTooltip_t801170144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (AndroidNativeControllerProvider_t1389606029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (EmulatorClientSocket_t2001911543), -1, sizeof(EmulatorClientSocket_t2001911543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2788[9] = 
{
	EmulatorClientSocket_t2001911543_StaticFields::get_offset_of_kPhoneEventPort_2(),
	0,
	0,
	EmulatorClientSocket_t2001911543::get_offset_of_phoneMirroringSocket_5(),
	EmulatorClientSocket_t2001911543::get_offset_of_phoneEventThread_6(),
	EmulatorClientSocket_t2001911543::get_offset_of_shouldStop_7(),
	EmulatorClientSocket_t2001911543::get_offset_of_lastConnectionAttemptWasSuccessful_8(),
	EmulatorClientSocket_t2001911543::get_offset_of_phoneRemote_9(),
	EmulatorClientSocket_t2001911543::get_offset_of_U3CconnectedU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (EmulatorConfig_t616150261), -1, sizeof(EmulatorConfig_t616150261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2789[4] = 
{
	EmulatorConfig_t616150261_StaticFields::get_offset_of_instance_2(),
	EmulatorConfig_t616150261::get_offset_of_PHONE_EVENT_MODE_3(),
	EmulatorConfig_t616150261_StaticFields::get_offset_of_USB_SERVER_IP_4(),
	EmulatorConfig_t616150261_StaticFields::get_offset_of_WIFI_SERVER_IP_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (Mode_t1624619217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[4] = 
{
	Mode_t1624619217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (EmulatorGyroEvent_t1858389926)+ sizeof (Il2CppObject), sizeof(EmulatorGyroEvent_t1858389926 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2791[2] = 
{
	EmulatorGyroEvent_t1858389926::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorGyroEvent_t1858389926::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (EmulatorAccelEvent_t621139879)+ sizeof (Il2CppObject), sizeof(EmulatorAccelEvent_t621139879 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2792[2] = 
{
	EmulatorAccelEvent_t621139879::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorAccelEvent_t621139879::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (EmulatorTouchEvent_t1122923020)+ sizeof (Il2CppObject), -1, sizeof(EmulatorTouchEvent_t1122923020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2793[6] = 
{
	EmulatorTouchEvent_t1122923020::get_offset_of_action_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t1122923020::get_offset_of_relativeTimestamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t1122923020::get_offset_of_pointers_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t1122923020_StaticFields::get_offset_of_ACTION_POINTER_INDEX_SHIFT_3(),
	EmulatorTouchEvent_t1122923020_StaticFields::get_offset_of_ACTION_POINTER_INDEX_MASK_4(),
	EmulatorTouchEvent_t1122923020_StaticFields::get_offset_of_ACTION_MASK_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (Action_t936529327)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2794[10] = 
{
	Action_t936529327::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (Pointer_t3000685002)+ sizeof (Il2CppObject), sizeof(Pointer_t3000685002 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2795[3] = 
{
	Pointer_t3000685002::get_offset_of_fingerId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Pointer_t3000685002::get_offset_of_normalizedX_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Pointer_t3000685002::get_offset_of_normalizedY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (EmulatorOrientationEvent_t4153005117)+ sizeof (Il2CppObject), sizeof(EmulatorOrientationEvent_t4153005117 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2796[2] = 
{
	EmulatorOrientationEvent_t4153005117::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorOrientationEvent_t4153005117::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (EmulatorButtonEvent_t156276569)+ sizeof (Il2CppObject), sizeof(EmulatorButtonEvent_t156276569_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2797[2] = 
{
	EmulatorButtonEvent_t156276569::get_offset_of_code_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorButtonEvent_t156276569::get_offset_of_down_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (ButtonCode_t4043921137)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2798[7] = 
{
	ButtonCode_t4043921137::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (EmulatorManager_t3364249716), -1, sizeof(EmulatorManager_t3364249716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2799[16] = 
{
	EmulatorManager_t3364249716::get_offset_of_emulatorUpdate_2(),
	EmulatorManager_t3364249716::get_offset_of_waitForEndOfFrame_3(),
	EmulatorManager_t3364249716_StaticFields::get_offset_of_instance_4(),
	EmulatorManager_t3364249716::get_offset_of_currentGyroEvent_5(),
	EmulatorManager_t3364249716::get_offset_of_currentAccelEvent_6(),
	EmulatorManager_t3364249716::get_offset_of_currentTouchEvent_7(),
	EmulatorManager_t3364249716::get_offset_of_currentOrientationEvent_8(),
	EmulatorManager_t3364249716::get_offset_of_currentButtonEvent_9(),
	EmulatorManager_t3364249716::get_offset_of_gyroEventListenersInternal_10(),
	EmulatorManager_t3364249716::get_offset_of_accelEventListenersInternal_11(),
	EmulatorManager_t3364249716::get_offset_of_touchEventListenersInternal_12(),
	EmulatorManager_t3364249716::get_offset_of_orientationEventListenersInternal_13(),
	EmulatorManager_t3364249716::get_offset_of_buttonEventListenersInternal_14(),
	EmulatorManager_t3364249716::get_offset_of_pendingEvents_15(),
	EmulatorManager_t3364249716::get_offset_of_socket_16(),
	EmulatorManager_t3364249716::get_offset_of_lastDownTimeMs_17(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
