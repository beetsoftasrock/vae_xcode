﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen682460794.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_MyPageChapterView_ViewTextUI4118675828.h"

// System.Void System.Array/InternalEnumerator`1<MyPageChapterView/ViewTextUI>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3620975307_gshared (InternalEnumerator_1_t682460794 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3620975307(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t682460794 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3620975307_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<MyPageChapterView/ViewTextUI>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2657710939_gshared (InternalEnumerator_1_t682460794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2657710939(__this, method) ((  void (*) (InternalEnumerator_1_t682460794 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2657710939_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<MyPageChapterView/ViewTextUI>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1047869815_gshared (InternalEnumerator_1_t682460794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1047869815(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t682460794 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1047869815_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MyPageChapterView/ViewTextUI>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m788967836_gshared (InternalEnumerator_1_t682460794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m788967836(__this, method) ((  void (*) (InternalEnumerator_1_t682460794 *, const MethodInfo*))InternalEnumerator_1_Dispose_m788967836_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<MyPageChapterView/ViewTextUI>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3198032319_gshared (InternalEnumerator_1_t682460794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3198032319(__this, method) ((  bool (*) (InternalEnumerator_1_t682460794 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3198032319_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<MyPageChapterView/ViewTextUI>::get_Current()
extern "C"  ViewTextUI_t4118675828  InternalEnumerator_1_get_Current_m3583183634_gshared (InternalEnumerator_1_t682460794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3583183634(__this, method) ((  ViewTextUI_t4118675828  (*) (InternalEnumerator_1_t682460794 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3583183634_gshared)(__this, method)
