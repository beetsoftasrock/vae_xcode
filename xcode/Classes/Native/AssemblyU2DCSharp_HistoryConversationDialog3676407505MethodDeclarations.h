﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryConversationDialog
struct HistoryConversationDialog_t3676407505;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HistoryConversationDialog::.ctor()
extern "C"  void HistoryConversationDialog__ctor_m505586222 (HistoryConversationDialog_t3676407505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog::Start()
extern "C"  void HistoryConversationDialog_Start_m1956145302 (HistoryConversationDialog_t3676407505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HistoryConversationDialog::get_showSubText()
extern "C"  bool HistoryConversationDialog_get_showSubText_m383471199 (HistoryConversationDialog_t3676407505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog::set_showSubText(System.Boolean)
extern "C"  void HistoryConversationDialog_set_showSubText_m22127694 (HistoryConversationDialog_t3676407505 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HistoryConversationDialog::LerpItem(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  Il2CppObject * HistoryConversationDialog_LerpItem_m2868852508 (HistoryConversationDialog_t3676407505 * __this, Transform_t3275118058 * ___item0, Transform_t3275118058 * ___startLerpTransform1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HistoryConversationDialog::AddConversationSentence(System.Boolean,ConversationTalkData,System.String,System.String,UnityEngine.Transform)
extern "C"  Il2CppObject * HistoryConversationDialog_AddConversationSentence_m1421968120 (HistoryConversationDialog_t3676407505 * __this, bool ___isPlayerRole0, ConversationTalkData_t1570298305 * ___data1, String_t* ___textEn2, String_t* ___textJp3, Transform_t3275118058 * ___dialogTransform4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog::RemoveHistoryItem(ConversationTalkData)
extern "C"  void HistoryConversationDialog_RemoveHistoryItem_m2370966650 (HistoryConversationDialog_t3676407505 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog::Clear()
extern "C"  void HistoryConversationDialog_Clear_m2407939649 (HistoryConversationDialog_t3676407505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversationDialog::RevertHistory(ConversationTalkData)
extern "C"  void HistoryConversationDialog_RevertHistory_m3424478411 (HistoryConversationDialog_t3676407505 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HistoryConversationDialog::MoveScrollViewToBottom()
extern "C"  Il2CppObject * HistoryConversationDialog_MoveScrollViewToBottom_m4048407475 (HistoryConversationDialog_t3676407505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
