﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalBasedSetupModule
struct GlobalBasedSetupModule_t1655890765;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalBasedSetupModule::.ctor()
extern "C"  void GlobalBasedSetupModule__ctor_m3896419858 (GlobalBasedSetupModule_t1655890765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
