﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingBasedListening2AnswerItem
struct SettingBasedListening2AnswerItem_t180429931;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingBasedListening2AnswerItem::.ctor()
extern "C"  void SettingBasedListening2AnswerItem__ctor_m3233388458 (SettingBasedListening2AnswerItem_t180429931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingBasedListening2AnswerItem::Start()
extern "C"  void SettingBasedListening2AnswerItem_Start_m1568282198 (SettingBasedListening2AnswerItem_t180429931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
