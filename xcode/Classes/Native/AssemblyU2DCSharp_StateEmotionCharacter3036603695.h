﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StateEmotionCharacter
struct  StateEmotionCharacter_t3036603695  : public Il2CppObject
{
public:
	// System.String StateEmotionCharacter::emotionName
	String_t* ___emotionName_0;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> StateEmotionCharacter::parts
	List_1_t3973682211 * ___parts_1;

public:
	inline static int32_t get_offset_of_emotionName_0() { return static_cast<int32_t>(offsetof(StateEmotionCharacter_t3036603695, ___emotionName_0)); }
	inline String_t* get_emotionName_0() const { return ___emotionName_0; }
	inline String_t** get_address_of_emotionName_0() { return &___emotionName_0; }
	inline void set_emotionName_0(String_t* value)
	{
		___emotionName_0 = value;
		Il2CppCodeGenWriteBarrier(&___emotionName_0, value);
	}

	inline static int32_t get_offset_of_parts_1() { return static_cast<int32_t>(offsetof(StateEmotionCharacter_t3036603695, ___parts_1)); }
	inline List_1_t3973682211 * get_parts_1() const { return ___parts_1; }
	inline List_1_t3973682211 ** get_address_of_parts_1() { return &___parts_1; }
	inline void set_parts_1(List_1_t3973682211 * value)
	{
		___parts_1 = value;
		Il2CppCodeGenWriteBarrier(&___parts_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
