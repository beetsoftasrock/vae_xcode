﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SentenceQuestionView
struct SentenceQuestionView_t1478559372;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void SentenceQuestionView::.ctor()
extern "C"  void SentenceQuestionView__ctor_m4174698677 (SentenceQuestionView_t1478559372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceQuestionView::ClearView()
extern "C"  void SentenceQuestionView_ClearView_m3012642993 (SentenceQuestionView_t1478559372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceQuestionView::ShowResult(System.Boolean,System.Int32)
extern "C"  void SentenceQuestionView_ShowResult_m277925021 (SentenceQuestionView_t1478559372 * __this, bool ___isCorrect0, int32_t ___correctAnswerId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SentenceQuestionView::PlayAnimation()
extern "C"  Il2CppObject * SentenceQuestionView_PlayAnimation_m4111335609 (SentenceQuestionView_t1478559372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SentenceQuestionView::IsLockedInteractive()
extern "C"  bool SentenceQuestionView_IsLockedInteractive_m713087845 (SentenceQuestionView_t1478559372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
