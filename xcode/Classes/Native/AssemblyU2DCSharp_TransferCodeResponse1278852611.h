﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransferCodeResponse
struct  TransferCodeResponse_t1278852611  : public Il2CppObject
{
public:
	// System.Boolean TransferCodeResponse::success
	bool ___success_0;
	// System.String TransferCodeResponse::userId
	String_t* ___userId_1;
	// System.String TransferCodeResponse::transferCode
	String_t* ___transferCode_2;
	// System.String TransferCodeResponse::transferExpired
	String_t* ___transferExpired_3;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(TransferCodeResponse_t1278852611, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(TransferCodeResponse_t1278852611, ___userId_1)); }
	inline String_t* get_userId_1() const { return ___userId_1; }
	inline String_t** get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(String_t* value)
	{
		___userId_1 = value;
		Il2CppCodeGenWriteBarrier(&___userId_1, value);
	}

	inline static int32_t get_offset_of_transferCode_2() { return static_cast<int32_t>(offsetof(TransferCodeResponse_t1278852611, ___transferCode_2)); }
	inline String_t* get_transferCode_2() const { return ___transferCode_2; }
	inline String_t** get_address_of_transferCode_2() { return &___transferCode_2; }
	inline void set_transferCode_2(String_t* value)
	{
		___transferCode_2 = value;
		Il2CppCodeGenWriteBarrier(&___transferCode_2, value);
	}

	inline static int32_t get_offset_of_transferExpired_3() { return static_cast<int32_t>(offsetof(TransferCodeResponse_t1278852611, ___transferExpired_3)); }
	inline String_t* get_transferExpired_3() const { return ___transferExpired_3; }
	inline String_t** get_address_of_transferExpired_3() { return &___transferExpired_3; }
	inline void set_transferExpired_3(String_t* value)
	{
		___transferExpired_3 = value;
		Il2CppCodeGenWriteBarrier(&___transferExpired_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
