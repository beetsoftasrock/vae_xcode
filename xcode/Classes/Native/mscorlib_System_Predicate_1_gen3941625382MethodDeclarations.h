﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.Purchasing.Product>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2540769500(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3941625382 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<UnityEngine.Purchasing.Product>::Invoke(T)
#define Predicate_1_Invoke_m3782825453(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3941625382 *, Product_t1203687971 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Purchasing.Product>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m2397762914(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3941625382 *, Product_t1203687971 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<UnityEngine.Purchasing.Product>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m3164526179(__this, ___result0, method) ((  bool (*) (Predicate_1_t3941625382 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
