﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenHelpScript/TrackIndexObject
struct  TrackIndexObject_t1096184091  : public Il2CppObject
{
public:
	// System.Int32 OpenHelpScript/TrackIndexObject::index
	int32_t ___index_0;
	// UnityEngine.Transform OpenHelpScript/TrackIndexObject::parent
	Transform_t3275118058 * ___parent_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(TrackIndexObject_t1096184091, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(TrackIndexObject_t1096184091, ___parent_1)); }
	inline Transform_t3275118058 * get_parent_1() const { return ___parent_1; }
	inline Transform_t3275118058 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(Transform_t3275118058 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
