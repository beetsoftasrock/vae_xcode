﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Text
struct Text_t356221433;
// ChapterLearningItemView
struct ChapterLearningItemView_t1984184315;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterTopController
struct  ChapterTopController_t2392392144  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ChapterTopController::itemNormal
	GameObject_t1756533147 * ___itemNormal_2;
	// UnityEngine.GameObject ChapterTopController::itemAuto
	GameObject_t1756533147 * ___itemAuto_3;
	// UnityEngine.RectTransform ChapterTopController::leftContainer
	RectTransform_t3349966182 * ___leftContainer_4;
	// UnityEngine.RectTransform ChapterTopController::rightContainer
	RectTransform_t3349966182 * ___rightContainer_5;
	// System.Int32 ChapterTopController::chapterId
	int32_t ___chapterId_6;
	// UnityEngine.Color ChapterTopController::mainColor
	Color_t2020392075  ___mainColor_7;
	// UnityEngine.Sprite ChapterTopController::buttonSprite
	Sprite_t309593783 * ___buttonSprite_8;
	// UnityEngine.Sprite ChapterTopController::singleButtonSprite
	Sprite_t309593783 * ___singleButtonSprite_9;
	// UnityEngine.Sprite ChapterTopController::autoButtonSprite
	Sprite_t309593783 * ___autoButtonSprite_10;
	// UnityEngine.UI.Text ChapterTopController::chapterNameText
	Text_t356221433 * ___chapterNameText_11;
	// ChapterLearningItemView ChapterTopController::vocabularyItem
	ChapterLearningItemView_t1984184315 * ___vocabularyItem_12;
	// ChapterLearningItemView ChapterTopController::sentenceItem
	ChapterLearningItemView_t1984184315 * ___sentenceItem_13;
	// ChapterLearningItemView ChapterTopController::listeningItem
	ChapterLearningItemView_t1984184315 * ___listeningItem_14;
	// ChapterLearningItemView ChapterTopController::conversationItem
	ChapterLearningItemView_t1984184315 * ___conversationItem_15;
	// ChapterLearningItemView ChapterTopController::exerciseItem
	ChapterLearningItemView_t1984184315 * ___exerciseItem_16;
	// ChapterLearningItemView ChapterTopController::practiceItem
	ChapterLearningItemView_t1984184315 * ___practiceItem_17;
	// UnityEngine.UI.Text ChapterTopController::titleText
	Text_t356221433 * ___titleText_18;
	// UnityEngine.UI.Text ChapterTopController::subText
	Text_t356221433 * ___subText_19;
	// UnityEngine.UI.Text ChapterTopController::detailText
	Text_t356221433 * ___detailText_20;
	// UnityEngine.UI.Image ChapterTopController::backgroundImage
	Image_t2042527209 * ___backgroundImage_21;

public:
	inline static int32_t get_offset_of_itemNormal_2() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___itemNormal_2)); }
	inline GameObject_t1756533147 * get_itemNormal_2() const { return ___itemNormal_2; }
	inline GameObject_t1756533147 ** get_address_of_itemNormal_2() { return &___itemNormal_2; }
	inline void set_itemNormal_2(GameObject_t1756533147 * value)
	{
		___itemNormal_2 = value;
		Il2CppCodeGenWriteBarrier(&___itemNormal_2, value);
	}

	inline static int32_t get_offset_of_itemAuto_3() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___itemAuto_3)); }
	inline GameObject_t1756533147 * get_itemAuto_3() const { return ___itemAuto_3; }
	inline GameObject_t1756533147 ** get_address_of_itemAuto_3() { return &___itemAuto_3; }
	inline void set_itemAuto_3(GameObject_t1756533147 * value)
	{
		___itemAuto_3 = value;
		Il2CppCodeGenWriteBarrier(&___itemAuto_3, value);
	}

	inline static int32_t get_offset_of_leftContainer_4() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___leftContainer_4)); }
	inline RectTransform_t3349966182 * get_leftContainer_4() const { return ___leftContainer_4; }
	inline RectTransform_t3349966182 ** get_address_of_leftContainer_4() { return &___leftContainer_4; }
	inline void set_leftContainer_4(RectTransform_t3349966182 * value)
	{
		___leftContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___leftContainer_4, value);
	}

	inline static int32_t get_offset_of_rightContainer_5() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___rightContainer_5)); }
	inline RectTransform_t3349966182 * get_rightContainer_5() const { return ___rightContainer_5; }
	inline RectTransform_t3349966182 ** get_address_of_rightContainer_5() { return &___rightContainer_5; }
	inline void set_rightContainer_5(RectTransform_t3349966182 * value)
	{
		___rightContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___rightContainer_5, value);
	}

	inline static int32_t get_offset_of_chapterId_6() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___chapterId_6)); }
	inline int32_t get_chapterId_6() const { return ___chapterId_6; }
	inline int32_t* get_address_of_chapterId_6() { return &___chapterId_6; }
	inline void set_chapterId_6(int32_t value)
	{
		___chapterId_6 = value;
	}

	inline static int32_t get_offset_of_mainColor_7() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___mainColor_7)); }
	inline Color_t2020392075  get_mainColor_7() const { return ___mainColor_7; }
	inline Color_t2020392075 * get_address_of_mainColor_7() { return &___mainColor_7; }
	inline void set_mainColor_7(Color_t2020392075  value)
	{
		___mainColor_7 = value;
	}

	inline static int32_t get_offset_of_buttonSprite_8() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___buttonSprite_8)); }
	inline Sprite_t309593783 * get_buttonSprite_8() const { return ___buttonSprite_8; }
	inline Sprite_t309593783 ** get_address_of_buttonSprite_8() { return &___buttonSprite_8; }
	inline void set_buttonSprite_8(Sprite_t309593783 * value)
	{
		___buttonSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_8, value);
	}

	inline static int32_t get_offset_of_singleButtonSprite_9() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___singleButtonSprite_9)); }
	inline Sprite_t309593783 * get_singleButtonSprite_9() const { return ___singleButtonSprite_9; }
	inline Sprite_t309593783 ** get_address_of_singleButtonSprite_9() { return &___singleButtonSprite_9; }
	inline void set_singleButtonSprite_9(Sprite_t309593783 * value)
	{
		___singleButtonSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___singleButtonSprite_9, value);
	}

	inline static int32_t get_offset_of_autoButtonSprite_10() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___autoButtonSprite_10)); }
	inline Sprite_t309593783 * get_autoButtonSprite_10() const { return ___autoButtonSprite_10; }
	inline Sprite_t309593783 ** get_address_of_autoButtonSprite_10() { return &___autoButtonSprite_10; }
	inline void set_autoButtonSprite_10(Sprite_t309593783 * value)
	{
		___autoButtonSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___autoButtonSprite_10, value);
	}

	inline static int32_t get_offset_of_chapterNameText_11() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___chapterNameText_11)); }
	inline Text_t356221433 * get_chapterNameText_11() const { return ___chapterNameText_11; }
	inline Text_t356221433 ** get_address_of_chapterNameText_11() { return &___chapterNameText_11; }
	inline void set_chapterNameText_11(Text_t356221433 * value)
	{
		___chapterNameText_11 = value;
		Il2CppCodeGenWriteBarrier(&___chapterNameText_11, value);
	}

	inline static int32_t get_offset_of_vocabularyItem_12() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___vocabularyItem_12)); }
	inline ChapterLearningItemView_t1984184315 * get_vocabularyItem_12() const { return ___vocabularyItem_12; }
	inline ChapterLearningItemView_t1984184315 ** get_address_of_vocabularyItem_12() { return &___vocabularyItem_12; }
	inline void set_vocabularyItem_12(ChapterLearningItemView_t1984184315 * value)
	{
		___vocabularyItem_12 = value;
		Il2CppCodeGenWriteBarrier(&___vocabularyItem_12, value);
	}

	inline static int32_t get_offset_of_sentenceItem_13() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___sentenceItem_13)); }
	inline ChapterLearningItemView_t1984184315 * get_sentenceItem_13() const { return ___sentenceItem_13; }
	inline ChapterLearningItemView_t1984184315 ** get_address_of_sentenceItem_13() { return &___sentenceItem_13; }
	inline void set_sentenceItem_13(ChapterLearningItemView_t1984184315 * value)
	{
		___sentenceItem_13 = value;
		Il2CppCodeGenWriteBarrier(&___sentenceItem_13, value);
	}

	inline static int32_t get_offset_of_listeningItem_14() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___listeningItem_14)); }
	inline ChapterLearningItemView_t1984184315 * get_listeningItem_14() const { return ___listeningItem_14; }
	inline ChapterLearningItemView_t1984184315 ** get_address_of_listeningItem_14() { return &___listeningItem_14; }
	inline void set_listeningItem_14(ChapterLearningItemView_t1984184315 * value)
	{
		___listeningItem_14 = value;
		Il2CppCodeGenWriteBarrier(&___listeningItem_14, value);
	}

	inline static int32_t get_offset_of_conversationItem_15() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___conversationItem_15)); }
	inline ChapterLearningItemView_t1984184315 * get_conversationItem_15() const { return ___conversationItem_15; }
	inline ChapterLearningItemView_t1984184315 ** get_address_of_conversationItem_15() { return &___conversationItem_15; }
	inline void set_conversationItem_15(ChapterLearningItemView_t1984184315 * value)
	{
		___conversationItem_15 = value;
		Il2CppCodeGenWriteBarrier(&___conversationItem_15, value);
	}

	inline static int32_t get_offset_of_exerciseItem_16() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___exerciseItem_16)); }
	inline ChapterLearningItemView_t1984184315 * get_exerciseItem_16() const { return ___exerciseItem_16; }
	inline ChapterLearningItemView_t1984184315 ** get_address_of_exerciseItem_16() { return &___exerciseItem_16; }
	inline void set_exerciseItem_16(ChapterLearningItemView_t1984184315 * value)
	{
		___exerciseItem_16 = value;
		Il2CppCodeGenWriteBarrier(&___exerciseItem_16, value);
	}

	inline static int32_t get_offset_of_practiceItem_17() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___practiceItem_17)); }
	inline ChapterLearningItemView_t1984184315 * get_practiceItem_17() const { return ___practiceItem_17; }
	inline ChapterLearningItemView_t1984184315 ** get_address_of_practiceItem_17() { return &___practiceItem_17; }
	inline void set_practiceItem_17(ChapterLearningItemView_t1984184315 * value)
	{
		___practiceItem_17 = value;
		Il2CppCodeGenWriteBarrier(&___practiceItem_17, value);
	}

	inline static int32_t get_offset_of_titleText_18() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___titleText_18)); }
	inline Text_t356221433 * get_titleText_18() const { return ___titleText_18; }
	inline Text_t356221433 ** get_address_of_titleText_18() { return &___titleText_18; }
	inline void set_titleText_18(Text_t356221433 * value)
	{
		___titleText_18 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_18, value);
	}

	inline static int32_t get_offset_of_subText_19() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___subText_19)); }
	inline Text_t356221433 * get_subText_19() const { return ___subText_19; }
	inline Text_t356221433 ** get_address_of_subText_19() { return &___subText_19; }
	inline void set_subText_19(Text_t356221433 * value)
	{
		___subText_19 = value;
		Il2CppCodeGenWriteBarrier(&___subText_19, value);
	}

	inline static int32_t get_offset_of_detailText_20() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___detailText_20)); }
	inline Text_t356221433 * get_detailText_20() const { return ___detailText_20; }
	inline Text_t356221433 ** get_address_of_detailText_20() { return &___detailText_20; }
	inline void set_detailText_20(Text_t356221433 * value)
	{
		___detailText_20 = value;
		Il2CppCodeGenWriteBarrier(&___detailText_20, value);
	}

	inline static int32_t get_offset_of_backgroundImage_21() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144, ___backgroundImage_21)); }
	inline Image_t2042527209 * get_backgroundImage_21() const { return ___backgroundImage_21; }
	inline Image_t2042527209 ** get_address_of_backgroundImage_21() { return &___backgroundImage_21; }
	inline void set_backgroundImage_21(Image_t2042527209 * value)
	{
		___backgroundImage_21 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImage_21, value);
	}
};

struct ChapterTopController_t2392392144_StaticFields
{
public:
	// UnityEngine.Events.UnityAction ChapterTopController::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_22;
	// UnityEngine.Events.UnityAction ChapterTopController::<>f__am$cache1
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache1_23;
	// UnityEngine.Events.UnityAction ChapterTopController::<>f__am$cache2
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache2_24;
	// UnityEngine.Events.UnityAction ChapterTopController::<>f__am$cache3
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache3_25;
	// UnityEngine.Events.UnityAction ChapterTopController::<>f__am$cache4
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache4_26;
	// System.Action ChapterTopController::<>f__am$cache5
	Action_t3226471752 * ___U3CU3Ef__amU24cache5_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_23() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144_StaticFields, ___U3CU3Ef__amU24cache1_23)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache1_23() const { return ___U3CU3Ef__amU24cache1_23; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache1_23() { return &___U3CU3Ef__amU24cache1_23; }
	inline void set_U3CU3Ef__amU24cache1_23(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_24() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144_StaticFields, ___U3CU3Ef__amU24cache2_24)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache2_24() const { return ___U3CU3Ef__amU24cache2_24; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache2_24() { return &___U3CU3Ef__amU24cache2_24; }
	inline void set_U3CU3Ef__amU24cache2_24(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache2_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_25() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144_StaticFields, ___U3CU3Ef__amU24cache3_25)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache3_25() const { return ___U3CU3Ef__amU24cache3_25; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache3_25() { return &___U3CU3Ef__amU24cache3_25; }
	inline void set_U3CU3Ef__amU24cache3_25(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache3_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_26() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144_StaticFields, ___U3CU3Ef__amU24cache4_26)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache4_26() const { return ___U3CU3Ef__amU24cache4_26; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache4_26() { return &___U3CU3Ef__amU24cache4_26; }
	inline void set_U3CU3Ef__amU24cache4_26(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache4_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_27() { return static_cast<int32_t>(offsetof(ChapterTopController_t2392392144_StaticFields, ___U3CU3Ef__amU24cache5_27)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache5_27() const { return ___U3CU3Ef__amU24cache5_27; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache5_27() { return &___U3CU3Ef__amU24cache5_27; }
	inline void set_U3CU3Ef__amU24cache5_27(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache5_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
