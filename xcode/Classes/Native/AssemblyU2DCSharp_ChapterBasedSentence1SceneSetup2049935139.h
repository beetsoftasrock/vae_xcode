﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Sentence1DataLoader
struct Sentence1DataLoader_t1039713923;
// Sentence1SceneController
struct Sentence1SceneController_t2055584412;

#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterBasedSentence1SceneSetup
struct  ChapterBasedSentence1SceneSetup_t2049935139  : public GlobalBasedSetupModule_t1655890765
{
public:
	// Sentence1DataLoader ChapterBasedSentence1SceneSetup::dataLoader
	Sentence1DataLoader_t1039713923 * ___dataLoader_3;
	// Sentence1SceneController ChapterBasedSentence1SceneSetup::sceneController
	Sentence1SceneController_t2055584412 * ___sceneController_4;

public:
	inline static int32_t get_offset_of_dataLoader_3() { return static_cast<int32_t>(offsetof(ChapterBasedSentence1SceneSetup_t2049935139, ___dataLoader_3)); }
	inline Sentence1DataLoader_t1039713923 * get_dataLoader_3() const { return ___dataLoader_3; }
	inline Sentence1DataLoader_t1039713923 ** get_address_of_dataLoader_3() { return &___dataLoader_3; }
	inline void set_dataLoader_3(Sentence1DataLoader_t1039713923 * value)
	{
		___dataLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_3, value);
	}

	inline static int32_t get_offset_of_sceneController_4() { return static_cast<int32_t>(offsetof(ChapterBasedSentence1SceneSetup_t2049935139, ___sceneController_4)); }
	inline Sentence1SceneController_t2055584412 * get_sceneController_4() const { return ___sceneController_4; }
	inline Sentence1SceneController_t2055584412 ** get_address_of_sceneController_4() { return &___sceneController_4; }
	inline void set_sceneController_4(Sentence1SceneController_t2055584412 * value)
	{
		___sceneController_4 = value;
		Il2CppCodeGenWriteBarrier(&___sceneController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
