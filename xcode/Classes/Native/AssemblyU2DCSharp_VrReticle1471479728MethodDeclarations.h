﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VrReticle
struct VrReticle_t1471479728;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void VrReticle::.ctor()
extern "C"  void VrReticle__ctor_m1355293563 (VrReticle_t1471479728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VrReticle::OnEnable()
extern "C"  void VrReticle_OnEnable_m1157568439 (VrReticle_t1471479728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VrReticle::Start()
extern "C"  void VrReticle_Start_m1606971895 (VrReticle_t1471479728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VrReticle::Reset()
extern "C"  void VrReticle_Reset_m1915839320 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VrReticle::ResetTransform(UnityEngine.Transform)
extern "C"  void VrReticle_ResetTransform_m3552384621 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___ts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VrReticle::Update()
extern "C"  void VrReticle_Update_m23743758 (VrReticle_t1471479728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VrReticle::SetObject(UnityEngine.GameObject)
extern "C"  void VrReticle_SetObject_m2473783130 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VrReticle::.cctor()
extern "C"  void VrReticle__cctor_m2979078470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
