﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabGroup
struct TabGroup_t2567651434;

#include "codegen/il2cpp-codegen.h"

// System.Void TabGroup::.ctor()
extern "C"  void TabGroup__ctor_m3740543551 (TabGroup_t2567651434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
