﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConstantConfig
struct ConstantConfig_t1104991690;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ConstantConfig::.ctor()
extern "C"  void ConstantConfig__ctor_m1451824939 (ConstantConfig_t1104991690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_API_SERVER_LINK()
extern "C"  String_t* ConstantConfig_get_API_SERVER_LINK_m324923394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_REGISTER_API_URL()
extern "C"  String_t* ConstantConfig_get_REGISTER_API_URL_m812689473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_EDIT_API_URL()
extern "C"  String_t* ConstantConfig_get_EDIT_API_URL_m2789525016 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_GET_API_URL()
extern "C"  String_t* ConstantConfig_get_GET_API_URL_m1033710056 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_CHANGE_DEVICE_URL()
extern "C"  String_t* ConstantConfig_get_CHANGE_DEVICE_URL_m4045983480 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_GET_TRANSFER_CODE()
extern "C"  String_t* ConstantConfig_get_GET_TRANSFER_CODE_m3752858643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_GET_USER_INFO()
extern "C"  String_t* ConstantConfig_get_GET_USER_INFO_m3893743462 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_GET_LEARNING_DATAS()
extern "C"  String_t* ConstantConfig_get_GET_LEARNING_DATAS_m949877076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_GET_LEARNING_LOG_CHAPTER()
extern "C"  String_t* ConstantConfig_get_GET_LEARNING_LOG_CHAPTER_m2598175715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_GET_LEARNING_LOG_CHAPTER_CATEGORY()
extern "C"  String_t* ConstantConfig_get_GET_LEARNING_LOG_CHAPTER_CATEGORY_m1727975708 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_GET_LEARNING_LOG()
extern "C"  String_t* ConstantConfig_get_GET_LEARNING_LOG_m218614069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_RESET_DATA()
extern "C"  String_t* ConstantConfig_get_RESET_DATA_m1819026769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_PUSH_ZIP_URL()
extern "C"  String_t* ConstantConfig_get_PUSH_ZIP_URL_m3115152697 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_DEFAULT_PUSH_RESULT_API_URL()
extern "C"  String_t* ConstantConfig_get_DEFAULT_PUSH_RESULT_API_URL_m695929990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConstantConfig::get_DEFAULT_PUSH_ZIP_API_URL()
extern "C"  String_t* ConstantConfig_get_DEFAULT_PUSH_ZIP_API_URL_m3121505266 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstantConfig::.cctor()
extern "C"  void ConstantConfig__cctor_m2036728188 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
