﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultStatisticChart
struct ResultStatisticChart_t3759290537;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultStatisticChart::.ctor()
extern "C"  void ResultStatisticChart__ctor_m1239321706 (ResultStatisticChart_t3759290537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
