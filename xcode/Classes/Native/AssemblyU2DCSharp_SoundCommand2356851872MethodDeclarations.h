﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundCommand
struct SoundCommand_t2356851872;
// System.String
struct String_t;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// ConversationLogicController
struct ConversationLogicController_t3911886281;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ConversationLogicController3911886281.h"

// System.Void SoundCommand::.ctor(System.String,System.Collections.Generic.List`1<CommandSheet/Param>,System.Int32)
extern "C"  void SoundCommand__ctor_m3947887096 (SoundCommand_t2356851872 * __this, String_t* ___commandName0, List_1_t3492939606 * ___levelSheet1, int32_t ___currentPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundCommand::Revert(ConversationLogicController)
extern "C"  void SoundCommand_Revert_m3868323736 (SoundCommand_t2356851872 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SoundCommand::AutoExecute(ConversationLogicController)
extern "C"  Il2CppObject * SoundCommand_AutoExecute_m70076064 (SoundCommand_t2356851872 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SoundCommand::Execute(ConversationLogicController)
extern "C"  Il2CppObject * SoundCommand_Execute_m2804222073 (SoundCommand_t2356851872 * __this, ConversationLogicController_t3911886281 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SoundCommand::GetRole()
extern "C"  String_t* SoundCommand_GetRole_m1922869540 (SoundCommand_t2356851872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
