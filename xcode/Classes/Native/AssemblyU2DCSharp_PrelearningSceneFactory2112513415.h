﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_BaseSettingFactory22059301.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrelearningSceneFactory
struct  PrelearningSceneFactory_t2112513415  : public BaseSettingFactory_t22059301
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
