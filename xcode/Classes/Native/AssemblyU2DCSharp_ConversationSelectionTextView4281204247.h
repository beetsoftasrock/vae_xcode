﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSelectionTextView
struct  ConversationSelectionTextView_t4281204247  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ConversationSelectionTextView::text
	Text_t356221433 * ___text_3;
	// UnityEngine.UI.Text ConversationSelectionTextView::subText
	Text_t356221433 * ___subText_4;
	// System.String ConversationSelectionTextView::_textEN
	String_t* ____textEN_5;
	// System.String ConversationSelectionTextView::_textJP
	String_t* ____textJP_6;
	// System.Boolean ConversationSelectionTextView::_showJP
	bool ____showJP_7;

public:
	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(ConversationSelectionTextView_t4281204247, ___text_3)); }
	inline Text_t356221433 * get_text_3() const { return ___text_3; }
	inline Text_t356221433 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(Text_t356221433 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier(&___text_3, value);
	}

	inline static int32_t get_offset_of_subText_4() { return static_cast<int32_t>(offsetof(ConversationSelectionTextView_t4281204247, ___subText_4)); }
	inline Text_t356221433 * get_subText_4() const { return ___subText_4; }
	inline Text_t356221433 ** get_address_of_subText_4() { return &___subText_4; }
	inline void set_subText_4(Text_t356221433 * value)
	{
		___subText_4 = value;
		Il2CppCodeGenWriteBarrier(&___subText_4, value);
	}

	inline static int32_t get_offset_of__textEN_5() { return static_cast<int32_t>(offsetof(ConversationSelectionTextView_t4281204247, ____textEN_5)); }
	inline String_t* get__textEN_5() const { return ____textEN_5; }
	inline String_t** get_address_of__textEN_5() { return &____textEN_5; }
	inline void set__textEN_5(String_t* value)
	{
		____textEN_5 = value;
		Il2CppCodeGenWriteBarrier(&____textEN_5, value);
	}

	inline static int32_t get_offset_of__textJP_6() { return static_cast<int32_t>(offsetof(ConversationSelectionTextView_t4281204247, ____textJP_6)); }
	inline String_t* get__textJP_6() const { return ____textJP_6; }
	inline String_t** get_address_of__textJP_6() { return &____textJP_6; }
	inline void set__textJP_6(String_t* value)
	{
		____textJP_6 = value;
		Il2CppCodeGenWriteBarrier(&____textJP_6, value);
	}

	inline static int32_t get_offset_of__showJP_7() { return static_cast<int32_t>(offsetof(ConversationSelectionTextView_t4281204247, ____showJP_7)); }
	inline bool get__showJP_7() const { return ____showJP_7; }
	inline bool* get_address_of__showJP_7() { return &____showJP_7; }
	inline void set__showJP_7(bool value)
	{
		____showJP_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
