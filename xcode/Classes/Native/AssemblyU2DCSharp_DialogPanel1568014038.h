﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Dialog
struct Dialog_t1378192732;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogPanel
struct  DialogPanel_t1568014038  : public MonoBehaviour_t1158329972
{
public:
	// Dialog DialogPanel::dialog
	Dialog_t1378192732 * ___dialog_2;
	// UnityEngine.UI.Text DialogPanel::txt_Eng
	Text_t356221433 * ___txt_Eng_3;
	// UnityEngine.UI.Text DialogPanel::txt_Jp
	Text_t356221433 * ___txt_Jp_4;
	// System.String DialogPanel::Type
	String_t* ___Type_5;
	// UnityEngine.AudioSource DialogPanel::audioSource
	AudioSource_t1135106623 * ___audioSource_6;

public:
	inline static int32_t get_offset_of_dialog_2() { return static_cast<int32_t>(offsetof(DialogPanel_t1568014038, ___dialog_2)); }
	inline Dialog_t1378192732 * get_dialog_2() const { return ___dialog_2; }
	inline Dialog_t1378192732 ** get_address_of_dialog_2() { return &___dialog_2; }
	inline void set_dialog_2(Dialog_t1378192732 * value)
	{
		___dialog_2 = value;
		Il2CppCodeGenWriteBarrier(&___dialog_2, value);
	}

	inline static int32_t get_offset_of_txt_Eng_3() { return static_cast<int32_t>(offsetof(DialogPanel_t1568014038, ___txt_Eng_3)); }
	inline Text_t356221433 * get_txt_Eng_3() const { return ___txt_Eng_3; }
	inline Text_t356221433 ** get_address_of_txt_Eng_3() { return &___txt_Eng_3; }
	inline void set_txt_Eng_3(Text_t356221433 * value)
	{
		___txt_Eng_3 = value;
		Il2CppCodeGenWriteBarrier(&___txt_Eng_3, value);
	}

	inline static int32_t get_offset_of_txt_Jp_4() { return static_cast<int32_t>(offsetof(DialogPanel_t1568014038, ___txt_Jp_4)); }
	inline Text_t356221433 * get_txt_Jp_4() const { return ___txt_Jp_4; }
	inline Text_t356221433 ** get_address_of_txt_Jp_4() { return &___txt_Jp_4; }
	inline void set_txt_Jp_4(Text_t356221433 * value)
	{
		___txt_Jp_4 = value;
		Il2CppCodeGenWriteBarrier(&___txt_Jp_4, value);
	}

	inline static int32_t get_offset_of_Type_5() { return static_cast<int32_t>(offsetof(DialogPanel_t1568014038, ___Type_5)); }
	inline String_t* get_Type_5() const { return ___Type_5; }
	inline String_t** get_address_of_Type_5() { return &___Type_5; }
	inline void set_Type_5(String_t* value)
	{
		___Type_5 = value;
		Il2CppCodeGenWriteBarrier(&___Type_5, value);
	}

	inline static int32_t get_offset_of_audioSource_6() { return static_cast<int32_t>(offsetof(DialogPanel_t1568014038, ___audioSource_6)); }
	inline AudioSource_t1135106623 * get_audioSource_6() const { return ___audioSource_6; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_6() { return &___audioSource_6; }
	inline void set_audioSource_6(AudioSource_t1135106623 * value)
	{
		___audioSource_6 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
