﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextKaraoke
struct TextKaraoke_t2450194175;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void TextKaraoke::.ctor()
extern "C"  void TextKaraoke__ctor_m1803632362 (TextKaraoke_t2450194175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextKaraoke::OnEnable()
extern "C"  void TextKaraoke_OnEnable_m2052673030 (TextKaraoke_t2450194175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextKaraoke::CreateTextKaraoke()
extern "C"  void TextKaraoke_CreateTextKaraoke_m2937641187 (TextKaraoke_t2450194175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextKaraoke::StartKaraoke(UnityEngine.AudioSource)
extern "C"  void TextKaraoke_StartKaraoke_m1172329642 (TextKaraoke_t2450194175 * __this, AudioSource_t1135106623 * ___audioSource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextKaraoke::StartKaraoke(System.String)
extern "C"  void TextKaraoke_StartKaraoke_m4048443264 (TextKaraoke_t2450194175 * __this, String_t* ___voice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TextKaraoke::AutoTyping(System.String)
extern "C"  Il2CppObject * TextKaraoke_AutoTyping_m3783292678 (TextKaraoke_t2450194175 * __this, String_t* ___voice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TextKaraoke::AutoTyping(UnityEngine.AudioSource)
extern "C"  Il2CppObject * TextKaraoke_AutoTyping_m2541641464 (TextKaraoke_t2450194175 * __this, AudioSource_t1135106623 * ___audioSource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TextKaraoke::ReplaceAt(System.String,System.Int32,System.String)
extern "C"  String_t* TextKaraoke_ReplaceAt_m4101082119 (TextKaraoke_t2450194175 * __this, String_t* ___input0, int32_t ___index1, String_t* ___newChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TextKaraoke::ToRGBHex(UnityEngine.Color)
extern "C"  String_t* TextKaraoke_ToRGBHex_m1098989870 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte TextKaraoke::ToByte(System.Single)
extern "C"  uint8_t TextKaraoke_ToByte_m3132195448 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
