﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// SoundData
struct SoundData_t3344892245;

#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundCommand
struct  SoundCommand_t2356851872  : public ConversationCommand_t3660105836
{
public:
	// System.Int32 SoundCommand::_preCurrentPoint
	int32_t ____preCurrentPoint_0;
	// System.String SoundCommand::commandName
	String_t* ___commandName_1;
	// SoundData SoundCommand::soundData
	SoundData_t3344892245 * ___soundData_2;

public:
	inline static int32_t get_offset_of__preCurrentPoint_0() { return static_cast<int32_t>(offsetof(SoundCommand_t2356851872, ____preCurrentPoint_0)); }
	inline int32_t get__preCurrentPoint_0() const { return ____preCurrentPoint_0; }
	inline int32_t* get_address_of__preCurrentPoint_0() { return &____preCurrentPoint_0; }
	inline void set__preCurrentPoint_0(int32_t value)
	{
		____preCurrentPoint_0 = value;
	}

	inline static int32_t get_offset_of_commandName_1() { return static_cast<int32_t>(offsetof(SoundCommand_t2356851872, ___commandName_1)); }
	inline String_t* get_commandName_1() const { return ___commandName_1; }
	inline String_t** get_address_of_commandName_1() { return &___commandName_1; }
	inline void set_commandName_1(String_t* value)
	{
		___commandName_1 = value;
		Il2CppCodeGenWriteBarrier(&___commandName_1, value);
	}

	inline static int32_t get_offset_of_soundData_2() { return static_cast<int32_t>(offsetof(SoundCommand_t2356851872, ___soundData_2)); }
	inline SoundData_t3344892245 * get_soundData_2() const { return ___soundData_2; }
	inline SoundData_t3344892245 ** get_address_of_soundData_2() { return &___soundData_2; }
	inline void set_soundData_2(SoundData_t3344892245 * value)
	{
		___soundData_2 = value;
		Il2CppCodeGenWriteBarrier(&___soundData_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
