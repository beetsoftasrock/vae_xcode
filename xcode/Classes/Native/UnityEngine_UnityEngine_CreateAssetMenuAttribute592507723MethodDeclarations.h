﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t592507723;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
extern "C"  void CreateAssetMenuAttribute__ctor_m2684223354 (CreateAssetMenuAttribute_t592507723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
extern "C"  void CreateAssetMenuAttribute_set_menuName_m616170585 (CreateAssetMenuAttribute_t592507723 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
extern "C"  void CreateAssetMenuAttribute_set_fileName_m3630143102 (CreateAssetMenuAttribute_t592507723 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
extern "C"  void CreateAssetMenuAttribute_set_order_m3532658672 (CreateAssetMenuAttribute_t592507723 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
