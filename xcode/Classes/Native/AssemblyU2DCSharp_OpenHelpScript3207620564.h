﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<OpenHelpScript/TrackIndexObject>
struct List_1_t465305223;
// ManageATutorial
struct ManageATutorial_t1512139496;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenHelpScript
struct  OpenHelpScript_t3207620564  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform[] OpenHelpScript::dockHands
	TransformU5BU5D_t3764228911* ___dockHands_2;
	// System.Collections.Generic.List`1<UnityEngine.Transform> OpenHelpScript::listObjectsHighlightFront
	List_1_t2644239190 * ___listObjectsHighlightFront_3;
	// System.Collections.Generic.List`1<UnityEngine.Transform> OpenHelpScript::listObjectHighlightBehind
	List_1_t2644239190 * ___listObjectHighlightBehind_4;
	// UnityEngine.UI.Button OpenHelpScript::nextButtonOverlay
	Button_t2872111280 * ___nextButtonOverlay_5;
	// UnityEngine.Transform OpenHelpScript::parentObjectsHighlightBeFront
	Transform_t3275118058 * ___parentObjectsHighlightBeFront_6;
	// System.Collections.Generic.List`1<OpenHelpScript/TrackIndexObject> OpenHelpScript::liTrackOriginIndexObjects
	List_1_t465305223 * ___liTrackOriginIndexObjects_7;
	// System.Boolean OpenHelpScript::isShowCustomButton
	bool ___isShowCustomButton_8;
	// ManageATutorial OpenHelpScript::<ManageHelp>k__BackingField
	ManageATutorial_t1512139496 * ___U3CManageHelpU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_dockHands_2() { return static_cast<int32_t>(offsetof(OpenHelpScript_t3207620564, ___dockHands_2)); }
	inline TransformU5BU5D_t3764228911* get_dockHands_2() const { return ___dockHands_2; }
	inline TransformU5BU5D_t3764228911** get_address_of_dockHands_2() { return &___dockHands_2; }
	inline void set_dockHands_2(TransformU5BU5D_t3764228911* value)
	{
		___dockHands_2 = value;
		Il2CppCodeGenWriteBarrier(&___dockHands_2, value);
	}

	inline static int32_t get_offset_of_listObjectsHighlightFront_3() { return static_cast<int32_t>(offsetof(OpenHelpScript_t3207620564, ___listObjectsHighlightFront_3)); }
	inline List_1_t2644239190 * get_listObjectsHighlightFront_3() const { return ___listObjectsHighlightFront_3; }
	inline List_1_t2644239190 ** get_address_of_listObjectsHighlightFront_3() { return &___listObjectsHighlightFront_3; }
	inline void set_listObjectsHighlightFront_3(List_1_t2644239190 * value)
	{
		___listObjectsHighlightFront_3 = value;
		Il2CppCodeGenWriteBarrier(&___listObjectsHighlightFront_3, value);
	}

	inline static int32_t get_offset_of_listObjectHighlightBehind_4() { return static_cast<int32_t>(offsetof(OpenHelpScript_t3207620564, ___listObjectHighlightBehind_4)); }
	inline List_1_t2644239190 * get_listObjectHighlightBehind_4() const { return ___listObjectHighlightBehind_4; }
	inline List_1_t2644239190 ** get_address_of_listObjectHighlightBehind_4() { return &___listObjectHighlightBehind_4; }
	inline void set_listObjectHighlightBehind_4(List_1_t2644239190 * value)
	{
		___listObjectHighlightBehind_4 = value;
		Il2CppCodeGenWriteBarrier(&___listObjectHighlightBehind_4, value);
	}

	inline static int32_t get_offset_of_nextButtonOverlay_5() { return static_cast<int32_t>(offsetof(OpenHelpScript_t3207620564, ___nextButtonOverlay_5)); }
	inline Button_t2872111280 * get_nextButtonOverlay_5() const { return ___nextButtonOverlay_5; }
	inline Button_t2872111280 ** get_address_of_nextButtonOverlay_5() { return &___nextButtonOverlay_5; }
	inline void set_nextButtonOverlay_5(Button_t2872111280 * value)
	{
		___nextButtonOverlay_5 = value;
		Il2CppCodeGenWriteBarrier(&___nextButtonOverlay_5, value);
	}

	inline static int32_t get_offset_of_parentObjectsHighlightBeFront_6() { return static_cast<int32_t>(offsetof(OpenHelpScript_t3207620564, ___parentObjectsHighlightBeFront_6)); }
	inline Transform_t3275118058 * get_parentObjectsHighlightBeFront_6() const { return ___parentObjectsHighlightBeFront_6; }
	inline Transform_t3275118058 ** get_address_of_parentObjectsHighlightBeFront_6() { return &___parentObjectsHighlightBeFront_6; }
	inline void set_parentObjectsHighlightBeFront_6(Transform_t3275118058 * value)
	{
		___parentObjectsHighlightBeFront_6 = value;
		Il2CppCodeGenWriteBarrier(&___parentObjectsHighlightBeFront_6, value);
	}

	inline static int32_t get_offset_of_liTrackOriginIndexObjects_7() { return static_cast<int32_t>(offsetof(OpenHelpScript_t3207620564, ___liTrackOriginIndexObjects_7)); }
	inline List_1_t465305223 * get_liTrackOriginIndexObjects_7() const { return ___liTrackOriginIndexObjects_7; }
	inline List_1_t465305223 ** get_address_of_liTrackOriginIndexObjects_7() { return &___liTrackOriginIndexObjects_7; }
	inline void set_liTrackOriginIndexObjects_7(List_1_t465305223 * value)
	{
		___liTrackOriginIndexObjects_7 = value;
		Il2CppCodeGenWriteBarrier(&___liTrackOriginIndexObjects_7, value);
	}

	inline static int32_t get_offset_of_isShowCustomButton_8() { return static_cast<int32_t>(offsetof(OpenHelpScript_t3207620564, ___isShowCustomButton_8)); }
	inline bool get_isShowCustomButton_8() const { return ___isShowCustomButton_8; }
	inline bool* get_address_of_isShowCustomButton_8() { return &___isShowCustomButton_8; }
	inline void set_isShowCustomButton_8(bool value)
	{
		___isShowCustomButton_8 = value;
	}

	inline static int32_t get_offset_of_U3CManageHelpU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(OpenHelpScript_t3207620564, ___U3CManageHelpU3Ek__BackingField_9)); }
	inline ManageATutorial_t1512139496 * get_U3CManageHelpU3Ek__BackingField_9() const { return ___U3CManageHelpU3Ek__BackingField_9; }
	inline ManageATutorial_t1512139496 ** get_address_of_U3CManageHelpU3Ek__BackingField_9() { return &___U3CManageHelpU3Ek__BackingField_9; }
	inline void set_U3CManageHelpU3Ek__BackingField_9(ManageATutorial_t1512139496 * value)
	{
		___U3CManageHelpU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CManageHelpU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
