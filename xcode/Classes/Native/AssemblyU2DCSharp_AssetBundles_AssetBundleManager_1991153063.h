﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1
struct U3CloadAssetBundlesU3Ec__Iterator1_t2422683914;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8
struct  U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063  : public Il2CppObject
{
public:
	// System.String[] AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8::bundleVariants
	StringU5BU5D_t1642385972* ___bundleVariants_0;
	// System.Action`1<System.Single> AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8::onProcess
	Action_1_t1878309314 * ___onProcess_1;
	// System.Collections.Generic.List`1<System.String> AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8::allBundles
	List_1_t1398341365 * ___allBundles_2;
	// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1 AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8::<>f__ref$1
	U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * ___U3CU3Ef__refU241_3;

public:
	inline static int32_t get_offset_of_bundleVariants_0() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063, ___bundleVariants_0)); }
	inline StringU5BU5D_t1642385972* get_bundleVariants_0() const { return ___bundleVariants_0; }
	inline StringU5BU5D_t1642385972** get_address_of_bundleVariants_0() { return &___bundleVariants_0; }
	inline void set_bundleVariants_0(StringU5BU5D_t1642385972* value)
	{
		___bundleVariants_0 = value;
		Il2CppCodeGenWriteBarrier(&___bundleVariants_0, value);
	}

	inline static int32_t get_offset_of_onProcess_1() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063, ___onProcess_1)); }
	inline Action_1_t1878309314 * get_onProcess_1() const { return ___onProcess_1; }
	inline Action_1_t1878309314 ** get_address_of_onProcess_1() { return &___onProcess_1; }
	inline void set_onProcess_1(Action_1_t1878309314 * value)
	{
		___onProcess_1 = value;
		Il2CppCodeGenWriteBarrier(&___onProcess_1, value);
	}

	inline static int32_t get_offset_of_allBundles_2() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063, ___allBundles_2)); }
	inline List_1_t1398341365 * get_allBundles_2() const { return ___allBundles_2; }
	inline List_1_t1398341365 ** get_address_of_allBundles_2() { return &___allBundles_2; }
	inline void set_allBundles_2(List_1_t1398341365 * value)
	{
		___allBundles_2 = value;
		Il2CppCodeGenWriteBarrier(&___allBundles_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_3() { return static_cast<int32_t>(offsetof(U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063, ___U3CU3Ef__refU241_3)); }
	inline U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * get_U3CU3Ef__refU241_3() const { return ___U3CU3Ef__refU241_3; }
	inline U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 ** get_address_of_U3CU3Ef__refU241_3() { return &___U3CU3Ef__refU241_3; }
	inline void set_U3CU3Ef__refU241_3(U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * value)
	{
		___U3CU3Ef__refU241_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
