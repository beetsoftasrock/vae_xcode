﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m390444870(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t896118630 *, String_t*, Dictionary_2_t1223994146 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_Key()
#define KeyValuePair_2_get_Key_m2287669112(__this, method) ((  String_t* (*) (KeyValuePair_2_t896118630 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3734400185(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t896118630 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_Value()
#define KeyValuePair_2_get_Value_m4187719416(__this, method) ((  Dictionary_2_t1223994146 * (*) (KeyValuePair_2_t896118630 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3057132145(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t896118630 *, Dictionary_2_t1223994146 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::ToString()
#define KeyValuePair_2_ToString_m646925947(__this, method) ((  String_t* (*) (KeyValuePair_2_t896118630 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
