﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GraphValue
struct  GraphValue_t2739901601 
{
public:
	// System.String GraphValue::name
	String_t* ___name_0;
	// System.Single GraphValue::percent
	float ___percent_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GraphValue_t2739901601, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_percent_1() { return static_cast<int32_t>(offsetof(GraphValue_t2739901601, ___percent_1)); }
	inline float get_percent_1() const { return ___percent_1; }
	inline float* get_address_of_percent_1() { return &___percent_1; }
	inline void set_percent_1(float value)
	{
		___percent_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GraphValue
struct GraphValue_t2739901601_marshaled_pinvoke
{
	char* ___name_0;
	float ___percent_1;
};
// Native definition for COM marshalling of GraphValue
struct GraphValue_t2739901601_marshaled_com
{
	Il2CppChar* ___name_0;
	float ___percent_1;
};
