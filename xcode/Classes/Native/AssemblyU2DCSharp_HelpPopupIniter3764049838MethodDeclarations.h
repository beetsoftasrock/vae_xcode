﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HelpPopupIniter
struct HelpPopupIniter_t3764049838;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"

// System.Void HelpPopupIniter::.ctor()
extern "C"  void HelpPopupIniter__ctor_m858460765 (HelpPopupIniter_t3764049838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopupIniter::Awake()
extern "C"  void HelpPopupIniter_Awake_m2230182926 (HelpPopupIniter_t3764049838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HelpPopupIniter::<Awake>m__0(UnityEngine.Sprite)
extern "C"  bool HelpPopupIniter_U3CAwakeU3Em__0_m2684041909 (HelpPopupIniter_t3764049838 * __this, Sprite_t309593783 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
