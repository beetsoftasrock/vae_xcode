﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalConfig
struct GlobalConfig_t3080413471;
// com.beetsoft.utility.SoundManager
struct SoundManager_t1001218626;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// com.beetsoft.utility.SoundManager
struct  SoundManager_t1001218626  : public MonoBehaviour_t1158329972
{
public:
	// GlobalConfig com.beetsoft.utility.SoundManager::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_2;

public:
	inline static int32_t get_offset_of_globalConfig_2() { return static_cast<int32_t>(offsetof(SoundManager_t1001218626, ___globalConfig_2)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_2() const { return ___globalConfig_2; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_2() { return &___globalConfig_2; }
	inline void set_globalConfig_2(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_2 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_2, value);
	}
};

struct SoundManager_t1001218626_StaticFields
{
public:
	// com.beetsoft.utility.SoundManager com.beetsoft.utility.SoundManager::_instance
	SoundManager_t1001218626 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(SoundManager_t1001218626_StaticFields, ____instance_3)); }
	inline SoundManager_t1001218626 * get__instance_3() const { return ____instance_3; }
	inline SoundManager_t1001218626 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(SoundManager_t1001218626 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
