﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputCodeScript
struct InputCodeScript_t3672565358;
// System.String
struct String_t;
// TransferCodeResponse
struct TransferCodeResponse_t1278852611;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_TransferCodeResponse1278852611.h"

// System.Void InputCodeScript::.ctor()
extern "C"  void InputCodeScript__ctor_m613701935 (InputCodeScript_t3672565358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputCodeScript::Start()
extern "C"  void InputCodeScript_Start_m1511522675 (InputCodeScript_t3672565358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputCodeScript::CopyCodeToClipBoard()
extern "C"  void InputCodeScript_CopyCodeToClipBoard_m1270179240 (InputCodeScript_t3672565358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputCodeScript::OnOffPopupID()
extern "C"  void InputCodeScript_OnOffPopupID_m8073988 (InputCodeScript_t3672565358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputCodeScript::OpenPopup()
extern "C"  void InputCodeScript_OpenPopup_m4246451697 (InputCodeScript_t3672565358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputCodeScript::GetTransferCode(System.String)
extern "C"  void InputCodeScript_GetTransferCode_m3804805873 (InputCodeScript_t3672565358 * __this, String_t* ___uuid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputCodeScript::HandleGetTransferCodeSuccess(TransferCodeResponse)
extern "C"  void InputCodeScript_HandleGetTransferCodeSuccess_m2750892107 (InputCodeScript_t3672565358 * __this, TransferCodeResponse_t1278852611 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputCodeScript::HandleGetTransferCodeError()
extern "C"  void InputCodeScript_HandleGetTransferCodeError_m1582204209 (InputCodeScript_t3672565358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
