﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResultRateView
struct ResultRateView_t1401090222;
// ResultRateSlider
struct ResultRateSlider_t127081558;
// ResultRateParam
struct ResultRateParam_t3783906052;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultRateController
struct  ResultRateController_t2087696941  : public MonoBehaviour_t1158329972
{
public:
	// ResultRateView ResultRateController::view
	ResultRateView_t1401090222 * ___view_2;
	// ResultRateSlider ResultRateController::slider
	ResultRateSlider_t127081558 * ___slider_3;
	// ResultRateParam ResultRateController::param
	ResultRateParam_t3783906052 * ___param_4;
	// System.String ResultRateController::nameLesson
	String_t* ___nameLesson_5;

public:
	inline static int32_t get_offset_of_view_2() { return static_cast<int32_t>(offsetof(ResultRateController_t2087696941, ___view_2)); }
	inline ResultRateView_t1401090222 * get_view_2() const { return ___view_2; }
	inline ResultRateView_t1401090222 ** get_address_of_view_2() { return &___view_2; }
	inline void set_view_2(ResultRateView_t1401090222 * value)
	{
		___view_2 = value;
		Il2CppCodeGenWriteBarrier(&___view_2, value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(ResultRateController_t2087696941, ___slider_3)); }
	inline ResultRateSlider_t127081558 * get_slider_3() const { return ___slider_3; }
	inline ResultRateSlider_t127081558 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(ResultRateSlider_t127081558 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier(&___slider_3, value);
	}

	inline static int32_t get_offset_of_param_4() { return static_cast<int32_t>(offsetof(ResultRateController_t2087696941, ___param_4)); }
	inline ResultRateParam_t3783906052 * get_param_4() const { return ___param_4; }
	inline ResultRateParam_t3783906052 ** get_address_of_param_4() { return &___param_4; }
	inline void set_param_4(ResultRateParam_t3783906052 * value)
	{
		___param_4 = value;
		Il2CppCodeGenWriteBarrier(&___param_4, value);
	}

	inline static int32_t get_offset_of_nameLesson_5() { return static_cast<int32_t>(offsetof(ResultRateController_t2087696941, ___nameLesson_5)); }
	inline String_t* get_nameLesson_5() const { return ___nameLesson_5; }
	inline String_t** get_address_of_nameLesson_5() { return &___nameLesson_5; }
	inline void set_nameLesson_5(String_t* value)
	{
		___nameLesson_5 = value;
		Il2CppCodeGenWriteBarrier(&___nameLesson_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
