﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSBoard
struct IOSBoard_t2012373677;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IOSBoard::.ctor()
extern "C"  void IOSBoard__ctor_m2544918228 (IOSBoard_t2012373677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSBoard::SetText_(System.String)
extern "C"  void IOSBoard_SetText__m3816762528 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSBoard::GetText_()
extern "C"  String_t* IOSBoard_GetText__m2143813767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSBoard::SetText(System.String)
extern "C"  void IOSBoard_SetText_m4028622803 (IOSBoard_t2012373677 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IOSBoard::GetText()
extern "C"  String_t* IOSBoard_GetText_m1072243192 (IOSBoard_t2012373677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
