﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2AnswerHelper
struct Listening2AnswerHelper_t4121473615;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening2AnswerHelper::.ctor()
extern "C"  void Listening2AnswerHelper__ctor_m3112231502 (Listening2AnswerHelper_t4121473615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2AnswerHelper::SetTrue()
extern "C"  void Listening2AnswerHelper_SetTrue_m458857102 (Listening2AnswerHelper_t4121473615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2AnswerHelper::SetFalse()
extern "C"  void Listening2AnswerHelper_SetFalse_m3056138049 (Listening2AnswerHelper_t4121473615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
