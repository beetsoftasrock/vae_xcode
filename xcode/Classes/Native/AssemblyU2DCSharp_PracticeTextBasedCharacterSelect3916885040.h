﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CharacterSelector
struct CharacterSelector_t2041732578;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PracticeTextBasedCharacterSelect
struct  PracticeTextBasedCharacterSelect_t3916885040  : public MonoBehaviour_t1158329972
{
public:
	// CharacterSelector PracticeTextBasedCharacterSelect::characterSelector
	CharacterSelector_t2041732578 * ___characterSelector_4;
	// UnityEngine.UI.Text PracticeTextBasedCharacterSelect::text
	Text_t356221433 * ___text_5;

public:
	inline static int32_t get_offset_of_characterSelector_4() { return static_cast<int32_t>(offsetof(PracticeTextBasedCharacterSelect_t3916885040, ___characterSelector_4)); }
	inline CharacterSelector_t2041732578 * get_characterSelector_4() const { return ___characterSelector_4; }
	inline CharacterSelector_t2041732578 ** get_address_of_characterSelector_4() { return &___characterSelector_4; }
	inline void set_characterSelector_4(CharacterSelector_t2041732578 * value)
	{
		___characterSelector_4 = value;
		Il2CppCodeGenWriteBarrier(&___characterSelector_4, value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(PracticeTextBasedCharacterSelect_t3916885040, ___text_5)); }
	inline Text_t356221433 * get_text_5() const { return ___text_5; }
	inline Text_t356221433 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t356221433 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier(&___text_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
