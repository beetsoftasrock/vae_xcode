﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Popup
struct Popup_t161311578;

#include "codegen/il2cpp-codegen.h"

// System.Void Popup::.ctor()
extern "C"  void Popup__ctor_m1949755225 (Popup_t161311578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::OnEnable()
extern "C"  void Popup_OnEnable_m846907105 (Popup_t161311578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::UpdateContent()
extern "C"  void Popup_UpdateContent_m3496645153 (Popup_t161311578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::PlayKaraoke()
extern "C"  void Popup_PlayKaraoke_m2303792709 (Popup_t161311578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
