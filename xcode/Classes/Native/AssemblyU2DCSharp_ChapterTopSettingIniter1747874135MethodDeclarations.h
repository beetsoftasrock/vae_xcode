﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopSettingIniter
struct ChapterTopSettingIniter_t1747874135;
// BaseSetting
struct BaseSetting_t2575616157;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterTopSettingIniter::.ctor()
extern "C"  void ChapterTopSettingIniter__ctor_m3961250812 (ChapterTopSettingIniter_t1747874135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseSetting ChapterTopSettingIniter::InitSetting()
extern "C"  BaseSetting_t2575616157 * ChapterTopSettingIniter_InitSetting_m2243384154 (ChapterTopSettingIniter_t1747874135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
