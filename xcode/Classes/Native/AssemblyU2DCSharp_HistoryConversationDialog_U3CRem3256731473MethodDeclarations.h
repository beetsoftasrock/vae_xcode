﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryConversationDialog/<RemoveHistoryItem>c__AnonStorey3
struct U3CRemoveHistoryItemU3Ec__AnonStorey3_t3256731473;
// ConversationHistoryItemView
struct ConversationHistoryItemView_t3846761105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationHistoryItemView3846761105.h"

// System.Void HistoryConversationDialog/<RemoveHistoryItem>c__AnonStorey3::.ctor()
extern "C"  void U3CRemoveHistoryItemU3Ec__AnonStorey3__ctor_m3107419460 (U3CRemoveHistoryItemU3Ec__AnonStorey3_t3256731473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HistoryConversationDialog/<RemoveHistoryItem>c__AnonStorey3::<>m__0(ConversationHistoryItemView)
extern "C"  bool U3CRemoveHistoryItemU3Ec__AnonStorey3_U3CU3Em__0_m3956251824 (U3CRemoveHistoryItemU3Ec__AnonStorey3_t3256731473 * __this, ConversationHistoryItemView_t3846761105 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
