﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultRateParam
struct  ResultRateParam_t3783906052  : public MonoBehaviour_t1158329972
{
public:
	// System.String ResultRateParam::nameResult
	String_t* ___nameResult_2;
	// System.String ResultRateParam::nameCharacter
	String_t* ___nameCharacter_3;
	// System.Int32 ResultRateParam::frequencyLearnToday
	int32_t ___frequencyLearnToday_4;
	// System.Int32 ResultRateParam::frequencyLearnTotal
	int32_t ___frequencyLearnTotal_5;
	// System.Single ResultRateParam::timeLearnToday
	float ___timeLearnToday_6;
	// System.Single ResultRateParam::timeLearnTotal
	float ___timeLearnTotal_7;
	// System.Single ResultRateParam::valueSlider
	float ___valueSlider_8;
	// System.Single ResultRateParam::totalTimeRecorded
	float ___totalTimeRecorded_9;
	// System.Single ResultRateParam::totalTimeSoundTemplate
	float ___totalTimeSoundTemplate_10;

public:
	inline static int32_t get_offset_of_nameResult_2() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___nameResult_2)); }
	inline String_t* get_nameResult_2() const { return ___nameResult_2; }
	inline String_t** get_address_of_nameResult_2() { return &___nameResult_2; }
	inline void set_nameResult_2(String_t* value)
	{
		___nameResult_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameResult_2, value);
	}

	inline static int32_t get_offset_of_nameCharacter_3() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___nameCharacter_3)); }
	inline String_t* get_nameCharacter_3() const { return ___nameCharacter_3; }
	inline String_t** get_address_of_nameCharacter_3() { return &___nameCharacter_3; }
	inline void set_nameCharacter_3(String_t* value)
	{
		___nameCharacter_3 = value;
		Il2CppCodeGenWriteBarrier(&___nameCharacter_3, value);
	}

	inline static int32_t get_offset_of_frequencyLearnToday_4() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___frequencyLearnToday_4)); }
	inline int32_t get_frequencyLearnToday_4() const { return ___frequencyLearnToday_4; }
	inline int32_t* get_address_of_frequencyLearnToday_4() { return &___frequencyLearnToday_4; }
	inline void set_frequencyLearnToday_4(int32_t value)
	{
		___frequencyLearnToday_4 = value;
	}

	inline static int32_t get_offset_of_frequencyLearnTotal_5() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___frequencyLearnTotal_5)); }
	inline int32_t get_frequencyLearnTotal_5() const { return ___frequencyLearnTotal_5; }
	inline int32_t* get_address_of_frequencyLearnTotal_5() { return &___frequencyLearnTotal_5; }
	inline void set_frequencyLearnTotal_5(int32_t value)
	{
		___frequencyLearnTotal_5 = value;
	}

	inline static int32_t get_offset_of_timeLearnToday_6() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___timeLearnToday_6)); }
	inline float get_timeLearnToday_6() const { return ___timeLearnToday_6; }
	inline float* get_address_of_timeLearnToday_6() { return &___timeLearnToday_6; }
	inline void set_timeLearnToday_6(float value)
	{
		___timeLearnToday_6 = value;
	}

	inline static int32_t get_offset_of_timeLearnTotal_7() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___timeLearnTotal_7)); }
	inline float get_timeLearnTotal_7() const { return ___timeLearnTotal_7; }
	inline float* get_address_of_timeLearnTotal_7() { return &___timeLearnTotal_7; }
	inline void set_timeLearnTotal_7(float value)
	{
		___timeLearnTotal_7 = value;
	}

	inline static int32_t get_offset_of_valueSlider_8() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___valueSlider_8)); }
	inline float get_valueSlider_8() const { return ___valueSlider_8; }
	inline float* get_address_of_valueSlider_8() { return &___valueSlider_8; }
	inline void set_valueSlider_8(float value)
	{
		___valueSlider_8 = value;
	}

	inline static int32_t get_offset_of_totalTimeRecorded_9() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___totalTimeRecorded_9)); }
	inline float get_totalTimeRecorded_9() const { return ___totalTimeRecorded_9; }
	inline float* get_address_of_totalTimeRecorded_9() { return &___totalTimeRecorded_9; }
	inline void set_totalTimeRecorded_9(float value)
	{
		___totalTimeRecorded_9 = value;
	}

	inline static int32_t get_offset_of_totalTimeSoundTemplate_10() { return static_cast<int32_t>(offsetof(ResultRateParam_t3783906052, ___totalTimeSoundTemplate_10)); }
	inline float get_totalTimeSoundTemplate_10() const { return ___totalTimeSoundTemplate_10; }
	inline float* get_address_of_totalTimeSoundTemplate_10() { return &___totalTimeSoundTemplate_10; }
	inline void set_totalTimeSoundTemplate_10(float value)
	{
		___totalTimeSoundTemplate_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
