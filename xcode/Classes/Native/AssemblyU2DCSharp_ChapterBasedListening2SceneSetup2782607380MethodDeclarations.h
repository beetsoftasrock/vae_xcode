﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedListening2SceneSetup
struct ChapterBasedListening2SceneSetup_t2782607380;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedListening2SceneSetup::.ctor()
extern "C"  void ChapterBasedListening2SceneSetup__ctor_m3957819565 (ChapterBasedListening2SceneSetup_t2782607380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedListening2SceneSetup::Awake()
extern "C"  void ChapterBasedListening2SceneSetup_Awake_m1551093104 (ChapterBasedListening2SceneSetup_t2782607380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
