﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.String
struct String_t;
// TextKaraoke
struct TextKaraoke_t2450194175;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextKaraoke/<AutoTyping>c__Iterator1
struct  U3CAutoTypingU3Ec__Iterator1_t2351930034  : public Il2CppObject
{
public:
	// UnityEngine.AudioSource TextKaraoke/<AutoTyping>c__Iterator1::audioSource
	AudioSource_t1135106623 * ___audioSource_0;
	// System.Single TextKaraoke/<AutoTyping>c__Iterator1::<timeSpace>__0
	float ___U3CtimeSpaceU3E__0_1;
	// System.String TextKaraoke/<AutoTyping>c__Iterator1::<temp>__1
	String_t* ___U3CtempU3E__1_2;
	// System.Int32 TextKaraoke/<AutoTyping>c__Iterator1::<i>__2
	int32_t ___U3CiU3E__2_3;
	// System.Int32 TextKaraoke/<AutoTyping>c__Iterator1::<j>__3
	int32_t ___U3CjU3E__3_4;
	// System.String TextKaraoke/<AutoTyping>c__Iterator1::<paintColor>__4
	String_t* ___U3CpaintColorU3E__4_5;
	// System.Single TextKaraoke/<AutoTyping>c__Iterator1::<passedTime>__5
	float ___U3CpassedTimeU3E__5_6;
	// TextKaraoke TextKaraoke/<AutoTyping>c__Iterator1::$this
	TextKaraoke_t2450194175 * ___U24this_7;
	// System.Object TextKaraoke/<AutoTyping>c__Iterator1::$current
	Il2CppObject * ___U24current_8;
	// System.Boolean TextKaraoke/<AutoTyping>c__Iterator1::$disposing
	bool ___U24disposing_9;
	// System.Int32 TextKaraoke/<AutoTyping>c__Iterator1::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_audioSource_0() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___audioSource_0)); }
	inline AudioSource_t1135106623 * get_audioSource_0() const { return ___audioSource_0; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_0() { return &___audioSource_0; }
	inline void set_audioSource_0(AudioSource_t1135106623 * value)
	{
		___audioSource_0 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_0, value);
	}

	inline static int32_t get_offset_of_U3CtimeSpaceU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U3CtimeSpaceU3E__0_1)); }
	inline float get_U3CtimeSpaceU3E__0_1() const { return ___U3CtimeSpaceU3E__0_1; }
	inline float* get_address_of_U3CtimeSpaceU3E__0_1() { return &___U3CtimeSpaceU3E__0_1; }
	inline void set_U3CtimeSpaceU3E__0_1(float value)
	{
		___U3CtimeSpaceU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U3CtempU3E__1_2)); }
	inline String_t* get_U3CtempU3E__1_2() const { return ___U3CtempU3E__1_2; }
	inline String_t** get_address_of_U3CtempU3E__1_2() { return &___U3CtempU3E__1_2; }
	inline void set_U3CtempU3E__1_2(String_t* value)
	{
		___U3CtempU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__2_3() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U3CiU3E__2_3)); }
	inline int32_t get_U3CiU3E__2_3() const { return ___U3CiU3E__2_3; }
	inline int32_t* get_address_of_U3CiU3E__2_3() { return &___U3CiU3E__2_3; }
	inline void set_U3CiU3E__2_3(int32_t value)
	{
		___U3CiU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E__3_4() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U3CjU3E__3_4)); }
	inline int32_t get_U3CjU3E__3_4() const { return ___U3CjU3E__3_4; }
	inline int32_t* get_address_of_U3CjU3E__3_4() { return &___U3CjU3E__3_4; }
	inline void set_U3CjU3E__3_4(int32_t value)
	{
		___U3CjU3E__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CpaintColorU3E__4_5() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U3CpaintColorU3E__4_5)); }
	inline String_t* get_U3CpaintColorU3E__4_5() const { return ___U3CpaintColorU3E__4_5; }
	inline String_t** get_address_of_U3CpaintColorU3E__4_5() { return &___U3CpaintColorU3E__4_5; }
	inline void set_U3CpaintColorU3E__4_5(String_t* value)
	{
		___U3CpaintColorU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpaintColorU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U3CpassedTimeU3E__5_6() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U3CpassedTimeU3E__5_6)); }
	inline float get_U3CpassedTimeU3E__5_6() const { return ___U3CpassedTimeU3E__5_6; }
	inline float* get_address_of_U3CpassedTimeU3E__5_6() { return &___U3CpassedTimeU3E__5_6; }
	inline void set_U3CpassedTimeU3E__5_6(float value)
	{
		___U3CpassedTimeU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U24this_7)); }
	inline TextKaraoke_t2450194175 * get_U24this_7() const { return ___U24this_7; }
	inline TextKaraoke_t2450194175 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(TextKaraoke_t2450194175 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CAutoTypingU3Ec__Iterator1_t2351930034, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
