﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3968042187MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioClip>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m917574772(__this, ___host0, method) ((  void (*) (Enumerator_t1238903360 *, Dictionary_2_t3847337892 *, const MethodInfo*))Enumerator__ctor_m3819430617_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioClip>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m559664345(__this, method) ((  Il2CppObject * (*) (Enumerator_t1238903360 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioClip>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3079124465(__this, method) ((  void (*) (Enumerator_t1238903360 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2482663638_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioClip>::Dispose()
#define Enumerator_Dispose_m2577530196(__this, method) ((  void (*) (Enumerator_t1238903360 *, const MethodInfo*))Enumerator_Dispose_m4238653081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioClip>::MoveNext()
#define Enumerator_MoveNext_m1424808732(__this, method) ((  bool (*) (Enumerator_t1238903360 *, const MethodInfo*))Enumerator_MoveNext_m335649778_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioClip>::get_Current()
#define Enumerator_get_Current_m3514057067(__this, method) ((  AudioClip_t1932558630 * (*) (Enumerator_t1238903360 *, const MethodInfo*))Enumerator_get_Current_m4025002300_gshared)(__this, method)
