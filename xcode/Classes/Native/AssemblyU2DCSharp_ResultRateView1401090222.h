﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultRateView
struct  ResultRateView_t1401090222  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ResultRateView::txtNameResult
	Text_t356221433 * ___txtNameResult_2;
	// UnityEngine.UI.Text ResultRateView::txtNameCharacter
	Text_t356221433 * ___txtNameCharacter_3;
	// UnityEngine.UI.Text ResultRateView::txtFrequencyLearnToday
	Text_t356221433 * ___txtFrequencyLearnToday_4;
	// UnityEngine.UI.Text ResultRateView::txtFrequencyLearnTotal
	Text_t356221433 * ___txtFrequencyLearnTotal_5;
	// UnityEngine.UI.Text ResultRateView::txtTimeLearnToday
	Text_t356221433 * ___txtTimeLearnToday_6;
	// UnityEngine.UI.Text ResultRateView::txtTimeLearnTotal
	Text_t356221433 * ___txtTimeLearnTotal_7;
	// UnityEngine.UI.Text ResultRateView::txtComment
	Text_t356221433 * ___txtComment_8;
	// UnityEngine.UI.Button ResultRateView::btnReload
	Button_t2872111280 * ___btnReload_9;
	// UnityEngine.UI.Button ResultRateView::btnAutoPlay
	Button_t2872111280 * ___btnAutoPlay_10;
	// UnityEngine.UI.Button ResultRateView::btnFinish
	Button_t2872111280 * ___btnFinish_11;
	// UnityEngine.UI.Button ResultRateView::btnHelp
	Button_t2872111280 * ___btnHelp_12;
	// UnityEngine.UI.Button ResultRateView::btnBack
	Button_t2872111280 * ___btnBack_13;

public:
	inline static int32_t get_offset_of_txtNameResult_2() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___txtNameResult_2)); }
	inline Text_t356221433 * get_txtNameResult_2() const { return ___txtNameResult_2; }
	inline Text_t356221433 ** get_address_of_txtNameResult_2() { return &___txtNameResult_2; }
	inline void set_txtNameResult_2(Text_t356221433 * value)
	{
		___txtNameResult_2 = value;
		Il2CppCodeGenWriteBarrier(&___txtNameResult_2, value);
	}

	inline static int32_t get_offset_of_txtNameCharacter_3() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___txtNameCharacter_3)); }
	inline Text_t356221433 * get_txtNameCharacter_3() const { return ___txtNameCharacter_3; }
	inline Text_t356221433 ** get_address_of_txtNameCharacter_3() { return &___txtNameCharacter_3; }
	inline void set_txtNameCharacter_3(Text_t356221433 * value)
	{
		___txtNameCharacter_3 = value;
		Il2CppCodeGenWriteBarrier(&___txtNameCharacter_3, value);
	}

	inline static int32_t get_offset_of_txtFrequencyLearnToday_4() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___txtFrequencyLearnToday_4)); }
	inline Text_t356221433 * get_txtFrequencyLearnToday_4() const { return ___txtFrequencyLearnToday_4; }
	inline Text_t356221433 ** get_address_of_txtFrequencyLearnToday_4() { return &___txtFrequencyLearnToday_4; }
	inline void set_txtFrequencyLearnToday_4(Text_t356221433 * value)
	{
		___txtFrequencyLearnToday_4 = value;
		Il2CppCodeGenWriteBarrier(&___txtFrequencyLearnToday_4, value);
	}

	inline static int32_t get_offset_of_txtFrequencyLearnTotal_5() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___txtFrequencyLearnTotal_5)); }
	inline Text_t356221433 * get_txtFrequencyLearnTotal_5() const { return ___txtFrequencyLearnTotal_5; }
	inline Text_t356221433 ** get_address_of_txtFrequencyLearnTotal_5() { return &___txtFrequencyLearnTotal_5; }
	inline void set_txtFrequencyLearnTotal_5(Text_t356221433 * value)
	{
		___txtFrequencyLearnTotal_5 = value;
		Il2CppCodeGenWriteBarrier(&___txtFrequencyLearnTotal_5, value);
	}

	inline static int32_t get_offset_of_txtTimeLearnToday_6() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___txtTimeLearnToday_6)); }
	inline Text_t356221433 * get_txtTimeLearnToday_6() const { return ___txtTimeLearnToday_6; }
	inline Text_t356221433 ** get_address_of_txtTimeLearnToday_6() { return &___txtTimeLearnToday_6; }
	inline void set_txtTimeLearnToday_6(Text_t356221433 * value)
	{
		___txtTimeLearnToday_6 = value;
		Il2CppCodeGenWriteBarrier(&___txtTimeLearnToday_6, value);
	}

	inline static int32_t get_offset_of_txtTimeLearnTotal_7() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___txtTimeLearnTotal_7)); }
	inline Text_t356221433 * get_txtTimeLearnTotal_7() const { return ___txtTimeLearnTotal_7; }
	inline Text_t356221433 ** get_address_of_txtTimeLearnTotal_7() { return &___txtTimeLearnTotal_7; }
	inline void set_txtTimeLearnTotal_7(Text_t356221433 * value)
	{
		___txtTimeLearnTotal_7 = value;
		Il2CppCodeGenWriteBarrier(&___txtTimeLearnTotal_7, value);
	}

	inline static int32_t get_offset_of_txtComment_8() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___txtComment_8)); }
	inline Text_t356221433 * get_txtComment_8() const { return ___txtComment_8; }
	inline Text_t356221433 ** get_address_of_txtComment_8() { return &___txtComment_8; }
	inline void set_txtComment_8(Text_t356221433 * value)
	{
		___txtComment_8 = value;
		Il2CppCodeGenWriteBarrier(&___txtComment_8, value);
	}

	inline static int32_t get_offset_of_btnReload_9() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___btnReload_9)); }
	inline Button_t2872111280 * get_btnReload_9() const { return ___btnReload_9; }
	inline Button_t2872111280 ** get_address_of_btnReload_9() { return &___btnReload_9; }
	inline void set_btnReload_9(Button_t2872111280 * value)
	{
		___btnReload_9 = value;
		Il2CppCodeGenWriteBarrier(&___btnReload_9, value);
	}

	inline static int32_t get_offset_of_btnAutoPlay_10() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___btnAutoPlay_10)); }
	inline Button_t2872111280 * get_btnAutoPlay_10() const { return ___btnAutoPlay_10; }
	inline Button_t2872111280 ** get_address_of_btnAutoPlay_10() { return &___btnAutoPlay_10; }
	inline void set_btnAutoPlay_10(Button_t2872111280 * value)
	{
		___btnAutoPlay_10 = value;
		Il2CppCodeGenWriteBarrier(&___btnAutoPlay_10, value);
	}

	inline static int32_t get_offset_of_btnFinish_11() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___btnFinish_11)); }
	inline Button_t2872111280 * get_btnFinish_11() const { return ___btnFinish_11; }
	inline Button_t2872111280 ** get_address_of_btnFinish_11() { return &___btnFinish_11; }
	inline void set_btnFinish_11(Button_t2872111280 * value)
	{
		___btnFinish_11 = value;
		Il2CppCodeGenWriteBarrier(&___btnFinish_11, value);
	}

	inline static int32_t get_offset_of_btnHelp_12() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___btnHelp_12)); }
	inline Button_t2872111280 * get_btnHelp_12() const { return ___btnHelp_12; }
	inline Button_t2872111280 ** get_address_of_btnHelp_12() { return &___btnHelp_12; }
	inline void set_btnHelp_12(Button_t2872111280 * value)
	{
		___btnHelp_12 = value;
		Il2CppCodeGenWriteBarrier(&___btnHelp_12, value);
	}

	inline static int32_t get_offset_of_btnBack_13() { return static_cast<int32_t>(offsetof(ResultRateView_t1401090222, ___btnBack_13)); }
	inline Button_t2872111280 * get_btnBack_13() const { return ___btnBack_13; }
	inline Button_t2872111280 ** get_address_of_btnBack_13() { return &___btnBack_13; }
	inline void set_btnBack_13(Button_t2872111280 * value)
	{
		___btnBack_13 = value;
		Il2CppCodeGenWriteBarrier(&___btnBack_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
