﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sentence1DatasWrapper
struct  Sentence1DatasWrapper_t174893388  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<CommandSheet/Param> Sentence1DatasWrapper::<wrappedDatas>k__BackingField
	List_1_t3492939606 * ___U3CwrappedDatasU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CwrappedDatasU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Sentence1DatasWrapper_t174893388, ___U3CwrappedDatasU3Ek__BackingField_0)); }
	inline List_1_t3492939606 * get_U3CwrappedDatasU3Ek__BackingField_0() const { return ___U3CwrappedDatasU3Ek__BackingField_0; }
	inline List_1_t3492939606 ** get_address_of_U3CwrappedDatasU3Ek__BackingField_0() { return &___U3CwrappedDatasU3Ek__BackingField_0; }
	inline void set_U3CwrappedDatasU3Ek__BackingField_0(List_1_t3492939606 * value)
	{
		___U3CwrappedDatasU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwrappedDatasU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
