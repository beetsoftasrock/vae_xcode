﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterSelector
struct CharacterSelector_t2041732578;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterSelector::.ctor()
extern "C"  void CharacterSelector__ctor_m3432728559 (CharacterSelector_t2041732578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CharacterSelector::get_selectedCharacterId()
extern "C"  int32_t CharacterSelector_get_selectedCharacterId_m296365265 (CharacterSelector_t2041732578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelector::set_selectedCharacterId(System.Int32)
extern "C"  void CharacterSelector_set_selectedCharacterId_m521716110 (CharacterSelector_t2041732578 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelector::UpdateView()
extern "C"  void CharacterSelector_UpdateView_m3394213603 (CharacterSelector_t2041732578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelector::OnCharacterSelectionViewClicked(System.Int32)
extern "C"  void CharacterSelector_OnCharacterSelectionViewClicked_m3689881544 (CharacterSelector_t2041732578 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterSelector::Awake()
extern "C"  void CharacterSelector_Awake_m2462203790 (CharacterSelector_t2041732578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
