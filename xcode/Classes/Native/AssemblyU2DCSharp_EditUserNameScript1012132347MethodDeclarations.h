﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditUserNameScript
struct EditUserNameScript_t1012132347;
// System.String
struct String_t;
// EditUsernameResponse
struct EditUsernameResponse_t1388559799;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_EditUsernameResponse1388559799.h"

// System.Void EditUserNameScript::.ctor()
extern "C"  void EditUserNameScript__ctor_m3997891362 (EditUserNameScript_t1012132347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUserNameScript::Start()
extern "C"  void EditUserNameScript_Start_m2135522342 (EditUserNameScript_t1012132347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUserNameScript::OnOffPopup()
extern "C"  void EditUserNameScript_OnOffPopup_m1423957616 (EditUserNameScript_t1012132347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUserNameScript::Edit()
extern "C"  void EditUserNameScript_Edit_m962374792 (EditUserNameScript_t1012132347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUserNameScript::PostEditURL()
extern "C"  void EditUserNameScript_PostEditURL_m891487513 (EditUserNameScript_t1012132347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EditUserNameScript::HasJapanese(System.String)
extern "C"  bool EditUserNameScript_HasJapanese_m2328610245 (EditUserNameScript_t1012132347 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUserNameScript::<PostEditURL>m__0(EditUsernameResponse)
extern "C"  void EditUserNameScript_U3CPostEditURLU3Em__0_m1386321875 (EditUserNameScript_t1012132347 * __this, EditUsernameResponse_t1388559799 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
