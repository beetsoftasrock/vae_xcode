﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2QuestionData
struct Listening2QuestionData_t2443975119;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Listening2QuestionData::.ctor()
extern "C"  void Listening2QuestionData__ctor_m189137424 (Listening2QuestionData_t2443975119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Listening2QuestionData::get_questionText()
extern "C"  String_t* Listening2QuestionData_get_questionText_m2717684525 (Listening2QuestionData_t2443975119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2QuestionData::set_questionText(System.String)
extern "C"  void Listening2QuestionData_set_questionText_m1453818038 (Listening2QuestionData_t2443975119 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Listening2QuestionData::get_questionSound()
extern "C"  String_t* Listening2QuestionData_get_questionSound_m2932726751 (Listening2QuestionData_t2443975119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2QuestionData::set_questionSound(System.String)
extern "C"  void Listening2QuestionData_set_questionSound_m2338981402 (Listening2QuestionData_t2443975119 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
