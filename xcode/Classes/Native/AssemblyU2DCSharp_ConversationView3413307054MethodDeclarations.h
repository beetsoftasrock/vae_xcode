﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationView
struct ConversationView_t3413307054;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseConversationController_Conve1349939935.h"

// System.Void ConversationView::.ctor()
extern "C"  void ConversationView__ctor_m4186478031 (ConversationView_t3413307054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationView::SetState(BaseConversationController/ConversationState)
extern "C"  void ConversationView_SetState_m4173951031 (ConversationView_t3413307054 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
