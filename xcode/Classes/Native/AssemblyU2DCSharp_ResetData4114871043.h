﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResetData
struct  ResetData_t4114871043  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform ResetData::popupResetData
	Transform_t3275118058 * ___popupResetData_2;
	// UnityEngine.Transform ResetData::popupYesNo
	Transform_t3275118058 * ___popupYesNo_3;
	// UnityEngine.Transform ResetData::popupConfirm
	Transform_t3275118058 * ___popupConfirm_4;
	// UnityEngine.Transform ResetData::popupResetDone
	Transform_t3275118058 * ___popupResetDone_5;
	// System.Boolean ResetData::<isResetDone>k__BackingField
	bool ___U3CisResetDoneU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_popupResetData_2() { return static_cast<int32_t>(offsetof(ResetData_t4114871043, ___popupResetData_2)); }
	inline Transform_t3275118058 * get_popupResetData_2() const { return ___popupResetData_2; }
	inline Transform_t3275118058 ** get_address_of_popupResetData_2() { return &___popupResetData_2; }
	inline void set_popupResetData_2(Transform_t3275118058 * value)
	{
		___popupResetData_2 = value;
		Il2CppCodeGenWriteBarrier(&___popupResetData_2, value);
	}

	inline static int32_t get_offset_of_popupYesNo_3() { return static_cast<int32_t>(offsetof(ResetData_t4114871043, ___popupYesNo_3)); }
	inline Transform_t3275118058 * get_popupYesNo_3() const { return ___popupYesNo_3; }
	inline Transform_t3275118058 ** get_address_of_popupYesNo_3() { return &___popupYesNo_3; }
	inline void set_popupYesNo_3(Transform_t3275118058 * value)
	{
		___popupYesNo_3 = value;
		Il2CppCodeGenWriteBarrier(&___popupYesNo_3, value);
	}

	inline static int32_t get_offset_of_popupConfirm_4() { return static_cast<int32_t>(offsetof(ResetData_t4114871043, ___popupConfirm_4)); }
	inline Transform_t3275118058 * get_popupConfirm_4() const { return ___popupConfirm_4; }
	inline Transform_t3275118058 ** get_address_of_popupConfirm_4() { return &___popupConfirm_4; }
	inline void set_popupConfirm_4(Transform_t3275118058 * value)
	{
		___popupConfirm_4 = value;
		Il2CppCodeGenWriteBarrier(&___popupConfirm_4, value);
	}

	inline static int32_t get_offset_of_popupResetDone_5() { return static_cast<int32_t>(offsetof(ResetData_t4114871043, ___popupResetDone_5)); }
	inline Transform_t3275118058 * get_popupResetDone_5() const { return ___popupResetDone_5; }
	inline Transform_t3275118058 ** get_address_of_popupResetDone_5() { return &___popupResetDone_5; }
	inline void set_popupResetDone_5(Transform_t3275118058 * value)
	{
		___popupResetDone_5 = value;
		Il2CppCodeGenWriteBarrier(&___popupResetDone_5, value);
	}

	inline static int32_t get_offset_of_U3CisResetDoneU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ResetData_t4114871043, ___U3CisResetDoneU3Ek__BackingField_6)); }
	inline bool get_U3CisResetDoneU3Ek__BackingField_6() const { return ___U3CisResetDoneU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CisResetDoneU3Ek__BackingField_6() { return &___U3CisResetDoneU3Ek__BackingField_6; }
	inline void set_U3CisResetDoneU3Ek__BackingField_6(bool value)
	{
		___U3CisResetDoneU3Ek__BackingField_6 = value;
	}
};

struct ResetData_t4114871043_StaticFields
{
public:
	// System.Action ResetData::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(ResetData_t4114871043_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
