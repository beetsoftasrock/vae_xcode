﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeAppleExtensions
struct  FakeAppleExtensions_t4039399289  : public Il2CppObject
{
public:
	// System.Boolean UnityEngine.Purchasing.FakeAppleExtensions::m_FailRefresh
	bool ___m_FailRefresh_0;

public:
	inline static int32_t get_offset_of_m_FailRefresh_0() { return static_cast<int32_t>(offsetof(FakeAppleExtensions_t4039399289, ___m_FailRefresh_0)); }
	inline bool get_m_FailRefresh_0() const { return ___m_FailRefresh_0; }
	inline bool* get_address_of_m_FailRefresh_0() { return &___m_FailRefresh_0; }
	inline void set_m_FailRefresh_0(bool value)
	{
		___m_FailRefresh_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
