﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundleManifest
struct AssetBundleManifest_t1328741589;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Hash1282836532937.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AssetBundleManifest1328741589.h"

// System.String[] UnityEngine.AssetBundleManifest::GetAllAssetBundles()
extern "C"  StringU5BU5D_t1642385972* AssetBundleManifest_GetAllAssetBundles_m4091763365 (AssetBundleManifest_t1328741589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Hash128 UnityEngine.AssetBundleManifest::GetAssetBundleHash(System.String)
extern "C"  Hash128_t2836532937  AssetBundleManifest_GetAssetBundleHash_m2978033537 (AssetBundleManifest_t1328741589 * __this, String_t* ___assetBundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleManifest::INTERNAL_CALL_GetAssetBundleHash(UnityEngine.AssetBundleManifest,System.String,UnityEngine.Hash128&)
extern "C"  void AssetBundleManifest_INTERNAL_CALL_GetAssetBundleHash_m4281547985 (Il2CppObject * __this /* static, unused */, AssetBundleManifest_t1328741589 * ___self0, String_t* ___assetBundleName1, Hash128_t2836532937 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
