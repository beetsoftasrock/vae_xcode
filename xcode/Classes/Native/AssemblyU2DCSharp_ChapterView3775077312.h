﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ChapterView_Part3646775580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterView
struct  ChapterView_t3775077312  : public MonoBehaviour_t1158329972
{
public:
	// ChapterView/Part ChapterView::partVocab
	Part_t3646775580  ___partVocab_2;
	// ChapterView/Part ChapterView::partSentence
	Part_t3646775580  ___partSentence_3;
	// ChapterView/Part ChapterView::partListening
	Part_t3646775580  ___partListening_4;
	// ChapterView/Part ChapterView::partConversation
	Part_t3646775580  ___partConversation_5;
	// ChapterView/Part ChapterView::partExercise
	Part_t3646775580  ___partExercise_6;
	// ChapterView/Part ChapterView::partPractice
	Part_t3646775580  ___partPractice_7;

public:
	inline static int32_t get_offset_of_partVocab_2() { return static_cast<int32_t>(offsetof(ChapterView_t3775077312, ___partVocab_2)); }
	inline Part_t3646775580  get_partVocab_2() const { return ___partVocab_2; }
	inline Part_t3646775580 * get_address_of_partVocab_2() { return &___partVocab_2; }
	inline void set_partVocab_2(Part_t3646775580  value)
	{
		___partVocab_2 = value;
	}

	inline static int32_t get_offset_of_partSentence_3() { return static_cast<int32_t>(offsetof(ChapterView_t3775077312, ___partSentence_3)); }
	inline Part_t3646775580  get_partSentence_3() const { return ___partSentence_3; }
	inline Part_t3646775580 * get_address_of_partSentence_3() { return &___partSentence_3; }
	inline void set_partSentence_3(Part_t3646775580  value)
	{
		___partSentence_3 = value;
	}

	inline static int32_t get_offset_of_partListening_4() { return static_cast<int32_t>(offsetof(ChapterView_t3775077312, ___partListening_4)); }
	inline Part_t3646775580  get_partListening_4() const { return ___partListening_4; }
	inline Part_t3646775580 * get_address_of_partListening_4() { return &___partListening_4; }
	inline void set_partListening_4(Part_t3646775580  value)
	{
		___partListening_4 = value;
	}

	inline static int32_t get_offset_of_partConversation_5() { return static_cast<int32_t>(offsetof(ChapterView_t3775077312, ___partConversation_5)); }
	inline Part_t3646775580  get_partConversation_5() const { return ___partConversation_5; }
	inline Part_t3646775580 * get_address_of_partConversation_5() { return &___partConversation_5; }
	inline void set_partConversation_5(Part_t3646775580  value)
	{
		___partConversation_5 = value;
	}

	inline static int32_t get_offset_of_partExercise_6() { return static_cast<int32_t>(offsetof(ChapterView_t3775077312, ___partExercise_6)); }
	inline Part_t3646775580  get_partExercise_6() const { return ___partExercise_6; }
	inline Part_t3646775580 * get_address_of_partExercise_6() { return &___partExercise_6; }
	inline void set_partExercise_6(Part_t3646775580  value)
	{
		___partExercise_6 = value;
	}

	inline static int32_t get_offset_of_partPractice_7() { return static_cast<int32_t>(offsetof(ChapterView_t3775077312, ___partPractice_7)); }
	inline Part_t3646775580  get_partPractice_7() const { return ___partPractice_7; }
	inline Part_t3646775580 * get_address_of_partPractice_7() { return &___partPractice_7; }
	inline void set_partPractice_7(Part_t3646775580  value)
	{
		___partPractice_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
