﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundData
struct SoundData_t3344892245;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SoundData::.ctor()
extern "C"  void SoundData__ctor_m3167327114 (SoundData_t3344892245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SoundData::get_Volume()
extern "C"  float SoundData_get_Volume_m4018293159 (SoundData_t3344892245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundData::get_IsLoop()
extern "C"  bool SoundData_get_IsLoop_m3687125891 (SoundData_t3344892245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SoundData::get_Position()
extern "C"  Vector3_t2243707580  SoundData_get_Position_m2381206992 (SoundData_t3344892245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
