﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8
struct U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063;

#include "codegen/il2cpp-codegen.h"

// System.Void AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey8::.ctor()
extern "C"  void U3CloadAssetBundlesU3Ec__AnonStorey8__ctor_m508257296 (U3CloadAssetBundlesU3Ec__AnonStorey8_t1991153063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
