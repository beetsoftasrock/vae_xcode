﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tab
struct Tab_t4262919697;

#include "codegen/il2cpp-codegen.h"

// System.Void Tab::.ctor()
extern "C"  void Tab__ctor_m1732295942 (Tab_t4262919697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
