﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoiceRecordModule
struct VoiceRecordModule_t679113955;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VoiceRecordModule::.ctor()
extern "C"  void VoiceRecordModule__ctor_m3990831374 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::OnConversationReset()
extern "C"  void VoiceRecordModule_OnConversationReset_m1663667003 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::OnConversationReplay()
extern "C"  void VoiceRecordModule_OnConversationReplay_m3166785341 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::Start()
extern "C"  void VoiceRecordModule_Start_m2673043114 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::StartVoiceRecording()
extern "C"  void VoiceRecordModule_StartVoiceRecording_m2803999413 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::UpdatePlayerTalkDialog(System.String,System.String)
extern "C"  void VoiceRecordModule_UpdatePlayerTalkDialog_m3123222268 (VoiceRecordModule_t679113955 * __this, String_t* ___strEn0, String_t* ___strJp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::EndVoiceRecording()
extern "C"  void VoiceRecordModule_EndVoiceRecording_m4065128046 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::ReplayRecordedVoice()
extern "C"  void VoiceRecordModule_ReplayRecordedVoice_m1383055911 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::EndReplayRecordedVoice()
extern "C"  void VoiceRecordModule_EndReplayRecordedVoice_m54784228 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoiceRecordModule::DoReplayRecorded()
extern "C"  Il2CppObject * VoiceRecordModule_DoReplayRecorded_m777532738 (VoiceRecordModule_t679113955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule::.cctor()
extern "C"  void VoiceRecordModule__cctor_m1586328217 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
