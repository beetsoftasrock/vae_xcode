﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MyPageChapterView
struct MyPageChapterView_t1534782717;
// Graph
struct Graph_t2735713688;
// LessonValue[]
struct LessonValueU5BU5D_t2514548700;
// GraphValue[]
struct GraphValueU5BU5D_t3529246652;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageChapterController
struct  MyPageChapterController_t2166380602  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 MyPageChapterController::ChapterId
	int32_t ___ChapterId_2;
	// System.String MyPageChapterController::CharacterLoadDataName
	String_t* ___CharacterLoadDataName_3;
	// MyPageChapterView MyPageChapterController::chapterView
	MyPageChapterView_t1534782717 * ___chapterView_4;
	// Graph MyPageChapterController::currentGraph
	Graph_t2735713688 * ___currentGraph_5;
	// Graph MyPageChapterController::prevGraph
	Graph_t2735713688 * ___prevGraph_6;
	// LessonValue[] MyPageChapterController::lessonValue
	LessonValueU5BU5D_t2514548700* ___lessonValue_7;
	// GraphValue[] MyPageChapterController::graphValue
	GraphValueU5BU5D_t3529246652* ___graphValue_8;
	// GraphValue[] MyPageChapterController::graphValuePrev
	GraphValueU5BU5D_t3529246652* ___graphValuePrev_9;
	// System.Single MyPageChapterController::timeLearnTotal
	float ___timeLearnTotal_10;

public:
	inline static int32_t get_offset_of_ChapterId_2() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___ChapterId_2)); }
	inline int32_t get_ChapterId_2() const { return ___ChapterId_2; }
	inline int32_t* get_address_of_ChapterId_2() { return &___ChapterId_2; }
	inline void set_ChapterId_2(int32_t value)
	{
		___ChapterId_2 = value;
	}

	inline static int32_t get_offset_of_CharacterLoadDataName_3() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___CharacterLoadDataName_3)); }
	inline String_t* get_CharacterLoadDataName_3() const { return ___CharacterLoadDataName_3; }
	inline String_t** get_address_of_CharacterLoadDataName_3() { return &___CharacterLoadDataName_3; }
	inline void set_CharacterLoadDataName_3(String_t* value)
	{
		___CharacterLoadDataName_3 = value;
		Il2CppCodeGenWriteBarrier(&___CharacterLoadDataName_3, value);
	}

	inline static int32_t get_offset_of_chapterView_4() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___chapterView_4)); }
	inline MyPageChapterView_t1534782717 * get_chapterView_4() const { return ___chapterView_4; }
	inline MyPageChapterView_t1534782717 ** get_address_of_chapterView_4() { return &___chapterView_4; }
	inline void set_chapterView_4(MyPageChapterView_t1534782717 * value)
	{
		___chapterView_4 = value;
		Il2CppCodeGenWriteBarrier(&___chapterView_4, value);
	}

	inline static int32_t get_offset_of_currentGraph_5() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___currentGraph_5)); }
	inline Graph_t2735713688 * get_currentGraph_5() const { return ___currentGraph_5; }
	inline Graph_t2735713688 ** get_address_of_currentGraph_5() { return &___currentGraph_5; }
	inline void set_currentGraph_5(Graph_t2735713688 * value)
	{
		___currentGraph_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentGraph_5, value);
	}

	inline static int32_t get_offset_of_prevGraph_6() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___prevGraph_6)); }
	inline Graph_t2735713688 * get_prevGraph_6() const { return ___prevGraph_6; }
	inline Graph_t2735713688 ** get_address_of_prevGraph_6() { return &___prevGraph_6; }
	inline void set_prevGraph_6(Graph_t2735713688 * value)
	{
		___prevGraph_6 = value;
		Il2CppCodeGenWriteBarrier(&___prevGraph_6, value);
	}

	inline static int32_t get_offset_of_lessonValue_7() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___lessonValue_7)); }
	inline LessonValueU5BU5D_t2514548700* get_lessonValue_7() const { return ___lessonValue_7; }
	inline LessonValueU5BU5D_t2514548700** get_address_of_lessonValue_7() { return &___lessonValue_7; }
	inline void set_lessonValue_7(LessonValueU5BU5D_t2514548700* value)
	{
		___lessonValue_7 = value;
		Il2CppCodeGenWriteBarrier(&___lessonValue_7, value);
	}

	inline static int32_t get_offset_of_graphValue_8() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___graphValue_8)); }
	inline GraphValueU5BU5D_t3529246652* get_graphValue_8() const { return ___graphValue_8; }
	inline GraphValueU5BU5D_t3529246652** get_address_of_graphValue_8() { return &___graphValue_8; }
	inline void set_graphValue_8(GraphValueU5BU5D_t3529246652* value)
	{
		___graphValue_8 = value;
		Il2CppCodeGenWriteBarrier(&___graphValue_8, value);
	}

	inline static int32_t get_offset_of_graphValuePrev_9() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___graphValuePrev_9)); }
	inline GraphValueU5BU5D_t3529246652* get_graphValuePrev_9() const { return ___graphValuePrev_9; }
	inline GraphValueU5BU5D_t3529246652** get_address_of_graphValuePrev_9() { return &___graphValuePrev_9; }
	inline void set_graphValuePrev_9(GraphValueU5BU5D_t3529246652* value)
	{
		___graphValuePrev_9 = value;
		Il2CppCodeGenWriteBarrier(&___graphValuePrev_9, value);
	}

	inline static int32_t get_offset_of_timeLearnTotal_10() { return static_cast<int32_t>(offsetof(MyPageChapterController_t2166380602, ___timeLearnTotal_10)); }
	inline float get_timeLearnTotal_10() const { return ___timeLearnTotal_10; }
	inline float* get_address_of_timeLearnTotal_10() { return &___timeLearnTotal_10; }
	inline void set_timeLearnTotal_10(float value)
	{
		___timeLearnTotal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
