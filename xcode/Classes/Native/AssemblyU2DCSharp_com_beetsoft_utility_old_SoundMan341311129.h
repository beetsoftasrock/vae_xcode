﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// com.beetsoft.utility_old.SoundManager
struct SoundManager_t695211022;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0
struct  U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129  : public Il2CppObject
{
public:
	// System.String com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::namePath
	String_t* ___namePath_0;
	// System.String com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::<path>__0
	String_t* ___U3CpathU3E__0_1;
	// UnityEngine.WWW com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::<www>__1
	WWW_t2919945039 * ___U3CwwwU3E__1_2;
	// UnityEngine.AudioClip com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::<clip>__2
	AudioClip_t1932558630 * ___U3CclipU3E__2_3;
	// com.beetsoft.utility_old.SoundManager com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::$this
	SoundManager_t695211022 * ___U24this_4;
	// System.Object com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_namePath_0() { return static_cast<int32_t>(offsetof(U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129, ___namePath_0)); }
	inline String_t* get_namePath_0() const { return ___namePath_0; }
	inline String_t** get_address_of_namePath_0() { return &___namePath_0; }
	inline void set_namePath_0(String_t* value)
	{
		___namePath_0 = value;
		Il2CppCodeGenWriteBarrier(&___namePath_0, value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129, ___U3CpathU3E__0_1)); }
	inline String_t* get_U3CpathU3E__0_1() const { return ___U3CpathU3E__0_1; }
	inline String_t** get_address_of_U3CpathU3E__0_1() { return &___U3CpathU3E__0_1; }
	inline void set_U3CpathU3E__0_1(String_t* value)
	{
		___U3CpathU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129, ___U3CwwwU3E__1_2)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__1_2() const { return ___U3CwwwU3E__1_2; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__1_2() { return &___U3CwwwU3E__1_2; }
	inline void set_U3CwwwU3E__1_2(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CclipU3E__2_3() { return static_cast<int32_t>(offsetof(U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129, ___U3CclipU3E__2_3)); }
	inline AudioClip_t1932558630 * get_U3CclipU3E__2_3() const { return ___U3CclipU3E__2_3; }
	inline AudioClip_t1932558630 ** get_address_of_U3CclipU3E__2_3() { return &___U3CclipU3E__2_3; }
	inline void set_U3CclipU3E__2_3(AudioClip_t1932558630 * value)
	{
		___U3CclipU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CclipU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129, ___U24this_4)); }
	inline SoundManager_t695211022 * get_U24this_4() const { return ___U24this_4; }
	inline SoundManager_t695211022 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SoundManager_t695211022 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
