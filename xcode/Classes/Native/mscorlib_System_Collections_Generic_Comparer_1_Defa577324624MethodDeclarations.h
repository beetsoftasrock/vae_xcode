﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Beetsoft.VAE.Purchaser/PurchaseSession>
struct DefaultComparer_t577324624;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor()
extern "C"  void DefaultComparer__ctor_m2579762341_gshared (DefaultComparer_t577324624 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2579762341(__this, method) ((  void (*) (DefaultComparer_t577324624 *, const MethodInfo*))DefaultComparer__ctor_m2579762341_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Beetsoft.VAE.Purchaser/PurchaseSession>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2638464888_gshared (DefaultComparer_t577324624 * __this, PurchaseSession_t2088054391  ___x0, PurchaseSession_t2088054391  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2638464888(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t577324624 *, PurchaseSession_t2088054391 , PurchaseSession_t2088054391 , const MethodInfo*))DefaultComparer_Compare_m2638464888_gshared)(__this, ___x0, ___y1, method)
