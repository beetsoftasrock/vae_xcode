﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2DataLoader
struct Listening2DataLoader_t3991471186;
// Listening2DatasWrapper
struct Listening2DatasWrapper_t1178512309;
// IDatasWrapper`1<Listening2QuestionData>
struct IDatasWrapper_1_t291767404;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Listening2DatasWrapper1178512309.h"

// System.Void Listening2DataLoader::.ctor()
extern "C"  void Listening2DataLoader__ctor_m3382918093 (Listening2DataLoader_t3991471186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Listening2DatasWrapper Listening2DataLoader::get_datasWrapper()
extern "C"  Listening2DatasWrapper_t1178512309 * Listening2DataLoader_get_datasWrapper_m1013050752 (Listening2DataLoader_t3991471186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2DataLoader::set_datasWrapper(Listening2DatasWrapper)
extern "C"  void Listening2DataLoader_set_datasWrapper_m1100099359 (Listening2DataLoader_t3991471186 * __this, Listening2DatasWrapper_t1178512309 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IDatasWrapper`1<Listening2QuestionData> Listening2DataLoader::GetDatasWrapper()
extern "C"  Il2CppObject* Listening2DataLoader_GetDatasWrapper_m2220447019 (Listening2DataLoader_t3991471186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
