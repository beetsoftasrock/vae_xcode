﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAP_Popup
struct IAP_Popup_t2375878299;

#include "codegen/il2cpp-codegen.h"

// System.Void IAP_Popup::.ctor()
extern "C"  void IAP_Popup__ctor_m3046015170 (IAP_Popup_t2375878299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IAP_Popup::get_DisplayPurchased()
extern "C"  bool IAP_Popup_get_DisplayPurchased_m1368869176 (IAP_Popup_t2375878299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_Popup::set_DisplayPurchased(System.Boolean)
extern "C"  void IAP_Popup_set_DisplayPurchased_m195277281 (IAP_Popup_t2375878299 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IAP_Popup::get_ButtonsInteracable()
extern "C"  bool IAP_Popup_get_ButtonsInteracable_m1019493110 (IAP_Popup_t2375878299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_Popup::set_ButtonsInteracable(System.Boolean)
extern "C"  void IAP_Popup_set_ButtonsInteracable_m4113669033 (IAP_Popup_t2375878299 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP_Popup::OnEnable()
extern "C"  void IAP_Popup_OnEnable_m222830710 (IAP_Popup_t2375878299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
