﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PracticeVRSwitcher/<GoToVRMode>c__AnonStorey1
struct U3CGoToVRModeU3Ec__AnonStorey1_t1203783803;

#include "codegen/il2cpp-codegen.h"

// System.Void PracticeVRSwitcher/<GoToVRMode>c__AnonStorey1::.ctor()
extern "C"  void U3CGoToVRModeU3Ec__AnonStorey1__ctor_m3813949924 (U3CGoToVRModeU3Ec__AnonStorey1_t1203783803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher/<GoToVRMode>c__AnonStorey1::<>m__0()
extern "C"  void U3CGoToVRModeU3Ec__AnonStorey1_U3CU3Em__0_m2847448305 (U3CGoToVRModeU3Ec__AnonStorey1_t1203783803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
