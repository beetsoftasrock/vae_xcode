﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LearningData
struct  LearningData_t1664811342  : public Il2CppObject
{
public:
	// System.Int32 LearningData::chapter
	int32_t ___chapter_0;
	// System.Int32 LearningData::category
	int32_t ___category_1;
	// System.Int32 LearningData::part
	int32_t ___part_2;
	// System.Int32 LearningData::time
	int32_t ___time_3;
	// System.Int32 LearningData::achievement
	int32_t ___achievement_4;
	// System.Int32 LearningData::positive
	int32_t ___positive_5;
	// System.Int32 LearningData::mistake
	int32_t ___mistake_6;
	// System.Int32 LearningData::fit
	int32_t ___fit_7;
	// System.Int32 LearningData::count
	int32_t ___count_8;

public:
	inline static int32_t get_offset_of_chapter_0() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___chapter_0)); }
	inline int32_t get_chapter_0() const { return ___chapter_0; }
	inline int32_t* get_address_of_chapter_0() { return &___chapter_0; }
	inline void set_chapter_0(int32_t value)
	{
		___chapter_0 = value;
	}

	inline static int32_t get_offset_of_category_1() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___category_1)); }
	inline int32_t get_category_1() const { return ___category_1; }
	inline int32_t* get_address_of_category_1() { return &___category_1; }
	inline void set_category_1(int32_t value)
	{
		___category_1 = value;
	}

	inline static int32_t get_offset_of_part_2() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___part_2)); }
	inline int32_t get_part_2() const { return ___part_2; }
	inline int32_t* get_address_of_part_2() { return &___part_2; }
	inline void set_part_2(int32_t value)
	{
		___part_2 = value;
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___time_3)); }
	inline int32_t get_time_3() const { return ___time_3; }
	inline int32_t* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(int32_t value)
	{
		___time_3 = value;
	}

	inline static int32_t get_offset_of_achievement_4() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___achievement_4)); }
	inline int32_t get_achievement_4() const { return ___achievement_4; }
	inline int32_t* get_address_of_achievement_4() { return &___achievement_4; }
	inline void set_achievement_4(int32_t value)
	{
		___achievement_4 = value;
	}

	inline static int32_t get_offset_of_positive_5() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___positive_5)); }
	inline int32_t get_positive_5() const { return ___positive_5; }
	inline int32_t* get_address_of_positive_5() { return &___positive_5; }
	inline void set_positive_5(int32_t value)
	{
		___positive_5 = value;
	}

	inline static int32_t get_offset_of_mistake_6() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___mistake_6)); }
	inline int32_t get_mistake_6() const { return ___mistake_6; }
	inline int32_t* get_address_of_mistake_6() { return &___mistake_6; }
	inline void set_mistake_6(int32_t value)
	{
		___mistake_6 = value;
	}

	inline static int32_t get_offset_of_fit_7() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___fit_7)); }
	inline int32_t get_fit_7() const { return ___fit_7; }
	inline int32_t* get_address_of_fit_7() { return &___fit_7; }
	inline void set_fit_7(int32_t value)
	{
		___fit_7 = value;
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(LearningData_t1664811342, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
