﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterScript/<DelaySomething>c__Iterator2
struct U3CDelaySomethingU3Ec__Iterator2_t4205573215;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterScript/<DelaySomething>c__Iterator2::.ctor()
extern "C"  void U3CDelaySomethingU3Ec__Iterator2__ctor_m1959408096 (U3CDelaySomethingU3Ec__Iterator2_t4205573215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterScript/<DelaySomething>c__Iterator2::MoveNext()
extern "C"  bool U3CDelaySomethingU3Ec__Iterator2_MoveNext_m2961322212 (U3CDelaySomethingU3Ec__Iterator2_t4205573215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterScript/<DelaySomething>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelaySomethingU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3343701380 (U3CDelaySomethingU3Ec__Iterator2_t4205573215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterScript/<DelaySomething>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelaySomethingU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3715423100 (U3CDelaySomethingU3Ec__Iterator2_t4205573215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript/<DelaySomething>c__Iterator2::Dispose()
extern "C"  void U3CDelaySomethingU3Ec__Iterator2_Dispose_m935713277 (U3CDelaySomethingU3Ec__Iterator2_t4205573215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript/<DelaySomething>c__Iterator2::Reset()
extern "C"  void U3CDelaySomethingU3Ec__Iterator2_Reset_m1124434743 (U3CDelaySomethingU3Ec__Iterator2_t4205573215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
