﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PrelearningSetting
struct PrelearningSetting_t219472927;

#include "codegen/il2cpp-codegen.h"

// System.Void PrelearningSetting::.ctor()
extern "C"  void PrelearningSetting__ctor_m3230314752 (PrelearningSetting_t219472927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
