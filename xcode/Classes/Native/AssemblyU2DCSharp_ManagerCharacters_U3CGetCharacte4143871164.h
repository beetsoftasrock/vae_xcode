﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManagerCharacters/<GetCharacter>c__AnonStorey0
struct  U3CGetCharacterU3Ec__AnonStorey0_t4143871164  : public Il2CppObject
{
public:
	// System.String ManagerCharacters/<GetCharacter>c__AnonStorey0::characterName
	String_t* ___characterName_0;

public:
	inline static int32_t get_offset_of_characterName_0() { return static_cast<int32_t>(offsetof(U3CGetCharacterU3Ec__AnonStorey0_t4143871164, ___characterName_0)); }
	inline String_t* get_characterName_0() const { return ___characterName_0; }
	inline String_t** get_address_of_characterName_0() { return &___characterName_0; }
	inline void set_characterName_0(String_t* value)
	{
		___characterName_0 = value;
		Il2CppCodeGenWriteBarrier(&___characterName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
