﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>
struct KeyCollection_t3021804267;
// System.Collections.Generic.Dictionary`2<System.Int32,proto.PhoneEvent/Types/Type>
struct Dictionary_2_t538306496;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3227809934.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4116650832_gshared (KeyCollection_t3021804267 * __this, Dictionary_2_t538306496 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4116650832(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3021804267 *, Dictionary_2_t538306496 *, const MethodInfo*))KeyCollection__ctor_m4116650832_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1013000846_gshared (KeyCollection_t3021804267 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1013000846(__this, ___item0, method) ((  void (*) (KeyCollection_t3021804267 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1013000846_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2372973929_gshared (KeyCollection_t3021804267 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2372973929(__this, method) ((  void (*) (KeyCollection_t3021804267 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2372973929_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1845238742_gshared (KeyCollection_t3021804267 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1845238742(__this, ___item0, method) ((  bool (*) (KeyCollection_t3021804267 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1845238742_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m14415821_gshared (KeyCollection_t3021804267 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m14415821(__this, ___item0, method) ((  bool (*) (KeyCollection_t3021804267 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m14415821_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1752379659_gshared (KeyCollection_t3021804267 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1752379659(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3021804267 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1752379659_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1843297171_gshared (KeyCollection_t3021804267 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1843297171(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3021804267 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1843297171_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2372580314_gshared (KeyCollection_t3021804267 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2372580314(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3021804267 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2372580314_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3413963129_gshared (KeyCollection_t3021804267 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3413963129(__this, method) ((  bool (*) (KeyCollection_t3021804267 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3413963129_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1511280355_gshared (KeyCollection_t3021804267 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1511280355(__this, method) ((  bool (*) (KeyCollection_t3021804267 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1511280355_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2476618487_gshared (KeyCollection_t3021804267 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2476618487(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3021804267 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2476618487_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2529261493_gshared (KeyCollection_t3021804267 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2529261493(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3021804267 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2529261493_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::GetEnumerator()
extern "C"  Enumerator_t3227809934  KeyCollection_GetEnumerator_m1641025898_gshared (KeyCollection_t3021804267 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1641025898(__this, method) ((  Enumerator_t3227809934  (*) (KeyCollection_t3021804267 *, const MethodInfo*))KeyCollection_GetEnumerator_m1641025898_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,proto.PhoneEvent/Types/Type>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3762052811_gshared (KeyCollection_t3021804267 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3762052811(__this, method) ((  int32_t (*) (KeyCollection_t3021804267 *, const MethodInfo*))KeyCollection_get_Count_m3762052811_gshared)(__this, method)
