﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelpScript/<MoveHand>c__Iterator0
struct U3CMoveHandU3Ec__Iterator0_t3351712;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenHelpScript/<MoveHand>c__Iterator0::.ctor()
extern "C"  void U3CMoveHandU3Ec__Iterator0__ctor_m2565631947 (U3CMoveHandU3Ec__Iterator0_t3351712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OpenHelpScript/<MoveHand>c__Iterator0::MoveNext()
extern "C"  bool U3CMoveHandU3Ec__Iterator0_MoveNext_m252723881 (U3CMoveHandU3Ec__Iterator0_t3351712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OpenHelpScript/<MoveHand>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveHandU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2161690705 (U3CMoveHandU3Ec__Iterator0_t3351712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OpenHelpScript/<MoveHand>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveHandU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3419933225 (U3CMoveHandU3Ec__Iterator0_t3351712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpScript/<MoveHand>c__Iterator0::Dispose()
extern "C"  void U3CMoveHandU3Ec__Iterator0_Dispose_m3971776698 (U3CMoveHandU3Ec__Iterator0_t3351712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenHelpScript/<MoveHand>c__Iterator0::Reset()
extern "C"  void U3CMoveHandU3Ec__Iterator0_Reset_m1386796616 (U3CMoveHandU3Ec__Iterator0_t3351712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
