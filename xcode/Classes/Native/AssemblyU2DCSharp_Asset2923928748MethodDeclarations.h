﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Asset
struct Asset_t2923928748;

#include "codegen/il2cpp-codegen.h"

// System.Void Asset::.ctor()
extern "C"  void Asset__ctor_m2939389471 (Asset_t2923928748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
