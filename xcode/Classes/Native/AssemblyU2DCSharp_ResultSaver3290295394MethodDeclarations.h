﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultSaver
struct ResultSaver_t3290295394;
// LearningResultData
struct LearningResultData_t1558878615;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LearningResultData1558878615.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResultSaver::.ctor()
extern "C"  void ResultSaver__ctor_m2512352477 (ResultSaver_t3290295394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ResultSaver ResultSaver::GetInstance()
extern "C"  ResultSaver_t3290295394 * ResultSaver_GetInstance_m2010784047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultSaver::PushLearningResult(System.Int32,System.Int32,System.Int32,LearningResultData,System.Action,System.Action)
extern "C"  void ResultSaver_PushLearningResult_m3234452776 (ResultSaver_t3290295394 * __this, int32_t ___chapter0, int32_t ___category1, int32_t ___part2, LearningResultData_t1558878615 * ___result3, Action_t3226471752 * ___successCallback4, Action_t3226471752 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultSaver::DoPushLearningResult(System.String,LearningResultData,System.Action,System.Action)
extern "C"  void ResultSaver_DoPushLearningResult_m664522798 (ResultSaver_t3290295394 * __this, String_t* ___url0, LearningResultData_t1558878615 * ___result1, Action_t3226471752 * ___successCallback2, Action_t3226471752 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultSaver::PushSoundZip(System.Int32,System.Int32,System.Int32,System.String[],System.String,System.Action,System.Action)
extern "C"  void ResultSaver_PushSoundZip_m3783511932 (ResultSaver_t3290295394 * __this, int32_t ___chapter0, int32_t ___category1, int32_t ___part2, StringU5BU5D_t1642385972* ___soundFileNames3, String_t* ___zipFilePath4, Action_t3226471752 * ___successCallback5, Action_t3226471752 * ___errorCallback6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
