﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.beetsoft.utility.SoundManager
struct SoundManager_t1001218626;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_com_beetsoft_utility_SoundManager552765882.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"

// System.Void com.beetsoft.utility.SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m2137074192 (SoundManager_t1001218626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip com.beetsoft.utility.SoundManager::GetAudioClip(System.String)
extern "C"  AudioClip_t1932558630 * SoundManager_GetAudioClip_m3101739454 (SoundManager_t1001218626 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String com.beetsoft.utility.SoundManager::GetBundleName()
extern "C"  String_t* SoundManager_GetBundleName_m2406754226 (SoundManager_t1001218626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// com.beetsoft.utility.SoundManager/BundleSoundName com.beetsoft.utility.SoundManager::parseResPath(System.String)
extern "C"  BundleSoundName_t552765882  SoundManager_parseResPath_m1817217673 (SoundManager_t1001218626 * __this, String_t* ___resPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::LoadSound(System.String)
extern "C"  void SoundManager_LoadSound_m3340870587 (SoundManager_t1001218626 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::UnloadSound(System.String)
extern "C"  void SoundManager_UnloadSound_m3750677712 (SoundManager_t1001218626 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::UnloadAllSound()
extern "C"  void SoundManager_UnloadAllSound_m3572274615 (SoundManager_t1001218626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::PlayBGM(System.String)
extern "C"  void SoundManager_PlayBGM_m621691694 (SoundManager_t1001218626 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::StopBGM()
extern "C"  void SoundManager_StopBGM_m336722054 (SoundManager_t1001218626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::PlayOneShoot(UnityEngine.AudioClip)
extern "C"  void SoundManager_PlayOneShoot_m3825374448 (SoundManager_t1001218626 * __this, AudioClip_t1932558630 * ___audioClip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::PlayOneShoot(System.String)
extern "C"  void SoundManager_PlayOneShoot_m3334314453 (SoundManager_t1001218626 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::PlaySingle(System.String)
extern "C"  void SoundManager_PlaySingle_m854472230 (SoundManager_t1001218626 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::StopAllOneShoot()
extern "C"  void SoundManager_StopAllOneShoot_m1771342324 (SoundManager_t1001218626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::StopAll()
extern "C"  void SoundManager_StopAll_m3460432001 (SoundManager_t1001218626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// com.beetsoft.utility.SoundManager com.beetsoft.utility.SoundManager::get_instance()
extern "C"  SoundManager_t1001218626 * SoundManager_get_instance_m1097100402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility.SoundManager::OnDestroy()
extern "C"  void SoundManager_OnDestroy_m1171800883 (SoundManager_t1001218626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
