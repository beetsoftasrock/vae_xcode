﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterScript
struct  CharacterScript_t1308706256  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image CharacterScript::imgPostureType
	Image_t2042527209 * ___imgPostureType_3;
	// UnityEngine.UI.Image CharacterScript::imgEyesType
	Image_t2042527209 * ___imgEyesType_4;
	// UnityEngine.UI.Image CharacterScript::imgMouth
	Image_t2042527209 * ___imgMouth_5;
	// System.Single CharacterScript::resizeRatioWithHeight
	float ___resizeRatioWithHeight_6;
	// System.Single CharacterScript::speedLerp
	float ___speedLerp_7;
	// System.String CharacterScript::strSpawnPostion
	String_t* ___strSpawnPostion_8;
	// UnityEngine.Vector2 CharacterScript::spawPosition
	Vector2_t2243707579  ___spawPosition_9;
	// UnityEngine.Sprite CharacterScript::eyesSprite
	Sprite_t309593783 * ___eyesSprite_10;
	// UnityEngine.Sprite CharacterScript::mouthSprite
	Sprite_t309593783 * ___mouthSprite_11;
	// UnityEngine.Sprite CharacterScript::postureSprite
	Sprite_t309593783 * ___postureSprite_12;
	// System.String CharacterScript::mouthAnim
	String_t* ___mouthAnim_13;
	// System.String[] CharacterScript::eyesAnim
	StringU5BU5D_t1642385972* ___eyesAnim_14;
	// System.String CharacterScript::currentEyesAnim
	String_t* ___currentEyesAnim_15;
	// UnityEngine.Animator CharacterScript::animatorMouth
	Animator_t69676727 * ___animatorMouth_16;
	// System.Single CharacterScript::wait
	float ___wait_17;
	// System.Boolean CharacterScript::isBlink
	bool ___isBlink_18;
	// System.Boolean CharacterScript::isTalk
	bool ___isTalk_19;
	// System.String CharacterScript::pathURL
	String_t* ___pathURL_20;
	// System.Boolean CharacterScript::alternativeTalking
	bool ___alternativeTalking_21;
	// System.String CharacterScript::characterName
	String_t* ___characterName_22;
	// System.String CharacterScript::eyesTypeName
	String_t* ___eyesTypeName_23;
	// System.String CharacterScript::mouthTypeName
	String_t* ___mouthTypeName_24;
	// System.String CharacterScript::postureTypeName
	String_t* ___postureTypeName_25;

public:
	inline static int32_t get_offset_of_imgPostureType_3() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___imgPostureType_3)); }
	inline Image_t2042527209 * get_imgPostureType_3() const { return ___imgPostureType_3; }
	inline Image_t2042527209 ** get_address_of_imgPostureType_3() { return &___imgPostureType_3; }
	inline void set_imgPostureType_3(Image_t2042527209 * value)
	{
		___imgPostureType_3 = value;
		Il2CppCodeGenWriteBarrier(&___imgPostureType_3, value);
	}

	inline static int32_t get_offset_of_imgEyesType_4() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___imgEyesType_4)); }
	inline Image_t2042527209 * get_imgEyesType_4() const { return ___imgEyesType_4; }
	inline Image_t2042527209 ** get_address_of_imgEyesType_4() { return &___imgEyesType_4; }
	inline void set_imgEyesType_4(Image_t2042527209 * value)
	{
		___imgEyesType_4 = value;
		Il2CppCodeGenWriteBarrier(&___imgEyesType_4, value);
	}

	inline static int32_t get_offset_of_imgMouth_5() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___imgMouth_5)); }
	inline Image_t2042527209 * get_imgMouth_5() const { return ___imgMouth_5; }
	inline Image_t2042527209 ** get_address_of_imgMouth_5() { return &___imgMouth_5; }
	inline void set_imgMouth_5(Image_t2042527209 * value)
	{
		___imgMouth_5 = value;
		Il2CppCodeGenWriteBarrier(&___imgMouth_5, value);
	}

	inline static int32_t get_offset_of_resizeRatioWithHeight_6() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___resizeRatioWithHeight_6)); }
	inline float get_resizeRatioWithHeight_6() const { return ___resizeRatioWithHeight_6; }
	inline float* get_address_of_resizeRatioWithHeight_6() { return &___resizeRatioWithHeight_6; }
	inline void set_resizeRatioWithHeight_6(float value)
	{
		___resizeRatioWithHeight_6 = value;
	}

	inline static int32_t get_offset_of_speedLerp_7() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___speedLerp_7)); }
	inline float get_speedLerp_7() const { return ___speedLerp_7; }
	inline float* get_address_of_speedLerp_7() { return &___speedLerp_7; }
	inline void set_speedLerp_7(float value)
	{
		___speedLerp_7 = value;
	}

	inline static int32_t get_offset_of_strSpawnPostion_8() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___strSpawnPostion_8)); }
	inline String_t* get_strSpawnPostion_8() const { return ___strSpawnPostion_8; }
	inline String_t** get_address_of_strSpawnPostion_8() { return &___strSpawnPostion_8; }
	inline void set_strSpawnPostion_8(String_t* value)
	{
		___strSpawnPostion_8 = value;
		Il2CppCodeGenWriteBarrier(&___strSpawnPostion_8, value);
	}

	inline static int32_t get_offset_of_spawPosition_9() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___spawPosition_9)); }
	inline Vector2_t2243707579  get_spawPosition_9() const { return ___spawPosition_9; }
	inline Vector2_t2243707579 * get_address_of_spawPosition_9() { return &___spawPosition_9; }
	inline void set_spawPosition_9(Vector2_t2243707579  value)
	{
		___spawPosition_9 = value;
	}

	inline static int32_t get_offset_of_eyesSprite_10() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___eyesSprite_10)); }
	inline Sprite_t309593783 * get_eyesSprite_10() const { return ___eyesSprite_10; }
	inline Sprite_t309593783 ** get_address_of_eyesSprite_10() { return &___eyesSprite_10; }
	inline void set_eyesSprite_10(Sprite_t309593783 * value)
	{
		___eyesSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___eyesSprite_10, value);
	}

	inline static int32_t get_offset_of_mouthSprite_11() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___mouthSprite_11)); }
	inline Sprite_t309593783 * get_mouthSprite_11() const { return ___mouthSprite_11; }
	inline Sprite_t309593783 ** get_address_of_mouthSprite_11() { return &___mouthSprite_11; }
	inline void set_mouthSprite_11(Sprite_t309593783 * value)
	{
		___mouthSprite_11 = value;
		Il2CppCodeGenWriteBarrier(&___mouthSprite_11, value);
	}

	inline static int32_t get_offset_of_postureSprite_12() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___postureSprite_12)); }
	inline Sprite_t309593783 * get_postureSprite_12() const { return ___postureSprite_12; }
	inline Sprite_t309593783 ** get_address_of_postureSprite_12() { return &___postureSprite_12; }
	inline void set_postureSprite_12(Sprite_t309593783 * value)
	{
		___postureSprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___postureSprite_12, value);
	}

	inline static int32_t get_offset_of_mouthAnim_13() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___mouthAnim_13)); }
	inline String_t* get_mouthAnim_13() const { return ___mouthAnim_13; }
	inline String_t** get_address_of_mouthAnim_13() { return &___mouthAnim_13; }
	inline void set_mouthAnim_13(String_t* value)
	{
		___mouthAnim_13 = value;
		Il2CppCodeGenWriteBarrier(&___mouthAnim_13, value);
	}

	inline static int32_t get_offset_of_eyesAnim_14() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___eyesAnim_14)); }
	inline StringU5BU5D_t1642385972* get_eyesAnim_14() const { return ___eyesAnim_14; }
	inline StringU5BU5D_t1642385972** get_address_of_eyesAnim_14() { return &___eyesAnim_14; }
	inline void set_eyesAnim_14(StringU5BU5D_t1642385972* value)
	{
		___eyesAnim_14 = value;
		Il2CppCodeGenWriteBarrier(&___eyesAnim_14, value);
	}

	inline static int32_t get_offset_of_currentEyesAnim_15() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___currentEyesAnim_15)); }
	inline String_t* get_currentEyesAnim_15() const { return ___currentEyesAnim_15; }
	inline String_t** get_address_of_currentEyesAnim_15() { return &___currentEyesAnim_15; }
	inline void set_currentEyesAnim_15(String_t* value)
	{
		___currentEyesAnim_15 = value;
		Il2CppCodeGenWriteBarrier(&___currentEyesAnim_15, value);
	}

	inline static int32_t get_offset_of_animatorMouth_16() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___animatorMouth_16)); }
	inline Animator_t69676727 * get_animatorMouth_16() const { return ___animatorMouth_16; }
	inline Animator_t69676727 ** get_address_of_animatorMouth_16() { return &___animatorMouth_16; }
	inline void set_animatorMouth_16(Animator_t69676727 * value)
	{
		___animatorMouth_16 = value;
		Il2CppCodeGenWriteBarrier(&___animatorMouth_16, value);
	}

	inline static int32_t get_offset_of_wait_17() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___wait_17)); }
	inline float get_wait_17() const { return ___wait_17; }
	inline float* get_address_of_wait_17() { return &___wait_17; }
	inline void set_wait_17(float value)
	{
		___wait_17 = value;
	}

	inline static int32_t get_offset_of_isBlink_18() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___isBlink_18)); }
	inline bool get_isBlink_18() const { return ___isBlink_18; }
	inline bool* get_address_of_isBlink_18() { return &___isBlink_18; }
	inline void set_isBlink_18(bool value)
	{
		___isBlink_18 = value;
	}

	inline static int32_t get_offset_of_isTalk_19() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___isTalk_19)); }
	inline bool get_isTalk_19() const { return ___isTalk_19; }
	inline bool* get_address_of_isTalk_19() { return &___isTalk_19; }
	inline void set_isTalk_19(bool value)
	{
		___isTalk_19 = value;
	}

	inline static int32_t get_offset_of_pathURL_20() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___pathURL_20)); }
	inline String_t* get_pathURL_20() const { return ___pathURL_20; }
	inline String_t** get_address_of_pathURL_20() { return &___pathURL_20; }
	inline void set_pathURL_20(String_t* value)
	{
		___pathURL_20 = value;
		Il2CppCodeGenWriteBarrier(&___pathURL_20, value);
	}

	inline static int32_t get_offset_of_alternativeTalking_21() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___alternativeTalking_21)); }
	inline bool get_alternativeTalking_21() const { return ___alternativeTalking_21; }
	inline bool* get_address_of_alternativeTalking_21() { return &___alternativeTalking_21; }
	inline void set_alternativeTalking_21(bool value)
	{
		___alternativeTalking_21 = value;
	}

	inline static int32_t get_offset_of_characterName_22() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___characterName_22)); }
	inline String_t* get_characterName_22() const { return ___characterName_22; }
	inline String_t** get_address_of_characterName_22() { return &___characterName_22; }
	inline void set_characterName_22(String_t* value)
	{
		___characterName_22 = value;
		Il2CppCodeGenWriteBarrier(&___characterName_22, value);
	}

	inline static int32_t get_offset_of_eyesTypeName_23() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___eyesTypeName_23)); }
	inline String_t* get_eyesTypeName_23() const { return ___eyesTypeName_23; }
	inline String_t** get_address_of_eyesTypeName_23() { return &___eyesTypeName_23; }
	inline void set_eyesTypeName_23(String_t* value)
	{
		___eyesTypeName_23 = value;
		Il2CppCodeGenWriteBarrier(&___eyesTypeName_23, value);
	}

	inline static int32_t get_offset_of_mouthTypeName_24() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___mouthTypeName_24)); }
	inline String_t* get_mouthTypeName_24() const { return ___mouthTypeName_24; }
	inline String_t** get_address_of_mouthTypeName_24() { return &___mouthTypeName_24; }
	inline void set_mouthTypeName_24(String_t* value)
	{
		___mouthTypeName_24 = value;
		Il2CppCodeGenWriteBarrier(&___mouthTypeName_24, value);
	}

	inline static int32_t get_offset_of_postureTypeName_25() { return static_cast<int32_t>(offsetof(CharacterScript_t1308706256, ___postureTypeName_25)); }
	inline String_t* get_postureTypeName_25() const { return ___postureTypeName_25; }
	inline String_t** get_address_of_postureTypeName_25() { return &___postureTypeName_25; }
	inline void set_postureTypeName_25(String_t* value)
	{
		___postureTypeName_25 = value;
		Il2CppCodeGenWriteBarrier(&___postureTypeName_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
