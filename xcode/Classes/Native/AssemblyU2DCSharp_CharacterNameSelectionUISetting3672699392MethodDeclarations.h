﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterNameSelectionUISetting
struct CharacterNameSelectionUISetting_t3672699392;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterNameSelectionUISetting::.ctor()
extern "C"  void CharacterNameSelectionUISetting__ctor_m279579915 (CharacterNameSelectionUISetting_t3672699392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterNameSelectionUISetting::Start()
extern "C"  void CharacterNameSelectionUISetting_Start_m531283463 (CharacterNameSelectionUISetting_t3672699392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
