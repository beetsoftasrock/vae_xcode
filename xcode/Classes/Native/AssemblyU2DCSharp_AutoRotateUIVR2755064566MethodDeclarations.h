﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoRotateUIVR
struct AutoRotateUIVR_t2755064566;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoRotateUIVR::.ctor()
extern "C"  void AutoRotateUIVR__ctor_m1903140529 (AutoRotateUIVR_t2755064566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoRotateUIVR::AutoRotateUI()
extern "C"  void AutoRotateUIVR_AutoRotateUI_m4013984751 (AutoRotateUIVR_t2755064566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
