﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<TrueFalseAnswerSelection>
struct List_1_t2775665709;

#include "AssemblyU2DCSharp_BaseData450384115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening1QuestionData
struct  Listening1QuestionData_t3381069024  : public BaseData_t450384115
{
public:
	// System.String Listening1QuestionData::<questionText>k__BackingField
	String_t* ___U3CquestionTextU3Ek__BackingField_0;
	// System.String Listening1QuestionData::<questionSound>k__BackingField
	String_t* ___U3CquestionSoundU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<TrueFalseAnswerSelection> Listening1QuestionData::answers
	List_1_t2775665709 * ___answers_2;

public:
	inline static int32_t get_offset_of_U3CquestionTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Listening1QuestionData_t3381069024, ___U3CquestionTextU3Ek__BackingField_0)); }
	inline String_t* get_U3CquestionTextU3Ek__BackingField_0() const { return ___U3CquestionTextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CquestionTextU3Ek__BackingField_0() { return &___U3CquestionTextU3Ek__BackingField_0; }
	inline void set_U3CquestionTextU3Ek__BackingField_0(String_t* value)
	{
		___U3CquestionTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionTextU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CquestionSoundU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Listening1QuestionData_t3381069024, ___U3CquestionSoundU3Ek__BackingField_1)); }
	inline String_t* get_U3CquestionSoundU3Ek__BackingField_1() const { return ___U3CquestionSoundU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CquestionSoundU3Ek__BackingField_1() { return &___U3CquestionSoundU3Ek__BackingField_1; }
	inline void set_U3CquestionSoundU3Ek__BackingField_1(String_t* value)
	{
		___U3CquestionSoundU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionSoundU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_answers_2() { return static_cast<int32_t>(offsetof(Listening1QuestionData_t3381069024, ___answers_2)); }
	inline List_1_t2775665709 * get_answers_2() const { return ___answers_2; }
	inline List_1_t2775665709 ** get_address_of_answers_2() { return &___answers_2; }
	inline void set_answers_2(List_1_t2775665709 * value)
	{
		___answers_2 = value;
		Il2CppCodeGenWriteBarrier(&___answers_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
