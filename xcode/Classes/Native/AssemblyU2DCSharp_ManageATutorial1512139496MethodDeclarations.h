﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManageATutorial
struct ManageATutorial_t1512139496;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"

// System.Void ManageATutorial::.ctor()
extern "C"  void ManageATutorial__ctor_m551929547 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ManageATutorial::get_IsTrackFirst()
extern "C"  bool ManageATutorial_get_IsTrackFirst_m1314027113 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageATutorial::Awake()
extern "C"  void ManageATutorial_Awake_m1080675780 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageATutorial::Update()
extern "C"  void ManageATutorial_Update_m1128227142 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform ManageATutorial::GetIconHand(System.Int32)
extern "C"  Transform_t3275118058 * ManageATutorial_GetIconHand_m4024508724 (ManageATutorial_t1512139496 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageATutorial::InactiveIconHand()
extern "C"  void ManageATutorial_InactiveIconHand_m1558340130 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageATutorial::OpenHelp()
extern "C"  void ManageATutorial_OpenHelp_m2249392720 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageATutorial::OpenNextHelp()
extern "C"  void ManageATutorial_OpenNextHelp_m172781613 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageATutorial::CloseHelp()
extern "C"  void ManageATutorial_CloseHelp_m918215060 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManageATutorial::SkipAll()
extern "C"  void ManageATutorial_SkipAll_m1613922227 (ManageATutorial_t1512139496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
