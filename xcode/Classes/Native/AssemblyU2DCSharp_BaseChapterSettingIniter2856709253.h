﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BaseSetting
struct BaseSetting_t2575616157;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseChapterSettingIniter
struct  BaseChapterSettingIniter_t2856709253  : public MonoBehaviour_t1158329972
{
public:
	// BaseSetting BaseChapterSettingIniter::_setting
	BaseSetting_t2575616157 * ____setting_2;

public:
	inline static int32_t get_offset_of__setting_2() { return static_cast<int32_t>(offsetof(BaseChapterSettingIniter_t2856709253, ____setting_2)); }
	inline BaseSetting_t2575616157 * get__setting_2() const { return ____setting_2; }
	inline BaseSetting_t2575616157 ** get_address_of__setting_2() { return &____setting_2; }
	inline void set__setting_2(BaseSetting_t2575616157 * value)
	{
		____setting_2 = value;
		Il2CppCodeGenWriteBarrier(&____setting_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
