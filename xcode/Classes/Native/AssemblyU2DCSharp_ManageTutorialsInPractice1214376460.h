﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CharacterSelector
struct CharacterSelector_t2041732578;
// System.Collections.Generic.List`1<ManageATutorial>
struct List_1_t881260628;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_ManageTutorialsASceneScript3167542912.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManageTutorialsInPractice
struct  ManageTutorialsInPractice_t1214376460  : public ManageTutorialsASceneScript_t3167542912
{
public:
	// CharacterSelector ManageTutorialsInPractice::characterSelector
	CharacterSelector_t2041732578 * ___characterSelector_2;
	// System.Collections.Generic.List`1<ManageATutorial> ManageTutorialsInPractice::liManageATutorial
	List_1_t881260628 * ___liManageATutorial_3;
	// UnityEngine.UI.Button ManageTutorialsInPractice::btnHelp
	Button_t2872111280 * ___btnHelp_4;
	// UnityEngine.GameObject ManageTutorialsInPractice::selectionDialog
	GameObject_t1756533147 * ___selectionDialog_5;

public:
	inline static int32_t get_offset_of_characterSelector_2() { return static_cast<int32_t>(offsetof(ManageTutorialsInPractice_t1214376460, ___characterSelector_2)); }
	inline CharacterSelector_t2041732578 * get_characterSelector_2() const { return ___characterSelector_2; }
	inline CharacterSelector_t2041732578 ** get_address_of_characterSelector_2() { return &___characterSelector_2; }
	inline void set_characterSelector_2(CharacterSelector_t2041732578 * value)
	{
		___characterSelector_2 = value;
		Il2CppCodeGenWriteBarrier(&___characterSelector_2, value);
	}

	inline static int32_t get_offset_of_liManageATutorial_3() { return static_cast<int32_t>(offsetof(ManageTutorialsInPractice_t1214376460, ___liManageATutorial_3)); }
	inline List_1_t881260628 * get_liManageATutorial_3() const { return ___liManageATutorial_3; }
	inline List_1_t881260628 ** get_address_of_liManageATutorial_3() { return &___liManageATutorial_3; }
	inline void set_liManageATutorial_3(List_1_t881260628 * value)
	{
		___liManageATutorial_3 = value;
		Il2CppCodeGenWriteBarrier(&___liManageATutorial_3, value);
	}

	inline static int32_t get_offset_of_btnHelp_4() { return static_cast<int32_t>(offsetof(ManageTutorialsInPractice_t1214376460, ___btnHelp_4)); }
	inline Button_t2872111280 * get_btnHelp_4() const { return ___btnHelp_4; }
	inline Button_t2872111280 ** get_address_of_btnHelp_4() { return &___btnHelp_4; }
	inline void set_btnHelp_4(Button_t2872111280 * value)
	{
		___btnHelp_4 = value;
		Il2CppCodeGenWriteBarrier(&___btnHelp_4, value);
	}

	inline static int32_t get_offset_of_selectionDialog_5() { return static_cast<int32_t>(offsetof(ManageTutorialsInPractice_t1214376460, ___selectionDialog_5)); }
	inline GameObject_t1756533147 * get_selectionDialog_5() const { return ___selectionDialog_5; }
	inline GameObject_t1756533147 ** get_address_of_selectionDialog_5() { return &___selectionDialog_5; }
	inline void set_selectionDialog_5(GameObject_t1756533147 * value)
	{
		___selectionDialog_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectionDialog_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
