﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t2068071072;
// BaseData
struct BaseData_t450384115;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t390723249;
// QuestionScenePopup
struct QuestionScenePopup_t792224618;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionScenePopup/<PlayAnimation>c__Iterator0
struct  U3CPlayAnimationU3Ec__Iterator0_t1105760000  : public Il2CppObject
{
public:
	// UnityEngine.Animation QuestionScenePopup/<PlayAnimation>c__Iterator0::anim
	Animation_t2068071072 * ___anim_0;
	// BaseData QuestionScenePopup/<PlayAnimation>c__Iterator0::baseData
	BaseData_t450384115 * ___baseData_1;
	// System.Collections.Generic.List`1<UnityEngine.Object> QuestionScenePopup/<PlayAnimation>c__Iterator0::answers
	List_1_t390723249 * ___answers_2;
	// QuestionScenePopup QuestionScenePopup/<PlayAnimation>c__Iterator0::$this
	QuestionScenePopup_t792224618 * ___U24this_3;
	// System.Object QuestionScenePopup/<PlayAnimation>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean QuestionScenePopup/<PlayAnimation>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 QuestionScenePopup/<PlayAnimation>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_anim_0() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ec__Iterator0_t1105760000, ___anim_0)); }
	inline Animation_t2068071072 * get_anim_0() const { return ___anim_0; }
	inline Animation_t2068071072 ** get_address_of_anim_0() { return &___anim_0; }
	inline void set_anim_0(Animation_t2068071072 * value)
	{
		___anim_0 = value;
		Il2CppCodeGenWriteBarrier(&___anim_0, value);
	}

	inline static int32_t get_offset_of_baseData_1() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ec__Iterator0_t1105760000, ___baseData_1)); }
	inline BaseData_t450384115 * get_baseData_1() const { return ___baseData_1; }
	inline BaseData_t450384115 ** get_address_of_baseData_1() { return &___baseData_1; }
	inline void set_baseData_1(BaseData_t450384115 * value)
	{
		___baseData_1 = value;
		Il2CppCodeGenWriteBarrier(&___baseData_1, value);
	}

	inline static int32_t get_offset_of_answers_2() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ec__Iterator0_t1105760000, ___answers_2)); }
	inline List_1_t390723249 * get_answers_2() const { return ___answers_2; }
	inline List_1_t390723249 ** get_address_of_answers_2() { return &___answers_2; }
	inline void set_answers_2(List_1_t390723249 * value)
	{
		___answers_2 = value;
		Il2CppCodeGenWriteBarrier(&___answers_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ec__Iterator0_t1105760000, ___U24this_3)); }
	inline QuestionScenePopup_t792224618 * get_U24this_3() const { return ___U24this_3; }
	inline QuestionScenePopup_t792224618 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(QuestionScenePopup_t792224618 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ec__Iterator0_t1105760000, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ec__Iterator0_t1105760000, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ec__Iterator0_t1105760000, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
