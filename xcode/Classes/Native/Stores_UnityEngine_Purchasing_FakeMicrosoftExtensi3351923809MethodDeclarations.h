﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FakeMicrosoftExtensions
struct FakeMicrosoftExtensions_t3351923809;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.FakeMicrosoftExtensions::.ctor()
extern "C"  void FakeMicrosoftExtensions__ctor_m4292199663 (FakeMicrosoftExtensions_t3351923809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.FakeMicrosoftExtensions::RestoreTransactions()
extern "C"  void FakeMicrosoftExtensions_RestoreTransactions_m1370110278 (FakeMicrosoftExtensions_t3351923809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
