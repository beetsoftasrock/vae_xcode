﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SentenceStructureIdiomQuestionData
struct SentenceStructureIdiomQuestionData_t3251732102;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SentenceStructureIdiomQuestionData::.ctor()
extern "C"  void SentenceStructureIdiomQuestionData__ctor_m3421911139 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceStructureIdiomQuestionData::get_answerText0()
extern "C"  String_t* SentenceStructureIdiomQuestionData_get_answerText0_m2021186326 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomQuestionData::set_answerText0(System.String)
extern "C"  void SentenceStructureIdiomQuestionData_set_answerText0_m1496345433 (SentenceStructureIdiomQuestionData_t3251732102 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceStructureIdiomQuestionData::get_answerText1()
extern "C"  String_t* SentenceStructureIdiomQuestionData_get_answerText1_m2021186231 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomQuestionData::set_answerText1(System.String)
extern "C"  void SentenceStructureIdiomQuestionData_set_answerText1_m2877081464 (SentenceStructureIdiomQuestionData_t3251732102 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceStructureIdiomQuestionData::get_answerText2()
extern "C"  String_t* SentenceStructureIdiomQuestionData_get_answerText2_m2021186260 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomQuestionData::set_answerText2(System.String)
extern "C"  void SentenceStructureIdiomQuestionData_set_answerText2_m926106519 (SentenceStructureIdiomQuestionData_t3251732102 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceStructureIdiomQuestionData::get_answerText3()
extern "C"  String_t* SentenceStructureIdiomQuestionData_get_answerText3_m2021186165 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomQuestionData::set_answerText3(System.String)
extern "C"  void SentenceStructureIdiomQuestionData_set_answerText3_m2306842550 (SentenceStructureIdiomQuestionData_t3251732102 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SentenceStructureIdiomQuestionData::get_correctAnswer()
extern "C"  int32_t SentenceStructureIdiomQuestionData_get_correctAnswer_m599091520 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomQuestionData::set_correctAnswer(System.Int32)
extern "C"  void SentenceStructureIdiomQuestionData_set_correctAnswer_m3401371489 (SentenceStructureIdiomQuestionData_t3251732102 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceStructureIdiomQuestionData::get_questionTextJP()
extern "C"  String_t* SentenceStructureIdiomQuestionData_get_questionTextJP_m4195830396 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomQuestionData::set_questionTextJP(System.String)
extern "C"  void SentenceStructureIdiomQuestionData_set_questionTextJP_m3539338261 (SentenceStructureIdiomQuestionData_t3251732102 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceStructureIdiomQuestionData::get_questionTextEN()
extern "C"  String_t* SentenceStructureIdiomQuestionData_get_questionTextEN_m4061315699 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomQuestionData::set_questionTextEN(System.String)
extern "C"  void SentenceStructureIdiomQuestionData_set_questionTextEN_m2187874554 (SentenceStructureIdiomQuestionData_t3251732102 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceStructureIdiomQuestionData::get_soundData()
extern "C"  String_t* SentenceStructureIdiomQuestionData_get_soundData_m1837454742 (SentenceStructureIdiomQuestionData_t3251732102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceStructureIdiomQuestionData::set_soundData(System.String)
extern "C"  void SentenceStructureIdiomQuestionData_set_soundData_m1629202035 (SentenceStructureIdiomQuestionData_t3251732102 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
