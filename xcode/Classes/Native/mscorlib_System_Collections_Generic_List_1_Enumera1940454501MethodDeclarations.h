﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<StateEmotionCharacter>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3218022704(__this, ___l0, method) ((  void (*) (Enumerator_t1940454501 *, List_1_t2405724827 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<StateEmotionCharacter>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m252503394(__this, method) ((  void (*) (Enumerator_t1940454501 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<StateEmotionCharacter>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2727266054(__this, method) ((  Il2CppObject * (*) (Enumerator_t1940454501 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<StateEmotionCharacter>::Dispose()
#define Enumerator_Dispose_m475015171(__this, method) ((  void (*) (Enumerator_t1940454501 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<StateEmotionCharacter>::VerifyState()
#define Enumerator_VerifyState_m2461086506(__this, method) ((  void (*) (Enumerator_t1940454501 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<StateEmotionCharacter>::MoveNext()
#define Enumerator_MoveNext_m2435625010(__this, method) ((  bool (*) (Enumerator_t1940454501 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<StateEmotionCharacter>::get_Current()
#define Enumerator_get_Current_m896314603(__this, method) ((  StateEmotionCharacter_t3036603695 * (*) (Enumerator_t1940454501 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
