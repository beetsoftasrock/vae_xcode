﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Beetsoft.VAE.Purchaser/PurchaseSession>
struct DefaultComparer_t3709984552;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor()
extern "C"  void DefaultComparer__ctor_m1163532141_gshared (DefaultComparer_t3709984552 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1163532141(__this, method) ((  void (*) (DefaultComparer_t3709984552 *, const MethodInfo*))DefaultComparer__ctor_m1163532141_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Beetsoft.VAE.Purchaser/PurchaseSession>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1506349670_gshared (DefaultComparer_t3709984552 * __this, PurchaseSession_t2088054391  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1506349670(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3709984552 *, PurchaseSession_t2088054391 , const MethodInfo*))DefaultComparer_GetHashCode_m1506349670_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Beetsoft.VAE.Purchaser/PurchaseSession>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3785207158_gshared (DefaultComparer_t3709984552 * __this, PurchaseSession_t2088054391  ___x0, PurchaseSession_t2088054391  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3785207158(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3709984552 *, PurchaseSession_t2088054391 , PurchaseSession_t2088054391 , const MethodInfo*))DefaultComparer_Equals_m3785207158_gshared)(__this, ___x0, ___y1, method)
