﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterScript
struct CharacterScript_t1308706256;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "System_Core_System_Action3226471752.h"

// System.Void CharacterScript::.ctor()
extern "C"  void CharacterScript__ctor_m2175527885 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::Start()
extern "C"  void CharacterScript_Start_m1923811669 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterScript::get_IsTalk()
extern "C"  bool CharacterScript_get_IsTalk_m2517283786 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::set_IsTalk(System.Boolean)
extern "C"  void CharacterScript_set_IsTalk_m2831481439 (CharacterScript_t1308706256 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::RandomMouthSpeed()
extern "C"  void CharacterScript_RandomMouthSpeed_m229412474 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CharacterScript::ChangeSpeed()
extern "C"  Il2CppObject * CharacterScript_ChangeSpeed_m2022503340 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CharacterScript::GetRandomTimeValue()
extern "C"  float CharacterScript_GetRandomTimeValue_m1220749134 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CharacterScript::GetRandomSpeed()
extern "C"  float CharacterScript_GetRandomSpeed_m280394425 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterScript::get_IsBlink()
extern "C"  bool CharacterScript_get_IsBlink_m3127771300 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::set_IsBlink(System.Boolean)
extern "C"  void CharacterScript_set_IsBlink_m2157958567 (CharacterScript_t1308706256 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CharacterScript::get_WaitValue()
extern "C"  float CharacterScript_get_WaitValue_m661577816 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::set_WaitValue(System.Single)
extern "C"  void CharacterScript_set_WaitValue_m1781852325 (CharacterScript_t1308706256 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CharacterScript::get_EyesTypeName()
extern "C"  String_t* CharacterScript_get_EyesTypeName_m3587429416 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::set_EyesTypeName(System.String)
extern "C"  void CharacterScript_set_EyesTypeName_m1893732411 (CharacterScript_t1308706256 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CharacterScript::get_MouthTypeName()
extern "C"  String_t* CharacterScript_get_MouthTypeName_m3673375309 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::set_MouthTypeName(System.String)
extern "C"  void CharacterScript_set_MouthTypeName_m2200180794 (CharacterScript_t1308706256 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CharacterScript::get_PostureTypeName()
extern "C"  String_t* CharacterScript_get_PostureTypeName_m3007722038 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::set_PostureTypeName(System.String)
extern "C"  void CharacterScript_set_PostureTypeName_m3503655015 (CharacterScript_t1308706256 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 CharacterScript::get_SpawPostion()
extern "C"  Vector2_t2243707579  CharacterScript_get_SpawPostion_m3070153082 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::set_SpawPostion(UnityEngine.Vector2)
extern "C"  void CharacterScript_set_SpawPostion_m1609662149 (CharacterScript_t1308706256 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CharacterScript::get_CharacterName()
extern "C"  String_t* CharacterScript_get_CharacterName_m4226027223 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::set_CharacterName(System.String)
extern "C"  void CharacterScript_set_CharacterName_m254764762 (CharacterScript_t1308706256 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::InitCharacter(System.String)
extern "C"  void CharacterScript_InitCharacter_m1127214408 (CharacterScript_t1308706256 * __this, String_t* ___pathCharacter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::BuildCharacter()
extern "C"  void CharacterScript_BuildCharacter_m1514647296 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::SetSprite(UnityEngine.UI.Image,UnityEngine.Sprite)
extern "C"  void CharacterScript_SetSprite_m3775479412 (CharacterScript_t1308706256 * __this, Image_t2042527209 * ___part0, Sprite_t309593783 * ___sp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite CharacterScript::GetSprite(System.String)
extern "C"  Sprite_t309593783 * CharacterScript_GetSprite_m504843709 (CharacterScript_t1308706256 * __this, String_t* ___pathToSprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite CharacterScript::GetSpriteFromAssetBundle(System.String)
extern "C"  Sprite_t309593783 * CharacterScript_GetSpriteFromAssetBundle_m2919634523 (CharacterScript_t1308706256 * __this, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::ConvertSpawnPosition(System.String)
extern "C"  void CharacterScript_ConvertSpawnPosition_m4099673156 (CharacterScript_t1308706256 * __this, String_t* ____spawnPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CharacterScript::ConvertDimension(System.String,System.Single)
extern "C"  float CharacterScript_ConvertDimension_m881068243 (CharacterScript_t1308706256 * __this, String_t* ___ratio0, float ___screenSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript::CallWaitTalking()
extern "C"  void CharacterScript_CallWaitTalking_m3075685398 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CharacterScript::WaitTalking()
extern "C"  Il2CppObject * CharacterScript_WaitTalking_m3229092678 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CharacterScript::DelaySomething(System.Action,System.Single)
extern "C"  Il2CppObject * CharacterScript_DelaySomething_m3731262010 (CharacterScript_t1308706256 * __this, Action_t3226471752 * ___function0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CharacterScript::LerpCharacterToSpawnPosition()
extern "C"  Il2CppObject * CharacterScript_LerpCharacterToSpawnPosition_m1937295860 (CharacterScript_t1308706256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
