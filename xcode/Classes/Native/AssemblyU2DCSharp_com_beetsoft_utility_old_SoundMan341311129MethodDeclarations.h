﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0
struct U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::.ctor()
extern "C"  void U3CLoadSoundFromExFileU3Ec__Iterator0__ctor_m1894416380 (U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadSoundFromExFileU3Ec__Iterator0_MoveNext_m3821072644 (U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadSoundFromExFileU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m975070410 (U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadSoundFromExFileU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1455280930 (U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::Dispose()
extern "C"  void U3CLoadSoundFromExFileU3Ec__Iterator0_Dispose_m3101445987 (U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.SoundManager/<LoadSoundFromExFile>c__Iterator0::Reset()
extern "C"  void U3CLoadSoundFromExFileU3Ec__Iterator0_Reset_m3247520385 (U3CLoadSoundFromExFileU3Ec__Iterator0_t341311129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
