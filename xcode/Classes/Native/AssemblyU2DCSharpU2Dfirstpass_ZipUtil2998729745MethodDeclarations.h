﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZipUtil
struct ZipUtil_t2998729745;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ZipUtil::.ctor()
extern "C"  void ZipUtil__ctor_m3230271356 (ZipUtil_t2998729745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZipUtil::unzip(System.String,System.String)
extern "C"  void ZipUtil_unzip_m397363156 (Il2CppObject * __this /* static, unused */, String_t* ___zipFilePath0, String_t* ___location1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZipUtil::zip(System.String)
extern "C"  void ZipUtil_zip_m1670743627 (Il2CppObject * __this /* static, unused */, String_t* ___zipFilePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZipUtil::addZipFile(System.String)
extern "C"  void ZipUtil_addZipFile_m3783422008 (Il2CppObject * __this /* static, unused */, String_t* ___addFile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZipUtil::Unzip(System.String,System.String)
extern "C"  void ZipUtil_Unzip_m775988660 (Il2CppObject * __this /* static, unused */, String_t* ___zipFilePath0, String_t* ___location1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZipUtil::Zip(System.String,System.String[])
extern "C"  void ZipUtil_Zip_m2650750703 (Il2CppObject * __this /* static, unused */, String_t* ___zipFileName0, StringU5BU5D_t1642385972* ___files1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
