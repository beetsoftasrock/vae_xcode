﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyPageView
struct MyPageView_t3400191636;

#include "codegen/il2cpp-codegen.h"

// System.Void MyPageView::.ctor()
extern "C"  void MyPageView__ctor_m3851691369 (MyPageView_t3400191636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageView::Start()
extern "C"  void MyPageView_Start_m3894637793 (MyPageView_t3400191636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageView::Update()
extern "C"  void MyPageView_Update_m358918818 (MyPageView_t3400191636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
