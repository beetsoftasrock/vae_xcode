﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundCommand/<Execute>c__Iterator1
struct U3CExecuteU3Ec__Iterator1_t748120696;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundCommand/<Execute>c__Iterator1::.ctor()
extern "C"  void U3CExecuteU3Ec__Iterator1__ctor_m842476485 (U3CExecuteU3Ec__Iterator1_t748120696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundCommand/<Execute>c__Iterator1::MoveNext()
extern "C"  bool U3CExecuteU3Ec__Iterator1_MoveNext_m3159289619 (U3CExecuteU3Ec__Iterator1_t748120696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SoundCommand/<Execute>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1066456459 (U3CExecuteU3Ec__Iterator1_t748120696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SoundCommand/<Execute>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m299196355 (U3CExecuteU3Ec__Iterator1_t748120696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundCommand/<Execute>c__Iterator1::Dispose()
extern "C"  void U3CExecuteU3Ec__Iterator1_Dispose_m440108402 (U3CExecuteU3Ec__Iterator1_t748120696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundCommand/<Execute>c__Iterator1::Reset()
extern "C"  void U3CExecuteU3Ec__Iterator1_Reset_m3179135500 (U3CExecuteU3Ec__Iterator1_t748120696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
