﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "AssemblyU2DCSharp_QuestionScenePopup792224618.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening1Popup
struct  Listening1Popup_t1745092138  : public QuestionScenePopup_t792224618
{
public:
	// UnityEngine.GameObject Listening1Popup::textPrefab
	GameObject_t1756533147 * ___textPrefab_9;
	// UnityEngine.GameObject Listening1Popup::answerTextHolder
	GameObject_t1756533147 * ___answerTextHolder_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Listening1Popup::_answerObjs
	List_1_t1125654279 * ____answerObjs_11;

public:
	inline static int32_t get_offset_of_textPrefab_9() { return static_cast<int32_t>(offsetof(Listening1Popup_t1745092138, ___textPrefab_9)); }
	inline GameObject_t1756533147 * get_textPrefab_9() const { return ___textPrefab_9; }
	inline GameObject_t1756533147 ** get_address_of_textPrefab_9() { return &___textPrefab_9; }
	inline void set_textPrefab_9(GameObject_t1756533147 * value)
	{
		___textPrefab_9 = value;
		Il2CppCodeGenWriteBarrier(&___textPrefab_9, value);
	}

	inline static int32_t get_offset_of_answerTextHolder_10() { return static_cast<int32_t>(offsetof(Listening1Popup_t1745092138, ___answerTextHolder_10)); }
	inline GameObject_t1756533147 * get_answerTextHolder_10() const { return ___answerTextHolder_10; }
	inline GameObject_t1756533147 ** get_address_of_answerTextHolder_10() { return &___answerTextHolder_10; }
	inline void set_answerTextHolder_10(GameObject_t1756533147 * value)
	{
		___answerTextHolder_10 = value;
		Il2CppCodeGenWriteBarrier(&___answerTextHolder_10, value);
	}

	inline static int32_t get_offset_of__answerObjs_11() { return static_cast<int32_t>(offsetof(Listening1Popup_t1745092138, ____answerObjs_11)); }
	inline List_1_t1125654279 * get__answerObjs_11() const { return ____answerObjs_11; }
	inline List_1_t1125654279 ** get_address_of__answerObjs_11() { return &____answerObjs_11; }
	inline void set__answerObjs_11(List_1_t1125654279 * value)
	{
		____answerObjs_11 = value;
		Il2CppCodeGenWriteBarrier(&____answerObjs_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
