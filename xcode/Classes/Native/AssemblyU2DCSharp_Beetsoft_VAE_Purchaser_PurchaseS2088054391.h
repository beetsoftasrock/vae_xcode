﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.Purchaser/PurchaseSession
struct  PurchaseSession_t2088054391 
{
public:
	// System.String Beetsoft.VAE.Purchaser/PurchaseSession::productId
	String_t* ___productId_0;
	// System.Action Beetsoft.VAE.Purchaser/PurchaseSession::onSuccess
	Action_t3226471752 * ___onSuccess_1;
	// System.Action`1<System.String> Beetsoft.VAE.Purchaser/PurchaseSession::onFailure
	Action_1_t1831019615 * ___onFailure_2;

public:
	inline static int32_t get_offset_of_productId_0() { return static_cast<int32_t>(offsetof(PurchaseSession_t2088054391, ___productId_0)); }
	inline String_t* get_productId_0() const { return ___productId_0; }
	inline String_t** get_address_of_productId_0() { return &___productId_0; }
	inline void set_productId_0(String_t* value)
	{
		___productId_0 = value;
		Il2CppCodeGenWriteBarrier(&___productId_0, value);
	}

	inline static int32_t get_offset_of_onSuccess_1() { return static_cast<int32_t>(offsetof(PurchaseSession_t2088054391, ___onSuccess_1)); }
	inline Action_t3226471752 * get_onSuccess_1() const { return ___onSuccess_1; }
	inline Action_t3226471752 ** get_address_of_onSuccess_1() { return &___onSuccess_1; }
	inline void set_onSuccess_1(Action_t3226471752 * value)
	{
		___onSuccess_1 = value;
		Il2CppCodeGenWriteBarrier(&___onSuccess_1, value);
	}

	inline static int32_t get_offset_of_onFailure_2() { return static_cast<int32_t>(offsetof(PurchaseSession_t2088054391, ___onFailure_2)); }
	inline Action_1_t1831019615 * get_onFailure_2() const { return ___onFailure_2; }
	inline Action_1_t1831019615 ** get_address_of_onFailure_2() { return &___onFailure_2; }
	inline void set_onFailure_2(Action_1_t1831019615 * value)
	{
		___onFailure_2 = value;
		Il2CppCodeGenWriteBarrier(&___onFailure_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Beetsoft.VAE.Purchaser/PurchaseSession
struct PurchaseSession_t2088054391_marshaled_pinvoke
{
	char* ___productId_0;
	Il2CppMethodPointer ___onSuccess_1;
	Il2CppMethodPointer ___onFailure_2;
};
// Native definition for COM marshalling of Beetsoft.VAE.Purchaser/PurchaseSession
struct PurchaseSession_t2088054391_marshaled_com
{
	Il2CppChar* ___productId_0;
	Il2CppMethodPointer ___onSuccess_1;
	Il2CppMethodPointer ___onFailure_2;
};
