﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening1SceneController/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t1868754606;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening1SceneController/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m1368487997 (U3CStartU3Ec__Iterator0_t1868754606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening1SceneController/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m966912963 (U3CStartU3Ec__Iterator0_t1868754606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Listening1SceneController/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m740658271 (U3CStartU3Ec__Iterator0_t1868754606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Listening1SceneController/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m251170759 (U3CStartU3Ec__Iterator0_t1868754606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1SceneController/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2685741000 (U3CStartU3Ec__Iterator0_t1868754606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1SceneController/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m369031802 (U3CStartU3Ec__Iterator0_t1868754606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
