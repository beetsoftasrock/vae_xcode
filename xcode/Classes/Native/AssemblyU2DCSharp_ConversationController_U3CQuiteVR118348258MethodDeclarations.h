﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationController/<QuiteVRMode>c__AnonStorey0
struct U3CQuiteVRModeU3Ec__AnonStorey0_t118348258;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationController/<QuiteVRMode>c__AnonStorey0::.ctor()
extern "C"  void U3CQuiteVRModeU3Ec__AnonStorey0__ctor_m1918892795 (U3CQuiteVRModeU3Ec__AnonStorey0_t118348258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationController/<QuiteVRMode>c__AnonStorey0::<>m__0()
extern "C"  void U3CQuiteVRModeU3Ec__AnonStorey0_U3CU3Em__0_m697933996 (U3CQuiteVRModeU3Ec__AnonStorey0_t118348258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
