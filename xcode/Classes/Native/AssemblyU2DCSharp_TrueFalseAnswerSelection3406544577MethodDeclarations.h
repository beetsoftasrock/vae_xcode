﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrueFalseAnswerSelection
struct TrueFalseAnswerSelection_t3406544577;

#include "codegen/il2cpp-codegen.h"

// System.Void TrueFalseAnswerSelection::.ctor()
extern "C"  void TrueFalseAnswerSelection__ctor_m1242095912 (TrueFalseAnswerSelection_t3406544577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
