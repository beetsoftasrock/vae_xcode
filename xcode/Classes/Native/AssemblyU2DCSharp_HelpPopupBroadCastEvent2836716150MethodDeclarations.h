﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HelpPopupBroadCastEvent
struct HelpPopupBroadCastEvent_t2836716150;

#include "codegen/il2cpp-codegen.h"

// System.Void HelpPopupBroadCastEvent::.ctor()
extern "C"  void HelpPopupBroadCastEvent__ctor_m2591763355 (HelpPopupBroadCastEvent_t2836716150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
