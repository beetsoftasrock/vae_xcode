﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterMyPageScript
struct ChapterMyPageScript_t3593529611;
// Table
struct Table_t4160395208;
// Cell
struct Cell_t3051913968;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Table4160395208.h"
#include "AssemblyU2DCSharp_Cell3051913968.h"
#include "System_Core_System_Action3226471752.h"

// System.Void ChapterMyPageScript::.ctor()
extern "C"  void ChapterMyPageScript__ctor_m2950060670 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::set_TableData(Table)
extern "C"  void ChapterMyPageScript_set_TableData_m2246257087 (ChapterMyPageScript_t3593529611 * __this, Table_t4160395208 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::Start()
extern "C"  void ChapterMyPageScript_Start_m3229177490 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::UpdateData()
extern "C"  void ChapterMyPageScript_UpdateData_m2887775963 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::BuildChapter()
extern "C"  void ChapterMyPageScript_BuildChapter_m2927204273 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::LoadValueFromData()
extern "C"  void ChapterMyPageScript_LoadValueFromData_m686877001 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::DisplayTable()
extern "C"  void ChapterMyPageScript_DisplayTable_m2462128038 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::DisplayTimeTotal()
extern "C"  void ChapterMyPageScript_DisplayTimeTotal_m38059515 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::DisplayRecommend()
extern "C"  void ChapterMyPageScript_DisplayRecommend_m1582403724 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::DisplayStatusLession(Cell)
extern "C"  void ChapterMyPageScript_DisplayStatusLession_m870216699 (ChapterMyPageScript_t3593529611 * __this, Cell_t3051913968 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::ClickStudy()
extern "C"  void ChapterMyPageScript_ClickStudy_m2628588883 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::UnloadBundleData()
extern "C"  void ChapterMyPageScript_UnloadBundleData_m405121007 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::WaitFadeOut(System.Action)
extern "C"  void ChapterMyPageScript_WaitFadeOut_m999771014 (ChapterMyPageScript_t3593529611 * __this, Action_t3226471752 * ___onFinish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChapterMyPageScript::waitFadeOut(System.Action)
extern "C"  Il2CppObject * ChapterMyPageScript_waitFadeOut_m2978384230 (ChapterMyPageScript_t3593529611 * __this, Action_t3226471752 * ___onFinish0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::OnDisable()
extern "C"  void ChapterMyPageScript_OnDisable_m2475386651 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Cell ChapterMyPageScript::GetCellMinPercent()
extern "C"  Cell_t3051913968 * ChapterMyPageScript_GetCellMinPercent_m907550352 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::<DisplayRecommend>m__0()
extern "C"  void ChapterMyPageScript_U3CDisplayRecommendU3Em__0_m4090404427 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::<ClickStudy>m__1()
extern "C"  void ChapterMyPageScript_U3CClickStudyU3Em__1_m1038063537 (ChapterMyPageScript_t3593529611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterMyPageScript::<UnloadBundleData>m__2()
extern "C"  void ChapterMyPageScript_U3CUnloadBundleDataU3Em__2_m3819075686 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
