﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnExcerciseSceneSelectionViewClickedEvent
struct OnExcerciseSceneSelectionViewClickedEvent_t3939024814;

#include "codegen/il2cpp-codegen.h"

// System.Void OnExcerciseSceneSelectionViewClickedEvent::.ctor()
extern "C"  void OnExcerciseSceneSelectionViewClickedEvent__ctor_m672575503 (OnExcerciseSceneSelectionViewClickedEvent_t3939024814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
