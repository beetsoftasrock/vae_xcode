﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager
struct AssetBundleManager_t364944953;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// UnityEngine.Object
struct Object_t1021602117;
// System.Type
struct Type_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"

// System.Void AssetBundles.AssetBundleManager::.ctor()
extern "C"  void AssetBundleManager__ctor_m83677269 (AssetBundleManager_t364944953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::OnRuntimeMethodLoad()
extern "C"  void AssetBundleManager_OnRuntimeMethodLoad_m3859079089 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] AssetBundles.AssetBundleManager::get_AllAssetBundles()
extern "C"  StringU5BU5D_t1642385972* AssetBundleManager_get_AllAssetBundles_m716594081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::set_AllAssetBundles(System.String[])
extern "C"  void AssetBundleManager_set_AllAssetBundles_m1546262334 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> AssetBundles.AssetBundleManager::get_SpecificVariants()
extern "C"  List_1_t1398341365 * AssetBundleManager_get_SpecificVariants_m1798518807 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::set_SpecificVariants(System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetBundleManager_set_SpecificVariants_m2646958054 (Il2CppObject * __this /* static, unused */, List_1_t1398341365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager::get_IsLoadedManifest()
extern "C"  bool AssetBundleManager_get_IsLoadedManifest_m3171936894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager::get_SimulationMode()
extern "C"  bool AssetBundleManager_get_SimulationMode_m2065641526 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager::CheckBundleExist(System.String)
extern "C"  bool AssetBundleManager_CheckBundleExist_m2327948338 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::UnloadAssetManifest()
extern "C"  void AssetBundleManager_UnloadAssetManifest_m3988461475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::LoadAssetManifest(System.Action`1<System.Single>,System.Action,System.Action`1<System.String>)
extern "C"  void AssetBundleManager_LoadAssetManifest_m3144147926 (Il2CppObject * __this /* static, unused */, Action_1_t1878309314 * ___onProcess0, Action_t3226471752 * ___onSuccess1, Action_1_t1831019615 * ___onFailure2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetBundles.AssetBundleManager::GetUrlOfBundle(System.String)
extern "C"  String_t* AssetBundleManager_GetUrlOfBundle_m2895315472 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager::IsVersionCached(System.String)
extern "C"  bool AssetBundleManager_IsVersionCached_m2640614535 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle AssetBundles.AssetBundleManager::GetAssetBundle(System.String)
extern "C"  AssetBundle_t2054978754 * AssetBundleManager_GetAssetBundle_m1404232681 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::LoadAssetBundle(System.String,System.Action`1<System.Single>,System.Action,System.Action`1<System.String>)
extern "C"  void AssetBundleManager_LoadAssetBundle_m3953885407 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, Action_1_t1878309314 * ___onProcess1, Action_t3226471752 * ___onSuccess2, Action_1_t1831019615 * ___onError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::LoadAssetBundles(System.String[],System.Action`1<System.Single>,System.Action,System.Action`1<System.String>)
extern "C"  void AssetBundleManager_LoadAssetBundles_m1973800938 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___bundlesName0, Action_1_t1878309314 * ___onProcess1, Action_t3226471752 * ___onSuccess2, Action_1_t1831019615 * ___onError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::LoadAllAssetBundles(System.Action`1<System.Single>,System.Action,System.Action`1<System.String>)
extern "C"  void AssetBundleManager_LoadAllAssetBundles_m2622725323 (Il2CppObject * __this /* static, unused */, Action_1_t1878309314 * ___onProcess0, Action_t3226471752 * ___onSuccess1, Action_1_t1831019615 * ___onError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::UnloadAssetBundle(System.String,System.Boolean)
extern "C"  void AssetBundleManager_UnloadAssetBundle_m1405747465 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, bool ___keepLoadeds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::UnloadAssetBundles(System.String[],System.Boolean)
extern "C"  void AssetBundleManager_UnloadAssetBundles_m1836426014 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___bundlesName0, bool ___keepLoadeds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::UnloadAllAssetBundle()
extern "C"  void AssetBundleManager_UnloadAllAssetBundle_m3142558901 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object AssetBundles.AssetBundleManager::LoadAsset(System.String,System.String,System.Type)
extern "C"  Object_t1021602117 * AssetBundleManager_LoadAsset_m1935321153 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___assetName1, Type_t * ___objectType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object AssetBundles.AssetBundleManager::LoadAsset(System.String,System.String)
extern "C"  Object_t1021602117 * AssetBundleManager_LoadAsset_m3120051400 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager::IsLoadedBundle(System.String)
extern "C"  bool AssetBundleManager_IsLoadedBundle_m1765708424 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager::IsLoadedBundles(System.String[])
extern "C"  bool AssetBundleManager_IsLoadedBundles_m4009944707 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___bundleNames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::LoadScene(System.String,System.String,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean)
extern "C"  void AssetBundleManager_LoadScene_m484767989 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___sceneName1, int32_t ___loadMode2, bool ___asyncLoad3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetBundles.AssetBundleManager::GetAssetPath(System.String,System.String)
extern "C"  String_t* AssetBundleManager_GetAssetPath_m3940680047 (Il2CppObject * __this /* static, unused */, String_t* ___bundleName0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetBundles.AssetBundleManager::BundleNameFromPath(System.String)
extern "C"  String_t* AssetBundleManager_BundleNameFromPath_m646132936 (Il2CppObject * __this /* static, unused */, String_t* ___assetPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AssetBundles.AssetBundleManager::AssetNameFromPath(System.String)
extern "C"  String_t* AssetBundleManager_AssetNameFromPath_m3937525558 (Il2CppObject * __this /* static, unused */, String_t* ___assetPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::Awake()
extern "C"  void AssetBundleManager_Awake_m4040763894 (AssetBundleManager_t364944953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager::OnApplicationQuit()
extern "C"  void AssetBundleManager_OnApplicationQuit_m1652509623 (AssetBundleManager_t364944953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AssetBundles.AssetBundleManager::loadManifestFile(System.Action`1<System.Single>,System.Action,System.Action`1<System.String>)
extern "C"  Il2CppObject * AssetBundleManager_loadManifestFile_m3996899520 (AssetBundleManager_t364944953 * __this, Action_1_t1878309314 * ___onProcess0, Action_t3226471752 * ___onSuccess1, Action_1_t1831019615 * ___onError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AssetBundles.AssetBundleManager::loadAssetBundles(System.String[],System.Action`1<System.Single>,System.Action,System.Action`1<System.String>)
extern "C"  Il2CppObject * AssetBundleManager_loadAssetBundles_m619061458 (AssetBundleManager_t364944953 * __this, StringU5BU5D_t1642385972* ___includeBundles0, Action_1_t1878309314 * ___onProcess1, Action_t3226471752 * ___onSuccess2, Action_1_t1831019615 * ___onError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AssetBundles.AssetBundleManager::loadAssetBundle(System.String,System.Action`1<System.Single>,System.Action,System.Action`1<System.String>)
extern "C"  Il2CppObject * AssetBundleManager_loadAssetBundle_m2607258941 (AssetBundleManager_t364944953 * __this, String_t* ___bundleName0, Action_1_t1878309314 * ___onProcess1, Action_t3226471752 * ___onSuccess2, Action_1_t1831019615 * ___onError3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager::safeEquals(System.Byte[],System.Byte[])
extern "C"  bool AssetBundleManager_safeEquals_m1647450315 (AssetBundleManager_t364944953 * __this, ByteU5BU5D_t3397334013* ___strA0, ByteU5BU5D_t3397334013* ___strB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager::containsArr(System.String,System.String[])
extern "C"  bool AssetBundleManager_containsArr_m657549769 (AssetBundleManager_t364944953 * __this, String_t* ___text0, StringU5BU5D_t1642385972* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
