﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.UIPolygon
struct UIPolygon_t4199244354;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"

// System.Void UnityEngine.UI.Extensions.UIPolygon::.ctor()
extern "C"  void UIPolygon__ctor_m1684570140 (UIPolygon_t4199244354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.UI.Extensions.UIPolygon::get_mainTexture()
extern "C"  Texture_t2243626319 * UIPolygon_get_mainTexture_m1915020024 (UIPolygon_t4199244354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.UI.Extensions.UIPolygon::get_texture()
extern "C"  Texture_t2243626319 * UIPolygon_get_texture_m3721646203 (UIPolygon_t4199244354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIPolygon::set_texture(UnityEngine.Texture)
extern "C"  void UIPolygon_set_texture_m1091734044 (UIPolygon_t4199244354 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIPolygon::DrawPolygon(System.Int32)
extern "C"  void UIPolygon_DrawPolygon_m424989459 (UIPolygon_t4199244354 * __this, int32_t ____sides0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIPolygon::DrawPolygon(System.Int32,System.Single[])
extern "C"  void UIPolygon_DrawPolygon_m691248886 (UIPolygon_t4199244354 * __this, int32_t ____sides0, SingleU5BU5D_t577127397* ____VerticesDistances1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIPolygon::DrawPolygon(System.Int32,System.Single[],System.Single)
extern "C"  void UIPolygon_DrawPolygon_m3149548373 (UIPolygon_t4199244354 * __this, int32_t ____sides0, SingleU5BU5D_t577127397* ____VerticesDistances1, float ____rotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIPolygon::Update()
extern "C"  void UIPolygon_Update_m321581879 (UIPolygon_t4199244354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UIVertex[] UnityEngine.UI.Extensions.UIPolygon::SetVbo(UnityEngine.Vector2[],UnityEngine.Vector2[])
extern "C"  UIVertexU5BU5D_t3048644023* UIPolygon_SetVbo_m783592001 (UIPolygon_t4199244354 * __this, Vector2U5BU5D_t686124026* ___vertices0, Vector2U5BU5D_t686124026* ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIPolygon::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern "C"  void UIPolygon_OnPopulateMesh_m3804344973 (UIPolygon_t4199244354 * __this, VertexHelper_t385374196 * ___vh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
