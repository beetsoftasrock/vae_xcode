﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioRecorder.Recorder/OnInit
struct OnInit_t1674205282;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void AudioRecorder.Recorder/OnInit::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInit__ctor_m1795815659 (OnInit_t1674205282 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder/OnInit::Invoke(System.Boolean)
extern "C"  void OnInit_Invoke_m3381069704 (OnInit_t1674205282 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AudioRecorder.Recorder/OnInit::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnInit_BeginInvoke_m189379415 (OnInit_t1674205282 * __this, bool ___success0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder/OnInit::EndInvoke(System.IAsyncResult)
extern "C"  void OnInit_EndInvoke_m1145370205 (OnInit_t1674205282 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
