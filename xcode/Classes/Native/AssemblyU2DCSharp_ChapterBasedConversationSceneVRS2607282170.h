﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BaseConversationCommandLoader
struct BaseConversationCommandLoader_t1091304408;
// BaseConversationController
struct BaseConversationController_t808907754;

#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterBasedConversationSceneVRSetup
struct  ChapterBasedConversationSceneVRSetup_t2607282170  : public GlobalBasedSetupModule_t1655890765
{
public:
	// BaseConversationCommandLoader ChapterBasedConversationSceneVRSetup::commandLoader
	BaseConversationCommandLoader_t1091304408 * ___commandLoader_3;
	// BaseConversationController ChapterBasedConversationSceneVRSetup::conversationController
	BaseConversationController_t808907754 * ___conversationController_4;

public:
	inline static int32_t get_offset_of_commandLoader_3() { return static_cast<int32_t>(offsetof(ChapterBasedConversationSceneVRSetup_t2607282170, ___commandLoader_3)); }
	inline BaseConversationCommandLoader_t1091304408 * get_commandLoader_3() const { return ___commandLoader_3; }
	inline BaseConversationCommandLoader_t1091304408 ** get_address_of_commandLoader_3() { return &___commandLoader_3; }
	inline void set_commandLoader_3(BaseConversationCommandLoader_t1091304408 * value)
	{
		___commandLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___commandLoader_3, value);
	}

	inline static int32_t get_offset_of_conversationController_4() { return static_cast<int32_t>(offsetof(ChapterBasedConversationSceneVRSetup_t2607282170, ___conversationController_4)); }
	inline BaseConversationController_t808907754 * get_conversationController_4() const { return ___conversationController_4; }
	inline BaseConversationController_t808907754 ** get_address_of_conversationController_4() { return &___conversationController_4; }
	inline void set_conversationController_4(BaseConversationController_t808907754 * value)
	{
		___conversationController_4 = value;
		Il2CppCodeGenWriteBarrier(&___conversationController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
