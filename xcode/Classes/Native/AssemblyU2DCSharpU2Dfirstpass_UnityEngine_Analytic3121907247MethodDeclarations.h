﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Analytics.RemoteSettings
struct RemoteSettings_t3121907247;
// UnityEngine.Analytics.DriveableProperty
struct DriveableProperty_t2641992127;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Analytic2641992127.h"

// System.Void UnityEngine.Analytics.RemoteSettings::.ctor()
extern "C"  void RemoteSettings__ctor_m171676155 (RemoteSettings_t3121907247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Analytics.DriveableProperty UnityEngine.Analytics.RemoteSettings::get_DP()
extern "C"  DriveableProperty_t2641992127 * RemoteSettings_get_DP_m4259510491 (RemoteSettings_t3121907247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.RemoteSettings::set_DP(UnityEngine.Analytics.DriveableProperty)
extern "C"  void RemoteSettings_set_DP_m1524225954 (RemoteSettings_t3121907247 * __this, DriveableProperty_t2641992127 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.RemoteSettings::Start()
extern "C"  void RemoteSettings_Start_m3812251055 (RemoteSettings_t3121907247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.RemoteSettings::RemoteSettingsUpdated()
extern "C"  void RemoteSettings_RemoteSettingsUpdated_m1172715871 (RemoteSettings_t3121907247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
