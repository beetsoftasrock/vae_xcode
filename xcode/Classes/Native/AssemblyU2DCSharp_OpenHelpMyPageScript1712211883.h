﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_OpenHelpScript3207620564.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenHelpMyPageScript
struct  OpenHelpMyPageScript_t1712211883  : public OpenHelpScript_t3207620564
{
public:
	// UnityEngine.Transform OpenHelpMyPageScript::Chapter
	Transform_t3275118058 * ___Chapter_10;

public:
	inline static int32_t get_offset_of_Chapter_10() { return static_cast<int32_t>(offsetof(OpenHelpMyPageScript_t1712211883, ___Chapter_10)); }
	inline Transform_t3275118058 * get_Chapter_10() const { return ___Chapter_10; }
	inline Transform_t3275118058 ** get_address_of_Chapter_10() { return &___Chapter_10; }
	inline void set_Chapter_10(Transform_t3275118058 * value)
	{
		___Chapter_10 = value;
		Il2CppCodeGenWriteBarrier(&___Chapter_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
