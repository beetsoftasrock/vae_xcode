﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedConversationSceneVRSetup
struct ChapterBasedConversationSceneVRSetup_t2607282170;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedConversationSceneVRSetup::.ctor()
extern "C"  void ChapterBasedConversationSceneVRSetup__ctor_m2875306063 (ChapterBasedConversationSceneVRSetup_t2607282170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChapterBasedConversationSceneVRSetup::get_lessonName()
extern "C"  String_t* ChapterBasedConversationSceneVRSetup_get_lessonName_m1373340734 (ChapterBasedConversationSceneVRSetup_t2607282170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedConversationSceneVRSetup::SetupBackgroundScene()
extern "C"  void ChapterBasedConversationSceneVRSetup_SetupBackgroundScene_m3003242978 (ChapterBasedConversationSceneVRSetup_t2607282170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedConversationSceneVRSetup::Awake()
extern "C"  void ChapterBasedConversationSceneVRSetup_Awake_m990052282 (ChapterBasedConversationSceneVRSetup_t2607282170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
