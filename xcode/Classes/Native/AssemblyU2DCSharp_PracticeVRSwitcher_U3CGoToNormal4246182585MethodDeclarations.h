﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PracticeVRSwitcher/<GoToNormalMode>c__AnonStorey0
struct U3CGoToNormalModeU3Ec__AnonStorey0_t4246182585;

#include "codegen/il2cpp-codegen.h"

// System.Void PracticeVRSwitcher/<GoToNormalMode>c__AnonStorey0::.ctor()
extern "C"  void U3CGoToNormalModeU3Ec__AnonStorey0__ctor_m3979258630 (U3CGoToNormalModeU3Ec__AnonStorey0_t4246182585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeVRSwitcher/<GoToNormalMode>c__AnonStorey0::<>m__0()
extern "C"  void U3CGoToNormalModeU3Ec__AnonStorey0_U3CU3Em__0_m3871834267 (U3CGoToNormalModeU3Ec__AnonStorey0_t4246182585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
