﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChapterView
struct ChapterView_t3775077312;

#include "AssemblyU2DCSharp_UIController2029583246.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChaperController
struct  ChaperController_t3790003223  : public UIController_t2029583246
{
public:
	// ChapterView ChaperController::chapterView
	ChapterView_t3775077312 * ___chapterView_2;

public:
	inline static int32_t get_offset_of_chapterView_2() { return static_cast<int32_t>(offsetof(ChaperController_t3790003223, ___chapterView_2)); }
	inline ChapterView_t3775077312 * get_chapterView_2() const { return ___chapterView_2; }
	inline ChapterView_t3775077312 ** get_address_of_chapterView_2() { return &___chapterView_2; }
	inline void set_chapterView_2(ChapterView_t3775077312 * value)
	{
		___chapterView_2 = value;
		Il2CppCodeGenWriteBarrier(&___chapterView_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
