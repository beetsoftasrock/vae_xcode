﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DialogController
struct DialogController_t3845653284;
// DialogPanel
struct DialogPanel_t1568014038;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DialogPanel1568014038.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void DialogController::.ctor()
extern "C"  void DialogController__ctor_m2310177551 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::Start()
extern "C"  void DialogController_Start_m3972914147 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::SetAutoPlayTemplate()
extern "C"  void DialogController_SetAutoPlayTemplate_m3445611572 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::SetAutoPlay()
extern "C"  void DialogController_SetAutoPlay_m2002352702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::ReStart()
extern "C"  void DialogController_ReStart_m1860982300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::ReopenDialog(DialogPanel)
extern "C"  void DialogController_ReopenDialog_m3118895874 (DialogController_t3845653284 * __this, DialogPanel_t1568014038 * ___dialog0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::DisableSubDialog()
extern "C"  void DialogController_DisableSubDialog_m363976171 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::EnableSubYouDialog()
extern "C"  void DialogController_EnableSubYouDialog_m2715470181 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::ChangeDialog(DialogPanel)
extern "C"  void DialogController_ChangeDialog_m2504855447 (DialogController_t3845653284 * __this, DialogPanel_t1568014038 * ___dialog0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::SetDialog()
extern "C"  void DialogController_SetDialog_m1874178649 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::ShowNexthistory()
extern "C"  void DialogController_ShowNexthistory_m3493803649 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::EndDialog()
extern "C"  void DialogController_EndDialog_m3225238878 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::NextDialog()
extern "C"  void DialogController_NextDialog_m1301199178 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DialogController::isUser()
extern "C"  bool DialogController_isUser_m3802980002 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::MoveInHierarchy(UnityEngine.Transform)
extern "C"  void DialogController_MoveInHierarchy_m980406489 (DialogController_t3845653284 * __this, Transform_t3275118058 * ___tf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::UpdateCurrentContent()
extern "C"  void DialogController_UpdateCurrentContent_m2341412988 (DialogController_t3845653284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::Set(System.Collections.Generic.List`1<CommandSheet/Param>,System.Int32)
extern "C"  void DialogController_Set_m165219300 (DialogController_t3845653284 * __this, List_1_t3492939606 * ___conversation0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::SetActiveChangingMode(System.Boolean)
extern "C"  void DialogController_SetActiveChangingMode_m3481071700 (DialogController_t3845653284 * __this, bool ___isValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogController::.cctor()
extern "C"  void DialogController__cctor_m3171133534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
