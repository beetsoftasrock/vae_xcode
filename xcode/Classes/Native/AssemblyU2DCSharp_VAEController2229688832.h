﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// UIController
struct UIController_t2029583246;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VAEController
struct  VAEController_t2229688832  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct VAEController_t2229688832_StaticFields
{
public:
	// System.Boolean VAEController::isVR
	bool ___isVR_2;
	// System.Boolean VAEController::isChangingMode
	bool ___isChangingMode_3;
	// System.String VAEController::typeChar
	String_t* ___typeChar_4;
	// System.String VAEController::nameChar
	String_t* ___nameChar_5;
	// System.String VAEController::VRchar
	String_t* ___VRchar_6;
	// System.Collections.Generic.List`1<CommandSheet/Param> VAEController::PracticeComand
	List_1_t3492939606 * ___PracticeComand_7;
	// System.Int32 VAEController::currentIndex
	int32_t ___currentIndex_8;
	// System.Int32 VAEController::count
	int32_t ___count_9;
	// UIController VAEController::uiController
	UIController_t2029583246 * ___uiController_10;
	// System.DateTime VAEController::startTime
	DateTime_t693205669  ___startTime_11;
	// System.DateTime VAEController::endTime
	DateTime_t693205669  ___endTime_12;
	// System.Single VAEController::totalTimeRecorded
	float ___totalTimeRecorded_13;
	// System.Single VAEController::totalTimeSoundTemplate
	float ___totalTimeSoundTemplate_14;
	// UnityEngine.Events.UnityAction VAEController::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_isVR_2() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___isVR_2)); }
	inline bool get_isVR_2() const { return ___isVR_2; }
	inline bool* get_address_of_isVR_2() { return &___isVR_2; }
	inline void set_isVR_2(bool value)
	{
		___isVR_2 = value;
	}

	inline static int32_t get_offset_of_isChangingMode_3() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___isChangingMode_3)); }
	inline bool get_isChangingMode_3() const { return ___isChangingMode_3; }
	inline bool* get_address_of_isChangingMode_3() { return &___isChangingMode_3; }
	inline void set_isChangingMode_3(bool value)
	{
		___isChangingMode_3 = value;
	}

	inline static int32_t get_offset_of_typeChar_4() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___typeChar_4)); }
	inline String_t* get_typeChar_4() const { return ___typeChar_4; }
	inline String_t** get_address_of_typeChar_4() { return &___typeChar_4; }
	inline void set_typeChar_4(String_t* value)
	{
		___typeChar_4 = value;
		Il2CppCodeGenWriteBarrier(&___typeChar_4, value);
	}

	inline static int32_t get_offset_of_nameChar_5() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___nameChar_5)); }
	inline String_t* get_nameChar_5() const { return ___nameChar_5; }
	inline String_t** get_address_of_nameChar_5() { return &___nameChar_5; }
	inline void set_nameChar_5(String_t* value)
	{
		___nameChar_5 = value;
		Il2CppCodeGenWriteBarrier(&___nameChar_5, value);
	}

	inline static int32_t get_offset_of_VRchar_6() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___VRchar_6)); }
	inline String_t* get_VRchar_6() const { return ___VRchar_6; }
	inline String_t** get_address_of_VRchar_6() { return &___VRchar_6; }
	inline void set_VRchar_6(String_t* value)
	{
		___VRchar_6 = value;
		Il2CppCodeGenWriteBarrier(&___VRchar_6, value);
	}

	inline static int32_t get_offset_of_PracticeComand_7() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___PracticeComand_7)); }
	inline List_1_t3492939606 * get_PracticeComand_7() const { return ___PracticeComand_7; }
	inline List_1_t3492939606 ** get_address_of_PracticeComand_7() { return &___PracticeComand_7; }
	inline void set_PracticeComand_7(List_1_t3492939606 * value)
	{
		___PracticeComand_7 = value;
		Il2CppCodeGenWriteBarrier(&___PracticeComand_7, value);
	}

	inline static int32_t get_offset_of_currentIndex_8() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___currentIndex_8)); }
	inline int32_t get_currentIndex_8() const { return ___currentIndex_8; }
	inline int32_t* get_address_of_currentIndex_8() { return &___currentIndex_8; }
	inline void set_currentIndex_8(int32_t value)
	{
		___currentIndex_8 = value;
	}

	inline static int32_t get_offset_of_count_9() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___count_9)); }
	inline int32_t get_count_9() const { return ___count_9; }
	inline int32_t* get_address_of_count_9() { return &___count_9; }
	inline void set_count_9(int32_t value)
	{
		___count_9 = value;
	}

	inline static int32_t get_offset_of_uiController_10() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___uiController_10)); }
	inline UIController_t2029583246 * get_uiController_10() const { return ___uiController_10; }
	inline UIController_t2029583246 ** get_address_of_uiController_10() { return &___uiController_10; }
	inline void set_uiController_10(UIController_t2029583246 * value)
	{
		___uiController_10 = value;
		Il2CppCodeGenWriteBarrier(&___uiController_10, value);
	}

	inline static int32_t get_offset_of_startTime_11() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___startTime_11)); }
	inline DateTime_t693205669  get_startTime_11() const { return ___startTime_11; }
	inline DateTime_t693205669 * get_address_of_startTime_11() { return &___startTime_11; }
	inline void set_startTime_11(DateTime_t693205669  value)
	{
		___startTime_11 = value;
	}

	inline static int32_t get_offset_of_endTime_12() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___endTime_12)); }
	inline DateTime_t693205669  get_endTime_12() const { return ___endTime_12; }
	inline DateTime_t693205669 * get_address_of_endTime_12() { return &___endTime_12; }
	inline void set_endTime_12(DateTime_t693205669  value)
	{
		___endTime_12 = value;
	}

	inline static int32_t get_offset_of_totalTimeRecorded_13() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___totalTimeRecorded_13)); }
	inline float get_totalTimeRecorded_13() const { return ___totalTimeRecorded_13; }
	inline float* get_address_of_totalTimeRecorded_13() { return &___totalTimeRecorded_13; }
	inline void set_totalTimeRecorded_13(float value)
	{
		___totalTimeRecorded_13 = value;
	}

	inline static int32_t get_offset_of_totalTimeSoundTemplate_14() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___totalTimeSoundTemplate_14)); }
	inline float get_totalTimeSoundTemplate_14() const { return ___totalTimeSoundTemplate_14; }
	inline float* get_address_of_totalTimeSoundTemplate_14() { return &___totalTimeSoundTemplate_14; }
	inline void set_totalTimeSoundTemplate_14(float value)
	{
		___totalTimeSoundTemplate_14 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(VAEController_t2229688832_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
