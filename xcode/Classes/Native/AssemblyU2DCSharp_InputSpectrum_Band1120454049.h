﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputSpectrum/Band
struct  Band_t1120454049 
{
public:
	// System.Single InputSpectrum/Band::value
	float ___value_0;
	// System.Single InputSpectrum/Band::target
	float ___target_1;
	// UnityEngine.Transform[] InputSpectrum/Band::bars
	TransformU5BU5D_t3764228911* ___bars_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Band_t1120454049, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(Band_t1120454049, ___target_1)); }
	inline float get_target_1() const { return ___target_1; }
	inline float* get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(float value)
	{
		___target_1 = value;
	}

	inline static int32_t get_offset_of_bars_2() { return static_cast<int32_t>(offsetof(Band_t1120454049, ___bars_2)); }
	inline TransformU5BU5D_t3764228911* get_bars_2() const { return ___bars_2; }
	inline TransformU5BU5D_t3764228911** get_address_of_bars_2() { return &___bars_2; }
	inline void set_bars_2(TransformU5BU5D_t3764228911* value)
	{
		___bars_2 = value;
		Il2CppCodeGenWriteBarrier(&___bars_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of InputSpectrum/Band
struct Band_t1120454049_marshaled_pinvoke
{
	float ___value_0;
	float ___target_1;
	TransformU5BU5D_t3764228911* ___bars_2;
};
// Native definition for COM marshalling of InputSpectrum/Band
struct Band_t1120454049_marshaled_com
{
	float ___value_0;
	float ___target_1;
	TransformU5BU5D_t3764228911* ___bars_2;
};
