﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupHelperVR
struct PopupHelperVR_t3784706002;

#include "codegen/il2cpp-codegen.h"

// System.Void PopupHelperVR::.ctor()
extern "C"  void PopupHelperVR__ctor_m2402578211 (PopupHelperVR_t3784706002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupHelperVR::Open()
extern "C"  void PopupHelperVR_Open_m1128211479 (PopupHelperVR_t3784706002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupHelperVR::Close()
extern "C"  void PopupHelperVR_Close_m2214851629 (PopupHelperVR_t3784706002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
