﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChapterTopSetting[]
struct ChapterTopSettingU5BU5D_t3798922145;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterTopSettings
struct  ChapterTopSettings_t1968541953  : public ScriptableObject_t1975622470
{
public:
	// ChapterTopSetting[] ChapterTopSettings::settings
	ChapterTopSettingU5BU5D_t3798922145* ___settings_2;

public:
	inline static int32_t get_offset_of_settings_2() { return static_cast<int32_t>(offsetof(ChapterTopSettings_t1968541953, ___settings_2)); }
	inline ChapterTopSettingU5BU5D_t3798922145* get_settings_2() const { return ___settings_2; }
	inline ChapterTopSettingU5BU5D_t3798922145** get_address_of_settings_2() { return &___settings_2; }
	inline void set_settings_2(ChapterTopSettingU5BU5D_t3798922145* value)
	{
		___settings_2 = value;
		Il2CppCodeGenWriteBarrier(&___settings_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
