﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// com.beetsoft.GUIElements.SelectableItem
struct SelectableItem_t2941161729;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.UI.Button
struct Button_t2872111280;
// AnswerSound
struct AnswerSound_t4040477861;
// QuestionScenePopup
struct QuestionScenePopup_t792224618;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;

#include "AssemblyU2DCSharp_BaseQuestionView79922856.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VocabularyQuestionView
struct  VocabularyQuestionView_t1631345783  : public BaseQuestionView_t79922856
{
public:
	// com.beetsoft.GUIElements.SelectableItem VocabularyQuestionView::answer0
	SelectableItem_t2941161729 * ___answer0_3;
	// com.beetsoft.GUIElements.SelectableItem VocabularyQuestionView::answer1
	SelectableItem_t2941161729 * ___answer1_4;
	// com.beetsoft.GUIElements.SelectableItem VocabularyQuestionView::answer2
	SelectableItem_t2941161729 * ___answer2_5;
	// com.beetsoft.GUIElements.SelectableItem VocabularyQuestionView::answer3
	SelectableItem_t2941161729 * ___answer3_6;
	// UnityEngine.UI.Text VocabularyQuestionView::questionText
	Text_t356221433 * ___questionText_7;
	// UnityEngine.Animation VocabularyQuestionView::anim
	Animation_t2068071072 * ___anim_8;
	// UnityEngine.UI.Button VocabularyQuestionView::questionSoundButton
	Button_t2872111280 * ___questionSoundButton_9;
	// UnityEngine.UI.Button VocabularyQuestionView::summitButton
	Button_t2872111280 * ___summitButton_10;
	// AnswerSound VocabularyQuestionView::answerSound
	AnswerSound_t4040477861 * ___answerSound_11;
	// QuestionScenePopup VocabularyQuestionView::popup
	QuestionScenePopup_t792224618 * ___popup_12;
	// UnityEngine.AnimationClip VocabularyQuestionView::animResultOpen
	AnimationClip_t3510324950 * ___animResultOpen_13;
	// UnityEngine.AnimationClip VocabularyQuestionView::animResultClose
	AnimationClip_t3510324950 * ___animResultClose_14;

public:
	inline static int32_t get_offset_of_answer0_3() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___answer0_3)); }
	inline SelectableItem_t2941161729 * get_answer0_3() const { return ___answer0_3; }
	inline SelectableItem_t2941161729 ** get_address_of_answer0_3() { return &___answer0_3; }
	inline void set_answer0_3(SelectableItem_t2941161729 * value)
	{
		___answer0_3 = value;
		Il2CppCodeGenWriteBarrier(&___answer0_3, value);
	}

	inline static int32_t get_offset_of_answer1_4() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___answer1_4)); }
	inline SelectableItem_t2941161729 * get_answer1_4() const { return ___answer1_4; }
	inline SelectableItem_t2941161729 ** get_address_of_answer1_4() { return &___answer1_4; }
	inline void set_answer1_4(SelectableItem_t2941161729 * value)
	{
		___answer1_4 = value;
		Il2CppCodeGenWriteBarrier(&___answer1_4, value);
	}

	inline static int32_t get_offset_of_answer2_5() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___answer2_5)); }
	inline SelectableItem_t2941161729 * get_answer2_5() const { return ___answer2_5; }
	inline SelectableItem_t2941161729 ** get_address_of_answer2_5() { return &___answer2_5; }
	inline void set_answer2_5(SelectableItem_t2941161729 * value)
	{
		___answer2_5 = value;
		Il2CppCodeGenWriteBarrier(&___answer2_5, value);
	}

	inline static int32_t get_offset_of_answer3_6() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___answer3_6)); }
	inline SelectableItem_t2941161729 * get_answer3_6() const { return ___answer3_6; }
	inline SelectableItem_t2941161729 ** get_address_of_answer3_6() { return &___answer3_6; }
	inline void set_answer3_6(SelectableItem_t2941161729 * value)
	{
		___answer3_6 = value;
		Il2CppCodeGenWriteBarrier(&___answer3_6, value);
	}

	inline static int32_t get_offset_of_questionText_7() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___questionText_7)); }
	inline Text_t356221433 * get_questionText_7() const { return ___questionText_7; }
	inline Text_t356221433 ** get_address_of_questionText_7() { return &___questionText_7; }
	inline void set_questionText_7(Text_t356221433 * value)
	{
		___questionText_7 = value;
		Il2CppCodeGenWriteBarrier(&___questionText_7, value);
	}

	inline static int32_t get_offset_of_anim_8() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___anim_8)); }
	inline Animation_t2068071072 * get_anim_8() const { return ___anim_8; }
	inline Animation_t2068071072 ** get_address_of_anim_8() { return &___anim_8; }
	inline void set_anim_8(Animation_t2068071072 * value)
	{
		___anim_8 = value;
		Il2CppCodeGenWriteBarrier(&___anim_8, value);
	}

	inline static int32_t get_offset_of_questionSoundButton_9() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___questionSoundButton_9)); }
	inline Button_t2872111280 * get_questionSoundButton_9() const { return ___questionSoundButton_9; }
	inline Button_t2872111280 ** get_address_of_questionSoundButton_9() { return &___questionSoundButton_9; }
	inline void set_questionSoundButton_9(Button_t2872111280 * value)
	{
		___questionSoundButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___questionSoundButton_9, value);
	}

	inline static int32_t get_offset_of_summitButton_10() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___summitButton_10)); }
	inline Button_t2872111280 * get_summitButton_10() const { return ___summitButton_10; }
	inline Button_t2872111280 ** get_address_of_summitButton_10() { return &___summitButton_10; }
	inline void set_summitButton_10(Button_t2872111280 * value)
	{
		___summitButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___summitButton_10, value);
	}

	inline static int32_t get_offset_of_answerSound_11() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___answerSound_11)); }
	inline AnswerSound_t4040477861 * get_answerSound_11() const { return ___answerSound_11; }
	inline AnswerSound_t4040477861 ** get_address_of_answerSound_11() { return &___answerSound_11; }
	inline void set_answerSound_11(AnswerSound_t4040477861 * value)
	{
		___answerSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___answerSound_11, value);
	}

	inline static int32_t get_offset_of_popup_12() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___popup_12)); }
	inline QuestionScenePopup_t792224618 * get_popup_12() const { return ___popup_12; }
	inline QuestionScenePopup_t792224618 ** get_address_of_popup_12() { return &___popup_12; }
	inline void set_popup_12(QuestionScenePopup_t792224618 * value)
	{
		___popup_12 = value;
		Il2CppCodeGenWriteBarrier(&___popup_12, value);
	}

	inline static int32_t get_offset_of_animResultOpen_13() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___animResultOpen_13)); }
	inline AnimationClip_t3510324950 * get_animResultOpen_13() const { return ___animResultOpen_13; }
	inline AnimationClip_t3510324950 ** get_address_of_animResultOpen_13() { return &___animResultOpen_13; }
	inline void set_animResultOpen_13(AnimationClip_t3510324950 * value)
	{
		___animResultOpen_13 = value;
		Il2CppCodeGenWriteBarrier(&___animResultOpen_13, value);
	}

	inline static int32_t get_offset_of_animResultClose_14() { return static_cast<int32_t>(offsetof(VocabularyQuestionView_t1631345783, ___animResultClose_14)); }
	inline AnimationClip_t3510324950 * get_animResultClose_14() const { return ___animResultClose_14; }
	inline AnimationClip_t3510324950 ** get_address_of_animResultClose_14() { return &___animResultClose_14; }
	inline void set_animResultClose_14(AnimationClip_t3510324950 * value)
	{
		___animResultClose_14 = value;
		Il2CppCodeGenWriteBarrier(&___animResultClose_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
