﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManagerCharacters
struct ManagerCharacters_t162625631;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// CharacterScript
struct CharacterScript_t1308706256;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void ManagerCharacters::.ctor()
extern "C"  void ManagerCharacters__ctor_m1461816798 (ManagerCharacters_t162625631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ManagerCharacters::get_PathCharacter()
extern "C"  String_t* ManagerCharacters_get_PathCharacter_m2827390062 (ManagerCharacters_t162625631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerCharacters::set_PathCharacter(System.String)
extern "C"  void ManagerCharacters_set_PathCharacter_m4273670153 (ManagerCharacters_t162625631 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform ManagerCharacters::get_PrefabCharacter()
extern "C"  Transform_t3275118058 * ManagerCharacters_get_PrefabCharacter_m2898712810 (ManagerCharacters_t162625631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerCharacters::set_PrefabCharacter(UnityEngine.Transform)
extern "C"  void ManagerCharacters_set_PrefabCharacter_m4205107423 (ManagerCharacters_t162625631 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ManagerCharacters::get_SetCharacter()
extern "C"  String_t* ManagerCharacters_get_SetCharacter_m3954505709 (ManagerCharacters_t162625631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerCharacters::set_SetCharacter(System.String)
extern "C"  void ManagerCharacters_set_SetCharacter_m754250176 (ManagerCharacters_t162625631 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerCharacters::InstantiateNewCharacter(System.String)
extern "C"  void ManagerCharacters_InstantiateNewCharacter_m4157067087 (ManagerCharacters_t162625631 * __this, String_t* ___characterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Transform> ManagerCharacters::get_ListCharacter()
extern "C"  List_1_t2644239190 * ManagerCharacters_get_ListCharacter_m2233515448 (ManagerCharacters_t162625631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerCharacters::set_ListCharacter(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern "C"  void ManagerCharacters_set_ListCharacter_m2975846537 (ManagerCharacters_t162625631 * __this, List_1_t2644239190 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CharacterScript ManagerCharacters::GetCharacter(System.String)
extern "C"  CharacterScript_t1308706256 * ManagerCharacters_GetCharacter_m4247947002 (ManagerCharacters_t162625631 * __this, String_t* ___characterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerCharacters::CallCurrentCharacterTalking()
extern "C"  void ManagerCharacters_CallCurrentCharacterTalking_m55579768 (ManagerCharacters_t162625631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ManagerCharacters::ClearListTransform()
extern "C"  void ManagerCharacters_ClearListTransform_m2234796269 (ManagerCharacters_t162625631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
