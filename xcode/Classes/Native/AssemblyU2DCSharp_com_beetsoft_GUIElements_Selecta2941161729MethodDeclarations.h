﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.beetsoft.GUIElements.SelectableItem
struct SelectableItem_t2941161729;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void com.beetsoft.GUIElements.SelectableItem::.ctor()
extern "C"  void SelectableItem__ctor_m817762233 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean com.beetsoft.GUIElements.SelectableItem::get_selected()
extern "C"  bool SelectableItem_get_selected_m3980897801 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem::set_selected(System.Boolean)
extern "C"  void SelectableItem_set_selected_m2377985316 (SelectableItem_t2941161729 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem::SetItemText(System.String)
extern "C"  void SelectableItem_SetItemText_m3230758609 (SelectableItem_t2941161729 * __this, String_t* ___itemText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem::UpdateSelectedView()
extern "C"  void SelectableItem_UpdateSelectedView_m1928306780 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem::OnItemClick()
extern "C"  void SelectableItem_OnItemClick_m1568844583 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem::SetSelected()
extern "C"  void SelectableItem_SetSelected_m2219521452 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem::SetNoSelected()
extern "C"  void SelectableItem_SetNoSelected_m2800345519 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem::DisplayFrame()
extern "C"  void SelectableItem_DisplayFrame_m2342295042 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.GUIElements.SelectableItem::ResetFrame()
extern "C"  void SelectableItem_ResetFrame_m901928285 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator com.beetsoft.GUIElements.SelectableItem::ShowFrameAnimation()
extern "C"  Il2CppObject * SelectableItem_ShowFrameAnimation_m3975093939 (SelectableItem_t2941161729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
