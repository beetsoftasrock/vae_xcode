﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRConversationSelectionButtonView
struct VRConversationSelectionButtonView_t1568012346;
// ConversationSelectionData
struct ConversationSelectionData_t4090008535;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationSelectionData4090008535.h"

// System.Void VRConversationSelectionButtonView::.ctor()
extern "C"  void VRConversationSelectionButtonView__ctor_m2692192273 (VRConversationSelectionButtonView_t1568012346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRConversationSelectionButtonView::SetData(ConversationSelectionData)
extern "C"  void VRConversationSelectionButtonView_SetData_m1373318668 (VRConversationSelectionButtonView_t1568012346 * __this, ConversationSelectionData_t4090008535 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRConversationSelectionButtonView::OnClick()
extern "C"  void VRConversationSelectionButtonView_OnClick_m3455005630 (VRConversationSelectionButtonView_t1568012346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
