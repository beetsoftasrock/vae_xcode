﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EyeRaycaster
struct  EyeRaycaster_t3983099531  : public MonoBehaviour_t1158329972
{
public:
	// System.Single EyeRaycaster::timeCheckingStep
	float ___timeCheckingStep_2;
	// UnityEngine.Camera EyeRaycaster::raycastCamera
	Camera_t189460977 * ___raycastCamera_3;
	// System.Single EyeRaycaster::timeOut
	float ___timeOut_4;
	// System.Single EyeRaycaster::_currentTimeOut
	float ____currentTimeOut_5;
	// UnityEngine.Events.UnityEvent EyeRaycaster::onTimeOutEvent
	UnityEvent_t408735097 * ___onTimeOutEvent_6;

public:
	inline static int32_t get_offset_of_timeCheckingStep_2() { return static_cast<int32_t>(offsetof(EyeRaycaster_t3983099531, ___timeCheckingStep_2)); }
	inline float get_timeCheckingStep_2() const { return ___timeCheckingStep_2; }
	inline float* get_address_of_timeCheckingStep_2() { return &___timeCheckingStep_2; }
	inline void set_timeCheckingStep_2(float value)
	{
		___timeCheckingStep_2 = value;
	}

	inline static int32_t get_offset_of_raycastCamera_3() { return static_cast<int32_t>(offsetof(EyeRaycaster_t3983099531, ___raycastCamera_3)); }
	inline Camera_t189460977 * get_raycastCamera_3() const { return ___raycastCamera_3; }
	inline Camera_t189460977 ** get_address_of_raycastCamera_3() { return &___raycastCamera_3; }
	inline void set_raycastCamera_3(Camera_t189460977 * value)
	{
		___raycastCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___raycastCamera_3, value);
	}

	inline static int32_t get_offset_of_timeOut_4() { return static_cast<int32_t>(offsetof(EyeRaycaster_t3983099531, ___timeOut_4)); }
	inline float get_timeOut_4() const { return ___timeOut_4; }
	inline float* get_address_of_timeOut_4() { return &___timeOut_4; }
	inline void set_timeOut_4(float value)
	{
		___timeOut_4 = value;
	}

	inline static int32_t get_offset_of__currentTimeOut_5() { return static_cast<int32_t>(offsetof(EyeRaycaster_t3983099531, ____currentTimeOut_5)); }
	inline float get__currentTimeOut_5() const { return ____currentTimeOut_5; }
	inline float* get_address_of__currentTimeOut_5() { return &____currentTimeOut_5; }
	inline void set__currentTimeOut_5(float value)
	{
		____currentTimeOut_5 = value;
	}

	inline static int32_t get_offset_of_onTimeOutEvent_6() { return static_cast<int32_t>(offsetof(EyeRaycaster_t3983099531, ___onTimeOutEvent_6)); }
	inline UnityEvent_t408735097 * get_onTimeOutEvent_6() const { return ___onTimeOutEvent_6; }
	inline UnityEvent_t408735097 ** get_address_of_onTimeOutEvent_6() { return &___onTimeOutEvent_6; }
	inline void set_onTimeOutEvent_6(UnityEvent_t408735097 * value)
	{
		___onTimeOutEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___onTimeOutEvent_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
