﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.Action
struct Action_t3226471752;
// PlayOtherTalkDialog
struct PlayOtherTalkDialog_t78515058;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0
struct  U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481  : public Il2CppObject
{
public:
	// System.String PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::pathName
	String_t* ___pathName_0;
	// ConversationTalkData PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::talkData
	ConversationTalkData_t1570298305 * ___talkData_1;
	// System.Action PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::callback
	Action_t3226471752 * ___callback_2;
	// PlayOtherTalkDialog PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::$this
	PlayOtherTalkDialog_t78515058 * ___U24this_3;
	// System.Object PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_pathName_0() { return static_cast<int32_t>(offsetof(U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481, ___pathName_0)); }
	inline String_t* get_pathName_0() const { return ___pathName_0; }
	inline String_t** get_address_of_pathName_0() { return &___pathName_0; }
	inline void set_pathName_0(String_t* value)
	{
		___pathName_0 = value;
		Il2CppCodeGenWriteBarrier(&___pathName_0, value);
	}

	inline static int32_t get_offset_of_talkData_1() { return static_cast<int32_t>(offsetof(U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481, ___talkData_1)); }
	inline ConversationTalkData_t1570298305 * get_talkData_1() const { return ___talkData_1; }
	inline ConversationTalkData_t1570298305 ** get_address_of_talkData_1() { return &___talkData_1; }
	inline void set_talkData_1(ConversationTalkData_t1570298305 * value)
	{
		___talkData_1 = value;
		Il2CppCodeGenWriteBarrier(&___talkData_1, value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481, ___callback_2)); }
	inline Action_t3226471752 * get_callback_2() const { return ___callback_2; }
	inline Action_t3226471752 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_t3226471752 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481, ___U24this_3)); }
	inline PlayOtherTalkDialog_t78515058 * get_U24this_3() const { return ___U24this_3; }
	inline PlayOtherTalkDialog_t78515058 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayOtherTalkDialog_t78515058 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
