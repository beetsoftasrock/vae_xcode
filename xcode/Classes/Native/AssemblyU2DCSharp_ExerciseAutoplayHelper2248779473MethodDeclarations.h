﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExerciseAutoplayHelper
struct ExerciseAutoplayHelper_t2248779473;

#include "codegen/il2cpp-codegen.h"

// System.Void ExerciseAutoplayHelper::.ctor()
extern "C"  void ExerciseAutoplayHelper__ctor_m898850926 (ExerciseAutoplayHelper_t2248779473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseAutoplayHelper::Awake()
extern "C"  void ExerciseAutoplayHelper_Awake_m1528816145 (ExerciseAutoplayHelper_t2248779473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
