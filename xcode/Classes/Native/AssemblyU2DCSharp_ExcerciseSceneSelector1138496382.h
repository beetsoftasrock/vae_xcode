﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalConfig
struct GlobalConfig_t3080413471;
// ExcerciseSceneSelectionView[]
struct ExcerciseSceneSelectionViewU5BU5D_t2584062699;
// ExcerciseSceneSelectionView
struct ExcerciseSceneSelectionView_t3932267486;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExcerciseSceneSelector
struct  ExcerciseSceneSelector_t1138496382  : public MonoBehaviour_t1158329972
{
public:
	// GlobalConfig ExcerciseSceneSelector::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_2;
	// ExcerciseSceneSelectionView[] ExcerciseSceneSelector::selectionViews
	ExcerciseSceneSelectionViewU5BU5D_t2584062699* ___selectionViews_3;
	// System.Int32 ExcerciseSceneSelector::_currentSelectedId
	int32_t ____currentSelectedId_4;
	// ExcerciseSceneSelectionView ExcerciseSceneSelector::_currentSelected
	ExcerciseSceneSelectionView_t3932267486 * ____currentSelected_5;

public:
	inline static int32_t get_offset_of_globalConfig_2() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelector_t1138496382, ___globalConfig_2)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_2() const { return ___globalConfig_2; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_2() { return &___globalConfig_2; }
	inline void set_globalConfig_2(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_2 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_2, value);
	}

	inline static int32_t get_offset_of_selectionViews_3() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelector_t1138496382, ___selectionViews_3)); }
	inline ExcerciseSceneSelectionViewU5BU5D_t2584062699* get_selectionViews_3() const { return ___selectionViews_3; }
	inline ExcerciseSceneSelectionViewU5BU5D_t2584062699** get_address_of_selectionViews_3() { return &___selectionViews_3; }
	inline void set_selectionViews_3(ExcerciseSceneSelectionViewU5BU5D_t2584062699* value)
	{
		___selectionViews_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectionViews_3, value);
	}

	inline static int32_t get_offset_of__currentSelectedId_4() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelector_t1138496382, ____currentSelectedId_4)); }
	inline int32_t get__currentSelectedId_4() const { return ____currentSelectedId_4; }
	inline int32_t* get_address_of__currentSelectedId_4() { return &____currentSelectedId_4; }
	inline void set__currentSelectedId_4(int32_t value)
	{
		____currentSelectedId_4 = value;
	}

	inline static int32_t get_offset_of__currentSelected_5() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelector_t1138496382, ____currentSelected_5)); }
	inline ExcerciseSceneSelectionView_t3932267486 * get__currentSelected_5() const { return ____currentSelected_5; }
	inline ExcerciseSceneSelectionView_t3932267486 ** get_address_of__currentSelected_5() { return &____currentSelected_5; }
	inline void set__currentSelected_5(ExcerciseSceneSelectionView_t3932267486 * value)
	{
		____currentSelected_5 = value;
		Il2CppCodeGenWriteBarrier(&____currentSelected_5, value);
	}
};

struct ExcerciseSceneSelector_t1138496382_StaticFields
{
public:
	// System.Action ExcerciseSceneSelector::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(ExcerciseSceneSelector_t1138496382_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
