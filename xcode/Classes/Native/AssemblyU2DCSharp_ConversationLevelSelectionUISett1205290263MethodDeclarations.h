﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationLevelSelectionUISetting
struct ConversationLevelSelectionUISetting_t1205290263;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationLevelSelectionUISetting::.ctor()
extern "C"  void ConversationLevelSelectionUISetting__ctor_m3699789798 (ConversationLevelSelectionUISetting_t1205290263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLevelSelectionUISetting::Start()
extern "C"  void ConversationLevelSelectionUISetting_Start_m1821167730 (ConversationLevelSelectionUISetting_t1205290263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
