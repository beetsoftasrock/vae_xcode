﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterLoader/<loadAssetBundle>c__AnonStorey0
struct U3CloadAssetBundleU3Ec__AnonStorey0_t99180152;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterLoader/<loadAssetBundle>c__AnonStorey0::.ctor()
extern "C"  void U3CloadAssetBundleU3Ec__AnonStorey0__ctor_m1584839643 (U3CloadAssetBundleU3Ec__AnonStorey0_t99180152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterLoader/<loadAssetBundle>c__AnonStorey0::<>m__0()
extern "C"  void U3CloadAssetBundleU3Ec__AnonStorey0_U3CU3Em__0_m3407622486 (U3CloadAssetBundleU3Ec__AnonStorey0_t99180152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
