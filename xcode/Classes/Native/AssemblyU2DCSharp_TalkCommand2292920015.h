﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationTalkData
struct ConversationTalkData_t1570298305;

#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkCommand
struct  TalkCommand_t2292920015  : public ConversationCommand_t3660105836
{
public:
	// ConversationTalkData TalkCommand::_data
	ConversationTalkData_t1570298305 * ____data_0;
	// ConversationTalkData TalkCommand::_preTalkData
	ConversationTalkData_t1570298305 * ____preTalkData_1;
	// System.Int32 TalkCommand::_preCurrentPoint
	int32_t ____preCurrentPoint_2;

public:
	inline static int32_t get_offset_of__data_0() { return static_cast<int32_t>(offsetof(TalkCommand_t2292920015, ____data_0)); }
	inline ConversationTalkData_t1570298305 * get__data_0() const { return ____data_0; }
	inline ConversationTalkData_t1570298305 ** get_address_of__data_0() { return &____data_0; }
	inline void set__data_0(ConversationTalkData_t1570298305 * value)
	{
		____data_0 = value;
		Il2CppCodeGenWriteBarrier(&____data_0, value);
	}

	inline static int32_t get_offset_of__preTalkData_1() { return static_cast<int32_t>(offsetof(TalkCommand_t2292920015, ____preTalkData_1)); }
	inline ConversationTalkData_t1570298305 * get__preTalkData_1() const { return ____preTalkData_1; }
	inline ConversationTalkData_t1570298305 ** get_address_of__preTalkData_1() { return &____preTalkData_1; }
	inline void set__preTalkData_1(ConversationTalkData_t1570298305 * value)
	{
		____preTalkData_1 = value;
		Il2CppCodeGenWriteBarrier(&____preTalkData_1, value);
	}

	inline static int32_t get_offset_of__preCurrentPoint_2() { return static_cast<int32_t>(offsetof(TalkCommand_t2292920015, ____preCurrentPoint_2)); }
	inline int32_t get__preCurrentPoint_2() const { return ____preCurrentPoint_2; }
	inline int32_t* get_address_of__preCurrentPoint_2() { return &____preCurrentPoint_2; }
	inline void set__preCurrentPoint_2(int32_t value)
	{
		____preCurrentPoint_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
