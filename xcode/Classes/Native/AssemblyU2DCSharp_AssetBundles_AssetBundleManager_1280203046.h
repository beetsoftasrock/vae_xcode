﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager/<LoadScene>c__AnonStorey6
struct  U3CLoadSceneU3Ec__AnonStorey6_t1280203046  : public Il2CppObject
{
public:
	// System.String AssetBundles.AssetBundleManager/<LoadScene>c__AnonStorey6::sceneName
	String_t* ___sceneName_0;

public:
	inline static int32_t get_offset_of_sceneName_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__AnonStorey6_t1280203046, ___sceneName_0)); }
	inline String_t* get_sceneName_0() const { return ___sceneName_0; }
	inline String_t** get_address_of_sceneName_0() { return &___sceneName_0; }
	inline void set_sceneName_0(String_t* value)
	{
		___sceneName_0 = value;
		Il2CppCodeGenWriteBarrier(&___sceneName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
