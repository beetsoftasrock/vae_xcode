﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApiConnector
struct ApiConnector_t2569785041;
// IApiRequest
struct IApiRequest_t1151250174;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void ApiConnector::.ctor()
extern "C"  void ApiConnector__ctor_m1961162670 (ApiConnector_t2569785041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ApiConnector ApiConnector::get_instance()
extern "C"  ApiConnector_t2569785041 * ApiConnector_get_instance_m1688114584 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApiConnector::Awake()
extern "C"  void ApiConnector_Awake_m3847348401 (ApiConnector_t2569785041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApiConnector::RequestApi(IApiRequest)
extern "C"  void ApiConnector_RequestApi_m944542649 (ApiConnector_t2569785041 * __this, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ApiConnector::DoRequestApi(IApiRequest)
extern "C"  Il2CppObject * ApiConnector_DoRequestApi_m585534194 (ApiConnector_t2569785041 * __this, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApiConnector::HandleRequestError()
extern "C"  void ApiConnector_HandleRequestError_m2535937051 (ApiConnector_t2569785041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApiConnector::ShowPopup()
extern "C"  void ApiConnector_ShowPopup_m2833058029 (ApiConnector_t2569785041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApiConnector::ClosePopup()
extern "C"  void ApiConnector_ClosePopup_m96490474 (ApiConnector_t2569785041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApiConnector::Retry()
extern "C"  void ApiConnector_Retry_m1843890752 (ApiConnector_t2569785041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
