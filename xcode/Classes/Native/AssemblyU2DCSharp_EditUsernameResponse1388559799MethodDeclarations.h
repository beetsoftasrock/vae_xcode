﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditUsernameResponse
struct EditUsernameResponse_t1388559799;

#include "codegen/il2cpp-codegen.h"

// System.Void EditUsernameResponse::.ctor()
extern "C"  void EditUsernameResponse__ctor_m1958210918 (EditUsernameResponse_t1388559799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
