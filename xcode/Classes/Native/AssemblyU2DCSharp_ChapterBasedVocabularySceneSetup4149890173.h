﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VocabularyDataLoader
struct VocabularyDataLoader_t2031208153;
// VocalController
struct VocalController_t1499553201;

#include "AssemblyU2DCSharp_GlobalBasedSetupModule1655890765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterBasedVocabularySceneSetup
struct  ChapterBasedVocabularySceneSetup_t4149890173  : public GlobalBasedSetupModule_t1655890765
{
public:
	// VocabularyDataLoader ChapterBasedVocabularySceneSetup::dataLoader
	VocabularyDataLoader_t2031208153 * ___dataLoader_3;
	// VocalController ChapterBasedVocabularySceneSetup::vocabularyController
	VocalController_t1499553201 * ___vocabularyController_4;

public:
	inline static int32_t get_offset_of_dataLoader_3() { return static_cast<int32_t>(offsetof(ChapterBasedVocabularySceneSetup_t4149890173, ___dataLoader_3)); }
	inline VocabularyDataLoader_t2031208153 * get_dataLoader_3() const { return ___dataLoader_3; }
	inline VocabularyDataLoader_t2031208153 ** get_address_of_dataLoader_3() { return &___dataLoader_3; }
	inline void set_dataLoader_3(VocabularyDataLoader_t2031208153 * value)
	{
		___dataLoader_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_3, value);
	}

	inline static int32_t get_offset_of_vocabularyController_4() { return static_cast<int32_t>(offsetof(ChapterBasedVocabularySceneSetup_t4149890173, ___vocabularyController_4)); }
	inline VocalController_t1499553201 * get_vocabularyController_4() const { return ___vocabularyController_4; }
	inline VocalController_t1499553201 ** get_address_of_vocabularyController_4() { return &___vocabularyController_4; }
	inline void set_vocabularyController_4(VocalController_t1499553201 * value)
	{
		___vocabularyController_4 = value;
		Il2CppCodeGenWriteBarrier(&___vocabularyController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
