﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.Purchaser/<ProcessPurchase>c__AnonStorey2
struct U3CProcessPurchaseU3Ec__AnonStorey2_t2428326241;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"

// System.Void Beetsoft.VAE.Purchaser/<ProcessPurchase>c__AnonStorey2::.ctor()
extern "C"  void U3CProcessPurchaseU3Ec__AnonStorey2__ctor_m4123854888 (U3CProcessPurchaseU3Ec__AnonStorey2_t2428326241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.Purchaser/<ProcessPurchase>c__AnonStorey2::<>m__0(UnityEngine.Purchasing.Product)
extern "C"  bool U3CProcessPurchaseU3Ec__AnonStorey2_U3CU3Em__0_m1995782363 (U3CProcessPurchaseU3Ec__AnonStorey2_t2428326241 * __this, Product_t1203687971 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.Purchaser/<ProcessPurchase>c__AnonStorey2::<>m__1(Beetsoft.VAE.Purchaser/PurchaseSession)
extern "C"  bool U3CProcessPurchaseU3Ec__AnonStorey2_U3CU3Em__1_m3703614443 (U3CProcessPurchaseU3Ec__AnonStorey2_t2428326241 * __this, PurchaseSession_t2088054391  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
