﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CommandSheet/Param
struct Param_t4123818474;

#include "codegen/il2cpp-codegen.h"

// System.Void CommandSheet/Param::.ctor()
extern "C"  void Param__ctor_m2367805823 (Param_t4123818474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
