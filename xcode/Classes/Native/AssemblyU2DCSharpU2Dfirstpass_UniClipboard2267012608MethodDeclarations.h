﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniClipboard
struct UniClipboard_t2267012608;
// IBoard
struct IBoard_t1215419429;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UniClipboard::.ctor()
extern "C"  void UniClipboard__ctor_m4127140487 (UniClipboard_t2267012608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IBoard UniClipboard::get_board()
extern "C"  Il2CppObject * UniClipboard_get_board_m80537946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniClipboard::SetText(System.String)
extern "C"  void UniClipboard_SetText_m2485813056 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniClipboard::GetText()
extern "C"  String_t* UniClipboard_GetText_m10305417 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
