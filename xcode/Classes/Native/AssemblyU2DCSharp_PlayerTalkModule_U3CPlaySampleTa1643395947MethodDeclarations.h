﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1
struct U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::.ctor()
extern "C"  void U3CPlaySampleTalkEffectU3Ec__Iterator1__ctor_m1400428990 (U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::MoveNext()
extern "C"  bool U3CPlaySampleTalkEffectU3Ec__Iterator1_MoveNext_m2179319850 (U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlaySampleTalkEffectU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1370074332 (U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlaySampleTalkEffectU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3530197044 (U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::Dispose()
extern "C"  void U3CPlaySampleTalkEffectU3Ec__Iterator1_Dispose_m3249366277 (U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::Reset()
extern "C"  void U3CPlaySampleTalkEffectU3Ec__Iterator1_Reset_m2082448819 (U3CPlaySampleTalkEffectU3Ec__Iterator1_t1643395947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTalkModule/<PlaySampleTalkEffect>c__Iterator1::<>m__0()
extern "C"  void U3CPlaySampleTalkEffectU3Ec__Iterator1_U3CU3Em__0_m3656346305 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
