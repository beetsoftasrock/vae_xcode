﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence1SceneController
struct Sentence1SceneController_t2055584412;
// SentenceStructureIdiomOnPlayQuestion
struct SentenceStructureIdiomOnPlayQuestion_t936886987;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomOnPlayQuest936886987.h"

// System.Void Sentence1SceneController::.ctor()
extern "C"  void Sentence1SceneController__ctor_m1648995137 (Sentence1SceneController_t2055584412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SentenceStructureIdiomOnPlayQuestion Sentence1SceneController::get_currentQuestion()
extern "C"  SentenceStructureIdiomOnPlayQuestion_t936886987 * Sentence1SceneController_get_currentQuestion_m2716295709 (Sentence1SceneController_t2055584412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1SceneController::set_currentQuestion(SentenceStructureIdiomOnPlayQuestion)
extern "C"  void Sentence1SceneController_set_currentQuestion_m4278050090 (Sentence1SceneController_t2055584412 * __this, SentenceStructureIdiomOnPlayQuestion_t936886987 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Sentence1SceneController::Start()
extern "C"  Il2CppObject * Sentence1SceneController_Start_m484598403 (Sentence1SceneController_t2055584412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1SceneController::StartPlay()
extern "C"  void Sentence1SceneController_StartPlay_m1988764073 (Sentence1SceneController_t2055584412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1SceneController::NextQuestion()
extern "C"  void Sentence1SceneController_NextQuestion_m1234784842 (Sentence1SceneController_t2055584412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1SceneController::SaveData()
extern "C"  void Sentence1SceneController_SaveData_m3270234090 (Sentence1SceneController_t2055584412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1SceneController::InitData()
extern "C"  void Sentence1SceneController_InitData_m3746664191 (Sentence1SceneController_t2055584412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1SceneController::BackToChapterTop()
extern "C"  void Sentence1SceneController_BackToChapterTop_m848290183 (Sentence1SceneController_t2055584412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
