﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// ConversationController
struct ConversationController_t3486580231;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationController/<QuiteVRMode>c__AnonStorey0
struct  U3CQuiteVRModeU3Ec__AnonStorey0_t118348258  : public Il2CppObject
{
public:
	// UnityEngine.Transform ConversationController/<QuiteVRMode>c__AnonStorey0::popupQuit
	Transform_t3275118058 * ___popupQuit_0;
	// ConversationController ConversationController/<QuiteVRMode>c__AnonStorey0::$this
	ConversationController_t3486580231 * ___U24this_1;

public:
	inline static int32_t get_offset_of_popupQuit_0() { return static_cast<int32_t>(offsetof(U3CQuiteVRModeU3Ec__AnonStorey0_t118348258, ___popupQuit_0)); }
	inline Transform_t3275118058 * get_popupQuit_0() const { return ___popupQuit_0; }
	inline Transform_t3275118058 ** get_address_of_popupQuit_0() { return &___popupQuit_0; }
	inline void set_popupQuit_0(Transform_t3275118058 * value)
	{
		___popupQuit_0 = value;
		Il2CppCodeGenWriteBarrier(&___popupQuit_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CQuiteVRModeU3Ec__AnonStorey0_t118348258, ___U24this_1)); }
	inline ConversationController_t3486580231 * get_U24this_1() const { return ___U24this_1; }
	inline ConversationController_t3486580231 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ConversationController_t3486580231 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
