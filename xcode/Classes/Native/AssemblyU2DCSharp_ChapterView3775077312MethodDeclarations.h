﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterView
struct ChapterView_t3775077312;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterView::.ctor()
extern "C"  void ChapterView__ctor_m739363347 (ChapterView_t3775077312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
