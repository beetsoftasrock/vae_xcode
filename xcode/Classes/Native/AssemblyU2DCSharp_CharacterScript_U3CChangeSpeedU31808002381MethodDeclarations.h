﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterScript/<ChangeSpeed>c__Iterator0
struct U3CChangeSpeedU3Ec__Iterator0_t1808002381;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterScript/<ChangeSpeed>c__Iterator0::.ctor()
extern "C"  void U3CChangeSpeedU3Ec__Iterator0__ctor_m1333176754 (U3CChangeSpeedU3Ec__Iterator0_t1808002381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterScript/<ChangeSpeed>c__Iterator0::MoveNext()
extern "C"  bool U3CChangeSpeedU3Ec__Iterator0_MoveNext_m1367891022 (U3CChangeSpeedU3Ec__Iterator0_t1808002381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterScript/<ChangeSpeed>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CChangeSpeedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1652081896 (U3CChangeSpeedU3Ec__Iterator0_t1808002381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CharacterScript/<ChangeSpeed>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CChangeSpeedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2606897216 (U3CChangeSpeedU3Ec__Iterator0_t1808002381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript/<ChangeSpeed>c__Iterator0::Dispose()
extern "C"  void U3CChangeSpeedU3Ec__Iterator0_Dispose_m2174654479 (U3CChangeSpeedU3Ec__Iterator0_t1808002381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterScript/<ChangeSpeed>c__Iterator0::Reset()
extern "C"  void U3CChangeSpeedU3Ec__Iterator0_Reset_m1931886573 (U3CChangeSpeedU3Ec__Iterator0_t1808002381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
