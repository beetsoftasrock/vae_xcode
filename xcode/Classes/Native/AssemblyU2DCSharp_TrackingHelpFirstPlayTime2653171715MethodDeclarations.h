﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrackingHelpFirstPlayTime
struct TrackingHelpFirstPlayTime_t2653171715;

#include "codegen/il2cpp-codegen.h"

// System.Void TrackingHelpFirstPlayTime::.ctor()
extern "C"  void TrackingHelpFirstPlayTime__ctor_m526525682 (TrackingHelpFirstPlayTime_t2653171715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackingHelpFirstPlayTime::Awake()
extern "C"  void TrackingHelpFirstPlayTime_Awake_m2877725507 (TrackingHelpFirstPlayTime_t2653171715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackingHelpFirstPlayTime::Start()
extern "C"  void TrackingHelpFirstPlayTime_Start_m3935617694 (TrackingHelpFirstPlayTime_t2653171715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
