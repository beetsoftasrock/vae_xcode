﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// MiniJSON.Json/Parser
struct Parser_t1915358011;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// MiniJSON.Json/Serializer
struct Serializer_t4088787656;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.IList
struct IList_t3321498491;
// UnityEngine.Purchasing.UnityPurchasingCallback
struct UnityPurchasingCallback_t2635187846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Purchasing_Common_U3CModuleU3E3783534214.h"
#include "Purchasing_Common_U3CModuleU3E3783534214MethodDeclarations.h"
#include "Purchasing_Common_MiniJson1660571557.h"
#include "Purchasing_Common_MiniJson1660571557MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "Purchasing_Common_MiniJSON_Json4020371770MethodDeclarations.h"
#include "Purchasing_Common_MiniJSON_Json4020371770.h"
#include "Purchasing_Common_MiniJSON_Json_Parser1915358011MethodDeclarations.h"
#include "Purchasing_Common_MiniJSON_Json_Serializer4088787656MethodDeclarations.h"
#include "Purchasing_Common_MiniJSON_Json_Parser1915358011.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader1480123486MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader1480123486.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Char3454481338MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_IO_TextReader1561828458MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "Purchasing_Common_MiniJSON_Json_Parser_TOKEN2182318091.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Double4078015681.h"
#include "Purchasing_Common_MiniJSON_Json_Parser_TOKEN2182318091MethodDeclarations.h"
#include "Purchasing_Common_MiniJSON_Json_Serializer4088787656.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Decimal724701077.h"
#include "Purchasing_Common_UnityEngine_Purchasing_UnityPurc2635187846.h"
#include "Purchasing_Common_UnityEngine_Purchasing_UnityPurc2635187846MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String MiniJson::JsonEncode(System.Object)
extern "C"  String_t* MiniJson_JsonEncode_m3848870306 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___json0;
		String_t* L_1 = Json_Serialize_m1551079672(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Object MiniJson::JsonDecode(System.String)
extern "C"  Il2CppObject * MiniJson_JsonDecode_m2130610334 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___json0;
		Il2CppObject * L_1 = Json_Deserialize_m4275350799(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// System.Object MiniJSON.Json::Deserialize(System.String)
extern "C"  Il2CppObject * Json_Deserialize_m4275350799 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___json0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		V_0 = NULL;
		goto IL_001b;
	}

IL_000f:
	{
		String_t* L_1 = ___json0;
		Il2CppObject * L_2 = Parser_Parse_m807207857(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001b;
	}

IL_001b:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// System.String MiniJSON.Json::Serialize(System.Object)
extern "C"  String_t* Json_Serialize_m1551079672 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		String_t* L_1 = Serializer_Serialize_m2185524401(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Void MiniJSON.Json/Parser::.ctor(System.String)
extern Il2CppClass* StringReader_t1480123486_il2cpp_TypeInfo_var;
extern const uint32_t Parser__ctor_m682009913_MetadataUsageId;
extern "C"  void Parser__ctor_m682009913 (Parser_t1915358011 * __this, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser__ctor_m682009913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonString0;
		StringReader_t1480123486 * L_1 = (StringReader_t1480123486 *)il2cpp_codegen_object_new(StringReader_t1480123486_il2cpp_TypeInfo_var);
		StringReader__ctor_m643998729(L_1, L_0, /*hidden argument*/NULL);
		__this->set_json_0(L_1);
		return;
	}
}
// System.Boolean MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1102494086;
extern const uint32_t Parser_IsWordBreak_m4078466927_MetadataUsageId;
extern "C"  bool Parser_IsWordBreak_m4078466927 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_IsWordBreak_m4078466927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Il2CppChar L_0 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsWhiteSpace_m1507160293(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppChar L_2 = ___c0;
		NullCheck(_stringLiteral1102494086);
		int32_t L_3 = String_IndexOf_m2358239236(_stringLiteral1102494086, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 1;
	}

IL_0020:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Object MiniJSON.Json/Parser::Parse(System.String)
extern Il2CppClass* Parser_t1915358011_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Parser_Parse_m807207857_MetadataUsageId;
extern "C"  Il2CppObject * Parser_Parse_m807207857 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_Parse_m807207857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Parser_t1915358011 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___jsonString0;
		Parser_t1915358011 * L_1 = (Parser_t1915358011 *)il2cpp_codegen_object_new(Parser_t1915358011_il2cpp_TypeInfo_var);
		Parser__ctor_m682009913(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		Parser_t1915358011 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = Parser_ParseValue_m3842716222(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_LEAVE(0x22, FINALLY_0015);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0015;
	}

FINALLY_0015:
	{ // begin finally (depth: 1)
		{
			Parser_t1915358011 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_0021;
			}
		}

IL_001b:
		{
			Parser_t1915358011 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_5);
		}

IL_0021:
		{
			IL2CPP_END_FINALLY(21)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(21)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0022:
	{
		Il2CppObject * L_6 = V_1;
		return L_6;
	}
}
// System.Void MiniJSON.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m84374196 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	{
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		TextReader_Dispose_m4077464570(L_0, /*hidden argument*/NULL);
		__this->set_json_0((StringReader_t1480123486 *)NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> MiniJSON.Json/Parser::ParseObject()
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m103268765_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m3920648534_MethodInfo_var;
extern const uint32_t Parser_ParseObject_m2334269663_MetadataUsageId;
extern "C"  Dictionary_2_t309261261 * Parser_ParseObject_m2334269663 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseObject_m2334269663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	Dictionary_2_t309261261 * V_2 = NULL;
	String_t* V_3 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m103268765(L_0, /*hidden argument*/Dictionary_2__ctor_m103268765_MethodInfo_var);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_0();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_1);
	}

IL_0013:
	{
		int32_t L_2 = Parser_get_NextToken_m843563530(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		switch (L_3)
		{
			case 0:
			{
				goto IL_003a;
			}
			case 1:
			{
				goto IL_002e;
			}
			case 2:
			{
				goto IL_0046;
			}
		}
	}

IL_002e:
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)6)))
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_003a:
	{
		V_2 = (Dictionary_2_t309261261 *)NULL;
		goto IL_009a;
	}

IL_0041:
	{
		goto IL_0013;
	}

IL_0046:
	{
		Dictionary_2_t309261261 * L_5 = V_0;
		V_2 = L_5;
		goto IL_009a;
	}

IL_004d:
	{
		String_t* L_6 = Parser_ParseString_m1728962792(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		String_t* L_7 = V_3;
		if (L_7)
		{
			goto IL_0062;
		}
	}
	{
		V_2 = (Dictionary_2_t309261261 *)NULL;
		goto IL_009a;
	}

IL_0062:
	{
		int32_t L_8 = Parser_get_NextToken_m843563530(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)5)))
		{
			goto IL_0076;
		}
	}
	{
		V_2 = (Dictionary_2_t309261261 *)NULL;
		goto IL_009a;
	}

IL_0076:
	{
		StringReader_t1480123486 * L_9 = __this->get_json_0();
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_9);
		Dictionary_2_t309261261 * L_10 = V_0;
		String_t* L_11 = V_3;
		Il2CppObject * L_12 = Parser_ParseValue_m3842716222(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_set_Item_m3920648534(L_10, L_11, L_12, /*hidden argument*/Dictionary_2_set_Item_m3920648534_MethodInfo_var);
		goto IL_0094;
	}

IL_0094:
	{
		goto IL_0013;
	}

IL_009a:
	{
		Dictionary_2_t309261261 * L_13 = V_2;
		return L_13;
	}
}
// System.Collections.Generic.List`1<System.Object> MiniJSON.Json/Parser::ParseArray()
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4167524594_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2984365430_MethodInfo_var;
extern const uint32_t Parser_ParseArray_m3944609374_MetadataUsageId;
extern "C"  List_1_t2058570427 * Parser_ParseArray_m3944609374 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseArray_m3944609374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2058570427 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	List_1_t2058570427 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m4167524594(L_0, /*hidden argument*/List_1__ctor_m4167524594_MethodInfo_var);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_0();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		V_1 = (bool)1;
		goto IL_006b;
	}

IL_001a:
	{
		int32_t L_2 = Parser_get_NextToken_m843563530(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		switch (((int32_t)((int32_t)L_3-(int32_t)4)))
		{
			case 0:
			{
				goto IL_004d;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_0048;
			}
		}
	}

IL_0036:
	{
		int32_t L_4 = V_2;
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0054;
	}

IL_0041:
	{
		V_3 = (List_1_t2058570427 *)NULL;
		goto IL_0078;
	}

IL_0048:
	{
		goto IL_006b;
	}

IL_004d:
	{
		V_1 = (bool)0;
		goto IL_006a;
	}

IL_0054:
	{
		int32_t L_5 = V_2;
		Il2CppObject * L_6 = Parser_ParseByToken_m3194251126(__this, L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		List_1_t2058570427 * L_7 = V_0;
		Il2CppObject * L_8 = V_4;
		NullCheck(L_7);
		List_1_Add_m2984365430(L_7, L_8, /*hidden argument*/List_1_Add_m2984365430_MethodInfo_var);
		goto IL_006a;
	}

IL_006a:
	{
	}

IL_006b:
	{
		bool L_9 = V_1;
		if (L_9)
		{
			goto IL_001a;
		}
	}
	{
		List_1_t2058570427 * L_10 = V_0;
		V_3 = L_10;
		goto IL_0078;
	}

IL_0078:
	{
		List_1_t2058570427 * L_11 = V_3;
		return L_11;
	}
}
// System.Object MiniJSON.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m3842716222 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		int32_t L_0 = Parser_get_NextToken_m843563530(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		Il2CppObject * L_2 = Parser_ParseByToken_m3194251126(__this, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0015;
	}

IL_0015:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Object MiniJSON.Json/Parser::ParseByToken(MiniJSON.Json/Parser/TOKEN)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseByToken_m3194251126_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseByToken_m3194251126 (Parser_t1915358011 * __this, int32_t ___token0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseByToken_m3194251126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___token0;
		switch (((int32_t)((int32_t)L_0-(int32_t)7)))
		{
			case 0:
			{
				goto IL_0036;
			}
			case 1:
			{
				goto IL_0042;
			}
			case 2:
			{
				goto IL_0066;
			}
			case 3:
			{
				goto IL_0072;
			}
			case 4:
			{
				goto IL_007e;
			}
		}
	}
	{
		int32_t L_1 = ___token0;
		switch (((int32_t)((int32_t)L_1-(int32_t)1)))
		{
			case 0:
			{
				goto IL_004e;
			}
			case 1:
			{
				goto IL_0085;
			}
			case 2:
			{
				goto IL_005a;
			}
		}
	}
	{
		goto IL_0085;
	}

IL_0036:
	{
		String_t* L_2 = Parser_ParseString_m1728962792(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_008c;
	}

IL_0042:
	{
		Il2CppObject * L_3 = Parser_ParseNumber_m3382138710(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_008c;
	}

IL_004e:
	{
		Dictionary_2_t309261261 * L_4 = Parser_ParseObject_m2334269663(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_008c;
	}

IL_005a:
	{
		List_1_t2058570427 * L_5 = Parser_ParseArray_m3944609374(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_008c;
	}

IL_0066:
	{
		bool L_6 = ((bool)1);
		Il2CppObject * L_7 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_6);
		V_0 = L_7;
		goto IL_008c;
	}

IL_0072:
	{
		bool L_8 = ((bool)0);
		Il2CppObject * L_9 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_8);
		V_0 = L_9;
		goto IL_008c;
	}

IL_007e:
	{
		V_0 = NULL;
		goto IL_008c;
	}

IL_0085:
	{
		V_0 = NULL;
		goto IL_008c;
	}

IL_008c:
	{
		Il2CppObject * L_10 = V_0;
		return L_10;
	}
}
// System.String MiniJSON.Json/Parser::ParseString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseString_m1728962792_MetadataUsageId;
extern "C"  String_t* Parser_ParseString_m1728962792 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseString_m1728962792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	CharU5BU5D_t1328083999* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_0();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		V_2 = (bool)1;
		goto IL_016d;
	}

IL_001a:
	{
		StringReader_t1480123486 * L_2 = __this->get_json_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Peek() */, L_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0034;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0173;
	}

IL_0034:
	{
		Il2CppChar L_4 = Parser_get_NextChar_m2249446465(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		Il2CppChar L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)34))))
		{
			goto IL_0050;
		}
	}
	{
		Il2CppChar L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)92))))
		{
			goto IL_0057;
		}
	}
	{
		goto IL_015f;
	}

IL_0050:
	{
		V_2 = (bool)0;
		goto IL_016c;
	}

IL_0057:
	{
		StringReader_t1480123486 * L_7 = __this->get_json_0();
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Peek() */, L_7);
		if ((!(((uint32_t)L_8) == ((uint32_t)(-1)))))
		{
			goto IL_0070;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_016c;
	}

IL_0070:
	{
		Il2CppChar L_9 = Parser_get_NextChar_m2249446465(__this, /*hidden argument*/NULL);
		V_1 = L_9;
		Il2CppChar L_10 = V_1;
		switch (((int32_t)((int32_t)L_10-(int32_t)((int32_t)114))))
		{
			case 0:
			{
				goto IL_00fb;
			}
			case 1:
			{
				goto IL_0090;
			}
			case 2:
			{
				goto IL_0109;
			}
			case 3:
			{
				goto IL_0117;
			}
		}
	}

IL_0090:
	{
		Il2CppChar L_11 = V_1;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)34))))
		{
			goto IL_00c5;
		}
	}
	{
		Il2CppChar L_12 = V_1;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)47))))
		{
			goto IL_00c5;
		}
	}
	{
		Il2CppChar L_13 = V_1;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)92))))
		{
			goto IL_00c5;
		}
	}
	{
		Il2CppChar L_14 = V_1;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)98))))
		{
			goto IL_00d2;
		}
	}
	{
		Il2CppChar L_15 = V_1;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)102))))
		{
			goto IL_00df;
		}
	}
	{
		Il2CppChar L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)110))))
		{
			goto IL_00ed;
		}
	}
	{
		goto IL_015a;
	}

IL_00c5:
	{
		StringBuilder_t1221177846 * L_17 = V_0;
		Il2CppChar L_18 = V_1;
		NullCheck(L_17);
		StringBuilder_Append_m3618697540(L_17, L_18, /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_00d2:
	{
		StringBuilder_t1221177846 * L_19 = V_0;
		NullCheck(L_19);
		StringBuilder_Append_m3618697540(L_19, 8, /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_00df:
	{
		StringBuilder_t1221177846 * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m3618697540(L_20, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_00ed:
	{
		StringBuilder_t1221177846 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m3618697540(L_21, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_00fb:
	{
		StringBuilder_t1221177846 * L_22 = V_0;
		NullCheck(L_22);
		StringBuilder_Append_m3618697540(L_22, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_0109:
	{
		StringBuilder_t1221177846 * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m3618697540(L_23, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_0117:
	{
		V_3 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)4));
		V_4 = 0;
		goto IL_0138;
	}

IL_0126:
	{
		CharU5BU5D_t1328083999* L_24 = V_3;
		int32_t L_25 = V_4;
		Il2CppChar L_26 = Parser_get_NextChar_m2249446465(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (Il2CppChar)L_26);
		int32_t L_27 = V_4;
		V_4 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0138:
	{
		int32_t L_28 = V_4;
		if ((((int32_t)L_28) < ((int32_t)4)))
		{
			goto IL_0126;
		}
	}
	{
		StringBuilder_t1221177846 * L_29 = V_0;
		CharU5BU5D_t1328083999* L_30 = V_3;
		String_t* L_31 = String_CreateString_m3818307083(NULL, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_32 = Convert_ToInt32_m3262696010(NULL /*static, unused*/, L_31, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_29);
		StringBuilder_Append_m3618697540(L_29, (((int32_t)((uint16_t)L_32))), /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_015a:
	{
		goto IL_016c;
	}

IL_015f:
	{
		StringBuilder_t1221177846 * L_33 = V_0;
		Il2CppChar L_34 = V_1;
		NullCheck(L_33);
		StringBuilder_Append_m3618697540(L_33, L_34, /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_016c:
	{
	}

IL_016d:
	{
		bool L_35 = V_2;
		if (L_35)
		{
			goto IL_001a;
		}
	}

IL_0173:
	{
		StringBuilder_t1221177846 * L_36 = V_0;
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_36);
		V_5 = L_37;
		goto IL_0180;
	}

IL_0180:
	{
		String_t* L_38 = V_5;
		return L_38;
	}
}
// System.Object MiniJSON.Json/Parser::ParseNumber()
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseNumber_m3382138710_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseNumber_m3382138710 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseNumber_m3382138710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int64_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	double V_3 = 0.0;
	{
		String_t* L_0 = Parser_get_NextWord_m2870787424(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m2358239236(L_1, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_3 = V_0;
		Int64_TryParse_m948922810(NULL /*static, unused*/, L_3, (&V_1), /*hidden argument*/NULL);
		int64_t L_4 = V_1;
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_5);
		V_2 = L_6;
		goto IL_0041;
	}

IL_002c:
	{
		String_t* L_7 = V_0;
		Double_TryParse_m3252018994(NULL /*static, unused*/, L_7, (&V_3), /*hidden argument*/NULL);
		double L_8 = V_3;
		double L_9 = L_8;
		Il2CppObject * L_10 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_9);
		V_2 = L_10;
		goto IL_0041;
	}

IL_0041:
	{
		Il2CppObject * L_11 = V_2;
		return L_11;
	}
}
// System.Void MiniJSON.Json/Parser::EatWhitespace()
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Parser_EatWhitespace_m3744811138_MetadataUsageId;
extern "C"  void Parser_EatWhitespace_m3744811138 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_EatWhitespace_m3744811138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_002b;
	}

IL_0006:
	{
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_0);
		StringReader_t1480123486 * L_1 = __this->get_json_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Peek() */, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002a;
		}
	}
	{
		goto IL_003b;
	}

IL_002a:
	{
	}

IL_002b:
	{
		Il2CppChar L_3 = Parser_get_PeekChar_m1635695189(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_4 = Char_IsWhiteSpace_m1507160293(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0006;
		}
	}

IL_003b:
	{
		return;
	}
}
// System.Char MiniJSON.Json/Parser::get_PeekChar()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_PeekChar_m1635695189_MetadataUsageId;
extern "C"  Il2CppChar Parser_get_PeekChar_m1635695189 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_PeekChar_m1635695189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	{
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Peek() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_2 = Convert_ToChar_m3827339132(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppChar L_3 = V_0;
		return L_3;
	}
}
// System.Char MiniJSON.Json/Parser::get_NextChar()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_NextChar_m2249446465_MetadataUsageId;
extern "C"  Il2CppChar Parser_get_NextChar_m2249446465 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextChar_m2249446465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	{
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_2 = Convert_ToChar_m3827339132(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppChar L_3 = V_0;
		return L_3;
	}
}
// System.String MiniJSON.Json/Parser::get_NextWord()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_NextWord_m2870787424_MetadataUsageId;
extern "C"  String_t* Parser_get_NextWord_m2870787424 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextWord_m2870787424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0032;
	}

IL_000c:
	{
		StringBuilder_t1221177846 * L_1 = V_0;
		Il2CppChar L_2 = Parser_get_NextChar_m2249446465(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m3618697540(L_1, L_2, /*hidden argument*/NULL);
		StringReader_t1480123486 * L_3 = __this->get_json_0();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Peek() */, L_3);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		goto IL_0042;
	}

IL_0031:
	{
	}

IL_0032:
	{
		Il2CppChar L_5 = Parser_get_PeekChar_m1635695189(__this, /*hidden argument*/NULL);
		bool L_6 = Parser_IsWordBreak_m4078466927(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_000c;
		}
	}

IL_0042:
	{
		StringBuilder_t1221177846 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		V_1 = L_8;
		goto IL_004e;
	}

IL_004e:
	{
		String_t* L_9 = V_1;
		return L_9;
	}
}
// MiniJSON.Json/Parser/TOKEN MiniJSON.Json/Parser::get_NextToken()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t Parser_get_NextToken_m843563530_MetadataUsageId;
extern "C"  int32_t Parser_get_NextToken_m843563530 (Parser_t1915358011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextToken_m843563530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	String_t* V_2 = NULL;
	{
		Parser_EatWhitespace_m3744811138(__this, /*hidden argument*/NULL);
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Peek() */, L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0020;
		}
	}
	{
		V_0 = 0;
		goto IL_0160;
	}

IL_0020:
	{
		Il2CppChar L_2 = Parser_get_PeekChar_m1635695189(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		Il2CppChar L_3 = V_1;
		switch (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))))
		{
			case 0:
			{
				goto IL_00d7;
			}
			case 1:
			{
				goto IL_00f8;
			}
			case 2:
			{
				goto IL_006c;
			}
			case 3:
			{
				goto IL_006c;
			}
			case 4:
			{
				goto IL_00f8;
			}
			case 5:
			{
				goto IL_00f8;
			}
			case 6:
			{
				goto IL_00f8;
			}
			case 7:
			{
				goto IL_00f8;
			}
			case 8:
			{
				goto IL_00f8;
			}
			case 9:
			{
				goto IL_00f8;
			}
			case 10:
			{
				goto IL_00f8;
			}
			case 11:
			{
				goto IL_00f8;
			}
			case 12:
			{
				goto IL_00f8;
			}
			case 13:
			{
				goto IL_00f8;
			}
			case 14:
			{
				goto IL_00f1;
			}
		}
	}

IL_006c:
	{
		Il2CppChar L_4 = V_1;
		switch (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))))
		{
			case 0:
			{
				goto IL_00bd;
			}
			case 1:
			{
				goto IL_0081;
			}
			case 2:
			{
				goto IL_00c4;
			}
		}
	}

IL_0081:
	{
		Il2CppChar L_5 = V_1;
		switch (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))))
		{
			case 0:
			{
				goto IL_00a3;
			}
			case 1:
			{
				goto IL_0096;
			}
			case 2:
			{
				goto IL_00aa;
			}
		}
	}

IL_0096:
	{
		Il2CppChar L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)34))))
		{
			goto IL_00ea;
		}
	}
	{
		goto IL_00ff;
	}

IL_00a3:
	{
		V_0 = 1;
		goto IL_0160;
	}

IL_00aa:
	{
		StringReader_t1480123486 * L_7 = __this->get_json_0();
		NullCheck(L_7);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_7);
		V_0 = 2;
		goto IL_0160;
	}

IL_00bd:
	{
		V_0 = 3;
		goto IL_0160;
	}

IL_00c4:
	{
		StringReader_t1480123486 * L_8 = __this->get_json_0();
		NullCheck(L_8);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_8);
		V_0 = 4;
		goto IL_0160;
	}

IL_00d7:
	{
		StringReader_t1480123486 * L_9 = __this->get_json_0();
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_9);
		V_0 = 6;
		goto IL_0160;
	}

IL_00ea:
	{
		V_0 = 7;
		goto IL_0160;
	}

IL_00f1:
	{
		V_0 = 5;
		goto IL_0160;
	}

IL_00f8:
	{
		V_0 = 8;
		goto IL_0160;
	}

IL_00ff:
	{
		String_t* L_10 = Parser_get_NextWord_m2870787424(__this, /*hidden argument*/NULL);
		V_2 = L_10;
		String_t* L_11 = V_2;
		if (!L_11)
		{
			goto IL_0159;
		}
	}
	{
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, _stringLiteral2609877245, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0141;
		}
	}
	{
		String_t* L_14 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral3323263070, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0149;
		}
	}
	{
		String_t* L_16 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, _stringLiteral1743624307, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0151;
		}
	}
	{
		goto IL_0159;
	}

IL_0141:
	{
		V_0 = ((int32_t)10);
		goto IL_0160;
	}

IL_0149:
	{
		V_0 = ((int32_t)9);
		goto IL_0160;
	}

IL_0151:
	{
		V_0 = ((int32_t)11);
		goto IL_0160;
	}

IL_0159:
	{
		V_0 = 0;
		goto IL_0160;
	}

IL_0160:
	{
		int32_t L_18 = V_0;
		return L_18;
	}
}
// System.Void MiniJSON.Json/Serializer::.ctor()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t Serializer__ctor_m3134823312_MetadataUsageId;
extern "C"  void Serializer__ctor_m3134823312 (Serializer_t4088787656 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer__ctor_m3134823312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		__this->set_builder_0(L_0);
		return;
	}
}
// System.String MiniJSON.Json/Serializer::Serialize(System.Object)
extern Il2CppClass* Serializer_t4088787656_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_Serialize_m2185524401_MetadataUsageId;
extern "C"  String_t* Serializer_Serialize_m2185524401 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_Serialize_m2185524401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Serializer_t4088787656 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Serializer_t4088787656 * L_0 = (Serializer_t4088787656 *)il2cpp_codegen_object_new(Serializer_t4088787656_il2cpp_TypeInfo_var);
		Serializer__ctor_m3134823312(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Serializer_t4088787656 * L_1 = V_0;
		Il2CppObject * L_2 = ___obj0;
		NullCheck(L_1);
		Serializer_SerializeValue_m289531367(L_1, L_2, /*hidden argument*/NULL);
		Serializer_t4088787656 * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_t1221177846 * L_4 = L_3->get_builder_0();
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_1 = L_5;
		goto IL_001f;
	}

IL_001f:
	{
		String_t* L_6 = V_1;
		return L_6;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeValue(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern const uint32_t Serializer_SerializeValue_m289531367_MetadataUsageId;
extern "C"  void Serializer_SerializeValue_m289531367 (Serializer_t4088787656 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeValue_m289531367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	StringBuilder_t1221177846 * G_B7_0 = NULL;
	StringBuilder_t1221177846 * G_B6_0 = NULL;
	String_t* G_B8_0 = NULL;
	StringBuilder_t1221177846 * G_B8_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		StringBuilder_t1221177846 * L_1 = __this->get_builder_0();
		NullCheck(L_1);
		StringBuilder_Append_m3636508479(L_1, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_001f:
	{
		Il2CppObject * L_2 = ___value0;
		String_t* L_3 = ((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var));
		V_2 = L_3;
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_4 = V_2;
		Serializer_SerializeString_m319412033(__this, L_4, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_003a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_5, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_0072;
		}
	}
	{
		StringBuilder_t1221177846 * L_6 = __this->get_builder_0();
		Il2CppObject * L_7 = ___value0;
		G_B6_0 = L_6;
		if (!((*(bool*)((bool*)UnBox (L_7, Boolean_t3825574718_il2cpp_TypeInfo_var)))))
		{
			G_B7_0 = L_6;
			goto IL_0061;
		}
	}
	{
		G_B8_0 = _stringLiteral3323263070;
		G_B8_1 = G_B6_0;
		goto IL_0066;
	}

IL_0061:
	{
		G_B8_0 = _stringLiteral2609877245;
		G_B8_1 = G_B7_0;
	}

IL_0066:
	{
		NullCheck(G_B8_1);
		StringBuilder_Append_m3636508479(G_B8_1, G_B8_0, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_0072:
	{
		Il2CppObject * L_8 = ___value0;
		Il2CppObject * L_9 = ((Il2CppObject *)IsInst(L_8, IList_t3321498491_il2cpp_TypeInfo_var));
		V_0 = L_9;
		if (!L_9)
		{
			goto IL_008d;
		}
	}
	{
		Il2CppObject * L_10 = V_0;
		Serializer_SerializeArray_m3333011920(__this, L_10, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_008d:
	{
		Il2CppObject * L_11 = ___value0;
		Il2CppObject * L_12 = ((Il2CppObject *)IsInst(L_11, IDictionary_t596158605_il2cpp_TypeInfo_var));
		V_1 = L_12;
		if (!L_12)
		{
			goto IL_00a8;
		}
	}
	{
		Il2CppObject * L_13 = V_1;
		Serializer_SerializeObject_m3749421416(__this, L_13, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_00a8:
	{
		Il2CppObject * L_14 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_14, Char_t3454481338_il2cpp_TypeInfo_var)))
		{
			goto IL_00cc;
		}
	}
	{
		Il2CppObject * L_15 = ___value0;
		String_t* L_16 = String_CreateString_m2556700934(NULL, ((*(Il2CppChar*)((Il2CppChar*)UnBox (L_15, Char_t3454481338_il2cpp_TypeInfo_var)))), 1, /*hidden argument*/NULL);
		Serializer_SerializeString_m319412033(__this, L_16, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_00cc:
	{
		Il2CppObject * L_17 = ___value0;
		Serializer_SerializeOther_m2278864098(__this, L_17, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		return;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeObject_m3749421416_MetadataUsageId;
extern "C"  void Serializer_SerializeObject_m3749421416 (Serializer_t4088787656 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeObject_m3749421416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)123), /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006b;
		}

IL_0023:
		{
			Il2CppObject * L_4 = V_2;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			bool L_6 = V_0;
			if (L_6)
			{
				goto IL_0041;
			}
		}

IL_0031:
		{
			StringBuilder_t1221177846 * L_7 = __this->get_builder_0();
			NullCheck(L_7);
			StringBuilder_Append_m3618697540(L_7, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0041:
		{
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
			Serializer_SerializeString_m319412033(__this, L_9, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_10 = __this->get_builder_0();
			NullCheck(L_10);
			StringBuilder_Append_m3618697540(L_10, ((int32_t)58), /*hidden argument*/NULL);
			Il2CppObject * L_11 = ___obj0;
			Il2CppObject * L_12 = V_1;
			NullCheck(L_11);
			Il2CppObject * L_13 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_11, L_12);
			Serializer_SerializeValue_m289531367(__this, L_13, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_006b:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0023;
			}
		}

IL_0076:
		{
			IL2CPP_LEAVE(0x8F, FINALLY_007b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007b;
	}

FINALLY_007b:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_2;
			Il2CppObject * L_17 = ((Il2CppObject *)IsInst(L_16, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_3 = L_17;
			if (!L_17)
			{
				goto IL_008e;
			}
		}

IL_0088:
		{
			Il2CppObject * L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_18);
		}

IL_008e:
		{
			IL2CPP_END_FINALLY(123)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(123)
	{
		IL2CPP_JUMP_TBL(0x8F, IL_008f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008f:
	{
		StringBuilder_t1221177846 * L_19 = __this->get_builder_0();
		NullCheck(L_19);
		StringBuilder_Append_m3618697540(L_19, ((int32_t)125), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeArray_m3333011920_MetadataUsageId;
extern "C"  void Serializer_SerializeArray_m3333011920 (Serializer_t4088787656 * __this, Il2CppObject * ___anArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeArray_m3333011920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)91), /*hidden argument*/NULL);
		V_0 = (bool)1;
		Il2CppObject * L_1 = ___anArray0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0046;
		}

IL_001e:
		{
			Il2CppObject * L_3 = V_2;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_003c;
			}
		}

IL_002c:
		{
			StringBuilder_t1221177846 * L_6 = __this->get_builder_0();
			NullCheck(L_6);
			StringBuilder_Append_m3618697540(L_6, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_003c:
		{
			Il2CppObject * L_7 = V_1;
			Serializer_SerializeValue_m289531367(__this, L_7, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_0046:
		{
			Il2CppObject * L_8 = V_2;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_001e;
			}
		}

IL_0051:
		{
			IL2CPP_LEAVE(0x6A, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_10 = V_2;
			Il2CppObject * L_11 = ((Il2CppObject *)IsInst(L_10, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_3 = L_11;
			if (!L_11)
			{
				goto IL_0069;
			}
		}

IL_0063:
		{
			Il2CppObject * L_12 = V_3;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_12);
		}

IL_0069:
		{
			IL2CPP_END_FINALLY(86)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006a:
	{
		StringBuilder_t1221177846 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m3618697540(L_13, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeString(System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3943473468;
extern Il2CppCodeGenString* _stringLiteral2088416310;
extern Il2CppCodeGenString* _stringLiteral1093630588;
extern Il2CppCodeGenString* _stringLiteral3419229416;
extern Il2CppCodeGenString* _stringLiteral3062999056;
extern Il2CppCodeGenString* _stringLiteral381169868;
extern Il2CppCodeGenString* _stringLiteral3869568110;
extern Il2CppCodeGenString* _stringLiteral2303484169;
extern Il2CppCodeGenString* _stringLiteral2424443666;
extern const uint32_t Serializer_SerializeString_m319412033_MetadataUsageId;
extern "C"  void Serializer_SerializeString_m319412033 (Serializer_t4088787656 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeString_m319412033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t1328083999* V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	CharU5BU5D_t1328083999* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)34), /*hidden argument*/NULL);
		String_t* L_1 = ___str0;
		NullCheck(L_1);
		CharU5BU5D_t1328083999* L_2 = String_ToCharArray_m870309954(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		CharU5BU5D_t1328083999* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0157;
	}

IL_0020:
	{
		CharU5BU5D_t1328083999* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		Il2CppChar L_8 = V_1;
		switch (((int32_t)((int32_t)L_8-(int32_t)8)))
		{
			case 0:
			{
				goto IL_0086;
			}
			case 1:
			{
				goto IL_00de;
			}
			case 2:
			{
				goto IL_00b2;
			}
			case 3:
			{
				goto IL_0045;
			}
			case 4:
			{
				goto IL_009c;
			}
			case 5:
			{
				goto IL_00c8;
			}
		}
	}

IL_0045:
	{
		Il2CppChar L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)34))))
		{
			goto IL_005a;
		}
	}
	{
		Il2CppChar L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_0070;
		}
	}
	{
		goto IL_00f4;
	}

IL_005a:
	{
		StringBuilder_t1221177846 * L_11 = __this->get_builder_0();
		NullCheck(L_11);
		StringBuilder_Append_m3636508479(L_11, _stringLiteral3943473468, /*hidden argument*/NULL);
		goto IL_0152;
	}

IL_0070:
	{
		StringBuilder_t1221177846 * L_12 = __this->get_builder_0();
		NullCheck(L_12);
		StringBuilder_Append_m3636508479(L_12, _stringLiteral2088416310, /*hidden argument*/NULL);
		goto IL_0152;
	}

IL_0086:
	{
		StringBuilder_t1221177846 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m3636508479(L_13, _stringLiteral1093630588, /*hidden argument*/NULL);
		goto IL_0152;
	}

IL_009c:
	{
		StringBuilder_t1221177846 * L_14 = __this->get_builder_0();
		NullCheck(L_14);
		StringBuilder_Append_m3636508479(L_14, _stringLiteral3419229416, /*hidden argument*/NULL);
		goto IL_0152;
	}

IL_00b2:
	{
		StringBuilder_t1221177846 * L_15 = __this->get_builder_0();
		NullCheck(L_15);
		StringBuilder_Append_m3636508479(L_15, _stringLiteral3062999056, /*hidden argument*/NULL);
		goto IL_0152;
	}

IL_00c8:
	{
		StringBuilder_t1221177846 * L_16 = __this->get_builder_0();
		NullCheck(L_16);
		StringBuilder_Append_m3636508479(L_16, _stringLiteral381169868, /*hidden argument*/NULL);
		goto IL_0152;
	}

IL_00de:
	{
		StringBuilder_t1221177846 * L_17 = __this->get_builder_0();
		NullCheck(L_17);
		StringBuilder_Append_m3636508479(L_17, _stringLiteral3869568110, /*hidden argument*/NULL);
		goto IL_0152;
	}

IL_00f4:
	{
		Il2CppChar L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_19 = Convert_ToInt32_m3683486440(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)32))))
		{
			goto IL_0122;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)126))))
		{
			goto IL_0122;
		}
	}
	{
		StringBuilder_t1221177846 * L_22 = __this->get_builder_0();
		Il2CppChar L_23 = V_1;
		NullCheck(L_22);
		StringBuilder_Append_m3618697540(L_22, L_23, /*hidden argument*/NULL);
		goto IL_014d;
	}

IL_0122:
	{
		StringBuilder_t1221177846 * L_24 = __this->get_builder_0();
		NullCheck(L_24);
		StringBuilder_Append_m3636508479(L_24, _stringLiteral2303484169, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_25 = __this->get_builder_0();
		String_t* L_26 = Int32_ToString_m1064459878((&V_4), _stringLiteral2424443666, /*hidden argument*/NULL);
		NullCheck(L_25);
		StringBuilder_Append_m3636508479(L_25, L_26, /*hidden argument*/NULL);
	}

IL_014d:
	{
		goto IL_0152;
	}

IL_0152:
	{
		int32_t L_27 = V_3;
		V_3 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0157:
	{
		int32_t L_28 = V_3;
		CharU5BU5D_t1328083999* L_29 = V_2;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		StringBuilder_t1221177846 * L_30 = __this->get_builder_0();
		NullCheck(L_30);
		StringBuilder_Append_m3618697540(L_30, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeOther(System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029424;
extern const uint32_t Serializer_SerializeOther_m2278864098_MetadataUsageId;
extern "C"  void Serializer_SerializeOther_m2278864098 (Serializer_t4088787656 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeOther_m2278864098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	double V_1 = 0.0;
	{
		Il2CppObject * L_0 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_0, Single_t2076509932_il2cpp_TypeInfo_var)))
		{
			goto IL_0032;
		}
	}
	{
		StringBuilder_t1221177846 * L_1 = __this->get_builder_0();
		Il2CppObject * L_2 = ___value0;
		V_0 = ((*(float*)((float*)UnBox (L_2, Single_t2076509932_il2cpp_TypeInfo_var))));
		String_t* L_3 = Single_ToString_m2359963436((&V_0), _stringLiteral372029424, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m3636508479(L_1, L_3, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_0032:
	{
		Il2CppObject * L_4 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_4, Int32_t2071877448_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_5 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_5, UInt32_t2149682021_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_6 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_6, Int64_t909078037_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_7 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_7, SByte_t454417549_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_8 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_8, Byte_t3683104436_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_9 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_9, Int16_t4041245914_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_10 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_10, UInt16_t986882611_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_11 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_11, UInt64_t2909196914_il2cpp_TypeInfo_var)))
		{
			goto IL_009e;
		}
	}

IL_008a:
	{
		StringBuilder_t1221177846 * L_12 = __this->get_builder_0();
		Il2CppObject * L_13 = ___value0;
		NullCheck(L_12);
		StringBuilder_Append_m3541816491(L_12, L_13, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_009e:
	{
		Il2CppObject * L_14 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_14, Double_t4078015681_il2cpp_TypeInfo_var)))
		{
			goto IL_00b4;
		}
	}
	{
		Il2CppObject * L_15 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_15, Decimal_t724701077_il2cpp_TypeInfo_var)))
		{
			goto IL_00da;
		}
	}

IL_00b4:
	{
		StringBuilder_t1221177846 * L_16 = __this->get_builder_0();
		Il2CppObject * L_17 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_18 = Convert_ToDouble_m3751930225(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		String_t* L_19 = Double_ToString_m2210043919((&V_1), _stringLiteral372029424, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_Append_m3636508479(L_16, L_19, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_00da:
	{
		Il2CppObject * L_20 = ___value0;
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		Serializer_SerializeString_m319412033(__this, L_21, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityPurchasingCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityPurchasingCallback__ctor_m905612093 (UnityPurchasingCallback_t2635187846 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Purchasing.UnityPurchasingCallback::Invoke(System.String,System.String,System.String,System.String)
extern "C"  void UnityPurchasingCallback_Invoke_m3954350125 (UnityPurchasingCallback_t2635187846 * __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityPurchasingCallback_Invoke_m3954350125((UnityPurchasingCallback_t2635187846 *)__this->get_prev_9(),___subject0, ___payload1, ___receipt2, ___transactionId3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___subject0, ___payload1, ___receipt2, ___transactionId3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___subject0, ___payload1, ___receipt2, ___transactionId3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___subject0, ___payload1, ___receipt2, ___transactionId3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityPurchasingCallback_t2635187846 (UnityPurchasingCallback_t2635187846 * __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___subject0' to native representation
	char* ____subject0_marshaled = NULL;
	____subject0_marshaled = il2cpp_codegen_marshal_string(___subject0);

	// Marshaling of parameter '___payload1' to native representation
	char* ____payload1_marshaled = NULL;
	____payload1_marshaled = il2cpp_codegen_marshal_string(___payload1);

	// Marshaling of parameter '___receipt2' to native representation
	char* ____receipt2_marshaled = NULL;
	____receipt2_marshaled = il2cpp_codegen_marshal_string(___receipt2);

	// Marshaling of parameter '___transactionId3' to native representation
	char* ____transactionId3_marshaled = NULL;
	____transactionId3_marshaled = il2cpp_codegen_marshal_string(___transactionId3);

	// Native function invocation
	il2cppPInvokeFunc(____subject0_marshaled, ____payload1_marshaled, ____receipt2_marshaled, ____transactionId3_marshaled);

	// Marshaling cleanup of parameter '___subject0' native representation
	il2cpp_codegen_marshal_free(____subject0_marshaled);
	____subject0_marshaled = NULL;

	// Marshaling cleanup of parameter '___payload1' native representation
	il2cpp_codegen_marshal_free(____payload1_marshaled);
	____payload1_marshaled = NULL;

	// Marshaling cleanup of parameter '___receipt2' native representation
	il2cpp_codegen_marshal_free(____receipt2_marshaled);
	____receipt2_marshaled = NULL;

	// Marshaling cleanup of parameter '___transactionId3' native representation
	il2cpp_codegen_marshal_free(____transactionId3_marshaled);
	____transactionId3_marshaled = NULL;

}
// System.IAsyncResult UnityEngine.Purchasing.UnityPurchasingCallback::BeginInvoke(System.String,System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityPurchasingCallback_BeginInvoke_m3821805568 (UnityPurchasingCallback_t2635187846 * __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___subject0;
	__d_args[1] = ___payload1;
	__d_args[2] = ___receipt2;
	__d_args[3] = ___transactionId3;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.Purchasing.UnityPurchasingCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnityPurchasingCallback_EndInvoke_m1856131231 (UnityPurchasingCallback_t2635187846 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
