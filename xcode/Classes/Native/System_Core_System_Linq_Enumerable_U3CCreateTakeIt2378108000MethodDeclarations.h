﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>
struct U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1158628263;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>::.ctor()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m675042433_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m675042433(__this, method) ((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m675042433_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  uint8_t U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m727050292_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m727050292(__this, method) ((  uint8_t (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m727050292_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m2895943659_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m2895943659(__this, method) ((  Il2CppObject * (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m2895943659_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m440532020_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m440532020(__this, method) ((  Il2CppObject * (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m440532020_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1906231717_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1906231717(__this, method) ((  Il2CppObject* (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1906231717_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>::MoveNext()
extern "C"  bool U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1386538591_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1386538591(__this, method) ((  bool (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m1386538591_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>::Dispose()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m2444870820_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m2444870820(__this, method) ((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m2444870820_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Byte>::Reset()
extern "C"  void U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m2976316566_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m2976316566(__this, method) ((  void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2378108000 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_Reset_m2976316566_gshared)(__this, method)
