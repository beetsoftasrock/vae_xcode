﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// AnswerSound
struct AnswerSound_t4040477861;
// VocabularyDataLoader
struct VocabularyDataLoader_t2031208153;
// System.Collections.Generic.List`1<VocabularyOnplayQuestion>
struct List_1_t2888346175;
// System.Collections.Generic.List`1<VocabularyQuestionData>
struct List_1_t1390196108;
// VocabularyQuestionView
struct VocabularyQuestionView_t1631345783;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// VocabularyOnplayQuestion
struct VocabularyOnplayQuestion_t3519225043;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VocalController
struct  VocalController_t1499553201  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text VocalController::questionBundleText
	Text_t356221433 * ___questionBundleText_2;
	// AnswerSound VocalController::answerSound
	AnswerSound_t4040477861 * ___answerSound_3;
	// VocabularyDataLoader VocalController::dataLoader
	VocabularyDataLoader_t2031208153 * ___dataLoader_4;
	// System.Collections.Generic.List`1<VocabularyOnplayQuestion> VocalController::onPlayQuestions
	List_1_t2888346175 * ___onPlayQuestions_5;
	// System.Collections.Generic.List`1<VocabularyQuestionData> VocalController::questionDatas
	List_1_t1390196108 * ___questionDatas_6;
	// VocabularyQuestionView VocalController::questionView
	VocabularyQuestionView_t1631345783 * ___questionView_7;
	// UnityEngine.GameObject VocalController::resultGo
	GameObject_t1756533147 * ___resultGo_8;
	// System.Int32 VocalController::completedQuestion
	int32_t ___completedQuestion_9;
	// System.String VocalController::soundPath
	String_t* ___soundPath_10;
	// VocabularyOnplayQuestion VocalController::_currentQuestion
	VocabularyOnplayQuestion_t3519225043 * ____currentQuestion_11;
	// System.DateTime VocalController::startTime
	DateTime_t693205669  ___startTime_12;

public:
	inline static int32_t get_offset_of_questionBundleText_2() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___questionBundleText_2)); }
	inline Text_t356221433 * get_questionBundleText_2() const { return ___questionBundleText_2; }
	inline Text_t356221433 ** get_address_of_questionBundleText_2() { return &___questionBundleText_2; }
	inline void set_questionBundleText_2(Text_t356221433 * value)
	{
		___questionBundleText_2 = value;
		Il2CppCodeGenWriteBarrier(&___questionBundleText_2, value);
	}

	inline static int32_t get_offset_of_answerSound_3() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___answerSound_3)); }
	inline AnswerSound_t4040477861 * get_answerSound_3() const { return ___answerSound_3; }
	inline AnswerSound_t4040477861 ** get_address_of_answerSound_3() { return &___answerSound_3; }
	inline void set_answerSound_3(AnswerSound_t4040477861 * value)
	{
		___answerSound_3 = value;
		Il2CppCodeGenWriteBarrier(&___answerSound_3, value);
	}

	inline static int32_t get_offset_of_dataLoader_4() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___dataLoader_4)); }
	inline VocabularyDataLoader_t2031208153 * get_dataLoader_4() const { return ___dataLoader_4; }
	inline VocabularyDataLoader_t2031208153 ** get_address_of_dataLoader_4() { return &___dataLoader_4; }
	inline void set_dataLoader_4(VocabularyDataLoader_t2031208153 * value)
	{
		___dataLoader_4 = value;
		Il2CppCodeGenWriteBarrier(&___dataLoader_4, value);
	}

	inline static int32_t get_offset_of_onPlayQuestions_5() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___onPlayQuestions_5)); }
	inline List_1_t2888346175 * get_onPlayQuestions_5() const { return ___onPlayQuestions_5; }
	inline List_1_t2888346175 ** get_address_of_onPlayQuestions_5() { return &___onPlayQuestions_5; }
	inline void set_onPlayQuestions_5(List_1_t2888346175 * value)
	{
		___onPlayQuestions_5 = value;
		Il2CppCodeGenWriteBarrier(&___onPlayQuestions_5, value);
	}

	inline static int32_t get_offset_of_questionDatas_6() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___questionDatas_6)); }
	inline List_1_t1390196108 * get_questionDatas_6() const { return ___questionDatas_6; }
	inline List_1_t1390196108 ** get_address_of_questionDatas_6() { return &___questionDatas_6; }
	inline void set_questionDatas_6(List_1_t1390196108 * value)
	{
		___questionDatas_6 = value;
		Il2CppCodeGenWriteBarrier(&___questionDatas_6, value);
	}

	inline static int32_t get_offset_of_questionView_7() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___questionView_7)); }
	inline VocabularyQuestionView_t1631345783 * get_questionView_7() const { return ___questionView_7; }
	inline VocabularyQuestionView_t1631345783 ** get_address_of_questionView_7() { return &___questionView_7; }
	inline void set_questionView_7(VocabularyQuestionView_t1631345783 * value)
	{
		___questionView_7 = value;
		Il2CppCodeGenWriteBarrier(&___questionView_7, value);
	}

	inline static int32_t get_offset_of_resultGo_8() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___resultGo_8)); }
	inline GameObject_t1756533147 * get_resultGo_8() const { return ___resultGo_8; }
	inline GameObject_t1756533147 ** get_address_of_resultGo_8() { return &___resultGo_8; }
	inline void set_resultGo_8(GameObject_t1756533147 * value)
	{
		___resultGo_8 = value;
		Il2CppCodeGenWriteBarrier(&___resultGo_8, value);
	}

	inline static int32_t get_offset_of_completedQuestion_9() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___completedQuestion_9)); }
	inline int32_t get_completedQuestion_9() const { return ___completedQuestion_9; }
	inline int32_t* get_address_of_completedQuestion_9() { return &___completedQuestion_9; }
	inline void set_completedQuestion_9(int32_t value)
	{
		___completedQuestion_9 = value;
	}

	inline static int32_t get_offset_of_soundPath_10() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___soundPath_10)); }
	inline String_t* get_soundPath_10() const { return ___soundPath_10; }
	inline String_t** get_address_of_soundPath_10() { return &___soundPath_10; }
	inline void set_soundPath_10(String_t* value)
	{
		___soundPath_10 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_10, value);
	}

	inline static int32_t get_offset_of__currentQuestion_11() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ____currentQuestion_11)); }
	inline VocabularyOnplayQuestion_t3519225043 * get__currentQuestion_11() const { return ____currentQuestion_11; }
	inline VocabularyOnplayQuestion_t3519225043 ** get_address_of__currentQuestion_11() { return &____currentQuestion_11; }
	inline void set__currentQuestion_11(VocabularyOnplayQuestion_t3519225043 * value)
	{
		____currentQuestion_11 = value;
		Il2CppCodeGenWriteBarrier(&____currentQuestion_11, value);
	}

	inline static int32_t get_offset_of_startTime_12() { return static_cast<int32_t>(offsetof(VocalController_t1499553201, ___startTime_12)); }
	inline DateTime_t693205669  get_startTime_12() const { return ___startTime_12; }
	inline DateTime_t693205669 * get_address_of_startTime_12() { return &___startTime_12; }
	inline void set_startTime_12(DateTime_t693205669  value)
	{
		___startTime_12 = value;
	}
};

struct VocalController_t1499553201_StaticFields
{
public:
	// System.Func`1<System.Boolean> VocalController::<>f__mg$cache0
	Func_1_t1485000104 * ___U3CU3Ef__mgU24cache0_13;
	// UnityEngine.Events.UnityAction VocalController::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_14;
	// UnityEngine.Events.UnityAction VocalController::<>f__am$cache1
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache1_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(VocalController_t1499553201_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(VocalController_t1499553201_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_15() { return static_cast<int32_t>(offsetof(VocalController_t1499553201_StaticFields, ___U3CU3Ef__amU24cache1_15)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache1_15() const { return ___U3CU3Ef__amU24cache1_15; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache1_15() { return &___U3CU3Ef__amU24cache1_15; }
	inline void set_U3CU3Ef__amU24cache1_15(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
