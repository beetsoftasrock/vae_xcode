﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultSaver/<PushSoundZip>c__AnonStorey1
struct  U3CPushSoundZipU3Ec__AnonStorey1_t1694356377  : public Il2CppObject
{
public:
	// System.Action ResultSaver/<PushSoundZip>c__AnonStorey1::successCallback
	Action_t3226471752 * ___successCallback_0;
	// System.Action ResultSaver/<PushSoundZip>c__AnonStorey1::errorCallback
	Action_t3226471752 * ___errorCallback_1;

public:
	inline static int32_t get_offset_of_successCallback_0() { return static_cast<int32_t>(offsetof(U3CPushSoundZipU3Ec__AnonStorey1_t1694356377, ___successCallback_0)); }
	inline Action_t3226471752 * get_successCallback_0() const { return ___successCallback_0; }
	inline Action_t3226471752 ** get_address_of_successCallback_0() { return &___successCallback_0; }
	inline void set_successCallback_0(Action_t3226471752 * value)
	{
		___successCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___successCallback_0, value);
	}

	inline static int32_t get_offset_of_errorCallback_1() { return static_cast<int32_t>(offsetof(U3CPushSoundZipU3Ec__AnonStorey1_t1694356377, ___errorCallback_1)); }
	inline Action_t3226471752 * get_errorCallback_1() const { return ___errorCallback_1; }
	inline Action_t3226471752 ** get_address_of_errorCallback_1() { return &___errorCallback_1; }
	inline void set_errorCallback_1(Action_t3226471752 * value)
	{
		___errorCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___errorCallback_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
