﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterSelector/CharacterChangeEvent
struct CharacterChangeEvent_t2435892010;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterSelector/CharacterChangeEvent::.ctor()
extern "C"  void CharacterChangeEvent__ctor_m3533553399 (CharacterChangeEvent_t2435892010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
