﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1
struct U3CloadAssetBundlesU3Ec__Iterator1_t2422683914;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::.ctor()
extern "C"  void U3CloadAssetBundlesU3Ec__Iterator1__ctor_m3526424201 (U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::MoveNext()
extern "C"  bool U3CloadAssetBundlesU3Ec__Iterator1_MoveNext_m2890842755 (U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CloadAssetBundlesU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3756890147 (U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CloadAssetBundlesU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1689533291 (U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::Dispose()
extern "C"  void U3CloadAssetBundlesU3Ec__Iterator1_Dispose_m213894516 (U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1::Reset()
extern "C"  void U3CloadAssetBundlesU3Ec__Iterator1_Reset_m1845091802 (U3CloadAssetBundlesU3Ec__Iterator1_t2422683914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
