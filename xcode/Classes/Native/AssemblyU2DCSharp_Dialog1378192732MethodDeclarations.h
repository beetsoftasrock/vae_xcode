﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Dialog
struct Dialog_t1378192732;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Dialog::.ctor(System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  void Dialog__ctor_m132493270 (Dialog_t1378192732 * __this, String_t* ___content0, String_t* ___subContent1, bool ___value2, String_t* ___voice3, String_t* ___type4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
