﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "AssemblyU2DCSharp_QuestionScenePopup792224618.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening2Popup
struct  Listening2Popup_t2917639739  : public QuestionScenePopup_t792224618
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Listening2Popup::_answerObjs
	List_1_t1125654279 * ____answerObjs_9;

public:
	inline static int32_t get_offset_of__answerObjs_9() { return static_cast<int32_t>(offsetof(Listening2Popup_t2917639739, ____answerObjs_9)); }
	inline List_1_t1125654279 * get__answerObjs_9() const { return ____answerObjs_9; }
	inline List_1_t1125654279 ** get_address_of__answerObjs_9() { return &____answerObjs_9; }
	inline void set__answerObjs_9(List_1_t1125654279 * value)
	{
		____answerObjs_9 = value;
		Il2CppCodeGenWriteBarrier(&____answerObjs_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
