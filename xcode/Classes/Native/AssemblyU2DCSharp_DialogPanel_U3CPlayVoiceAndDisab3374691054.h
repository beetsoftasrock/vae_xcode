﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// DialogPanel
struct DialogPanel_t1568014038;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogPanel/<PlayVoiceAndDisable>c__Iterator1
struct  U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054  : public Il2CppObject
{
public:
	// System.String DialogPanel/<PlayVoiceAndDisable>c__Iterator1::name
	String_t* ___name_0;
	// DialogPanel DialogPanel/<PlayVoiceAndDisable>c__Iterator1::currentDialog
	DialogPanel_t1568014038 * ___currentDialog_1;
	// DialogPanel DialogPanel/<PlayVoiceAndDisable>c__Iterator1::$this
	DialogPanel_t1568014038 * ___U24this_2;
	// System.Object DialogPanel/<PlayVoiceAndDisable>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean DialogPanel/<PlayVoiceAndDisable>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 DialogPanel/<PlayVoiceAndDisable>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_currentDialog_1() { return static_cast<int32_t>(offsetof(U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054, ___currentDialog_1)); }
	inline DialogPanel_t1568014038 * get_currentDialog_1() const { return ___currentDialog_1; }
	inline DialogPanel_t1568014038 ** get_address_of_currentDialog_1() { return &___currentDialog_1; }
	inline void set_currentDialog_1(DialogPanel_t1568014038 * value)
	{
		___currentDialog_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentDialog_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054, ___U24this_2)); }
	inline DialogPanel_t1568014038 * get_U24this_2() const { return ___U24this_2; }
	inline DialogPanel_t1568014038 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(DialogPanel_t1568014038 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CPlayVoiceAndDisableU3Ec__Iterator1_t3374691054, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
