﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyPageController
struct MyPageController_t1695642829;

#include "codegen/il2cpp-codegen.h"

// System.Void MyPageController::.ctor()
extern "C"  void MyPageController__ctor_m4005235400 (MyPageController_t1695642829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageController::Awake()
extern "C"  void MyPageController_Awake_m1593230365 (MyPageController_t1695642829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MyPageController::InitChapter()
extern "C"  void MyPageController_InitChapter_m190492767 (MyPageController_t1695642829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
