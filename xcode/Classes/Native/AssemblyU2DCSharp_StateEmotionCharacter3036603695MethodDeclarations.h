﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StateEmotionCharacter
struct StateEmotionCharacter_t3036603695;

#include "codegen/il2cpp-codegen.h"

// System.Void StateEmotionCharacter::.ctor()
extern "C"  void StateEmotionCharacter__ctor_m3181014760 (StateEmotionCharacter_t3036603695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
