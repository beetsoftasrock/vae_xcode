﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExerciseVRSwitcher/<GoToVRMode>c__AnonStorey1
struct U3CGoToVRModeU3Ec__AnonStorey1_t3426300496;

#include "codegen/il2cpp-codegen.h"

// System.Void ExerciseVRSwitcher/<GoToVRMode>c__AnonStorey1::.ctor()
extern "C"  void U3CGoToVRModeU3Ec__AnonStorey1__ctor_m179189819 (U3CGoToVRModeU3Ec__AnonStorey1_t3426300496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher/<GoToVRMode>c__AnonStorey1::<>m__0()
extern "C"  void U3CGoToVRModeU3Ec__AnonStorey1_U3CU3Em__0_m2689570790 (U3CGoToVRModeU3Ec__AnonStorey1_t3426300496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
