﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HelpPopup
struct HelpPopup_t3079166541;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void HelpPopup::.ctor()
extern "C"  void HelpPopup__ctor_m1694545310 (HelpPopup_t3079166541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HelpPopup::IsAnyHelpPopupOpen()
extern "C"  bool HelpPopup_IsAnyHelpPopupOpen_m1143982163 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::Init(System.String,System.Collections.Generic.List`1<UnityEngine.Sprite>)
extern "C"  void HelpPopup_Init_m1426209934 (HelpPopup_t3079166541 * __this, String_t* ___identify0, List_1_t3973682211 * ___sprites1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::ShowHelp()
extern "C"  void HelpPopup_ShowHelp_m1687782538 (HelpPopup_t3079166541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::NextSprite()
extern "C"  void HelpPopup_NextSprite_m3162184252 (HelpPopup_t3079166541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::PrevSprite()
extern "C"  void HelpPopup_PrevSprite_m1048549556 (HelpPopup_t3079166541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::UpdateSprite()
extern "C"  void HelpPopup_UpdateSprite_m2883761492 (HelpPopup_t3079166541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::SetSprite(UnityEngine.Sprite)
extern "C"  void HelpPopup_SetSprite_m913333185 (HelpPopup_t3079166541 * __this, Sprite_t309593783 * ___sprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::CloseHelp()
extern "C"  void HelpPopup_CloseHelp_m1478996875 (HelpPopup_t3079166541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::OpenHelp(System.String)
extern "C"  void HelpPopup_OpenHelp_m247451779 (HelpPopup_t3079166541 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::Awake()
extern "C"  void HelpPopup_Awake_m1880688285 (HelpPopup_t3079166541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::OnDestroy()
extern "C"  void HelpPopup_OnDestroy_m4274899027 (HelpPopup_t3079166541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void HelpPopup_OnPointerClick_m2347295382 (HelpPopup_t3079166541 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HelpPopup::.cctor()
extern "C"  void HelpPopup__cctor_m951236527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
