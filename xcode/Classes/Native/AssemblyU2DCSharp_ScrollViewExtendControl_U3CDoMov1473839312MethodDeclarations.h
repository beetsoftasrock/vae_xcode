﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollViewExtendControl/<DoMoveContent>c__Iterator0
struct U3CDoMoveContentU3Ec__Iterator0_t1473839312;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollViewExtendControl/<DoMoveContent>c__Iterator0::.ctor()
extern "C"  void U3CDoMoveContentU3Ec__Iterator0__ctor_m3838855289 (U3CDoMoveContentU3Ec__Iterator0_t1473839312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScrollViewExtendControl/<DoMoveContent>c__Iterator0::MoveNext()
extern "C"  bool U3CDoMoveContentU3Ec__Iterator0_MoveNext_m1510369295 (U3CDoMoveContentU3Ec__Iterator0_t1473839312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScrollViewExtendControl/<DoMoveContent>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoMoveContentU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m299997611 (U3CDoMoveContentU3Ec__Iterator0_t1473839312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScrollViewExtendControl/<DoMoveContent>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoMoveContentU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2388938579 (U3CDoMoveContentU3Ec__Iterator0_t1473839312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollViewExtendControl/<DoMoveContent>c__Iterator0::Dispose()
extern "C"  void U3CDoMoveContentU3Ec__Iterator0_Dispose_m1627140794 (U3CDoMoveContentU3Ec__Iterator0_t1473839312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollViewExtendControl/<DoMoveContent>c__Iterator0::Reset()
extern "C"  void U3CDoMoveContentU3Ec__Iterator0_Reset_m2796326972 (U3CDoMoveContentU3Ec__Iterator0_t1473839312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
