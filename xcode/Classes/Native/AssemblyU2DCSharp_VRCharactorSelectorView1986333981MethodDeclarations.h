﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VRCharactorSelectorView
struct VRCharactorSelectorView_t1986333981;

#include "codegen/il2cpp-codegen.h"

// System.Void VRCharactorSelectorView::.ctor()
extern "C"  void VRCharactorSelectorView__ctor_m1086300800 (VRCharactorSelectorView_t1986333981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VRCharactorSelectorView::UpdateView()
extern "C"  void VRCharactorSelectorView_UpdateView_m318970946 (VRCharactorSelectorView_t1986333981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
