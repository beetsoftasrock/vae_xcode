﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// SceneController
struct SceneController_t38942716;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneController/<LoadScene>c__Iterator1
struct  U3CLoadSceneU3Ec__Iterator1_t1985522205  : public Il2CppObject
{
public:
	// System.String SceneController/<LoadScene>c__Iterator1::SceneName
	String_t* ___SceneName_0;
	// UnityEngine.AsyncOperation SceneController/<LoadScene>c__Iterator1::<async>__0
	AsyncOperation_t3814632279 * ___U3CasyncU3E__0_1;
	// SceneController SceneController/<LoadScene>c__Iterator1::$this
	SceneController_t38942716 * ___U24this_2;
	// System.Object SceneController/<LoadScene>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean SceneController/<LoadScene>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 SceneController/<LoadScene>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_SceneName_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator1_t1985522205, ___SceneName_0)); }
	inline String_t* get_SceneName_0() const { return ___SceneName_0; }
	inline String_t** get_address_of_SceneName_0() { return &___SceneName_0; }
	inline void set_SceneName_0(String_t* value)
	{
		___SceneName_0 = value;
		Il2CppCodeGenWriteBarrier(&___SceneName_0, value);
	}

	inline static int32_t get_offset_of_U3CasyncU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator1_t1985522205, ___U3CasyncU3E__0_1)); }
	inline AsyncOperation_t3814632279 * get_U3CasyncU3E__0_1() const { return ___U3CasyncU3E__0_1; }
	inline AsyncOperation_t3814632279 ** get_address_of_U3CasyncU3E__0_1() { return &___U3CasyncU3E__0_1; }
	inline void set_U3CasyncU3E__0_1(AsyncOperation_t3814632279 * value)
	{
		___U3CasyncU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CasyncU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator1_t1985522205, ___U24this_2)); }
	inline SceneController_t38942716 * get_U24this_2() const { return ___U24this_2; }
	inline SceneController_t38942716 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SceneController_t38942716 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator1_t1985522205, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator1_t1985522205, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__Iterator1_t1985522205, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
