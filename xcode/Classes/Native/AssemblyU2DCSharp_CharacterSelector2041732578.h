﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CharacterSelector/CharacterChangeEvent
struct CharacterChangeEvent_t2435892010;
// CharacterSelectionView[]
struct CharacterSelectionViewU5BU5D_t2172268843;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterSelector
struct  CharacterSelector_t2041732578  : public MonoBehaviour_t1158329972
{
public:
	// CharacterSelector/CharacterChangeEvent CharacterSelector::onCharacterChanged
	CharacterChangeEvent_t2435892010 * ___onCharacterChanged_2;
	// CharacterSelectionView[] CharacterSelector::characterSelectionViews
	CharacterSelectionViewU5BU5D_t2172268843* ___characterSelectionViews_3;
	// System.Int32 CharacterSelector::_selectedCharacterId
	int32_t ____selectedCharacterId_4;

public:
	inline static int32_t get_offset_of_onCharacterChanged_2() { return static_cast<int32_t>(offsetof(CharacterSelector_t2041732578, ___onCharacterChanged_2)); }
	inline CharacterChangeEvent_t2435892010 * get_onCharacterChanged_2() const { return ___onCharacterChanged_2; }
	inline CharacterChangeEvent_t2435892010 ** get_address_of_onCharacterChanged_2() { return &___onCharacterChanged_2; }
	inline void set_onCharacterChanged_2(CharacterChangeEvent_t2435892010 * value)
	{
		___onCharacterChanged_2 = value;
		Il2CppCodeGenWriteBarrier(&___onCharacterChanged_2, value);
	}

	inline static int32_t get_offset_of_characterSelectionViews_3() { return static_cast<int32_t>(offsetof(CharacterSelector_t2041732578, ___characterSelectionViews_3)); }
	inline CharacterSelectionViewU5BU5D_t2172268843* get_characterSelectionViews_3() const { return ___characterSelectionViews_3; }
	inline CharacterSelectionViewU5BU5D_t2172268843** get_address_of_characterSelectionViews_3() { return &___characterSelectionViews_3; }
	inline void set_characterSelectionViews_3(CharacterSelectionViewU5BU5D_t2172268843* value)
	{
		___characterSelectionViews_3 = value;
		Il2CppCodeGenWriteBarrier(&___characterSelectionViews_3, value);
	}

	inline static int32_t get_offset_of__selectedCharacterId_4() { return static_cast<int32_t>(offsetof(CharacterSelector_t2041732578, ____selectedCharacterId_4)); }
	inline int32_t get__selectedCharacterId_4() const { return ____selectedCharacterId_4; }
	inline int32_t* get_address_of__selectedCharacterId_4() { return &____selectedCharacterId_4; }
	inline void set__selectedCharacterId_4(int32_t value)
	{
		____selectedCharacterId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
