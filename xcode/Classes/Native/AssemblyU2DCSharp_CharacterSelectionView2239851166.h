﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// OnCharacterSelectionViewClickedEvent
struct OnCharacterSelectionViewClickedEvent_t3002519456;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterSelectionView
struct  CharacterSelectionView_t2239851166  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 CharacterSelectionView::charracterId
	int32_t ___charracterId_2;
	// UnityEngine.UI.Image CharacterSelectionView::selectedImage
	Image_t2042527209 * ___selectedImage_3;
	// UnityEngine.UI.Image CharacterSelectionView::normalImage
	Image_t2042527209 * ___normalImage_4;
	// UnityEngine.UI.Image CharacterSelectionView::textBackGroundImage
	Image_t2042527209 * ___textBackGroundImage_5;
	// UnityEngine.Color CharacterSelectionView::selectedBackgroundColor
	Color_t2020392075  ___selectedBackgroundColor_6;
	// UnityEngine.Color CharacterSelectionView::normalBackgroundColor
	Color_t2020392075  ___normalBackgroundColor_7;
	// UnityEngine.Color CharacterSelectionView::selectedTextColor
	Color_t2020392075  ___selectedTextColor_8;
	// UnityEngine.Color CharacterSelectionView::normalTextColor
	Color_t2020392075  ___normalTextColor_9;
	// UnityEngine.UI.Text CharacterSelectionView::nameText
	Text_t356221433 * ___nameText_10;
	// System.Boolean CharacterSelectionView::_selected
	bool ____selected_11;
	// OnCharacterSelectionViewClickedEvent CharacterSelectionView::onViewClicked
	OnCharacterSelectionViewClickedEvent_t3002519456 * ___onViewClicked_12;

public:
	inline static int32_t get_offset_of_charracterId_2() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___charracterId_2)); }
	inline int32_t get_charracterId_2() const { return ___charracterId_2; }
	inline int32_t* get_address_of_charracterId_2() { return &___charracterId_2; }
	inline void set_charracterId_2(int32_t value)
	{
		___charracterId_2 = value;
	}

	inline static int32_t get_offset_of_selectedImage_3() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___selectedImage_3)); }
	inline Image_t2042527209 * get_selectedImage_3() const { return ___selectedImage_3; }
	inline Image_t2042527209 ** get_address_of_selectedImage_3() { return &___selectedImage_3; }
	inline void set_selectedImage_3(Image_t2042527209 * value)
	{
		___selectedImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectedImage_3, value);
	}

	inline static int32_t get_offset_of_normalImage_4() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___normalImage_4)); }
	inline Image_t2042527209 * get_normalImage_4() const { return ___normalImage_4; }
	inline Image_t2042527209 ** get_address_of_normalImage_4() { return &___normalImage_4; }
	inline void set_normalImage_4(Image_t2042527209 * value)
	{
		___normalImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___normalImage_4, value);
	}

	inline static int32_t get_offset_of_textBackGroundImage_5() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___textBackGroundImage_5)); }
	inline Image_t2042527209 * get_textBackGroundImage_5() const { return ___textBackGroundImage_5; }
	inline Image_t2042527209 ** get_address_of_textBackGroundImage_5() { return &___textBackGroundImage_5; }
	inline void set_textBackGroundImage_5(Image_t2042527209 * value)
	{
		___textBackGroundImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___textBackGroundImage_5, value);
	}

	inline static int32_t get_offset_of_selectedBackgroundColor_6() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___selectedBackgroundColor_6)); }
	inline Color_t2020392075  get_selectedBackgroundColor_6() const { return ___selectedBackgroundColor_6; }
	inline Color_t2020392075 * get_address_of_selectedBackgroundColor_6() { return &___selectedBackgroundColor_6; }
	inline void set_selectedBackgroundColor_6(Color_t2020392075  value)
	{
		___selectedBackgroundColor_6 = value;
	}

	inline static int32_t get_offset_of_normalBackgroundColor_7() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___normalBackgroundColor_7)); }
	inline Color_t2020392075  get_normalBackgroundColor_7() const { return ___normalBackgroundColor_7; }
	inline Color_t2020392075 * get_address_of_normalBackgroundColor_7() { return &___normalBackgroundColor_7; }
	inline void set_normalBackgroundColor_7(Color_t2020392075  value)
	{
		___normalBackgroundColor_7 = value;
	}

	inline static int32_t get_offset_of_selectedTextColor_8() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___selectedTextColor_8)); }
	inline Color_t2020392075  get_selectedTextColor_8() const { return ___selectedTextColor_8; }
	inline Color_t2020392075 * get_address_of_selectedTextColor_8() { return &___selectedTextColor_8; }
	inline void set_selectedTextColor_8(Color_t2020392075  value)
	{
		___selectedTextColor_8 = value;
	}

	inline static int32_t get_offset_of_normalTextColor_9() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___normalTextColor_9)); }
	inline Color_t2020392075  get_normalTextColor_9() const { return ___normalTextColor_9; }
	inline Color_t2020392075 * get_address_of_normalTextColor_9() { return &___normalTextColor_9; }
	inline void set_normalTextColor_9(Color_t2020392075  value)
	{
		___normalTextColor_9 = value;
	}

	inline static int32_t get_offset_of_nameText_10() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___nameText_10)); }
	inline Text_t356221433 * get_nameText_10() const { return ___nameText_10; }
	inline Text_t356221433 ** get_address_of_nameText_10() { return &___nameText_10; }
	inline void set_nameText_10(Text_t356221433 * value)
	{
		___nameText_10 = value;
		Il2CppCodeGenWriteBarrier(&___nameText_10, value);
	}

	inline static int32_t get_offset_of__selected_11() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ____selected_11)); }
	inline bool get__selected_11() const { return ____selected_11; }
	inline bool* get_address_of__selected_11() { return &____selected_11; }
	inline void set__selected_11(bool value)
	{
		____selected_11 = value;
	}

	inline static int32_t get_offset_of_onViewClicked_12() { return static_cast<int32_t>(offsetof(CharacterSelectionView_t2239851166, ___onViewClicked_12)); }
	inline OnCharacterSelectionViewClickedEvent_t3002519456 * get_onViewClicked_12() const { return ___onViewClicked_12; }
	inline OnCharacterSelectionViewClickedEvent_t3002519456 ** get_address_of_onViewClicked_12() { return &___onViewClicked_12; }
	inline void set_onViewClicked_12(OnCharacterSelectionViewClickedEvent_t3002519456 * value)
	{
		___onViewClicked_12 = value;
		Il2CppCodeGenWriteBarrier(&___onViewClicked_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
