﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayTalkDialog
struct ReplayTalkDialog_t3732871695;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.String
struct String_t;
// CharacterScript
struct CharacterScript_t1308706256;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// KaraokeTextEffect
struct KaraokeTextEffect_t1279919556;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_CharacterScript1308706256.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_KaraokeTextEffect1279919556.h"

// System.Void ReplayTalkDialog::.ctor()
extern "C"  void ReplayTalkDialog__ctor_m2502988452 (ReplayTalkDialog_t3732871695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayTalkDialog::get_showSubText()
extern "C"  bool ReplayTalkDialog_get_showSubText_m2513760689 (ReplayTalkDialog_t3732871695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayTalkDialog::set_showSubText(System.Boolean)
extern "C"  void ReplayTalkDialog_set_showSubText_m231766480 (ReplayTalkDialog_t3732871695 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayTalkDialog::PlayRecordedVoice(UnityEngine.AudioClip,System.String,System.String,CharacterScript,ConversationTalkData,System.Action)
extern "C"  void ReplayTalkDialog_PlayRecordedVoice_m2901569039 (ReplayTalkDialog_t3732871695 * __this, AudioClip_t1932558630 * ___clip0, String_t* ___pathName1, String_t* ___resourcePathName2, CharacterScript_t1308706256 * ___characterScript3, ConversationTalkData_t1570298305 * ___talkData4, Action_t3226471752 * ___callback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ReplayTalkDialog::Play(UnityEngine.AudioClip,System.String,System.String,ConversationTalkData,System.Action)
extern "C"  Il2CppObject * ReplayTalkDialog_Play_m378347553 (ReplayTalkDialog_t3732871695 * __this, AudioClip_t1932558630 * ___clip0, String_t* ___recordedPathName1, String_t* ___resourcePathName2, ConversationTalkData_t1570298305 * ___talkData3, Action_t3226471752 * ___callback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ReplayTalkDialog::FakePlay(System.String,System.String,ConversationTalkData,System.Action)
extern "C"  Il2CppObject * ReplayTalkDialog_FakePlay_m893567471 (ReplayTalkDialog_t3732871695 * __this, String_t* ___pathName0, String_t* ___resourcePathName1, ConversationTalkData_t1570298305 * ___talkData2, Action_t3226471752 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ReplayTalkDialog::PlayKaraOkeEffect(KaraokeTextEffect,System.Single)
extern "C"  Il2CppObject * ReplayTalkDialog_PlayKaraOkeEffect_m4007430870 (ReplayTalkDialog_t3732871695 * __this, KaraokeTextEffect_t1279919556 * ___karaokeTextEffect0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
