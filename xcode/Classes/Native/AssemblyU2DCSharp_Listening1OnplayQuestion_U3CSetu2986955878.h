﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Listening1OnplayQuestion
struct Listening1OnplayQuestion_t3534871625;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening1OnplayQuestion/<SetupEventHandler>c__AnonStorey0
struct  U3CSetupEventHandlerU3Ec__AnonStorey0_t2986955878  : public Il2CppObject
{
public:
	// System.Int32 Listening1OnplayQuestion/<SetupEventHandler>c__AnonStorey0::index
	int32_t ___index_0;
	// Listening1OnplayQuestion Listening1OnplayQuestion/<SetupEventHandler>c__AnonStorey0::$this
	Listening1OnplayQuestion_t3534871625 * ___U24this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CSetupEventHandlerU3Ec__AnonStorey0_t2986955878, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSetupEventHandlerU3Ec__AnonStorey0_t2986955878, ___U24this_1)); }
	inline Listening1OnplayQuestion_t3534871625 * get_U24this_1() const { return ___U24this_1; }
	inline Listening1OnplayQuestion_t3534871625 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Listening1OnplayQuestion_t3534871625 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
