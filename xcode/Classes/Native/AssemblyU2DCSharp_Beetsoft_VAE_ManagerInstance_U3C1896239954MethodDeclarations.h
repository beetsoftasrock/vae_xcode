﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.ManagerInstance/<IsPlaying>c__AnonStorey2
struct U3CIsPlayingU3Ec__AnonStorey2_t1896239954;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"

// System.Void Beetsoft.VAE.ManagerInstance/<IsPlaying>c__AnonStorey2::.ctor()
extern "C"  void U3CIsPlayingU3Ec__AnonStorey2__ctor_m3887281613 (U3CIsPlayingU3Ec__AnonStorey2_t1896239954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.ManagerInstance/<IsPlaying>c__AnonStorey2::<>m__0(UnityEngine.AudioSource)
extern "C"  bool U3CIsPlayingU3Ec__AnonStorey2_U3CU3Em__0_m1348160090 (U3CIsPlayingU3Ec__AnonStorey2_t1896239954 * __this, AudioSource_t1135106623 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
