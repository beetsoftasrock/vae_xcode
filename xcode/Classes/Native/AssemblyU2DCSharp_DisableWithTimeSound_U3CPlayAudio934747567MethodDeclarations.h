﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisableWithTimeSound/<PlayAudioAndDisable>c__Iterator0
struct U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DisableWithTimeSound/<PlayAudioAndDisable>c__Iterator0::.ctor()
extern "C"  void U3CPlayAudioAndDisableU3Ec__Iterator0__ctor_m884644686 (U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DisableWithTimeSound/<PlayAudioAndDisable>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayAudioAndDisableU3Ec__Iterator0_MoveNext_m1117336486 (U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DisableWithTimeSound/<PlayAudioAndDisable>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayAudioAndDisableU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3295041342 (U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DisableWithTimeSound/<PlayAudioAndDisable>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayAudioAndDisableU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1937081846 (U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeSound/<PlayAudioAndDisable>c__Iterator0::Dispose()
extern "C"  void U3CPlayAudioAndDisableU3Ec__Iterator0_Dispose_m876225765 (U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeSound/<PlayAudioAndDisable>c__Iterator0::Reset()
extern "C"  void U3CPlayAudioAndDisableU3Ec__Iterator0_Reset_m701752799 (U3CPlayAudioAndDisableU3Ec__Iterator0_t934747567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
