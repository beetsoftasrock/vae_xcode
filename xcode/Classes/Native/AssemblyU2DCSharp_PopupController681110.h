﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupController
struct  PopupController_t681110  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PopupController::trueImage
	GameObject_t1756533147 * ___trueImage_2;
	// UnityEngine.GameObject PopupController::falseImage
	GameObject_t1756533147 * ___falseImage_3;
	// UnityEngine.UI.Text PopupController::Title
	Text_t356221433 * ___Title_4;
	// System.Action PopupController::OnNextPress
	Action_t3226471752 * ___OnNextPress_5;
	// UnityEngine.GameObject PopupController::Content
	GameObject_t1756533147 * ___Content_6;
	// UnityEngine.GameObject PopupController::contentUI
	GameObject_t1756533147 * ___contentUI_7;

public:
	inline static int32_t get_offset_of_trueImage_2() { return static_cast<int32_t>(offsetof(PopupController_t681110, ___trueImage_2)); }
	inline GameObject_t1756533147 * get_trueImage_2() const { return ___trueImage_2; }
	inline GameObject_t1756533147 ** get_address_of_trueImage_2() { return &___trueImage_2; }
	inline void set_trueImage_2(GameObject_t1756533147 * value)
	{
		___trueImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___trueImage_2, value);
	}

	inline static int32_t get_offset_of_falseImage_3() { return static_cast<int32_t>(offsetof(PopupController_t681110, ___falseImage_3)); }
	inline GameObject_t1756533147 * get_falseImage_3() const { return ___falseImage_3; }
	inline GameObject_t1756533147 ** get_address_of_falseImage_3() { return &___falseImage_3; }
	inline void set_falseImage_3(GameObject_t1756533147 * value)
	{
		___falseImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___falseImage_3, value);
	}

	inline static int32_t get_offset_of_Title_4() { return static_cast<int32_t>(offsetof(PopupController_t681110, ___Title_4)); }
	inline Text_t356221433 * get_Title_4() const { return ___Title_4; }
	inline Text_t356221433 ** get_address_of_Title_4() { return &___Title_4; }
	inline void set_Title_4(Text_t356221433 * value)
	{
		___Title_4 = value;
		Il2CppCodeGenWriteBarrier(&___Title_4, value);
	}

	inline static int32_t get_offset_of_OnNextPress_5() { return static_cast<int32_t>(offsetof(PopupController_t681110, ___OnNextPress_5)); }
	inline Action_t3226471752 * get_OnNextPress_5() const { return ___OnNextPress_5; }
	inline Action_t3226471752 ** get_address_of_OnNextPress_5() { return &___OnNextPress_5; }
	inline void set_OnNextPress_5(Action_t3226471752 * value)
	{
		___OnNextPress_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnNextPress_5, value);
	}

	inline static int32_t get_offset_of_Content_6() { return static_cast<int32_t>(offsetof(PopupController_t681110, ___Content_6)); }
	inline GameObject_t1756533147 * get_Content_6() const { return ___Content_6; }
	inline GameObject_t1756533147 ** get_address_of_Content_6() { return &___Content_6; }
	inline void set_Content_6(GameObject_t1756533147 * value)
	{
		___Content_6 = value;
		Il2CppCodeGenWriteBarrier(&___Content_6, value);
	}

	inline static int32_t get_offset_of_contentUI_7() { return static_cast<int32_t>(offsetof(PopupController_t681110, ___contentUI_7)); }
	inline GameObject_t1756533147 * get_contentUI_7() const { return ___contentUI_7; }
	inline GameObject_t1756533147 ** get_address_of_contentUI_7() { return &___contentUI_7; }
	inline void set_contentUI_7(GameObject_t1756533147 * value)
	{
		___contentUI_7 = value;
		Il2CppCodeGenWriteBarrier(&___contentUI_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
