﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ChapterItem>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1051787991(__this, ___l0, method) ((  void (*) (Enumerator_t1059142140 *, List_1_t1524412466 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ChapterItem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2865712171(__this, method) ((  void (*) (Enumerator_t1059142140 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ChapterItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2989011615(__this, method) ((  Il2CppObject * (*) (Enumerator_t1059142140 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ChapterItem>::Dispose()
#define Enumerator_Dispose_m791038618(__this, method) ((  void (*) (Enumerator_t1059142140 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ChapterItem>::VerifyState()
#define Enumerator_VerifyState_m268328305(__this, method) ((  void (*) (Enumerator_t1059142140 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ChapterItem>::MoveNext()
#define Enumerator_MoveNext_m603772694(__this, method) ((  bool (*) (Enumerator_t1059142140 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ChapterItem>::get_Current()
#define Enumerator_get_Current_m2981816476(__this, method) ((  ChapterItem_t2155291334 * (*) (Enumerator_t1059142140 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
