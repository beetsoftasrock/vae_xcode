﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EyeRaycaster/<DoCheckingLoop>c__Iterator0
struct U3CDoCheckingLoopU3Ec__Iterator0_t3338893804;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EyeRaycaster/<DoCheckingLoop>c__Iterator0::.ctor()
extern "C"  void U3CDoCheckingLoopU3Ec__Iterator0__ctor_m3209723243 (U3CDoCheckingLoopU3Ec__Iterator0_t3338893804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EyeRaycaster/<DoCheckingLoop>c__Iterator0::MoveNext()
extern "C"  bool U3CDoCheckingLoopU3Ec__Iterator0_MoveNext_m2299564269 (U3CDoCheckingLoopU3Ec__Iterator0_t3338893804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EyeRaycaster/<DoCheckingLoop>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoCheckingLoopU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3752330141 (U3CDoCheckingLoopU3Ec__Iterator0_t3338893804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EyeRaycaster/<DoCheckingLoop>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoCheckingLoopU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1695656005 (U3CDoCheckingLoopU3Ec__Iterator0_t3338893804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EyeRaycaster/<DoCheckingLoop>c__Iterator0::Dispose()
extern "C"  void U3CDoCheckingLoopU3Ec__Iterator0_Dispose_m2791660790 (U3CDoCheckingLoopU3Ec__Iterator0_t3338893804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EyeRaycaster/<DoCheckingLoop>c__Iterator0::Reset()
extern "C"  void U3CDoCheckingLoopU3Ec__Iterator0_Reset_m3036621048 (U3CDoCheckingLoopU3Ec__Iterator0_t3338893804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
