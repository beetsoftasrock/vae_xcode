﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationCommand
struct ConversationCommand_t3660105836;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationCommand::.ctor()
extern "C"  void ConversationCommand__ctor_m2981785105 (ConversationCommand_t3660105836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
