﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayOtherTalkDialog/<Play>c__Iterator1
struct U3CPlayU3Ec__Iterator1_t3847971905;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayOtherTalkDialog/<Play>c__Iterator1::.ctor()
extern "C"  void U3CPlayU3Ec__Iterator1__ctor_m3974952006 (U3CPlayU3Ec__Iterator1_t3847971905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayOtherTalkDialog/<Play>c__Iterator1::MoveNext()
extern "C"  bool U3CPlayU3Ec__Iterator1_MoveNext_m2976965446 (U3CPlayU3Ec__Iterator1_t3847971905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayOtherTalkDialog/<Play>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4212218474 (U3CPlayU3Ec__Iterator1_t3847971905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayOtherTalkDialog/<Play>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4088849442 (U3CPlayU3Ec__Iterator1_t3847971905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayOtherTalkDialog/<Play>c__Iterator1::Dispose()
extern "C"  void U3CPlayU3Ec__Iterator1_Dispose_m1751875835 (U3CPlayU3Ec__Iterator1_t3847971905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayOtherTalkDialog/<Play>c__Iterator1::Reset()
extern "C"  void U3CPlayU3Ec__Iterator1_Reset_m3793144693 (U3CPlayU3Ec__Iterator1_t3847971905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
