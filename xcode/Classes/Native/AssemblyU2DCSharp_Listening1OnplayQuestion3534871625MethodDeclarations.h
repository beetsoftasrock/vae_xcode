﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening1OnplayQuestion
struct Listening1OnplayQuestion_t3534871625;
// Listening1QuestionData
struct Listening1QuestionData_t3381069024;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3194695850;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// TrueFalseAnswerSelection
struct TrueFalseAnswerSelection_t3406544577;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Listening1QuestionData3381069024.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_TrueFalseAnswerSelection3406544577.h"

// System.Void Listening1OnplayQuestion::.ctor(Listening1QuestionData)
extern "C"  void Listening1OnplayQuestion__ctor_m2260116882 (Listening1OnplayQuestion_t3534871625 * __this, Listening1QuestionData_t3381069024 * ___questionData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening1OnplayQuestion::CheckCorrect()
extern "C"  bool Listening1OnplayQuestion_CheckCorrect_m2288222940 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Listening1OnplayQuestion::GetSumScoreCorrect()
extern "C"  int32_t Listening1OnplayQuestion_GetSumScoreCorrect_m3545473361 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Listening1OnplayQuestion::CheckCorrectNew()
extern "C"  String_t* Listening1OnplayQuestion_CheckCorrectNew_m999038121 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening1OnplayQuestion::GetTotalScore(System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern "C"  bool Listening1OnplayQuestion_GetTotalScore_m618538198 (Listening1OnplayQuestion_t3534871625 * __this, int32_t* ___numberQuestion0, int32_t* ___numberWrong1, int32_t* ___numberRight2, int32_t* ___totalScore3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::SetView(UnityEngine.GameObject)
extern "C"  void Listening1OnplayQuestion_SetView_m2223044827 (Listening1OnplayQuestion_t3534871625 * __this, GameObject_t1756533147 * ___viewGo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::SetupEventHandler()
extern "C"  void Listening1OnplayQuestion_SetupEventHandler_m1933772719 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::HandleAnswerChangedEvent(System.Int32)
extern "C"  void Listening1OnplayQuestion_HandleAnswerChangedEvent_m3384760281 (Listening1OnplayQuestion_t3534871625 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::RemoveView()
extern "C"  void Listening1OnplayQuestion_RemoveView_m1276454399 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::ShowResult()
extern "C"  void Listening1OnplayQuestion_ShowResult_m3236930456 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Listening1OnplayQuestion::GetScore()
extern "C"  float Listening1OnplayQuestion_GetScore_m1242664856 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Listening1QuestionData Listening1OnplayQuestion::get_questionData()
extern "C"  Listening1QuestionData_t3381069024 * Listening1OnplayQuestion_get_questionData_m2654321030 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::set_questionData(Listening1QuestionData)
extern "C"  void Listening1OnplayQuestion_set_questionData_m3827993105 (Listening1OnplayQuestion_t3534871625 * __this, Listening1QuestionData_t3381069024 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Boolean> Listening1OnplayQuestion::get_currentAnswer()
extern "C"  List_1_t3194695850 * Listening1OnplayQuestion_get_currentAnswer_m3760285888 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::set_currentAnswer(System.Collections.Generic.List`1<System.Boolean>)
extern "C"  void Listening1OnplayQuestion_set_currentAnswer_m1525806069 (Listening1OnplayQuestion_t3534871625 * __this, List_1_t3194695850 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> Listening1OnplayQuestion::get_currentIndexAnswer()
extern "C"  List_1_t1440998580 * Listening1OnplayQuestion_get_currentIndexAnswer_m326705986 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::set_currentIndexAnswer(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void Listening1OnplayQuestion_set_currentIndexAnswer_m3871293697 (Listening1OnplayQuestion_t3534871625 * __this, List_1_t1440998580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::PlayQuestionSound()
extern "C"  void Listening1OnplayQuestion_PlayQuestionSound_m943759077 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::HandleSummitAction()
extern "C"  void Listening1OnplayQuestion_HandleSummitAction_m3549300727 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening1OnplayQuestion::<GetSumScoreCorrect>m__0(TrueFalseAnswerSelection)
extern "C"  bool Listening1OnplayQuestion_U3CGetSumScoreCorrectU3Em__0_m3889756131 (Il2CppObject * __this /* static, unused */, TrueFalseAnswerSelection_t3406544577 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1OnplayQuestion::<ShowResult>m__1()
extern "C"  void Listening1OnplayQuestion_U3CShowResultU3Em__1_m903903750 (Listening1OnplayQuestion_t3534871625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
