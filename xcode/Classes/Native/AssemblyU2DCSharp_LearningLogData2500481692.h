﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LearningChapterLogData[]
struct LearningChapterLogDataU5BU5D_t2306100374;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LearningLogData
struct  LearningLogData_t2500481692  : public Il2CppObject
{
public:
	// System.Int32 LearningLogData::chapter
	int32_t ___chapter_0;
	// System.Int32 LearningLogData::alltime
	int32_t ___alltime_1;
	// LearningChapterLogData[] LearningLogData::logData
	LearningChapterLogDataU5BU5D_t2306100374* ___logData_2;

public:
	inline static int32_t get_offset_of_chapter_0() { return static_cast<int32_t>(offsetof(LearningLogData_t2500481692, ___chapter_0)); }
	inline int32_t get_chapter_0() const { return ___chapter_0; }
	inline int32_t* get_address_of_chapter_0() { return &___chapter_0; }
	inline void set_chapter_0(int32_t value)
	{
		___chapter_0 = value;
	}

	inline static int32_t get_offset_of_alltime_1() { return static_cast<int32_t>(offsetof(LearningLogData_t2500481692, ___alltime_1)); }
	inline int32_t get_alltime_1() const { return ___alltime_1; }
	inline int32_t* get_address_of_alltime_1() { return &___alltime_1; }
	inline void set_alltime_1(int32_t value)
	{
		___alltime_1 = value;
	}

	inline static int32_t get_offset_of_logData_2() { return static_cast<int32_t>(offsetof(LearningLogData_t2500481692, ___logData_2)); }
	inline LearningChapterLogDataU5BU5D_t2306100374* get_logData_2() const { return ___logData_2; }
	inline LearningChapterLogDataU5BU5D_t2306100374** get_address_of_logData_2() { return &___logData_2; }
	inline void set_logData_2(LearningChapterLogDataU5BU5D_t2306100374* value)
	{
		___logData_2 = value;
		Il2CppCodeGenWriteBarrier(&___logData_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
