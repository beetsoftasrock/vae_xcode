﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoiceRecordModuleNormal
struct VoiceRecordModuleNormal_t1880663460;

#include "codegen/il2cpp-codegen.h"

// System.Void VoiceRecordModuleNormal::.ctor()
extern "C"  void VoiceRecordModuleNormal__ctor_m161510981 (VoiceRecordModuleNormal_t1880663460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModuleNormal::StartVoiceRecording()
extern "C"  void VoiceRecordModuleNormal_StartVoiceRecording_m3359334794 (VoiceRecordModuleNormal_t1880663460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModuleNormal::EndVoiceRecording()
extern "C"  void VoiceRecordModuleNormal_EndVoiceRecording_m1471911941 (VoiceRecordModuleNormal_t1880663460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
