﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearningData
struct LearningData_t1664811342;

#include "codegen/il2cpp-codegen.h"

// System.Void LearningData::.ctor()
extern "C"  void LearningData__ctor_m3302988379 (LearningData_t1664811342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
