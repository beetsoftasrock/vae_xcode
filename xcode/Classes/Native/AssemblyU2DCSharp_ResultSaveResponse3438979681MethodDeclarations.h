﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultSaveResponse
struct ResultSaveResponse_t3438979681;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultSaveResponse::.ctor()
extern "C"  void ResultSaveResponse__ctor_m2441062782 (ResultSaveResponse_t3438979681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
