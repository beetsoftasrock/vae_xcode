﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KaraokeTextEffect/<PlayEffect>c__Iterator0
struct U3CPlayEffectU3Ec__Iterator0_t4283951911;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void KaraokeTextEffect/<PlayEffect>c__Iterator0::.ctor()
extern "C"  void U3CPlayEffectU3Ec__Iterator0__ctor_m3961741888 (U3CPlayEffectU3Ec__Iterator0_t4283951911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean KaraokeTextEffect/<PlayEffect>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayEffectU3Ec__Iterator0_MoveNext_m3448732708 (U3CPlayEffectU3Ec__Iterator0_t4283951911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KaraokeTextEffect/<PlayEffect>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayEffectU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1532805740 (U3CPlayEffectU3Ec__Iterator0_t4283951911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object KaraokeTextEffect/<PlayEffect>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayEffectU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m996599924 (U3CPlayEffectU3Ec__Iterator0_t4283951911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KaraokeTextEffect/<PlayEffect>c__Iterator0::Dispose()
extern "C"  void U3CPlayEffectU3Ec__Iterator0_Dispose_m2312453869 (U3CPlayEffectU3Ec__Iterator0_t4283951911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KaraokeTextEffect/<PlayEffect>c__Iterator0::Reset()
extern "C"  void U3CPlayEffectU3Ec__Iterator0_Reset_m2001044503 (U3CPlayEffectU3Ec__Iterator0_t4283951911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
