﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3598653863.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_GraphValue2739901601.h"

// System.Void System.Array/InternalEnumerator`1<GraphValue>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1321083234_gshared (InternalEnumerator_1_t3598653863 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1321083234(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3598653863 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1321083234_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<GraphValue>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m200737798_gshared (InternalEnumerator_1_t3598653863 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m200737798(__this, method) ((  void (*) (InternalEnumerator_1_t3598653863 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m200737798_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<GraphValue>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1350355206_gshared (InternalEnumerator_1_t3598653863 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1350355206(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3598653863 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1350355206_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<GraphValue>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2762980425_gshared (InternalEnumerator_1_t3598653863 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2762980425(__this, method) ((  void (*) (InternalEnumerator_1_t3598653863 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2762980425_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<GraphValue>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1321567794_gshared (InternalEnumerator_1_t3598653863 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1321567794(__this, method) ((  bool (*) (InternalEnumerator_1_t3598653863 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1321567794_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<GraphValue>::get_Current()
extern "C"  GraphValue_t2739901601  InternalEnumerator_1_get_Current_m1139268705_gshared (InternalEnumerator_1_t3598653863 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1139268705(__this, method) ((  GraphValue_t2739901601  (*) (InternalEnumerator_1_t3598653863 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1139268705_gshared)(__this, method)
