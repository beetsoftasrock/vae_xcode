﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnCharacterSelectionViewClickedEvent
struct OnCharacterSelectionViewClickedEvent_t3002519456;

#include "codegen/il2cpp-codegen.h"

// System.Void OnCharacterSelectionViewClickedEvent::.ctor()
extern "C"  void OnCharacterSelectionViewClickedEvent__ctor_m3579184425 (OnCharacterSelectionViewClickedEvent_t3002519456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
