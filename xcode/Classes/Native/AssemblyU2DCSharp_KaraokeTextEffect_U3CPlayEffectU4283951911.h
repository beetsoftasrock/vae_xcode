﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// KaraokeTextEffect
struct KaraokeTextEffect_t1279919556;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KaraokeTextEffect/<PlayEffect>c__Iterator0
struct  U3CPlayEffectU3Ec__Iterator0_t4283951911  : public Il2CppObject
{
public:
	// System.String KaraokeTextEffect/<PlayEffect>c__Iterator0::<content>__0
	String_t* ___U3CcontentU3E__0_0;
	// System.Single KaraokeTextEffect/<PlayEffect>c__Iterator0::time
	float ___time_1;
	// System.Single KaraokeTextEffect/<PlayEffect>c__Iterator0::<timeSpace>__1
	float ___U3CtimeSpaceU3E__1_2;
	// System.String KaraokeTextEffect/<PlayEffect>c__Iterator0::<temp>__2
	String_t* ___U3CtempU3E__2_3;
	// System.String KaraokeTextEffect/<PlayEffect>c__Iterator0::<temp2>__3
	String_t* ___U3Ctemp2U3E__3_4;
	// System.Int32 KaraokeTextEffect/<PlayEffect>c__Iterator0::<j>__4
	int32_t ___U3CjU3E__4_5;
	// System.Single KaraokeTextEffect/<PlayEffect>c__Iterator0::<passedTime>__5
	float ___U3CpassedTimeU3E__5_6;
	// KaraokeTextEffect KaraokeTextEffect/<PlayEffect>c__Iterator0::$this
	KaraokeTextEffect_t1279919556 * ___U24this_7;
	// System.Object KaraokeTextEffect/<PlayEffect>c__Iterator0::$current
	Il2CppObject * ___U24current_8;
	// System.Boolean KaraokeTextEffect/<PlayEffect>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 KaraokeTextEffect/<PlayEffect>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CcontentU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U3CcontentU3E__0_0)); }
	inline String_t* get_U3CcontentU3E__0_0() const { return ___U3CcontentU3E__0_0; }
	inline String_t** get_address_of_U3CcontentU3E__0_0() { return &___U3CcontentU3E__0_0; }
	inline void set_U3CcontentU3E__0_0(String_t* value)
	{
		___U3CcontentU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcontentU3E__0_0, value);
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_U3CtimeSpaceU3E__1_2() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U3CtimeSpaceU3E__1_2)); }
	inline float get_U3CtimeSpaceU3E__1_2() const { return ___U3CtimeSpaceU3E__1_2; }
	inline float* get_address_of_U3CtimeSpaceU3E__1_2() { return &___U3CtimeSpaceU3E__1_2; }
	inline void set_U3CtimeSpaceU3E__1_2(float value)
	{
		___U3CtimeSpaceU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E__2_3() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U3CtempU3E__2_3)); }
	inline String_t* get_U3CtempU3E__2_3() const { return ___U3CtempU3E__2_3; }
	inline String_t** get_address_of_U3CtempU3E__2_3() { return &___U3CtempU3E__2_3; }
	inline void set_U3CtempU3E__2_3(String_t* value)
	{
		___U3CtempU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3Ctemp2U3E__3_4() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U3Ctemp2U3E__3_4)); }
	inline String_t* get_U3Ctemp2U3E__3_4() const { return ___U3Ctemp2U3E__3_4; }
	inline String_t** get_address_of_U3Ctemp2U3E__3_4() { return &___U3Ctemp2U3E__3_4; }
	inline void set_U3Ctemp2U3E__3_4(String_t* value)
	{
		___U3Ctemp2U3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Ctemp2U3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CjU3E__4_5() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U3CjU3E__4_5)); }
	inline int32_t get_U3CjU3E__4_5() const { return ___U3CjU3E__4_5; }
	inline int32_t* get_address_of_U3CjU3E__4_5() { return &___U3CjU3E__4_5; }
	inline void set_U3CjU3E__4_5(int32_t value)
	{
		___U3CjU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CpassedTimeU3E__5_6() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U3CpassedTimeU3E__5_6)); }
	inline float get_U3CpassedTimeU3E__5_6() const { return ___U3CpassedTimeU3E__5_6; }
	inline float* get_address_of_U3CpassedTimeU3E__5_6() { return &___U3CpassedTimeU3E__5_6; }
	inline void set_U3CpassedTimeU3E__5_6(float value)
	{
		___U3CpassedTimeU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U24this_7)); }
	inline KaraokeTextEffect_t1279919556 * get_U24this_7() const { return ___U24this_7; }
	inline KaraokeTextEffect_t1279919556 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(KaraokeTextEffect_t1279919556 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__Iterator0_t4283951911, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
