﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey
struct FieldWithRemoteSettingsKey_t2620356393;
// UnityEngine.Object
struct Object_t1021602117;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::.ctor()
extern "C"  void FieldWithRemoteSettingsKey__ctor_m360199534 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::get_target()
extern "C"  Object_t1021602117 * FieldWithRemoteSettingsKey_get_target_m1374164995 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::set_target(UnityEngine.Object)
extern "C"  void FieldWithRemoteSettingsKey_set_target_m3009102684 (FieldWithRemoteSettingsKey_t2620356393 * __this, Object_t1021602117 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::get_fieldPath()
extern "C"  String_t* FieldWithRemoteSettingsKey_get_fieldPath_m325365421 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::set_fieldPath(System.String)
extern "C"  void FieldWithRemoteSettingsKey_set_fieldPath_m1486844300 (FieldWithRemoteSettingsKey_t2620356393 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::get_rsKeyName()
extern "C"  String_t* FieldWithRemoteSettingsKey_get_rsKeyName_m299883495 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::set_rsKeyName(System.String)
extern "C"  void FieldWithRemoteSettingsKey_set_rsKeyName_m655532302 (FieldWithRemoteSettingsKey_t2620356393 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::get_type()
extern "C"  String_t* FieldWithRemoteSettingsKey_get_type_m1923228720 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::set_type(System.String)
extern "C"  void FieldWithRemoteSettingsKey_set_type_m2857900137 (FieldWithRemoteSettingsKey_t2620356393 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::SetValue(System.Object)
extern "C"  void FieldWithRemoteSettingsKey_SetValue_m1404583385 (FieldWithRemoteSettingsKey_t2620356393 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey::GetTypeOfField()
extern "C"  Type_t * FieldWithRemoteSettingsKey_GetTypeOfField_m928225135 (FieldWithRemoteSettingsKey_t2620356393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
