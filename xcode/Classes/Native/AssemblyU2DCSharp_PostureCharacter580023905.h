﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String
struct String_t;
// System.Collections.Generic.List`1<StateEmotionCharacter>
struct List_1_t2405724827;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PostureCharacter
struct  PostureCharacter_t580023905  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 PostureCharacter::index
	int32_t ___index_2;
	// UnityEngine.Sprite PostureCharacter::postureSprite
	Sprite_t309593783 * ___postureSprite_3;
	// System.String PostureCharacter::postureName
	String_t* ___postureName_4;
	// System.String PostureCharacter::remark
	String_t* ___remark_5;
	// System.Collections.Generic.List`1<StateEmotionCharacter> PostureCharacter::emotions
	List_1_t2405724827 * ___emotions_6;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(PostureCharacter_t580023905, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_postureSprite_3() { return static_cast<int32_t>(offsetof(PostureCharacter_t580023905, ___postureSprite_3)); }
	inline Sprite_t309593783 * get_postureSprite_3() const { return ___postureSprite_3; }
	inline Sprite_t309593783 ** get_address_of_postureSprite_3() { return &___postureSprite_3; }
	inline void set_postureSprite_3(Sprite_t309593783 * value)
	{
		___postureSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___postureSprite_3, value);
	}

	inline static int32_t get_offset_of_postureName_4() { return static_cast<int32_t>(offsetof(PostureCharacter_t580023905, ___postureName_4)); }
	inline String_t* get_postureName_4() const { return ___postureName_4; }
	inline String_t** get_address_of_postureName_4() { return &___postureName_4; }
	inline void set_postureName_4(String_t* value)
	{
		___postureName_4 = value;
		Il2CppCodeGenWriteBarrier(&___postureName_4, value);
	}

	inline static int32_t get_offset_of_remark_5() { return static_cast<int32_t>(offsetof(PostureCharacter_t580023905, ___remark_5)); }
	inline String_t* get_remark_5() const { return ___remark_5; }
	inline String_t** get_address_of_remark_5() { return &___remark_5; }
	inline void set_remark_5(String_t* value)
	{
		___remark_5 = value;
		Il2CppCodeGenWriteBarrier(&___remark_5, value);
	}

	inline static int32_t get_offset_of_emotions_6() { return static_cast<int32_t>(offsetof(PostureCharacter_t580023905, ___emotions_6)); }
	inline List_1_t2405724827 * get_emotions_6() const { return ___emotions_6; }
	inline List_1_t2405724827 ** get_address_of_emotions_6() { return &___emotions_6; }
	inline void set_emotions_6(List_1_t2405724827 * value)
	{
		___emotions_6 = value;
		Il2CppCodeGenWriteBarrier(&___emotions_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
