﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// com.beetsoft.utility_old.FakeSoundManager
struct FakeSoundManager_t3415940987;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void com.beetsoft.utility_old.FakeSoundManager::.ctor()
extern "C"  void FakeSoundManager__ctor_m3408095429 (FakeSoundManager_t3415940987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.FakeSoundManager::StopAllOneShoot()
extern "C"  void FakeSoundManager_StopAllOneShoot_m4222880715 (FakeSoundManager_t3415940987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.FakeSoundManager::StopBGM()
extern "C"  void FakeSoundManager_StopBGM_m4024178041 (FakeSoundManager_t3415940987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.FakeSoundManager::StopAll()
extern "C"  void FakeSoundManager_StopAll_m11534542 (FakeSoundManager_t3415940987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.FakeSoundManager::LoadSound(System.String)
extern "C"  void FakeSoundManager_LoadSound_m600893396 (FakeSoundManager_t3415940987 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.FakeSoundManager::UnloadSound(System.String)
extern "C"  void FakeSoundManager_UnloadSound_m501197797 (FakeSoundManager_t3415940987 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.FakeSoundManager::UnloadAllSound()
extern "C"  void FakeSoundManager_UnloadAllSound_m2353575256 (FakeSoundManager_t3415940987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.FakeSoundManager::PlayBGM(System.String)
extern "C"  void FakeSoundManager_PlayBGM_m18914503 (FakeSoundManager_t3415940987 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void com.beetsoft.utility_old.FakeSoundManager::PlayOneShoot(System.String)
extern "C"  void FakeSoundManager_PlayOneShoot_m3538446442 (FakeSoundManager_t3415940987 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
