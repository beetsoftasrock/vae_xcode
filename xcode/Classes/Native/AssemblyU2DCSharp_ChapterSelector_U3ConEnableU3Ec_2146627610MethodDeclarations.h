﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterSelector/<onEnable>c__Iterator0
struct U3ConEnableU3Ec__Iterator0_t2146627610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterSelector/<onEnable>c__Iterator0::.ctor()
extern "C"  void U3ConEnableU3Ec__Iterator0__ctor_m1224491911 (U3ConEnableU3Ec__Iterator0_t2146627610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterSelector/<onEnable>c__Iterator0::MoveNext()
extern "C"  bool U3ConEnableU3Ec__Iterator0_MoveNext_m2409451189 (U3ConEnableU3Ec__Iterator0_t2146627610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterSelector/<onEnable>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3ConEnableU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1357010209 (U3ConEnableU3Ec__Iterator0_t2146627610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterSelector/<onEnable>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ConEnableU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3436064617 (U3ConEnableU3Ec__Iterator0_t2146627610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<onEnable>c__Iterator0::Dispose()
extern "C"  void U3ConEnableU3Ec__Iterator0_Dispose_m4126823984 (U3ConEnableU3Ec__Iterator0_t2146627610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<onEnable>c__Iterator0::Reset()
extern "C"  void U3ConEnableU3Ec__Iterator0_Reset_m3184163286 (U3ConEnableU3Ec__Iterator0_t2146627610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
