﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CharacterSelector
struct CharacterSelector_t2041732578;

#include "AssemblyU2DCSharp_BaseConversationController808907754.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewPracticeSceneController
struct  NewPracticeSceneController_t1536734399  : public BaseConversationController_t808907754
{
public:
	// CharacterSelector NewPracticeSceneController::characterSelector
	CharacterSelector_t2041732578 * ___characterSelector_25;

public:
	inline static int32_t get_offset_of_characterSelector_25() { return static_cast<int32_t>(offsetof(NewPracticeSceneController_t1536734399, ___characterSelector_25)); }
	inline CharacterSelector_t2041732578 * get_characterSelector_25() const { return ___characterSelector_25; }
	inline CharacterSelector_t2041732578 ** get_address_of_characterSelector_25() { return &___characterSelector_25; }
	inline void set_characterSelector_25(CharacterSelector_t2041732578 * value)
	{
		___characterSelector_25 = value;
		Il2CppCodeGenWriteBarrier(&___characterSelector_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
