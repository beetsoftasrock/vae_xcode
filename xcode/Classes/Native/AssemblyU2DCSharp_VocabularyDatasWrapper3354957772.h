﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VocabularyDatasWrapper
struct  VocabularyDatasWrapper_t3354957772  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<CommandSheet/Param> VocabularyDatasWrapper::wrappedDatas
	List_1_t3492939606 * ___wrappedDatas_0;

public:
	inline static int32_t get_offset_of_wrappedDatas_0() { return static_cast<int32_t>(offsetof(VocabularyDatasWrapper_t3354957772, ___wrappedDatas_0)); }
	inline List_1_t3492939606 * get_wrappedDatas_0() const { return ___wrappedDatas_0; }
	inline List_1_t3492939606 ** get_address_of_wrappedDatas_0() { return &___wrappedDatas_0; }
	inline void set_wrappedDatas_0(List_1_t3492939606 * value)
	{
		___wrappedDatas_0 = value;
		Il2CppCodeGenWriteBarrier(&___wrappedDatas_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
