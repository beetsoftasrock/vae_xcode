﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneController
struct SceneController_t38942716;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SceneController::.ctor()
extern "C"  void SceneController__ctor_m1093984853 (SceneController_t38942716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneController SceneController::get_Instance()
extern "C"  SceneController_t38942716 * SceneController_get_Instance_m3657729138 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneController::OnApplicationQuit()
extern "C"  void SceneController_OnApplicationQuit_m3041303103 (SceneController_t38942716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneController::GoToScene(System.String)
extern "C"  void SceneController_GoToScene_m3789103904 (SceneController_t38942716 * __this, String_t* ___SceneName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneController::GoToScene(System.Int32)
extern "C"  void SceneController_GoToScene_m1451992825 (SceneController_t38942716 * __this, int32_t ___indexScene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneController::FadeWithTime(System.Single)
extern "C"  void SceneController_FadeWithTime_m3085152925 (SceneController_t38942716 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SceneController::IEFadeWithTime(System.Single)
extern "C"  Il2CppObject * SceneController_IEFadeWithTime_m467747203 (SceneController_t38942716 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SceneController::LoadScene(System.String)
extern "C"  Il2CppObject * SceneController_LoadScene_m3418794509 (SceneController_t38942716 * __this, String_t* ___SceneName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SceneController::LoadScene(System.Int32)
extern "C"  Il2CppObject * SceneController_LoadScene_m2777835618 (SceneController_t38942716 * __this, int32_t ___indexScene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
