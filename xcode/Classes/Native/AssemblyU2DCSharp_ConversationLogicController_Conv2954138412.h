﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3398846683.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationLogicController/ConversationStateChangeEvent
struct  ConversationStateChangeEvent_t2954138412  : public UnityEvent_1_t3398846683
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
