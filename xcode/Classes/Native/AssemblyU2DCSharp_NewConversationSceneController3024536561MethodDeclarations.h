﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewConversationSceneController
struct NewConversationSceneController_t3024536561;

#include "codegen/il2cpp-codegen.h"

// System.Void NewConversationSceneController::.ctor()
extern "C"  void NewConversationSceneController__ctor_m185797562 (NewConversationSceneController_t3024536561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewConversationSceneController::LoadLevel()
extern "C"  void NewConversationSceneController_LoadLevel_m2505707972 (NewConversationSceneController_t3024536561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewConversationSceneController::LoadLevelAutoPlay()
extern "C"  void NewConversationSceneController_LoadLevelAutoPlay_m1697343763 (NewConversationSceneController_t3024536561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewConversationSceneController::AutoPlayCallback()
extern "C"  void NewConversationSceneController_AutoPlayCallback_m301813226 (NewConversationSceneController_t3024536561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewConversationSceneController::Quit()
extern "C"  void NewConversationSceneController_Quit_m3851527557 (NewConversationSceneController_t3024536561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewConversationSceneController::Awake()
extern "C"  void NewConversationSceneController_Awake_m2075864653 (NewConversationSceneController_t3024536561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
