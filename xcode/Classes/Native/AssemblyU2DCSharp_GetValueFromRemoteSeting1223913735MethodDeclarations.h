﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetValueFromRemoteSeting
struct GetValueFromRemoteSeting_t1223913735;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GetValueFromRemoteSeting1223913735.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GetValueFromRemoteSeting::.ctor()
extern "C"  void GetValueFromRemoteSeting__ctor_m3264255938 (GetValueFromRemoteSeting_t1223913735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GetValueFromRemoteSeting GetValueFromRemoteSeting::get_Instance()
extern "C"  GetValueFromRemoteSeting_t1223913735 * GetValueFromRemoteSeting_get_Instance_m3628421884 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetValueFromRemoteSeting::set_Instance(GetValueFromRemoteSeting)
extern "C"  void GetValueFromRemoteSeting_set_Instance_m2459776879 (Il2CppObject * __this /* static, unused */, GetValueFromRemoteSeting_t1223913735 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetValueFromRemoteSeting::Initialize()
extern "C"  void GetValueFromRemoteSeting_Initialize_m1759591326 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GetValueFromRemoteSeting::get_ProductID()
extern "C"  String_t* GetValueFromRemoteSeting_get_ProductID_m385850692 (GetValueFromRemoteSeting_t1223913735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetValueFromRemoteSeting::set_ProductID(System.String)
extern "C"  void GetValueFromRemoteSeting_set_ProductID_m2083532523 (GetValueFromRemoteSeting_t1223913735 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GetValueFromRemoteSeting::get_Version()
extern "C"  String_t* GetValueFromRemoteSeting_get_Version_m3456763246 (GetValueFromRemoteSeting_t1223913735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetValueFromRemoteSeting::set_Version(System.String)
extern "C"  void GetValueFromRemoteSeting_set_Version_m3431601995 (GetValueFromRemoteSeting_t1223913735 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GetValueFromRemoteSeting::get_PriceProduct()
extern "C"  String_t* GetValueFromRemoteSeting_get_PriceProduct_m2056185336 (GetValueFromRemoteSeting_t1223913735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetValueFromRemoteSeting::set_PriceProduct(System.String)
extern "C"  void GetValueFromRemoteSeting_set_PriceProduct_m1131630467 (GetValueFromRemoteSeting_t1223913735 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GetValueFromRemoteSeting::get_Url()
extern "C"  String_t* GetValueFromRemoteSeting_get_Url_m901518509 (GetValueFromRemoteSeting_t1223913735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetValueFromRemoteSeting::set_Url(System.String)
extern "C"  void GetValueFromRemoteSeting_set_Url_m914156846 (GetValueFromRemoteSeting_t1223913735 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetValueFromRemoteSeting::HandleRemoteUpdate()
extern "C"  void GetValueFromRemoteSeting_HandleRemoteUpdate_m3264976429 (GetValueFromRemoteSeting_t1223913735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
