﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Analytics.DriveableProperty
struct DriveableProperty_t2641992127;
// System.Collections.Generic.List`1<UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey>
struct List_1_t1989477525;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Analytics.DriveableProperty::.ctor()
extern "C"  void DriveableProperty__ctor_m1388005943 (DriveableProperty_t2641992127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey> UnityEngine.Analytics.DriveableProperty::get_fields()
extern "C"  List_1_t1989477525 * DriveableProperty_get_fields_m3958983649 (DriveableProperty_t2641992127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Analytics.DriveableProperty::set_fields(System.Collections.Generic.List`1<UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey>)
extern "C"  void DriveableProperty_set_fields_m2028066638 (DriveableProperty_t2641992127 * __this, List_1_t1989477525 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
