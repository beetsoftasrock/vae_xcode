﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectionButtonSelectedEvent
struct SelectionButtonSelectedEvent_t3088917173;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectionButtonSelectedEvent::.ctor()
extern "C"  void SelectionButtonSelectedEvent__ctor_m2195863328 (SelectionButtonSelectedEvent_t3088917173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
