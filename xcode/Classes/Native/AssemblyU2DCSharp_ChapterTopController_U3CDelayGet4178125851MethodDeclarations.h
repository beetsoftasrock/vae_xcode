﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1
struct U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::.ctor()
extern "C"  void U3CDelayGetPopupSelectModeU3Ec__Iterator1__ctor_m1320652454 (U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::MoveNext()
extern "C"  bool U3CDelayGetPopupSelectModeU3Ec__Iterator1_MoveNext_m653493978 (U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayGetPopupSelectModeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m919149570 (U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayGetPopupSelectModeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4084230538 (U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::Dispose()
extern "C"  void U3CDelayGetPopupSelectModeU3Ec__Iterator1_Dispose_m1417205521 (U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::Reset()
extern "C"  void U3CDelayGetPopupSelectModeU3Ec__Iterator1_Reset_m3286008215 (U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
