﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResetData
struct ResetData_t4114871043;
// ResetDataResponse
struct ResetDataResponse_t2526516544;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResetDataResponse2526516544.h"

// System.Void ResetData::.ctor()
extern "C"  void ResetData__ctor_m1957432850 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResetData::get_isResetDone()
extern "C"  bool ResetData_get_isResetDone_m3044215748 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::set_isResetDone(System.Boolean)
extern "C"  void ResetData_set_isResetDone_m4022663067 (ResetData_t4114871043 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::Start()
extern "C"  void ResetData_Start_m3923283358 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::StartReset()
extern "C"  void ResetData_StartReset_m3026008663 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::ResetDone()
extern "C"  void ResetData_ResetDone_m168581927 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::OnOffResetData()
extern "C"  void ResetData_OnOffResetData_m3914731565 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::ShowLastConfirm()
extern "C"  void ResetData_ShowLastConfirm_m3674740169 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::OnLastConfirmNotPassed()
extern "C"  void ResetData_OnLastConfirmNotPassed_m3385600704 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::PostResetURL()
extern "C"  void ResetData_PostResetURL_m2432613182 (ResetData_t4114871043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::<PostResetURL>m__0(ResetDataResponse)
extern "C"  void ResetData_U3CPostResetURLU3Em__0_m922817309 (ResetData_t4114871043 * __this, ResetDataResponse_t2526516544 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetData::<PostResetURL>m__1()
extern "C"  void ResetData_U3CPostResetURLU3Em__1_m839053876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
