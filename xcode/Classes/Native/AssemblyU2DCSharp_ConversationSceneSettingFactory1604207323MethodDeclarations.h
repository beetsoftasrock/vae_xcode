﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSceneSettingFactory
struct ConversationSceneSettingFactory_t1604207323;
// ConversationSceneSetting
struct ConversationSceneSetting_t1311883407;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ConversationSceneSettingFactory::.ctor()
extern "C"  void ConversationSceneSettingFactory__ctor_m1194637678 (ConversationSceneSettingFactory_t1604207323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationSceneSetting ConversationSceneSettingFactory::GetConversationSceneSetting(System.String,System.Int32)
extern "C"  ConversationSceneSetting_t1311883407 * ConversationSceneSettingFactory_GetConversationSceneSetting_m3000540062 (Il2CppObject * __this /* static, unused */, String_t* ___lessonName0, int32_t ___chapter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
