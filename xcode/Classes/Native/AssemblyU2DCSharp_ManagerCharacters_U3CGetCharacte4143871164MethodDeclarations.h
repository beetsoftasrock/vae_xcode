﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ManagerCharacters/<GetCharacter>c__AnonStorey0
struct U3CGetCharacterU3Ec__AnonStorey0_t4143871164;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void ManagerCharacters/<GetCharacter>c__AnonStorey0::.ctor()
extern "C"  void U3CGetCharacterU3Ec__AnonStorey0__ctor_m3497327491 (U3CGetCharacterU3Ec__AnonStorey0_t4143871164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ManagerCharacters/<GetCharacter>c__AnonStorey0::<>m__0(UnityEngine.Transform)
extern "C"  bool U3CGetCharacterU3Ec__AnonStorey0_U3CU3Em__0_m4197363235 (U3CGetCharacterU3Ec__AnonStorey0_t4143871164 * __this, Transform_t3275118058 * ___character0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
