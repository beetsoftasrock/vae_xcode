﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3951188146MethodDeclarations.h"

// System.Void System.Comparison`1<Listening1QuestionData>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3419367691(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t347840579 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2929820459_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Listening1QuestionData>::Invoke(T,T)
#define Comparison_1_Invoke_m1846377245(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t347840579 *, Listening1QuestionData_t3381069024 *, Listening1QuestionData_t3381069024 *, const MethodInfo*))Comparison_1_Invoke_m2798106261_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Listening1QuestionData>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m859205252(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t347840579 *, Listening1QuestionData_t3381069024 *, Listening1QuestionData_t3381069024 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1817828810_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Listening1QuestionData>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m775396007(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t347840579 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m1056665895_gshared)(__this, ___result0, method)
