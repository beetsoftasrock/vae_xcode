﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Popup
struct  Popup_t161311578  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Popup::txtContent
	Text_t356221433 * ___txtContent_2;
	// UnityEngine.UI.Text Popup::txtSubContent
	Text_t356221433 * ___txtSubContent_3;

public:
	inline static int32_t get_offset_of_txtContent_2() { return static_cast<int32_t>(offsetof(Popup_t161311578, ___txtContent_2)); }
	inline Text_t356221433 * get_txtContent_2() const { return ___txtContent_2; }
	inline Text_t356221433 ** get_address_of_txtContent_2() { return &___txtContent_2; }
	inline void set_txtContent_2(Text_t356221433 * value)
	{
		___txtContent_2 = value;
		Il2CppCodeGenWriteBarrier(&___txtContent_2, value);
	}

	inline static int32_t get_offset_of_txtSubContent_3() { return static_cast<int32_t>(offsetof(Popup_t161311578, ___txtSubContent_3)); }
	inline Text_t356221433 * get_txtSubContent_3() const { return ___txtSubContent_3; }
	inline Text_t356221433 ** get_address_of_txtSubContent_3() { return &___txtSubContent_3; }
	inline void set_txtSubContent_3(Text_t356221433 * value)
	{
		___txtSubContent_3 = value;
		Il2CppCodeGenWriteBarrier(&___txtSubContent_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
