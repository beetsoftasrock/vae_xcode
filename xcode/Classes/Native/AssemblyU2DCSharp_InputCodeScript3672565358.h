﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Button
struct Button_t2872111280;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputCodeScript
struct  InputCodeScript_t3672565358  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform InputCodeScript::popupID
	Transform_t3275118058 * ___popupID_2;
	// UnityEngine.UI.InputField InputCodeScript::inputCode
	InputField_t1631627530 * ___inputCode_3;
	// UnityEngine.UI.Button InputCodeScript::close
	Button_t2872111280 * ___close_4;

public:
	inline static int32_t get_offset_of_popupID_2() { return static_cast<int32_t>(offsetof(InputCodeScript_t3672565358, ___popupID_2)); }
	inline Transform_t3275118058 * get_popupID_2() const { return ___popupID_2; }
	inline Transform_t3275118058 ** get_address_of_popupID_2() { return &___popupID_2; }
	inline void set_popupID_2(Transform_t3275118058 * value)
	{
		___popupID_2 = value;
		Il2CppCodeGenWriteBarrier(&___popupID_2, value);
	}

	inline static int32_t get_offset_of_inputCode_3() { return static_cast<int32_t>(offsetof(InputCodeScript_t3672565358, ___inputCode_3)); }
	inline InputField_t1631627530 * get_inputCode_3() const { return ___inputCode_3; }
	inline InputField_t1631627530 ** get_address_of_inputCode_3() { return &___inputCode_3; }
	inline void set_inputCode_3(InputField_t1631627530 * value)
	{
		___inputCode_3 = value;
		Il2CppCodeGenWriteBarrier(&___inputCode_3, value);
	}

	inline static int32_t get_offset_of_close_4() { return static_cast<int32_t>(offsetof(InputCodeScript_t3672565358, ___close_4)); }
	inline Button_t2872111280 * get_close_4() const { return ___close_4; }
	inline Button_t2872111280 ** get_address_of_close_4() { return &___close_4; }
	inline void set_close_4(Button_t2872111280 * value)
	{
		___close_4 = value;
		Il2CppCodeGenWriteBarrier(&___close_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
