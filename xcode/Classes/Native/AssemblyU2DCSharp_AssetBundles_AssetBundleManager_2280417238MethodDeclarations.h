﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<GetAssetBundle>c__AnonStorey4
struct U3CGetAssetBundleU3Ec__AnonStorey4_t2280417238;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754.h"

// System.Void AssetBundles.AssetBundleManager/<GetAssetBundle>c__AnonStorey4::.ctor()
extern "C"  void U3CGetAssetBundleU3Ec__AnonStorey4__ctor_m789087441 (U3CGetAssetBundleU3Ec__AnonStorey4_t2280417238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager/<GetAssetBundle>c__AnonStorey4::<>m__0(UnityEngine.AssetBundle)
extern "C"  bool U3CGetAssetBundleU3Ec__AnonStorey4_U3CU3Em__0_m1076583349 (U3CGetAssetBundleU3Ec__AnonStorey4_t2280417238 * __this, AssetBundle_t2054978754 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
