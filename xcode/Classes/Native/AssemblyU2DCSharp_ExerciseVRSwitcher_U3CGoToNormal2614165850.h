﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExerciseVRSwitcher/<GoToNormalMode>c__AnonStorey0
struct  U3CGoToNormalModeU3Ec__AnonStorey0_t2614165850  : public Il2CppObject
{
public:
	// UnityEngine.Transform ExerciseVRSwitcher/<GoToNormalMode>c__AnonStorey0::parentPopupQuit
	Transform_t3275118058 * ___parentPopupQuit_0;

public:
	inline static int32_t get_offset_of_parentPopupQuit_0() { return static_cast<int32_t>(offsetof(U3CGoToNormalModeU3Ec__AnonStorey0_t2614165850, ___parentPopupQuit_0)); }
	inline Transform_t3275118058 * get_parentPopupQuit_0() const { return ___parentPopupQuit_0; }
	inline Transform_t3275118058 ** get_address_of_parentPopupQuit_0() { return &___parentPopupQuit_0; }
	inline void set_parentPopupQuit_0(Transform_t3275118058 * value)
	{
		___parentPopupQuit_0 = value;
		Il2CppCodeGenWriteBarrier(&___parentPopupQuit_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
