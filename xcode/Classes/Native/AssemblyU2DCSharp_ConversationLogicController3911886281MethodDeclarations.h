﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationLogicController
struct ConversationLogicController_t3911886281;
// System.Collections.Generic.List`1<ConversationTalkData>
struct List_1_t939419437;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Action
struct Action_t3226471752;
// ConversationTalkData
struct ConversationTalkData_t1570298305;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationLogicController_Conv3360496668.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"

// System.Void ConversationLogicController::.ctor()
extern "C"  void ConversationLogicController__ctor_m231985494 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ConversationLogicController::get_startPlayTime()
extern "C"  float ConversationLogicController_get_startPlayTime_m3279284528 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::set_startPlayTime(System.Single)
extern "C"  void ConversationLogicController_set_startPlayTime_m550302921 (ConversationLogicController_t3911886281 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ConversationTalkData> ConversationLogicController::get_playerTalkDatas()
extern "C"  List_1_t939419437 * ConversationLogicController_get_playerTalkDatas_m1684119849 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::set_playerTalkDatas(System.Collections.Generic.List`1<ConversationTalkData>)
extern "C"  void ConversationLogicController_set_playerTalkDatas_m4189561904 (ConversationLogicController_t3911886281 * __this, List_1_t939419437 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationLogicController/ConversationState ConversationLogicController::get_state()
extern "C"  int32_t ConversationLogicController_get_state_m3933088683 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::set_state(ConversationLogicController/ConversationState)
extern "C"  void ConversationLogicController_set_state_m965990434 (ConversationLogicController_t3911886281 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::SetStartState()
extern "C"  void ConversationLogicController_SetStartState_m1378901017 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::SetConversationConfig(System.String,System.String,UnityEngine.Transform,System.String,System.String,System.String,System.String)
extern "C"  void ConversationLogicController_SetConversationConfig_m1629741592 (ConversationLogicController_t3911886281 * __this, String_t* ___playerRole0, String_t* ___otherRole1, Transform_t3275118058 * ___prefabCharacter2, String_t* ___soundSavePath3, String_t* ___soundResourcePath4, String_t* ___textureResourcePath5, String_t* ___nameLesson6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::StartConversation()
extern "C"  void ConversationLogicController_StartConversation_m297932309 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::EndConversation()
extern "C"  void ConversationLogicController_EndConversation_m644859620 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::CompleteConversation()
extern "C"  void ConversationLogicController_CompleteConversation_m62373604 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::Replay()
extern "C"  void ConversationLogicController_Replay_m2565745835 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::AutoPlay(System.Action)
extern "C"  void ConversationLogicController_AutoPlay_m2387760480 (ConversationLogicController_t3911886281 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::OnEnable()
extern "C"  void ConversationLogicController_OnEnable_m2110830574 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::OnDisable()
extern "C"  void ConversationLogicController_OnDisable_m3504773813 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::NextButtonClick()
extern "C"  void ConversationLogicController_NextButtonClick_m1050919951 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationLogicController::RemoveTalkData(ConversationTalkData)
extern "C"  void ConversationLogicController_RemoveTalkData_m234560239 (ConversationLogicController_t3911886281 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ConversationTalkData> ConversationLogicController::GetUserTalkData()
extern "C"  List_1_t939419437 * ConversationLogicController_GetUserTalkData_m381046903 (ConversationLogicController_t3911886281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConversationLogicController::<GetUserTalkData>m__0(ConversationTalkData)
extern "C"  bool ConversationLogicController_U3CGetUserTalkDataU3Em__0_m4288320097 (ConversationLogicController_t3911886281 * __this, ConversationTalkData_t1570298305 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
