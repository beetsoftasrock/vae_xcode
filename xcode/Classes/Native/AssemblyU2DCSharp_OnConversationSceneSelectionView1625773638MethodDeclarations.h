﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnConversationSceneSelectionViewClickedEvent
struct OnConversationSceneSelectionViewClickedEvent_t1625773638;

#include "codegen/il2cpp-codegen.h"

// System.Void OnConversationSceneSelectionViewClickedEvent::.ctor()
extern "C"  void OnConversationSceneSelectionViewClickedEvent__ctor_m1830804507 (OnConversationSceneSelectionViewClickedEvent_t1625773638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
