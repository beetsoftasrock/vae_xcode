﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening1DatasWrapper
struct Listening1DatasWrapper_t1360134800;
// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// System.Collections.Generic.List`1<Listening1QuestionData>
struct List_1_t2750190156;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening1DatasWrapper::.ctor(System.Collections.Generic.List`1<CommandSheet/Param>)
extern "C"  void Listening1DatasWrapper__ctor_m683776353 (Listening1DatasWrapper_t1360134800 * __this, List_1_t3492939606 * ___wrappedDatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Listening1QuestionData> Listening1DatasWrapper::GetConvertedDatas()
extern "C"  List_1_t2750190156 * Listening1DatasWrapper_GetConvertedDatas_m2388104923 (Listening1DatasWrapper_t1360134800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
