﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,proto.PhoneEvent/Types/Type>
struct Dictionary_2_t538306496;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3227809934.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,proto.PhoneEvent/Types/Type>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3248657279_gshared (Enumerator_t3227809934 * __this, Dictionary_2_t538306496 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3248657279(__this, ___host0, method) ((  void (*) (Enumerator_t3227809934 *, Dictionary_2_t538306496 *, const MethodInfo*))Enumerator__ctor_m3248657279_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m600651968_gshared (Enumerator_t3227809934 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m600651968(__this, method) ((  Il2CppObject * (*) (Enumerator_t3227809934 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m600651968_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,proto.PhoneEvent/Types/Type>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1594482870_gshared (Enumerator_t3227809934 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1594482870(__this, method) ((  void (*) (Enumerator_t3227809934 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1594482870_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,proto.PhoneEvent/Types/Type>::Dispose()
extern "C"  void Enumerator_Dispose_m1000121499_gshared (Enumerator_t3227809934 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1000121499(__this, method) ((  void (*) (Enumerator_t3227809934 *, const MethodInfo*))Enumerator_Dispose_m1000121499_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,proto.PhoneEvent/Types/Type>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2092951454_gshared (Enumerator_t3227809934 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2092951454(__this, method) ((  bool (*) (Enumerator_t3227809934 *, const MethodInfo*))Enumerator_MoveNext_m2092951454_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,proto.PhoneEvent/Types/Type>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3703550746_gshared (Enumerator_t3227809934 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3703550746(__this, method) ((  int32_t (*) (Enumerator_t3227809934 *, const MethodInfo*))Enumerator_get_Current_m3703550746_gshared)(__this, method)
