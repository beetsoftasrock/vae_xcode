﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listenning1DataLoader
struct Listenning1DataLoader_t2634637697;
// Listening1DatasWrapper
struct Listening1DatasWrapper_t1360134800;
// IDatasWrapper`1<Listening1QuestionData>
struct IDatasWrapper_1_t1228861309;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Listening1DatasWrapper1360134800.h"

// System.Void Listenning1DataLoader::.ctor()
extern "C"  void Listenning1DataLoader__ctor_m1595337374 (Listenning1DataLoader_t2634637697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Listening1DatasWrapper Listenning1DataLoader::get_datasWrapper()
extern "C"  Listening1DatasWrapper_t1360134800 * Listenning1DataLoader_get_datasWrapper_m2323746056 (Listenning1DataLoader_t2634637697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listenning1DataLoader::set_datasWrapper(Listening1DatasWrapper)
extern "C"  void Listenning1DataLoader_set_datasWrapper_m155661993 (Listenning1DataLoader_t2634637697 * __this, Listening1DatasWrapper_t1360134800 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IDatasWrapper`1<Listening1QuestionData> Listenning1DataLoader::GetDatasWrapper()
extern "C"  Il2CppObject* Listenning1DataLoader_GetDatasWrapper_m120945137 (Listenning1DataLoader_t2634637697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
