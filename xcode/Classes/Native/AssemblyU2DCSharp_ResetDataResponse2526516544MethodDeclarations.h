﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResetDataResponse
struct ResetDataResponse_t2526516544;

#include "codegen/il2cpp-codegen.h"

// System.Void ResetDataResponse::.ctor()
extern "C"  void ResetDataResponse__ctor_m167650045 (ResetDataResponse_t2526516544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
