﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterSelector/<onEnable>c__Iterator0/<onEnable>c__AnonStorey5
struct U3ConEnableU3Ec__AnonStorey5_t1980318813;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterSelector/<onEnable>c__Iterator0/<onEnable>c__AnonStorey5::.ctor()
extern "C"  void U3ConEnableU3Ec__AnonStorey5__ctor_m479810586 (U3ConEnableU3Ec__AnonStorey5_t1980318813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<onEnable>c__Iterator0/<onEnable>c__AnonStorey5::<>m__0()
extern "C"  void U3ConEnableU3Ec__AnonStorey5_U3CU3Em__0_m4069251079 (U3ConEnableU3Ec__AnonStorey5_t1980318813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
