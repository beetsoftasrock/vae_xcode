﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionChecker
struct VersionChecker_t1395544065;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void VersionChecker::.ctor()
extern "C"  void VersionChecker__ctor_m3562904670 (VersionChecker_t1395544065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionChecker::get_IsCheckDone()
extern "C"  bool VersionChecker_get_IsCheckDone_m288283761 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionChecker::set_IsCheckDone(System.Boolean)
extern "C"  void VersionChecker_set_IsCheckDone_m3874377468 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionChecker::Initialize()
extern "C"  void VersionChecker_Initialize_m3546292294 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VersionChecker::coroutineCheck()
extern "C"  Il2CppObject * VersionChecker_coroutineCheck_m3245285318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionChecker::isOldVersion(System.String,System.String)
extern "C"  bool VersionChecker_isOldVersion_m1153327151 (Il2CppObject * __this /* static, unused */, String_t* ___current0, String_t* ___required1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionChecker::checksUpdate()
extern "C"  void VersionChecker_checksUpdate_m3352679064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
