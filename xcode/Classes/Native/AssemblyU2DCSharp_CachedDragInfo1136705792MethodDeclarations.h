﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CachedDragInfo
struct CachedDragInfo_t1136705792;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void CachedDragInfo::.ctor(System.Single,UnityEngine.Vector2)
extern "C"  void CachedDragInfo__ctor_m847878890 (CachedDragInfo_t1136705792 * __this, float ___time0, Vector2_t2243707579  ___mousePosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
