﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Predicate`1<System.String>
struct Predicate_1_t472190348;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterLoader
struct  ChapterLoader_t1965880082  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityEvent ChapterLoader::OnLoadedTermsOfUse
	UnityEvent_t408735097 * ___OnLoadedTermsOfUse_2;
	// UnityEngine.Events.UnityEvent ChapterLoader::OnLoadedMyPageData
	UnityEvent_t408735097 * ___OnLoadedMyPageData_3;

public:
	inline static int32_t get_offset_of_OnLoadedTermsOfUse_2() { return static_cast<int32_t>(offsetof(ChapterLoader_t1965880082, ___OnLoadedTermsOfUse_2)); }
	inline UnityEvent_t408735097 * get_OnLoadedTermsOfUse_2() const { return ___OnLoadedTermsOfUse_2; }
	inline UnityEvent_t408735097 ** get_address_of_OnLoadedTermsOfUse_2() { return &___OnLoadedTermsOfUse_2; }
	inline void set_OnLoadedTermsOfUse_2(UnityEvent_t408735097 * value)
	{
		___OnLoadedTermsOfUse_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnLoadedTermsOfUse_2, value);
	}

	inline static int32_t get_offset_of_OnLoadedMyPageData_3() { return static_cast<int32_t>(offsetof(ChapterLoader_t1965880082, ___OnLoadedMyPageData_3)); }
	inline UnityEvent_t408735097 * get_OnLoadedMyPageData_3() const { return ___OnLoadedMyPageData_3; }
	inline UnityEvent_t408735097 ** get_address_of_OnLoadedMyPageData_3() { return &___OnLoadedMyPageData_3; }
	inline void set_OnLoadedMyPageData_3(UnityEvent_t408735097 * value)
	{
		___OnLoadedMyPageData_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnLoadedMyPageData_3, value);
	}
};

struct ChapterLoader_t1965880082_StaticFields
{
public:
	// System.Action`1<System.String> ChapterLoader::<>f__am$cache0
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache0_4;
	// System.Predicate`1<System.String> ChapterLoader::<>f__am$cache1
	Predicate_1_t472190348 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(ChapterLoader_t1965880082_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(ChapterLoader_t1965880082_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Predicate_1_t472190348 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Predicate_1_t472190348 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Predicate_1_t472190348 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
