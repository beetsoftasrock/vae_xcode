﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey>
struct List_1_t1989477525;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.DriveableProperty
struct  DriveableProperty_t2641992127  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.DriveableProperty/FieldWithRemoteSettingsKey> UnityEngine.Analytics.DriveableProperty::m_Fields
	List_1_t1989477525 * ___m_Fields_0;

public:
	inline static int32_t get_offset_of_m_Fields_0() { return static_cast<int32_t>(offsetof(DriveableProperty_t2641992127, ___m_Fields_0)); }
	inline List_1_t1989477525 * get_m_Fields_0() const { return ___m_Fields_0; }
	inline List_1_t1989477525 ** get_address_of_m_Fields_0() { return &___m_Fields_0; }
	inline void set_m_Fields_0(List_1_t1989477525 * value)
	{
		___m_Fields_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Fields_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
