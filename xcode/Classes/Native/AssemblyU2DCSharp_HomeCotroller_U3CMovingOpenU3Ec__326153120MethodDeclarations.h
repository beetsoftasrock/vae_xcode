﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeCotroller/<MovingOpen>c__Iterator1
struct U3CMovingOpenU3Ec__Iterator1_t326153120;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeCotroller/<MovingOpen>c__Iterator1::.ctor()
extern "C"  void U3CMovingOpenU3Ec__Iterator1__ctor_m1998947979 (U3CMovingOpenU3Ec__Iterator1_t326153120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HomeCotroller/<MovingOpen>c__Iterator1::MoveNext()
extern "C"  bool U3CMovingOpenU3Ec__Iterator1_MoveNext_m3242337369 (U3CMovingOpenU3Ec__Iterator1_t326153120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HomeCotroller/<MovingOpen>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMovingOpenU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3961919037 (U3CMovingOpenU3Ec__Iterator1_t326153120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HomeCotroller/<MovingOpen>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMovingOpenU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3684491381 (U3CMovingOpenU3Ec__Iterator1_t326153120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller/<MovingOpen>c__Iterator1::Dispose()
extern "C"  void U3CMovingOpenU3Ec__Iterator1_Dispose_m4280978134 (U3CMovingOpenU3Ec__Iterator1_t326153120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller/<MovingOpen>c__Iterator1::Reset()
extern "C"  void U3CMovingOpenU3Ec__Iterator1_Reset_m280207324 (U3CMovingOpenU3Ec__Iterator1_t326153120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
