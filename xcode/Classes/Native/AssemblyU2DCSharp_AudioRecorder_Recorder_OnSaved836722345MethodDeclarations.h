﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioRecorder.Recorder/OnSaved
struct OnSaved_t836722345;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void AudioRecorder.Recorder/OnSaved::.ctor(System.Object,System.IntPtr)
extern "C"  void OnSaved__ctor_m33672078 (OnSaved_t836722345 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder/OnSaved::Invoke(System.String)
extern "C"  void OnSaved_Invoke_m4005045894 (OnSaved_t836722345 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AudioRecorder.Recorder/OnSaved::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnSaved_BeginInvoke_m2224300837 (OnSaved_t836722345 * __this, String_t* ___path0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecorder.Recorder/OnSaved::EndInvoke(System.IAsyncResult)
extern "C"  void OnSaved_EndInvoke_m734965616 (OnSaved_t836722345 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
