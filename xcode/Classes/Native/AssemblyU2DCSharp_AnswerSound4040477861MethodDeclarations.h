﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnswerSound
struct AnswerSound_t4040477861;

#include "codegen/il2cpp-codegen.h"

// System.Void AnswerSound::.ctor()
extern "C"  void AnswerSound__ctor_m3594626020 (AnswerSound_t4040477861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnswerSound::Play(System.Boolean)
extern "C"  void AnswerSound_Play_m88223575 (AnswerSound_t4040477861 * __this, bool ___isCorrect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnswerSound::PlayCorrect()
extern "C"  void AnswerSound_PlayCorrect_m729351332 (AnswerSound_t4040477861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnswerSound::PlayInCorrect()
extern "C"  void AnswerSound_PlayInCorrect_m1535061677 (AnswerSound_t4040477861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnswerSound::Awake()
extern "C"  void AnswerSound_Awake_m3872205141 (AnswerSound_t4040477861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
