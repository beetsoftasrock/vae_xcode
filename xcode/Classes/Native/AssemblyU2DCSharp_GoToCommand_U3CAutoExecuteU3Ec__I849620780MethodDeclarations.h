﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToCommand/<AutoExecute>c__Iterator0
struct U3CAutoExecuteU3Ec__Iterator0_t849620780;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GoToCommand/<AutoExecute>c__Iterator0::.ctor()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0__ctor_m2971413671 (U3CAutoExecuteU3Ec__Iterator0_t849620780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoToCommand/<AutoExecute>c__Iterator0::MoveNext()
extern "C"  bool U3CAutoExecuteU3Ec__Iterator0_MoveNext_m2595871293 (U3CAutoExecuteU3Ec__Iterator0_t849620780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoToCommand/<AutoExecute>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAutoExecuteU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1235996349 (U3CAutoExecuteU3Ec__Iterator0_t849620780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoToCommand/<AutoExecute>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAutoExecuteU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2043236757 (U3CAutoExecuteU3Ec__Iterator0_t849620780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToCommand/<AutoExecute>c__Iterator0::Dispose()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0_Dispose_m1790168974 (U3CAutoExecuteU3Ec__Iterator0_t849620780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToCommand/<AutoExecute>c__Iterator0::Reset()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0_Reset_m1829724492 (U3CAutoExecuteU3Ec__Iterator0_t849620780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
