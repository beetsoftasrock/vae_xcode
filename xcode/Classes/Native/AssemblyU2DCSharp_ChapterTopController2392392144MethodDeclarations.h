﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterTopController
struct ChapterTopController_t2392392144;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PopupOpener
struct PopupOpener_t1646050995;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// LearningLogResponse
struct LearningLogResponse_t112900141;
// LearningLogData[]
struct LearningLogDataU5BU5D_t3295896437;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PopupOpener1646050995.h"
#include "AssemblyU2DCSharp_SceneName904752595.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_LearningLogResponse112900141.h"

// System.Void ChapterTopController::.ctor()
extern "C"  void ChapterTopController__ctor_m2730889517 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::Setup()
extern "C"  void ChapterTopController_Setup_m3974526098 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::Awake()
extern "C"  void ChapterTopController_Awake_m2103749824 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::Start()
extern "C"  void ChapterTopController_Start_m1736816093 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::UnloadBundleData()
extern "C"  void ChapterTopController_UnloadBundleData_m2748451264 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChapterTopController::unloadBundleData()
extern "C"  Il2CppObject * ChapterTopController_unloadBundleData_m75503116 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::InitItemViews()
extern "C"  void ChapterTopController_InitItemViews_m4103822238 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::InitBottomView()
extern "C"  void ChapterTopController_InitBottomView_m2202281625 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChapterTopController::DelayGetPopupSelectMode(PopupOpener,SceneName,System.String)
extern "C"  Il2CppObject * ChapterTopController_DelayGetPopupSelectMode_m4136402527 (ChapterTopController_t2392392144 * __this, PopupOpener_t1646050995 * ___popupOpener0, int32_t ___sceneName1, String_t* ___normalScene2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::SelectMode(UnityEngine.GameObject,SceneName,System.String)
extern "C"  void ChapterTopController_SelectMode_m3726805157 (ChapterTopController_t2392392144 * __this, GameObject_t1756533147 * ___popupSelectMode0, int32_t ___sceneName1, String_t* ___normalScene2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::GoToScene(SceneName)
extern "C"  void ChapterTopController_GoToScene_m3840397913 (ChapterTopController_t2392392144 * __this, int32_t ___sceneName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::UpdatePercent()
extern "C"  void ChapterTopController_UpdatePercent_m2259527439 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::GetLearningData(System.String,System.Int32,System.DateTime,System.DateTime)
extern "C"  void ChapterTopController_GetLearningData_m2849892080 (ChapterTopController_t2392392144 * __this, String_t* ___uuid0, int32_t ___chapter1, DateTime_t693205669  ___startDate2, DateTime_t693205669  ___endDate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::HandleLearningDataResponse(LearningLogResponse)
extern "C"  void ChapterTopController_HandleLearningDataResponse_m2689962991 (ChapterTopController_t2392392144 * __this, LearningLogResponse_t112900141 * ___learningLogResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::UpdateLearningDatas(LearningLogData[])
extern "C"  void ChapterTopController_UpdateLearningDatas_m3506937629 (ChapterTopController_t2392392144 * __this, LearningLogDataU5BU5D_t3295896437* ___learningLogs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<InitBottomView>m__0()
extern "C"  void ChapterTopController_U3CInitBottomViewU3Em__0_m3045209834 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<InitBottomView>m__1()
extern "C"  void ChapterTopController_U3CInitBottomViewU3Em__1_m296123503 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<InitBottomView>m__2()
extern "C"  void ChapterTopController_U3CInitBottomViewU3Em__2_m3327534836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<InitBottomView>m__3()
extern "C"  void ChapterTopController_U3CInitBottomViewU3Em__3_m578448505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<InitBottomView>m__4()
extern "C"  void ChapterTopController_U3CInitBottomViewU3Em__4_m3609859838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<InitBottomView>m__5()
extern "C"  void ChapterTopController_U3CInitBottomViewU3Em__5_m860773507 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<InitBottomView>m__6()
extern "C"  void ChapterTopController_U3CInitBottomViewU3Em__6_m3892184840 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<InitBottomView>m__7()
extern "C"  void ChapterTopController_U3CInitBottomViewU3Em__7_m1143098509 (ChapterTopController_t2392392144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterTopController::<GetLearningData>m__8()
extern "C"  void ChapterTopController_U3CGetLearningDataU3Em__8_m914471252 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
