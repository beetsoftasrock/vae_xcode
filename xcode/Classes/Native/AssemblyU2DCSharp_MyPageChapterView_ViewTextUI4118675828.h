﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageChapterView/ViewTextUI
struct  ViewTextUI_t4118675828 
{
public:
	// System.String MyPageChapterView/ViewTextUI::name
	String_t* ___name_0;
	// UnityEngine.UI.Text[] MyPageChapterView/ViewTextUI::txtNamePracticed
	TextU5BU5D_t4216439300* ___txtNamePracticed_1;
	// UnityEngine.UI.Text MyPageChapterView/ViewTextUI::txtPercentPracticed
	Text_t356221433 * ___txtPercentPracticed_2;
	// UnityEngine.UI.Image MyPageChapterView/ViewTextUI::imgStatus
	Image_t2042527209 * ___imgStatus_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ViewTextUI_t4118675828, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_txtNamePracticed_1() { return static_cast<int32_t>(offsetof(ViewTextUI_t4118675828, ___txtNamePracticed_1)); }
	inline TextU5BU5D_t4216439300* get_txtNamePracticed_1() const { return ___txtNamePracticed_1; }
	inline TextU5BU5D_t4216439300** get_address_of_txtNamePracticed_1() { return &___txtNamePracticed_1; }
	inline void set_txtNamePracticed_1(TextU5BU5D_t4216439300* value)
	{
		___txtNamePracticed_1 = value;
		Il2CppCodeGenWriteBarrier(&___txtNamePracticed_1, value);
	}

	inline static int32_t get_offset_of_txtPercentPracticed_2() { return static_cast<int32_t>(offsetof(ViewTextUI_t4118675828, ___txtPercentPracticed_2)); }
	inline Text_t356221433 * get_txtPercentPracticed_2() const { return ___txtPercentPracticed_2; }
	inline Text_t356221433 ** get_address_of_txtPercentPracticed_2() { return &___txtPercentPracticed_2; }
	inline void set_txtPercentPracticed_2(Text_t356221433 * value)
	{
		___txtPercentPracticed_2 = value;
		Il2CppCodeGenWriteBarrier(&___txtPercentPracticed_2, value);
	}

	inline static int32_t get_offset_of_imgStatus_3() { return static_cast<int32_t>(offsetof(ViewTextUI_t4118675828, ___imgStatus_3)); }
	inline Image_t2042527209 * get_imgStatus_3() const { return ___imgStatus_3; }
	inline Image_t2042527209 ** get_address_of_imgStatus_3() { return &___imgStatus_3; }
	inline void set_imgStatus_3(Image_t2042527209 * value)
	{
		___imgStatus_3 = value;
		Il2CppCodeGenWriteBarrier(&___imgStatus_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MyPageChapterView/ViewTextUI
struct ViewTextUI_t4118675828_marshaled_pinvoke
{
	char* ___name_0;
	TextU5BU5D_t4216439300* ___txtNamePracticed_1;
	Text_t356221433 * ___txtPercentPracticed_2;
	Image_t2042527209 * ___imgStatus_3;
};
// Native definition for COM marshalling of MyPageChapterView/ViewTextUI
struct ViewTextUI_t4118675828_marshaled_com
{
	Il2CppChar* ___name_0;
	TextU5BU5D_t4216439300* ___txtNamePracticed_1;
	Text_t356221433 * ___txtPercentPracticed_2;
	Image_t2042527209 * ___imgStatus_3;
};
