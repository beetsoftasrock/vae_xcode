﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectButton
struct SelectButton_t132497280;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void SelectButton::.ctor()
extern "C"  void SelectButton__ctor_m3854909469 (SelectButton_t132497280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color SelectButton::get_mainColor()
extern "C"  Color_t2020392075  SelectButton_get_mainColor_m621296735 (SelectButton_t132497280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectButton::set_mainColor(UnityEngine.Color)
extern "C"  void SelectButton_set_mainColor_m1418795040 (SelectButton_t132497280 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectButton::UpdateMainColor()
extern "C"  void SelectButton_UpdateMainColor_m3589302304 (SelectButton_t132497280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SelectButton::get_selected()
extern "C"  bool SelectButton_get_selected_m4189562125 (SelectButton_t132497280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectButton::set_selected(System.Boolean)
extern "C"  void SelectButton_set_selected_m1579005398 (SelectButton_t132497280 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectButton::UpdateState()
extern "C"  void SelectButton_UpdateState_m2670705787 (SelectButton_t132497280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
