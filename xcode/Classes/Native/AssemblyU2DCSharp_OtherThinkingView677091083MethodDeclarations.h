﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OtherThinkingView
struct OtherThinkingView_t677091083;

#include "codegen/il2cpp-codegen.h"

// System.Void OtherThinkingView::.ctor()
extern "C"  void OtherThinkingView__ctor_m638157638 (OtherThinkingView_t677091083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
