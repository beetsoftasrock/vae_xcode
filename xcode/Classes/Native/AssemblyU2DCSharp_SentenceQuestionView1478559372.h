﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// com.beetsoft.GUIElements.SelectableItem
struct SelectableItem_t2941161729;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// QuestionScenePopup
struct QuestionScenePopup_t792224618;
// UnityEngine.Animation
struct Animation_t2068071072;
// AnswerSound
struct AnswerSound_t4040477861;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;

#include "AssemblyU2DCSharp_BaseQuestionView79922856.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SentenceQuestionView
struct  SentenceQuestionView_t1478559372  : public BaseQuestionView_t79922856
{
public:
	// com.beetsoft.GUIElements.SelectableItem SentenceQuestionView::answer0
	SelectableItem_t2941161729 * ___answer0_3;
	// com.beetsoft.GUIElements.SelectableItem SentenceQuestionView::answer1
	SelectableItem_t2941161729 * ___answer1_4;
	// com.beetsoft.GUIElements.SelectableItem SentenceQuestionView::answer2
	SelectableItem_t2941161729 * ___answer2_5;
	// com.beetsoft.GUIElements.SelectableItem SentenceQuestionView::answer3
	SelectableItem_t2941161729 * ___answer3_6;
	// UnityEngine.UI.Text SentenceQuestionView::questionTextJP
	Text_t356221433 * ___questionTextJP_7;
	// UnityEngine.UI.Text SentenceQuestionView::questionTextEN
	Text_t356221433 * ___questionTextEN_8;
	// UnityEngine.UI.Button SentenceQuestionView::summitButton
	Button_t2872111280 * ___summitButton_9;
	// UnityEngine.UI.Button SentenceQuestionView::soundButton
	Button_t2872111280 * ___soundButton_10;
	// QuestionScenePopup SentenceQuestionView::popup
	QuestionScenePopup_t792224618 * ___popup_11;
	// UnityEngine.Animation SentenceQuestionView::anim
	Animation_t2068071072 * ___anim_12;
	// AnswerSound SentenceQuestionView::answerSound
	AnswerSound_t4040477861 * ___answerSound_13;
	// UnityEngine.AnimationClip SentenceQuestionView::animResultOpen
	AnimationClip_t3510324950 * ___animResultOpen_14;
	// UnityEngine.AnimationClip SentenceQuestionView::animResultClose
	AnimationClip_t3510324950 * ___animResultClose_15;

public:
	inline static int32_t get_offset_of_answer0_3() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___answer0_3)); }
	inline SelectableItem_t2941161729 * get_answer0_3() const { return ___answer0_3; }
	inline SelectableItem_t2941161729 ** get_address_of_answer0_3() { return &___answer0_3; }
	inline void set_answer0_3(SelectableItem_t2941161729 * value)
	{
		___answer0_3 = value;
		Il2CppCodeGenWriteBarrier(&___answer0_3, value);
	}

	inline static int32_t get_offset_of_answer1_4() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___answer1_4)); }
	inline SelectableItem_t2941161729 * get_answer1_4() const { return ___answer1_4; }
	inline SelectableItem_t2941161729 ** get_address_of_answer1_4() { return &___answer1_4; }
	inline void set_answer1_4(SelectableItem_t2941161729 * value)
	{
		___answer1_4 = value;
		Il2CppCodeGenWriteBarrier(&___answer1_4, value);
	}

	inline static int32_t get_offset_of_answer2_5() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___answer2_5)); }
	inline SelectableItem_t2941161729 * get_answer2_5() const { return ___answer2_5; }
	inline SelectableItem_t2941161729 ** get_address_of_answer2_5() { return &___answer2_5; }
	inline void set_answer2_5(SelectableItem_t2941161729 * value)
	{
		___answer2_5 = value;
		Il2CppCodeGenWriteBarrier(&___answer2_5, value);
	}

	inline static int32_t get_offset_of_answer3_6() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___answer3_6)); }
	inline SelectableItem_t2941161729 * get_answer3_6() const { return ___answer3_6; }
	inline SelectableItem_t2941161729 ** get_address_of_answer3_6() { return &___answer3_6; }
	inline void set_answer3_6(SelectableItem_t2941161729 * value)
	{
		___answer3_6 = value;
		Il2CppCodeGenWriteBarrier(&___answer3_6, value);
	}

	inline static int32_t get_offset_of_questionTextJP_7() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___questionTextJP_7)); }
	inline Text_t356221433 * get_questionTextJP_7() const { return ___questionTextJP_7; }
	inline Text_t356221433 ** get_address_of_questionTextJP_7() { return &___questionTextJP_7; }
	inline void set_questionTextJP_7(Text_t356221433 * value)
	{
		___questionTextJP_7 = value;
		Il2CppCodeGenWriteBarrier(&___questionTextJP_7, value);
	}

	inline static int32_t get_offset_of_questionTextEN_8() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___questionTextEN_8)); }
	inline Text_t356221433 * get_questionTextEN_8() const { return ___questionTextEN_8; }
	inline Text_t356221433 ** get_address_of_questionTextEN_8() { return &___questionTextEN_8; }
	inline void set_questionTextEN_8(Text_t356221433 * value)
	{
		___questionTextEN_8 = value;
		Il2CppCodeGenWriteBarrier(&___questionTextEN_8, value);
	}

	inline static int32_t get_offset_of_summitButton_9() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___summitButton_9)); }
	inline Button_t2872111280 * get_summitButton_9() const { return ___summitButton_9; }
	inline Button_t2872111280 ** get_address_of_summitButton_9() { return &___summitButton_9; }
	inline void set_summitButton_9(Button_t2872111280 * value)
	{
		___summitButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___summitButton_9, value);
	}

	inline static int32_t get_offset_of_soundButton_10() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___soundButton_10)); }
	inline Button_t2872111280 * get_soundButton_10() const { return ___soundButton_10; }
	inline Button_t2872111280 ** get_address_of_soundButton_10() { return &___soundButton_10; }
	inline void set_soundButton_10(Button_t2872111280 * value)
	{
		___soundButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___soundButton_10, value);
	}

	inline static int32_t get_offset_of_popup_11() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___popup_11)); }
	inline QuestionScenePopup_t792224618 * get_popup_11() const { return ___popup_11; }
	inline QuestionScenePopup_t792224618 ** get_address_of_popup_11() { return &___popup_11; }
	inline void set_popup_11(QuestionScenePopup_t792224618 * value)
	{
		___popup_11 = value;
		Il2CppCodeGenWriteBarrier(&___popup_11, value);
	}

	inline static int32_t get_offset_of_anim_12() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___anim_12)); }
	inline Animation_t2068071072 * get_anim_12() const { return ___anim_12; }
	inline Animation_t2068071072 ** get_address_of_anim_12() { return &___anim_12; }
	inline void set_anim_12(Animation_t2068071072 * value)
	{
		___anim_12 = value;
		Il2CppCodeGenWriteBarrier(&___anim_12, value);
	}

	inline static int32_t get_offset_of_answerSound_13() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___answerSound_13)); }
	inline AnswerSound_t4040477861 * get_answerSound_13() const { return ___answerSound_13; }
	inline AnswerSound_t4040477861 ** get_address_of_answerSound_13() { return &___answerSound_13; }
	inline void set_answerSound_13(AnswerSound_t4040477861 * value)
	{
		___answerSound_13 = value;
		Il2CppCodeGenWriteBarrier(&___answerSound_13, value);
	}

	inline static int32_t get_offset_of_animResultOpen_14() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___animResultOpen_14)); }
	inline AnimationClip_t3510324950 * get_animResultOpen_14() const { return ___animResultOpen_14; }
	inline AnimationClip_t3510324950 ** get_address_of_animResultOpen_14() { return &___animResultOpen_14; }
	inline void set_animResultOpen_14(AnimationClip_t3510324950 * value)
	{
		___animResultOpen_14 = value;
		Il2CppCodeGenWriteBarrier(&___animResultOpen_14, value);
	}

	inline static int32_t get_offset_of_animResultClose_15() { return static_cast<int32_t>(offsetof(SentenceQuestionView_t1478559372, ___animResultClose_15)); }
	inline AnimationClip_t3510324950 * get_animResultClose_15() const { return ___animResultClose_15; }
	inline AnimationClip_t3510324950 ** get_address_of_animResultClose_15() { return &___animResultClose_15; }
	inline void set_animResultClose_15(AnimationClip_t3510324950 * value)
	{
		___animResultClose_15 = value;
		Il2CppCodeGenWriteBarrier(&___animResultClose_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
