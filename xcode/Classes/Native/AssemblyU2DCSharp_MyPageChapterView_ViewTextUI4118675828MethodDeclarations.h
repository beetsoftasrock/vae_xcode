﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MyPageChapterView/ViewTextUI
struct ViewTextUI_t4118675828;
struct ViewTextUI_t4118675828_marshaled_pinvoke;
struct ViewTextUI_t4118675828_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ViewTextUI_t4118675828;
struct ViewTextUI_t4118675828_marshaled_pinvoke;

extern "C" void ViewTextUI_t4118675828_marshal_pinvoke(const ViewTextUI_t4118675828& unmarshaled, ViewTextUI_t4118675828_marshaled_pinvoke& marshaled);
extern "C" void ViewTextUI_t4118675828_marshal_pinvoke_back(const ViewTextUI_t4118675828_marshaled_pinvoke& marshaled, ViewTextUI_t4118675828& unmarshaled);
extern "C" void ViewTextUI_t4118675828_marshal_pinvoke_cleanup(ViewTextUI_t4118675828_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ViewTextUI_t4118675828;
struct ViewTextUI_t4118675828_marshaled_com;

extern "C" void ViewTextUI_t4118675828_marshal_com(const ViewTextUI_t4118675828& unmarshaled, ViewTextUI_t4118675828_marshaled_com& marshaled);
extern "C" void ViewTextUI_t4118675828_marshal_com_back(const ViewTextUI_t4118675828_marshaled_com& marshaled, ViewTextUI_t4118675828& unmarshaled);
extern "C" void ViewTextUI_t4118675828_marshal_com_cleanup(ViewTextUI_t4118675828_marshaled_com& marshaled);
