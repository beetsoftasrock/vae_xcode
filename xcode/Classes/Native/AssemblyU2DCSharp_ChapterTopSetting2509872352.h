﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterTopSetting
struct  ChapterTopSetting_t2509872352  : public Il2CppObject
{
public:
	// System.String ChapterTopSetting::chapterText
	String_t* ___chapterText_0;
	// System.String ChapterTopSetting::titleText
	String_t* ___titleText_1;
	// System.String ChapterTopSetting::subTitleText
	String_t* ___subTitleText_2;
	// System.String ChapterTopSetting::chapterInfoText
	String_t* ___chapterInfoText_3;
	// UnityEngine.Sprite ChapterTopSetting::background
	Sprite_t309593783 * ___background_4;
	// UnityEngine.Color ChapterTopSetting::mainColor
	Color_t2020392075  ___mainColor_5;
	// UnityEngine.Sprite ChapterTopSetting::buttonSprite
	Sprite_t309593783 * ___buttonSprite_6;
	// UnityEngine.Sprite ChapterTopSetting::singleButtonSprite
	Sprite_t309593783 * ___singleButtonSprite_7;
	// UnityEngine.Sprite ChapterTopSetting::gageSprite
	Sprite_t309593783 * ___gageSprite_8;
	// UnityEngine.Sprite ChapterTopSetting::autoButtonSprite
	Sprite_t309593783 * ___autoButtonSprite_9;
	// UnityEngine.Sprite ChapterTopSetting::tabBackgroundSprite
	Sprite_t309593783 * ___tabBackgroundSprite_10;

public:
	inline static int32_t get_offset_of_chapterText_0() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___chapterText_0)); }
	inline String_t* get_chapterText_0() const { return ___chapterText_0; }
	inline String_t** get_address_of_chapterText_0() { return &___chapterText_0; }
	inline void set_chapterText_0(String_t* value)
	{
		___chapterText_0 = value;
		Il2CppCodeGenWriteBarrier(&___chapterText_0, value);
	}

	inline static int32_t get_offset_of_titleText_1() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___titleText_1)); }
	inline String_t* get_titleText_1() const { return ___titleText_1; }
	inline String_t** get_address_of_titleText_1() { return &___titleText_1; }
	inline void set_titleText_1(String_t* value)
	{
		___titleText_1 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_1, value);
	}

	inline static int32_t get_offset_of_subTitleText_2() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___subTitleText_2)); }
	inline String_t* get_subTitleText_2() const { return ___subTitleText_2; }
	inline String_t** get_address_of_subTitleText_2() { return &___subTitleText_2; }
	inline void set_subTitleText_2(String_t* value)
	{
		___subTitleText_2 = value;
		Il2CppCodeGenWriteBarrier(&___subTitleText_2, value);
	}

	inline static int32_t get_offset_of_chapterInfoText_3() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___chapterInfoText_3)); }
	inline String_t* get_chapterInfoText_3() const { return ___chapterInfoText_3; }
	inline String_t** get_address_of_chapterInfoText_3() { return &___chapterInfoText_3; }
	inline void set_chapterInfoText_3(String_t* value)
	{
		___chapterInfoText_3 = value;
		Il2CppCodeGenWriteBarrier(&___chapterInfoText_3, value);
	}

	inline static int32_t get_offset_of_background_4() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___background_4)); }
	inline Sprite_t309593783 * get_background_4() const { return ___background_4; }
	inline Sprite_t309593783 ** get_address_of_background_4() { return &___background_4; }
	inline void set_background_4(Sprite_t309593783 * value)
	{
		___background_4 = value;
		Il2CppCodeGenWriteBarrier(&___background_4, value);
	}

	inline static int32_t get_offset_of_mainColor_5() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___mainColor_5)); }
	inline Color_t2020392075  get_mainColor_5() const { return ___mainColor_5; }
	inline Color_t2020392075 * get_address_of_mainColor_5() { return &___mainColor_5; }
	inline void set_mainColor_5(Color_t2020392075  value)
	{
		___mainColor_5 = value;
	}

	inline static int32_t get_offset_of_buttonSprite_6() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___buttonSprite_6)); }
	inline Sprite_t309593783 * get_buttonSprite_6() const { return ___buttonSprite_6; }
	inline Sprite_t309593783 ** get_address_of_buttonSprite_6() { return &___buttonSprite_6; }
	inline void set_buttonSprite_6(Sprite_t309593783 * value)
	{
		___buttonSprite_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_6, value);
	}

	inline static int32_t get_offset_of_singleButtonSprite_7() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___singleButtonSprite_7)); }
	inline Sprite_t309593783 * get_singleButtonSprite_7() const { return ___singleButtonSprite_7; }
	inline Sprite_t309593783 ** get_address_of_singleButtonSprite_7() { return &___singleButtonSprite_7; }
	inline void set_singleButtonSprite_7(Sprite_t309593783 * value)
	{
		___singleButtonSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___singleButtonSprite_7, value);
	}

	inline static int32_t get_offset_of_gageSprite_8() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___gageSprite_8)); }
	inline Sprite_t309593783 * get_gageSprite_8() const { return ___gageSprite_8; }
	inline Sprite_t309593783 ** get_address_of_gageSprite_8() { return &___gageSprite_8; }
	inline void set_gageSprite_8(Sprite_t309593783 * value)
	{
		___gageSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___gageSprite_8, value);
	}

	inline static int32_t get_offset_of_autoButtonSprite_9() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___autoButtonSprite_9)); }
	inline Sprite_t309593783 * get_autoButtonSprite_9() const { return ___autoButtonSprite_9; }
	inline Sprite_t309593783 ** get_address_of_autoButtonSprite_9() { return &___autoButtonSprite_9; }
	inline void set_autoButtonSprite_9(Sprite_t309593783 * value)
	{
		___autoButtonSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___autoButtonSprite_9, value);
	}

	inline static int32_t get_offset_of_tabBackgroundSprite_10() { return static_cast<int32_t>(offsetof(ChapterTopSetting_t2509872352, ___tabBackgroundSprite_10)); }
	inline Sprite_t309593783 * get_tabBackgroundSprite_10() const { return ___tabBackgroundSprite_10; }
	inline Sprite_t309593783 ** get_address_of_tabBackgroundSprite_10() { return &___tabBackgroundSprite_10; }
	inline void set_tabBackgroundSprite_10(Sprite_t309593783 * value)
	{
		___tabBackgroundSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___tabBackgroundSprite_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
