﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LearningData[]
struct LearningDataU5BU5D_t142178747;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LearningsResponse
struct  LearningsResponse_t3741862804  : public Il2CppObject
{
public:
	// System.Boolean LearningsResponse::success
	bool ___success_0;
	// LearningData[] LearningsResponse::learningData
	LearningDataU5BU5D_t142178747* ___learningData_1;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(LearningsResponse_t3741862804, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_learningData_1() { return static_cast<int32_t>(offsetof(LearningsResponse_t3741862804, ___learningData_1)); }
	inline LearningDataU5BU5D_t142178747* get_learningData_1() const { return ___learningData_1; }
	inline LearningDataU5BU5D_t142178747** get_address_of_learningData_1() { return &___learningData_1; }
	inline void set_learningData_1(LearningDataU5BU5D_t142178747* value)
	{
		___learningData_1 = value;
		Il2CppCodeGenWriteBarrier(&___learningData_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
