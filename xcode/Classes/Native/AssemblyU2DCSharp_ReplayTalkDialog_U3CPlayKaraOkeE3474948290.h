﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// KaraokeTextEffect
struct KaraokeTextEffect_t1279919556;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2
struct  U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290  : public Il2CppObject
{
public:
	// KaraokeTextEffect ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::karaokeTextEffect
	KaraokeTextEffect_t1279919556 * ___karaokeTextEffect_0;
	// System.Single ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::time
	float ___time_1;
	// System.Object ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 ReplayTalkDialog/<PlayKaraOkeEffect>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_karaokeTextEffect_0() { return static_cast<int32_t>(offsetof(U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290, ___karaokeTextEffect_0)); }
	inline KaraokeTextEffect_t1279919556 * get_karaokeTextEffect_0() const { return ___karaokeTextEffect_0; }
	inline KaraokeTextEffect_t1279919556 ** get_address_of_karaokeTextEffect_0() { return &___karaokeTextEffect_0; }
	inline void set_karaokeTextEffect_0(KaraokeTextEffect_t1279919556 * value)
	{
		___karaokeTextEffect_0 = value;
		Il2CppCodeGenWriteBarrier(&___karaokeTextEffect_0, value);
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CPlayKaraOkeEffectU3Ec__Iterator2_t3474948290, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
