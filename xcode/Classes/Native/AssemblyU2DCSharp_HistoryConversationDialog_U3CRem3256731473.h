﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationTalkData
struct ConversationTalkData_t1570298305;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HistoryConversationDialog/<RemoveHistoryItem>c__AnonStorey3
struct  U3CRemoveHistoryItemU3Ec__AnonStorey3_t3256731473  : public Il2CppObject
{
public:
	// ConversationTalkData HistoryConversationDialog/<RemoveHistoryItem>c__AnonStorey3::data
	ConversationTalkData_t1570298305 * ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(U3CRemoveHistoryItemU3Ec__AnonStorey3_t3256731473, ___data_0)); }
	inline ConversationTalkData_t1570298305 * get_data_0() const { return ___data_0; }
	inline ConversationTalkData_t1570298305 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ConversationTalkData_t1570298305 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
