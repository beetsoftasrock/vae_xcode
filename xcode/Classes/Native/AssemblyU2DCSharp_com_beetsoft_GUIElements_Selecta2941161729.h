﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// com.beetsoft.GUIElements.SelectableItem
struct  SelectableItem_t2941161729  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject com.beetsoft.GUIElements.SelectableItem::_offBG
	GameObject_t1756533147 * ____offBG_2;
	// UnityEngine.GameObject com.beetsoft.GUIElements.SelectableItem::onBG
	GameObject_t1756533147 * ___onBG_3;
	// UnityEngine.UI.Text com.beetsoft.GUIElements.SelectableItem::_text
	Text_t356221433 * ____text_4;
	// UnityEngine.UI.Image com.beetsoft.GUIElements.SelectableItem::_frame
	Image_t2042527209 * ____frame_5;
	// System.Boolean com.beetsoft.GUIElements.SelectableItem::_selected
	bool ____selected_6;
	// UnityEngine.Events.UnityEvent com.beetsoft.GUIElements.SelectableItem::onSelectItemClicked
	UnityEvent_t408735097 * ___onSelectItemClicked_7;

public:
	inline static int32_t get_offset_of__offBG_2() { return static_cast<int32_t>(offsetof(SelectableItem_t2941161729, ____offBG_2)); }
	inline GameObject_t1756533147 * get__offBG_2() const { return ____offBG_2; }
	inline GameObject_t1756533147 ** get_address_of__offBG_2() { return &____offBG_2; }
	inline void set__offBG_2(GameObject_t1756533147 * value)
	{
		____offBG_2 = value;
		Il2CppCodeGenWriteBarrier(&____offBG_2, value);
	}

	inline static int32_t get_offset_of_onBG_3() { return static_cast<int32_t>(offsetof(SelectableItem_t2941161729, ___onBG_3)); }
	inline GameObject_t1756533147 * get_onBG_3() const { return ___onBG_3; }
	inline GameObject_t1756533147 ** get_address_of_onBG_3() { return &___onBG_3; }
	inline void set_onBG_3(GameObject_t1756533147 * value)
	{
		___onBG_3 = value;
		Il2CppCodeGenWriteBarrier(&___onBG_3, value);
	}

	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(SelectableItem_t2941161729, ____text_4)); }
	inline Text_t356221433 * get__text_4() const { return ____text_4; }
	inline Text_t356221433 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_t356221433 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier(&____text_4, value);
	}

	inline static int32_t get_offset_of__frame_5() { return static_cast<int32_t>(offsetof(SelectableItem_t2941161729, ____frame_5)); }
	inline Image_t2042527209 * get__frame_5() const { return ____frame_5; }
	inline Image_t2042527209 ** get_address_of__frame_5() { return &____frame_5; }
	inline void set__frame_5(Image_t2042527209 * value)
	{
		____frame_5 = value;
		Il2CppCodeGenWriteBarrier(&____frame_5, value);
	}

	inline static int32_t get_offset_of__selected_6() { return static_cast<int32_t>(offsetof(SelectableItem_t2941161729, ____selected_6)); }
	inline bool get__selected_6() const { return ____selected_6; }
	inline bool* get_address_of__selected_6() { return &____selected_6; }
	inline void set__selected_6(bool value)
	{
		____selected_6 = value;
	}

	inline static int32_t get_offset_of_onSelectItemClicked_7() { return static_cast<int32_t>(offsetof(SelectableItem_t2941161729, ___onSelectItemClicked_7)); }
	inline UnityEvent_t408735097 * get_onSelectItemClicked_7() const { return ___onSelectItemClicked_7; }
	inline UnityEvent_t408735097 ** get_address_of_onSelectItemClicked_7() { return &___onSelectItemClicked_7; }
	inline void set_onSelectItemClicked_7(UnityEvent_t408735097 * value)
	{
		___onSelectItemClicked_7 = value;
		Il2CppCodeGenWriteBarrier(&___onSelectItemClicked_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
