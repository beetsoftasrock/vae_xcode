﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TalkCommand/<AutoExecute>c__Iterator0
struct U3CAutoExecuteU3Ec__Iterator0_t3903119071;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TalkCommand/<AutoExecute>c__Iterator0::.ctor()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0__ctor_m3328733598 (U3CAutoExecuteU3Ec__Iterator0_t3903119071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TalkCommand/<AutoExecute>c__Iterator0::MoveNext()
extern "C"  bool U3CAutoExecuteU3Ec__Iterator0_MoveNext_m3271568174 (U3CAutoExecuteU3Ec__Iterator0_t3903119071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TalkCommand/<AutoExecute>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAutoExecuteU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1313084560 (U3CAutoExecuteU3Ec__Iterator0_t3903119071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TalkCommand/<AutoExecute>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAutoExecuteU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3121580152 (U3CAutoExecuteU3Ec__Iterator0_t3903119071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkCommand/<AutoExecute>c__Iterator0::Dispose()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0_Dispose_m3680565921 (U3CAutoExecuteU3Ec__Iterator0_t3903119071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkCommand/<AutoExecute>c__Iterator0::Reset()
extern "C"  void U3CAutoExecuteU3Ec__Iterator0_Reset_m3000360323 (U3CAutoExecuteU3Ec__Iterator0_t3903119071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
