﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayOtherTalkDialog
struct PlayOtherTalkDialog_t78515058;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.Action
struct Action_t3226471752;
// KaraokeTextEffect
struct KaraokeTextEffect_t1279919556;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_KaraokeTextEffect1279919556.h"

// System.Void PlayOtherTalkDialog::.ctor()
extern "C"  void PlayOtherTalkDialog__ctor_m324190233 (PlayOtherTalkDialog_t78515058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayOtherTalkDialog::get_showSubText()
extern "C"  bool PlayOtherTalkDialog_get_showSubText_m1476800730 (PlayOtherTalkDialog_t78515058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayOtherTalkDialog::set_showSubText(System.Boolean)
extern "C"  void PlayOtherTalkDialog_set_showSubText_m1915453833 (PlayOtherTalkDialog_t78515058 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayOtherTalkDialog::PlayOtherTalkEffect(System.String,ConversationTalkData,System.Action)
extern "C"  Il2CppObject * PlayOtherTalkDialog_PlayOtherTalkEffect_m575148216 (PlayOtherTalkDialog_t78515058 * __this, String_t* ___pathName0, ConversationTalkData_t1570298305 * ___talkData1, Action_t3226471752 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayOtherTalkDialog::Play(System.String,ConversationTalkData,System.Action)
extern "C"  Il2CppObject * PlayOtherTalkDialog_Play_m2374464863 (PlayOtherTalkDialog_t78515058 * __this, String_t* ___pathName0, ConversationTalkData_t1570298305 * ___talkData1, Action_t3226471752 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayOtherTalkDialog::PlayKaraOkeEffect(KaraokeTextEffect,System.Single)
extern "C"  Il2CppObject * PlayOtherTalkDialog_PlayKaraOkeEffect_m2794112531 (PlayOtherTalkDialog_t78515058 * __this, KaraokeTextEffect_t1279919556 * ___karaokeTextEffect0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
