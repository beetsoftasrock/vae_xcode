﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultRateSlider
struct ResultRateSlider_t127081558;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultRateSlider::.ctor()
extern "C"  void ResultRateSlider__ctor_m4030207121 (ResultRateSlider_t127081558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateSlider::SetTimeRateSlider(System.Single)
extern "C"  void ResultRateSlider_SetTimeRateSlider_m3906810440 (ResultRateSlider_t127081558 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateSlider::SetTimeFitSlider(System.Single)
extern "C"  void ResultRateSlider_SetTimeFitSlider_m932015733 (ResultRateSlider_t127081558 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultRateSlider::TestSliders()
extern "C"  void ResultRateSlider_TestSliders_m9376237 (ResultRateSlider_t127081558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
