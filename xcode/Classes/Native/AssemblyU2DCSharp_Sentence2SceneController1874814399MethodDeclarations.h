﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence2SceneController
struct Sentence2SceneController_t1874814399;
// SentenceListeningSpellingOnplayQuestion
struct SentenceListeningSpellingOnplayQuestion_t2514545327;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SentenceListeningSpellingOnplayQ2514545327.h"

// System.Void Sentence2SceneController::.ctor()
extern "C"  void Sentence2SceneController__ctor_m3061941054 (Sentence2SceneController_t1874814399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SentenceListeningSpellingOnplayQuestion Sentence2SceneController::get_currentQuestion()
extern "C"  SentenceListeningSpellingOnplayQuestion_t2514545327 * Sentence2SceneController_get_currentQuestion_m3079002356 (Sentence2SceneController_t1874814399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController::set_currentQuestion(SentenceListeningSpellingOnplayQuestion)
extern "C"  void Sentence2SceneController_set_currentQuestion_m4043395713 (Sentence2SceneController_t1874814399 * __this, SentenceListeningSpellingOnplayQuestion_t2514545327 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Sentence2SceneController::Start()
extern "C"  Il2CppObject * Sentence2SceneController_Start_m915634610 (Sentence2SceneController_t1874814399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController::NextQuestion()
extern "C"  void Sentence2SceneController_NextQuestion_m2604045799 (Sentence2SceneController_t1874814399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController::StartPlay()
extern "C"  void Sentence2SceneController_StartPlay_m498951276 (Sentence2SceneController_t1874814399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController::InitData()
extern "C"  void Sentence2SceneController_InitData_m3046087774 (Sentence2SceneController_t1874814399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController::ShowResult()
extern "C"  void Sentence2SceneController_ShowResult_m158866840 (Sentence2SceneController_t1874814399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController::BackToChapterTop()
extern "C"  void Sentence2SceneController_BackToChapterTop_m2612259050 (Sentence2SceneController_t1874814399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController::<ShowResult>m__0()
extern "C"  void Sentence2SceneController_U3CShowResultU3Em__0_m2433957171 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2SceneController::<ShowResult>m__1()
extern "C"  void Sentence2SceneController_U3CShowResultU3Em__1_m2292794670 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
