﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSettingAsset
struct  ColorSettingAsset_t2075258157  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.Color ColorSettingAsset::mainColor
	Color_t2020392075  ___mainColor_2;
	// UnityEngine.Color ColorSettingAsset::transparentColor
	Color_t2020392075  ___transparentColor_3;

public:
	inline static int32_t get_offset_of_mainColor_2() { return static_cast<int32_t>(offsetof(ColorSettingAsset_t2075258157, ___mainColor_2)); }
	inline Color_t2020392075  get_mainColor_2() const { return ___mainColor_2; }
	inline Color_t2020392075 * get_address_of_mainColor_2() { return &___mainColor_2; }
	inline void set_mainColor_2(Color_t2020392075  value)
	{
		___mainColor_2 = value;
	}

	inline static int32_t get_offset_of_transparentColor_3() { return static_cast<int32_t>(offsetof(ColorSettingAsset_t2075258157, ___transparentColor_3)); }
	inline Color_t2020392075  get_transparentColor_3() const { return ___transparentColor_3; }
	inline Color_t2020392075 * get_address_of_transparentColor_3() { return &___transparentColor_3; }
	inline void set_transparentColor_3(Color_t2020392075  value)
	{
		___transparentColor_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
