﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening1QuestionView
struct Listening1QuestionView_t2345876901;
// Listening1QuestionData
struct Listening1QuestionData_t3381069024;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Listening1QuestionData3381069024.h"
#include "System_Core_System_Action3226471752.h"

// System.Void Listening1QuestionView::.ctor()
extern "C"  void Listening1QuestionView__ctor_m687673752 (Listening1QuestionView_t2345876901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1QuestionView::ClearView()
extern "C"  void Listening1QuestionView_ClearView_m955246492 (Listening1QuestionView_t2345876901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1QuestionView::ClearQuestionSelections()
extern "C"  void Listening1QuestionView_ClearQuestionSelections_m3229601486 (Listening1QuestionView_t2345876901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening1QuestionView::IsLockedInteractive()
extern "C"  bool Listening1QuestionView_IsLockedInteractive_m2969174300 (Listening1QuestionView_t2345876901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1QuestionView::InitView(Listening1QuestionData)
extern "C"  void Listening1QuestionView_InitView_m53529057 (Listening1QuestionView_t2345876901 * __this, Listening1QuestionData_t3381069024 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1QuestionView::ShowResult(System.Boolean,Listening1QuestionData,System.Action)
extern "C"  void Listening1QuestionView_ShowResult_m3057222086 (Listening1QuestionView_t2345876901 * __this, bool ___isCorrect0, Listening1QuestionData_t3381069024 * ___data1, Action_t3226471752 * ___popupClosedCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1QuestionView::LockInteractive()
extern "C"  void Listening1QuestionView_LockInteractive_m3385607483 (Listening1QuestionView_t2345876901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
