﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterResultSettingLoader
struct  ChapterResultSettingLoader_t4141859061  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ChapterResultSettingLoader::_chapter
	int32_t ____chapter_4;
	// UnityEngine.UI.Text ChapterResultSettingLoader::leftTitleText
	Text_t356221433 * ___leftTitleText_5;
	// UnityEngine.UI.Text ChapterResultSettingLoader::rightTitleText
	Text_t356221433 * ___rightTitleText_6;

public:
	inline static int32_t get_offset_of__chapter_4() { return static_cast<int32_t>(offsetof(ChapterResultSettingLoader_t4141859061, ____chapter_4)); }
	inline int32_t get__chapter_4() const { return ____chapter_4; }
	inline int32_t* get_address_of__chapter_4() { return &____chapter_4; }
	inline void set__chapter_4(int32_t value)
	{
		____chapter_4 = value;
	}

	inline static int32_t get_offset_of_leftTitleText_5() { return static_cast<int32_t>(offsetof(ChapterResultSettingLoader_t4141859061, ___leftTitleText_5)); }
	inline Text_t356221433 * get_leftTitleText_5() const { return ___leftTitleText_5; }
	inline Text_t356221433 ** get_address_of_leftTitleText_5() { return &___leftTitleText_5; }
	inline void set_leftTitleText_5(Text_t356221433 * value)
	{
		___leftTitleText_5 = value;
		Il2CppCodeGenWriteBarrier(&___leftTitleText_5, value);
	}

	inline static int32_t get_offset_of_rightTitleText_6() { return static_cast<int32_t>(offsetof(ChapterResultSettingLoader_t4141859061, ___rightTitleText_6)); }
	inline Text_t356221433 * get_rightTitleText_6() const { return ___rightTitleText_6; }
	inline Text_t356221433 ** get_address_of_rightTitleText_6() { return &___rightTitleText_6; }
	inline void set_rightTitleText_6(Text_t356221433 * value)
	{
		___rightTitleText_6 = value;
		Il2CppCodeGenWriteBarrier(&___rightTitleText_6, value);
	}
};

struct ChapterResultSettingLoader_t4141859061_StaticFields
{
public:
	// UnityEngine.Color ChapterResultSettingLoader::CHAPTER_1_MAIN_COLOR
	Color_t2020392075  ___CHAPTER_1_MAIN_COLOR_2;
	// UnityEngine.Color ChapterResultSettingLoader::CHAPTER_2_MAIN_COLOR
	Color_t2020392075  ___CHAPTER_2_MAIN_COLOR_3;

public:
	inline static int32_t get_offset_of_CHAPTER_1_MAIN_COLOR_2() { return static_cast<int32_t>(offsetof(ChapterResultSettingLoader_t4141859061_StaticFields, ___CHAPTER_1_MAIN_COLOR_2)); }
	inline Color_t2020392075  get_CHAPTER_1_MAIN_COLOR_2() const { return ___CHAPTER_1_MAIN_COLOR_2; }
	inline Color_t2020392075 * get_address_of_CHAPTER_1_MAIN_COLOR_2() { return &___CHAPTER_1_MAIN_COLOR_2; }
	inline void set_CHAPTER_1_MAIN_COLOR_2(Color_t2020392075  value)
	{
		___CHAPTER_1_MAIN_COLOR_2 = value;
	}

	inline static int32_t get_offset_of_CHAPTER_2_MAIN_COLOR_3() { return static_cast<int32_t>(offsetof(ChapterResultSettingLoader_t4141859061_StaticFields, ___CHAPTER_2_MAIN_COLOR_3)); }
	inline Color_t2020392075  get_CHAPTER_2_MAIN_COLOR_3() const { return ___CHAPTER_2_MAIN_COLOR_3; }
	inline Color_t2020392075 * get_address_of_CHAPTER_2_MAIN_COLOR_3() { return &___CHAPTER_2_MAIN_COLOR_3; }
	inline void set_CHAPTER_2_MAIN_COLOR_3(Color_t2020392075  value)
	{
		___CHAPTER_2_MAIN_COLOR_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
