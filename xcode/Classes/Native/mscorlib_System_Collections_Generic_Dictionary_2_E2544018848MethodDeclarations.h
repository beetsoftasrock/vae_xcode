﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3860655191(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2544018848 *, Dictionary_2_t1223994146 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3621685288(__this, method) ((  Il2CppObject * (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3285369846(__this, method) ((  void (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3902900725(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3402896314(__this, method) ((  Il2CppObject * (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3915246920(__this, method) ((  Il2CppObject * (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::MoveNext()
#define Enumerator_MoveNext_m4261889070(__this, method) ((  bool (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::get_Current()
#define Enumerator_get_Current_m1402731950(__this, method) ((  KeyValuePair_2_t3276306664  (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m396476115(__this, method) ((  String_t* (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2271175723(__this, method) ((  Log_t3604182180 * (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::Reset()
#define Enumerator_Reset_m917987725(__this, method) ((  void (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::VerifyState()
#define Enumerator_VerifyState_m1057587842(__this, method) ((  void (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4184100134(__this, method) ((  void (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Reporter/Log>::Dispose()
#define Enumerator_Dispose_m1745262443(__this, method) ((  void (*) (Enumerator_t2544018848 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
