﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence1DataLoader
struct Sentence1DataLoader_t1039713923;
// IDatasWrapper`1<SentenceStructureIdiomQuestionData>
struct IDatasWrapper_1_t1099524387;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Sentence1DataLoader::.ctor()
extern "C"  void Sentence1DataLoader__ctor_m1610404494 (Sentence1DataLoader_t1039713923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IDatasWrapper`1<SentenceStructureIdiomQuestionData> Sentence1DataLoader::get_datasWrapper()
extern "C"  Il2CppObject* Sentence1DataLoader_get_datasWrapper_m3780639130 (Sentence1DataLoader_t1039713923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1DataLoader::set_datasWrapper(IDatasWrapper`1<SentenceStructureIdiomQuestionData>)
extern "C"  void Sentence1DataLoader_set_datasWrapper_m574994169 (Sentence1DataLoader_t1039713923 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence1DataLoader::Awake()
extern "C"  void Sentence1DataLoader_Awake_m1912070319 (Sentence1DataLoader_t1039713923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Sentence1DataLoader::GetQuestionBundleText()
extern "C"  String_t* Sentence1DataLoader_GetQuestionBundleText_m3875309408 (Sentence1DataLoader_t1039713923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IDatasWrapper`1<SentenceStructureIdiomQuestionData> Sentence1DataLoader::GetDatasWrapper()
extern "C"  Il2CppObject* Sentence1DataLoader_GetDatasWrapper_m1672121213 (Sentence1DataLoader_t1039713923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
