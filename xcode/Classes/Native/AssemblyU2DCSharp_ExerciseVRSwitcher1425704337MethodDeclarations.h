﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExerciseVRSwitcher
struct ExerciseVRSwitcher_t1425704337;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void ExerciseVRSwitcher::.ctor()
extern "C"  void ExerciseVRSwitcher__ctor_m3264216752 (ExerciseVRSwitcher_t1425704337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher::GoToNormalMode(UnityEngine.Transform)
extern "C"  void ExerciseVRSwitcher_GoToNormalMode_m1106184470 (ExerciseVRSwitcher_t1425704337 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher::YesGoToVRMode()
extern "C"  void ExerciseVRSwitcher_YesGoToVRMode_m2570717891 (ExerciseVRSwitcher_t1425704337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher::GoToVRMode(UnityEngine.Transform)
extern "C"  void ExerciseVRSwitcher_GoToVRMode_m1413546407 (ExerciseVRSwitcher_t1425704337 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher::BackToTop()
extern "C"  void ExerciseVRSwitcher_BackToTop_m3771278983 (ExerciseVRSwitcher_t1425704337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher::QuitVRMode(UnityEngine.Transform)
extern "C"  void ExerciseVRSwitcher_QuitVRMode_m434578941 (ExerciseVRSwitcher_t1425704337 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher::YesQuitVRMode()
extern "C"  void ExerciseVRSwitcher_YesQuitVRMode_m328079409 (ExerciseVRSwitcher_t1425704337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher::NoQuitVRMode(UnityEngine.Transform)
extern "C"  void ExerciseVRSwitcher_NoQuitVRMode_m4273335966 (ExerciseVRSwitcher_t1425704337 * __this, Transform_t3275118058 * ___popupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher::<GoToNormalMode>m__0()
extern "C"  void ExerciseVRSwitcher_U3CGoToNormalModeU3Em__0_m2333136794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
