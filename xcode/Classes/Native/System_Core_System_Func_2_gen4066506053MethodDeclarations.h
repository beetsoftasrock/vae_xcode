﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"

// System.Void System.Func`2<GvrBasePointer,GvrBasePointer>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3014017874(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t4066506053 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<GvrBasePointer,GvrBasePointer>::Invoke(T)
#define Func_2_Invoke_m2660783210(__this, ___arg10, method) ((  GvrBasePointer_t2150122635 * (*) (Func_2_t4066506053 *, GvrBasePointer_t2150122635 *, const MethodInfo*))Func_2_Invoke_m3288232740_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<GvrBasePointer,GvrBasePointer>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2135587349(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t4066506053 *, GvrBasePointer_t2150122635 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4034295761_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<GvrBasePointer,GvrBasePointer>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2847862016(__this, ___result0, method) ((  GvrBasePointer_t2150122635 * (*) (Func_2_t4066506053 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1674435418_gshared)(__this, ___result0, method)
