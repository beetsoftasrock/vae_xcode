﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Images
struct  Images_t563431294  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D Images::clearImage
	Texture2D_t3542995729 * ___clearImage_0;
	// UnityEngine.Texture2D Images::collapseImage
	Texture2D_t3542995729 * ___collapseImage_1;
	// UnityEngine.Texture2D Images::clearOnNewSceneImage
	Texture2D_t3542995729 * ___clearOnNewSceneImage_2;
	// UnityEngine.Texture2D Images::showTimeImage
	Texture2D_t3542995729 * ___showTimeImage_3;
	// UnityEngine.Texture2D Images::showSceneImage
	Texture2D_t3542995729 * ___showSceneImage_4;
	// UnityEngine.Texture2D Images::userImage
	Texture2D_t3542995729 * ___userImage_5;
	// UnityEngine.Texture2D Images::showMemoryImage
	Texture2D_t3542995729 * ___showMemoryImage_6;
	// UnityEngine.Texture2D Images::softwareImage
	Texture2D_t3542995729 * ___softwareImage_7;
	// UnityEngine.Texture2D Images::dateImage
	Texture2D_t3542995729 * ___dateImage_8;
	// UnityEngine.Texture2D Images::showFpsImage
	Texture2D_t3542995729 * ___showFpsImage_9;
	// UnityEngine.Texture2D Images::infoImage
	Texture2D_t3542995729 * ___infoImage_10;
	// UnityEngine.Texture2D Images::searchImage
	Texture2D_t3542995729 * ___searchImage_11;
	// UnityEngine.Texture2D Images::closeImage
	Texture2D_t3542995729 * ___closeImage_12;
	// UnityEngine.Texture2D Images::buildFromImage
	Texture2D_t3542995729 * ___buildFromImage_13;
	// UnityEngine.Texture2D Images::systemInfoImage
	Texture2D_t3542995729 * ___systemInfoImage_14;
	// UnityEngine.Texture2D Images::graphicsInfoImage
	Texture2D_t3542995729 * ___graphicsInfoImage_15;
	// UnityEngine.Texture2D Images::backImage
	Texture2D_t3542995729 * ___backImage_16;
	// UnityEngine.Texture2D Images::logImage
	Texture2D_t3542995729 * ___logImage_17;
	// UnityEngine.Texture2D Images::warningImage
	Texture2D_t3542995729 * ___warningImage_18;
	// UnityEngine.Texture2D Images::errorImage
	Texture2D_t3542995729 * ___errorImage_19;
	// UnityEngine.Texture2D Images::barImage
	Texture2D_t3542995729 * ___barImage_20;
	// UnityEngine.Texture2D Images::button_activeImage
	Texture2D_t3542995729 * ___button_activeImage_21;
	// UnityEngine.Texture2D Images::even_logImage
	Texture2D_t3542995729 * ___even_logImage_22;
	// UnityEngine.Texture2D Images::odd_logImage
	Texture2D_t3542995729 * ___odd_logImage_23;
	// UnityEngine.Texture2D Images::selectedImage
	Texture2D_t3542995729 * ___selectedImage_24;
	// UnityEngine.GUISkin Images::reporterScrollerSkin
	GUISkin_t1436893342 * ___reporterScrollerSkin_25;

public:
	inline static int32_t get_offset_of_clearImage_0() { return static_cast<int32_t>(offsetof(Images_t563431294, ___clearImage_0)); }
	inline Texture2D_t3542995729 * get_clearImage_0() const { return ___clearImage_0; }
	inline Texture2D_t3542995729 ** get_address_of_clearImage_0() { return &___clearImage_0; }
	inline void set_clearImage_0(Texture2D_t3542995729 * value)
	{
		___clearImage_0 = value;
		Il2CppCodeGenWriteBarrier(&___clearImage_0, value);
	}

	inline static int32_t get_offset_of_collapseImage_1() { return static_cast<int32_t>(offsetof(Images_t563431294, ___collapseImage_1)); }
	inline Texture2D_t3542995729 * get_collapseImage_1() const { return ___collapseImage_1; }
	inline Texture2D_t3542995729 ** get_address_of_collapseImage_1() { return &___collapseImage_1; }
	inline void set_collapseImage_1(Texture2D_t3542995729 * value)
	{
		___collapseImage_1 = value;
		Il2CppCodeGenWriteBarrier(&___collapseImage_1, value);
	}

	inline static int32_t get_offset_of_clearOnNewSceneImage_2() { return static_cast<int32_t>(offsetof(Images_t563431294, ___clearOnNewSceneImage_2)); }
	inline Texture2D_t3542995729 * get_clearOnNewSceneImage_2() const { return ___clearOnNewSceneImage_2; }
	inline Texture2D_t3542995729 ** get_address_of_clearOnNewSceneImage_2() { return &___clearOnNewSceneImage_2; }
	inline void set_clearOnNewSceneImage_2(Texture2D_t3542995729 * value)
	{
		___clearOnNewSceneImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___clearOnNewSceneImage_2, value);
	}

	inline static int32_t get_offset_of_showTimeImage_3() { return static_cast<int32_t>(offsetof(Images_t563431294, ___showTimeImage_3)); }
	inline Texture2D_t3542995729 * get_showTimeImage_3() const { return ___showTimeImage_3; }
	inline Texture2D_t3542995729 ** get_address_of_showTimeImage_3() { return &___showTimeImage_3; }
	inline void set_showTimeImage_3(Texture2D_t3542995729 * value)
	{
		___showTimeImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___showTimeImage_3, value);
	}

	inline static int32_t get_offset_of_showSceneImage_4() { return static_cast<int32_t>(offsetof(Images_t563431294, ___showSceneImage_4)); }
	inline Texture2D_t3542995729 * get_showSceneImage_4() const { return ___showSceneImage_4; }
	inline Texture2D_t3542995729 ** get_address_of_showSceneImage_4() { return &___showSceneImage_4; }
	inline void set_showSceneImage_4(Texture2D_t3542995729 * value)
	{
		___showSceneImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___showSceneImage_4, value);
	}

	inline static int32_t get_offset_of_userImage_5() { return static_cast<int32_t>(offsetof(Images_t563431294, ___userImage_5)); }
	inline Texture2D_t3542995729 * get_userImage_5() const { return ___userImage_5; }
	inline Texture2D_t3542995729 ** get_address_of_userImage_5() { return &___userImage_5; }
	inline void set_userImage_5(Texture2D_t3542995729 * value)
	{
		___userImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___userImage_5, value);
	}

	inline static int32_t get_offset_of_showMemoryImage_6() { return static_cast<int32_t>(offsetof(Images_t563431294, ___showMemoryImage_6)); }
	inline Texture2D_t3542995729 * get_showMemoryImage_6() const { return ___showMemoryImage_6; }
	inline Texture2D_t3542995729 ** get_address_of_showMemoryImage_6() { return &___showMemoryImage_6; }
	inline void set_showMemoryImage_6(Texture2D_t3542995729 * value)
	{
		___showMemoryImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___showMemoryImage_6, value);
	}

	inline static int32_t get_offset_of_softwareImage_7() { return static_cast<int32_t>(offsetof(Images_t563431294, ___softwareImage_7)); }
	inline Texture2D_t3542995729 * get_softwareImage_7() const { return ___softwareImage_7; }
	inline Texture2D_t3542995729 ** get_address_of_softwareImage_7() { return &___softwareImage_7; }
	inline void set_softwareImage_7(Texture2D_t3542995729 * value)
	{
		___softwareImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___softwareImage_7, value);
	}

	inline static int32_t get_offset_of_dateImage_8() { return static_cast<int32_t>(offsetof(Images_t563431294, ___dateImage_8)); }
	inline Texture2D_t3542995729 * get_dateImage_8() const { return ___dateImage_8; }
	inline Texture2D_t3542995729 ** get_address_of_dateImage_8() { return &___dateImage_8; }
	inline void set_dateImage_8(Texture2D_t3542995729 * value)
	{
		___dateImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___dateImage_8, value);
	}

	inline static int32_t get_offset_of_showFpsImage_9() { return static_cast<int32_t>(offsetof(Images_t563431294, ___showFpsImage_9)); }
	inline Texture2D_t3542995729 * get_showFpsImage_9() const { return ___showFpsImage_9; }
	inline Texture2D_t3542995729 ** get_address_of_showFpsImage_9() { return &___showFpsImage_9; }
	inline void set_showFpsImage_9(Texture2D_t3542995729 * value)
	{
		___showFpsImage_9 = value;
		Il2CppCodeGenWriteBarrier(&___showFpsImage_9, value);
	}

	inline static int32_t get_offset_of_infoImage_10() { return static_cast<int32_t>(offsetof(Images_t563431294, ___infoImage_10)); }
	inline Texture2D_t3542995729 * get_infoImage_10() const { return ___infoImage_10; }
	inline Texture2D_t3542995729 ** get_address_of_infoImage_10() { return &___infoImage_10; }
	inline void set_infoImage_10(Texture2D_t3542995729 * value)
	{
		___infoImage_10 = value;
		Il2CppCodeGenWriteBarrier(&___infoImage_10, value);
	}

	inline static int32_t get_offset_of_searchImage_11() { return static_cast<int32_t>(offsetof(Images_t563431294, ___searchImage_11)); }
	inline Texture2D_t3542995729 * get_searchImage_11() const { return ___searchImage_11; }
	inline Texture2D_t3542995729 ** get_address_of_searchImage_11() { return &___searchImage_11; }
	inline void set_searchImage_11(Texture2D_t3542995729 * value)
	{
		___searchImage_11 = value;
		Il2CppCodeGenWriteBarrier(&___searchImage_11, value);
	}

	inline static int32_t get_offset_of_closeImage_12() { return static_cast<int32_t>(offsetof(Images_t563431294, ___closeImage_12)); }
	inline Texture2D_t3542995729 * get_closeImage_12() const { return ___closeImage_12; }
	inline Texture2D_t3542995729 ** get_address_of_closeImage_12() { return &___closeImage_12; }
	inline void set_closeImage_12(Texture2D_t3542995729 * value)
	{
		___closeImage_12 = value;
		Il2CppCodeGenWriteBarrier(&___closeImage_12, value);
	}

	inline static int32_t get_offset_of_buildFromImage_13() { return static_cast<int32_t>(offsetof(Images_t563431294, ___buildFromImage_13)); }
	inline Texture2D_t3542995729 * get_buildFromImage_13() const { return ___buildFromImage_13; }
	inline Texture2D_t3542995729 ** get_address_of_buildFromImage_13() { return &___buildFromImage_13; }
	inline void set_buildFromImage_13(Texture2D_t3542995729 * value)
	{
		___buildFromImage_13 = value;
		Il2CppCodeGenWriteBarrier(&___buildFromImage_13, value);
	}

	inline static int32_t get_offset_of_systemInfoImage_14() { return static_cast<int32_t>(offsetof(Images_t563431294, ___systemInfoImage_14)); }
	inline Texture2D_t3542995729 * get_systemInfoImage_14() const { return ___systemInfoImage_14; }
	inline Texture2D_t3542995729 ** get_address_of_systemInfoImage_14() { return &___systemInfoImage_14; }
	inline void set_systemInfoImage_14(Texture2D_t3542995729 * value)
	{
		___systemInfoImage_14 = value;
		Il2CppCodeGenWriteBarrier(&___systemInfoImage_14, value);
	}

	inline static int32_t get_offset_of_graphicsInfoImage_15() { return static_cast<int32_t>(offsetof(Images_t563431294, ___graphicsInfoImage_15)); }
	inline Texture2D_t3542995729 * get_graphicsInfoImage_15() const { return ___graphicsInfoImage_15; }
	inline Texture2D_t3542995729 ** get_address_of_graphicsInfoImage_15() { return &___graphicsInfoImage_15; }
	inline void set_graphicsInfoImage_15(Texture2D_t3542995729 * value)
	{
		___graphicsInfoImage_15 = value;
		Il2CppCodeGenWriteBarrier(&___graphicsInfoImage_15, value);
	}

	inline static int32_t get_offset_of_backImage_16() { return static_cast<int32_t>(offsetof(Images_t563431294, ___backImage_16)); }
	inline Texture2D_t3542995729 * get_backImage_16() const { return ___backImage_16; }
	inline Texture2D_t3542995729 ** get_address_of_backImage_16() { return &___backImage_16; }
	inline void set_backImage_16(Texture2D_t3542995729 * value)
	{
		___backImage_16 = value;
		Il2CppCodeGenWriteBarrier(&___backImage_16, value);
	}

	inline static int32_t get_offset_of_logImage_17() { return static_cast<int32_t>(offsetof(Images_t563431294, ___logImage_17)); }
	inline Texture2D_t3542995729 * get_logImage_17() const { return ___logImage_17; }
	inline Texture2D_t3542995729 ** get_address_of_logImage_17() { return &___logImage_17; }
	inline void set_logImage_17(Texture2D_t3542995729 * value)
	{
		___logImage_17 = value;
		Il2CppCodeGenWriteBarrier(&___logImage_17, value);
	}

	inline static int32_t get_offset_of_warningImage_18() { return static_cast<int32_t>(offsetof(Images_t563431294, ___warningImage_18)); }
	inline Texture2D_t3542995729 * get_warningImage_18() const { return ___warningImage_18; }
	inline Texture2D_t3542995729 ** get_address_of_warningImage_18() { return &___warningImage_18; }
	inline void set_warningImage_18(Texture2D_t3542995729 * value)
	{
		___warningImage_18 = value;
		Il2CppCodeGenWriteBarrier(&___warningImage_18, value);
	}

	inline static int32_t get_offset_of_errorImage_19() { return static_cast<int32_t>(offsetof(Images_t563431294, ___errorImage_19)); }
	inline Texture2D_t3542995729 * get_errorImage_19() const { return ___errorImage_19; }
	inline Texture2D_t3542995729 ** get_address_of_errorImage_19() { return &___errorImage_19; }
	inline void set_errorImage_19(Texture2D_t3542995729 * value)
	{
		___errorImage_19 = value;
		Il2CppCodeGenWriteBarrier(&___errorImage_19, value);
	}

	inline static int32_t get_offset_of_barImage_20() { return static_cast<int32_t>(offsetof(Images_t563431294, ___barImage_20)); }
	inline Texture2D_t3542995729 * get_barImage_20() const { return ___barImage_20; }
	inline Texture2D_t3542995729 ** get_address_of_barImage_20() { return &___barImage_20; }
	inline void set_barImage_20(Texture2D_t3542995729 * value)
	{
		___barImage_20 = value;
		Il2CppCodeGenWriteBarrier(&___barImage_20, value);
	}

	inline static int32_t get_offset_of_button_activeImage_21() { return static_cast<int32_t>(offsetof(Images_t563431294, ___button_activeImage_21)); }
	inline Texture2D_t3542995729 * get_button_activeImage_21() const { return ___button_activeImage_21; }
	inline Texture2D_t3542995729 ** get_address_of_button_activeImage_21() { return &___button_activeImage_21; }
	inline void set_button_activeImage_21(Texture2D_t3542995729 * value)
	{
		___button_activeImage_21 = value;
		Il2CppCodeGenWriteBarrier(&___button_activeImage_21, value);
	}

	inline static int32_t get_offset_of_even_logImage_22() { return static_cast<int32_t>(offsetof(Images_t563431294, ___even_logImage_22)); }
	inline Texture2D_t3542995729 * get_even_logImage_22() const { return ___even_logImage_22; }
	inline Texture2D_t3542995729 ** get_address_of_even_logImage_22() { return &___even_logImage_22; }
	inline void set_even_logImage_22(Texture2D_t3542995729 * value)
	{
		___even_logImage_22 = value;
		Il2CppCodeGenWriteBarrier(&___even_logImage_22, value);
	}

	inline static int32_t get_offset_of_odd_logImage_23() { return static_cast<int32_t>(offsetof(Images_t563431294, ___odd_logImage_23)); }
	inline Texture2D_t3542995729 * get_odd_logImage_23() const { return ___odd_logImage_23; }
	inline Texture2D_t3542995729 ** get_address_of_odd_logImage_23() { return &___odd_logImage_23; }
	inline void set_odd_logImage_23(Texture2D_t3542995729 * value)
	{
		___odd_logImage_23 = value;
		Il2CppCodeGenWriteBarrier(&___odd_logImage_23, value);
	}

	inline static int32_t get_offset_of_selectedImage_24() { return static_cast<int32_t>(offsetof(Images_t563431294, ___selectedImage_24)); }
	inline Texture2D_t3542995729 * get_selectedImage_24() const { return ___selectedImage_24; }
	inline Texture2D_t3542995729 ** get_address_of_selectedImage_24() { return &___selectedImage_24; }
	inline void set_selectedImage_24(Texture2D_t3542995729 * value)
	{
		___selectedImage_24 = value;
		Il2CppCodeGenWriteBarrier(&___selectedImage_24, value);
	}

	inline static int32_t get_offset_of_reporterScrollerSkin_25() { return static_cast<int32_t>(offsetof(Images_t563431294, ___reporterScrollerSkin_25)); }
	inline GUISkin_t1436893342 * get_reporterScrollerSkin_25() const { return ___reporterScrollerSkin_25; }
	inline GUISkin_t1436893342 ** get_address_of_reporterScrollerSkin_25() { return &___reporterScrollerSkin_25; }
	inline void set_reporterScrollerSkin_25(GUISkin_t1436893342 * value)
	{
		___reporterScrollerSkin_25 = value;
		Il2CppCodeGenWriteBarrier(&___reporterScrollerSkin_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
