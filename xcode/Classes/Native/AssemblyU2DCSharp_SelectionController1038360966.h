﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectionItem[]
struct SelectionItemU5BU5D_t463946666;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionController
struct  SelectionController_t1038360966  : public MonoBehaviour_t1158329972
{
public:
	// SelectionItem[] SelectionController::selectionitems
	SelectionItemU5BU5D_t463946666* ___selectionitems_2;
	// System.String[] SelectionController::selectBranchs
	StringU5BU5D_t1642385972* ___selectBranchs_3;

public:
	inline static int32_t get_offset_of_selectionitems_2() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___selectionitems_2)); }
	inline SelectionItemU5BU5D_t463946666* get_selectionitems_2() const { return ___selectionitems_2; }
	inline SelectionItemU5BU5D_t463946666** get_address_of_selectionitems_2() { return &___selectionitems_2; }
	inline void set_selectionitems_2(SelectionItemU5BU5D_t463946666* value)
	{
		___selectionitems_2 = value;
		Il2CppCodeGenWriteBarrier(&___selectionitems_2, value);
	}

	inline static int32_t get_offset_of_selectBranchs_3() { return static_cast<int32_t>(offsetof(SelectionController_t1038360966, ___selectBranchs_3)); }
	inline StringU5BU5D_t1642385972* get_selectBranchs_3() const { return ___selectBranchs_3; }
	inline StringU5BU5D_t1642385972** get_address_of_selectBranchs_3() { return &___selectBranchs_3; }
	inline void set_selectBranchs_3(StringU5BU5D_t1642385972* value)
	{
		___selectBranchs_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectBranchs_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
