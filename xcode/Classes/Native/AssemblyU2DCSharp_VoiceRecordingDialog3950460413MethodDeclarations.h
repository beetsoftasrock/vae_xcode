﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoiceRecordingDialog
struct VoiceRecordingDialog_t3950460413;
// System.String
struct String_t;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// KaraokeTextEffect
struct KaraokeTextEffect_t1279919556;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "AssemblyU2DCSharp_KaraokeTextEffect1279919556.h"

// System.Void VoiceRecordingDialog::.ctor()
extern "C"  void VoiceRecordingDialog__ctor_m2752887808 (VoiceRecordingDialog_t3950460413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoiceRecordingDialog::get_showSubText()
extern "C"  bool VoiceRecordingDialog_get_showSubText_m2053389427 (VoiceRecordingDialog_t3950460413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordingDialog::set_showSubText(System.Boolean)
extern "C"  void VoiceRecordingDialog_set_showSubText_m2875814904 (VoiceRecordingDialog_t3950460413 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordingDialog::PlayRecordingEffect(System.String,ConversationTalkData,System.Action`2<System.String,System.String>)
extern "C"  void VoiceRecordingDialog_PlayRecordingEffect_m3046874260 (VoiceRecordingDialog_t3950460413 * __this, String_t* ___pathName0, ConversationTalkData_t1570298305 * ___talkData1, Action_2_t4234541925 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoiceRecordingDialog::Play(System.String,ConversationTalkData,System.Action`2<System.String,System.String>)
extern "C"  Il2CppObject * VoiceRecordingDialog_Play_m848896516 (VoiceRecordingDialog_t3950460413 * __this, String_t* ___pathName0, ConversationTalkData_t1570298305 * ___talkData1, Action_2_t4234541925 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoiceRecordingDialog::PlayKaraOkeEffect(KaraokeTextEffect,System.Single)
extern "C"  Il2CppObject * VoiceRecordingDialog_PlayKaraOkeEffect_m2341259894 (VoiceRecordingDialog_t3950460413 * __this, KaraokeTextEffect_t1279919556 * ___karaokeTextEffect0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
