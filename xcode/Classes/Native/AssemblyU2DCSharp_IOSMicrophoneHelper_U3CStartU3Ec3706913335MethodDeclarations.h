﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IOSMicrophoneHelper/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3706913335;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void IOSMicrophoneHelper/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m1077031390 (U3CStartU3Ec__Iterator0_t3706913335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IOSMicrophoneHelper/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2553617198 (U3CStartU3Ec__Iterator0_t3706913335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSMicrophoneHelper/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3971174376 (U3CStartU3Ec__Iterator0_t3706913335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object IOSMicrophoneHelper/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2455521856 (U3CStartU3Ec__Iterator0_t3706913335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSMicrophoneHelper/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3124558465 (U3CStartU3Ec__Iterator0_t3706913335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IOSMicrophoneHelper/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m4170864035 (U3CStartU3Ec__Iterator0_t3706913335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
