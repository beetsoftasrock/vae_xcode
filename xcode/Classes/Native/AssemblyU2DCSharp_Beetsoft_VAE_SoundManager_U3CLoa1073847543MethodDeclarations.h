﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.SoundManager/<LoadSoundBundle>c__AnonStorey0
struct U3CLoadSoundBundleU3Ec__AnonStorey0_t1073847543;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Beetsoft.VAE.SoundManager/<LoadSoundBundle>c__AnonStorey0::.ctor()
extern "C"  void U3CLoadSoundBundleU3Ec__AnonStorey0__ctor_m2449353568 (U3CLoadSoundBundleU3Ec__AnonStorey0_t1073847543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager/<LoadSoundBundle>c__AnonStorey0::<>m__0()
extern "C"  void U3CLoadSoundBundleU3Ec__AnonStorey0_U3CU3Em__0_m1678062725 (U3CLoadSoundBundleU3Ec__AnonStorey0_t1073847543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.SoundManager/<LoadSoundBundle>c__AnonStorey0::<>m__1(System.String)
extern "C"  void U3CLoadSoundBundleU3Ec__AnonStorey0_U3CU3Em__1_m1046536926 (U3CLoadSoundBundleU3Ec__AnonStorey0_t1073847543 * __this, String_t* ___errMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
