﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Sprite
struct Sprite_t309593783;
// MyPageChapterView/ViewTextUI[]
struct ViewTextUIU5BU5D_t3674121789;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageChapterView
struct  MyPageChapterView_t1534782717  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MyPageChapterView::txtChapter
	Text_t356221433 * ___txtChapter_2;
	// UnityEngine.UI.Text MyPageChapterView::txtTotalTimeComplete
	Text_t356221433 * ___txtTotalTimeComplete_3;
	// UnityEngine.UI.Text MyPageChapterView::txtRecommend
	Text_t356221433 * ___txtRecommend_4;
	// UnityEngine.UI.Button MyPageChapterView::btnStudy
	Button_t2872111280 * ___btnStudy_5;
	// UnityEngine.Color MyPageChapterView::colorIncrease
	Color_t2020392075  ___colorIncrease_6;
	// UnityEngine.Color MyPageChapterView::colorDecrease
	Color_t2020392075  ___colorDecrease_7;
	// UnityEngine.Color MyPageChapterView::colorUnchanged
	Color_t2020392075  ___colorUnchanged_8;
	// UnityEngine.Sprite MyPageChapterView::sprIncrease
	Sprite_t309593783 * ___sprIncrease_9;
	// UnityEngine.Sprite MyPageChapterView::sprDecrease
	Sprite_t309593783 * ___sprDecrease_10;
	// UnityEngine.Sprite MyPageChapterView::sprUnchanged
	Sprite_t309593783 * ___sprUnchanged_11;
	// MyPageChapterView/ViewTextUI[] MyPageChapterView::tableContent
	ViewTextUIU5BU5D_t3674121789* ___tableContent_12;

public:
	inline static int32_t get_offset_of_txtChapter_2() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___txtChapter_2)); }
	inline Text_t356221433 * get_txtChapter_2() const { return ___txtChapter_2; }
	inline Text_t356221433 ** get_address_of_txtChapter_2() { return &___txtChapter_2; }
	inline void set_txtChapter_2(Text_t356221433 * value)
	{
		___txtChapter_2 = value;
		Il2CppCodeGenWriteBarrier(&___txtChapter_2, value);
	}

	inline static int32_t get_offset_of_txtTotalTimeComplete_3() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___txtTotalTimeComplete_3)); }
	inline Text_t356221433 * get_txtTotalTimeComplete_3() const { return ___txtTotalTimeComplete_3; }
	inline Text_t356221433 ** get_address_of_txtTotalTimeComplete_3() { return &___txtTotalTimeComplete_3; }
	inline void set_txtTotalTimeComplete_3(Text_t356221433 * value)
	{
		___txtTotalTimeComplete_3 = value;
		Il2CppCodeGenWriteBarrier(&___txtTotalTimeComplete_3, value);
	}

	inline static int32_t get_offset_of_txtRecommend_4() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___txtRecommend_4)); }
	inline Text_t356221433 * get_txtRecommend_4() const { return ___txtRecommend_4; }
	inline Text_t356221433 ** get_address_of_txtRecommend_4() { return &___txtRecommend_4; }
	inline void set_txtRecommend_4(Text_t356221433 * value)
	{
		___txtRecommend_4 = value;
		Il2CppCodeGenWriteBarrier(&___txtRecommend_4, value);
	}

	inline static int32_t get_offset_of_btnStudy_5() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___btnStudy_5)); }
	inline Button_t2872111280 * get_btnStudy_5() const { return ___btnStudy_5; }
	inline Button_t2872111280 ** get_address_of_btnStudy_5() { return &___btnStudy_5; }
	inline void set_btnStudy_5(Button_t2872111280 * value)
	{
		___btnStudy_5 = value;
		Il2CppCodeGenWriteBarrier(&___btnStudy_5, value);
	}

	inline static int32_t get_offset_of_colorIncrease_6() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___colorIncrease_6)); }
	inline Color_t2020392075  get_colorIncrease_6() const { return ___colorIncrease_6; }
	inline Color_t2020392075 * get_address_of_colorIncrease_6() { return &___colorIncrease_6; }
	inline void set_colorIncrease_6(Color_t2020392075  value)
	{
		___colorIncrease_6 = value;
	}

	inline static int32_t get_offset_of_colorDecrease_7() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___colorDecrease_7)); }
	inline Color_t2020392075  get_colorDecrease_7() const { return ___colorDecrease_7; }
	inline Color_t2020392075 * get_address_of_colorDecrease_7() { return &___colorDecrease_7; }
	inline void set_colorDecrease_7(Color_t2020392075  value)
	{
		___colorDecrease_7 = value;
	}

	inline static int32_t get_offset_of_colorUnchanged_8() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___colorUnchanged_8)); }
	inline Color_t2020392075  get_colorUnchanged_8() const { return ___colorUnchanged_8; }
	inline Color_t2020392075 * get_address_of_colorUnchanged_8() { return &___colorUnchanged_8; }
	inline void set_colorUnchanged_8(Color_t2020392075  value)
	{
		___colorUnchanged_8 = value;
	}

	inline static int32_t get_offset_of_sprIncrease_9() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___sprIncrease_9)); }
	inline Sprite_t309593783 * get_sprIncrease_9() const { return ___sprIncrease_9; }
	inline Sprite_t309593783 ** get_address_of_sprIncrease_9() { return &___sprIncrease_9; }
	inline void set_sprIncrease_9(Sprite_t309593783 * value)
	{
		___sprIncrease_9 = value;
		Il2CppCodeGenWriteBarrier(&___sprIncrease_9, value);
	}

	inline static int32_t get_offset_of_sprDecrease_10() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___sprDecrease_10)); }
	inline Sprite_t309593783 * get_sprDecrease_10() const { return ___sprDecrease_10; }
	inline Sprite_t309593783 ** get_address_of_sprDecrease_10() { return &___sprDecrease_10; }
	inline void set_sprDecrease_10(Sprite_t309593783 * value)
	{
		___sprDecrease_10 = value;
		Il2CppCodeGenWriteBarrier(&___sprDecrease_10, value);
	}

	inline static int32_t get_offset_of_sprUnchanged_11() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___sprUnchanged_11)); }
	inline Sprite_t309593783 * get_sprUnchanged_11() const { return ___sprUnchanged_11; }
	inline Sprite_t309593783 ** get_address_of_sprUnchanged_11() { return &___sprUnchanged_11; }
	inline void set_sprUnchanged_11(Sprite_t309593783 * value)
	{
		___sprUnchanged_11 = value;
		Il2CppCodeGenWriteBarrier(&___sprUnchanged_11, value);
	}

	inline static int32_t get_offset_of_tableContent_12() { return static_cast<int32_t>(offsetof(MyPageChapterView_t1534782717, ___tableContent_12)); }
	inline ViewTextUIU5BU5D_t3674121789* get_tableContent_12() const { return ___tableContent_12; }
	inline ViewTextUIU5BU5D_t3674121789** get_address_of_tableContent_12() { return &___tableContent_12; }
	inline void set_tableContent_12(ViewTextUIU5BU5D_t3674121789* value)
	{
		___tableContent_12 = value;
		Il2CppCodeGenWriteBarrier(&___tableContent_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
