﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HUDFPS
struct HUDFPS_t647744412;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void HUDFPS::.ctor()
extern "C"  void HUDFPS__ctor_m2743180055 (HUDFPS_t647744412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUDFPS::Awake()
extern "C"  void HUDFPS_Awake_m852816396 (HUDFPS_t647744412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUDFPS::Start()
extern "C"  void HUDFPS_Start_m2385152411 (HUDFPS_t647744412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUDFPS::Update()
extern "C"  void HUDFPS_Update_m798695710 (HUDFPS_t647744412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HUDFPS::FPS()
extern "C"  Il2CppObject * HUDFPS_FPS_m3679108476 (HUDFPS_t647744412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUDFPS::OnGUI()
extern "C"  void HUDFPS_OnGUI_m2169413121 (HUDFPS_t647744412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUDFPS::DoMyWindow(System.Int32)
extern "C"  void HUDFPS_DoMyWindow_m3077406609 (HUDFPS_t647744412 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
