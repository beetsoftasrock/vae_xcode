﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VocabularyDataLoader
struct VocabularyDataLoader_t2031208153;
// IDatasWrapper`1<VocabularyQuestionData>
struct IDatasWrapper_1_t4163834557;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VocabularyDataLoader::.ctor()
extern "C"  void VocabularyDataLoader__ctor_m3854289710 (VocabularyDataLoader_t2031208153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IDatasWrapper`1<VocabularyQuestionData> VocabularyDataLoader::get_datasWrapper()
extern "C"  Il2CppObject* VocabularyDataLoader_get_datasWrapper_m982313494 (VocabularyDataLoader_t2031208153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyDataLoader::set_datasWrapper(IDatasWrapper`1<VocabularyQuestionData>)
extern "C"  void VocabularyDataLoader_set_datasWrapper_m1584487615 (VocabularyDataLoader_t2031208153 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VocabularyDataLoader::GetQuestionBundleText()
extern "C"  String_t* VocabularyDataLoader_GetQuestionBundleText_m2909118710 (VocabularyDataLoader_t2031208153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IDatasWrapper`1<VocabularyQuestionData> VocabularyDataLoader::GetDatasWrapper()
extern "C"  Il2CppObject* VocabularyDataLoader_GetDatasWrapper_m3561907947 (VocabularyDataLoader_t2031208153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
