﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryConversation
struct HistoryConversation_t3119154081;
// System.Collections.Generic.List`1<Dialog>
struct List_1_t747313864;
// DialogPanel
struct DialogPanel_t1568014038;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DialogPanel1568014038.h"

// System.Void HistoryConversation::.ctor()
extern "C"  void HistoryConversation__ctor_m4134769138 (HistoryConversation_t3119154081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversation::OnEnable()
extern "C"  void HistoryConversation_OnEnable_m635328434 (HistoryConversation_t3119154081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversation::Start()
extern "C"  void HistoryConversation_Start_m3007872210 (HistoryConversation_t3119154081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversation::CreateItem(System.Collections.Generic.List`1<Dialog>)
extern "C"  void HistoryConversation_CreateItem_m547457379 (HistoryConversation_t3119154081 * __this, List_1_t747313864 * ___lstDialog0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversation::SetContent(System.Collections.Generic.List`1<Dialog>,System.Int32,DialogPanel)
extern "C"  void HistoryConversation_SetContent_m2190710854 (HistoryConversation_t3119154081 * __this, List_1_t747313864 * ___lstDialog0, int32_t ___index1, DialogPanel_t1568014038 * ___dialog2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversation::ShowNextItem()
extern "C"  void HistoryConversation_ShowNextItem_m814132733 (HistoryConversation_t3119154081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversation::ShowIndexItem(System.Int32)
extern "C"  void HistoryConversation_ShowIndexItem_m392565697 (HistoryConversation_t3119154081 * __this, int32_t ___indexItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversation::EnableButtonScroll()
extern "C"  void HistoryConversation_EnableButtonScroll_m2645224126 (HistoryConversation_t3119154081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryConversation::ClearItem()
extern "C"  void HistoryConversation_ClearItem_m2934221580 (HistoryConversation_t3119154081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
