﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectionCommand/<Execute>c__Iterator1
struct U3CExecuteU3Ec__Iterator1_t191178719;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectionCommand/<Execute>c__Iterator1::.ctor()
extern "C"  void U3CExecuteU3Ec__Iterator1__ctor_m2939563242 (U3CExecuteU3Ec__Iterator1_t191178719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SelectionCommand/<Execute>c__Iterator1::MoveNext()
extern "C"  bool U3CExecuteU3Ec__Iterator1_MoveNext_m3736895718 (U3CExecuteU3Ec__Iterator1_t191178719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SelectionCommand/<Execute>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3608052710 (U3CExecuteU3Ec__Iterator1_t191178719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SelectionCommand/<Execute>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3513226926 (U3CExecuteU3Ec__Iterator1_t191178719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionCommand/<Execute>c__Iterator1::Dispose()
extern "C"  void U3CExecuteU3Ec__Iterator1_Dispose_m2205716589 (U3CExecuteU3Ec__Iterator1_t191178719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionCommand/<Execute>c__Iterator1::Reset()
extern "C"  void U3CExecuteU3Ec__Iterator1_Reset_m1258513587 (U3CExecuteU3Ec__Iterator1_t191178719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
