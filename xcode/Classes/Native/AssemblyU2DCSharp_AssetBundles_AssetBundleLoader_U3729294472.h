﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// AssetBundles.AssetBundleLoader
struct AssetBundleLoader_t639004779;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0
struct  U3CforceShowU3Ec__Iterator0_t729294472  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::<specificBundles>__0
	List_1_t1398341365 * ___U3CspecificBundlesU3E__0_0;
	// System.Collections.Generic.List`1/Enumerator<System.String> AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::$locvar0
	Enumerator_t933071039  ___U24locvar0_1;
	// AssetBundles.AssetBundleLoader AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::$this
	AssetBundleLoader_t639004779 * ___U24this_2;
	// System.Object AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CspecificBundlesU3E__0_0() { return static_cast<int32_t>(offsetof(U3CforceShowU3Ec__Iterator0_t729294472, ___U3CspecificBundlesU3E__0_0)); }
	inline List_1_t1398341365 * get_U3CspecificBundlesU3E__0_0() const { return ___U3CspecificBundlesU3E__0_0; }
	inline List_1_t1398341365 ** get_address_of_U3CspecificBundlesU3E__0_0() { return &___U3CspecificBundlesU3E__0_0; }
	inline void set_U3CspecificBundlesU3E__0_0(List_1_t1398341365 * value)
	{
		___U3CspecificBundlesU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CspecificBundlesU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CforceShowU3Ec__Iterator0_t729294472, ___U24locvar0_1)); }
	inline Enumerator_t933071039  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t933071039 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t933071039  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CforceShowU3Ec__Iterator0_t729294472, ___U24this_2)); }
	inline AssetBundleLoader_t639004779 * get_U24this_2() const { return ___U24this_2; }
	inline AssetBundleLoader_t639004779 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AssetBundleLoader_t639004779 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CforceShowU3Ec__Iterator0_t729294472, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CforceShowU3Ec__Iterator0_t729294472, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CforceShowU3Ec__Iterator0_t729294472, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
