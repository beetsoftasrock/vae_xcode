﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Log>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m354934765(__this, ___l0, method) ((  void (*) (Enumerator_t2508032986 *, List_1_t2973303312 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Log>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4155493405(__this, method) ((  void (*) (Enumerator_t2508032986 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Reporter/Log>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m576993817(__this, method) ((  Il2CppObject * (*) (Enumerator_t2508032986 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Log>::Dispose()
#define Enumerator_Dispose_m1077468276(__this, method) ((  void (*) (Enumerator_t2508032986 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Log>::VerifyState()
#define Enumerator_VerifyState_m238705811(__this, method) ((  void (*) (Enumerator_t2508032986 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Reporter/Log>::MoveNext()
#define Enumerator_MoveNext_m3109042021(__this, method) ((  bool (*) (Enumerator_t2508032986 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Reporter/Log>::get_Current()
#define Enumerator_get_Current_m590856836(__this, method) ((  Log_t3604182180 * (*) (Enumerator_t2508032986 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
