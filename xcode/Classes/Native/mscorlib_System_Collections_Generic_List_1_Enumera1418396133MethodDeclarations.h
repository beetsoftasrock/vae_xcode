﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingOnplayQuestion>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1583840562(__this, ___l0, method) ((  void (*) (Enumerator_t1418396133 *, List_1_t1883666459 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingOnplayQuestion>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3911321696(__this, method) ((  void (*) (Enumerator_t1418396133 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingOnplayQuestion>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3601228852(__this, method) ((  Il2CppObject * (*) (Enumerator_t1418396133 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingOnplayQuestion>::Dispose()
#define Enumerator_Dispose_m417751951(__this, method) ((  void (*) (Enumerator_t1418396133 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingOnplayQuestion>::VerifyState()
#define Enumerator_VerifyState_m2105752032(__this, method) ((  void (*) (Enumerator_t1418396133 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingOnplayQuestion>::MoveNext()
#define Enumerator_MoveNext_m3588181872(__this, method) ((  bool (*) (Enumerator_t1418396133 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SentenceListeningSpellingOnplayQuestion>::get_Current()
#define Enumerator_get_Current_m2959551503(__this, method) ((  SentenceListeningSpellingOnplayQuestion_t2514545327 * (*) (Enumerator_t1418396133 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
