﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2SceneController
struct Listening2SceneController_t3779648327;
// Listening2OnplayQuestion
struct Listening2OnplayQuestion_t3701806074;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Listening2OnplayQuestion3701806074.h"

// System.Void Listening2SceneController::.ctor()
extern "C"  void Listening2SceneController__ctor_m2915933550 (Listening2SceneController_t3779648327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Listening2OnplayQuestion Listening2SceneController::get_currentQuestion()
extern "C"  Listening2OnplayQuestion_t3701806074 * Listening2SceneController_get_currentQuestion_m876682435 (Listening2SceneController_t3779648327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController::set_currentQuestion(Listening2OnplayQuestion)
extern "C"  void Listening2SceneController_set_currentQuestion_m1446453786 (Listening2SceneController_t3779648327 * __this, Listening2OnplayQuestion_t3701806074 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Listening2SceneController::Start()
extern "C"  Il2CppObject * Listening2SceneController_Start_m3404945270 (Listening2SceneController_t3779648327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController::StartPlay()
extern "C"  void Listening2SceneController_StartPlay_m3956352700 (Listening2SceneController_t3779648327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController::InitData()
extern "C"  void Listening2SceneController_InitData_m2205690958 (Listening2SceneController_t3779648327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController::NextQuestion()
extern "C"  void Listening2SceneController_NextQuestion_m317840763 (Listening2SceneController_t3779648327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController::BackToChapterTop()
extern "C"  void Listening2SceneController_BackToChapterTop_m2568202314 (Listening2SceneController_t3779648327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController::ShowResult()
extern "C"  void Listening2SceneController_ShowResult_m4075523208 (Listening2SceneController_t3779648327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController::<ShowResult>m__0()
extern "C"  void Listening2SceneController_U3CShowResultU3Em__0_m2304341455 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SceneController::<ShowResult>m__1()
extern "C"  void Listening2SceneController_U3CShowResultU3Em__1_m2304341422 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
