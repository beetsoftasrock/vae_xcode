﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuHandler/<DoFade>c__Iterator1
struct U3CDoFadeU3Ec__Iterator1_t3187801445;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuHandler/<DoFade>c__Iterator1::.ctor()
extern "C"  void U3CDoFadeU3Ec__Iterator1__ctor_m1840691800 (U3CDoFadeU3Ec__Iterator1_t3187801445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MenuHandler/<DoFade>c__Iterator1::MoveNext()
extern "C"  bool U3CDoFadeU3Ec__Iterator1_MoveNext_m3133592884 (U3CDoFadeU3Ec__Iterator1_t3187801445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MenuHandler/<DoFade>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoFadeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3691389976 (U3CDoFadeU3Ec__Iterator1_t3187801445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MenuHandler/<DoFade>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoFadeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3333190112 (U3CDoFadeU3Ec__Iterator1_t3187801445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuHandler/<DoFade>c__Iterator1::Dispose()
extern "C"  void U3CDoFadeU3Ec__Iterator1_Dispose_m3002980175 (U3CDoFadeU3Ec__Iterator1_t3187801445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuHandler/<DoFade>c__Iterator1::Reset()
extern "C"  void U3CDoFadeU3Ec__Iterator1_Reset_m4176845817 (U3CDoFadeU3Ec__Iterator1_t3187801445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
