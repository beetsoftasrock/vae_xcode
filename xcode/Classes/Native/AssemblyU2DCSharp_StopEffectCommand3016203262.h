﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StopEffectCommand
struct  StopEffectCommand_t3016203262  : public ConversationCommand_t3660105836
{
public:
	// System.Int32 StopEffectCommand::_preCurrentPoint
	int32_t ____preCurrentPoint_0;
	// System.String StopEffectCommand::_imageName
	String_t* ____imageName_1;
	// System.String StopEffectCommand::_role
	String_t* ____role_2;

public:
	inline static int32_t get_offset_of__preCurrentPoint_0() { return static_cast<int32_t>(offsetof(StopEffectCommand_t3016203262, ____preCurrentPoint_0)); }
	inline int32_t get__preCurrentPoint_0() const { return ____preCurrentPoint_0; }
	inline int32_t* get_address_of__preCurrentPoint_0() { return &____preCurrentPoint_0; }
	inline void set__preCurrentPoint_0(int32_t value)
	{
		____preCurrentPoint_0 = value;
	}

	inline static int32_t get_offset_of__imageName_1() { return static_cast<int32_t>(offsetof(StopEffectCommand_t3016203262, ____imageName_1)); }
	inline String_t* get__imageName_1() const { return ____imageName_1; }
	inline String_t** get_address_of__imageName_1() { return &____imageName_1; }
	inline void set__imageName_1(String_t* value)
	{
		____imageName_1 = value;
		Il2CppCodeGenWriteBarrier(&____imageName_1, value);
	}

	inline static int32_t get_offset_of__role_2() { return static_cast<int32_t>(offsetof(StopEffectCommand_t3016203262, ____role_2)); }
	inline String_t* get__role_2() const { return ____role_2; }
	inline String_t** get_address_of__role_2() { return &____role_2; }
	inline void set__role_2(String_t* value)
	{
		____role_2 = value;
		Il2CppCodeGenWriteBarrier(&____role_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
