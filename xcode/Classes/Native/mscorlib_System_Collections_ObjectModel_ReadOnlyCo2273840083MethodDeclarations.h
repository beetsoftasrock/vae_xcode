﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct ReadOnlyCollection_1_t2273840083;
// System.Collections.Generic.IList`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct IList_1_t2628994992;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Beetsoft.VAE.Purchaser/PurchaseSession[]
struct PurchaseSessionU5BU5D_t64753102;
// System.Collections.Generic.IEnumerator`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct IEnumerator_1_t3858545514;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Beetsoft_VAE_Purchaser_PurchaseS2088054391.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m949148201_gshared (ReadOnlyCollection_1_t2273840083 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m949148201(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m949148201_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m937804735_gshared (ReadOnlyCollection_1_t2273840083 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m937804735(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, PurchaseSession_t2088054391 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m937804735_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m682819251_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m682819251(__this, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m682819251_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3127962362_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, PurchaseSession_t2088054391  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3127962362(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, PurchaseSession_t2088054391 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3127962362_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1324060916_gshared (ReadOnlyCollection_1_t2273840083 * __this, PurchaseSession_t2088054391  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1324060916(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2273840083 *, PurchaseSession_t2088054391 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1324060916_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1704431958_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1704431958(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1704431958_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  PurchaseSession_t2088054391  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3262320186_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3262320186(__this, ___index0, method) ((  PurchaseSession_t2088054391  (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3262320186_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2599598595_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, PurchaseSession_t2088054391  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2599598595(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, PurchaseSession_t2088054391 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2599598595_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2058453519_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2058453519(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2058453519_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m211771512_gshared (ReadOnlyCollection_1_t2273840083 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m211771512(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m211771512_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1406411547_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1406411547(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1406411547_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1304088060_gshared (ReadOnlyCollection_1_t2273840083 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1304088060(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2273840083 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1304088060_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3374428846_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3374428846(__this, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3374428846_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1943115930_gshared (ReadOnlyCollection_1_t2273840083 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1943115930(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2273840083 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1943115930_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2620451690_gshared (ReadOnlyCollection_1_t2273840083 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2620451690(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2273840083 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2620451690_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m4152258035_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m4152258035(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m4152258035_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m455470235_gshared (ReadOnlyCollection_1_t2273840083 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m455470235(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m455470235_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4065468169_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4065468169(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4065468169_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2673550804_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2673550804(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2673550804_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1968805762_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1968805762(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1968805762_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4153017859_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4153017859(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4153017859_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3698827016_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3698827016(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3698827016_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3483663519_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3483663519(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3483663519_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3466282474_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3466282474(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3466282474_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1806842821_gshared (ReadOnlyCollection_1_t2273840083 * __this, PurchaseSession_t2088054391  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1806842821(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2273840083 *, PurchaseSession_t2088054391 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1806842821_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m948997539_gshared (ReadOnlyCollection_1_t2273840083 * __this, PurchaseSessionU5BU5D_t64753102* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m948997539(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2273840083 *, PurchaseSessionU5BU5D_t64753102*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m948997539_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2243511440_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2243511440(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2243511440_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1390030887_gshared (ReadOnlyCollection_1_t2273840083 * __this, PurchaseSession_t2088054391  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1390030887(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2273840083 *, PurchaseSession_t2088054391 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1390030887_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m816013452_gshared (ReadOnlyCollection_1_t2273840083 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m816013452(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2273840083 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m816013452_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Beetsoft.VAE.Purchaser/PurchaseSession>::get_Item(System.Int32)
extern "C"  PurchaseSession_t2088054391  ReadOnlyCollection_1_get_Item_m2335700942_gshared (ReadOnlyCollection_1_t2273840083 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2335700942(__this, ___index0, method) ((  PurchaseSession_t2088054391  (*) (ReadOnlyCollection_1_t2273840083 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2335700942_gshared)(__this, ___index0, method)
