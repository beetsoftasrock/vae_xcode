﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1500295812.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa2731437128.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=244 <PrivateImplementationDetails>::$field-CD7787BED8C586AC83821711019168D548FED538
	U24ArrayTypeU3D244_t1500295812  ___U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0;
	// <PrivateImplementationDetails>/$ArrayType=60 <PrivateImplementationDetails>::$field-2FBBC74EA06412856C501165AE981BA196B29BC8
	U24ArrayTypeU3D60_t2731437128  ___U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1;

public:
	inline static int32_t get_offset_of_U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0)); }
	inline U24ArrayTypeU3D244_t1500295812  get_U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0() const { return ___U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0; }
	inline U24ArrayTypeU3D244_t1500295812 * get_address_of_U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0() { return &___U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0; }
	inline void set_U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0(U24ArrayTypeU3D244_t1500295812  value)
	{
		___U24fieldU2DCD7787BED8C586AC83821711019168D548FED538_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1)); }
	inline U24ArrayTypeU3D60_t2731437128  get_U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1() const { return ___U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1; }
	inline U24ArrayTypeU3D60_t2731437128 * get_address_of_U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1() { return &___U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1; }
	inline void set_U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1(U24ArrayTypeU3D60_t2731437128  value)
	{
		___U24fieldU2D2FBBC74EA06412856C501165AE981BA196B29BC8_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
