﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadSceneScript
struct LoadSceneScript_t888325993;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void LoadSceneScript::.ctor()
extern "C"  void LoadSceneScript__ctor_m2925440906 (LoadSceneScript_t888325993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadSceneScript::Awake()
extern "C"  void LoadSceneScript_Awake_m563075353 (LoadSceneScript_t888325993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadSceneScript::LoadScene(UnityEngine.Transform)
extern "C"  void LoadSceneScript_LoadScene_m2134058859 (LoadSceneScript_t888325993 * __this, Transform_t3275118058 * ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadSceneScript::LoadScene()
extern "C"  void LoadSceneScript_LoadScene_m3470595826 (LoadSceneScript_t888325993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadSceneScript::DelayLoadVR()
extern "C"  Il2CppObject * LoadSceneScript_DelayLoadVR_m1709816213 (LoadSceneScript_t888325993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
