﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectionItem
struct SelectionItem_t610427083;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SelectionItem::.ctor()
extern "C"  void SelectionItem__ctor_m4109814824 (SelectionItem_t610427083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionItem::Set(System.String,System.String)
extern "C"  void SelectionItem_Set_m1830955518 (SelectionItem_t610427083 * __this, String_t* ___t0, String_t* ___st1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionItem::SetSelectedButton(System.Boolean)
extern "C"  void SelectionItem_SetSelectedButton_m1210675744 (SelectionItem_t610427083 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
