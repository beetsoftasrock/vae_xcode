﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseConversationCommandLoader/<GoTo>c__AnonStorey0
struct  U3CGoToU3Ec__AnonStorey0_t144537213  : public Il2CppObject
{
public:
	// System.String BaseConversationCommandLoader/<GoTo>c__AnonStorey0::destination
	String_t* ___destination_0;

public:
	inline static int32_t get_offset_of_destination_0() { return static_cast<int32_t>(offsetof(U3CGoToU3Ec__AnonStorey0_t144537213, ___destination_0)); }
	inline String_t* get_destination_0() const { return ___destination_0; }
	inline String_t** get_address_of_destination_0() { return &___destination_0; }
	inline void set_destination_0(String_t* value)
	{
		___destination_0 = value;
		Il2CppCodeGenWriteBarrier(&___destination_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
