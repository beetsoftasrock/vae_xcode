﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExerciseVRSwitcher/<GoToNormalMode>c__AnonStorey0
struct U3CGoToNormalModeU3Ec__AnonStorey0_t2614165850;

#include "codegen/il2cpp-codegen.h"

// System.Void ExerciseVRSwitcher/<GoToNormalMode>c__AnonStorey0::.ctor()
extern "C"  void U3CGoToNormalModeU3Ec__AnonStorey0__ctor_m626283729 (U3CGoToNormalModeU3Ec__AnonStorey0_t2614165850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExerciseVRSwitcher/<GoToNormalMode>c__AnonStorey0::<>m__0()
extern "C"  void U3CGoToNormalModeU3Ec__AnonStorey0_U3CU3Em__0_m2172013444 (U3CGoToNormalModeU3Ec__AnonStorey0_t2614165850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
