﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CachedDragInfo
struct CachedDragInfo_t1136705792;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeCotroller/<UpdateCache>c__AnonStorey3
struct  U3CUpdateCacheU3Ec__AnonStorey3_t2625163487  : public Il2CppObject
{
public:
	// CachedDragInfo HomeCotroller/<UpdateCache>c__AnonStorey3::dragData
	CachedDragInfo_t1136705792 * ___dragData_0;

public:
	inline static int32_t get_offset_of_dragData_0() { return static_cast<int32_t>(offsetof(U3CUpdateCacheU3Ec__AnonStorey3_t2625163487, ___dragData_0)); }
	inline CachedDragInfo_t1136705792 * get_dragData_0() const { return ___dragData_0; }
	inline CachedDragInfo_t1136705792 ** get_address_of_dragData_0() { return &___dragData_0; }
	inline void set_dragData_0(CachedDragInfo_t1136705792 * value)
	{
		___dragData_0 = value;
		Il2CppCodeGenWriteBarrier(&___dragData_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
