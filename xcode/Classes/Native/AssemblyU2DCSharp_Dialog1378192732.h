﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dialog
struct  Dialog_t1378192732  : public Il2CppObject
{
public:
	// System.String Dialog::content
	String_t* ___content_0;
	// System.String Dialog::subContent
	String_t* ___subContent_1;
	// System.Boolean Dialog::isUser
	bool ___isUser_2;
	// System.String Dialog::voice
	String_t* ___voice_3;
	// System.String Dialog::type
	String_t* ___type_4;

public:
	inline static int32_t get_offset_of_content_0() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___content_0)); }
	inline String_t* get_content_0() const { return ___content_0; }
	inline String_t** get_address_of_content_0() { return &___content_0; }
	inline void set_content_0(String_t* value)
	{
		___content_0 = value;
		Il2CppCodeGenWriteBarrier(&___content_0, value);
	}

	inline static int32_t get_offset_of_subContent_1() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___subContent_1)); }
	inline String_t* get_subContent_1() const { return ___subContent_1; }
	inline String_t** get_address_of_subContent_1() { return &___subContent_1; }
	inline void set_subContent_1(String_t* value)
	{
		___subContent_1 = value;
		Il2CppCodeGenWriteBarrier(&___subContent_1, value);
	}

	inline static int32_t get_offset_of_isUser_2() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___isUser_2)); }
	inline bool get_isUser_2() const { return ___isUser_2; }
	inline bool* get_address_of_isUser_2() { return &___isUser_2; }
	inline void set_isUser_2(bool value)
	{
		___isUser_2 = value;
	}

	inline static int32_t get_offset_of_voice_3() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___voice_3)); }
	inline String_t* get_voice_3() const { return ___voice_3; }
	inline String_t** get_address_of_voice_3() { return &___voice_3; }
	inline void set_voice_3(String_t* value)
	{
		___voice_3 = value;
		Il2CppCodeGenWriteBarrier(&___voice_3, value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___type_4)); }
	inline String_t* get_type_4() const { return ___type_4; }
	inline String_t** get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(String_t* value)
	{
		___type_4 = value;
		Il2CppCodeGenWriteBarrier(&___type_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
