﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2595447308MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3495140130(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1522289892 *, Dictionary_2_t3333759417 *, const MethodInfo*))KeyCollection__ctor_m157823507_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2671937000(__this, ___item0, method) ((  void (*) (KeyCollection_t1522289892 *, ExtensionIntPair_t3093161221 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151094689_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m400409871(__this, method) ((  void (*) (KeyCollection_t1522289892 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2542448756_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4164228612(__this, ___item0, method) ((  bool (*) (KeyCollection_t1522289892 *, ExtensionIntPair_t3093161221 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m609886271_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3926647191(__this, ___item0, method) ((  bool (*) (KeyCollection_t1522289892 *, ExtensionIntPair_t3093161221 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m481603688_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2476413269(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1522289892 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m841782026_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1048512613(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1522289892 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m894372018_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3877058244(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1522289892 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2524436037_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2909192227(__this, method) ((  bool (*) (KeyCollection_t1522289892 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2353914802_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3913892545(__this, method) ((  bool (*) (KeyCollection_t1522289892 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2285549930_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3211548953(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1522289892 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2305589458_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m111748655(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1522289892 *, ExtensionIntPairU5BU5D_t1445074248*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1489653396_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1642567236(__this, method) ((  Enumerator_t1728295559  (*) (KeyCollection_t1522289892 *, const MethodInfo*))KeyCollection_GetEnumerator_m1220293775_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>::get_Count()
#define KeyCollection_get_Count_m2371844125(__this, method) ((  int32_t (*) (KeyCollection_t1522289892 *, const MethodInfo*))KeyCollection_get_Count_m776027594_gshared)(__this, method)
