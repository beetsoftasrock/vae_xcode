﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveContent
struct  MoveContent_t4217566478  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MoveContent::speed
	float ___speed_2;
	// UnityEngine.GameObject MoveContent::content
	GameObject_t1756533147 * ___content_3;
	// System.Boolean MoveContent::isHolding
	bool ___isHolding_4;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(MoveContent_t4217566478, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(MoveContent_t4217566478, ___content_3)); }
	inline GameObject_t1756533147 * get_content_3() const { return ___content_3; }
	inline GameObject_t1756533147 ** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(GameObject_t1756533147 * value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier(&___content_3, value);
	}

	inline static int32_t get_offset_of_isHolding_4() { return static_cast<int32_t>(offsetof(MoveContent_t4217566478, ___isHolding_4)); }
	inline bool get_isHolding_4() const { return ___isHolding_4; }
	inline bool* get_address_of_isHolding_4() { return &___isHolding_4; }
	inline void set_isHolding_4(bool value)
	{
		___isHolding_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
