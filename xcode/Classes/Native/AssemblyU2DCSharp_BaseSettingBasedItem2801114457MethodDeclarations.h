﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseSettingBasedItem
struct BaseSettingBasedItem_t2801114457;
// BaseSetting
struct BaseSetting_t2575616157;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseSettingBasedItem::.ctor()
extern "C"  void BaseSettingBasedItem__ctor_m1824976604 (BaseSettingBasedItem_t2801114457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BaseSetting BaseSettingBasedItem::GetBaseSetting()
extern "C"  BaseSetting_t2575616157 * BaseSettingBasedItem_GetBaseSetting_m4179586601 (BaseSettingBasedItem_t2801114457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
