﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupHelperVR
struct  PopupHelperVR_t3784706002  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> PopupHelperVR::cachedTransforms
	List_1_t2644239190 * ___cachedTransforms_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PopupHelperVR::cachedScales
	List_1_t1612828712 * ___cachedScales_3;

public:
	inline static int32_t get_offset_of_cachedTransforms_2() { return static_cast<int32_t>(offsetof(PopupHelperVR_t3784706002, ___cachedTransforms_2)); }
	inline List_1_t2644239190 * get_cachedTransforms_2() const { return ___cachedTransforms_2; }
	inline List_1_t2644239190 ** get_address_of_cachedTransforms_2() { return &___cachedTransforms_2; }
	inline void set_cachedTransforms_2(List_1_t2644239190 * value)
	{
		___cachedTransforms_2 = value;
		Il2CppCodeGenWriteBarrier(&___cachedTransforms_2, value);
	}

	inline static int32_t get_offset_of_cachedScales_3() { return static_cast<int32_t>(offsetof(PopupHelperVR_t3784706002, ___cachedScales_3)); }
	inline List_1_t1612828712 * get_cachedScales_3() const { return ___cachedScales_3; }
	inline List_1_t1612828712 ** get_address_of_cachedScales_3() { return &___cachedScales_3; }
	inline void set_cachedScales_3(List_1_t1612828712 * value)
	{
		___cachedScales_3 = value;
		Il2CppCodeGenWriteBarrier(&___cachedScales_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
