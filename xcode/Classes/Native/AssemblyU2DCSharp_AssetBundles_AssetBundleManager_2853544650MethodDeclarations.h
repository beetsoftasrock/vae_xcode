﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<CheckBundleExist>c__AnonStorey3
struct U3CCheckBundleExistU3Ec__AnonStorey3_t2853544650;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AssetBundles.AssetBundleManager/<CheckBundleExist>c__AnonStorey3::.ctor()
extern "C"  void U3CCheckBundleExistU3Ec__AnonStorey3__ctor_m1789164349 (U3CCheckBundleExistU3Ec__AnonStorey3_t2853544650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager/<CheckBundleExist>c__AnonStorey3::<>m__0(System.String)
extern "C"  bool U3CCheckBundleExistU3Ec__AnonStorey3_U3CU3Em__0_m2481724136 (U3CCheckBundleExistU3Ec__AnonStorey3_t2853544650 * __this, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
