﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// SentenceQuestionView
struct SentenceQuestionView_t1478559372;
// SentenceStructureIdiomQuestionData
struct SentenceStructureIdiomQuestionData_t3251732102;

#include "AssemblyU2DCSharp_BaseOnplayQuestion1717064182.h"
#include "AssemblyU2DCSharp_SentenceStructureIdiomOnPlayQues3958551864.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SentenceStructureIdiomOnPlayQuestion
struct  SentenceStructureIdiomOnPlayQuestion_t936886987  : public BaseOnplayQuestion_t1717064182
{
public:
	// System.String SentenceStructureIdiomOnPlayQuestion::soundPath
	String_t* ___soundPath_2;
	// SentenceStructureIdiomOnPlayQuestion/SelectedState SentenceStructureIdiomOnPlayQuestion::_currentSelected
	int32_t ____currentSelected_3;
	// SentenceQuestionView SentenceStructureIdiomOnPlayQuestion::_view
	SentenceQuestionView_t1478559372 * ____view_4;
	// SentenceStructureIdiomQuestionData SentenceStructureIdiomOnPlayQuestion::<questionData>k__BackingField
	SentenceStructureIdiomQuestionData_t3251732102 * ___U3CquestionDataU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_soundPath_2() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomOnPlayQuestion_t936886987, ___soundPath_2)); }
	inline String_t* get_soundPath_2() const { return ___soundPath_2; }
	inline String_t** get_address_of_soundPath_2() { return &___soundPath_2; }
	inline void set_soundPath_2(String_t* value)
	{
		___soundPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___soundPath_2, value);
	}

	inline static int32_t get_offset_of__currentSelected_3() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomOnPlayQuestion_t936886987, ____currentSelected_3)); }
	inline int32_t get__currentSelected_3() const { return ____currentSelected_3; }
	inline int32_t* get_address_of__currentSelected_3() { return &____currentSelected_3; }
	inline void set__currentSelected_3(int32_t value)
	{
		____currentSelected_3 = value;
	}

	inline static int32_t get_offset_of__view_4() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomOnPlayQuestion_t936886987, ____view_4)); }
	inline SentenceQuestionView_t1478559372 * get__view_4() const { return ____view_4; }
	inline SentenceQuestionView_t1478559372 ** get_address_of__view_4() { return &____view_4; }
	inline void set__view_4(SentenceQuestionView_t1478559372 * value)
	{
		____view_4 = value;
		Il2CppCodeGenWriteBarrier(&____view_4, value);
	}

	inline static int32_t get_offset_of_U3CquestionDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SentenceStructureIdiomOnPlayQuestion_t936886987, ___U3CquestionDataU3Ek__BackingField_5)); }
	inline SentenceStructureIdiomQuestionData_t3251732102 * get_U3CquestionDataU3Ek__BackingField_5() const { return ___U3CquestionDataU3Ek__BackingField_5; }
	inline SentenceStructureIdiomQuestionData_t3251732102 ** get_address_of_U3CquestionDataU3Ek__BackingField_5() { return &___U3CquestionDataU3Ek__BackingField_5; }
	inline void set_U3CquestionDataU3Ek__BackingField_5(SentenceStructureIdiomQuestionData_t3251732102 * value)
	{
		___U3CquestionDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CquestionDataU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
