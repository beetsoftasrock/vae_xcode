﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionChecker/<coroutineCheck>c__Iterator0
struct U3CcoroutineCheckU3Ec__Iterator0_t204427749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VersionChecker/<coroutineCheck>c__Iterator0::.ctor()
extern "C"  void U3CcoroutineCheckU3Ec__Iterator0__ctor_m3852316698 (U3CcoroutineCheckU3Ec__Iterator0_t204427749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionChecker/<coroutineCheck>c__Iterator0::MoveNext()
extern "C"  bool U3CcoroutineCheckU3Ec__Iterator0_MoveNext_m216753414 (U3CcoroutineCheckU3Ec__Iterator0_t204427749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VersionChecker/<coroutineCheck>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CcoroutineCheckU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1073736416 (U3CcoroutineCheckU3Ec__Iterator0_t204427749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VersionChecker/<coroutineCheck>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CcoroutineCheckU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2047618312 (U3CcoroutineCheckU3Ec__Iterator0_t204427749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionChecker/<coroutineCheck>c__Iterator0::Dispose()
extern "C"  void U3CcoroutineCheckU3Ec__Iterator0_Dispose_m3564231895 (U3CcoroutineCheckU3Ec__Iterator0_t204427749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionChecker/<coroutineCheck>c__Iterator0::Reset()
extern "C"  void U3CcoroutineCheckU3Ec__Iterator0_Reset_m2604523173 (U3CcoroutineCheckU3Ec__Iterator0_t204427749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
