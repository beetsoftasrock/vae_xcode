﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionItem
struct  SelectionItem_t610427083  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text SelectionItem::text
	Text_t356221433 * ___text_2;
	// UnityEngine.UI.Text SelectionItem::subText
	Text_t356221433 * ___subText_3;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(SelectionItem_t610427083, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_subText_3() { return static_cast<int32_t>(offsetof(SelectionItem_t610427083, ___subText_3)); }
	inline Text_t356221433 * get_subText_3() const { return ___subText_3; }
	inline Text_t356221433 ** get_address_of_subText_3() { return &___subText_3; }
	inline void set_subText_3(Text_t356221433 * value)
	{
		___subText_3 = value;
		Il2CppCodeGenWriteBarrier(&___subText_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
