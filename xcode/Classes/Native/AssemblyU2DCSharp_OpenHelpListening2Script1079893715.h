﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_OpenHelpScript3207620564.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenHelpListening2Script
struct  OpenHelpListening2Script_t1079893715  : public OpenHelpScript_t3207620564
{
public:
	// UnityEngine.Transform OpenHelpListening2Script::answerGrid
	Transform_t3275118058 * ___answerGrid_10;

public:
	inline static int32_t get_offset_of_answerGrid_10() { return static_cast<int32_t>(offsetof(OpenHelpListening2Script_t1079893715, ___answerGrid_10)); }
	inline Transform_t3275118058 * get_answerGrid_10() const { return ___answerGrid_10; }
	inline Transform_t3275118058 ** get_address_of_answerGrid_10() { return &___answerGrid_10; }
	inline void set_answerGrid_10(Transform_t3275118058 * value)
	{
		___answerGrid_10 = value;
		Il2CppCodeGenWriteBarrier(&___answerGrid_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
