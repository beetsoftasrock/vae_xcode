﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip>
struct Dictionary_2_t3847337892;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t504227755;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;
// com.beetsoft.utility_old.SoundManager
struct SoundManager_t695211022;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// com.beetsoft.utility_old.SoundManager
struct  SoundManager_t695211022  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip> com.beetsoft.utility_old.SoundManager::audioClips
	Dictionary_2_t3847337892 * ___audioClips_2;
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> com.beetsoft.utility_old.SoundManager::audioSources
	List_1_t504227755 * ___audioSources_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> com.beetsoft.utility_old.SoundManager::dicSoundCommand
	Dictionary_2_t3671312409 * ___dicSoundCommand_4;

public:
	inline static int32_t get_offset_of_audioClips_2() { return static_cast<int32_t>(offsetof(SoundManager_t695211022, ___audioClips_2)); }
	inline Dictionary_2_t3847337892 * get_audioClips_2() const { return ___audioClips_2; }
	inline Dictionary_2_t3847337892 ** get_address_of_audioClips_2() { return &___audioClips_2; }
	inline void set_audioClips_2(Dictionary_2_t3847337892 * value)
	{
		___audioClips_2 = value;
		Il2CppCodeGenWriteBarrier(&___audioClips_2, value);
	}

	inline static int32_t get_offset_of_audioSources_3() { return static_cast<int32_t>(offsetof(SoundManager_t695211022, ___audioSources_3)); }
	inline List_1_t504227755 * get_audioSources_3() const { return ___audioSources_3; }
	inline List_1_t504227755 ** get_address_of_audioSources_3() { return &___audioSources_3; }
	inline void set_audioSources_3(List_1_t504227755 * value)
	{
		___audioSources_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioSources_3, value);
	}

	inline static int32_t get_offset_of_dicSoundCommand_4() { return static_cast<int32_t>(offsetof(SoundManager_t695211022, ___dicSoundCommand_4)); }
	inline Dictionary_2_t3671312409 * get_dicSoundCommand_4() const { return ___dicSoundCommand_4; }
	inline Dictionary_2_t3671312409 ** get_address_of_dicSoundCommand_4() { return &___dicSoundCommand_4; }
	inline void set_dicSoundCommand_4(Dictionary_2_t3671312409 * value)
	{
		___dicSoundCommand_4 = value;
		Il2CppCodeGenWriteBarrier(&___dicSoundCommand_4, value);
	}
};

struct SoundManager_t695211022_StaticFields
{
public:
	// com.beetsoft.utility_old.SoundManager com.beetsoft.utility_old.SoundManager::_instance
	SoundManager_t695211022 * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(SoundManager_t695211022_StaticFields, ____instance_5)); }
	inline SoundManager_t695211022 * get__instance_5() const { return ____instance_5; }
	inline SoundManager_t695211022 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(SoundManager_t695211022 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier(&____instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
