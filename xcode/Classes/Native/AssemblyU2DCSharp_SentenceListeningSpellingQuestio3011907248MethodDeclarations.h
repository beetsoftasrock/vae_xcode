﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SentenceListeningSpellingQuestionData
struct SentenceListeningSpellingQuestionData_t3011907248;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SentenceListeningSpellingQuestionData::.ctor()
extern "C"  void SentenceListeningSpellingQuestionData__ctor_m3317163015 (SentenceListeningSpellingQuestionData_t3011907248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceListeningSpellingQuestionData::get_questionText()
extern "C"  String_t* SentenceListeningSpellingQuestionData_get_questionText_m1814503238 (SentenceListeningSpellingQuestionData_t3011907248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingQuestionData::set_questionText(System.String)
extern "C"  void SentenceListeningSpellingQuestionData_set_questionText_m47144919 (SentenceListeningSpellingQuestionData_t3011907248 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceListeningSpellingQuestionData::get_soundData()
extern "C"  String_t* SentenceListeningSpellingQuestionData_get_soundData_m1750189382 (SentenceListeningSpellingQuestionData_t3011907248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingQuestionData::set_soundData(System.String)
extern "C"  void SentenceListeningSpellingQuestionData_set_soundData_m3512023111 (SentenceListeningSpellingQuestionData_t3011907248 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SentenceListeningSpellingQuestionData::get_correctAnswer()
extern "C"  String_t* SentenceListeningSpellingQuestionData_get_correctAnswer_m1026943633 (SentenceListeningSpellingQuestionData_t3011907248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SentenceListeningSpellingQuestionData::set_correctAnswer(System.String)
extern "C"  void SentenceListeningSpellingQuestionData_set_correctAnswer_m31783682 (SentenceListeningSpellingQuestionData_t3011907248 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
