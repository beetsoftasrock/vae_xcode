﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextKaraoke
struct  TextKaraoke_t2450194175  : public MonoBehaviour_t1158329972
{
public:
	// System.String TextKaraoke::content
	String_t* ___content_2;
	// UnityEngine.Color TextKaraoke::colorDefault
	Color_t2020392075  ___colorDefault_3;
	// UnityEngine.Color TextKaraoke::colorKaraoke
	Color_t2020392075  ___colorKaraoke_4;
	// UnityEngine.AudioSource TextKaraoke::audioSource
	AudioSource_t1135106623 * ___audioSource_5;
	// UnityEngine.UI.Text TextKaraoke::txtMain
	Text_t356221433 * ___txtMain_6;
	// UnityEngine.GameObject TextKaraoke::goKara
	GameObject_t1756533147 * ___goKara_7;
	// System.Single TextKaraoke::delayStart
	float ___delayStart_8;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(TextKaraoke_t2450194175, ___content_2)); }
	inline String_t* get_content_2() const { return ___content_2; }
	inline String_t** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(String_t* value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier(&___content_2, value);
	}

	inline static int32_t get_offset_of_colorDefault_3() { return static_cast<int32_t>(offsetof(TextKaraoke_t2450194175, ___colorDefault_3)); }
	inline Color_t2020392075  get_colorDefault_3() const { return ___colorDefault_3; }
	inline Color_t2020392075 * get_address_of_colorDefault_3() { return &___colorDefault_3; }
	inline void set_colorDefault_3(Color_t2020392075  value)
	{
		___colorDefault_3 = value;
	}

	inline static int32_t get_offset_of_colorKaraoke_4() { return static_cast<int32_t>(offsetof(TextKaraoke_t2450194175, ___colorKaraoke_4)); }
	inline Color_t2020392075  get_colorKaraoke_4() const { return ___colorKaraoke_4; }
	inline Color_t2020392075 * get_address_of_colorKaraoke_4() { return &___colorKaraoke_4; }
	inline void set_colorKaraoke_4(Color_t2020392075  value)
	{
		___colorKaraoke_4 = value;
	}

	inline static int32_t get_offset_of_audioSource_5() { return static_cast<int32_t>(offsetof(TextKaraoke_t2450194175, ___audioSource_5)); }
	inline AudioSource_t1135106623 * get_audioSource_5() const { return ___audioSource_5; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_5() { return &___audioSource_5; }
	inline void set_audioSource_5(AudioSource_t1135106623 * value)
	{
		___audioSource_5 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_5, value);
	}

	inline static int32_t get_offset_of_txtMain_6() { return static_cast<int32_t>(offsetof(TextKaraoke_t2450194175, ___txtMain_6)); }
	inline Text_t356221433 * get_txtMain_6() const { return ___txtMain_6; }
	inline Text_t356221433 ** get_address_of_txtMain_6() { return &___txtMain_6; }
	inline void set_txtMain_6(Text_t356221433 * value)
	{
		___txtMain_6 = value;
		Il2CppCodeGenWriteBarrier(&___txtMain_6, value);
	}

	inline static int32_t get_offset_of_goKara_7() { return static_cast<int32_t>(offsetof(TextKaraoke_t2450194175, ___goKara_7)); }
	inline GameObject_t1756533147 * get_goKara_7() const { return ___goKara_7; }
	inline GameObject_t1756533147 ** get_address_of_goKara_7() { return &___goKara_7; }
	inline void set_goKara_7(GameObject_t1756533147 * value)
	{
		___goKara_7 = value;
		Il2CppCodeGenWriteBarrier(&___goKara_7, value);
	}

	inline static int32_t get_offset_of_delayStart_8() { return static_cast<int32_t>(offsetof(TextKaraoke_t2450194175, ___delayStart_8)); }
	inline float get_delayStart_8() const { return ___delayStart_8; }
	inline float* get_address_of_delayStart_8() { return &___delayStart_8; }
	inline void set_delayStart_8(float value)
	{
		___delayStart_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
