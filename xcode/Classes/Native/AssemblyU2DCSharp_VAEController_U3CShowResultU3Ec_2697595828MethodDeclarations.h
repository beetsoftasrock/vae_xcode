﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VAEController/<ShowResult>c__AnonStorey0
struct U3CShowResultU3Ec__AnonStorey0_t2697595828;

#include "codegen/il2cpp-codegen.h"

// System.Void VAEController/<ShowResult>c__AnonStorey0::.ctor()
extern "C"  void U3CShowResultU3Ec__AnonStorey0__ctor_m4258216705 (U3CShowResultU3Ec__AnonStorey0_t2697595828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController/<ShowResult>c__AnonStorey0::<>m__0()
extern "C"  void U3CShowResultU3Ec__AnonStorey0_U3CU3Em__0_m2799366438 (U3CShowResultU3Ec__AnonStorey0_t2697595828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VAEController/<ShowResult>c__AnonStorey0::<>m__1()
extern "C"  void U3CShowResultU3Ec__AnonStorey0_U3CU3Em__1_m2940528939 (U3CShowResultU3Ec__AnonStorey0_t2697595828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
