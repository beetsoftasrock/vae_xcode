﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterBasedListening1SceneSetup
struct ChapterBasedListening1SceneSetup_t344182819;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterBasedListening1SceneSetup::.ctor()
extern "C"  void ChapterBasedListening1SceneSetup__ctor_m4216932220 (ChapterBasedListening1SceneSetup_t344182819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterBasedListening1SceneSetup::Awake()
extern "C"  void ChapterBasedListening1SceneSetup_Awake_m1810205759 (ChapterBasedListening1SceneSetup_t344182819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
