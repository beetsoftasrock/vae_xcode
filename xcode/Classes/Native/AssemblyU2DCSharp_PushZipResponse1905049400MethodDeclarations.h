﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PushZipResponse
struct PushZipResponse_t1905049400;

#include "codegen/il2cpp-codegen.h"

// System.Void PushZipResponse::.ctor()
extern "C"  void PushZipResponse__ctor_m305272093 (PushZipResponse_t1905049400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
