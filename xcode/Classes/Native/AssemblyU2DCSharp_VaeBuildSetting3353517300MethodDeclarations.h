﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VaeBuildSetting
struct VaeBuildSetting_t3353517300;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VaeBuildSetting::.ctor()
extern "C"  void VaeBuildSetting__ctor_m4162131049 (VaeBuildSetting_t3353517300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VaeBuildSetting::GetApiServerLink()
extern "C"  String_t* VaeBuildSetting_GetApiServerLink_m3794092919 (VaeBuildSetting_t3353517300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
