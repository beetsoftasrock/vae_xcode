﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CharacterScript
struct CharacterScript_t1308706256;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManagerCharacters
struct  ManagerCharacters_t162625631  : public Il2CppObject
{
public:
	// CharacterScript ManagerCharacters::currentCharTalking
	CharacterScript_t1308706256 * ___currentCharTalking_0;
	// System.String ManagerCharacters::currentCharNameTalk
	String_t* ___currentCharNameTalk_1;
	// System.String ManagerCharacters::pathCharacterResource
	String_t* ___pathCharacterResource_2;
	// System.String ManagerCharacters::setCharacter
	String_t* ___setCharacter_3;
	// UnityEngine.Transform ManagerCharacters::<PrefabCharacter>k__BackingField
	Transform_t3275118058 * ___U3CPrefabCharacterU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<UnityEngine.Transform> ManagerCharacters::listCharacter
	List_1_t2644239190 * ___listCharacter_5;

public:
	inline static int32_t get_offset_of_currentCharTalking_0() { return static_cast<int32_t>(offsetof(ManagerCharacters_t162625631, ___currentCharTalking_0)); }
	inline CharacterScript_t1308706256 * get_currentCharTalking_0() const { return ___currentCharTalking_0; }
	inline CharacterScript_t1308706256 ** get_address_of_currentCharTalking_0() { return &___currentCharTalking_0; }
	inline void set_currentCharTalking_0(CharacterScript_t1308706256 * value)
	{
		___currentCharTalking_0 = value;
		Il2CppCodeGenWriteBarrier(&___currentCharTalking_0, value);
	}

	inline static int32_t get_offset_of_currentCharNameTalk_1() { return static_cast<int32_t>(offsetof(ManagerCharacters_t162625631, ___currentCharNameTalk_1)); }
	inline String_t* get_currentCharNameTalk_1() const { return ___currentCharNameTalk_1; }
	inline String_t** get_address_of_currentCharNameTalk_1() { return &___currentCharNameTalk_1; }
	inline void set_currentCharNameTalk_1(String_t* value)
	{
		___currentCharNameTalk_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentCharNameTalk_1, value);
	}

	inline static int32_t get_offset_of_pathCharacterResource_2() { return static_cast<int32_t>(offsetof(ManagerCharacters_t162625631, ___pathCharacterResource_2)); }
	inline String_t* get_pathCharacterResource_2() const { return ___pathCharacterResource_2; }
	inline String_t** get_address_of_pathCharacterResource_2() { return &___pathCharacterResource_2; }
	inline void set_pathCharacterResource_2(String_t* value)
	{
		___pathCharacterResource_2 = value;
		Il2CppCodeGenWriteBarrier(&___pathCharacterResource_2, value);
	}

	inline static int32_t get_offset_of_setCharacter_3() { return static_cast<int32_t>(offsetof(ManagerCharacters_t162625631, ___setCharacter_3)); }
	inline String_t* get_setCharacter_3() const { return ___setCharacter_3; }
	inline String_t** get_address_of_setCharacter_3() { return &___setCharacter_3; }
	inline void set_setCharacter_3(String_t* value)
	{
		___setCharacter_3 = value;
		Il2CppCodeGenWriteBarrier(&___setCharacter_3, value);
	}

	inline static int32_t get_offset_of_U3CPrefabCharacterU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ManagerCharacters_t162625631, ___U3CPrefabCharacterU3Ek__BackingField_4)); }
	inline Transform_t3275118058 * get_U3CPrefabCharacterU3Ek__BackingField_4() const { return ___U3CPrefabCharacterU3Ek__BackingField_4; }
	inline Transform_t3275118058 ** get_address_of_U3CPrefabCharacterU3Ek__BackingField_4() { return &___U3CPrefabCharacterU3Ek__BackingField_4; }
	inline void set_U3CPrefabCharacterU3Ek__BackingField_4(Transform_t3275118058 * value)
	{
		___U3CPrefabCharacterU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPrefabCharacterU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_listCharacter_5() { return static_cast<int32_t>(offsetof(ManagerCharacters_t162625631, ___listCharacter_5)); }
	inline List_1_t2644239190 * get_listCharacter_5() const { return ___listCharacter_5; }
	inline List_1_t2644239190 ** get_address_of_listCharacter_5() { return &___listCharacter_5; }
	inline void set_listCharacter_5(List_1_t2644239190 * value)
	{
		___listCharacter_5 = value;
		Il2CppCodeGenWriteBarrier(&___listCharacter_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
