﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoiceRecordModule/<DoReplayRecorded>c__Iterator0
struct U3CDoReplayRecordedU3Ec__Iterator0_t381392127;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VoiceRecordModule/<DoReplayRecorded>c__Iterator0::.ctor()
extern "C"  void U3CDoReplayRecordedU3Ec__Iterator0__ctor_m60796214 (U3CDoReplayRecordedU3Ec__Iterator0_t381392127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoiceRecordModule/<DoReplayRecorded>c__Iterator0::MoveNext()
extern "C"  bool U3CDoReplayRecordedU3Ec__Iterator0_MoveNext_m3620690174 (U3CDoReplayRecordedU3Ec__Iterator0_t381392127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoiceRecordModule/<DoReplayRecorded>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoReplayRecordedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1812348686 (U3CDoReplayRecordedU3Ec__Iterator0_t381392127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoiceRecordModule/<DoReplayRecorded>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoReplayRecordedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m942732422 (U3CDoReplayRecordedU3Ec__Iterator0_t381392127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule/<DoReplayRecorded>c__Iterator0::Dispose()
extern "C"  void U3CDoReplayRecordedU3Ec__Iterator0_Dispose_m2172423117 (U3CDoReplayRecordedU3Ec__Iterator0_t381392127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoiceRecordModule/<DoReplayRecorded>c__Iterator0::Reset()
extern "C"  void U3CDoReplayRecordedU3Ec__Iterator0_Reset_m2907460087 (U3CDoReplayRecordedU3Ec__Iterator0_t381392127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
