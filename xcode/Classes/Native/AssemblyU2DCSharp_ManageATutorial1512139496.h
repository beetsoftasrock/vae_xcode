﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.Collections.Generic.List`1<OpenHelpScript>
struct List_1_t2576741696;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// PageHelpScript
struct PageHelpScript_t1232253699;
// OpenHelpScript
struct OpenHelpScript_t3207620564;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManageATutorial
struct  ManageATutorial_t1512139496  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityEvent ManageATutorial::onOpenHelpEvent
	UnityEvent_t408735097 * ___onOpenHelpEvent_2;
	// UnityEngine.Events.UnityEvent ManageATutorial::onCloseHelpEvent
	UnityEvent_t408735097 * ___onCloseHelpEvent_3;
	// System.Collections.Generic.List`1<OpenHelpScript> ManageATutorial::listOpenHelpScript
	List_1_t2576741696 * ___listOpenHelpScript_4;
	// UnityEngine.Transform ManageATutorial::iconHandContainer
	Transform_t3275118058 * ___iconHandContainer_5;
	// UnityEngine.GameObject ManageATutorial::trackObjectToOpenHelp
	GameObject_t1756533147 * ___trackObjectToOpenHelp_6;
	// System.String ManageATutorial::keyHelpTutorialName
	String_t* ___keyHelpTutorialName_7;
	// System.Single ManageATutorial::speedMoveIconHand
	float ___speedMoveIconHand_8;
	// PageHelpScript ManageATutorial::pageHelpScript
	PageHelpScript_t1232253699 * ___pageHelpScript_9;
	// System.Boolean ManageATutorial::isResetIconHand
	bool ___isResetIconHand_10;
	// OpenHelpScript ManageATutorial::currentHelpScript
	OpenHelpScript_t3207620564 * ___currentHelpScript_11;
	// UnityEngine.Vector3 ManageATutorial::oriPositionIconHand
	Vector3_t2243707580  ___oriPositionIconHand_12;
	// UnityEngine.Vector3 ManageATutorial::oriEulerIconHand
	Vector3_t2243707580  ___oriEulerIconHand_13;
	// System.Int32 ManageATutorial::index
	int32_t ___index_14;
	// System.Boolean ManageATutorial::isCheckTrack
	bool ___isCheckTrack_15;

public:
	inline static int32_t get_offset_of_onOpenHelpEvent_2() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___onOpenHelpEvent_2)); }
	inline UnityEvent_t408735097 * get_onOpenHelpEvent_2() const { return ___onOpenHelpEvent_2; }
	inline UnityEvent_t408735097 ** get_address_of_onOpenHelpEvent_2() { return &___onOpenHelpEvent_2; }
	inline void set_onOpenHelpEvent_2(UnityEvent_t408735097 * value)
	{
		___onOpenHelpEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___onOpenHelpEvent_2, value);
	}

	inline static int32_t get_offset_of_onCloseHelpEvent_3() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___onCloseHelpEvent_3)); }
	inline UnityEvent_t408735097 * get_onCloseHelpEvent_3() const { return ___onCloseHelpEvent_3; }
	inline UnityEvent_t408735097 ** get_address_of_onCloseHelpEvent_3() { return &___onCloseHelpEvent_3; }
	inline void set_onCloseHelpEvent_3(UnityEvent_t408735097 * value)
	{
		___onCloseHelpEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___onCloseHelpEvent_3, value);
	}

	inline static int32_t get_offset_of_listOpenHelpScript_4() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___listOpenHelpScript_4)); }
	inline List_1_t2576741696 * get_listOpenHelpScript_4() const { return ___listOpenHelpScript_4; }
	inline List_1_t2576741696 ** get_address_of_listOpenHelpScript_4() { return &___listOpenHelpScript_4; }
	inline void set_listOpenHelpScript_4(List_1_t2576741696 * value)
	{
		___listOpenHelpScript_4 = value;
		Il2CppCodeGenWriteBarrier(&___listOpenHelpScript_4, value);
	}

	inline static int32_t get_offset_of_iconHandContainer_5() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___iconHandContainer_5)); }
	inline Transform_t3275118058 * get_iconHandContainer_5() const { return ___iconHandContainer_5; }
	inline Transform_t3275118058 ** get_address_of_iconHandContainer_5() { return &___iconHandContainer_5; }
	inline void set_iconHandContainer_5(Transform_t3275118058 * value)
	{
		___iconHandContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___iconHandContainer_5, value);
	}

	inline static int32_t get_offset_of_trackObjectToOpenHelp_6() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___trackObjectToOpenHelp_6)); }
	inline GameObject_t1756533147 * get_trackObjectToOpenHelp_6() const { return ___trackObjectToOpenHelp_6; }
	inline GameObject_t1756533147 ** get_address_of_trackObjectToOpenHelp_6() { return &___trackObjectToOpenHelp_6; }
	inline void set_trackObjectToOpenHelp_6(GameObject_t1756533147 * value)
	{
		___trackObjectToOpenHelp_6 = value;
		Il2CppCodeGenWriteBarrier(&___trackObjectToOpenHelp_6, value);
	}

	inline static int32_t get_offset_of_keyHelpTutorialName_7() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___keyHelpTutorialName_7)); }
	inline String_t* get_keyHelpTutorialName_7() const { return ___keyHelpTutorialName_7; }
	inline String_t** get_address_of_keyHelpTutorialName_7() { return &___keyHelpTutorialName_7; }
	inline void set_keyHelpTutorialName_7(String_t* value)
	{
		___keyHelpTutorialName_7 = value;
		Il2CppCodeGenWriteBarrier(&___keyHelpTutorialName_7, value);
	}

	inline static int32_t get_offset_of_speedMoveIconHand_8() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___speedMoveIconHand_8)); }
	inline float get_speedMoveIconHand_8() const { return ___speedMoveIconHand_8; }
	inline float* get_address_of_speedMoveIconHand_8() { return &___speedMoveIconHand_8; }
	inline void set_speedMoveIconHand_8(float value)
	{
		___speedMoveIconHand_8 = value;
	}

	inline static int32_t get_offset_of_pageHelpScript_9() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___pageHelpScript_9)); }
	inline PageHelpScript_t1232253699 * get_pageHelpScript_9() const { return ___pageHelpScript_9; }
	inline PageHelpScript_t1232253699 ** get_address_of_pageHelpScript_9() { return &___pageHelpScript_9; }
	inline void set_pageHelpScript_9(PageHelpScript_t1232253699 * value)
	{
		___pageHelpScript_9 = value;
		Il2CppCodeGenWriteBarrier(&___pageHelpScript_9, value);
	}

	inline static int32_t get_offset_of_isResetIconHand_10() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___isResetIconHand_10)); }
	inline bool get_isResetIconHand_10() const { return ___isResetIconHand_10; }
	inline bool* get_address_of_isResetIconHand_10() { return &___isResetIconHand_10; }
	inline void set_isResetIconHand_10(bool value)
	{
		___isResetIconHand_10 = value;
	}

	inline static int32_t get_offset_of_currentHelpScript_11() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___currentHelpScript_11)); }
	inline OpenHelpScript_t3207620564 * get_currentHelpScript_11() const { return ___currentHelpScript_11; }
	inline OpenHelpScript_t3207620564 ** get_address_of_currentHelpScript_11() { return &___currentHelpScript_11; }
	inline void set_currentHelpScript_11(OpenHelpScript_t3207620564 * value)
	{
		___currentHelpScript_11 = value;
		Il2CppCodeGenWriteBarrier(&___currentHelpScript_11, value);
	}

	inline static int32_t get_offset_of_oriPositionIconHand_12() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___oriPositionIconHand_12)); }
	inline Vector3_t2243707580  get_oriPositionIconHand_12() const { return ___oriPositionIconHand_12; }
	inline Vector3_t2243707580 * get_address_of_oriPositionIconHand_12() { return &___oriPositionIconHand_12; }
	inline void set_oriPositionIconHand_12(Vector3_t2243707580  value)
	{
		___oriPositionIconHand_12 = value;
	}

	inline static int32_t get_offset_of_oriEulerIconHand_13() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___oriEulerIconHand_13)); }
	inline Vector3_t2243707580  get_oriEulerIconHand_13() const { return ___oriEulerIconHand_13; }
	inline Vector3_t2243707580 * get_address_of_oriEulerIconHand_13() { return &___oriEulerIconHand_13; }
	inline void set_oriEulerIconHand_13(Vector3_t2243707580  value)
	{
		___oriEulerIconHand_13 = value;
	}

	inline static int32_t get_offset_of_index_14() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___index_14)); }
	inline int32_t get_index_14() const { return ___index_14; }
	inline int32_t* get_address_of_index_14() { return &___index_14; }
	inline void set_index_14(int32_t value)
	{
		___index_14 = value;
	}

	inline static int32_t get_offset_of_isCheckTrack_15() { return static_cast<int32_t>(offsetof(ManageATutorial_t1512139496, ___isCheckTrack_15)); }
	inline bool get_isCheckTrack_15() const { return ___isCheckTrack_15; }
	inline bool* get_address_of_isCheckTrack_15() { return &___isCheckTrack_15; }
	inline void set_isCheckTrack_15(bool value)
	{
		___isCheckTrack_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
