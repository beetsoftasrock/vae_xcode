﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Beetsoft.VAE.Purchaser
struct Purchaser_t1674559779;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.Security.CrossPlatformValidator
struct CrossPlatformValidator_t305796431;
// System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession>
struct List_1_t1457175523;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.Purchaser
struct  Purchaser_t1674559779  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Purchasing.IStoreController Beetsoft.VAE.Purchaser::storeController
	Il2CppObject * ___storeController_6;
	// UnityEngine.Purchasing.IExtensionProvider Beetsoft.VAE.Purchaser::storeExtensionProvider
	Il2CppObject * ___storeExtensionProvider_7;
	// UnityEngine.Purchasing.Security.CrossPlatformValidator Beetsoft.VAE.Purchaser::validator
	CrossPlatformValidator_t305796431 * ___validator_8;
	// System.Collections.Generic.List`1<Beetsoft.VAE.Purchaser/PurchaseSession> Beetsoft.VAE.Purchaser::sessions
	List_1_t1457175523 * ___sessions_9;

public:
	inline static int32_t get_offset_of_storeController_6() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779, ___storeController_6)); }
	inline Il2CppObject * get_storeController_6() const { return ___storeController_6; }
	inline Il2CppObject ** get_address_of_storeController_6() { return &___storeController_6; }
	inline void set_storeController_6(Il2CppObject * value)
	{
		___storeController_6 = value;
		Il2CppCodeGenWriteBarrier(&___storeController_6, value);
	}

	inline static int32_t get_offset_of_storeExtensionProvider_7() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779, ___storeExtensionProvider_7)); }
	inline Il2CppObject * get_storeExtensionProvider_7() const { return ___storeExtensionProvider_7; }
	inline Il2CppObject ** get_address_of_storeExtensionProvider_7() { return &___storeExtensionProvider_7; }
	inline void set_storeExtensionProvider_7(Il2CppObject * value)
	{
		___storeExtensionProvider_7 = value;
		Il2CppCodeGenWriteBarrier(&___storeExtensionProvider_7, value);
	}

	inline static int32_t get_offset_of_validator_8() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779, ___validator_8)); }
	inline CrossPlatformValidator_t305796431 * get_validator_8() const { return ___validator_8; }
	inline CrossPlatformValidator_t305796431 ** get_address_of_validator_8() { return &___validator_8; }
	inline void set_validator_8(CrossPlatformValidator_t305796431 * value)
	{
		___validator_8 = value;
		Il2CppCodeGenWriteBarrier(&___validator_8, value);
	}

	inline static int32_t get_offset_of_sessions_9() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779, ___sessions_9)); }
	inline List_1_t1457175523 * get_sessions_9() const { return ___sessions_9; }
	inline List_1_t1457175523 ** get_address_of_sessions_9() { return &___sessions_9; }
	inline void set_sessions_9(List_1_t1457175523 * value)
	{
		___sessions_9 = value;
		Il2CppCodeGenWriteBarrier(&___sessions_9, value);
	}
};

struct Purchaser_t1674559779_StaticFields
{
public:
	// System.String Beetsoft.VAE.Purchaser::PRODUCT_FULL_VERSION
	String_t* ___PRODUCT_FULL_VERSION_2;
	// Beetsoft.VAE.Purchaser Beetsoft.VAE.Purchaser::instance
	Purchaser_t1674559779 * ___instance_3;
	// System.Collections.Generic.List`1<System.String> Beetsoft.VAE.Purchaser::_ValidatedProducts
	List_1_t1398341365 * ____ValidatedProducts_4;
	// System.Boolean Beetsoft.VAE.Purchaser::<DoneValidated>k__BackingField
	bool ___U3CDoneValidatedU3Ek__BackingField_5;
	// System.Action Beetsoft.VAE.Purchaser::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_PRODUCT_FULL_VERSION_2() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779_StaticFields, ___PRODUCT_FULL_VERSION_2)); }
	inline String_t* get_PRODUCT_FULL_VERSION_2() const { return ___PRODUCT_FULL_VERSION_2; }
	inline String_t** get_address_of_PRODUCT_FULL_VERSION_2() { return &___PRODUCT_FULL_VERSION_2; }
	inline void set_PRODUCT_FULL_VERSION_2(String_t* value)
	{
		___PRODUCT_FULL_VERSION_2 = value;
		Il2CppCodeGenWriteBarrier(&___PRODUCT_FULL_VERSION_2, value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779_StaticFields, ___instance_3)); }
	inline Purchaser_t1674559779 * get_instance_3() const { return ___instance_3; }
	inline Purchaser_t1674559779 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(Purchaser_t1674559779 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}

	inline static int32_t get_offset_of__ValidatedProducts_4() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779_StaticFields, ____ValidatedProducts_4)); }
	inline List_1_t1398341365 * get__ValidatedProducts_4() const { return ____ValidatedProducts_4; }
	inline List_1_t1398341365 ** get_address_of__ValidatedProducts_4() { return &____ValidatedProducts_4; }
	inline void set__ValidatedProducts_4(List_1_t1398341365 * value)
	{
		____ValidatedProducts_4 = value;
		Il2CppCodeGenWriteBarrier(&____ValidatedProducts_4, value);
	}

	inline static int32_t get_offset_of_U3CDoneValidatedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779_StaticFields, ___U3CDoneValidatedU3Ek__BackingField_5)); }
	inline bool get_U3CDoneValidatedU3Ek__BackingField_5() const { return ___U3CDoneValidatedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CDoneValidatedU3Ek__BackingField_5() { return &___U3CDoneValidatedU3Ek__BackingField_5; }
	inline void set_U3CDoneValidatedU3Ek__BackingField_5(bool value)
	{
		___U3CDoneValidatedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(Purchaser_t1674559779_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
