﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultStatisticController
struct ResultStatisticController_t2929782681;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResultStatisticController::.ctor()
extern "C"  void ResultStatisticController__ctor_m3215874436 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::Awake()
extern "C"  void ResultStatisticController_Awake_m2750602453 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::Start()
extern "C"  void ResultStatisticController_Start_m784571316 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::TestValue()
extern "C"  void ResultStatisticController_TestValue_m2430229629 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::GetValueFromData()
extern "C"  void ResultStatisticController_GetValueFromData_m2201056607 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::SetNameLesson(System.String)
extern "C"  void ResultStatisticController_SetNameLesson_m3812285551 (ResultStatisticController_t2929782681 * __this, String_t* ___nameType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::SetNumberOfCorrectWrong(System.Int32,System.Int32)
extern "C"  void ResultStatisticController_SetNumberOfCorrectWrong_m2494296445 (ResultStatisticController_t2929782681 * __this, int32_t ___correct0, int32_t ___wrong1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::SetNumberCorrectWrongListening(System.Int32)
extern "C"  void ResultStatisticController_SetNumberCorrectWrongListening_m2053274476 (ResultStatisticController_t2929782681 * __this, int32_t ___totalScorePerChapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::SetFrequencyLearn(System.Int32,System.Int32)
extern "C"  void ResultStatisticController_SetFrequencyLearn_m4248297076 (ResultStatisticController_t2929782681 * __this, int32_t ___today0, int32_t ___total1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::SetTimeLearn(System.Single,System.Single)
extern "C"  void ResultStatisticController_SetTimeLearn_m1563058927 (ResultStatisticController_t2929782681 * __this, float ___today0, float ___total1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResultStatisticController::isToday()
extern "C"  bool ResultStatisticController_isToday_m3864414153 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::SetTimeComplete(System.Single,System.String)
extern "C"  void ResultStatisticController_SetTimeComplete_m3299887373 (ResultStatisticController_t2929782681 * __this, float ___timeLearnToday0, String_t* ____nameLesson1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::ShowResult()
extern "C"  void ResultStatisticController_ShowResult_m297892554 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::ShowCorrectWrong()
extern "C"  void ResultStatisticController_ShowCorrectWrong_m2644780196 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::ShowValueChart()
extern "C"  void ResultStatisticController_ShowValueChart_m1077596612 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::ShowFrequencyLearn()
extern "C"  void ResultStatisticController_ShowFrequencyLearn_m2579481759 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultStatisticController::ShowTimeLearn()
extern "C"  void ResultStatisticController_ShowTimeLearn_m1185733554 (ResultStatisticController_t2929782681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ResultStatisticController::timeLearnFromHours(System.Single)
extern "C"  String_t* ResultStatisticController_timeLearnFromHours_m85104874 (ResultStatisticController_t2929782681 * __this, float ___timeOfHours0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
