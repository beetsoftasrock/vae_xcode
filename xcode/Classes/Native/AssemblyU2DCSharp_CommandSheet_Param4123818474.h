﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandSheet/Param
struct  Param_t4123818474  : public Il2CppObject
{
public:
	// System.String CommandSheet/Param::Command
	String_t* ___Command_0;
	// System.Single CommandSheet/Param::wait
	float ___wait_1;
	// System.String CommandSheet/Param::Arg1
	String_t* ___Arg1_2;
	// System.String CommandSheet/Param::Arg2
	String_t* ___Arg2_3;
	// System.String CommandSheet/Param::Arg3
	String_t* ___Arg3_4;
	// System.String CommandSheet/Param::Arg4
	String_t* ___Arg4_5;
	// System.String CommandSheet/Param::Arg5
	String_t* ___Arg5_6;
	// System.String CommandSheet/Param::Type
	String_t* ___Type_7;
	// System.String CommandSheet/Param::Text
	String_t* ___Text_8;
	// System.String CommandSheet/Param::SubText
	String_t* ___SubText_9;
	// System.String CommandSheet/Param::Voice
	String_t* ___Voice_10;

public:
	inline static int32_t get_offset_of_Command_0() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Command_0)); }
	inline String_t* get_Command_0() const { return ___Command_0; }
	inline String_t** get_address_of_Command_0() { return &___Command_0; }
	inline void set_Command_0(String_t* value)
	{
		___Command_0 = value;
		Il2CppCodeGenWriteBarrier(&___Command_0, value);
	}

	inline static int32_t get_offset_of_wait_1() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___wait_1)); }
	inline float get_wait_1() const { return ___wait_1; }
	inline float* get_address_of_wait_1() { return &___wait_1; }
	inline void set_wait_1(float value)
	{
		___wait_1 = value;
	}

	inline static int32_t get_offset_of_Arg1_2() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Arg1_2)); }
	inline String_t* get_Arg1_2() const { return ___Arg1_2; }
	inline String_t** get_address_of_Arg1_2() { return &___Arg1_2; }
	inline void set_Arg1_2(String_t* value)
	{
		___Arg1_2 = value;
		Il2CppCodeGenWriteBarrier(&___Arg1_2, value);
	}

	inline static int32_t get_offset_of_Arg2_3() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Arg2_3)); }
	inline String_t* get_Arg2_3() const { return ___Arg2_3; }
	inline String_t** get_address_of_Arg2_3() { return &___Arg2_3; }
	inline void set_Arg2_3(String_t* value)
	{
		___Arg2_3 = value;
		Il2CppCodeGenWriteBarrier(&___Arg2_3, value);
	}

	inline static int32_t get_offset_of_Arg3_4() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Arg3_4)); }
	inline String_t* get_Arg3_4() const { return ___Arg3_4; }
	inline String_t** get_address_of_Arg3_4() { return &___Arg3_4; }
	inline void set_Arg3_4(String_t* value)
	{
		___Arg3_4 = value;
		Il2CppCodeGenWriteBarrier(&___Arg3_4, value);
	}

	inline static int32_t get_offset_of_Arg4_5() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Arg4_5)); }
	inline String_t* get_Arg4_5() const { return ___Arg4_5; }
	inline String_t** get_address_of_Arg4_5() { return &___Arg4_5; }
	inline void set_Arg4_5(String_t* value)
	{
		___Arg4_5 = value;
		Il2CppCodeGenWriteBarrier(&___Arg4_5, value);
	}

	inline static int32_t get_offset_of_Arg5_6() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Arg5_6)); }
	inline String_t* get_Arg5_6() const { return ___Arg5_6; }
	inline String_t** get_address_of_Arg5_6() { return &___Arg5_6; }
	inline void set_Arg5_6(String_t* value)
	{
		___Arg5_6 = value;
		Il2CppCodeGenWriteBarrier(&___Arg5_6, value);
	}

	inline static int32_t get_offset_of_Type_7() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Type_7)); }
	inline String_t* get_Type_7() const { return ___Type_7; }
	inline String_t** get_address_of_Type_7() { return &___Type_7; }
	inline void set_Type_7(String_t* value)
	{
		___Type_7 = value;
		Il2CppCodeGenWriteBarrier(&___Type_7, value);
	}

	inline static int32_t get_offset_of_Text_8() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Text_8)); }
	inline String_t* get_Text_8() const { return ___Text_8; }
	inline String_t** get_address_of_Text_8() { return &___Text_8; }
	inline void set_Text_8(String_t* value)
	{
		___Text_8 = value;
		Il2CppCodeGenWriteBarrier(&___Text_8, value);
	}

	inline static int32_t get_offset_of_SubText_9() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___SubText_9)); }
	inline String_t* get_SubText_9() const { return ___SubText_9; }
	inline String_t** get_address_of_SubText_9() { return &___SubText_9; }
	inline void set_SubText_9(String_t* value)
	{
		___SubText_9 = value;
		Il2CppCodeGenWriteBarrier(&___SubText_9, value);
	}

	inline static int32_t get_offset_of_Voice_10() { return static_cast<int32_t>(offsetof(Param_t4123818474, ___Voice_10)); }
	inline String_t* get_Voice_10() const { return ___Voice_10; }
	inline String_t** get_address_of_Voice_10() { return &___Voice_10; }
	inline void set_Voice_10(String_t* value)
	{
		___Voice_10 = value;
		Il2CppCodeGenWriteBarrier(&___Voice_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
