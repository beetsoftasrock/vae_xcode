﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey7
struct U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey7::.ctor()
extern "C"  void U3CloadAssetBundlesU3Ec__AnonStorey7__ctor_m497943129 (U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleManager/<loadAssetBundles>c__Iterator1/<loadAssetBundles>c__AnonStorey7::<>m__0(System.String)
extern "C"  bool U3CloadAssetBundlesU3Ec__AnonStorey7_U3CU3Em__0_m184518306 (U3CloadAssetBundlesU3Ec__AnonStorey7_t1991153060 * __this, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
