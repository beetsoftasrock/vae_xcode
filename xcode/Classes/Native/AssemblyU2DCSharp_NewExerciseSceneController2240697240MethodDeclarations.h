﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewExerciseSceneController
struct NewExerciseSceneController_t2240697240;

#include "codegen/il2cpp-codegen.h"

// System.Void NewExerciseSceneController::.ctor()
extern "C"  void NewExerciseSceneController__ctor_m4101832595 (NewExerciseSceneController_t2240697240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewExerciseSceneController::AutoPlayCallback()
extern "C"  void NewExerciseSceneController_AutoPlayCallback_m2546940577 (NewExerciseSceneController_t2240697240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewExerciseSceneController::LoadLevel()
extern "C"  void NewExerciseSceneController_LoadLevel_m2063428635 (NewExerciseSceneController_t2240697240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewExerciseSceneController::LoadLevelAutoPlay()
extern "C"  void NewExerciseSceneController_LoadLevelAutoPlay_m1375302620 (NewExerciseSceneController_t2240697240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewExerciseSceneController::Quit()
extern "C"  void NewExerciseSceneController_Quit_m3259386496 (NewExerciseSceneController_t2240697240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewExerciseSceneController::Awake()
extern "C"  void NewExerciseSceneController_Awake_m436583480 (NewExerciseSceneController_t2240697240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
