﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.Product
struct Product_t1203687971;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.Purchaser/<OnPurchaseFailed>c__AnonStorey3
struct  U3COnPurchaseFailedU3Ec__AnonStorey3_t1880872493  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.Product Beetsoft.VAE.Purchaser/<OnPurchaseFailed>c__AnonStorey3::product
	Product_t1203687971 * ___product_0;

public:
	inline static int32_t get_offset_of_product_0() { return static_cast<int32_t>(offsetof(U3COnPurchaseFailedU3Ec__AnonStorey3_t1880872493, ___product_0)); }
	inline Product_t1203687971 * get_product_0() const { return ___product_0; }
	inline Product_t1203687971 ** get_address_of_product_0() { return &___product_0; }
	inline void set_product_0(Product_t1203687971 * value)
	{
		___product_0 = value;
		Il2CppCodeGenWriteBarrier(&___product_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
