﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Button
struct Button_t2872111280;
// Table
struct Table_t4160395208;
// Graph
struct Graph_t2735713688;
// GlobalConfig
struct GlobalConfig_t3080413471;
// Cell
struct Cell_t3051913968;
// System.Action
struct Action_t3226471752;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_RunMode3692523130.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterMyPageScript
struct  ChapterMyPageScript_t3593529611  : public MonoBehaviour_t1158329972
{
public:
	// RunMode ChapterMyPageScript::runMode
	int32_t ___runMode_2;
	// UnityEngine.UI.Text ChapterMyPageScript::txtChapterName
	Text_t356221433 * ___txtChapterName_3;
	// UnityEngine.UI.Text ChapterMyPageScript::txtTime
	Text_t356221433 * ___txtTime_4;
	// UnityEngine.UI.Text ChapterMyPageScript::txtRecommend
	Text_t356221433 * ___txtRecommend_5;
	// UnityEngine.Sprite ChapterMyPageScript::sprStatusIncrease
	Sprite_t309593783 * ___sprStatusIncrease_6;
	// UnityEngine.Sprite ChapterMyPageScript::sprStatusDecrease
	Sprite_t309593783 * ___sprStatusDecrease_7;
	// UnityEngine.Sprite ChapterMyPageScript::sprStatusNoChange
	Sprite_t309593783 * ___sprStatusNoChange_8;
	// UnityEngine.UI.Button ChapterMyPageScript::btnStudy
	Button_t2872111280 * ___btnStudy_9;
	// Table ChapterMyPageScript::table
	Table_t4160395208 * ___table_10;
	// Graph ChapterMyPageScript::graphOldValue
	Graph_t2735713688 * ___graphOldValue_11;
	// Graph ChapterMyPageScript::graphNewValue
	Graph_t2735713688 * ___graphNewValue_12;
	// System.Int32 ChapterMyPageScript::chapter
	int32_t ___chapter_13;
	// GlobalConfig ChapterMyPageScript::globalConfig
	GlobalConfig_t3080413471 * ___globalConfig_14;
	// System.Single ChapterMyPageScript::totalTimeLeant
	float ___totalTimeLeant_15;
	// Cell ChapterMyPageScript::minCell
	Cell_t3051913968 * ___minCell_16;

public:
	inline static int32_t get_offset_of_runMode_2() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___runMode_2)); }
	inline int32_t get_runMode_2() const { return ___runMode_2; }
	inline int32_t* get_address_of_runMode_2() { return &___runMode_2; }
	inline void set_runMode_2(int32_t value)
	{
		___runMode_2 = value;
	}

	inline static int32_t get_offset_of_txtChapterName_3() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___txtChapterName_3)); }
	inline Text_t356221433 * get_txtChapterName_3() const { return ___txtChapterName_3; }
	inline Text_t356221433 ** get_address_of_txtChapterName_3() { return &___txtChapterName_3; }
	inline void set_txtChapterName_3(Text_t356221433 * value)
	{
		___txtChapterName_3 = value;
		Il2CppCodeGenWriteBarrier(&___txtChapterName_3, value);
	}

	inline static int32_t get_offset_of_txtTime_4() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___txtTime_4)); }
	inline Text_t356221433 * get_txtTime_4() const { return ___txtTime_4; }
	inline Text_t356221433 ** get_address_of_txtTime_4() { return &___txtTime_4; }
	inline void set_txtTime_4(Text_t356221433 * value)
	{
		___txtTime_4 = value;
		Il2CppCodeGenWriteBarrier(&___txtTime_4, value);
	}

	inline static int32_t get_offset_of_txtRecommend_5() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___txtRecommend_5)); }
	inline Text_t356221433 * get_txtRecommend_5() const { return ___txtRecommend_5; }
	inline Text_t356221433 ** get_address_of_txtRecommend_5() { return &___txtRecommend_5; }
	inline void set_txtRecommend_5(Text_t356221433 * value)
	{
		___txtRecommend_5 = value;
		Il2CppCodeGenWriteBarrier(&___txtRecommend_5, value);
	}

	inline static int32_t get_offset_of_sprStatusIncrease_6() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___sprStatusIncrease_6)); }
	inline Sprite_t309593783 * get_sprStatusIncrease_6() const { return ___sprStatusIncrease_6; }
	inline Sprite_t309593783 ** get_address_of_sprStatusIncrease_6() { return &___sprStatusIncrease_6; }
	inline void set_sprStatusIncrease_6(Sprite_t309593783 * value)
	{
		___sprStatusIncrease_6 = value;
		Il2CppCodeGenWriteBarrier(&___sprStatusIncrease_6, value);
	}

	inline static int32_t get_offset_of_sprStatusDecrease_7() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___sprStatusDecrease_7)); }
	inline Sprite_t309593783 * get_sprStatusDecrease_7() const { return ___sprStatusDecrease_7; }
	inline Sprite_t309593783 ** get_address_of_sprStatusDecrease_7() { return &___sprStatusDecrease_7; }
	inline void set_sprStatusDecrease_7(Sprite_t309593783 * value)
	{
		___sprStatusDecrease_7 = value;
		Il2CppCodeGenWriteBarrier(&___sprStatusDecrease_7, value);
	}

	inline static int32_t get_offset_of_sprStatusNoChange_8() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___sprStatusNoChange_8)); }
	inline Sprite_t309593783 * get_sprStatusNoChange_8() const { return ___sprStatusNoChange_8; }
	inline Sprite_t309593783 ** get_address_of_sprStatusNoChange_8() { return &___sprStatusNoChange_8; }
	inline void set_sprStatusNoChange_8(Sprite_t309593783 * value)
	{
		___sprStatusNoChange_8 = value;
		Il2CppCodeGenWriteBarrier(&___sprStatusNoChange_8, value);
	}

	inline static int32_t get_offset_of_btnStudy_9() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___btnStudy_9)); }
	inline Button_t2872111280 * get_btnStudy_9() const { return ___btnStudy_9; }
	inline Button_t2872111280 ** get_address_of_btnStudy_9() { return &___btnStudy_9; }
	inline void set_btnStudy_9(Button_t2872111280 * value)
	{
		___btnStudy_9 = value;
		Il2CppCodeGenWriteBarrier(&___btnStudy_9, value);
	}

	inline static int32_t get_offset_of_table_10() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___table_10)); }
	inline Table_t4160395208 * get_table_10() const { return ___table_10; }
	inline Table_t4160395208 ** get_address_of_table_10() { return &___table_10; }
	inline void set_table_10(Table_t4160395208 * value)
	{
		___table_10 = value;
		Il2CppCodeGenWriteBarrier(&___table_10, value);
	}

	inline static int32_t get_offset_of_graphOldValue_11() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___graphOldValue_11)); }
	inline Graph_t2735713688 * get_graphOldValue_11() const { return ___graphOldValue_11; }
	inline Graph_t2735713688 ** get_address_of_graphOldValue_11() { return &___graphOldValue_11; }
	inline void set_graphOldValue_11(Graph_t2735713688 * value)
	{
		___graphOldValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___graphOldValue_11, value);
	}

	inline static int32_t get_offset_of_graphNewValue_12() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___graphNewValue_12)); }
	inline Graph_t2735713688 * get_graphNewValue_12() const { return ___graphNewValue_12; }
	inline Graph_t2735713688 ** get_address_of_graphNewValue_12() { return &___graphNewValue_12; }
	inline void set_graphNewValue_12(Graph_t2735713688 * value)
	{
		___graphNewValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___graphNewValue_12, value);
	}

	inline static int32_t get_offset_of_chapter_13() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___chapter_13)); }
	inline int32_t get_chapter_13() const { return ___chapter_13; }
	inline int32_t* get_address_of_chapter_13() { return &___chapter_13; }
	inline void set_chapter_13(int32_t value)
	{
		___chapter_13 = value;
	}

	inline static int32_t get_offset_of_globalConfig_14() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___globalConfig_14)); }
	inline GlobalConfig_t3080413471 * get_globalConfig_14() const { return ___globalConfig_14; }
	inline GlobalConfig_t3080413471 ** get_address_of_globalConfig_14() { return &___globalConfig_14; }
	inline void set_globalConfig_14(GlobalConfig_t3080413471 * value)
	{
		___globalConfig_14 = value;
		Il2CppCodeGenWriteBarrier(&___globalConfig_14, value);
	}

	inline static int32_t get_offset_of_totalTimeLeant_15() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___totalTimeLeant_15)); }
	inline float get_totalTimeLeant_15() const { return ___totalTimeLeant_15; }
	inline float* get_address_of_totalTimeLeant_15() { return &___totalTimeLeant_15; }
	inline void set_totalTimeLeant_15(float value)
	{
		___totalTimeLeant_15 = value;
	}

	inline static int32_t get_offset_of_minCell_16() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611, ___minCell_16)); }
	inline Cell_t3051913968 * get_minCell_16() const { return ___minCell_16; }
	inline Cell_t3051913968 ** get_address_of_minCell_16() { return &___minCell_16; }
	inline void set_minCell_16(Cell_t3051913968 * value)
	{
		___minCell_16 = value;
		Il2CppCodeGenWriteBarrier(&___minCell_16, value);
	}
};

struct ChapterMyPageScript_t3593529611_StaticFields
{
public:
	// System.Action ChapterMyPageScript::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(ChapterMyPageScript_t3593529611_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
