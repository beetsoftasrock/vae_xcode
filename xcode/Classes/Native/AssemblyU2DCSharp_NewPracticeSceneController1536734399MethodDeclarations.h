﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NewPracticeSceneController
struct NewPracticeSceneController_t1536734399;

#include "codegen/il2cpp-codegen.h"

// System.Void NewPracticeSceneController::.ctor()
extern "C"  void NewPracticeSceneController__ctor_m807991880 (NewPracticeSceneController_t1536734399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewPracticeSceneController::AutoPlayCallback()
extern "C"  void NewPracticeSceneController_AutoPlayCallback_m3824469840 (NewPracticeSceneController_t1536734399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewPracticeSceneController::LoadLevel()
extern "C"  void NewPracticeSceneController_LoadLevel_m1381018742 (NewPracticeSceneController_t1536734399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewPracticeSceneController::LoadLevelAutoPlay()
extern "C"  void NewPracticeSceneController_LoadLevelAutoPlay_m2926909677 (NewPracticeSceneController_t1536734399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NewPracticeSceneController::Quit()
extern "C"  void NewPracticeSceneController_Quit_m291295435 (NewPracticeSceneController_t1536734399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
