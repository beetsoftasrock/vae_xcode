﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputSpectrum/Band
struct Band_t1120454049;
struct Band_t1120454049_marshaled_pinvoke;
struct Band_t1120454049_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Band_t1120454049;
struct Band_t1120454049_marshaled_pinvoke;

extern "C" void Band_t1120454049_marshal_pinvoke(const Band_t1120454049& unmarshaled, Band_t1120454049_marshaled_pinvoke& marshaled);
extern "C" void Band_t1120454049_marshal_pinvoke_back(const Band_t1120454049_marshaled_pinvoke& marshaled, Band_t1120454049& unmarshaled);
extern "C" void Band_t1120454049_marshal_pinvoke_cleanup(Band_t1120454049_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Band_t1120454049;
struct Band_t1120454049_marshaled_com;

extern "C" void Band_t1120454049_marshal_com(const Band_t1120454049& unmarshaled, Band_t1120454049_marshaled_com& marshaled);
extern "C" void Band_t1120454049_marshal_com_back(const Band_t1120454049_marshaled_com& marshaled, Band_t1120454049& unmarshaled);
extern "C" void Band_t1120454049_marshal_com_cleanup(Band_t1120454049_marshaled_com& marshaled);
