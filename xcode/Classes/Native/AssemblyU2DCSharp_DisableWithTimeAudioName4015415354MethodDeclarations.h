﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisableWithTimeAudioName
struct DisableWithTimeAudioName_t4015415354;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void DisableWithTimeAudioName::.ctor()
extern "C"  void DisableWithTimeAudioName__ctor_m299303225 (DisableWithTimeAudioName_t4015415354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeAudioName::OnEnable()
extern "C"  void DisableWithTimeAudioName_OnEnable_m3599537465 (DisableWithTimeAudioName_t4015415354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableWithTimeAudioName::StartCountdown()
extern "C"  void DisableWithTimeAudioName_StartCountdown_m1648647902 (DisableWithTimeAudioName_t4015415354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DisableWithTimeAudioName::PlayAudioAndDisable()
extern "C"  Il2CppObject * DisableWithTimeAudioName_PlayAudioAndDisable_m322549174 (DisableWithTimeAudioName_t4015415354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
