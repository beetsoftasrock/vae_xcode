﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<ConversationHistoryItemView>
struct List_1_t3215882237;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HistoryConversationDialog
struct  HistoryConversationDialog_t3676407505  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean HistoryConversationDialog::_showSubText
	bool ____showSubText_2;
	// UnityEngine.GameObject HistoryConversationDialog::conversationHistoryItemPrefab
	GameObject_t1756533147 * ___conversationHistoryItemPrefab_3;
	// UnityEngine.Transform HistoryConversationDialog::viewPort
	Transform_t3275118058 * ___viewPort_4;
	// UnityEngine.Transform HistoryConversationDialog::pointAnchor
	Transform_t3275118058 * ___pointAnchor_5;
	// System.Collections.Generic.List`1<ConversationHistoryItemView> HistoryConversationDialog::_historyItems
	List_1_t3215882237 * ____historyItems_6;
	// System.Single HistoryConversationDialog::speedLerp
	float ___speedLerp_7;
	// System.Single HistoryConversationDialog::timeLerp
	float ___timeLerp_8;

public:
	inline static int32_t get_offset_of__showSubText_2() { return static_cast<int32_t>(offsetof(HistoryConversationDialog_t3676407505, ____showSubText_2)); }
	inline bool get__showSubText_2() const { return ____showSubText_2; }
	inline bool* get_address_of__showSubText_2() { return &____showSubText_2; }
	inline void set__showSubText_2(bool value)
	{
		____showSubText_2 = value;
	}

	inline static int32_t get_offset_of_conversationHistoryItemPrefab_3() { return static_cast<int32_t>(offsetof(HistoryConversationDialog_t3676407505, ___conversationHistoryItemPrefab_3)); }
	inline GameObject_t1756533147 * get_conversationHistoryItemPrefab_3() const { return ___conversationHistoryItemPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_conversationHistoryItemPrefab_3() { return &___conversationHistoryItemPrefab_3; }
	inline void set_conversationHistoryItemPrefab_3(GameObject_t1756533147 * value)
	{
		___conversationHistoryItemPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___conversationHistoryItemPrefab_3, value);
	}

	inline static int32_t get_offset_of_viewPort_4() { return static_cast<int32_t>(offsetof(HistoryConversationDialog_t3676407505, ___viewPort_4)); }
	inline Transform_t3275118058 * get_viewPort_4() const { return ___viewPort_4; }
	inline Transform_t3275118058 ** get_address_of_viewPort_4() { return &___viewPort_4; }
	inline void set_viewPort_4(Transform_t3275118058 * value)
	{
		___viewPort_4 = value;
		Il2CppCodeGenWriteBarrier(&___viewPort_4, value);
	}

	inline static int32_t get_offset_of_pointAnchor_5() { return static_cast<int32_t>(offsetof(HistoryConversationDialog_t3676407505, ___pointAnchor_5)); }
	inline Transform_t3275118058 * get_pointAnchor_5() const { return ___pointAnchor_5; }
	inline Transform_t3275118058 ** get_address_of_pointAnchor_5() { return &___pointAnchor_5; }
	inline void set_pointAnchor_5(Transform_t3275118058 * value)
	{
		___pointAnchor_5 = value;
		Il2CppCodeGenWriteBarrier(&___pointAnchor_5, value);
	}

	inline static int32_t get_offset_of__historyItems_6() { return static_cast<int32_t>(offsetof(HistoryConversationDialog_t3676407505, ____historyItems_6)); }
	inline List_1_t3215882237 * get__historyItems_6() const { return ____historyItems_6; }
	inline List_1_t3215882237 ** get_address_of__historyItems_6() { return &____historyItems_6; }
	inline void set__historyItems_6(List_1_t3215882237 * value)
	{
		____historyItems_6 = value;
		Il2CppCodeGenWriteBarrier(&____historyItems_6, value);
	}

	inline static int32_t get_offset_of_speedLerp_7() { return static_cast<int32_t>(offsetof(HistoryConversationDialog_t3676407505, ___speedLerp_7)); }
	inline float get_speedLerp_7() const { return ___speedLerp_7; }
	inline float* get_address_of_speedLerp_7() { return &___speedLerp_7; }
	inline void set_speedLerp_7(float value)
	{
		___speedLerp_7 = value;
	}

	inline static int32_t get_offset_of_timeLerp_8() { return static_cast<int32_t>(offsetof(HistoryConversationDialog_t3676407505, ___timeLerp_8)); }
	inline float get_timeLerp_8() const { return ___timeLerp_8; }
	inline float* get_address_of_timeLerp_8() { return &___timeLerp_8; }
	inline void set_timeLerp_8(float value)
	{
		___timeLerp_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
