﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenHelpScript/TrackIndexObject
struct TrackIndexObject_t1096184091;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenHelpScript/TrackIndexObject::.ctor()
extern "C"  void TrackIndexObject__ctor_m2492951694 (TrackIndexObject_t1096184091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
