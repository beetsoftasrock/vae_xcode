﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationVRSwitcher
struct ConversationVRSwitcher_t368413640;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void ConversationVRSwitcher::.ctor()
extern "C"  void ConversationVRSwitcher__ctor_m4090695671 (ConversationVRSwitcher_t368413640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher::GoToNormalMode(UnityEngine.Transform)
extern "C"  void ConversationVRSwitcher_GoToNormalMode_m2437812287 (ConversationVRSwitcher_t368413640 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher::YesGoToVRMode()
extern "C"  void ConversationVRSwitcher_YesGoToVRMode_m319973856 (ConversationVRSwitcher_t368413640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher::GoToVRMode(UnityEngine.Transform)
extern "C"  void ConversationVRSwitcher_GoToVRMode_m4201973824 (ConversationVRSwitcher_t368413640 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher::BackToTop()
extern "C"  void ConversationVRSwitcher_BackToTop_m3550921114 (ConversationVRSwitcher_t368413640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher::QuitVRMode(UnityEngine.Transform)
extern "C"  void ConversationVRSwitcher_QuitVRMode_m2361784672 (ConversationVRSwitcher_t368413640 * __this, Transform_t3275118058 * ___parentPopupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher::YesQuitVRMode()
extern "C"  void ConversationVRSwitcher_YesQuitVRMode_m610595764 (ConversationVRSwitcher_t368413640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher::NoQuitVRMode(UnityEngine.Transform)
extern "C"  void ConversationVRSwitcher_NoQuitVRMode_m965873211 (ConversationVRSwitcher_t368413640 * __this, Transform_t3275118058 * ___popupQuit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationVRSwitcher::<GoToNormalMode>m__0()
extern "C"  void ConversationVRSwitcher_U3CGoToNormalModeU3Em__0_m194266419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
