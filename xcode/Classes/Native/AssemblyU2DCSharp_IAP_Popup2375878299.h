﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAP_Popup
struct  IAP_Popup_t2375878299  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject IAP_Popup::buttonsGroup
	GameObject_t1756533147 * ___buttonsGroup_2;
	// UnityEngine.UI.Button IAP_Popup::buttonBuyFull
	Button_t2872111280 * ___buttonBuyFull_3;
	// UnityEngine.UI.Button IAP_Popup::buttonRestore
	Button_t2872111280 * ___buttonRestore_4;
	// UnityEngine.UI.Text IAP_Popup::textPurchased
	Text_t356221433 * ___textPurchased_5;
	// UnityEngine.GameObject IAP_Popup::msgRestoreFail
	GameObject_t1756533147 * ___msgRestoreFail_6;

public:
	inline static int32_t get_offset_of_buttonsGroup_2() { return static_cast<int32_t>(offsetof(IAP_Popup_t2375878299, ___buttonsGroup_2)); }
	inline GameObject_t1756533147 * get_buttonsGroup_2() const { return ___buttonsGroup_2; }
	inline GameObject_t1756533147 ** get_address_of_buttonsGroup_2() { return &___buttonsGroup_2; }
	inline void set_buttonsGroup_2(GameObject_t1756533147 * value)
	{
		___buttonsGroup_2 = value;
		Il2CppCodeGenWriteBarrier(&___buttonsGroup_2, value);
	}

	inline static int32_t get_offset_of_buttonBuyFull_3() { return static_cast<int32_t>(offsetof(IAP_Popup_t2375878299, ___buttonBuyFull_3)); }
	inline Button_t2872111280 * get_buttonBuyFull_3() const { return ___buttonBuyFull_3; }
	inline Button_t2872111280 ** get_address_of_buttonBuyFull_3() { return &___buttonBuyFull_3; }
	inline void set_buttonBuyFull_3(Button_t2872111280 * value)
	{
		___buttonBuyFull_3 = value;
		Il2CppCodeGenWriteBarrier(&___buttonBuyFull_3, value);
	}

	inline static int32_t get_offset_of_buttonRestore_4() { return static_cast<int32_t>(offsetof(IAP_Popup_t2375878299, ___buttonRestore_4)); }
	inline Button_t2872111280 * get_buttonRestore_4() const { return ___buttonRestore_4; }
	inline Button_t2872111280 ** get_address_of_buttonRestore_4() { return &___buttonRestore_4; }
	inline void set_buttonRestore_4(Button_t2872111280 * value)
	{
		___buttonRestore_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttonRestore_4, value);
	}

	inline static int32_t get_offset_of_textPurchased_5() { return static_cast<int32_t>(offsetof(IAP_Popup_t2375878299, ___textPurchased_5)); }
	inline Text_t356221433 * get_textPurchased_5() const { return ___textPurchased_5; }
	inline Text_t356221433 ** get_address_of_textPurchased_5() { return &___textPurchased_5; }
	inline void set_textPurchased_5(Text_t356221433 * value)
	{
		___textPurchased_5 = value;
		Il2CppCodeGenWriteBarrier(&___textPurchased_5, value);
	}

	inline static int32_t get_offset_of_msgRestoreFail_6() { return static_cast<int32_t>(offsetof(IAP_Popup_t2375878299, ___msgRestoreFail_6)); }
	inline GameObject_t1756533147 * get_msgRestoreFail_6() const { return ___msgRestoreFail_6; }
	inline GameObject_t1756533147 ** get_address_of_msgRestoreFail_6() { return &___msgRestoreFail_6; }
	inline void set_msgRestoreFail_6(GameObject_t1756533147 * value)
	{
		___msgRestoreFail_6 = value;
		Il2CppCodeGenWriteBarrier(&___msgRestoreFail_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
