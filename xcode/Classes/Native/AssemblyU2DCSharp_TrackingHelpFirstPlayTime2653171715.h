﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CallHelpInSceneScript
struct CallHelpInSceneScript_t2709929151;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackingHelpFirstPlayTime
struct  TrackingHelpFirstPlayTime_t2653171715  : public MonoBehaviour_t1158329972
{
public:
	// CallHelpInSceneScript TrackingHelpFirstPlayTime::_helpControl
	CallHelpInSceneScript_t2709929151 * ____helpControl_2;

public:
	inline static int32_t get_offset_of__helpControl_2() { return static_cast<int32_t>(offsetof(TrackingHelpFirstPlayTime_t2653171715, ____helpControl_2)); }
	inline CallHelpInSceneScript_t2709929151 * get__helpControl_2() const { return ____helpControl_2; }
	inline CallHelpInSceneScript_t2709929151 ** get_address_of__helpControl_2() { return &____helpControl_2; }
	inline void set__helpControl_2(CallHelpInSceneScript_t2709929151 * value)
	{
		____helpControl_2 = value;
		Il2CppCodeGenWriteBarrier(&____helpControl_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
