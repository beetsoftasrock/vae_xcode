﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VocabularyQuestionView
struct VocabularyQuestionView_t1631345783;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void VocabularyQuestionView::.ctor()
extern "C"  void VocabularyQuestionView__ctor_m877557588 (VocabularyQuestionView_t1631345783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionView::ClearView()
extern "C"  void VocabularyQuestionView_ClearView_m493365484 (VocabularyQuestionView_t1631345783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VocabularyQuestionView::IsLockedInteractive()
extern "C"  bool VocabularyQuestionView_IsLockedInteractive_m1279439416 (VocabularyQuestionView_t1631345783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocabularyQuestionView::ShowResult(System.Boolean,System.Int32)
extern "C"  void VocabularyQuestionView_ShowResult_m853874258 (VocabularyQuestionView_t1631345783 * __this, bool ___isCorrect0, int32_t ___correctAnswerId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VocabularyQuestionView::PlayAnimation()
extern "C"  Il2CppObject * VocabularyQuestionView_PlayAnimation_m1984282264 (VocabularyQuestionView_t1631345783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
