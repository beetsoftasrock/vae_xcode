﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRTutorial
struct  VRTutorial_t3527795282  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VRTutorial::popupContructorPlugDevice
	GameObject_t1756533147 * ___popupContructorPlugDevice_2;
	// UnityEngine.GameObject VRTutorial::popupBeforeIntoVR
	GameObject_t1756533147 * ___popupBeforeIntoVR_3;
	// UnityEngine.GameObject VRTutorial::vRMainArea
	GameObject_t1756533147 * ___vRMainArea_4;

public:
	inline static int32_t get_offset_of_popupContructorPlugDevice_2() { return static_cast<int32_t>(offsetof(VRTutorial_t3527795282, ___popupContructorPlugDevice_2)); }
	inline GameObject_t1756533147 * get_popupContructorPlugDevice_2() const { return ___popupContructorPlugDevice_2; }
	inline GameObject_t1756533147 ** get_address_of_popupContructorPlugDevice_2() { return &___popupContructorPlugDevice_2; }
	inline void set_popupContructorPlugDevice_2(GameObject_t1756533147 * value)
	{
		___popupContructorPlugDevice_2 = value;
		Il2CppCodeGenWriteBarrier(&___popupContructorPlugDevice_2, value);
	}

	inline static int32_t get_offset_of_popupBeforeIntoVR_3() { return static_cast<int32_t>(offsetof(VRTutorial_t3527795282, ___popupBeforeIntoVR_3)); }
	inline GameObject_t1756533147 * get_popupBeforeIntoVR_3() const { return ___popupBeforeIntoVR_3; }
	inline GameObject_t1756533147 ** get_address_of_popupBeforeIntoVR_3() { return &___popupBeforeIntoVR_3; }
	inline void set_popupBeforeIntoVR_3(GameObject_t1756533147 * value)
	{
		___popupBeforeIntoVR_3 = value;
		Il2CppCodeGenWriteBarrier(&___popupBeforeIntoVR_3, value);
	}

	inline static int32_t get_offset_of_vRMainArea_4() { return static_cast<int32_t>(offsetof(VRTutorial_t3527795282, ___vRMainArea_4)); }
	inline GameObject_t1756533147 * get_vRMainArea_4() const { return ___vRMainArea_4; }
	inline GameObject_t1756533147 ** get_address_of_vRMainArea_4() { return &___vRMainArea_4; }
	inline void set_vRMainArea_4(GameObject_t1756533147 * value)
	{
		___vRMainArea_4 = value;
		Il2CppCodeGenWriteBarrier(&___vRMainArea_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
