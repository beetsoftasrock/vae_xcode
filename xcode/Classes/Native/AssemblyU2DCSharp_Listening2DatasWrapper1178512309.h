﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;
// System.Collections.Generic.List`1<Listening2QuestionData>
struct List_1_t1813096251;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening2DatasWrapper
struct  Listening2DatasWrapper_t1178512309  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<CommandSheet/Param> Listening2DatasWrapper::_wrappedDatas
	List_1_t3492939606 * ____wrappedDatas_0;
	// System.Collections.Generic.List`1<Listening2QuestionData> Listening2DatasWrapper::_convertedDatas
	List_1_t1813096251 * ____convertedDatas_1;

public:
	inline static int32_t get_offset_of__wrappedDatas_0() { return static_cast<int32_t>(offsetof(Listening2DatasWrapper_t1178512309, ____wrappedDatas_0)); }
	inline List_1_t3492939606 * get__wrappedDatas_0() const { return ____wrappedDatas_0; }
	inline List_1_t3492939606 ** get_address_of__wrappedDatas_0() { return &____wrappedDatas_0; }
	inline void set__wrappedDatas_0(List_1_t3492939606 * value)
	{
		____wrappedDatas_0 = value;
		Il2CppCodeGenWriteBarrier(&____wrappedDatas_0, value);
	}

	inline static int32_t get_offset_of__convertedDatas_1() { return static_cast<int32_t>(offsetof(Listening2DatasWrapper_t1178512309, ____convertedDatas_1)); }
	inline List_1_t1813096251 * get__convertedDatas_1() const { return ____convertedDatas_1; }
	inline List_1_t1813096251 ** get_address_of__convertedDatas_1() { return &____convertedDatas_1; }
	inline void set__convertedDatas_1(List_1_t1813096251 * value)
	{
		____convertedDatas_1 = value;
		Il2CppCodeGenWriteBarrier(&____convertedDatas_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
