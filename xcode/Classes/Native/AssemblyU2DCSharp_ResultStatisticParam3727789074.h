﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultStatisticParam
struct  ResultStatisticParam_t3727789074  : public MonoBehaviour_t1158329972
{
public:
	// System.String ResultStatisticParam::nameResult
	String_t* ___nameResult_2;
	// System.Int32 ResultStatisticParam::totalAnswerCorrect
	int32_t ___totalAnswerCorrect_3;
	// System.Int32 ResultStatisticParam::totalAnswerWrong
	int32_t ___totalAnswerWrong_4;
	// System.Int32 ResultStatisticParam::totalAnwerCorrectListening
	int32_t ___totalAnwerCorrectListening_5;
	// System.Int32 ResultStatisticParam::totalAnwerWrongListening
	int32_t ___totalAnwerWrongListening_6;
	// System.Int32 ResultStatisticParam::frequencyLearnToday
	int32_t ___frequencyLearnToday_7;
	// System.Int32 ResultStatisticParam::frequencyLearnTotal
	int32_t ___frequencyLearnTotal_8;
	// System.Single ResultStatisticParam::timeLearnToday
	float ___timeLearnToday_9;
	// System.Single ResultStatisticParam::timeLearnTotal
	float ___timeLearnTotal_10;

public:
	inline static int32_t get_offset_of_nameResult_2() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___nameResult_2)); }
	inline String_t* get_nameResult_2() const { return ___nameResult_2; }
	inline String_t** get_address_of_nameResult_2() { return &___nameResult_2; }
	inline void set_nameResult_2(String_t* value)
	{
		___nameResult_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameResult_2, value);
	}

	inline static int32_t get_offset_of_totalAnswerCorrect_3() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___totalAnswerCorrect_3)); }
	inline int32_t get_totalAnswerCorrect_3() const { return ___totalAnswerCorrect_3; }
	inline int32_t* get_address_of_totalAnswerCorrect_3() { return &___totalAnswerCorrect_3; }
	inline void set_totalAnswerCorrect_3(int32_t value)
	{
		___totalAnswerCorrect_3 = value;
	}

	inline static int32_t get_offset_of_totalAnswerWrong_4() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___totalAnswerWrong_4)); }
	inline int32_t get_totalAnswerWrong_4() const { return ___totalAnswerWrong_4; }
	inline int32_t* get_address_of_totalAnswerWrong_4() { return &___totalAnswerWrong_4; }
	inline void set_totalAnswerWrong_4(int32_t value)
	{
		___totalAnswerWrong_4 = value;
	}

	inline static int32_t get_offset_of_totalAnwerCorrectListening_5() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___totalAnwerCorrectListening_5)); }
	inline int32_t get_totalAnwerCorrectListening_5() const { return ___totalAnwerCorrectListening_5; }
	inline int32_t* get_address_of_totalAnwerCorrectListening_5() { return &___totalAnwerCorrectListening_5; }
	inline void set_totalAnwerCorrectListening_5(int32_t value)
	{
		___totalAnwerCorrectListening_5 = value;
	}

	inline static int32_t get_offset_of_totalAnwerWrongListening_6() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___totalAnwerWrongListening_6)); }
	inline int32_t get_totalAnwerWrongListening_6() const { return ___totalAnwerWrongListening_6; }
	inline int32_t* get_address_of_totalAnwerWrongListening_6() { return &___totalAnwerWrongListening_6; }
	inline void set_totalAnwerWrongListening_6(int32_t value)
	{
		___totalAnwerWrongListening_6 = value;
	}

	inline static int32_t get_offset_of_frequencyLearnToday_7() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___frequencyLearnToday_7)); }
	inline int32_t get_frequencyLearnToday_7() const { return ___frequencyLearnToday_7; }
	inline int32_t* get_address_of_frequencyLearnToday_7() { return &___frequencyLearnToday_7; }
	inline void set_frequencyLearnToday_7(int32_t value)
	{
		___frequencyLearnToday_7 = value;
	}

	inline static int32_t get_offset_of_frequencyLearnTotal_8() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___frequencyLearnTotal_8)); }
	inline int32_t get_frequencyLearnTotal_8() const { return ___frequencyLearnTotal_8; }
	inline int32_t* get_address_of_frequencyLearnTotal_8() { return &___frequencyLearnTotal_8; }
	inline void set_frequencyLearnTotal_8(int32_t value)
	{
		___frequencyLearnTotal_8 = value;
	}

	inline static int32_t get_offset_of_timeLearnToday_9() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___timeLearnToday_9)); }
	inline float get_timeLearnToday_9() const { return ___timeLearnToday_9; }
	inline float* get_address_of_timeLearnToday_9() { return &___timeLearnToday_9; }
	inline void set_timeLearnToday_9(float value)
	{
		___timeLearnToday_9 = value;
	}

	inline static int32_t get_offset_of_timeLearnTotal_10() { return static_cast<int32_t>(offsetof(ResultStatisticParam_t3727789074, ___timeLearnTotal_10)); }
	inline float get_timeLearnTotal_10() const { return ___timeLearnTotal_10; }
	inline float* get_address_of_timeLearnTotal_10() { return &___timeLearnTotal_10; }
	inline void set_timeLearnTotal_10(float value)
	{
		___timeLearnTotal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
