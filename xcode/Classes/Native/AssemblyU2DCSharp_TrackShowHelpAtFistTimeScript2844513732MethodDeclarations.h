﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrackShowHelpAtFistTimeScript
struct TrackShowHelpAtFistTimeScript_t2844513732;

#include "codegen/il2cpp-codegen.h"

// System.Void TrackShowHelpAtFistTimeScript::.ctor()
extern "C"  void TrackShowHelpAtFistTimeScript__ctor_m3244292177 (TrackShowHelpAtFistTimeScript_t2844513732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackShowHelpAtFistTimeScript::Start()
extern "C"  void TrackShowHelpAtFistTimeScript_Start_m1945300873 (TrackShowHelpAtFistTimeScript_t2844513732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackShowHelpAtFistTimeScript::Update()
extern "C"  void TrackShowHelpAtFistTimeScript_Update_m925194894 (TrackShowHelpAtFistTimeScript_t2844513732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
