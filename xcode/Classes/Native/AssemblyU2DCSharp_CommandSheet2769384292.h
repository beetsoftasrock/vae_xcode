﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CommandSheet/Param>
struct List_1_t3492939606;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandSheet
struct  CommandSheet_t2769384292  : public ScriptableObject_t1975622470
{
public:
	// System.Collections.Generic.List`1<CommandSheet/Param> CommandSheet::param
	List_1_t3492939606 * ___param_2;

public:
	inline static int32_t get_offset_of_param_2() { return static_cast<int32_t>(offsetof(CommandSheet_t2769384292, ___param_2)); }
	inline List_1_t3492939606 * get_param_2() const { return ___param_2; }
	inline List_1_t3492939606 ** get_address_of_param_2() { return &___param_2; }
	inline void set_param_2(List_1_t3492939606 * value)
	{
		___param_2 = value;
		Il2CppCodeGenWriteBarrier(&___param_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
