﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StopEffectCommand/<Execute>c__Iterator0
struct U3CExecuteU3Ec__Iterator0_t386014617;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StopEffectCommand/<Execute>c__Iterator0::.ctor()
extern "C"  void U3CExecuteU3Ec__Iterator0__ctor_m2789743484 (U3CExecuteU3Ec__Iterator0_t386014617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StopEffectCommand/<Execute>c__Iterator0::MoveNext()
extern "C"  bool U3CExecuteU3Ec__Iterator0_MoveNext_m2650434564 (U3CExecuteU3Ec__Iterator0_t386014617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StopEffectCommand/<Execute>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1918778250 (U3CExecuteU3Ec__Iterator0_t386014617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StopEffectCommand/<Execute>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1239350370 (U3CExecuteU3Ec__Iterator0_t386014617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StopEffectCommand/<Execute>c__Iterator0::Dispose()
extern "C"  void U3CExecuteU3Ec__Iterator0_Dispose_m2987884387 (U3CExecuteU3Ec__Iterator0_t386014617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StopEffectCommand/<Execute>c__Iterator0::Reset()
extern "C"  void U3CExecuteU3Ec__Iterator0_Reset_m2464682625 (U3CExecuteU3Ec__Iterator0_t386014617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
