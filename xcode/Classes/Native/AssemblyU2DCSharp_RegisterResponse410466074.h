﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegisterResponse
struct  RegisterResponse_t410466074  : public Il2CppObject
{
public:
	// System.Boolean RegisterResponse::success
	bool ___success_0;
	// System.String RegisterResponse::errorMsg
	String_t* ___errorMsg_1;
	// System.String RegisterResponse::uuid
	String_t* ___uuid_2;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(RegisterResponse_t410466074, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_errorMsg_1() { return static_cast<int32_t>(offsetof(RegisterResponse_t410466074, ___errorMsg_1)); }
	inline String_t* get_errorMsg_1() const { return ___errorMsg_1; }
	inline String_t** get_address_of_errorMsg_1() { return &___errorMsg_1; }
	inline void set_errorMsg_1(String_t* value)
	{
		___errorMsg_1 = value;
		Il2CppCodeGenWriteBarrier(&___errorMsg_1, value);
	}

	inline static int32_t get_offset_of_uuid_2() { return static_cast<int32_t>(offsetof(RegisterResponse_t410466074, ___uuid_2)); }
	inline String_t* get_uuid_2() const { return ___uuid_2; }
	inline String_t** get_address_of_uuid_2() { return &___uuid_2; }
	inline void set_uuid_2(String_t* value)
	{
		___uuid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
