﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SoundRecorder
struct SoundRecorder_t3431875919;
// KaraokeTextEffect
struct KaraokeTextEffect_t1279919556;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayTalkDialog
struct  ReplayTalkDialog_t3732871695  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ReplayTalkDialog::_showSubText
	bool ____showSubText_2;
	// SoundRecorder ReplayTalkDialog::soundRecorder
	SoundRecorder_t3431875919 * ___soundRecorder_3;
	// KaraokeTextEffect ReplayTalkDialog::enTextEffect
	KaraokeTextEffect_t1279919556 * ___enTextEffect_4;
	// KaraokeTextEffect ReplayTalkDialog::jpTextEffect
	KaraokeTextEffect_t1279919556 * ___jpTextEffect_5;

public:
	inline static int32_t get_offset_of__showSubText_2() { return static_cast<int32_t>(offsetof(ReplayTalkDialog_t3732871695, ____showSubText_2)); }
	inline bool get__showSubText_2() const { return ____showSubText_2; }
	inline bool* get_address_of__showSubText_2() { return &____showSubText_2; }
	inline void set__showSubText_2(bool value)
	{
		____showSubText_2 = value;
	}

	inline static int32_t get_offset_of_soundRecorder_3() { return static_cast<int32_t>(offsetof(ReplayTalkDialog_t3732871695, ___soundRecorder_3)); }
	inline SoundRecorder_t3431875919 * get_soundRecorder_3() const { return ___soundRecorder_3; }
	inline SoundRecorder_t3431875919 ** get_address_of_soundRecorder_3() { return &___soundRecorder_3; }
	inline void set_soundRecorder_3(SoundRecorder_t3431875919 * value)
	{
		___soundRecorder_3 = value;
		Il2CppCodeGenWriteBarrier(&___soundRecorder_3, value);
	}

	inline static int32_t get_offset_of_enTextEffect_4() { return static_cast<int32_t>(offsetof(ReplayTalkDialog_t3732871695, ___enTextEffect_4)); }
	inline KaraokeTextEffect_t1279919556 * get_enTextEffect_4() const { return ___enTextEffect_4; }
	inline KaraokeTextEffect_t1279919556 ** get_address_of_enTextEffect_4() { return &___enTextEffect_4; }
	inline void set_enTextEffect_4(KaraokeTextEffect_t1279919556 * value)
	{
		___enTextEffect_4 = value;
		Il2CppCodeGenWriteBarrier(&___enTextEffect_4, value);
	}

	inline static int32_t get_offset_of_jpTextEffect_5() { return static_cast<int32_t>(offsetof(ReplayTalkDialog_t3732871695, ___jpTextEffect_5)); }
	inline KaraokeTextEffect_t1279919556 * get_jpTextEffect_5() const { return ___jpTextEffect_5; }
	inline KaraokeTextEffect_t1279919556 ** get_address_of_jpTextEffect_5() { return &___jpTextEffect_5; }
	inline void set_jpTextEffect_5(KaraokeTextEffect_t1279919556 * value)
	{
		___jpTextEffect_5 = value;
		Il2CppCodeGenWriteBarrier(&___jpTextEffect_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
