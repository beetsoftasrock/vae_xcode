﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FakeAppleConfiguation
struct FakeAppleConfiguation_t4052738437;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.FakeAppleConfiguation::.ctor()
extern "C"  void FakeAppleConfiguation__ctor_m217023035 (FakeAppleConfiguation_t4052738437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.FakeAppleConfiguation::get_appReceipt()
extern "C"  String_t* FakeAppleConfiguation_get_appReceipt_m4171590724 (FakeAppleConfiguation_t4052738437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
