﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TalkDialog
struct TalkDialog_t1412511810;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationTalkData1570298305.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TalkDialog::.ctor()
extern "C"  void TalkDialog__ctor_m2020284593 (TalkDialog_t1412511810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TalkDialog::get_showSubText()
extern "C"  bool TalkDialog_get_showSubText_m3306776594 (TalkDialog_t1412511810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkDialog::set_showSubText(System.Boolean)
extern "C"  void TalkDialog_set_showSubText_m1844691329 (TalkDialog_t1412511810 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConversationTalkData TalkDialog::get_data()
extern "C"  ConversationTalkData_t1570298305 * TalkDialog_get_data_m3989901632 (TalkDialog_t1412511810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkDialog::set_data(ConversationTalkData)
extern "C"  void TalkDialog_set_data_m1294132675 (TalkDialog_t1412511810 * __this, ConversationTalkData_t1570298305 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkDialog::SetData(ConversationTalkData)
extern "C"  void TalkDialog_SetData_m3592041958 (TalkDialog_t1412511810 * __this, ConversationTalkData_t1570298305 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkDialog::SetText(System.String,System.String)
extern "C"  void TalkDialog_SetText_m1592179176 (TalkDialog_t1412511810 * __this, String_t* ___strEn0, String_t* ___strJp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TalkDialog::UpdateView()
extern "C"  void TalkDialog_UpdateView_m2370392849 (TalkDialog_t1412511810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
