﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// VRHandleButton
struct VRHandleButton_t3511644738;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VrReticle
struct  VrReticle_t1471479728  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VrReticle::timeHold
	float ___timeHold_2;
	// System.Single VrReticle::adjustment
	float ___adjustment_6;
	// System.Single VrReticle::timeScale
	float ___timeScale_8;
	// UnityEngine.Vector3 VrReticle::newScale
	Vector3_t2243707580  ___newScale_9;
	// UnityEngine.Vector3 VrReticle::newSmallScale
	Vector3_t2243707580  ___newSmallScale_10;

public:
	inline static int32_t get_offset_of_timeHold_2() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728, ___timeHold_2)); }
	inline float get_timeHold_2() const { return ___timeHold_2; }
	inline float* get_address_of_timeHold_2() { return &___timeHold_2; }
	inline void set_timeHold_2(float value)
	{
		___timeHold_2 = value;
	}

	inline static int32_t get_offset_of_adjustment_6() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728, ___adjustment_6)); }
	inline float get_adjustment_6() const { return ___adjustment_6; }
	inline float* get_address_of_adjustment_6() { return &___adjustment_6; }
	inline void set_adjustment_6(float value)
	{
		___adjustment_6 = value;
	}

	inline static int32_t get_offset_of_timeScale_8() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728, ___timeScale_8)); }
	inline float get_timeScale_8() const { return ___timeScale_8; }
	inline float* get_address_of_timeScale_8() { return &___timeScale_8; }
	inline void set_timeScale_8(float value)
	{
		___timeScale_8 = value;
	}

	inline static int32_t get_offset_of_newScale_9() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728, ___newScale_9)); }
	inline Vector3_t2243707580  get_newScale_9() const { return ___newScale_9; }
	inline Vector3_t2243707580 * get_address_of_newScale_9() { return &___newScale_9; }
	inline void set_newScale_9(Vector3_t2243707580  value)
	{
		___newScale_9 = value;
	}

	inline static int32_t get_offset_of_newSmallScale_10() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728, ___newSmallScale_10)); }
	inline Vector3_t2243707580  get_newSmallScale_10() const { return ___newSmallScale_10; }
	inline Vector3_t2243707580 * get_address_of_newSmallScale_10() { return &___newSmallScale_10; }
	inline void set_newSmallScale_10(Vector3_t2243707580  value)
	{
		___newSmallScale_10 = value;
	}
};

struct VrReticle_t1471479728_StaticFields
{
public:
	// System.Single VrReticle::timeCountdown
	float ___timeCountdown_3;
	// System.Single VrReticle::tempTimeHold
	float ___tempTimeHold_4;
	// UnityEngine.UI.Image VrReticle::imgFill
	Image_t2042527209 * ___imgFill_5;
	// System.Boolean VrReticle::isGrow
	bool ___isGrow_7;
	// UnityEngine.Vector3 VrReticle::originalScale
	Vector3_t2243707580  ___originalScale_11;
	// UnityEngine.Vector3 VrReticle::currentlScale
	Vector3_t2243707580  ___currentlScale_12;
	// System.Boolean VrReticle::isZoomIn
	bool ___isZoomIn_13;
	// UnityEngine.GameObject VrReticle::currentObject
	GameObject_t1756533147 * ___currentObject_14;
	// VRHandleButton VrReticle::vrHandleButton
	VRHandleButton_t3511644738 * ___vrHandleButton_15;

public:
	inline static int32_t get_offset_of_timeCountdown_3() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___timeCountdown_3)); }
	inline float get_timeCountdown_3() const { return ___timeCountdown_3; }
	inline float* get_address_of_timeCountdown_3() { return &___timeCountdown_3; }
	inline void set_timeCountdown_3(float value)
	{
		___timeCountdown_3 = value;
	}

	inline static int32_t get_offset_of_tempTimeHold_4() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___tempTimeHold_4)); }
	inline float get_tempTimeHold_4() const { return ___tempTimeHold_4; }
	inline float* get_address_of_tempTimeHold_4() { return &___tempTimeHold_4; }
	inline void set_tempTimeHold_4(float value)
	{
		___tempTimeHold_4 = value;
	}

	inline static int32_t get_offset_of_imgFill_5() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___imgFill_5)); }
	inline Image_t2042527209 * get_imgFill_5() const { return ___imgFill_5; }
	inline Image_t2042527209 ** get_address_of_imgFill_5() { return &___imgFill_5; }
	inline void set_imgFill_5(Image_t2042527209 * value)
	{
		___imgFill_5 = value;
		Il2CppCodeGenWriteBarrier(&___imgFill_5, value);
	}

	inline static int32_t get_offset_of_isGrow_7() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___isGrow_7)); }
	inline bool get_isGrow_7() const { return ___isGrow_7; }
	inline bool* get_address_of_isGrow_7() { return &___isGrow_7; }
	inline void set_isGrow_7(bool value)
	{
		___isGrow_7 = value;
	}

	inline static int32_t get_offset_of_originalScale_11() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___originalScale_11)); }
	inline Vector3_t2243707580  get_originalScale_11() const { return ___originalScale_11; }
	inline Vector3_t2243707580 * get_address_of_originalScale_11() { return &___originalScale_11; }
	inline void set_originalScale_11(Vector3_t2243707580  value)
	{
		___originalScale_11 = value;
	}

	inline static int32_t get_offset_of_currentlScale_12() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___currentlScale_12)); }
	inline Vector3_t2243707580  get_currentlScale_12() const { return ___currentlScale_12; }
	inline Vector3_t2243707580 * get_address_of_currentlScale_12() { return &___currentlScale_12; }
	inline void set_currentlScale_12(Vector3_t2243707580  value)
	{
		___currentlScale_12 = value;
	}

	inline static int32_t get_offset_of_isZoomIn_13() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___isZoomIn_13)); }
	inline bool get_isZoomIn_13() const { return ___isZoomIn_13; }
	inline bool* get_address_of_isZoomIn_13() { return &___isZoomIn_13; }
	inline void set_isZoomIn_13(bool value)
	{
		___isZoomIn_13 = value;
	}

	inline static int32_t get_offset_of_currentObject_14() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___currentObject_14)); }
	inline GameObject_t1756533147 * get_currentObject_14() const { return ___currentObject_14; }
	inline GameObject_t1756533147 ** get_address_of_currentObject_14() { return &___currentObject_14; }
	inline void set_currentObject_14(GameObject_t1756533147 * value)
	{
		___currentObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___currentObject_14, value);
	}

	inline static int32_t get_offset_of_vrHandleButton_15() { return static_cast<int32_t>(offsetof(VrReticle_t1471479728_StaticFields, ___vrHandleButton_15)); }
	inline VRHandleButton_t3511644738 * get_vrHandleButton_15() const { return ___vrHandleButton_15; }
	inline VRHandleButton_t3511644738 ** get_address_of_vrHandleButton_15() { return &___vrHandleButton_15; }
	inline void set_vrHandleButton_15(VRHandleButton_t3511644738 * value)
	{
		___vrHandleButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___vrHandleButton_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
