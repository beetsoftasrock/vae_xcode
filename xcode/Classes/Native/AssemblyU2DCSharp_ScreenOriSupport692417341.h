﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenOriSupport
struct  ScreenOriSupport_t692417341  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ScreenOriSupport::isPortrait
	bool ___isPortrait_2;

public:
	inline static int32_t get_offset_of_isPortrait_2() { return static_cast<int32_t>(offsetof(ScreenOriSupport_t692417341, ___isPortrait_2)); }
	inline bool get_isPortrait_2() const { return ___isPortrait_2; }
	inline bool* get_address_of_isPortrait_2() { return &___isPortrait_2; }
	inline void set_isPortrait_2(bool value)
	{
		___isPortrait_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
