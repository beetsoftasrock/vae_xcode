﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeCotroller/<MovingClose>c__Iterator0
struct U3CMovingCloseU3Ec__Iterator0_t1094654469;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeCotroller/<MovingClose>c__Iterator0::.ctor()
extern "C"  void U3CMovingCloseU3Ec__Iterator0__ctor_m2117700602 (U3CMovingCloseU3Ec__Iterator0_t1094654469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HomeCotroller/<MovingClose>c__Iterator0::MoveNext()
extern "C"  bool U3CMovingCloseU3Ec__Iterator0_MoveNext_m1725307174 (U3CMovingCloseU3Ec__Iterator0_t1094654469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HomeCotroller/<MovingClose>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMovingCloseU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1991164160 (U3CMovingCloseU3Ec__Iterator0_t1094654469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HomeCotroller/<MovingClose>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMovingCloseU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m576825640 (U3CMovingCloseU3Ec__Iterator0_t1094654469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller/<MovingClose>c__Iterator0::Dispose()
extern "C"  void U3CMovingCloseU3Ec__Iterator0_Dispose_m76481591 (U3CMovingCloseU3Ec__Iterator0_t1094654469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller/<MovingClose>c__Iterator0::Reset()
extern "C"  void U3CMovingCloseU3Ec__Iterator0_Reset_m2585192517 (U3CMovingCloseU3Ec__Iterator0_t1094654469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
