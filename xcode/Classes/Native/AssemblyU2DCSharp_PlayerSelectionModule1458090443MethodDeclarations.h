﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerSelectionModule
struct PlayerSelectionModule_t1458090443;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ConversationSelectionGroupData
struct ConversationSelectionGroupData_t3437723178;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConversationSelectionGroupData3437723178.h"

// System.Void PlayerSelectionModule::.ctor()
extern "C"  void PlayerSelectionModule__ctor_m1118423668 (PlayerSelectionModule_t1458090443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerSelectionModule::Awake()
extern "C"  void PlayerSelectionModule_Awake_m1162463835 (PlayerSelectionModule_t1458090443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerSelectionModule::OnConversationReset()
extern "C"  void PlayerSelectionModule_OnConversationReset_m85641471 (PlayerSelectionModule_t1458090443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerSelectionModule::OnConversationReplay()
extern "C"  void PlayerSelectionModule_OnConversationReplay_m892620613 (PlayerSelectionModule_t1458090443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerSelectionModule::ShowPlayerSelections(ConversationSelectionGroupData)
extern "C"  Il2CppObject * PlayerSelectionModule_ShowPlayerSelections_m2853792521 (PlayerSelectionModule_t1458090443 * __this, ConversationSelectionGroupData_t3437723178 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
