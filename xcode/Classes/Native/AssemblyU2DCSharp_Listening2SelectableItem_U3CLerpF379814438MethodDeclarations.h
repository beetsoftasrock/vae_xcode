﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening2SelectableItem/<LerpFrame>c__Iterator0
struct U3CLerpFrameU3Ec__Iterator0_t379814438;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Listening2SelectableItem/<LerpFrame>c__Iterator0::.ctor()
extern "C"  void U3CLerpFrameU3Ec__Iterator0__ctor_m3725225517 (U3CLerpFrameU3Ec__Iterator0_t379814438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Listening2SelectableItem/<LerpFrame>c__Iterator0::MoveNext()
extern "C"  bool U3CLerpFrameU3Ec__Iterator0_MoveNext_m1112151375 (U3CLerpFrameU3Ec__Iterator0_t379814438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Listening2SelectableItem/<LerpFrame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLerpFrameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m426050911 (U3CLerpFrameU3Ec__Iterator0_t379814438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Listening2SelectableItem/<LerpFrame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLerpFrameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m332715815 (U3CLerpFrameU3Ec__Iterator0_t379814438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem/<LerpFrame>c__Iterator0::Dispose()
extern "C"  void U3CLerpFrameU3Ec__Iterator0_Dispose_m925776624 (U3CLerpFrameU3Ec__Iterator0_t379814438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening2SelectableItem/<LerpFrame>c__Iterator0::Reset()
extern "C"  void U3CLerpFrameU3Ec__Iterator0_Reset_m264948438 (U3CLerpFrameU3Ec__Iterator0_t379814438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
