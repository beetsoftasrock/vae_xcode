﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollViewExtendControl
struct ScrollViewExtendControl_t924978223;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void ScrollViewExtendControl::.ctor()
extern "C"  void ScrollViewExtendControl__ctor_m3914918116 (ScrollViewExtendControl_t924978223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollViewExtendControl::MoveUp()
extern "C"  void ScrollViewExtendControl_MoveUp_m2456446390 (ScrollViewExtendControl_t924978223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollViewExtendControl::MoveDown()
extern "C"  void ScrollViewExtendControl_MoveDown_m633118909 (ScrollViewExtendControl_t924978223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScrollViewExtendControl::DoMoveContent(UnityEngine.Vector3)
extern "C"  Il2CppObject * ScrollViewExtendControl_DoMoveContent_m991980390 (ScrollViewExtendControl_t924978223 * __this, Vector3_t2243707580  ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
