﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`1<ConversationLogicController/ConversationState>
struct UnityEvent_1_t3398846683;
// UnityEngine.Events.UnityAction`1<ConversationLogicController/ConversationState>
struct UnityAction_1_t432115123;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "AssemblyU2DCSharp_ConversationLogicController_Conv3360496668.h"

// System.Void UnityEngine.Events.UnityEvent`1<ConversationLogicController/ConversationState>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2044736755_gshared (UnityEvent_1_t3398846683 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m2044736755(__this, method) ((  void (*) (UnityEvent_1_t3398846683 *, const MethodInfo*))UnityEvent_1__ctor_m2044736755_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<ConversationLogicController/ConversationState>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1906171401_gshared (UnityEvent_1_t3398846683 * __this, UnityAction_1_t432115123 * ___call0, const MethodInfo* method);
#define UnityEvent_1_AddListener_m1906171401(__this, ___call0, method) ((  void (*) (UnityEvent_1_t3398846683 *, UnityAction_1_t432115123 *, const MethodInfo*))UnityEvent_1_AddListener_m1906171401_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`1<ConversationLogicController/ConversationState>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m3420845078_gshared (UnityEvent_1_t3398846683 * __this, UnityAction_1_t432115123 * ___call0, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m3420845078(__this, ___call0, method) ((  void (*) (UnityEvent_1_t3398846683 *, UnityAction_1_t432115123 *, const MethodInfo*))UnityEvent_1_RemoveListener_m3420845078_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<ConversationLogicController/ConversationState>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3837634723_gshared (UnityEvent_1_t3398846683 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m3837634723(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_1_t3398846683 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m3837634723_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<ConversationLogicController/ConversationState>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m4104021447_gshared (UnityEvent_1_t3398846683 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m4104021447(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_1_t3398846683 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m4104021447_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<ConversationLogicController/ConversationState>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m4220968566_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t432115123 * ___action0, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m4220968566(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t432115123 *, const MethodInfo*))UnityEvent_1_GetDelegate_m4220968566_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`1<ConversationLogicController/ConversationState>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m3736792222_gshared (UnityEvent_1_t3398846683 * __this, int32_t ___arg00, const MethodInfo* method);
#define UnityEvent_1_Invoke_m3736792222(__this, ___arg00, method) ((  void (*) (UnityEvent_1_t3398846683 *, int32_t, const MethodInfo*))UnityEvent_1_Invoke_m3736792222_gshared)(__this, ___arg00, method)
