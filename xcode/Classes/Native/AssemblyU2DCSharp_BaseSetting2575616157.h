﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Object>
struct Dictionary_2_t2936381379;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseSetting
struct  BaseSetting_t2575616157  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Object> BaseSetting::_dictionary
	Dictionary_2_t2936381379 * ____dictionary_0;

public:
	inline static int32_t get_offset_of__dictionary_0() { return static_cast<int32_t>(offsetof(BaseSetting_t2575616157, ____dictionary_0)); }
	inline Dictionary_2_t2936381379 * get__dictionary_0() const { return ____dictionary_0; }
	inline Dictionary_2_t2936381379 ** get_address_of__dictionary_0() { return &____dictionary_0; }
	inline void set__dictionary_0(Dictionary_2_t2936381379 * value)
	{
		____dictionary_0 = value;
		Il2CppCodeGenWriteBarrier(&____dictionary_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
