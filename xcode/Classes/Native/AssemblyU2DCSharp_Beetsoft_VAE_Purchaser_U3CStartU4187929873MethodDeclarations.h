﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.Purchaser/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t4187929873;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Beetsoft.VAE.Purchaser/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m1444759308 (U3CStartU3Ec__Iterator0_t4187929873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Beetsoft.VAE.Purchaser/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2565415344 (U3CStartU3Ec__Iterator0_t4187929873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Beetsoft.VAE.Purchaser/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2585030724 (U3CStartU3Ec__Iterator0_t4187929873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Beetsoft.VAE.Purchaser/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3030923084 (U3CStartU3Ec__Iterator0_t4187929873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3496708475 (U3CStartU3Ec__Iterator0_t4187929873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Beetsoft.VAE.Purchaser/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1257042789 (U3CStartU3Ec__Iterator0_t4187929873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
