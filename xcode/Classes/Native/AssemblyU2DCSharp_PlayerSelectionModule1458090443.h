﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerTalkModule
struct PlayerTalkModule_t3356839145;
// OtherTalkModule
struct OtherTalkModule_t2158105276;
// ConversationLogicController
struct ConversationLogicController_t3911886281;
// ConversationSelectionDialog
struct ConversationSelectionDialog_t2460570659;
// CommandLoader
struct CommandLoader_t2460237222;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerSelectionModule
struct  PlayerSelectionModule_t1458090443  : public MonoBehaviour_t1158329972
{
public:
	// PlayerTalkModule PlayerSelectionModule::playerTalkModule
	PlayerTalkModule_t3356839145 * ___playerTalkModule_2;
	// OtherTalkModule PlayerSelectionModule::otherTalkModule
	OtherTalkModule_t2158105276 * ___otherTalkModule_3;
	// ConversationLogicController PlayerSelectionModule::clc
	ConversationLogicController_t3911886281 * ___clc_4;
	// ConversationSelectionDialog PlayerSelectionModule::selectionDialog
	ConversationSelectionDialog_t2460570659 * ___selectionDialog_5;
	// CommandLoader PlayerSelectionModule::commandLoader
	CommandLoader_t2460237222 * ___commandLoader_6;

public:
	inline static int32_t get_offset_of_playerTalkModule_2() { return static_cast<int32_t>(offsetof(PlayerSelectionModule_t1458090443, ___playerTalkModule_2)); }
	inline PlayerTalkModule_t3356839145 * get_playerTalkModule_2() const { return ___playerTalkModule_2; }
	inline PlayerTalkModule_t3356839145 ** get_address_of_playerTalkModule_2() { return &___playerTalkModule_2; }
	inline void set_playerTalkModule_2(PlayerTalkModule_t3356839145 * value)
	{
		___playerTalkModule_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerTalkModule_2, value);
	}

	inline static int32_t get_offset_of_otherTalkModule_3() { return static_cast<int32_t>(offsetof(PlayerSelectionModule_t1458090443, ___otherTalkModule_3)); }
	inline OtherTalkModule_t2158105276 * get_otherTalkModule_3() const { return ___otherTalkModule_3; }
	inline OtherTalkModule_t2158105276 ** get_address_of_otherTalkModule_3() { return &___otherTalkModule_3; }
	inline void set_otherTalkModule_3(OtherTalkModule_t2158105276 * value)
	{
		___otherTalkModule_3 = value;
		Il2CppCodeGenWriteBarrier(&___otherTalkModule_3, value);
	}

	inline static int32_t get_offset_of_clc_4() { return static_cast<int32_t>(offsetof(PlayerSelectionModule_t1458090443, ___clc_4)); }
	inline ConversationLogicController_t3911886281 * get_clc_4() const { return ___clc_4; }
	inline ConversationLogicController_t3911886281 ** get_address_of_clc_4() { return &___clc_4; }
	inline void set_clc_4(ConversationLogicController_t3911886281 * value)
	{
		___clc_4 = value;
		Il2CppCodeGenWriteBarrier(&___clc_4, value);
	}

	inline static int32_t get_offset_of_selectionDialog_5() { return static_cast<int32_t>(offsetof(PlayerSelectionModule_t1458090443, ___selectionDialog_5)); }
	inline ConversationSelectionDialog_t2460570659 * get_selectionDialog_5() const { return ___selectionDialog_5; }
	inline ConversationSelectionDialog_t2460570659 ** get_address_of_selectionDialog_5() { return &___selectionDialog_5; }
	inline void set_selectionDialog_5(ConversationSelectionDialog_t2460570659 * value)
	{
		___selectionDialog_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectionDialog_5, value);
	}

	inline static int32_t get_offset_of_commandLoader_6() { return static_cast<int32_t>(offsetof(PlayerSelectionModule_t1458090443, ___commandLoader_6)); }
	inline CommandLoader_t2460237222 * get_commandLoader_6() const { return ___commandLoader_6; }
	inline CommandLoader_t2460237222 ** get_address_of_commandLoader_6() { return &___commandLoader_6; }
	inline void set_commandLoader_6(CommandLoader_t2460237222 * value)
	{
		___commandLoader_6 = value;
		Il2CppCodeGenWriteBarrier(&___commandLoader_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
