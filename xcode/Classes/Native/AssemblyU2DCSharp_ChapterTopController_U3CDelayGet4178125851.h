﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PopupOpener
struct PopupOpener_t1646050995;
// System.String
struct String_t;
// ChapterTopController
struct ChapterTopController_t2392392144;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SceneName904752595.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1
struct  U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851  : public Il2CppObject
{
public:
	// PopupOpener ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::popupOpener
	PopupOpener_t1646050995 * ___popupOpener_0;
	// SceneName ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::sceneName
	int32_t ___sceneName_1;
	// System.String ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::normalScene
	String_t* ___normalScene_2;
	// ChapterTopController ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::$this
	ChapterTopController_t2392392144 * ___U24this_3;
	// System.Object ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 ChapterTopController/<DelayGetPopupSelectMode>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_popupOpener_0() { return static_cast<int32_t>(offsetof(U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851, ___popupOpener_0)); }
	inline PopupOpener_t1646050995 * get_popupOpener_0() const { return ___popupOpener_0; }
	inline PopupOpener_t1646050995 ** get_address_of_popupOpener_0() { return &___popupOpener_0; }
	inline void set_popupOpener_0(PopupOpener_t1646050995 * value)
	{
		___popupOpener_0 = value;
		Il2CppCodeGenWriteBarrier(&___popupOpener_0, value);
	}

	inline static int32_t get_offset_of_sceneName_1() { return static_cast<int32_t>(offsetof(U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851, ___sceneName_1)); }
	inline int32_t get_sceneName_1() const { return ___sceneName_1; }
	inline int32_t* get_address_of_sceneName_1() { return &___sceneName_1; }
	inline void set_sceneName_1(int32_t value)
	{
		___sceneName_1 = value;
	}

	inline static int32_t get_offset_of_normalScene_2() { return static_cast<int32_t>(offsetof(U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851, ___normalScene_2)); }
	inline String_t* get_normalScene_2() const { return ___normalScene_2; }
	inline String_t** get_address_of_normalScene_2() { return &___normalScene_2; }
	inline void set_normalScene_2(String_t* value)
	{
		___normalScene_2 = value;
		Il2CppCodeGenWriteBarrier(&___normalScene_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851, ___U24this_3)); }
	inline ChapterTopController_t2392392144 * get_U24this_3() const { return ___U24this_3; }
	inline ChapterTopController_t2392392144 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ChapterTopController_t2392392144 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDelayGetPopupSelectModeU3Ec__Iterator1_t4178125851, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
