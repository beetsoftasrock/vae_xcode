﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationHistoryItemView/ConversationHistoryItemClickedEvent
struct ConversationHistoryItemClickedEvent_t3133779843;
// ConversationTalkData
struct ConversationTalkData_t1570298305;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationHistoryItemView
struct  ConversationHistoryItemView_t3846761105  : public MonoBehaviour_t1158329972
{
public:
	// ConversationHistoryItemView/ConversationHistoryItemClickedEvent ConversationHistoryItemView::onHistoryItemClicked
	ConversationHistoryItemClickedEvent_t3133779843 * ___onHistoryItemClicked_2;
	// ConversationTalkData ConversationHistoryItemView::data
	ConversationTalkData_t1570298305 * ___data_3;
	// UnityEngine.UI.Image ConversationHistoryItemView::conversationImg
	Image_t2042527209 * ___conversationImg_4;
	// UnityEngine.UI.Text ConversationHistoryItemView::textEn
	Text_t356221433 * ___textEn_5;
	// UnityEngine.UI.Text ConversationHistoryItemView::textJp
	Text_t356221433 * ___textJp_6;
	// UnityEngine.Sprite ConversationHistoryItemView::role0Sprite
	Sprite_t309593783 * ___role0Sprite_7;
	// UnityEngine.Sprite ConversationHistoryItemView::role1Sprite
	Sprite_t309593783 * ___role1Sprite_8;
	// UnityEngine.Color ConversationHistoryItemView::role0ColorText
	Color_t2020392075  ___role0ColorText_9;
	// UnityEngine.Color32 ConversationHistoryItemView::role1ColorText
	Color32_t874517518  ___role1ColorText_10;
	// System.Boolean ConversationHistoryItemView::_isPlayerRole
	bool ____isPlayerRole_11;

public:
	inline static int32_t get_offset_of_onHistoryItemClicked_2() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___onHistoryItemClicked_2)); }
	inline ConversationHistoryItemClickedEvent_t3133779843 * get_onHistoryItemClicked_2() const { return ___onHistoryItemClicked_2; }
	inline ConversationHistoryItemClickedEvent_t3133779843 ** get_address_of_onHistoryItemClicked_2() { return &___onHistoryItemClicked_2; }
	inline void set_onHistoryItemClicked_2(ConversationHistoryItemClickedEvent_t3133779843 * value)
	{
		___onHistoryItemClicked_2 = value;
		Il2CppCodeGenWriteBarrier(&___onHistoryItemClicked_2, value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___data_3)); }
	inline ConversationTalkData_t1570298305 * get_data_3() const { return ___data_3; }
	inline ConversationTalkData_t1570298305 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(ConversationTalkData_t1570298305 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier(&___data_3, value);
	}

	inline static int32_t get_offset_of_conversationImg_4() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___conversationImg_4)); }
	inline Image_t2042527209 * get_conversationImg_4() const { return ___conversationImg_4; }
	inline Image_t2042527209 ** get_address_of_conversationImg_4() { return &___conversationImg_4; }
	inline void set_conversationImg_4(Image_t2042527209 * value)
	{
		___conversationImg_4 = value;
		Il2CppCodeGenWriteBarrier(&___conversationImg_4, value);
	}

	inline static int32_t get_offset_of_textEn_5() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___textEn_5)); }
	inline Text_t356221433 * get_textEn_5() const { return ___textEn_5; }
	inline Text_t356221433 ** get_address_of_textEn_5() { return &___textEn_5; }
	inline void set_textEn_5(Text_t356221433 * value)
	{
		___textEn_5 = value;
		Il2CppCodeGenWriteBarrier(&___textEn_5, value);
	}

	inline static int32_t get_offset_of_textJp_6() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___textJp_6)); }
	inline Text_t356221433 * get_textJp_6() const { return ___textJp_6; }
	inline Text_t356221433 ** get_address_of_textJp_6() { return &___textJp_6; }
	inline void set_textJp_6(Text_t356221433 * value)
	{
		___textJp_6 = value;
		Il2CppCodeGenWriteBarrier(&___textJp_6, value);
	}

	inline static int32_t get_offset_of_role0Sprite_7() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___role0Sprite_7)); }
	inline Sprite_t309593783 * get_role0Sprite_7() const { return ___role0Sprite_7; }
	inline Sprite_t309593783 ** get_address_of_role0Sprite_7() { return &___role0Sprite_7; }
	inline void set_role0Sprite_7(Sprite_t309593783 * value)
	{
		___role0Sprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___role0Sprite_7, value);
	}

	inline static int32_t get_offset_of_role1Sprite_8() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___role1Sprite_8)); }
	inline Sprite_t309593783 * get_role1Sprite_8() const { return ___role1Sprite_8; }
	inline Sprite_t309593783 ** get_address_of_role1Sprite_8() { return &___role1Sprite_8; }
	inline void set_role1Sprite_8(Sprite_t309593783 * value)
	{
		___role1Sprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___role1Sprite_8, value);
	}

	inline static int32_t get_offset_of_role0ColorText_9() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___role0ColorText_9)); }
	inline Color_t2020392075  get_role0ColorText_9() const { return ___role0ColorText_9; }
	inline Color_t2020392075 * get_address_of_role0ColorText_9() { return &___role0ColorText_9; }
	inline void set_role0ColorText_9(Color_t2020392075  value)
	{
		___role0ColorText_9 = value;
	}

	inline static int32_t get_offset_of_role1ColorText_10() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ___role1ColorText_10)); }
	inline Color32_t874517518  get_role1ColorText_10() const { return ___role1ColorText_10; }
	inline Color32_t874517518 * get_address_of_role1ColorText_10() { return &___role1ColorText_10; }
	inline void set_role1ColorText_10(Color32_t874517518  value)
	{
		___role1ColorText_10 = value;
	}

	inline static int32_t get_offset_of__isPlayerRole_11() { return static_cast<int32_t>(offsetof(ConversationHistoryItemView_t3846761105, ____isPlayerRole_11)); }
	inline bool get__isPlayerRole_11() const { return ____isPlayerRole_11; }
	inline bool* get_address_of__isPlayerRole_11() { return &____isPlayerRole_11; }
	inline void set__isPlayerRole_11(bool value)
	{
		____isPlayerRole_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
