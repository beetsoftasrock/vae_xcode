﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubtextControlModule
struct SubtextControlModule_t4093059790;

#include "codegen/il2cpp-codegen.h"

// System.Void SubtextControlModule::.ctor()
extern "C"  void SubtextControlModule__ctor_m1063984947 (SubtextControlModule_t4093059790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubtextControlModule::OnConversationReset()
extern "C"  void SubtextControlModule_OnConversationReset_m1755777916 (SubtextControlModule_t4093059790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubtextControlModule::OnConversationReplay()
extern "C"  void SubtextControlModule_OnConversationReplay_m179849180 (SubtextControlModule_t4093059790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SubtextControlModule::IsShowing()
extern "C"  bool SubtextControlModule_IsShowing_m3700599004 (SubtextControlModule_t4093059790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubtextControlModule::ShowSubText()
extern "C"  void SubtextControlModule_ShowSubText_m3860439945 (SubtextControlModule_t4093059790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubtextControlModule::HideSubText()
extern "C"  void SubtextControlModule_HideSubText_m2039863204 (SubtextControlModule_t4093059790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubtextControlModule::SetSubtextViews(System.Boolean)
extern "C"  void SubtextControlModule_SetSubtextViews_m2132552721 (SubtextControlModule_t4093059790 * __this, bool ___isShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubtextControlModule::Switch()
extern "C"  void SubtextControlModule_Switch_m645491265 (SubtextControlModule_t4093059790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SubtextControlModule::OnEnable()
extern "C"  void SubtextControlModule_OnEnable_m1056422543 (SubtextControlModule_t4093059790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
