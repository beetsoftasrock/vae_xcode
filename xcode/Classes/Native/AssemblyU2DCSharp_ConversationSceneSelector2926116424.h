﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConversationSceneSelectionView[]
struct ConversationSceneSelectionViewU5BU5D_t2992794585;
// ConversationSceneSelectionView
struct ConversationSceneSelectionView_t407867144;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConversationSceneSelector
struct  ConversationSceneSelector_t2926116424  : public MonoBehaviour_t1158329972
{
public:
	// ConversationSceneSelectionView[] ConversationSceneSelector::selectionViews
	ConversationSceneSelectionViewU5BU5D_t2992794585* ___selectionViews_2;
	// System.Int32 ConversationSceneSelector::_currentSelectedId
	int32_t ____currentSelectedId_3;
	// ConversationSceneSelectionView ConversationSceneSelector::_currentSelected
	ConversationSceneSelectionView_t407867144 * ____currentSelected_4;

public:
	inline static int32_t get_offset_of_selectionViews_2() { return static_cast<int32_t>(offsetof(ConversationSceneSelector_t2926116424, ___selectionViews_2)); }
	inline ConversationSceneSelectionViewU5BU5D_t2992794585* get_selectionViews_2() const { return ___selectionViews_2; }
	inline ConversationSceneSelectionViewU5BU5D_t2992794585** get_address_of_selectionViews_2() { return &___selectionViews_2; }
	inline void set_selectionViews_2(ConversationSceneSelectionViewU5BU5D_t2992794585* value)
	{
		___selectionViews_2 = value;
		Il2CppCodeGenWriteBarrier(&___selectionViews_2, value);
	}

	inline static int32_t get_offset_of__currentSelectedId_3() { return static_cast<int32_t>(offsetof(ConversationSceneSelector_t2926116424, ____currentSelectedId_3)); }
	inline int32_t get__currentSelectedId_3() const { return ____currentSelectedId_3; }
	inline int32_t* get_address_of__currentSelectedId_3() { return &____currentSelectedId_3; }
	inline void set__currentSelectedId_3(int32_t value)
	{
		____currentSelectedId_3 = value;
	}

	inline static int32_t get_offset_of__currentSelected_4() { return static_cast<int32_t>(offsetof(ConversationSceneSelector_t2926116424, ____currentSelected_4)); }
	inline ConversationSceneSelectionView_t407867144 * get__currentSelected_4() const { return ____currentSelected_4; }
	inline ConversationSceneSelectionView_t407867144 ** get_address_of__currentSelected_4() { return &____currentSelected_4; }
	inline void set__currentSelected_4(ConversationSceneSelectionView_t407867144 * value)
	{
		____currentSelected_4 = value;
		Il2CppCodeGenWriteBarrier(&____currentSelected_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
