﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TempSceneManager
struct TempSceneManager_t785418923;

#include "codegen/il2cpp-codegen.h"

// System.Void TempSceneManager::.ctor()
extern "C"  void TempSceneManager__ctor_m2860785268 (TempSceneManager_t785418923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TempSceneManager TempSceneManager::get_Instance()
extern "C"  TempSceneManager_t785418923 * TempSceneManager_get_Instance_m2234515660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TempSceneManager::get_IndexTabChapterTopOpened()
extern "C"  int32_t TempSceneManager_get_IndexTabChapterTopOpened_m3483012687 (TempSceneManager_t785418923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempSceneManager::set_IndexTabChapterTopOpened(System.Int32)
extern "C"  void TempSceneManager_set_IndexTabChapterTopOpened_m2970406176 (TempSceneManager_t785418923 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
