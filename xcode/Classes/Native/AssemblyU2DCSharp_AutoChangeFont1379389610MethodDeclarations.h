﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoChangeFont
struct AutoChangeFont_t1379389610;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoChangeFont::.ctor()
extern "C"  void AutoChangeFont__ctor_m3212955741 (AutoChangeFont_t1379389610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoChangeFont::ChangeFont()
extern "C"  void AutoChangeFont_ChangeFont_m1269564728 (AutoChangeFont_t1379389610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
