﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransferCodeResponse
struct TransferCodeResponse_t1278852611;

#include "codegen/il2cpp-codegen.h"

// System.Void TransferCodeResponse::.ctor()
extern "C"  void TransferCodeResponse__ctor_m1095598920 (TransferCodeResponse_t1278852611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
