﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManagerHelp/<LoadSprite>c__AnonStorey0
struct  U3CLoadSpriteU3Ec__AnonStorey0_t1128402625  : public Il2CppObject
{
public:
	// System.String ManagerHelp/<LoadSprite>c__AnonStorey0::prefixName
	String_t* ___prefixName_0;

public:
	inline static int32_t get_offset_of_prefixName_0() { return static_cast<int32_t>(offsetof(U3CLoadSpriteU3Ec__AnonStorey0_t1128402625, ___prefixName_0)); }
	inline String_t* get_prefixName_0() const { return ___prefixName_0; }
	inline String_t** get_address_of_prefixName_0() { return &___prefixName_0; }
	inline void set_prefixName_0(String_t* value)
	{
		___prefixName_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefixName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
