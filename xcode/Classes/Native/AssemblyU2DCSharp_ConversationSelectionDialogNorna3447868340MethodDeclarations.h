﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConversationSelectionDialogNornal/<ShowSelection>c__Iterator0
struct U3CShowSelectionU3Ec__Iterator0_t3447868340;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ConversationSelectionDialogNornal/<ShowSelection>c__Iterator0::.ctor()
extern "C"  void U3CShowSelectionU3Ec__Iterator0__ctor_m3941972865 (U3CShowSelectionU3Ec__Iterator0_t3447868340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConversationSelectionDialogNornal/<ShowSelection>c__Iterator0::MoveNext()
extern "C"  bool U3CShowSelectionU3Ec__Iterator0_MoveNext_m2328524739 (U3CShowSelectionU3Ec__Iterator0_t3447868340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConversationSelectionDialogNornal/<ShowSelection>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowSelectionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1863512047 (U3CShowSelectionU3Ec__Iterator0_t3447868340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConversationSelectionDialogNornal/<ShowSelection>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowSelectionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3247277527 (U3CShowSelectionU3Ec__Iterator0_t3447868340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogNornal/<ShowSelection>c__Iterator0::Dispose()
extern "C"  void U3CShowSelectionU3Ec__Iterator0_Dispose_m4049818686 (U3CShowSelectionU3Ec__Iterator0_t3447868340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConversationSelectionDialogNornal/<ShowSelection>c__Iterator0::Reset()
extern "C"  void U3CShowSelectionU3Ec__Iterator0_Reset_m2100689276 (U3CShowSelectionU3Ec__Iterator0_t3447868340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConversationSelectionDialogNornal/<ShowSelection>c__Iterator0::<>m__0()
extern "C"  bool U3CShowSelectionU3Ec__Iterator0_U3CU3Em__0_m2834844068 (U3CShowSelectionU3Ec__Iterator0_t3447868340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
