﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChapterSelector/<DofocusSelected>c__Iterator3
struct U3CDofocusSelectedU3Ec__Iterator3_t945102547;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ChapterSelector/<DofocusSelected>c__Iterator3::.ctor()
extern "C"  void U3CDofocusSelectedU3Ec__Iterator3__ctor_m3740182816 (U3CDofocusSelectedU3Ec__Iterator3_t945102547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChapterSelector/<DofocusSelected>c__Iterator3::MoveNext()
extern "C"  bool U3CDofocusSelectedU3Ec__Iterator3_MoveNext_m243743576 (U3CDofocusSelectedU3Ec__Iterator3_t945102547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterSelector/<DofocusSelected>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDofocusSelectedU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m536288078 (U3CDofocusSelectedU3Ec__Iterator3_t945102547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChapterSelector/<DofocusSelected>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDofocusSelectedU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3290793638 (U3CDofocusSelectedU3Ec__Iterator3_t945102547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<DofocusSelected>c__Iterator3::Dispose()
extern "C"  void U3CDofocusSelectedU3Ec__Iterator3_Dispose_m4155617397 (U3CDofocusSelectedU3Ec__Iterator3_t945102547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChapterSelector/<DofocusSelected>c__Iterator3::Reset()
extern "C"  void U3CDofocusSelectedU3Ec__Iterator3_Reset_m1023036179 (U3CDofocusSelectedU3Ec__Iterator3_t945102547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
