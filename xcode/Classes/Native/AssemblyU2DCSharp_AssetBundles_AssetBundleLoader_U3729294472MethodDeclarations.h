﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0
struct U3CforceShowU3Ec__Iterator0_t729294472;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::.ctor()
extern "C"  void U3CforceShowU3Ec__Iterator0__ctor_m494690977 (U3CforceShowU3Ec__Iterator0_t729294472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::MoveNext()
extern "C"  bool U3CforceShowU3Ec__Iterator0_MoveNext_m1555556627 (U3CforceShowU3Ec__Iterator0_t729294472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CforceShowU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3904222123 (U3CforceShowU3Ec__Iterator0_t729294472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CforceShowU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m448504163 (U3CforceShowU3Ec__Iterator0_t729294472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::Dispose()
extern "C"  void U3CforceShowU3Ec__Iterator0_Dispose_m3034520130 (U3CforceShowU3Ec__Iterator0_t729294472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetBundles.AssetBundleLoader/<forceShow>c__Iterator0::Reset()
extern "C"  void U3CforceShowU3Ec__Iterator0_Reset_m3105871032 (U3CforceShowU3Ec__Iterator0_t729294472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
