﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sentence2QuestionView
struct Sentence2QuestionView_t1942347884;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Sentence2QuestionView::.ctor()
extern "C"  void Sentence2QuestionView__ctor_m2048889247 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView::Awake()
extern "C"  void Sentence2QuestionView_Awake_m600095768 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView::ClearView()
extern "C"  void Sentence2QuestionView_ClearView_m977690407 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Sentence2QuestionView::IsLockedInteractive()
extern "C"  bool Sentence2QuestionView_IsLockedInteractive_m1876375319 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView::Start()
extern "C"  void Sentence2QuestionView_Start_m2150127539 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView::StartPointerEffect()
extern "C"  void Sentence2QuestionView_StartPointerEffect_m4239474739 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView::StopPointerEffect()
extern "C"  void Sentence2QuestionView_StopPointerEffect_m685684175 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView::SetQuestionText(System.String,System.String)
extern "C"  void Sentence2QuestionView_SetQuestionText_m1722512430 (Sentence2QuestionView_t1942347884 * __this, String_t* ___mainText0, String_t* ___hightLight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Sentence2QuestionView::FakeTextPointer()
extern "C"  Il2CppObject * Sentence2QuestionView_FakeTextPointer_m1449572828 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sentence2QuestionView::ShowResult(System.Boolean,System.String,System.String,System.String)
extern "C"  void Sentence2QuestionView_ShowResult_m1233628394 (Sentence2QuestionView_t1942347884 * __this, bool ___isCorrect0, String_t* ___mainText1, String_t* ___correctText2, String_t* ___answerText3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Sentence2QuestionView::PlayAnimation()
extern "C"  Il2CppObject * Sentence2QuestionView_PlayAnimation_m2420609279 (Sentence2QuestionView_t1942347884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
