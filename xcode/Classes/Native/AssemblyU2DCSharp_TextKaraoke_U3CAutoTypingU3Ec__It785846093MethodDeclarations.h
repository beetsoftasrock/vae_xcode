﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextKaraoke/<AutoTyping>c__Iterator0
struct U3CAutoTypingU3Ec__Iterator0_t785846093;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TextKaraoke/<AutoTyping>c__Iterator0::.ctor()
extern "C"  void U3CAutoTypingU3Ec__Iterator0__ctor_m2319585276 (U3CAutoTypingU3Ec__Iterator0_t785846093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TextKaraoke/<AutoTyping>c__Iterator0::MoveNext()
extern "C"  bool U3CAutoTypingU3Ec__Iterator0_MoveNext_m1944973900 (U3CAutoTypingU3Ec__Iterator0_t785846093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TextKaraoke/<AutoTyping>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAutoTypingU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m684427440 (U3CAutoTypingU3Ec__Iterator0_t785846093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TextKaraoke/<AutoTyping>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAutoTypingU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m28791224 (U3CAutoTypingU3Ec__Iterator0_t785846093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextKaraoke/<AutoTyping>c__Iterator0::Dispose()
extern "C"  void U3CAutoTypingU3Ec__Iterator0_Dispose_m3686912583 (U3CAutoTypingU3Ec__Iterator0_t785846093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextKaraoke/<AutoTyping>c__Iterator0::Reset()
extern "C"  void U3CAutoTypingU3Ec__Iterator0_Reset_m3762195741 (U3CAutoTypingU3Ec__Iterator0_t785846093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
