﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VocalController/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t956566065;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VocalController/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m1521609518 (U3CStartU3Ec__Iterator0_t956566065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VocalController/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m3571001442 (U3CStartU3Ec__Iterator0_t956566065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VocalController/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2208682252 (U3CStartU3Ec__Iterator0_t956566065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VocalController/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1903969252 (U3CStartU3Ec__Iterator0_t956566065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2214044643 (U3CStartU3Ec__Iterator0_t956566065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VocalController/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2110592401 (U3CStartU3Ec__Iterator0_t956566065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
