﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0
struct U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::.ctor()
extern "C"  void U3CPlayOtherTalkEffectU3Ec__Iterator0__ctor_m4149787572 (U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayOtherTalkEffectU3Ec__Iterator0_MoveNext_m1494004716 (U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlayOtherTalkEffectU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1753893234 (U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlayOtherTalkEffectU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2574758794 (U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::Dispose()
extern "C"  void U3CPlayOtherTalkEffectU3Ec__Iterator0_Dispose_m894851419 (U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayOtherTalkDialog/<PlayOtherTalkEffect>c__Iterator0::Reset()
extern "C"  void U3CPlayOtherTalkEffectU3Ec__Iterator0_Reset_m3787597193 (U3CPlayOtherTalkEffectU3Ec__Iterator0_t157632481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
