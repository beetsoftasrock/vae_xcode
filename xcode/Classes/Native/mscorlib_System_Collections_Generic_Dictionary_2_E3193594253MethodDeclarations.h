﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3897162496(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3193594253 *, Dictionary_2_t1873569551 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3796086367(__this, method) ((  Il2CppObject * (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1369613727(__this, method) ((  void (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3500253736(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2169670241(__this, method) ((  Il2CppObject * (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1752204625(__this, method) ((  Il2CppObject * (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m1831421323(__this, method) ((  bool (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_Current()
#define Enumerator_get_Current_m1679034555(__this, method) ((  KeyValuePair_2_t3925882070  (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4232907744(__this, method) ((  Il2CppObject * (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m907211336(__this, method) ((  Dictionary_2_t2281509423 * (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::Reset()
#define Enumerator_Reset_m749431606(__this, method) ((  void (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m534243849(__this, method) ((  void (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m566299983(__this, method) ((  void (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::Dispose()
#define Enumerator_Dispose_m118355428(__this, method) ((  void (*) (Enumerator_t3193594253 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
