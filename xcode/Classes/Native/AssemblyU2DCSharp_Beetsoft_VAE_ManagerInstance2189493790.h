﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t504227755;
// System.Predicate`1<UnityEngine.AudioSource>
struct Predicate_1_t3873044034;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beetsoft.VAE.ManagerInstance
struct  ManagerInstance_t2189493790  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> Beetsoft.VAE.ManagerInstance::pooledObjects
	List_1_t504227755 * ___pooledObjects_2;

public:
	inline static int32_t get_offset_of_pooledObjects_2() { return static_cast<int32_t>(offsetof(ManagerInstance_t2189493790, ___pooledObjects_2)); }
	inline List_1_t504227755 * get_pooledObjects_2() const { return ___pooledObjects_2; }
	inline List_1_t504227755 ** get_address_of_pooledObjects_2() { return &___pooledObjects_2; }
	inline void set_pooledObjects_2(List_1_t504227755 * value)
	{
		___pooledObjects_2 = value;
		Il2CppCodeGenWriteBarrier(&___pooledObjects_2, value);
	}
};

struct ManagerInstance_t2189493790_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.AudioSource> Beetsoft.VAE.ManagerInstance::<>f__am$cache0
	Predicate_1_t3873044034 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(ManagerInstance_t2189493790_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Predicate_1_t3873044034 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Predicate_1_t3873044034 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Predicate_1_t3873044034 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
