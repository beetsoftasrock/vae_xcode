﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ChapterTopController
struct ChapterTopController_t2392392144;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SceneName904752595.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterTopController/<SelectMode>c__AnonStorey2
struct  U3CSelectModeU3Ec__AnonStorey2_t101845251  : public Il2CppObject
{
public:
	// SceneName ChapterTopController/<SelectMode>c__AnonStorey2::sceneName
	int32_t ___sceneName_0;
	// System.String ChapterTopController/<SelectMode>c__AnonStorey2::normalScene
	String_t* ___normalScene_1;
	// ChapterTopController ChapterTopController/<SelectMode>c__AnonStorey2::$this
	ChapterTopController_t2392392144 * ___U24this_2;

public:
	inline static int32_t get_offset_of_sceneName_0() { return static_cast<int32_t>(offsetof(U3CSelectModeU3Ec__AnonStorey2_t101845251, ___sceneName_0)); }
	inline int32_t get_sceneName_0() const { return ___sceneName_0; }
	inline int32_t* get_address_of_sceneName_0() { return &___sceneName_0; }
	inline void set_sceneName_0(int32_t value)
	{
		___sceneName_0 = value;
	}

	inline static int32_t get_offset_of_normalScene_1() { return static_cast<int32_t>(offsetof(U3CSelectModeU3Ec__AnonStorey2_t101845251, ___normalScene_1)); }
	inline String_t* get_normalScene_1() const { return ___normalScene_1; }
	inline String_t** get_address_of_normalScene_1() { return &___normalScene_1; }
	inline void set_normalScene_1(String_t* value)
	{
		___normalScene_1 = value;
		Il2CppCodeGenWriteBarrier(&___normalScene_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSelectModeU3Ec__AnonStorey2_t101845251, ___U24this_2)); }
	inline ChapterTopController_t2392392144 * get_U24this_2() const { return ___U24this_2; }
	inline ChapterTopController_t2392392144 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ChapterTopController_t2392392144 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
