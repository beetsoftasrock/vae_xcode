﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// PracticeVRSwitcher
struct PracticeVRSwitcher_t654240754;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PracticeVRSwitcher/<QuitVRMode>c__AnonStorey2
struct  U3CQuitVRModeU3Ec__AnonStorey2_t1152556888  : public Il2CppObject
{
public:
	// UnityEngine.Transform PracticeVRSwitcher/<QuitVRMode>c__AnonStorey2::parentPopupQuit
	Transform_t3275118058 * ___parentPopupQuit_0;
	// PracticeVRSwitcher PracticeVRSwitcher/<QuitVRMode>c__AnonStorey2::$this
	PracticeVRSwitcher_t654240754 * ___U24this_1;

public:
	inline static int32_t get_offset_of_parentPopupQuit_0() { return static_cast<int32_t>(offsetof(U3CQuitVRModeU3Ec__AnonStorey2_t1152556888, ___parentPopupQuit_0)); }
	inline Transform_t3275118058 * get_parentPopupQuit_0() const { return ___parentPopupQuit_0; }
	inline Transform_t3275118058 ** get_address_of_parentPopupQuit_0() { return &___parentPopupQuit_0; }
	inline void set_parentPopupQuit_0(Transform_t3275118058 * value)
	{
		___parentPopupQuit_0 = value;
		Il2CppCodeGenWriteBarrier(&___parentPopupQuit_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CQuitVRModeU3Ec__AnonStorey2_t1152556888, ___U24this_1)); }
	inline PracticeVRSwitcher_t654240754 * get_U24this_1() const { return ___U24this_1; }
	inline PracticeVRSwitcher_t654240754 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PracticeVRSwitcher_t654240754 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
