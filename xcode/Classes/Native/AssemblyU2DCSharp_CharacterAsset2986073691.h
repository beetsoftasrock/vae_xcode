﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<PostureCharacter>
struct List_1_t4244112333;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterAsset
struct  CharacterAsset_t2986073691  : public ScriptableObject_t1975622470
{
public:
	// System.String CharacterAsset::nameCharacter
	String_t* ___nameCharacter_2;
	// System.Collections.Generic.List`1<PostureCharacter> CharacterAsset::posturese
	List_1_t4244112333 * ___posturese_3;

public:
	inline static int32_t get_offset_of_nameCharacter_2() { return static_cast<int32_t>(offsetof(CharacterAsset_t2986073691, ___nameCharacter_2)); }
	inline String_t* get_nameCharacter_2() const { return ___nameCharacter_2; }
	inline String_t** get_address_of_nameCharacter_2() { return &___nameCharacter_2; }
	inline void set_nameCharacter_2(String_t* value)
	{
		___nameCharacter_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameCharacter_2, value);
	}

	inline static int32_t get_offset_of_posturese_3() { return static_cast<int32_t>(offsetof(CharacterAsset_t2986073691, ___posturese_3)); }
	inline List_1_t4244112333 * get_posturese_3() const { return ___posturese_3; }
	inline List_1_t4244112333 ** get_address_of_posturese_3() { return &___posturese_3; }
	inline void set_posturese_3(List_1_t4244112333 * value)
	{
		___posturese_3 = value;
		Il2CppCodeGenWriteBarrier(&___posturese_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
