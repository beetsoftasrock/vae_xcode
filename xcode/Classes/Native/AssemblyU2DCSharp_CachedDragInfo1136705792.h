﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CachedDragInfo
struct  CachedDragInfo_t1136705792  : public Il2CppObject
{
public:
	// System.Single CachedDragInfo::time
	float ___time_0;
	// UnityEngine.Vector2 CachedDragInfo::mousePosition
	Vector2_t2243707579  ___mousePosition_1;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(CachedDragInfo_t1136705792, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_mousePosition_1() { return static_cast<int32_t>(offsetof(CachedDragInfo_t1136705792, ___mousePosition_1)); }
	inline Vector2_t2243707579  get_mousePosition_1() const { return ___mousePosition_1; }
	inline Vector2_t2243707579 * get_address_of_mousePosition_1() { return &___mousePosition_1; }
	inline void set_mousePosition_1(Vector2_t2243707579  value)
	{
		___mousePosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
