﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reporter/<readInfo>c__Iterator0
struct U3CreadInfoU3Ec__Iterator0_t2714882631;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Reporter/<readInfo>c__Iterator0::.ctor()
extern "C"  void U3CreadInfoU3Ec__Iterator0__ctor_m4129795054 (U3CreadInfoU3Ec__Iterator0_t2714882631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Reporter/<readInfo>c__Iterator0::MoveNext()
extern "C"  bool U3CreadInfoU3Ec__Iterator0_MoveNext_m156869630 (U3CreadInfoU3Ec__Iterator0_t2714882631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Reporter/<readInfo>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2233128600 (U3CreadInfoU3Ec__Iterator0_t2714882631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Reporter/<readInfo>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1942210096 (U3CreadInfoU3Ec__Iterator0_t2714882631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter/<readInfo>c__Iterator0::Dispose()
extern "C"  void U3CreadInfoU3Ec__Iterator0_Dispose_m3273113713 (U3CreadInfoU3Ec__Iterator0_t2714882631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter/<readInfo>c__Iterator0::Reset()
extern "C"  void U3CreadInfoU3Ec__Iterator0_Reset_m3922888659 (U3CreadInfoU3Ec__Iterator0_t2714882631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
