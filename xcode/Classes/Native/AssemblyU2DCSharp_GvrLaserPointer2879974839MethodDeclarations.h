﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrLaserPointer
struct GvrLaserPointer_t2879974839;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrLaserPointer::.ctor()
extern "C"  void GvrLaserPointer__ctor_m3374328308 (GvrLaserPointer_t2879974839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
