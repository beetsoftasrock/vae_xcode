﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HistoryLogModule/<AppendTalkDialogs>c__Iterator0
struct U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HistoryLogModule/<AppendTalkDialogs>c__Iterator0::.ctor()
extern "C"  void U3CAppendTalkDialogsU3Ec__Iterator0__ctor_m3538426016 (U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HistoryLogModule/<AppendTalkDialogs>c__Iterator0::MoveNext()
extern "C"  bool U3CAppendTalkDialogsU3Ec__Iterator0_MoveNext_m2077201080 (U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryLogModule/<AppendTalkDialogs>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAppendTalkDialogsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m652159716 (U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HistoryLogModule/<AppendTalkDialogs>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAppendTalkDialogsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2622765276 (U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryLogModule/<AppendTalkDialogs>c__Iterator0::Dispose()
extern "C"  void U3CAppendTalkDialogsU3Ec__Iterator0_Dispose_m1726408371 (U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HistoryLogModule/<AppendTalkDialogs>c__Iterator0::Reset()
extern "C"  void U3CAppendTalkDialogsU3Ec__Iterator0_Reset_m1813441369 (U3CAppendTalkDialogsU3Ec__Iterator0_t2635638849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
