﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Beetsoft.VAE.Purchaser/PurchaseSession
struct PurchaseSession_t2088054391;
struct PurchaseSession_t2088054391_marshaled_pinvoke;
struct PurchaseSession_t2088054391_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PurchaseSession_t2088054391;
struct PurchaseSession_t2088054391_marshaled_pinvoke;

extern "C" void PurchaseSession_t2088054391_marshal_pinvoke(const PurchaseSession_t2088054391& unmarshaled, PurchaseSession_t2088054391_marshaled_pinvoke& marshaled);
extern "C" void PurchaseSession_t2088054391_marshal_pinvoke_back(const PurchaseSession_t2088054391_marshaled_pinvoke& marshaled, PurchaseSession_t2088054391& unmarshaled);
extern "C" void PurchaseSession_t2088054391_marshal_pinvoke_cleanup(PurchaseSession_t2088054391_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PurchaseSession_t2088054391;
struct PurchaseSession_t2088054391_marshaled_com;

extern "C" void PurchaseSession_t2088054391_marshal_com(const PurchaseSession_t2088054391& unmarshaled, PurchaseSession_t2088054391_marshaled_com& marshaled);
extern "C" void PurchaseSession_t2088054391_marshal_com_back(const PurchaseSession_t2088054391_marshaled_com& marshaled, PurchaseSession_t2088054391& unmarshaled);
extern "C" void PurchaseSession_t2088054391_marshal_com_cleanup(PurchaseSession_t2088054391_marshaled_com& marshaled);
