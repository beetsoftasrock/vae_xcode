﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseSettingBasedImageItem
struct BaseSettingBasedImageItem_t1370958008;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseSettingBasedImageItem::.ctor()
extern "C"  void BaseSettingBasedImageItem__ctor_m1614163175 (BaseSettingBasedImageItem_t1370958008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseSettingBasedImageItem::Start()
extern "C"  void BaseSettingBasedImageItem_Start_m861027027 (BaseSettingBasedImageItem_t1370958008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
