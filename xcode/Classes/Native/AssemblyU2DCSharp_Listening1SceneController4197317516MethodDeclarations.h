﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Listening1SceneController
struct Listening1SceneController_t4197317516;
// Listening1OnplayQuestion
struct Listening1OnplayQuestion_t3534871625;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Listening1OnplayQuestion3534871625.h"

// System.Void Listening1SceneController::.ctor()
extern "C"  void Listening1SceneController__ctor_m4186376093 (Listening1SceneController_t4197317516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Listening1OnplayQuestion Listening1SceneController::get_currentQuestion()
extern "C"  Listening1OnplayQuestion_t3534871625 * Listening1SceneController_get_currentQuestion_m146571807 (Listening1SceneController_t4197317516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1SceneController::set_currentQuestion(Listening1OnplayQuestion)
extern "C"  void Listening1SceneController_set_currentQuestion_m3756168168 (Listening1SceneController_t4197317516 * __this, Listening1OnplayQuestion_t3534871625 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Listening1SceneController::Start()
extern "C"  Il2CppObject * Listening1SceneController_Start_m3228805495 (Listening1SceneController_t4197317516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1SceneController::StartPlay()
extern "C"  void Listening1SceneController_StartPlay_m1431301837 (Listening1SceneController_t4197317516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1SceneController::InitData()
extern "C"  void Listening1SceneController_InitData_m806506707 (Listening1SceneController_t4197317516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1SceneController::NextQuestion()
extern "C"  void Listening1SceneController_NextQuestion_m892858762 (Listening1SceneController_t4197317516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1SceneController::BackToChapterTop()
extern "C"  void Listening1SceneController_BackToChapterTop_m215088443 (Listening1SceneController_t4197317516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Listening1SceneController::SaveData()
extern "C"  void Listening1SceneController_SaveData_m3703143834 (Listening1SceneController_t4197317516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
