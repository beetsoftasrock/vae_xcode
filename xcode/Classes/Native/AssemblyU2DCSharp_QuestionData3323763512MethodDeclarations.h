﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuestionData
struct QuestionData_t3323763512;

#include "codegen/il2cpp-codegen.h"

// System.Void QuestionData::.ctor()
extern "C"  void QuestionData__ctor_m2249072145 (QuestionData_t3323763512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
