﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Action
struct Action_t3226471752;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeObject
struct  FadeObject_t1506616101  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image FadeObject::img
	Image_t2042527209 * ___img_2;
	// System.Action FadeObject::OnStartFade
	Action_t3226471752 * ___OnStartFade_4;
	// System.Action FadeObject::OnEndFade
	Action_t3226471752 * ___OnEndFade_5;
	// UnityEngine.Coroutine FadeObject::coroutineFade
	Coroutine_t2299508840 * ___coroutineFade_6;

public:
	inline static int32_t get_offset_of_img_2() { return static_cast<int32_t>(offsetof(FadeObject_t1506616101, ___img_2)); }
	inline Image_t2042527209 * get_img_2() const { return ___img_2; }
	inline Image_t2042527209 ** get_address_of_img_2() { return &___img_2; }
	inline void set_img_2(Image_t2042527209 * value)
	{
		___img_2 = value;
		Il2CppCodeGenWriteBarrier(&___img_2, value);
	}

	inline static int32_t get_offset_of_OnStartFade_4() { return static_cast<int32_t>(offsetof(FadeObject_t1506616101, ___OnStartFade_4)); }
	inline Action_t3226471752 * get_OnStartFade_4() const { return ___OnStartFade_4; }
	inline Action_t3226471752 ** get_address_of_OnStartFade_4() { return &___OnStartFade_4; }
	inline void set_OnStartFade_4(Action_t3226471752 * value)
	{
		___OnStartFade_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnStartFade_4, value);
	}

	inline static int32_t get_offset_of_OnEndFade_5() { return static_cast<int32_t>(offsetof(FadeObject_t1506616101, ___OnEndFade_5)); }
	inline Action_t3226471752 * get_OnEndFade_5() const { return ___OnEndFade_5; }
	inline Action_t3226471752 ** get_address_of_OnEndFade_5() { return &___OnEndFade_5; }
	inline void set_OnEndFade_5(Action_t3226471752 * value)
	{
		___OnEndFade_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnEndFade_5, value);
	}

	inline static int32_t get_offset_of_coroutineFade_6() { return static_cast<int32_t>(offsetof(FadeObject_t1506616101, ___coroutineFade_6)); }
	inline Coroutine_t2299508840 * get_coroutineFade_6() const { return ___coroutineFade_6; }
	inline Coroutine_t2299508840 ** get_address_of_coroutineFade_6() { return &___coroutineFade_6; }
	inline void set_coroutineFade_6(Coroutine_t2299508840 * value)
	{
		___coroutineFade_6 = value;
		Il2CppCodeGenWriteBarrier(&___coroutineFade_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
