﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TempSceneManager
struct TempSceneManager_t785418923;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TempSceneManager
struct  TempSceneManager_t785418923  : public Il2CppObject
{
public:
	// System.Int32 TempSceneManager::tabOpenedIndext
	int32_t ___tabOpenedIndext_1;

public:
	inline static int32_t get_offset_of_tabOpenedIndext_1() { return static_cast<int32_t>(offsetof(TempSceneManager_t785418923, ___tabOpenedIndext_1)); }
	inline int32_t get_tabOpenedIndext_1() const { return ___tabOpenedIndext_1; }
	inline int32_t* get_address_of_tabOpenedIndext_1() { return &___tabOpenedIndext_1; }
	inline void set_tabOpenedIndext_1(int32_t value)
	{
		___tabOpenedIndext_1 = value;
	}
};

struct TempSceneManager_t785418923_StaticFields
{
public:
	// TempSceneManager TempSceneManager::instance
	TempSceneManager_t785418923 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(TempSceneManager_t785418923_StaticFields, ___instance_0)); }
	inline TempSceneManager_t785418923 * get_instance_0() const { return ___instance_0; }
	inline TempSceneManager_t785418923 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(TempSceneManager_t785418923 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
