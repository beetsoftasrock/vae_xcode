﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LipingShare.LCLib.Asn1Processor.RelativeOid
struct RelativeOid_t880150712;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void LipingShare.LCLib.Asn1Processor.RelativeOid::.ctor()
extern "C"  void RelativeOid__ctor_m2864312816 (RelativeOid_t880150712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.RelativeOid::Decode(System.IO.Stream)
extern "C"  String_t* RelativeOid_Decode_m1265304508 (RelativeOid_t880150712 * __this, Stream_t3255436806 * ___bt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
