﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeCotroller/<Moving>c__Iterator2
struct U3CMovingU3Ec__Iterator2_t2354585089;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeCotroller/<Moving>c__Iterator2::.ctor()
extern "C"  void U3CMovingU3Ec__Iterator2__ctor_m2398792640 (U3CMovingU3Ec__Iterator2_t2354585089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HomeCotroller/<Moving>c__Iterator2::MoveNext()
extern "C"  bool U3CMovingU3Ec__Iterator2_MoveNext_m1586850008 (U3CMovingU3Ec__Iterator2_t2354585089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HomeCotroller/<Moving>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMovingU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3869688996 (U3CMovingU3Ec__Iterator2_t2354585089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HomeCotroller/<Moving>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMovingU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1530668268 (U3CMovingU3Ec__Iterator2_t2354585089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller/<Moving>c__Iterator2::Dispose()
extern "C"  void U3CMovingU3Ec__Iterator2_Dispose_m728429011 (U3CMovingU3Ec__Iterator2_t2354585089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCotroller/<Moving>c__Iterator2::Reset()
extern "C"  void U3CMovingU3Ec__Iterator2_Reset_m2587086169 (U3CMovingU3Ec__Iterator2_t2354585089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
