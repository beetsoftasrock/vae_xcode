﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterConfigData
struct  ChapterConfigData_t999749205  : public Il2CppObject
{
public:
	// System.Int32 ChapterConfigData::chapterId
	int32_t ___chapterId_0;
	// System.String ChapterConfigData::chapterName
	String_t* ___chapterName_1;
	// System.String ChapterConfigData::title
	String_t* ___title_2;
	// System.String ChapterConfigData::sub
	String_t* ___sub_3;
	// System.String ChapterConfigData::detail
	String_t* ___detail_4;
	// System.String ChapterConfigData::backgroundImage
	String_t* ___backgroundImage_5;

public:
	inline static int32_t get_offset_of_chapterId_0() { return static_cast<int32_t>(offsetof(ChapterConfigData_t999749205, ___chapterId_0)); }
	inline int32_t get_chapterId_0() const { return ___chapterId_0; }
	inline int32_t* get_address_of_chapterId_0() { return &___chapterId_0; }
	inline void set_chapterId_0(int32_t value)
	{
		___chapterId_0 = value;
	}

	inline static int32_t get_offset_of_chapterName_1() { return static_cast<int32_t>(offsetof(ChapterConfigData_t999749205, ___chapterName_1)); }
	inline String_t* get_chapterName_1() const { return ___chapterName_1; }
	inline String_t** get_address_of_chapterName_1() { return &___chapterName_1; }
	inline void set_chapterName_1(String_t* value)
	{
		___chapterName_1 = value;
		Il2CppCodeGenWriteBarrier(&___chapterName_1, value);
	}

	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(ChapterConfigData_t999749205, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}

	inline static int32_t get_offset_of_sub_3() { return static_cast<int32_t>(offsetof(ChapterConfigData_t999749205, ___sub_3)); }
	inline String_t* get_sub_3() const { return ___sub_3; }
	inline String_t** get_address_of_sub_3() { return &___sub_3; }
	inline void set_sub_3(String_t* value)
	{
		___sub_3 = value;
		Il2CppCodeGenWriteBarrier(&___sub_3, value);
	}

	inline static int32_t get_offset_of_detail_4() { return static_cast<int32_t>(offsetof(ChapterConfigData_t999749205, ___detail_4)); }
	inline String_t* get_detail_4() const { return ___detail_4; }
	inline String_t** get_address_of_detail_4() { return &___detail_4; }
	inline void set_detail_4(String_t* value)
	{
		___detail_4 = value;
		Il2CppCodeGenWriteBarrier(&___detail_4, value);
	}

	inline static int32_t get_offset_of_backgroundImage_5() { return static_cast<int32_t>(offsetof(ChapterConfigData_t999749205, ___backgroundImage_5)); }
	inline String_t* get_backgroundImage_5() const { return ___backgroundImage_5; }
	inline String_t** get_address_of_backgroundImage_5() { return &___backgroundImage_5; }
	inline void set_backgroundImage_5(String_t* value)
	{
		___backgroundImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImage_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
