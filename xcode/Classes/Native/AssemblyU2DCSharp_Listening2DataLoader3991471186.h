﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CommandSheet
struct CommandSheet_t2769384292;
// Listening2DatasWrapper
struct Listening2DatasWrapper_t1178512309;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Listening2DataLoader
struct  Listening2DataLoader_t3991471186  : public MonoBehaviour_t1158329972
{
public:
	// CommandSheet Listening2DataLoader::dataSheet
	CommandSheet_t2769384292 * ___dataSheet_2;
	// Listening2DatasWrapper Listening2DataLoader::<datasWrapper>k__BackingField
	Listening2DatasWrapper_t1178512309 * ___U3CdatasWrapperU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_dataSheet_2() { return static_cast<int32_t>(offsetof(Listening2DataLoader_t3991471186, ___dataSheet_2)); }
	inline CommandSheet_t2769384292 * get_dataSheet_2() const { return ___dataSheet_2; }
	inline CommandSheet_t2769384292 ** get_address_of_dataSheet_2() { return &___dataSheet_2; }
	inline void set_dataSheet_2(CommandSheet_t2769384292 * value)
	{
		___dataSheet_2 = value;
		Il2CppCodeGenWriteBarrier(&___dataSheet_2, value);
	}

	inline static int32_t get_offset_of_U3CdatasWrapperU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Listening2DataLoader_t3991471186, ___U3CdatasWrapperU3Ek__BackingField_3)); }
	inline Listening2DatasWrapper_t1178512309 * get_U3CdatasWrapperU3Ek__BackingField_3() const { return ___U3CdatasWrapperU3Ek__BackingField_3; }
	inline Listening2DatasWrapper_t1178512309 ** get_address_of_U3CdatasWrapperU3Ek__BackingField_3() { return &___U3CdatasWrapperU3Ek__BackingField_3; }
	inline void set_U3CdatasWrapperU3Ek__BackingField_3(Listening2DatasWrapper_t1178512309 * value)
	{
		___U3CdatasWrapperU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdatasWrapperU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
