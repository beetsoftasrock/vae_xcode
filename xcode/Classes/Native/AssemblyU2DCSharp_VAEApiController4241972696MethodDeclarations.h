﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VAEApiController
struct VAEApiController_t4241972696;

#include "codegen/il2cpp-codegen.h"

// System.Void VAEApiController::.ctor()
extern "C"  void VAEApiController__ctor_m2341840389 (VAEApiController_t4241972696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
