﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnG1804908545.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnA1967739812.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnT4143287487.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnOr602701282.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnBu358370788.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_U3C4253624923.h"
#include "AssemblyU2DCSharp_proto_Proto_PhoneEvent3882078222.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent2572128318.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types3648109718.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Type1530480861.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve4072706903.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1262104803.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1211758263.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve2701542133.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve3452538341.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_GyroscopeE182225200.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_GyroscopeEv33558588.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Accelerom1893725728.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Accelerom1480486140.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_DepthMapE1516604558.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_DepthMapE3483346914.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Orientati2038376807.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Orientati2561526853.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_KeyEvent639576718.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_KeyEvent_2056133158.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Builder2537253112.h"
#include "AssemblyU2DCSharp_GvrBasePointer2150122635.h"
#include "AssemblyU2DCSharp_GvrBasePointerRaycaster1189534163.h"
#include "AssemblyU2DCSharp_GvrBasePointerRaycaster_RaycastM3965091944.h"
#include "AssemblyU2DCSharp_GvrExecuteEventsExtension3083691626.h"
#include "AssemblyU2DCSharp_GvrPointerGraphicRaycaster1649506702.h"
#include "AssemblyU2DCSharp_GvrPointerGraphicRaycaster_Block4215129352.h"
#include "AssemblyU2DCSharp_GvrPointerInputModule1603976810.h"
#include "AssemblyU2DCSharp_GvrPointerPhysicsRaycaster2558158517.h"
#include "AssemblyU2DCSharp_GazeInputModule197612175.h"
#include "AssemblyU2DCSharp_GvrUnitySdkVersion4210256426.h"
#include "AssemblyU2DCSharp_GvrViewer2583885279.h"
#include "AssemblyU2DCSharp_GvrViewer_DistortionCorrectionMe1613770858.h"
#include "AssemblyU2DCSharp_GvrViewer_StereoScreenChangeDele1350813851.h"
#include "AssemblyU2DCSharp_GvrViewer_Eye1346324485.h"
#include "AssemblyU2DCSharp_GvrViewer_Distortion351632083.h"
#include "AssemblyU2DCSharp_Pose3D3872859958.h"
#include "AssemblyU2DCSharp_MutablePose3D1015643808.h"
#include "AssemblyU2DCSharp_GvrDropdown2234606196.h"
#include "AssemblyU2DCSharp_GvrGaze2249568644.h"
#include "AssemblyU2DCSharp_GvrLaserPointer2879974839.h"
#include "AssemblyU2DCSharp_GvrLaserPointerImpl2141976067.h"
#include "AssemblyU2DCSharp_GvrReticle1834592217.h"
#include "AssemblyU2DCSharp_GvrReticlePointer2836438988.h"
#include "AssemblyU2DCSharp_GvrReticlePointerImpl3911945438.h"
#include "AssemblyU2DCSharp_GvrActivityHelper1610839920.h"
#include "AssemblyU2DCSharp_GvrFPS750935016.h"
#include "AssemblyU2DCSharp_GvrIntent542233401.h"
#include "AssemblyU2DCSharp_GvrUIHelpers1244281516.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture673526704.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoType3515677472.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoResol1153223030.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoPlaye2474362314.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoEvents660621533.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_RenderComma104445810.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_OnVideoEve3117232894.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_OnExceptio1653610982.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CStartU32919987970.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CCallPlu2924899295.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CInternal129051828.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CInterna2021693473.h"
#include "AssemblyU2DCSharp_PlayerPrefsUtility259601127.h"
#include "AssemblyU2DCSharp_Sabresaurus_PlayerPrefsExtensions619545967.h"
#include "AssemblyU2DCSharp_CommandSheet2769384292.h"
#include "AssemblyU2DCSharp_CommandSheet_Param4123818474.h"
#include "AssemblyU2DCSharp_Images563431294.h"
#include "AssemblyU2DCSharp_Reporter3561640551.h"
#include "AssemblyU2DCSharp_Reporter__LogType4109028099.h"
#include "AssemblyU2DCSharp_Reporter_Sample3185432476.h"
#include "AssemblyU2DCSharp_Reporter_Log3604182180.h"
#include "AssemblyU2DCSharp_Reporter_ReportView608488827.h"
#include "AssemblyU2DCSharp_Reporter_DetailView904843682.h"
#include "AssemblyU2DCSharp_Reporter_U3CreadInfoU3Ec__Iterat2714882631.h"
#include "AssemblyU2DCSharp_ReporterGUI402918452.h"
#include "AssemblyU2DCSharp_ReporterMessageReceiver1067427633.h"
#include "AssemblyU2DCSharp_Rotate4255939431.h"
#include "AssemblyU2DCSharp_TestReporter864384665.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206772.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (OnGyroEvent_t1804908545), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (OnAccelEvent_t1967739812), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (OnTouchEvent_t4143287487), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (OnOrientationEvent_t602701282), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (OnButtonEvent_t358370788), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t4253624923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[5] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24locvar0_0(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24this_1(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24current_2(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24disposing_3(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (PhoneEvent_t3882078222), -1, sizeof(PhoneEvent_t3882078222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2806[1] = 
{
	PhoneEvent_t3882078222_StaticFields::get_offset_of_Descriptor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (PhoneEvent_t2572128318), -1, sizeof(PhoneEvent_t2572128318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2807[25] = 
{
	PhoneEvent_t2572128318_StaticFields::get_offset_of_defaultInstance_0(),
	PhoneEvent_t2572128318_StaticFields::get_offset_of__phoneEventFieldNames_1(),
	PhoneEvent_t2572128318_StaticFields::get_offset_of__phoneEventFieldTags_2(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasType_4(),
	PhoneEvent_t2572128318::get_offset_of_type__5(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasMotionEvent_7(),
	PhoneEvent_t2572128318::get_offset_of_motionEvent__8(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasGyroscopeEvent_10(),
	PhoneEvent_t2572128318::get_offset_of_gyroscopeEvent__11(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasAccelerometerEvent_13(),
	PhoneEvent_t2572128318::get_offset_of_accelerometerEvent__14(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasDepthMapEvent_16(),
	PhoneEvent_t2572128318::get_offset_of_depthMapEvent__17(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasOrientationEvent_19(),
	PhoneEvent_t2572128318::get_offset_of_orientationEvent__20(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasKeyEvent_22(),
	PhoneEvent_t2572128318::get_offset_of_keyEvent__23(),
	PhoneEvent_t2572128318::get_offset_of_memoizedSerializedSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (Types_t3648109718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (Type_t1530480861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2809[7] = 
{
	Type_t1530480861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (MotionEvent_t4072706903), -1, sizeof(MotionEvent_t4072706903_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2810[12] = 
{
	MotionEvent_t4072706903_StaticFields::get_offset_of_defaultInstance_0(),
	MotionEvent_t4072706903_StaticFields::get_offset_of__motionEventFieldNames_1(),
	MotionEvent_t4072706903_StaticFields::get_offset_of__motionEventFieldTags_2(),
	0,
	MotionEvent_t4072706903::get_offset_of_hasTimestamp_4(),
	MotionEvent_t4072706903::get_offset_of_timestamp__5(),
	0,
	MotionEvent_t4072706903::get_offset_of_hasAction_7(),
	MotionEvent_t4072706903::get_offset_of_action__8(),
	0,
	MotionEvent_t4072706903::get_offset_of_pointers__10(),
	MotionEvent_t4072706903::get_offset_of_memoizedSerializedSize_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (Types_t1262104803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (Pointer_t1211758263), -1, sizeof(Pointer_t1211758263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2812[13] = 
{
	Pointer_t1211758263_StaticFields::get_offset_of_defaultInstance_0(),
	Pointer_t1211758263_StaticFields::get_offset_of__pointerFieldNames_1(),
	Pointer_t1211758263_StaticFields::get_offset_of__pointerFieldTags_2(),
	0,
	Pointer_t1211758263::get_offset_of_hasId_4(),
	Pointer_t1211758263::get_offset_of_id__5(),
	0,
	Pointer_t1211758263::get_offset_of_hasNormalizedX_7(),
	Pointer_t1211758263::get_offset_of_normalizedX__8(),
	0,
	Pointer_t1211758263::get_offset_of_hasNormalizedY_10(),
	Pointer_t1211758263::get_offset_of_normalizedY__11(),
	Pointer_t1211758263::get_offset_of_memoizedSerializedSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (Builder_t2701542133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[2] = 
{
	Builder_t2701542133::get_offset_of_resultIsReadOnly_0(),
	Builder_t2701542133::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (Builder_t3452538341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[2] = 
{
	Builder_t3452538341::get_offset_of_resultIsReadOnly_0(),
	Builder_t3452538341::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (GyroscopeEvent_t182225200), -1, sizeof(GyroscopeEvent_t182225200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2815[16] = 
{
	GyroscopeEvent_t182225200_StaticFields::get_offset_of_defaultInstance_0(),
	GyroscopeEvent_t182225200_StaticFields::get_offset_of__gyroscopeEventFieldNames_1(),
	GyroscopeEvent_t182225200_StaticFields::get_offset_of__gyroscopeEventFieldTags_2(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_hasTimestamp_4(),
	GyroscopeEvent_t182225200::get_offset_of_timestamp__5(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_hasX_7(),
	GyroscopeEvent_t182225200::get_offset_of_x__8(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_hasY_10(),
	GyroscopeEvent_t182225200::get_offset_of_y__11(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_hasZ_13(),
	GyroscopeEvent_t182225200::get_offset_of_z__14(),
	GyroscopeEvent_t182225200::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (Builder_t33558588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[2] = 
{
	Builder_t33558588::get_offset_of_resultIsReadOnly_0(),
	Builder_t33558588::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (AccelerometerEvent_t1893725728), -1, sizeof(AccelerometerEvent_t1893725728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2817[16] = 
{
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of_defaultInstance_0(),
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of__accelerometerEventFieldNames_1(),
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of__accelerometerEventFieldTags_2(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_hasTimestamp_4(),
	AccelerometerEvent_t1893725728::get_offset_of_timestamp__5(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_hasX_7(),
	AccelerometerEvent_t1893725728::get_offset_of_x__8(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_hasY_10(),
	AccelerometerEvent_t1893725728::get_offset_of_y__11(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_hasZ_13(),
	AccelerometerEvent_t1893725728::get_offset_of_z__14(),
	AccelerometerEvent_t1893725728::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (Builder_t1480486140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[2] = 
{
	Builder_t1480486140::get_offset_of_resultIsReadOnly_0(),
	Builder_t1480486140::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (DepthMapEvent_t1516604558), -1, sizeof(DepthMapEvent_t1516604558_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2819[16] = 
{
	DepthMapEvent_t1516604558_StaticFields::get_offset_of_defaultInstance_0(),
	DepthMapEvent_t1516604558_StaticFields::get_offset_of__depthMapEventFieldNames_1(),
	DepthMapEvent_t1516604558_StaticFields::get_offset_of__depthMapEventFieldTags_2(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_hasTimestamp_4(),
	DepthMapEvent_t1516604558::get_offset_of_timestamp__5(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_hasWidth_7(),
	DepthMapEvent_t1516604558::get_offset_of_width__8(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_hasHeight_10(),
	DepthMapEvent_t1516604558::get_offset_of_height__11(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_zDistancesMemoizedSerializedSize_13(),
	DepthMapEvent_t1516604558::get_offset_of_zDistances__14(),
	DepthMapEvent_t1516604558::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (Builder_t3483346914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[2] = 
{
	Builder_t3483346914::get_offset_of_resultIsReadOnly_0(),
	Builder_t3483346914::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (OrientationEvent_t2038376807), -1, sizeof(OrientationEvent_t2038376807_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2821[19] = 
{
	OrientationEvent_t2038376807_StaticFields::get_offset_of_defaultInstance_0(),
	OrientationEvent_t2038376807_StaticFields::get_offset_of__orientationEventFieldNames_1(),
	OrientationEvent_t2038376807_StaticFields::get_offset_of__orientationEventFieldTags_2(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasTimestamp_4(),
	OrientationEvent_t2038376807::get_offset_of_timestamp__5(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasX_7(),
	OrientationEvent_t2038376807::get_offset_of_x__8(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasY_10(),
	OrientationEvent_t2038376807::get_offset_of_y__11(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasZ_13(),
	OrientationEvent_t2038376807::get_offset_of_z__14(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasW_16(),
	OrientationEvent_t2038376807::get_offset_of_w__17(),
	OrientationEvent_t2038376807::get_offset_of_memoizedSerializedSize_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (Builder_t2561526853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[2] = 
{
	Builder_t2561526853::get_offset_of_resultIsReadOnly_0(),
	Builder_t2561526853::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (KeyEvent_t639576718), -1, sizeof(KeyEvent_t639576718_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2823[10] = 
{
	KeyEvent_t639576718_StaticFields::get_offset_of_defaultInstance_0(),
	KeyEvent_t639576718_StaticFields::get_offset_of__keyEventFieldNames_1(),
	KeyEvent_t639576718_StaticFields::get_offset_of__keyEventFieldTags_2(),
	0,
	KeyEvent_t639576718::get_offset_of_hasAction_4(),
	KeyEvent_t639576718::get_offset_of_action__5(),
	0,
	KeyEvent_t639576718::get_offset_of_hasCode_7(),
	KeyEvent_t639576718::get_offset_of_code__8(),
	KeyEvent_t639576718::get_offset_of_memoizedSerializedSize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (Builder_t2056133158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[2] = 
{
	Builder_t2056133158::get_offset_of_resultIsReadOnly_0(),
	Builder_t2056133158::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (Builder_t2537253112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[2] = 
{
	Builder_t2537253112::get_offset_of_resultIsReadOnly_0(),
	Builder_t2537253112::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (GvrBasePointer_t2150122635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[2] = 
{
	GvrBasePointer_t2150122635::get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_0(),
	GvrBasePointer_t2150122635::get_offset_of_U3CPointerTransformU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (GvrBasePointerRaycaster_t1189534163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[2] = 
{
	GvrBasePointerRaycaster_t1189534163::get_offset_of_raycastMode_2(),
	GvrBasePointerRaycaster_t1189534163::get_offset_of_lastRay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (RaycastMode_t3965091944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2829[3] = 
{
	RaycastMode_t3965091944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (GvrExecuteEventsExtension_t3083691626), -1, sizeof(GvrExecuteEventsExtension_t3083691626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2831[2] = 
{
	GvrExecuteEventsExtension_t3083691626_StaticFields::get_offset_of_s_HoverHandler_0(),
	GvrExecuteEventsExtension_t3083691626_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (GvrPointerGraphicRaycaster_t1649506702), -1, sizeof(GvrPointerGraphicRaycaster_t1649506702_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2832[9] = 
{
	0,
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_ignoreReversedGraphics_5(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_blockingObjects_6(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_blockingMask_7(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_targetCanvas_8(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_raycastResults_9(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_cachedPointerEventCamera_10(),
	GvrPointerGraphicRaycaster_t1649506702_StaticFields::get_offset_of_sortedGraphics_11(),
	GvrPointerGraphicRaycaster_t1649506702_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (BlockingObjects_t4215129352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2833[5] = 
{
	BlockingObjects_t4215129352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (GvrPointerInputModule_t1603976810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[9] = 
{
	GvrPointerInputModule_t1603976810::get_offset_of_vrModeOnly_8(),
	GvrPointerInputModule_t1603976810::get_offset_of_pointerData_9(),
	GvrPointerInputModule_t1603976810::get_offset_of_lastPose_10(),
	GvrPointerInputModule_t1603976810::get_offset_of_lastScroll_11(),
	GvrPointerInputModule_t1603976810::get_offset_of_eligibleForScroll_12(),
	GvrPointerInputModule_t1603976810::get_offset_of_isPointerHovering_13(),
	GvrPointerInputModule_t1603976810::get_offset_of_isActive_14(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (GvrPointerPhysicsRaycaster_t2558158517), -1, sizeof(GvrPointerPhysicsRaycaster_t2558158517_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2835[4] = 
{
	0,
	GvrPointerPhysicsRaycaster_t2558158517::get_offset_of_raycasterEventMask_5(),
	GvrPointerPhysicsRaycaster_t2558158517::get_offset_of_cachedEventCamera_6(),
	GvrPointerPhysicsRaycaster_t2558158517_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (GazeInputModule_t197612175), -1, sizeof(GazeInputModule_t197612175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2836[6] = 
{
	GazeInputModule_t197612175::get_offset_of_vrModeOnly_8(),
	GazeInputModule_t197612175_StaticFields::get_offset_of_gazePointer_9(),
	GazeInputModule_t197612175::get_offset_of_pointerData_10(),
	GazeInputModule_t197612175::get_offset_of_lastHeadPose_11(),
	GazeInputModule_t197612175::get_offset_of_isActive_12(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (GvrUnitySdkVersion_t4210256426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (GvrViewer_t2583885279), -1, sizeof(GvrViewer_t2583885279_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2838[20] = 
{
	0,
	GvrViewer_t2583885279_StaticFields::get_offset_of_instance_3(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_currentController_4(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_currentMainCamera_5(),
	GvrViewer_t2583885279::get_offset_of_vrModeEnabled_6(),
	GvrViewer_t2583885279::get_offset_of_distortionCorrection_7(),
	GvrViewer_t2583885279::get_offset_of_neckModelScale_8(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_device_9(),
	GvrViewer_t2583885279::get_offset_of_U3CNativeDistortionCorrectionSupportedU3Ek__BackingField_10(),
	GvrViewer_t2583885279::get_offset_of_U3CNativeUILayerSupportedU3Ek__BackingField_11(),
	GvrViewer_t2583885279::get_offset_of_stereoScreenScale_12(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_stereoScreen_13(),
	GvrViewer_t2583885279::get_offset_of_OnStereoScreenChanged_14(),
	GvrViewer_t2583885279::get_offset_of_defaultComfortableViewingRange_15(),
	GvrViewer_t2583885279::get_offset_of_DefaultDeviceProfile_16(),
	GvrViewer_t2583885279::get_offset_of_U3CTriggeredU3Ek__BackingField_17(),
	GvrViewer_t2583885279::get_offset_of_U3CTiltedU3Ek__BackingField_18(),
	GvrViewer_t2583885279::get_offset_of_U3CProfileChangedU3Ek__BackingField_19(),
	GvrViewer_t2583885279::get_offset_of_U3CBackButtonPressedU3Ek__BackingField_20(),
	GvrViewer_t2583885279::get_offset_of_updatedToFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (DistortionCorrectionMethod_t1613770858)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2839[4] = 
{
	DistortionCorrectionMethod_t1613770858::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (StereoScreenChangeDelegate_t1350813851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (Eye_t1346324485)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2841[4] = 
{
	Eye_t1346324485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (Distortion_t351632083)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2842[3] = 
{
	Distortion_t351632083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (Pose3D_t3872859958), -1, sizeof(Pose3D_t3872859958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2843[4] = 
{
	Pose3D_t3872859958_StaticFields::get_offset_of_flipZ_0(),
	Pose3D_t3872859958::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	Pose3D_t3872859958::get_offset_of_U3COrientationU3Ek__BackingField_2(),
	Pose3D_t3872859958::get_offset_of_U3CMatrixU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (MutablePose3D_t1015643808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (GvrDropdown_t2234606196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[1] = 
{
	GvrDropdown_t2234606196::get_offset_of_currentBlocker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (GvrGaze_t2249568644), -1, sizeof(GvrGaze_t2249568644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2846[11] = 
{
	GvrGaze_t2249568644::get_offset_of_pointerObject_2(),
	GvrGaze_t2249568644::get_offset_of_pointer_3(),
	GvrGaze_t2249568644::get_offset_of_U3CcamU3Ek__BackingField_4(),
	GvrGaze_t2249568644::get_offset_of_mask_5(),
	GvrGaze_t2249568644::get_offset_of_currentTarget_6(),
	GvrGaze_t2249568644::get_offset_of_currentGazeObject_7(),
	GvrGaze_t2249568644::get_offset_of_lastIntersectPosition_8(),
	GvrGaze_t2249568644::get_offset_of_lastIntersectionRay_9(),
	GvrGaze_t2249568644::get_offset_of_isTriggered_10(),
	GvrGaze_t2249568644_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
	GvrGaze_t2249568644_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (GvrLaserPointer_t2879974839), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (GvrLaserPointerImpl_t2141976067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[11] = 
{
	0,
	0,
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CMainCameraU3Ek__BackingField_4(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CLaserColorU3Ek__BackingField_5(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CLaserLineRendererU3Ek__BackingField_6(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CReticleU3Ek__BackingField_7(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CMaxLaserDistanceU3Ek__BackingField_8(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CMaxReticleDistanceU3Ek__BackingField_9(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CPointerIntersectionU3Ek__BackingField_10(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CIsPointerIntersectingU3Ek__BackingField_11(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CPointerIntersectionRayU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (GvrReticle_t1834592217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (GvrReticlePointer_t2836438988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[3] = 
{
	GvrReticlePointer_t2836438988::get_offset_of_reticlePointerImpl_2(),
	GvrReticlePointer_t2836438988::get_offset_of_reticleSegments_3(),
	GvrReticlePointer_t2836438988::get_offset_of_reticleGrowthSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (GvrReticlePointerImpl_t3911945438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[12] = 
{
	0,
	0,
	0,
	0,
	0,
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleGrowthSpeedU3Ek__BackingField_7(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CMaterialCompU3Ek__BackingField_8(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleInnerAngleU3Ek__BackingField_9(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleOuterAngleU3Ek__BackingField_10(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleDistanceInMetersU3Ek__BackingField_11(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleInnerDiameterU3Ek__BackingField_12(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleOuterDiameterU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (GvrActivityHelper_t1610839920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (GvrFPS_t750935016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[6] = 
{
	0,
	0,
	0,
	GvrFPS_t750935016::get_offset_of_textField_5(),
	GvrFPS_t750935016::get_offset_of_fps_6(),
	GvrFPS_t750935016::get_offset_of_cam_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (GvrIntent_t542233401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (GvrUIHelpers_t1244281516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (GvrVideoPlayerTexture_t673526704), -1, sizeof(GvrVideoPlayerTexture_t673526704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2858[33] = 
{
	0,
	0,
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoTextures_4(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_currentTexture_5(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoPlayerPtr_6(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoPlayerEventBase_7(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_initialTexture_8(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_initialized_9(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_texWidth_10(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_texHeight_11(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_lastBufferedPosition_12(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_framecount_13(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_graphicComponent_14(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_rendererComponent_15(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_renderEventFunction_16(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_processingRunning_17(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_exitProcessing_18(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_onEventCallbacks_19(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_onExceptionCallbacks_20(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_ExecuteOnMainThread_21(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_statusText_22(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_bufferSize_23(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoType_24(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoURL_25(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoContentID_26(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoProviderId_27(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_initialResolution_28(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_adjustAspectRatio_29(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_useSecurePath_30(),
	0,
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_32(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_33(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (VideoType_t3515677472)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2859[4] = 
{
	VideoType_t3515677472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (VideoResolution_t1153223030)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2860[6] = 
{
	VideoResolution_t1153223030::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (VideoPlayerState_t2474362314)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2861[6] = 
{
	VideoPlayerState_t2474362314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (VideoEvents_t660621533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2862[6] = 
{
	VideoEvents_t660621533::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (RenderCommand_t104445810)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2863[8] = 
{
	RenderCommand_t104445810::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (OnVideoEventCallback_t3117232894), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (OnExceptionCallback_t1653610982), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (U3CStartU3Ec__Iterator0_t2919987970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[4] = 
{
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[10] = 
{
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CrunningU3E__0_0(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CwfeofU3E__1_1(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CtexU3E__2_2(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CwU3E__3_3(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3ChU3E__4_4(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CbpU3E__5_5(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U24this_6(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U24current_7(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U24disposing_8(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[2] = 
{
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828::get_offset_of_player_0(),
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828::get_offset_of_eventId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[3] = 
{
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_player_0(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_type_1(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_msg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (PlayerPrefsUtility_t259601127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (SimpleEncryption_t619545967), -1, sizeof(SimpleEncryption_t619545967_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2871[2] = 
{
	SimpleEncryption_t619545967_StaticFields::get_offset_of_key_0(),
	SimpleEncryption_t619545967_StaticFields::get_offset_of_provider_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (CommandSheet_t2769384292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[1] = 
{
	CommandSheet_t2769384292::get_offset_of_param_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (Param_t4123818474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[11] = 
{
	Param_t4123818474::get_offset_of_Command_0(),
	Param_t4123818474::get_offset_of_wait_1(),
	Param_t4123818474::get_offset_of_Arg1_2(),
	Param_t4123818474::get_offset_of_Arg2_3(),
	Param_t4123818474::get_offset_of_Arg3_4(),
	Param_t4123818474::get_offset_of_Arg4_5(),
	Param_t4123818474::get_offset_of_Arg5_6(),
	Param_t4123818474::get_offset_of_Type_7(),
	Param_t4123818474::get_offset_of_Text_8(),
	Param_t4123818474::get_offset_of_SubText_9(),
	Param_t4123818474::get_offset_of_Voice_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (Images_t563431294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[26] = 
{
	Images_t563431294::get_offset_of_clearImage_0(),
	Images_t563431294::get_offset_of_collapseImage_1(),
	Images_t563431294::get_offset_of_clearOnNewSceneImage_2(),
	Images_t563431294::get_offset_of_showTimeImage_3(),
	Images_t563431294::get_offset_of_showSceneImage_4(),
	Images_t563431294::get_offset_of_userImage_5(),
	Images_t563431294::get_offset_of_showMemoryImage_6(),
	Images_t563431294::get_offset_of_softwareImage_7(),
	Images_t563431294::get_offset_of_dateImage_8(),
	Images_t563431294::get_offset_of_showFpsImage_9(),
	Images_t563431294::get_offset_of_infoImage_10(),
	Images_t563431294::get_offset_of_searchImage_11(),
	Images_t563431294::get_offset_of_closeImage_12(),
	Images_t563431294::get_offset_of_buildFromImage_13(),
	Images_t563431294::get_offset_of_systemInfoImage_14(),
	Images_t563431294::get_offset_of_graphicsInfoImage_15(),
	Images_t563431294::get_offset_of_backImage_16(),
	Images_t563431294::get_offset_of_logImage_17(),
	Images_t563431294::get_offset_of_warningImage_18(),
	Images_t563431294::get_offset_of_errorImage_19(),
	Images_t563431294::get_offset_of_barImage_20(),
	Images_t563431294::get_offset_of_button_activeImage_21(),
	Images_t563431294::get_offset_of_even_logImage_22(),
	Images_t563431294::get_offset_of_odd_logImage_23(),
	Images_t563431294::get_offset_of_selectedImage_24(),
	Images_t563431294::get_offset_of_reporterScrollerSkin_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (Reporter_t3561640551), -1, sizeof(Reporter_t3561640551_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2876[147] = 
{
	Reporter_t3561640551::get_offset_of_samples_2(),
	Reporter_t3561640551::get_offset_of_logs_3(),
	Reporter_t3561640551::get_offset_of_collapsedLogs_4(),
	Reporter_t3561640551::get_offset_of_currentLog_5(),
	Reporter_t3561640551::get_offset_of_logsDic_6(),
	Reporter_t3561640551::get_offset_of_cachedString_7(),
	Reporter_t3561640551::get_offset_of_show_8(),
	Reporter_t3561640551::get_offset_of_collapse_9(),
	Reporter_t3561640551::get_offset_of_clearOnNewSceneLoaded_10(),
	Reporter_t3561640551::get_offset_of_showTime_11(),
	Reporter_t3561640551::get_offset_of_showScene_12(),
	Reporter_t3561640551::get_offset_of_showMemory_13(),
	Reporter_t3561640551::get_offset_of_showFps_14(),
	Reporter_t3561640551::get_offset_of_showGraph_15(),
	Reporter_t3561640551::get_offset_of_showLog_16(),
	Reporter_t3561640551::get_offset_of_showWarning_17(),
	Reporter_t3561640551::get_offset_of_showError_18(),
	Reporter_t3561640551::get_offset_of_numOfLogs_19(),
	Reporter_t3561640551::get_offset_of_numOfLogsWarning_20(),
	Reporter_t3561640551::get_offset_of_numOfLogsError_21(),
	Reporter_t3561640551::get_offset_of_numOfCollapsedLogs_22(),
	Reporter_t3561640551::get_offset_of_numOfCollapsedLogsWarning_23(),
	Reporter_t3561640551::get_offset_of_numOfCollapsedLogsError_24(),
	Reporter_t3561640551::get_offset_of_showClearOnNewSceneLoadedButton_25(),
	Reporter_t3561640551::get_offset_of_showTimeButton_26(),
	Reporter_t3561640551::get_offset_of_showSceneButton_27(),
	Reporter_t3561640551::get_offset_of_showMemButton_28(),
	Reporter_t3561640551::get_offset_of_showFpsButton_29(),
	Reporter_t3561640551::get_offset_of_showSearchText_30(),
	Reporter_t3561640551::get_offset_of_buildDate_31(),
	Reporter_t3561640551::get_offset_of_logDate_32(),
	Reporter_t3561640551::get_offset_of_logsMemUsage_33(),
	Reporter_t3561640551::get_offset_of_graphMemUsage_34(),
	Reporter_t3561640551::get_offset_of_gcTotalMemory_35(),
	Reporter_t3561640551::get_offset_of_UserData_36(),
	Reporter_t3561640551::get_offset_of_fps_37(),
	Reporter_t3561640551::get_offset_of_fpsText_38(),
	Reporter_t3561640551::get_offset_of_currentView_39(),
	Reporter_t3561640551_StaticFields::get_offset_of_created_40(),
	Reporter_t3561640551::get_offset_of_images_41(),
	Reporter_t3561640551::get_offset_of_clearContent_42(),
	Reporter_t3561640551::get_offset_of_collapseContent_43(),
	Reporter_t3561640551::get_offset_of_clearOnNewSceneContent_44(),
	Reporter_t3561640551::get_offset_of_showTimeContent_45(),
	Reporter_t3561640551::get_offset_of_showSceneContent_46(),
	Reporter_t3561640551::get_offset_of_userContent_47(),
	Reporter_t3561640551::get_offset_of_showMemoryContent_48(),
	Reporter_t3561640551::get_offset_of_softwareContent_49(),
	Reporter_t3561640551::get_offset_of_dateContent_50(),
	Reporter_t3561640551::get_offset_of_showFpsContent_51(),
	Reporter_t3561640551::get_offset_of_infoContent_52(),
	Reporter_t3561640551::get_offset_of_searchContent_53(),
	Reporter_t3561640551::get_offset_of_closeContent_54(),
	Reporter_t3561640551::get_offset_of_buildFromContent_55(),
	Reporter_t3561640551::get_offset_of_systemInfoContent_56(),
	Reporter_t3561640551::get_offset_of_graphicsInfoContent_57(),
	Reporter_t3561640551::get_offset_of_backContent_58(),
	Reporter_t3561640551::get_offset_of_logContent_59(),
	Reporter_t3561640551::get_offset_of_warningContent_60(),
	Reporter_t3561640551::get_offset_of_errorContent_61(),
	Reporter_t3561640551::get_offset_of_barStyle_62(),
	Reporter_t3561640551::get_offset_of_buttonActiveStyle_63(),
	Reporter_t3561640551::get_offset_of_nonStyle_64(),
	Reporter_t3561640551::get_offset_of_lowerLeftFontStyle_65(),
	Reporter_t3561640551::get_offset_of_backStyle_66(),
	Reporter_t3561640551::get_offset_of_evenLogStyle_67(),
	Reporter_t3561640551::get_offset_of_oddLogStyle_68(),
	Reporter_t3561640551::get_offset_of_logButtonStyle_69(),
	Reporter_t3561640551::get_offset_of_selectedLogStyle_70(),
	Reporter_t3561640551::get_offset_of_selectedLogFontStyle_71(),
	Reporter_t3561640551::get_offset_of_stackLabelStyle_72(),
	Reporter_t3561640551::get_offset_of_scrollerStyle_73(),
	Reporter_t3561640551::get_offset_of_searchStyle_74(),
	Reporter_t3561640551::get_offset_of_sliderBackStyle_75(),
	Reporter_t3561640551::get_offset_of_sliderThumbStyle_76(),
	Reporter_t3561640551::get_offset_of_toolbarScrollerSkin_77(),
	Reporter_t3561640551::get_offset_of_logScrollerSkin_78(),
	Reporter_t3561640551::get_offset_of_graphScrollerSkin_79(),
	Reporter_t3561640551::get_offset_of_size_80(),
	Reporter_t3561640551::get_offset_of_maxSize_81(),
	Reporter_t3561640551::get_offset_of_numOfCircleToShow_82(),
	Reporter_t3561640551_StaticFields::get_offset_of_scenes_83(),
	Reporter_t3561640551::get_offset_of_currentScene_84(),
	Reporter_t3561640551::get_offset_of_filterText_85(),
	Reporter_t3561640551::get_offset_of_deviceModel_86(),
	Reporter_t3561640551::get_offset_of_deviceType_87(),
	Reporter_t3561640551::get_offset_of_deviceName_88(),
	Reporter_t3561640551::get_offset_of_graphicsMemorySize_89(),
	Reporter_t3561640551::get_offset_of_maxTextureSize_90(),
	Reporter_t3561640551::get_offset_of_systemMemorySize_91(),
	Reporter_t3561640551::get_offset_of_Initialized_92(),
	Reporter_t3561640551::get_offset_of_screenRect_93(),
	Reporter_t3561640551::get_offset_of_toolBarRect_94(),
	Reporter_t3561640551::get_offset_of_logsRect_95(),
	Reporter_t3561640551::get_offset_of_stackRect_96(),
	Reporter_t3561640551::get_offset_of_graphRect_97(),
	Reporter_t3561640551::get_offset_of_graphMinRect_98(),
	Reporter_t3561640551::get_offset_of_graphMaxRect_99(),
	Reporter_t3561640551::get_offset_of_buttomRect_100(),
	Reporter_t3561640551::get_offset_of_stackRectTopLeft_101(),
	Reporter_t3561640551::get_offset_of_detailRect_102(),
	Reporter_t3561640551::get_offset_of_scrollPosition_103(),
	Reporter_t3561640551::get_offset_of_scrollPosition2_104(),
	Reporter_t3561640551::get_offset_of_toolbarScrollPosition_105(),
	Reporter_t3561640551::get_offset_of_selectedLog_106(),
	Reporter_t3561640551::get_offset_of_toolbarOldDrag_107(),
	Reporter_t3561640551::get_offset_of_oldDrag_108(),
	Reporter_t3561640551::get_offset_of_oldDrag2_109(),
	Reporter_t3561640551::get_offset_of_oldDrag3_110(),
	Reporter_t3561640551::get_offset_of_startIndex_111(),
	Reporter_t3561640551::get_offset_of_countRect_112(),
	Reporter_t3561640551::get_offset_of_timeRect_113(),
	Reporter_t3561640551::get_offset_of_timeLabelRect_114(),
	Reporter_t3561640551::get_offset_of_sceneRect_115(),
	Reporter_t3561640551::get_offset_of_sceneLabelRect_116(),
	Reporter_t3561640551::get_offset_of_memoryRect_117(),
	Reporter_t3561640551::get_offset_of_memoryLabelRect_118(),
	Reporter_t3561640551::get_offset_of_fpsRect_119(),
	Reporter_t3561640551::get_offset_of_fpsLabelRect_120(),
	Reporter_t3561640551::get_offset_of_tempContent_121(),
	Reporter_t3561640551::get_offset_of_infoScrollPosition_122(),
	Reporter_t3561640551::get_offset_of_oldInfoDrag_123(),
	Reporter_t3561640551::get_offset_of_tempRect_124(),
	Reporter_t3561640551::get_offset_of_graphSize_125(),
	Reporter_t3561640551::get_offset_of_startFrame_126(),
	Reporter_t3561640551::get_offset_of_currentFrame_127(),
	Reporter_t3561640551::get_offset_of_tempVector1_128(),
	Reporter_t3561640551::get_offset_of_tempVector2_129(),
	Reporter_t3561640551::get_offset_of_graphScrollerPos_130(),
	Reporter_t3561640551::get_offset_of_maxFpsValue_131(),
	Reporter_t3561640551::get_offset_of_minFpsValue_132(),
	Reporter_t3561640551::get_offset_of_maxMemoryValue_133(),
	Reporter_t3561640551::get_offset_of_minMemoryValue_134(),
	Reporter_t3561640551::get_offset_of_gestureDetector_135(),
	Reporter_t3561640551::get_offset_of_gestureSum_136(),
	Reporter_t3561640551::get_offset_of_gestureLength_137(),
	Reporter_t3561640551::get_offset_of_gestureCount_138(),
	Reporter_t3561640551::get_offset_of_lastClickTime_139(),
	Reporter_t3561640551::get_offset_of_startPos_140(),
	Reporter_t3561640551::get_offset_of_downPos_141(),
	Reporter_t3561640551::get_offset_of_mousePosition_142(),
	Reporter_t3561640551::get_offset_of_frames_143(),
	Reporter_t3561640551::get_offset_of_firstTime_144(),
	Reporter_t3561640551::get_offset_of_lastUpdate_145(),
	0,
	0,
	Reporter_t3561640551::get_offset_of_threadedLogs_148(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (_LogType_t4109028099)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2877[6] = 
{
	_LogType_t4109028099::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (Sample_t3185432476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[5] = 
{
	Sample_t3185432476::get_offset_of_time_0(),
	Sample_t3185432476::get_offset_of_loadedScene_1(),
	Sample_t3185432476::get_offset_of_memory_2(),
	Sample_t3185432476::get_offset_of_fps_3(),
	Sample_t3185432476::get_offset_of_fpsText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (Log_t3604182180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[5] = 
{
	Log_t3604182180::get_offset_of_count_0(),
	Log_t3604182180::get_offset_of_logType_1(),
	Log_t3604182180::get_offset_of_condition_2(),
	Log_t3604182180::get_offset_of_stacktrace_3(),
	Log_t3604182180::get_offset_of_sampleId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (ReportView_t608488827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2880[5] = 
{
	ReportView_t608488827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (DetailView_t904843682)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2881[4] = 
{
	DetailView_t904843682::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (U3CreadInfoU3Ec__Iterator0_t2714882631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[7] = 
{
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U3CprefFileU3E__0_0(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U3CurlU3E__1_1(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U3CwwwU3E__2_2(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U24this_3(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U24current_4(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U24disposing_5(),
	U3CreadInfoU3Ec__Iterator0_t2714882631::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (ReporterGUI_t402918452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[1] = 
{
	ReporterGUI_t402918452::get_offset_of_reporter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (ReporterMessageReceiver_t1067427633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2884[1] = 
{
	ReporterMessageReceiver_t1067427633::get_offset_of_reporter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (Rotate_t4255939431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[1] = 
{
	Rotate_t4255939431::get_offset_of_angle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (TestReporter_t864384665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[14] = 
{
	TestReporter_t864384665::get_offset_of_logTestCount_2(),
	TestReporter_t864384665::get_offset_of_threadLogTestCount_3(),
	TestReporter_t864384665::get_offset_of_logEverySecond_4(),
	TestReporter_t864384665::get_offset_of_currentLogTestCount_5(),
	TestReporter_t864384665::get_offset_of_reporter_6(),
	TestReporter_t864384665::get_offset_of_style_7(),
	TestReporter_t864384665::get_offset_of_rect1_8(),
	TestReporter_t864384665::get_offset_of_rect2_9(),
	TestReporter_t864384665::get_offset_of_rect3_10(),
	TestReporter_t864384665::get_offset_of_rect4_11(),
	TestReporter_t864384665::get_offset_of_rect5_12(),
	TestReporter_t864384665::get_offset_of_rect6_13(),
	TestReporter_t864384665::get_offset_of_thread_14(),
	TestReporter_t864384665::get_offset_of_elapsed_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2887[8] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2DBCF138F04584B19429C46D7843B6049BC19AAA41_0(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D51B2AA051AFFF21EBC28102EA2F57BEF007038AE_1(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_2(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_3(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_5(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_6(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (U24ArrayTypeU3D28_t2375206772)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D28_t2375206772 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
