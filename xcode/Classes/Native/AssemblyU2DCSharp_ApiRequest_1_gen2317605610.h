﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action
struct Action_t3226471752;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiRequest`1<System.Object>
struct  ApiRequest_1_t2317605610  : public Il2CppObject
{
public:
	// System.String ApiRequest`1::apiUrl
	String_t* ___apiUrl_0;
	// System.Action`1<T> ApiRequest`1::onRequestSuccess
	Action_1_t2491248677 * ___onRequestSuccess_1;
	// System.Action ApiRequest`1::onRequestError
	Action_t3226471752 * ___onRequestError_2;
	// UnityEngine.WWWForm ApiRequest`1::form
	WWWForm_t3950226929 * ___form_3;

public:
	inline static int32_t get_offset_of_apiUrl_0() { return static_cast<int32_t>(offsetof(ApiRequest_1_t2317605610, ___apiUrl_0)); }
	inline String_t* get_apiUrl_0() const { return ___apiUrl_0; }
	inline String_t** get_address_of_apiUrl_0() { return &___apiUrl_0; }
	inline void set_apiUrl_0(String_t* value)
	{
		___apiUrl_0 = value;
		Il2CppCodeGenWriteBarrier(&___apiUrl_0, value);
	}

	inline static int32_t get_offset_of_onRequestSuccess_1() { return static_cast<int32_t>(offsetof(ApiRequest_1_t2317605610, ___onRequestSuccess_1)); }
	inline Action_1_t2491248677 * get_onRequestSuccess_1() const { return ___onRequestSuccess_1; }
	inline Action_1_t2491248677 ** get_address_of_onRequestSuccess_1() { return &___onRequestSuccess_1; }
	inline void set_onRequestSuccess_1(Action_1_t2491248677 * value)
	{
		___onRequestSuccess_1 = value;
		Il2CppCodeGenWriteBarrier(&___onRequestSuccess_1, value);
	}

	inline static int32_t get_offset_of_onRequestError_2() { return static_cast<int32_t>(offsetof(ApiRequest_1_t2317605610, ___onRequestError_2)); }
	inline Action_t3226471752 * get_onRequestError_2() const { return ___onRequestError_2; }
	inline Action_t3226471752 ** get_address_of_onRequestError_2() { return &___onRequestError_2; }
	inline void set_onRequestError_2(Action_t3226471752 * value)
	{
		___onRequestError_2 = value;
		Il2CppCodeGenWriteBarrier(&___onRequestError_2, value);
	}

	inline static int32_t get_offset_of_form_3() { return static_cast<int32_t>(offsetof(ApiRequest_1_t2317605610, ___form_3)); }
	inline WWWForm_t3950226929 * get_form_3() const { return ___form_3; }
	inline WWWForm_t3950226929 ** get_address_of_form_3() { return &___form_3; }
	inline void set_form_3(WWWForm_t3950226929 * value)
	{
		___form_3 = value;
		Il2CppCodeGenWriteBarrier(&___form_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
