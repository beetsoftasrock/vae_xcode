﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectController
struct SelectController_t1269418144;

#include "AssemblyU2DCSharp_OpenHelpScript3207620564.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenHelp01ChapterTopScript
struct  OpenHelp01ChapterTopScript_t2491901699  : public OpenHelpScript_t3207620564
{
public:
	// SelectController OpenHelp01ChapterTopScript::selectorControler
	SelectController_t1269418144 * ___selectorControler_10;

public:
	inline static int32_t get_offset_of_selectorControler_10() { return static_cast<int32_t>(offsetof(OpenHelp01ChapterTopScript_t2491901699, ___selectorControler_10)); }
	inline SelectController_t1269418144 * get_selectorControler_10() const { return ___selectorControler_10; }
	inline SelectController_t1269418144 ** get_address_of_selectorControler_10() { return &___selectorControler_10; }
	inline void set_selectorControler_10(SelectController_t1269418144 * value)
	{
		___selectorControler_10 = value;
		Il2CppCodeGenWriteBarrier(&___selectorControler_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
