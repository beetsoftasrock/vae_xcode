﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZipSoundItem
struct ZipSoundItem_t558808959;

#include "codegen/il2cpp-codegen.h"

// System.Void ZipSoundItem::.ctor()
extern "C"  void ZipSoundItem__ctor_m24920296 (ZipSoundItem_t558808959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
