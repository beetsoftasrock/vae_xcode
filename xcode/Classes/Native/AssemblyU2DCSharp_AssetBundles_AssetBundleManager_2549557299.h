﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;
// System.String
struct String_t;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action
struct Action_t3226471752;
// AssetBundles.AssetBundleManager
struct AssetBundleManager_t364944953;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0
struct  U3CloadManifestFileU3Ec__Iterator0_t2549557299  : public Il2CppObject
{
public:
	// UnityEngine.WWW AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::<download>__0
	WWW_t2919945039 * ___U3CdownloadU3E__0_0;
	// System.String AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::<manifestPath>__1
	String_t* ___U3CmanifestPathU3E__1_1;
	// System.Boolean AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::<existManifest>__2
	bool ___U3CexistManifestU3E__2_2;
	// System.Single AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::<t>__3
	float ___U3CtU3E__3_3;
	// System.Action`1<System.Single> AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::onProcess
	Action_1_t1878309314 * ___onProcess_4;
	// System.Action`1<System.String> AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::onError
	Action_1_t1831019615 * ___onError_5;
	// System.Action AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::onSuccess
	Action_t3226471752 * ___onSuccess_6;
	// AssetBundles.AssetBundleManager AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::$this
	AssetBundleManager_t364944953 * ___U24this_7;
	// System.Object AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::$current
	Il2CppObject * ___U24current_8;
	// System.Boolean AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 AssetBundles.AssetBundleManager/<loadManifestFile>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CdownloadU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___U3CdownloadU3E__0_0)); }
	inline WWW_t2919945039 * get_U3CdownloadU3E__0_0() const { return ___U3CdownloadU3E__0_0; }
	inline WWW_t2919945039 ** get_address_of_U3CdownloadU3E__0_0() { return &___U3CdownloadU3E__0_0; }
	inline void set_U3CdownloadU3E__0_0(WWW_t2919945039 * value)
	{
		___U3CdownloadU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdownloadU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CmanifestPathU3E__1_1() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___U3CmanifestPathU3E__1_1)); }
	inline String_t* get_U3CmanifestPathU3E__1_1() const { return ___U3CmanifestPathU3E__1_1; }
	inline String_t** get_address_of_U3CmanifestPathU3E__1_1() { return &___U3CmanifestPathU3E__1_1; }
	inline void set_U3CmanifestPathU3E__1_1(String_t* value)
	{
		___U3CmanifestPathU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmanifestPathU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CexistManifestU3E__2_2() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___U3CexistManifestU3E__2_2)); }
	inline bool get_U3CexistManifestU3E__2_2() const { return ___U3CexistManifestU3E__2_2; }
	inline bool* get_address_of_U3CexistManifestU3E__2_2() { return &___U3CexistManifestU3E__2_2; }
	inline void set_U3CexistManifestU3E__2_2(bool value)
	{
		___U3CexistManifestU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__3_3() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___U3CtU3E__3_3)); }
	inline float get_U3CtU3E__3_3() const { return ___U3CtU3E__3_3; }
	inline float* get_address_of_U3CtU3E__3_3() { return &___U3CtU3E__3_3; }
	inline void set_U3CtU3E__3_3(float value)
	{
		___U3CtU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_onProcess_4() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___onProcess_4)); }
	inline Action_1_t1878309314 * get_onProcess_4() const { return ___onProcess_4; }
	inline Action_1_t1878309314 ** get_address_of_onProcess_4() { return &___onProcess_4; }
	inline void set_onProcess_4(Action_1_t1878309314 * value)
	{
		___onProcess_4 = value;
		Il2CppCodeGenWriteBarrier(&___onProcess_4, value);
	}

	inline static int32_t get_offset_of_onError_5() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___onError_5)); }
	inline Action_1_t1831019615 * get_onError_5() const { return ___onError_5; }
	inline Action_1_t1831019615 ** get_address_of_onError_5() { return &___onError_5; }
	inline void set_onError_5(Action_1_t1831019615 * value)
	{
		___onError_5 = value;
		Il2CppCodeGenWriteBarrier(&___onError_5, value);
	}

	inline static int32_t get_offset_of_onSuccess_6() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___onSuccess_6)); }
	inline Action_t3226471752 * get_onSuccess_6() const { return ___onSuccess_6; }
	inline Action_t3226471752 ** get_address_of_onSuccess_6() { return &___onSuccess_6; }
	inline void set_onSuccess_6(Action_t3226471752 * value)
	{
		___onSuccess_6 = value;
		Il2CppCodeGenWriteBarrier(&___onSuccess_6, value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___U24this_7)); }
	inline AssetBundleManager_t364944953 * get_U24this_7() const { return ___U24this_7; }
	inline AssetBundleManager_t364944953 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(AssetBundleManager_t364944953 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CloadManifestFileU3Ec__Iterator0_t2549557299, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
