﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ConversationSelectionGroupData
struct ConversationSelectionGroupData_t3437723178;

#include "AssemblyU2DCSharp_ConversationCommand3660105836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionCommand
struct  SelectionCommand_t2511546469  : public ConversationCommand_t3660105836
{
public:
	// System.String SelectionCommand::_role
	String_t* ____role_0;
	// ConversationSelectionGroupData SelectionCommand::_data
	ConversationSelectionGroupData_t3437723178 * ____data_1;
	// System.Int32 SelectionCommand::_preCurrentPoint
	int32_t ____preCurrentPoint_2;

public:
	inline static int32_t get_offset_of__role_0() { return static_cast<int32_t>(offsetof(SelectionCommand_t2511546469, ____role_0)); }
	inline String_t* get__role_0() const { return ____role_0; }
	inline String_t** get_address_of__role_0() { return &____role_0; }
	inline void set__role_0(String_t* value)
	{
		____role_0 = value;
		Il2CppCodeGenWriteBarrier(&____role_0, value);
	}

	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(SelectionCommand_t2511546469, ____data_1)); }
	inline ConversationSelectionGroupData_t3437723178 * get__data_1() const { return ____data_1; }
	inline ConversationSelectionGroupData_t3437723178 ** get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(ConversationSelectionGroupData_t3437723178 * value)
	{
		____data_1 = value;
		Il2CppCodeGenWriteBarrier(&____data_1, value);
	}

	inline static int32_t get_offset_of__preCurrentPoint_2() { return static_cast<int32_t>(offsetof(SelectionCommand_t2511546469, ____preCurrentPoint_2)); }
	inline int32_t get__preCurrentPoint_2() const { return ____preCurrentPoint_2; }
	inline int32_t* get_address_of__preCurrentPoint_2() { return &____preCurrentPoint_2; }
	inline void set__preCurrentPoint_2(int32_t value)
	{
		____preCurrentPoint_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
