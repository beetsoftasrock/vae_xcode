﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorSettingAsset
struct ColorSettingAsset_t2075258157;

#include "codegen/il2cpp-codegen.h"

// System.Void ColorSettingAsset::.ctor()
extern "C"  void ColorSettingAsset__ctor_m2945850174 (ColorSettingAsset_t2075258157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
