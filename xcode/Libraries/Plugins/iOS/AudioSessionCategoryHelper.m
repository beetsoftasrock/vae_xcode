//
//  AudioSessionCategoryHelper.m
//  make-unity-plugin
//
//  Created by BEETSOFT on 7/10/17.
//  Copyright © 2017 BEETSOFT. All rights reserved.
//

#import<Foundation/Foundation.h>
#include "AudioToolbox/AudioToolbox.h"
#include <AVFoundation/AVFoundation.h>

extern void _activeSilentMode()
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryAmbient error:nil];
    [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];

    [audioSession setActive:YES error:nil];
    NSLog(@"-ios: active silent mode");
}

extern void _deactiveSilentMode()
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [audioSession setActive:YES error:nil];
    NSLog(@"-ios : deactive silent mode");
}
